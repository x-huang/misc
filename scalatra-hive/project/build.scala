import sbt._
import Keys._
import org.scalatra.sbt._
import org.scalatra.sbt.PluginKeys._
import com.earldouglas.xwp.JettyPlugin
// import com.mojolly.scalate.ScalatePlugin._
// import ScalateKeys._

object ScalatrahiveBuild extends Build {
  val Organization = "xw.data"
  val Name = "scalatra-hivelite"
  val Version = "1.0"
  val ScalaVersion = "2.11.8"
  val ScalatraVersion = "2.4.1"
  val HiveVersion = "1.0.1"
  val HadoopVersion = "2.7.1"

  lazy val project = Project (
    "scalatra-hive",
    file("."),
    settings = ScalatraPlugin.scalatraSettings /* ++ scalateSettings*/ ++ Seq(
      organization := Organization,
      name := Name,
      version := Version,
      scalaVersion := ScalaVersion,

      javacOptions ++= Seq("-source", "1.7", "-target", "1.7"),
      scalacOptions += "-target:jvm-1.7",

      resolvers += Classpaths.typesafeReleases,
      resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases",
  	  resolvers += "Cloudara" at "https://repository.cloudera.com/artifactory/datanucleus",

      libraryDependencies ++= Seq(
		
        "org.scalatra" %% "scalatra" % ScalatraVersion,
        "org.scalatra" %% "scalatra-specs2" % ScalatraVersion % "test",
        "ch.qos.logback" % "logback-classic" % "1.1.5" % "runtime",
        "org.eclipse.jetty" % "jetty-webapp" % "9.2.15.v20160210" % "container,compile",
        "javax.servlet" % "javax.servlet-api" % "3.1.0", // % "provided",
		    "org.scalatra" %% "scalatra-json" % ScalatraVersion,
		    "org.json4s"   %% "json4s-jackson" % "3.3.0",
		    "org.apache.hadoop" % "hadoop-common" % HadoopVersion % "provided",
		    "org.apache.hadoop" % "hadoop-mapreduce-client-core" % HadoopVersion % "provided",
		    "org.apache.hive" % "hive-common" % HiveVersion % "provided",
        "org.apache.hive" % "hive-service" % HiveVersion % "provided" excludeAll (
				  ExclusionRule(organization = "org.pentaho"),
				  ExclusionRule(organization = "org.apache.calcite")
			  ),
		    "org.apache.hive" % "hive-exec" % HiveVersion % "provided" excludeAll (
				  ExclusionRule(organization = "org.pentaho"),
				  ExclusionRule(organization = "org.apache.calcite")
			  )
      )/*,
      scalateTemplateConfig in Compile <<= (sourceDirectory in Compile){ base =>
        Seq(
          TemplateConfig(
            base / "webapp" / "WEB-INF" / "templates",
            Seq.empty,  /* default imports should be added here */
            Seq(
              Binding("context", "_root_.org.scalatra.scalate.ScalatraRenderContext", importMembers = true, isImplicit = true)
            ),  /* add extra bindings here */
            Some("templates")
          )
        )
      } */
    )
  ).enablePlugins(JettyPlugin)
}
