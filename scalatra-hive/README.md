# scalatra-hive #

Web app wrapper of `hive-lite` allowing executing hive queries through web API, build on Scalatra.

## Build & Run ##

```sh
$ cd scalatra-hive
$ ./sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.


## Example

Assuming *test.ql* contains a valid Hive select statement.

```
$ cat test.ql | curl -i -X POST --header 'Content-Type: application/text' --data-binary @- http://localhost:8099/execute
HTTP/1.1 200 OK
Date: Tue, 02 Aug 2016 21:15:39 GMT
Content-Type: application/json; charset=UTF-8
schema: id:string,event_time:string,last_event_time:string,purchased:int
Access-Control-Allow-Origin: *
Content-Length: 4400
Server: Jetty(9.2.z-SNAPSHOT)

10005344627 20160801235934  20160801232437  0
10005344627 20160801235934  20160801231315  0
10005344627 20160801235934  20160801230343  0
10005344627 20160801235934  20160801225659  0
10005344627 20160801235934  20160801231103  0
...
```
