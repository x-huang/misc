package xw.data.hivelite

import org.apache.hadoop.hive.conf.HiveConf
import org.apache.hive.service.cli.CLIService

/**
 * Created by xhuang on 8/1/16.
 */
class HiveLite(username: String, password: String, hiveConf: HiveConf = new HiveConf())
    extends AutoCloseable {

  private val logger = org.slf4j.LoggerFactory.getLogger(getClass)

  val client: CLIService = new CLIService(null)
  client.init(hiveConf)

  val session = client.openSession(username, password, new java.util.HashMap[String, String]())
  logger.debug("new session id: {}", session.getSessionId)
  setConf(HiveConf.ConfVars.HIVE_SUPPORT_CONCURRENCY.varname, "false")

  val confOverlay = new java.util.HashMap[String, String]()

  def execCmd(cmd: String): Unit = {
    logger.debug("executing cmd: {}", cmd)
    val op = client.executeStatement(session, cmd, confOverlay)
    client.closeOperation(op)
  }

  def setConf(name: String, value: String): Unit = {
    val cmd = "SET " + name + " = " + value
    execCmd(cmd)
  }

  def execQuery(stmt: String) = new HiveLiteQuery(this, stmt)

  override def close(): Unit = {
    val id = session.getSessionId
    client.closeSession(session)
    logger.debug("hive session closed: {}", id)
  }
}
