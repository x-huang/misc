package xw.data.hivelite

import xw.data.schema.{TypeUtils, Schema}

import scala.collection.JavaConversions._

/**
 * Created by xhuang on 8/1/16.
 */
class HiveLiteQuery(hivelite: HiveLite, stmt: String) extends AutoCloseable {

  private val logger = org.slf4j.LoggerFactory.getLogger(getClass)

  private val op = hivelite.client.executeStatement(hivelite.session, stmt, hivelite.confOverlay)
  private val rowSet = hivelite.client.fetchResults(op)
  private var closed = false

  def getStateName = hivelite.client.getOperationStatus(op).getState.name()
  def numRows = rowSet.numRows()
  def numColumns = rowSet.numColumns()

  def resultSchema(): Schema = {
    val builder = Schema.newBuilder()
    val metadata = hivelite.client.getResultSetMetadata(op)
    metadata.getColumnDescriptors.foreach { col =>
      val s = col.getName
      val colName = s.substring(s.lastIndexOf('.') + 1)
      builder.addColumn(colName, TypeUtils.parse(col.getTypeName.toLowerCase))
    }
    builder.build()
  }

  def rowIterator(): Iterable[Array[AnyRef]] = {
    rowSet.iterator().toIterable
  }

  override def close(): Unit = {
    hivelite.client.closeOperation(op)
    logger.debug("hive query operation closed")
    closed = true
  }
}
