package xw.data.schema;

import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.*;

import java.util.List;

/**
 * Created by xhuang on 6/2/16.
 */
public class TypeUtils
{
    public static TypeInfo parse(String s) {
        return TypeInfoUtils.getTypeInfoFromTypeString(s);
    }

    public static boolean strictlyEqual(TypeInfo t1, TypeInfo t2) {
        final ObjectInspector.Category category = t1.getCategory();
        if (category != t2.getCategory()) {
            return false;
        }
        switch (category) {
            case PRIMITIVE:
                return t1.getTypeName().equals(t2.getTypeName());
            case LIST:
                final ListTypeInfo l1 = (ListTypeInfo) t1;
                final ListTypeInfo l2 = (ListTypeInfo) t2;
                return strictlyEqual(l1.getListElementTypeInfo(), l2.getListElementTypeInfo());
            case MAP:
                final MapTypeInfo m1 = (MapTypeInfo) t1;
                final MapTypeInfo m2 = (MapTypeInfo) t2;
                return strictlyEqual(m1.getMapKeyTypeInfo(), m2.getMapKeyTypeInfo()) && strictlyEqual(m1.getMapValueTypeInfo(), m2.getMapValueTypeInfo());
            case STRUCT:
                final StructTypeInfo s1 = (StructTypeInfo) t1;
                final StructTypeInfo s2 = (StructTypeInfo) t2;
                final List<String> fieldNames1 = s1.getAllStructFieldNames();
                final List<String> fieldNames2 = s2.getAllStructFieldNames();
                if (fieldNames1.size() != fieldNames2.size()) {
                    return false;
                } else {
                    final List<TypeInfo> fieldTypes1 = s1.getAllStructFieldTypeInfos();
                    final List<TypeInfo> fieldTypes2 = s2.getAllStructFieldTypeInfos();
                    for (int i=0; i<fieldNames1.size(); i++) {
                        if (! fieldNames1.get(i).equals(fieldNames2.get(i))) {
                            return false;
                        } else if (! strictlyEqual(fieldTypes1.get(i), fieldTypes2.get(i))) {
                            return false;
                        }
                    }
                    return true;
                }
            case UNION:
                final UnionTypeInfo u1 = (UnionTypeInfo) t1;
                final UnionTypeInfo u2 = (UnionTypeInfo) t2;
                final List<TypeInfo> unionTypes1 = u1.getAllUnionObjectTypeInfos();
                final List<TypeInfo> unionTypes2 = u2.getAllUnionObjectTypeInfos();
                if (unionTypes1.size() != unionTypes2.size()) {
                    return false;
                } else {
                    for (int i=0; i<unionTypes1.size(); i++) {
                        if (! strictlyEqual(unionTypes1.get(i), unionTypes2.get(i))) {
                            return false;
                        }
                    }
                    return true;
                }
            default:
                throw new IllegalStateException("unknown type category: " + category);
        }
    }

    public static boolean semanticallyEqual(TypeInfo t1, TypeInfo t2) {
        final ObjectInspector.Category category = t1.getCategory();
        if (category != t2.getCategory()) {
            return false;
        }
        switch (category) {
            case PRIMITIVE:
                return t1.getTypeName().equals(t2.getTypeName());
            case LIST:
                final ListTypeInfo l1 = (ListTypeInfo) t1;
                final ListTypeInfo l2 = (ListTypeInfo) t2;
                return semanticallyEqual(l1.getListElementTypeInfo(), l2.getListElementTypeInfo());
            case MAP:
                final MapTypeInfo m1 = (MapTypeInfo) t1;
                final MapTypeInfo m2 = (MapTypeInfo) t2;
                return semanticallyEqual(m1.getMapKeyTypeInfo(), m2.getMapKeyTypeInfo()) && semanticallyEqual(m1.getMapValueTypeInfo(), m2.getMapValueTypeInfo());
            case STRUCT:
                final StructTypeInfo s1 = (StructTypeInfo) t1;
                final StructTypeInfo s2 = (StructTypeInfo) t2;
                    final List<TypeInfo> fieldTypes1 = s1.getAllStructFieldTypeInfos();
                    final List<TypeInfo> fieldTypes2 = s2.getAllStructFieldTypeInfos();
                if (fieldTypes1.size() != fieldTypes2.size()) {
                    return false;
                } else {
                    for (int i = 0; i < fieldTypes1.size(); i++) {
                        if (!semanticallyEqual(fieldTypes1.get(i), fieldTypes2.get(i))) {
                            return false;
                        }
                    }
                    return true;
                }
            case UNION:
                final UnionTypeInfo u1 = (UnionTypeInfo) t1;
                final UnionTypeInfo u2 = (UnionTypeInfo) t2;
                final List<TypeInfo> unionTypes1 = u1.getAllUnionObjectTypeInfos();
                final List<TypeInfo> unionTypes2 = u2.getAllUnionObjectTypeInfos();
                if (unionTypes1.size() != unionTypes2.size()) {
                    return false;
                } else {
                    for (int i=0; i<unionTypes1.size(); i++) {
                        if (! semanticallyEqual(unionTypes1.get(i), unionTypes2.get(i))) {
                            return false;
                        }
                    }
                    return true;
                }
            default:
                throw new IllegalStateException("unknown type category: " + category);
        }
    }

}
