package xw.data.scalatra.hive

import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json.JacksonJsonSupport
import xw.data.hivelite.HiveLite

class ScalatraHiveServlet extends ScalatraServlet with JacksonJsonSupport with CorsSupport {

  private val logger = org.slf4j.LoggerFactory.getLogger(getClass)

  protected implicit val jsonFormats: Formats = DefaultFormats

  before() {
    log(s"${request.getMethod}\t${request.getRequestURI}\t${request.getQueryString}\t${request.getRemoteUser}\t${request.getRemoteAddr}")
    contentType = formats("json")
  }

  after() {
    response.setHeader("Access-Control-Allow-Origin", "*")
  }

  error {
    case e: IllegalArgumentException => BadRequest(e.getMessage)
  }

/*
echo 'SELECT * FROM db.tbl LIMIT 10 ' | \
  curl -X POST --header 'Content-Type: application/text' --data-binary @- \
    http://localhost:8099/execute
*/
  post("/execute") {
    val stmt = request.body
    log(s"query statement: $stmt")

    val hivelite = new HiveLite("hive", "hive")
    val query = hivelite.execQuery(stmt)
    response.headers("Result-Schema") = query.resultSchema().toString
    response.headers("Row-Number") = query.numRows.toString
    response.headers("Column-Number") = query.numColumns.toString

    val sb = new StringBuilder
    for (row <- query.rowIterator()) {
      var delim = ""
      for (datum <- row) {
        sb.append(delim).append(datum)
        delim = "\t"
      }
      sb.append('\n')
    }
    query.close()
    hivelite.close()

    sb.toString()
  }
}


