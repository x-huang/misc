
Description

This tool is to 'repartition' hive table by 

1.	copying raw bucket files to desired Hadoop file system location with certain partition path pattern;
2.	generating DDL statements to help load partition for destination table.
Usage

Aiming to run as an Oozie java action but also can be used in command line style.


usage: java Main [-ddl-only] [-dry-run] <source-location>

It accepts only one command line argument which is a Hive table partition location, such as:

hdfs:///hive/warehouse/telemetry-hourly/dt=2013-11-14/


Upon starting it reads a property files 'repartition.properties' in current working directory. Below is an example:

# path setting

repartition.custom.binding=cluster=vpc,db=default,tbl=telemetry_daily_tbl,hostname=${RESTORE_HOSTNAME}

repartition.src.template=dt=$dt/hour=$hour/service=$service/$filename

repartition.dest.template=s3n://test-backup-restore/hive/warehouse/default/telemetry-daily/dt=$dt/service=$service/$dt-$hour-$service.$seqno

repartition.ddl.template=s3n://test-backup-restore/hive-ddl/$cluster/$db/$tbl/$dt/$cluster-$db-$tbl-$dt-$hour.q

# ddl statement template settings

repartition.ddl.prologue=\n\
-- table type: EXTERNAL_TABLE\n\
\n\
CREATE EXTERNAL TABLE IF NOT EXISTS $tbl ( \n\
gamertag STRING,\n\
ip BIGINT,\n\
mac STRING,\n\
persona BIGINT,\n\
region STRING,\n\
session STRING,\n\
ts STRING,\n\
step INT,\n\
m STRING,\n\
g STRING,\n\
s STRING,\n\
param MAP<STRING,STRING>,\n\
sku STRING\n\
) \n\
PARTITIONED BY ( \n\
dt string,\n\
service string\n\
) \n\
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' \n\
WITH SERDEPROPERTIES (\n\
'colelction.delim'='&',\n\
'mapkey.delim'='=',\n\
'serialization.format'='1'\n\
)\n\
STORED AS INPUTFORMAT 'org.apache.hadoop.hive.ql.io.RCFileInputFormat' \n\
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.RCFileOutputFormat' \n\
LOCATION '$hostname/hive/warehouse/telemetry-daily/'\n\
;

repartition.ddl.statement=ALTER TABLE  ADD IF NOT EXISTS PARTITION ( dt='$dt', service='$service') \n\
LOCATION  '$hostname/hive/warehouse/telemetry-daily/dt=$dt/service=$service/';

# transport engine parameters

repartition.transport.threads=6
repartition.transport.queue.size=5000
repartition.transport.retries=yes

 
Dev Notes

Single process; multi-threaded.

Will recursively trace down to deepest partition locations if given parameter (path) is of partial spec.

Simply do text template extracting & instantiation to convert partition paths and populate DDL statement content. Template variables are defined in three means:

1.	User specified in property file, see “repartition.custom.binding”.
2.	Partition specifications inferred from concrete file path.
3.	Built-in variables including:
a.	$file – original source file basename
b.	$seqno - No. of this bucket file in partition directory, in form of “000”, “001”, …
c.	$timestamp – system current time when repartition is happening, namely System.currentTimeMillis() 

Template variable variables are specified as:

1. $var
2. ${var}
3. ${var:<regex>}  such as: '${dt:\d\d\d\d-\d\d-\d\d}', no padding space.

Infer partition schema from path pattern, no Hive metastore accessing. Partition specifications are extracted from concrete 

Use transport library for multi-threaded file transferring, with callback hooks to track progress and log. 

Failed task will be retried once.  

Entire file transferring tasks will be undone upon fatal error on one single task. 

Has ddl-only mode to produce DDL statements only; dry-run mode to only go through log. 

Performance

Tested on telemetry_hourly_tbl table. Repartitioning one hour data takes roughly 5 minutes. Two hours to repartition daily data. Statistics for repartitioning 11-14 daily data as an example:

13/11/20 04:33:56 INFO repartition.Repartitioner: [SUCCEEDED] all transport tasks finished successfully (4852/4852 files, 153306473326 bytes, 8168 seconds)
Roughly 67.5GB data per hour.

DDL generation is quick.

