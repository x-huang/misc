#!/bin/bash

export CLASSPATH=/mnt/repartition/lib/*:$HADOOP_HOME/conf:$HADOOP_HOME/*:$HADOOP_HOME/lib/*:$HIVE/lib/
# echo $CLASSPATH

MAIN_CLASS=com.ea.eadp.data.ocean.repartition.Main

echo "sample location: hdfs:///hive/warehouse/telemetry-hourly/dt=2013-11-20/hour=00"

java $MAIN_CLASS $*

