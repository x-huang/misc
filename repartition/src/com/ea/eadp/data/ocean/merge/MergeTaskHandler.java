package com.ea.eadp.data.ocean.merge;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import com.ea.eadp.data.amq.MessageHandler;
import com.ea.eadp.data.ocean.merge.rcfile.FileMerger;

public class MergeTaskHandler implements MessageHandler
{
	private FileMerger _merger;
	private final String S3N_KEY_ID = "AKIAIRUQPEY3NTEO6ALQ";
	private final String S3N_SECRET_KEY = "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";
	
	public MergeTaskHandler(int colSize, int initCapacity) {
		_merger = new FileMerger(colSize, initCapacity);
	}
	
	@Override
	public Status handle(Message msg)
	{
		String text;
		try {
			text = ((TextMessage) msg).getText();
		}
		catch (JMSException e) {
			e.printStackTrace();
			return Status.ERROR;
		}
		if (text.equals("<EOF>")) {
			System.err.println("<EOF> received");
			return Status.OVER;
		}
		String[] splitted = text.split("\t");
		if (splitted.length != 2) {
			System.err.println("[error] mal-formatted task: " + text);
		}
		
		final String dir = splitted[0];
		final String dest = splitted[1].replace("s3n://", "s3n://" + S3N_KEY_ID + ":" + S3N_SECRET_KEY + "@");
		try {
			_merger.merge(dir, dest);
		}
		catch (IOException | URISyntaxException e) {
			System.err.println("[error] " + dir + " => " + dest + " : " + e.getMessage());
			return Status.ERROR;
		}
		System.err.println("[done] " + dir + " => " + dest);
		return Status.ERROR;
	}
}
