package com.ea.eadp.data.ocean.merge.tasks;

public class MergeDirTask
{
    final protected String  _dir;
    final protected String _dest;
    
    public MergeDirTask(String dir, String dest) {
        _dir = dir;
        _dest = dest;
    }
    
    public String getSourceDir() {
        return _dir;
    }

    public String getDestination() {
        return _dest;
    }
    
    @Override
    public String toString() {
    	return _dir + "\t" + _dest;
    }
    
}
