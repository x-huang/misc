package com.ea.eadp.data.ocean.merge.rcfile;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFile;
import org.apache.hadoop.hive.ql.io.RCFileOutputFormat;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.io.SequenceFile.Metadata;
import org.apache.hadoop.io.compress.DefaultCodec;

public class RCFileWritableOutput implements Closeable
{
	private int _count;
	final private Path _file;
	final private RCFile.Writer _writer;
	final private RCFileContext _ctxt;
	
	public RCFileWritableOutput(URI uri, RCFileContext ctxt) 
			throws IOException 
	{
		_ctxt = ctxt;
		_file = new Path(uri);
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(uri, conf);
		RCFileOutputFormat.setColumnNumber(conf, _ctxt.getColumnSize());
		_writer = new RCFile.Writer(fs, conf, _file, null, 
									new Metadata(), new DefaultCodec());
		
		_count = 0;
	}
	
	@Override
	public void close() throws IOException
	{
		_writer.close();
	}

	public void write(BytesRefArrayWritable writable) throws IOException
	{
		_writer.append(writable);
		_count++;
	}

	
	public long getCount()
	{
		return _count;
	}
	
}
