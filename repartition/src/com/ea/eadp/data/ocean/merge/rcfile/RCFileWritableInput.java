package com.ea.eadp.data.ocean.merge.rcfile;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFile;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.io.LongWritable;

public class RCFileWritableInput implements Closeable
{
	private long _count;
	final private Path _file;
	final private RCFile.Reader _reader;
	final private LongWritable _rowId;
	final private RCFileContext _ctxt;
	
	public RCFileWritableInput(URI uri, RCFileContext ctxt) 
			throws IOException
	{
		_ctxt = ctxt;
		_file = new Path(uri);
		Configuration conf = new Configuration();
		
		FileSystem fs = FileSystem.get(uri, conf);
		_reader = new RCFile.Reader(fs, _file, conf);
		
		_rowId = new LongWritable();
		_count = 0;
		
	}
	
	public Path getFilePath()
	{
		return _file;
	}
	
	@Override
	public void close() throws IOException
	{
		_reader.close();
	}

	public BytesRefArrayWritable read() throws IOException
	{
		if (! _reader.next(_rowId))
			return null;
		
		BytesRefArrayWritable writable = new BytesRefArrayWritable(_ctxt.getInitialCapacity());
		_reader.getCurrentRow(writable);
		writable.resetValid(_ctxt.getColumnSize());
		_count++;
		return writable;
	}

	public long getCount()
	{
		return _count;
	}

}
