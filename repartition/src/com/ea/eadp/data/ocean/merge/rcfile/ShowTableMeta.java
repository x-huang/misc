package com.ea.eadp.data.ocean.merge.rcfile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import com.ea.eadp.data.common.uri.DatabaseUriParser;
import com.ea.eadp.data.stream.hive.HiveCommon;
import com.ea.eadp.data.stream.hive.TableRepr;

public class ShowTableMeta
{
	public static void main(String[] args) 
			throws IOException, URISyntaxException
	{
		if (args.length != 1) {
			System.err.println("usage: ShowTableMeta <hive-uri>");
			return;
		}
	
		final URI hiveUri = new URI(args[0]);	
		DatabaseUriParser parser = new DatabaseUriParser(hiveUri);
		final String dbName = parser.getDatabase();
		final String tblName = parser.getTable();
		
		TableRepr tbl = (TableRepr) HiveCommon.getTableRepr(dbName, tblName);
		final int colSize = tbl.columns.size();
		System.out.println("non-partition column size: " + colSize);

		final String location = tbl.dataLocation;
		System.out.println("data location: " + location);

	}
}
