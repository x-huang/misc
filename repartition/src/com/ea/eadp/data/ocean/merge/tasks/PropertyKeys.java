package com.ea.eadp.data.ocean.merge.tasks;

public class PropertyKeys
{
    public static final String MERGE_SRC_TEMPLATE = "merge.src.template";
    public static final String MERGE_SRC_LOCATION = "merge.src.location";
    public static final String MERGE_DEST_TEMPLATE = "merge.dest.template";
    public static final String MERGE_CUSTOM_BINDING = "merge.custom.binding";

    public static final String S3N_ACCESS_KEY_ID = "fs.s3n.awsAccessKeyId";
    public static final String S3N_SECRET_KEY = "fs.s3n.awsSecretAccessKey";
    
    public static final String AMQ_SERVER_URI = "amq.server.uri";
    public static final String AMQ_TASK_QUEUE = "amq.task.queue";
    public static final String AMQ_RESULT_QUEUE = "amq.task.result.queue";
    
    public static final String RCFILE_COL_SIZE = "rcfile.col.size";
    public static final String RCFILE_INIT_CAPACITY = "rcfile.init.capacity";
}
