package com.ea.eadp.data.ocean.merge.tasks;

import java.util.List;

import com.ea.eadp.data.common.lang.StringUtils;

public final class MergeTask 
{
    final protected String[]  _files;
    final protected String _dest;
    
    public MergeTask(List<String> files, String dest) {
        _files = new String[files.size()];
        files.toArray(_files);
        _dest = dest;
    }

    public MergeTask(String str) {
    	String[] splitted = str.split("\t");
    	_files = splitted[0].split(",");
    	_dest = splitted[1];
    }
    
    public String[] getSources() {
        return _files;
    }

    public String getDestination() {
        return _dest;
    }
    
    public int getSourceFileNumber() {
    	return _files.length;
    }
    
    @Override
    public String toString() {
    	return StringUtils.join(_files, "|") + "\t" + _dest;
    }
    
}
