package com.ea.eadp.data.ocean.merge.ddl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import com.ea.eadp.data.common.text.Binding;
import com.ea.eadp.data.common.text.Template;

public class Main
{
	public static String PATH_TMPL = "s3n://eadp-data-backup/data/vpc/default/telemetry_daily_tbl/hive/warehouse/telemetry_daily_tbl/dt=$dt/service=$service/$filename";
	public static String DDL_TMPL = "ALTER TABLE telemetry_daily_tbl ADD IF NOT EXISTS PARTITION (dt='$dt', service='$service');";

	public static void main(String[] args) 
			throws IOException, URISyntaxException
	{	
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int taskCount = 0;
		final Template pathT = new Template(PATH_TMPL, "/", true);
		final Template ddlT = new Template(DDL_TMPL, "\n", false);
		
		while (true) {
			final String line = br.readLine();
			if (line == null)
				break;
			if (line.startsWith("#"))
				continue;
			String[] splitted = line.split("\\s+");
			if (splitted.length != 2) {
				System.err.println("[WARN] mal-formatted task: " + line);
			}
			final String path = splitted[1];
			Binding b = pathT.extract(path);
			System.out.println(ddlT.instantiate(b));
			taskCount++;
			
		}
		
		System.err.println("[INFO] total tasks: " + taskCount);
	}

}
