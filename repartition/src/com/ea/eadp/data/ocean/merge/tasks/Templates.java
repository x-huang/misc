package com.ea.eadp.data.ocean.merge.tasks;

import java.util.Properties;

import com.ea.eadp.data.common.text.Template;

public class Templates
{
	public final Template src;
	public final Template dest;
	
	protected Templates(String srcT, String destT)
	{
		src = new Template(srcT, "/", true);
		dest = new Template(destT, "/", false);
	}
	
	public static Templates fromProperties(Properties prop)
	{
		String srcT = prop.getProperty(PropertyKeys.MERGE_SRC_TEMPLATE);
		String destT = prop.getProperty(PropertyKeys.MERGE_DEST_TEMPLATE);
		return new Templates(srcT, destT);
	}
}
