package com.ea.eadp.data.ocean.merge;

import java.io.IOException;
import java.util.Properties;

import javax.jms.JMSException;

import com.ea.eadp.data.amq.ReadMQ;
import com.ea.eadp.data.ocean.merge.tasks.PropertyKeys;

public class Main
{
	public static final String PROPERTY_FILE = "merge.properties";
	
	public static void main(String[] args) throws JMSException, IOException
	{
		final Properties prop = new Properties();
    	prop.load(Main.class.getClassLoader().getResourceAsStream(PROPERTY_FILE));
    	// prop.load(new java.io.FileInputStream(PROPERTY_FILE));
    	
		final String serverUri = prop.getProperty(PropertyKeys.AMQ_SERVER_URI);
		final String taskQueue = prop.getProperty(PropertyKeys.AMQ_TASK_QUEUE);
		// final String resultQueue = prop.getProperty(PropertyKeys.AMQ_RESULT_QUEUE);
		final int colSize = Integer.parseInt(prop.getProperty(PropertyKeys.RCFILE_COL_SIZE));
		final int initCapacity = Integer.parseInt(prop.getProperty(PropertyKeys.RCFILE_INIT_CAPACITY));
		
		ReadMQ msgReader = new ReadMQ(serverUri, taskQueue, new MergeTaskHandler(colSize, initCapacity));
		msgReader.run();
	}
}
