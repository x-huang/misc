package com.ea.eadp.data.ocean.merge.verify;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;


import com.ea.eadp.data.stream.hadoop.FileStatusRepr;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public class Main
{
	private static boolean fileExists(String uri) 
			throws URISyntaxException, IOException 
	{
		final FileStatusRepr status;
		try {
			status = HadoopCommon.getFileStatusRepr(new URI(uri));
		}
		catch (FileNotFoundException e) {
			return false;
		}
		return !status.isDir && status.length > 0;
	}
	
	public static void main(String[] args) 
			throws IOException, URISyntaxException
	{
		boolean silent = (args.length == 1 && args[0].equals("-s"));
		
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int taskCount = 0;
		int nonExistCount = 0;
		
		while (true) {
			final String line = br.readLine();
			if (line == null)
				break;
			if (line.startsWith("#"))
				continue;
			String[] splitted = line.split("\\s+");
			if (splitted.length != 2) {
				System.err.println("[WARN] mal-formatted task: " + line);
			}
			taskCount++;
			if (!fileExists(splitted[1])) {
				nonExistCount++;
				if (!silent) {
					System.out.println(line);
				}
			}
		}
		
		System.err.println("[INFO] total tasks: " + taskCount);
		System.err.println("[INFO] not found: " + nonExistCount);
	}

}
