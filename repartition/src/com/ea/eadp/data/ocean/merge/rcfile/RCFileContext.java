package com.ea.eadp.data.ocean.merge.rcfile;

public class RCFileContext
{
	final private int _cols;
	final private int _capacity;
	
	public RCFileContext(int cols, int cap) 
	{
		_cols = cols;
		_capacity = cap;
	}
	
	public int getColumnSize() 
	{
		return _cols;
	}
	
	public int getInitialCapacity()
	{
		return _capacity;
	}
}
