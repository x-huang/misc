package com.ea.eadp.data.ocean.merge.tasks;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import com.ea.eadp.data.common.text.Binding;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public final class Main
{
	public static final String PROPERTY_FILE = "merge.properties";
	private static Templates _t = null;
	private static String _srcLocation = null;
	
	protected static List<String> listDates(String startDate, String endDate) 
			throws ParseException 
	{
		
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		final Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(startDate));
		List<String> dates = new ArrayList<>();
		while (true) {
			String dt = sdf.format(c.getTime());
			if (dt.compareTo(endDate) > 0)
				break;
			dates.add(dt);
			c.add(Calendar.DATE, 1);
		}
		return dates;
	}
	
    protected static void listPartitionDirs(String path, List<String> partitionDirs) 
    		throws IOException, URISyntaxException 
    { 	
    	if (!path.endsWith("/"))
    		path += "/";
    		
    	List<String> entries = HadoopCommon.listEntries(new URI(path));
    	    	
    	int dirCount = 0;
    	for (String entry: entries) {
    		if (entry.endsWith("/")) {
    			listPartitionDirs(path + entry, partitionDirs);
    			dirCount++;
    		}
    	}
    	
    	if (dirCount == 0) {
    		partitionDirs.add(path);
    	}
    }
    
    protected static void printMergeTask(String path) 
    		throws IOException, URISyntaxException 
    { 	
    	if (!path.endsWith("/"))
    		path += "/";
    		
    	List<String> entries = HadoopCommon.listEntries(new URI(path));
    	    	
    	int dirCount = 0;
    	for (String entry: entries) {
    		if (entry.endsWith("/")) {
    			printMergeTask(path + entry);
    			dirCount++;
    		}
    	}
    	
    	if (dirCount == 0) {
    		Object task = generateTask(path);
    		if (task != null)
    			System.out.println(task);
    	}
    }
    
	private static URI verifyUri(String rawUri)
	{
		try {
			URI uri = new URI(rawUri);
			if (uri.getScheme() == null) 
				uri = new URI("hdfs://" + rawUri);
			return uri;
		}
		catch (URISyntaxException e) {
			throw new IllegalArgumentException("unrecognized uri: " + rawUri);
		}
	}
    
	private static Object generateTask(String path) 
			throws IOException
	{
		final URI srcUri;
		try {
			srcUri = verifyUri(path);
		}
		catch (IllegalArgumentException e) {
			System.err.println("# unrecognized uri: " + path);
			return null;
		}
		
		if (HadoopCommon.getFileStatusRepr(srcUri).isDir) {
			Binding binding;
			try {
				binding = _t.src.extract(path);
			}
			catch (IllegalArgumentException e) {
				System.err.println("# error extracting from: " + path);
				return null;
			}
			String destPath = _t.dest.instantiate(binding);
			return new MergeDirTask(path, destPath);
			/*
			List<String> files = new ArrayList<>();
			for (String srcFile: HadoopCommon.listFiles(srcUri)) {
				files.add(srcUri + srcFile);
			}
			return new MergeTask(files, destPath); 
			*/
		}
		else {
			System.err.println("# not a dir: " + path);
			return null;
		}
	}
    
    public static void main(String[] args) throws Exception
    {	
    	if (args.length != 2) {
    		System.err.println("usage: java Main <start-date> <end-date>");
        	return;
    	}
    	
    	final List<String> dates = listDates(args[0], args[1]);

    	final Properties prop = new Properties();
    	prop.load(Main.class.getClassLoader().getResourceAsStream(PROPERTY_FILE));
    	// prop.load(new java.io.FileInputStream(PROPERTY_FILE));
    	
    	_srcLocation = prop.getProperty(PropertyKeys.MERGE_SRC_LOCATION);
    	
		_t = Templates.fromProperties(prop);
		
    	//List<String> partitionDirs = new ArrayList<String>();
    	for (String date: dates) {
    		//partitionDirs.clear();
    		String dirLoc = _srcLocation + "/dt=" + date;
    		System.err.println("# listing " + dirLoc);
    		try {
    			printMergeTask(dirLoc);
    		}
    		catch (Exception e) {
    			System.err.println("# file system error: " + e.getMessage());
    		}
    		/*listPartitionDirs(dirLoc, partitionDirs);
    		for (String dir: partitionDirs) {
    			MergeTask task = generateTask(dir);
    			System.out.println(task);
    		}
    		*/
    	}
    	
    }
}
