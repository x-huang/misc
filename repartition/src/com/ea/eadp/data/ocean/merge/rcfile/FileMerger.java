package com.ea.eadp.data.ocean.merge.rcfile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;

import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public class FileMerger
{
	final RCFileContext _ctxt;
	
	public FileMerger(int colSize, int initCapacity) 
	{
		_ctxt = new RCFileContext(colSize, initCapacity);
		System.err.println("[INFO] rcfile context: cols=" + colSize + ", capacity=" + initCapacity);
	}
	
	public void merge(String dir, String dest) 
			throws IOException, URISyntaxException 
	{
		final List<String> srcs;
		if (!dir.endsWith("/")) {
			dir += "/";
		}
		srcs = new ArrayList<>(); ;
		for (String file: HadoopCommon.listFiles(new URI(dir))) {
			srcs.add(dir + file);
		}
		final URI destUri = new URI(dest);
		
		System.err.println("[INFO] start merging " + srcs.size() + " files into one");
		final long startTime = System.currentTimeMillis();
		
		RCFileWritableOutput out = new RCFileWritableOutput(destUri, _ctxt);
		for (String src: srcs) {
			final URI srcUri = new URI(src);
			RCFileWritableInput in = new RCFileWritableInput(srcUri, _ctxt);
			while (true) {
				BytesRefArrayWritable cols = in.read();
				if (cols == null)
					break;
				out.write(cols);
			}
			in.close();
			System.err.println("[INFO] " + srcUri + ": read " + in.getCount() + " rows");
			System.err.println("[INFO] " + destUri + ": accumulatively wrote " + out.getCount() + " rows");
		}
		out.close();
		System.err.println("[INFO] " + destUri + ": successfully closed");
		
		final long endTime = System.currentTimeMillis();
		System.err.println("[DONE] in " + (endTime - startTime) / 1000 + " seconds");
	}
	
	public static void main(String[] args) 
			throws IOException, URISyntaxException 
	{
		if (args.length != 4) {
			System.err.println("[usage] java FileMerger <src-dir> <dest-file> <col-size> <init-capacity>");
			return;
		}
		int colSize = Integer.parseInt(args[2]);
		int initCapacity = Integer.parseInt(args[3]);
		FileMerger merger = new FileMerger(colSize, initCapacity); 
		merger.merge(args[0], args[1]);
	}
}
