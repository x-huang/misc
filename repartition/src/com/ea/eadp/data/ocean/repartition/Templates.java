package com.ea.eadp.data.ocean.repartition;

import java.util.Properties;

import com.ea.eadp.data.common.text.Template;

public class Templates
{
	public final Template src;
	public final Template dest;
	public final Template ddl;
	public final Template prologue;
	public final Template stmt;
	public final Template done;
	
	protected Templates(String srcT, String destT, 
						String ddlT, String prologueT, String stmtT,
						String ddlDoneT)
	{
		src = new Template(srcT, "/", true);
		dest = new Template(destT, "/", false);
		ddl = new Template(ddlT, "/", false);
		prologue = new Template(prologueT, "\n", false);
		stmt = new Template(stmtT, "\n", false);
		done = new Template(ddlDoneT, "\n", false);
	}
	
	public static Templates fromProperties(Properties prop)
	{
		String srcT = prop.getProperty(PropertyKeys.REPARTITION_SRC_TEMPLATE);
		String destT = prop.getProperty(PropertyKeys.REPARTITION_DEST_TEMPLATE);
		String ddlT = prop.getProperty(PropertyKeys.REPARTITION_DDL_TEMPLATE);
		String prologueT = prop.getProperty(PropertyKeys.REPARTITION_PROLOGUE_TEMPLATE);
		String stmtT = prop.getProperty(PropertyKeys.REPARTITION_STATEMENT_TEMPLATE);
		String ddlDoneT = prop.getProperty(PropertyKeys.REPARTITION_DDL_DONE_TEMPLATE);
		return new Templates(srcT, destT, ddlT, prologueT, stmtT, ddlDoneT);
	}
}
