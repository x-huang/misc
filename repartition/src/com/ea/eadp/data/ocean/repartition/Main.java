package com.ea.eadp.data.ocean.repartition;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public final class Main
{
	public static final String PROPERTY_FILE = "repartition.properties";
	
    protected static void listPartitionDirs(String path, List<String> partitionDirs) 
    		throws IOException, URISyntaxException 
    { 	
    	if (!path.endsWith("/"))
    		path += "/";
    		
    	List<String> entries = HadoopCommon.listEntries(new URI(path));
    	
    	int dirCount = 0;
    	for (String entry: entries) {
    		if (entry.endsWith("/")) {
    			listPartitionDirs(path + entry, partitionDirs);
    			dirCount++;
    		}
    	}
    	
    	if (dirCount == 0) {
    		partitionDirs.add(path);
    	}
    }
    
    public static void main(String[] args) throws Exception
    {	
    	boolean ddlonly = false;
    	boolean dryrun = false; 
    	String srcLocation = null;
    	
    	for (String arg: args) {
    		if (arg.equals("-ddl-only"))
    			ddlonly = true;
    		else if (arg.equals("-dry-run"))
    			dryrun = true;
    		else if (srcLocation == null) 
    			srcLocation = arg;
    		else {
    			System.err.println("usage: java Main [-ddl-only] [-dry-run] <source-location>");
        		return;
    		}
    	}
    	
    	if (srcLocation == null) {
    		System.err.println("usage: java Main [-ddl-only] [-dry-run] <source-location>");
    		return;
    	}
    	
    	final Properties prop = new Properties();
    	prop.load(Main.class.getClassLoader().getResourceAsStream(PROPERTY_FILE));
    	// prop.load(new java.io.FileInputStream(PROPERTY_FILE));
    	
    	List<String> partitionDirs = new ArrayList<String>();
    	listPartitionDirs(srcLocation, partitionDirs);
    	
    	Repartitioner repartitioner = new Repartitioner(prop);
    	if (ddlonly)
    		repartitioner.setDdlOnly();
    	if (dryrun)
    		repartitioner.setDryRun();
    	repartitioner.run(partitionDirs);
    }
}
