package com.ea.eadp.data.ocean.repartition;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ea.eadp.data.common.text.Binding;
// import com.ea.eadp.data.common.text.PartitionSchema;
import com.ea.eadp.data.stream.DataStreamMaker;

public class DdlSerializer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DdlSerializer.class);
	
	static class DdlSerializeContent {
		final StringBuilder _sb;
		final String _touch;
		
		public DdlSerializeContent(String touchPath) {
			_touch = touchPath;
			_sb = new StringBuilder();
		}
	}
	
	protected final Templates _t;
	protected final Binding _context;
	private final Map<String, DdlSerializeContent> _ddls;
	private boolean _dryRun = false;
	
	public DdlSerializer(Templates templates, Binding context) // , PartitionSchema destSchema) 
						 throws IOException, URISyntaxException
	{
		_t = templates;
		_context = new Binding();
		_context.putAll(context);
		_ddls = new HashMap<String, DdlSerializeContent>();
	}
	
	public void setDryRun()
	{
		_dryRun = true;
	}
	
	public void readBinding(Binding binding)
	{
		final Binding combined = _context.combineWith(binding);
		final String ddlPath = _t.ddl.instantiate(combined);
		final String touchPath = _t.done.instantiate(combined);
		final DdlSerializeContent serialize;
		
		if (_ddls.containsKey(ddlPath)) {
			serialize = _ddls.get(ddlPath);
		}
		else {
			serialize = new DdlSerializeContent(touchPath);
			_ddls.put(ddlPath, serialize);
			LOGGER.info("adding new ddl path: " + ddlPath);
		}
		
		serialize._sb.append(_t.stmt.instantiate(combined));
		serialize._sb.append("\n\n");
	}
	
	public void serialize() throws IOException, URISyntaxException
	{
		for (Map.Entry<String, DdlSerializeContent> entry: _ddls.entrySet()) {
			String ddlPath = entry.getKey();
			if (_dryRun) {
				LOGGER.info("[DRYRUN] serializing ddl to: " + ddlPath);
				continue;
			}
			
			LOGGER.info("serializing ddl to: " + ddlPath);
			final StringBuilder sb = entry.getValue()._sb; 
			OutputStream os = DataStreamMaker.createOutputStream(ddlPath  + "?overwrite=ok");
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write(_t.prologue.instantiate(_context));
			bw.write("\n\n");
			bw.write(sb.toString());
			bw.close();
			
			final String touchPath = entry.getValue()._touch;
			LOGGER.info("creating done file at: " + touchPath);
			os = DataStreamMaker.createOutputStream(touchPath  + "?overwrite=ok");
			PrintWriter pw = new PrintWriter(os);
			pw.print(ddlPath);
			pw.close();
		}
	}
	
}
