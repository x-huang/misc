/*
$Id: //DATA/Ocean/BackupRestore/dev_branches/hchen_dev_1.0/BackupRestore/src/main/java/com/ea/eadp/data/ocean/backuprestore/utils/DataFileEntry.java#1 $
$DateTime: 2013/06/11 18:36:44 $
$Change: 772049 $
$Author: EAHQ\hachen $
 */

/**
 * (c) 2002-2013 Electronic Arts Inc. All rights reserved.
 * 
 */
package com.ea.eadp.data.ocean.repartition;

public final class Task 
{
    final public String source;
    final public String destination;
    final public String connector;

    public Task(String src, String dest, String conn) {
        source = src;
        destination = dest;
        connector = conn;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        return "[ " + source + " => " + destination + " | " + connector + " ]";
    }

}
