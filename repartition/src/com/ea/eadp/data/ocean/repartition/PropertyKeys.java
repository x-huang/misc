package com.ea.eadp.data.ocean.repartition;

public class PropertyKeys
{
    public static final String REPARTITION_SRC_TEMPLATE = "repartition.src.template";
    
    public static final String REPARTITION_DEST_TEMPLATE = "repartition.dest.template";
    
    public static final String REPARTITION_DDL_TEMPLATE = "repartition.ddl.template";
    
    public static final String REPARTITION_DDL_DONE_TEMPLATE = "repartition.ddl.done.template";
    
    public static final String REPARTITION_PROLOGUE_TEMPLATE = "repartition.ddl.prologue";
    
    public static final String REPARTITION_STATEMENT_TEMPLATE = "repartition.ddl.statement";
      
    public static final String REPARTITION_CUSTOM_BINDING = "repartition.custom.binding";
    
    public static final String TRANSPORT_THREADS = "repartition.transport.threads";
    
    public static final String TRANSPORT_QUEUE_SIZE = "repartition.transport.queue.size";
    
    public static final String TRANSPORT_RETRIES = "repartition.transport.retries";

    public static final String S3N_ACCESS_KEY_ID = "fs.s3n.awsAccessKeyId";
    
    public static final String S3N_SECRET_KEY = "fs.s3n.awsSecretAccessKey";
}
