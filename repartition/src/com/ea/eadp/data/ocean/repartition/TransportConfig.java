package com.ea.eadp.data.ocean.repartition;

import java.util.Properties;

public class TransportConfig
{
	final public int nthreads;
	final public int queueSize;
	final public int keepAlive;
	final public int retries;	// 0 or 1, never greater than 1
	
	protected TransportConfig(int nt, int qs, int ka, int r)
	{
		nthreads = nt;
		queueSize = qs;
		keepAlive = ka;
		retries = r;
	}
	
	public static TransportConfig fromProperties(Properties prop)
	{
		int nt = 5;
		int qs = 50000;
		int ka = 600;
		int r = 0;
		
		String threads = prop.getProperty(PropertyKeys.TRANSPORT_THREADS);
		if (threads != null)
			nt = Integer.parseInt(threads);
		
		String qsize = prop.getProperty(PropertyKeys.TRANSPORT_QUEUE_SIZE);
		if (qsize != null)
			qs = Integer.parseInt(qsize);
		
		String retry = prop.getProperty(PropertyKeys.TRANSPORT_RETRIES);
		if (retry != null && retry.equalsIgnoreCase("yes")) {
			r = 1;
		}
		
		return new TransportConfig(nt, qs, ka, r);
	}
	
}
