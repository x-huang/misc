package com.ea.eadp.data.ocean.repartition;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ea.eadp.data.common.text.Binding;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;
import com.ea.eadp.data.transport.ErrorHandlingCallback;
import com.ea.eadp.data.transport.PostExecutionCallback;
import com.ea.eadp.data.transport.PreExecutionCallback;
import com.ea.eadp.data.transport.TransportService;
import com.ea.eadp.data.transport.TransportTask;


/**
 * @author xhuang
 */
public class Repartitioner
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Repartitioner.class);
	
	final public static String TRANSPORT_CONNECTOR = "default";
	final public static String VAR_FILE = "file";
	final public static String VAR_SEQNO = "seqno";
	final public static String VAR_TIMESTAMP = "timestamp";
	
	final private Templates _t;
	final private TransportConfig _tc;
	final private Binding _contextBinding;
	final private DdlSerializer _ddl;
	
	private boolean _ddlOnly = false;
	private boolean _dryRun = false;
	
	transient final private List<Task> _succeeded = new ArrayList<Task>();
	transient final private List<Task> _failed = new ArrayList<Task>(); 
	transient private AtomicLong _nbytes = new AtomicLong(0);
	
	
	@SuppressWarnings("deprecation")
	public Repartitioner(Properties prop)
	{
		{
			String s3nAccessKeyId = prop.getProperty(PropertyKeys.S3N_ACCESS_KEY_ID);
			String s3nSecretKey = prop.getProperty(PropertyKeys.S3N_SECRET_KEY);
			if (s3nAccessKeyId != null && s3nSecretKey != null) {
				HadoopCommon.setCustomizedConfigurationParam(PropertyKeys.S3N_ACCESS_KEY_ID, s3nAccessKeyId);
				HadoopCommon.setCustomizedConfigurationParam(PropertyKeys.S3N_SECRET_KEY, s3nSecretKey);
				LOGGER.info("set non-default s3n secret key");
			}
		}
	
		_t = Templates.fromProperties(prop);
		_contextBinding = new Binding();
		_contextBinding.importFromString(prop.getProperty(PropertyKeys.REPARTITION_CUSTOM_BINDING));
		
		_tc = TransportConfig.fromProperties(prop);
		
		try {
			_ddl = new DdlSerializer(_t, _contextBinding);
		}
		catch (IOException | URISyntaxException e) {
			LOGGER.error("cannot initialize DDL Serializer due to: " + e.getMessage());
			throw new RuntimeException("cannot initialize DDL Serializer due to: " + e.getMessage(), e);
		}
		
		LOGGER.info("initializing repartitioner: " 
					+ "src template: " + _t.src
					+ ", dest template: " + _t.dest
					+ ", ddl template: " + _t.ddl
					+ ", context binding: " + _contextBinding); 

		LOGGER.info("using transport service parameters: "
					+ "threads: " + _tc.nthreads 
					+ ", queueSize: " + _tc.queueSize
					+ ", retries: " + _tc.retries);
	}
	
	public void setDdlOnly() 
	{
		_ddlOnly = true;
	}
	
	public void setDryRun()
	{
		_dryRun = true;
		_ddl.setDryRun();
	}

	private static URI verifyUri(String rawUri)
	{
		try {
			URI uri = new URI(rawUri);
			if (uri.getScheme() == null) 
				uri = new URI("hdfs://" + rawUri);
			return uri;
		}
		catch (URISyntaxException e) {
			throw new IllegalArgumentException("unrecognized uri: " + rawUri);
		}
	}
		
	private List<Task> expandTaskList(String path) 
			throws IOException
	{
		List<Task> tasks = new ArrayList<Task>();
		final URI srcUri = verifyUri(path);
		
		if (! HadoopCommon.getFileStatusRepr(srcUri).isDir) {
			Binding binding = _t.src.extract(path);
			binding.put(VAR_TIMESTAMP, "" + System.currentTimeMillis());
			String destPath = _t.dest.instantiate(binding);
			tasks.add(new Task(path, destPath, TRANSPORT_CONNECTOR));
			return tasks;
		}
		
		int seqno = 0;
		
		for (String srcFile: HadoopCommon.listFiles(srcUri)) {
			final String srcPath = srcUri + srcFile;
			Binding binding = _contextBinding.combineWith(_t.src.extract(srcPath));
			binding.put(VAR_FILE, srcFile);
			binding.put(VAR_SEQNO, String.format("%03d", seqno));
			binding.put(VAR_TIMESTAMP, "" + System.currentTimeMillis());

			_ddl.readBinding(binding);
			
			String destPath  = _t.dest.instantiate(binding);
			tasks.add(new Task(srcPath, destPath, TRANSPORT_CONNECTOR));
			seqno++;
		}
		
		return tasks;
	}
	 
	public void run(List<String> list)
	{
		final List<Task> tasks = new ArrayList<Task>();
		
		for (String path: list) {
			try {
				tasks.addAll(expandTaskList(path)); 
			}
			catch (IOException e) {
				LOGGER.error("cannot expand tasks: " + e.getMessage());
				throw new RuntimeException("failed expanding tasks", e);
			}
		}
		
		final TransportService service;
		try {
			int qsize =  _tc.queueSize;
			if (tasks.size() > qsize) {
				qsize = tasks.size();
				LOGGER.warn("change transport service queue size to be " + qsize);
			}
			service = new TransportService(_tc.nthreads, qsize, _tc.keepAlive);
			// service.enableJmxReport();
			if (_tc.retries > 0) 
				service.setRetrier(1); // retry only once
			TransportTaskCallback cb = new TransportTaskCallback();
			service.addPreAction(cb).addPostAction(cb).addErrorHandler(cb);
		}
		catch (IOException e) {
			LOGGER.error("cannot initialize transport service: " + e.getMessage());
			throw new RuntimeException("failed initializing transport service", e);
		}
		
		final long startTime = System.currentTimeMillis(); 
		
		for (Task task: tasks) {
			String src = task.source;
			String dest = task.destination + "?overwrite=yes";
			String connector = task.connector;
			
			if (_ddlOnly || _dryRun) {
				LOGGER.info("[DRYRUN] " + task);
				_succeeded.add(task);
				continue;
			}
				
			LOGGER.info("[SUBMIT] " + task);
			try {
				service.submitTask(src, dest, connector, task);
			}
			catch (Exception e) {
				LOGGER.error("error submitting transport task: " + e.getMessage());
				throw new RuntimeException("failed submitting transport task", e);
			}
		}
		
		service.shutdown();
		LOGGER.info("transport service terminated");
		
		final long endTime = System.currentTimeMillis();
		final long seconds = (endTime - startTime)/1000;
		
		if (_succeeded.size() == tasks.size()) {
			LOGGER.info("[SUCCEEDED] all transport tasks finished successfully (" 
						+ tasks.size() + "/" + tasks.size() + " files, " 
						+ _nbytes.get() + " bytes, "
						+ seconds + " seconds)");
			LOGGER.info("generating DDL statements...");
			try {
				_ddl.serialize();
			}
			catch (IOException | URISyntaxException e) {
				LOGGER.error("cannot generate ddl statements due to: " + e.getMessage());
				cleanUp();
				throw new RuntimeException("failed generatinig ddl statements", e);
			}
			LOGGER.info("[SUCCEEDED] DDL statements generated successfully");
			return;
		}
		else {
			LOGGER.error("[FAILED] transport tasks finished unsuccessfully (" 
						+ _succeeded.size() + "/" + tasks.size() + " files, " 
						+ seconds + " seconds)");
			cleanUp();
			throw new RuntimeException("failed transferring files");
		}
	}
	
	private void cleanUp()
	{
		LOGGER.info("undo all succeeded transfers");
		for (Task task: _succeeded) {
			String dest = task.getDestination();
			LOGGER.info("deleting " + dest);
			try {
				HadoopCommon.delete(new URI(dest));
			}
			catch (IOException | URISyntaxException e) {
				LOGGER.warn("cannot clean up due to: " + e.getMessage());
			}
		}
	}
	
	class TransportTaskCallback 
			implements PreExecutionCallback, PostExecutionCallback, ErrorHandlingCallback
	{
		final private Object _succeededListLock = new Integer(0);
		final private Object _failedListLock = new Integer(0);
		
		@Override
		public void handleError(TransportTask task, int retried,
								Map<String, Object> stat, Throwable t)
		{
			if (retried + 1 >= _tc.retries) {
				LOGGER.error("[FAIL] " + task + " failed maximun times due to: " + t.getMessage());
				synchronized (_failedListLock) {
					_failed.add((Task) task.getAdditionalInfo());
				}
			}
		}
		
		@Override
		public void doPostAction(TransportTask task, int retried,
								 Map<String, Object> stat)
		{
			long nbytes = (Long)stat.get("bytes");
			long seconds = (Long)stat.get("timeUsage");
			LOGGER.info("[DONE] " + task + " (" + nbytes + " bytes, " + seconds + " seconds)");
			synchronized (_succeededListLock) {
				_succeeded.add((Task) task.getAdditionalInfo());
			}
			_nbytes.addAndGet(nbytes);
		}

		@Override
		public void doPreAction(TransportTask task, long threadId)
		{
			LOGGER.info("[START] " + task);
		}
	}
}
