package com.ea.eadp.data.common.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ea.eadp.data.common.lang.StringUtils;

public class Template
{
	abstract class Term {
		abstract public String readValue(Map<String, String> context);
	}
	
	class Literal extends Term {
		final protected String _s; 
		protected Literal(String s) { _s = s; } 
		@Override 
		public String toString() { return _s; }
		@Override
		public String readValue(Map<String, String> context) { return _s; }
	}
	
	class Variable extends Term {
		final protected String _var;
		final protected Pattern _p;
		protected Variable(String v, String regex) { 
			_var = v;
			_p = (regex == null)? null: Pattern.compile(regex);
		}
		@Override
		public String readValue(Map<String, String> binding) {
			String val = binding.get(_var);
			if (val == null) {
				throw new IllegalArgumentException("variable " + _var + " not found in binding");
			}
			return val;
		}
		@Override 
		public String toString() { 
			if (_p == null) 
				return "$" + _var;
			else
				return "${" + _var + ":" + _p.toString() + "}";
		}
	}
	
	static class Lexer {
		
		static public List<String> parse(String s) {
			List<String> tokens = new ArrayList<String>();
			Pattern var = Pattern.compile("(\\$\\w+)|(\\$\\{\\w+\\})|(\\$\\{\\w+:[^\\{\\}/:]+\\})");
			Matcher matcher = var.matcher(s);
			
			int start = 0; 
			int end = 0;		
			while (matcher.find()) {
				start = matcher.start();
				if (start > end) {
					tokens.add(s.substring(end, start));
				}
				
				end = matcher.end();
				tokens.add(s.substring(start, end));
			}
			
			if (s.length() > end) {
				tokens.add(s.substring(end));
			}
			return tokens;
		}

		static public boolean isVariable(String s) {
			return s.startsWith("$");
		}
		
		static public String[] getVariableDef(String s) {
			if (!s.startsWith("$"))
				return null;
			String var = s.substring(1);
			if (var.startsWith("{")) {
				var = var.substring(1, var.length() - 1);
			}
			if (var.contains(":")) {
				return var.split(":", 2);
			}
			else {
				return new String[] { var, null };
			}
		}
	}
	
	class Segment {
		final protected List<Term> _terms;
		
		public Segment(String segment) {
			List<String> tokens = Lexer.parse(segment);
			_terms = new ArrayList<Term>(tokens.size());
			for (String tok: tokens) {
				if (Lexer.isVariable(tok)) {
					String[] vardef = Lexer.getVariableDef(tok);
					String var = vardef[0];
					_terms.add(new Variable(var, vardef[1]));
				}
				else {
					_terms.add(new Literal(tok));
				}
			}
		}
		
		protected void verifyVariables()
		{
			// in a segment, no wildcard variables (_p==null) except the last one
			boolean lastVar = true;
			for (int i=_terms.size()-1; i>=0; i--) {
				Term term = _terms.get(i);
				if (term instanceof Variable) {
					if (lastVar) {
						lastVar = false;
					}
					else {
						if (((Variable) term)._p == null) { 
							throw new IllegalArgumentException(
									"no wildcard variables except the last one: " + term.toString());
						}
					}
				}
			}
		}
			
		protected String instantiate(Map<String, String> binding) {
			StringBuffer sb = new StringBuffer();
			for (Term term: _terms) {
				sb.append(term.readValue(binding));
			}
			return sb.toString();
		}
		
		protected void extractTo(String s, Map<String, String> binding) {
			int idx = 0;
			String t = s;
			for (Term term: _terms) {
				if (term instanceof Literal) {
					String lit = ((Literal) term)._s;
					if (! t.startsWith(lit)) { 
						throw new IllegalArgumentException(
								"unexpected literal token in: " + s + ", index: " + idx);
					}
					t = t.substring(lit.length());
					idx += lit.length();
				}
				else if (term instanceof Variable) {
					final String var = ((Variable) term)._var;
					final Pattern p = ((Variable) term)._p;
					final String val;
					if (p == null) {
						val = t;
						idx += t.length();
						t = "";
					}
					else {
						Matcher matcher = p.matcher(t);
						if (!matcher.find() || matcher.start() != 0) {
							throw new IllegalArgumentException(
									"unexpected variable token in: " + s + ", index: " + idx);
						}
						int end = matcher.end();
						val = t.substring(0, end);
						idx += end;
						t = t.substring(end);
					}
					if (binding.containsKey(var)) {
						if (!binding.get(var).equals(val)) {
							throw new IllegalArgumentException(
											"variable " + var + " has multiple definitions: " 
											+ binding.get(var) + " vs. " + val);
						}
					}
					else {
						binding.put(var, val);
					}
				}
			}
			if (! t.isEmpty()) {
				throw new IllegalArgumentException(
						"unexpected residual in: " + s + ", index: " + idx);
			}
		}
		
		@Override
		public String toString() {
			List<String> terms = new ArrayList<String>();
			for (Term term: _terms) {
				terms.add(term.toString());
			}
			return StringUtils.join(terms, "");
		}

	}
	
	final protected List<Segment> _segs;
	final protected String _delimiter;
	
	public Template(String s, String delimiter, boolean toExtract) {
		_segs = new ArrayList<Segment>();
		_delimiter = delimiter;
		for (String segment: s.split(delimiter)) {
			_segs.add(new Segment(segment));
		}
		
		if (toExtract) {
			for (Segment seg: _segs) {
				seg.verifyVariables();
			}
		}
	}
	
	public String getDelimiter() {
		return _delimiter;
	}
	
	@Override
	public String toString() {
		List<String> segs = new ArrayList<String>();
		for (Segment seg: _segs) {
			segs.add(seg.toString());
		}
		return StringUtils.join(segs, _delimiter);
	}
	
	public Binding extract(String path) {
		final String[] splitted = path.split(_delimiter);
		if (splitted.length < _segs.size()) {
			throw new IllegalArgumentException(
					"path contains less segments than template: " + path);
		}
		final int start = splitted.length - _segs.size();
		Binding binding = new Binding();
		for (int i=0; i<_segs.size(); i++) {
			_segs.get(i).extractTo(splitted[i+start], binding);
		}
		if (start > 0) {
			String[] prefix = Arrays.copyOf(splitted, start);
			binding.put("_prefix", StringUtils.join(prefix, _delimiter));
		}
		return binding;
	}
	
	public String instantiate(Binding binding) {
		List<String> segs = new ArrayList<String>(_segs.size());
		for (Segment seg: _segs) {
			segs.add(seg.instantiate(binding));
		}
		return StringUtils.join(segs, _delimiter);
	}
}
 