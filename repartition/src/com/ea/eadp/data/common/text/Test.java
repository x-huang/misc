package com.ea.eadp.data.common.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Test
{
	public static void main(String[] args) throws IOException {
		
		if (args.length != 3) {
			System.err.println("usage:");
			System.err.println("	java com.ea.eadp.data.common.text.Test <src-template> <dest-template> <src-path>");
			System.err.println("example:");
			InputStream in = Test.class.getResourceAsStream("example.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String inputLine;
			while ((inputLine = br.readLine()) != null)
			    System.err.println(inputLine);
			br.close();
			return;
		}
		
		Template srcT = new Template(args[0], "/", true);
		System.out.println("src template: " + srcT); 
	
		Template destT = new Template(args[1], "/", false);
		System.out.println("dest template: " + destT);
		
		System.out.println("extracting path: " + args[2]);
		
		Binding binding = srcT.extract(args[2]);
		System.out.println("extracted binding: " + binding);

		String dest = destT.instantiate(binding);
		System.out.println("instantiated destination: " + dest);
	}
}

