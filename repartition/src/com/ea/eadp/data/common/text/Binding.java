package com.ea.eadp.data.common.text;

import java.util.HashMap;
import java.util.Map;

import com.ea.eadp.data.common.lang.StringUtils;

public class Binding extends HashMap <String, String> 
{
	private static final long serialVersionUID = -1070312045917338565L;

	public void importFromString(String s) 
	{
		String[] splitted = s.split("[,]+");
		for (String t: splitted) {
			String[] kv = t.split("=", 2);
			if (kv.length != 2) 
				throw new IllegalArgumentException("not a 'key=value' pair: " + t);
			this.put(kv[0], kv[1]);
		}
	}
	
	public Binding combineWith(Binding that)
	{
		Binding combined = new Binding();
		combined.putAll(this);
		for (Map.Entry<String, String> entry: that.entrySet()) {
			String key = entry.getKey();
			String val = entry.getValue();
			if (combined.containsKey(key)) {
				if (!combined.get(key).equals(val)) {
					throw new IllegalArgumentException(
							"cannot combine two bindings: conflict at key " 
							+ key + ": " + combined.get(key) + " vs. " + val);
				}
			}
			else {
				combined.put(key, val);
			}
		}
		combined.putAll(that);
		return combined;
	}
	
	@Override
	public String toString()
	{
		return StringUtils.join(StringUtils.zip(this, "="), ",");
	}
}

