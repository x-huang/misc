package com.ea.eadp.data.amq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

public class MessageLogger implements MessageHandler
{
	@Override
	public Status handle(Message msg)
	{
		String text;
		try {
			text = ((TextMessage) msg).getText();
		}
		catch (JMSException e) {
			e.printStackTrace();
			return Status.ERROR;
		}
		if (text.equals("<EOF>")) {
			System.err.println("<EOF> received");
			return Status.OVER;
		}
		System.out.println(text);
		return Status.DONE;
	}

}
