package com.ea.eadp.data.amq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;

import com.ea.eadp.data.jms.client.AMQClientSpec;
import com.ea.eadp.data.jms.client.AMQSession;

public class ReadMQ implements Runnable
{
	final private MessageHandler _handler;
	
	final private AMQClientSpec _spec;
	final private AMQSession _session;
	final private MessageConsumer _consumer;
	
	public ReadMQ(String serverUri, String destName, MessageHandler h) throws JMSException 
	{
		_spec = new AMQClientSpec(AMQClientSpec.DestinationType.QUEUE, destName);
		_spec.setPrefetchSize(1);
		_spec.setDispatchAsync(true);
		_session = new AMQSession(serverUri);
		_consumer = _session.createQueueConsumer(_spec);
		_handler = h;
	}

	@Override
	public void run()
	{
		int count = 0;
		while (true) {
			try {
				Message msg = _consumer.receive();
				MessageHandler.Status status = _handler.handle(msg);
				if (status == MessageHandler.Status.OVER) {
					break;
				}
				count++;
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.err.println("[done] handled " + count + " messages");
		try {
			_consumer.close();
			_session.close();
		}
		catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws JMSException
	{
		if (args.length != 2) {
			System.err.println("[usage] java Main <server-uri> <dest-name>");
			return;
		}
		
		final String serverUri = args[0];
		final String destName = args[1];
		
		ReadMQ msgReader = new ReadMQ(serverUri, destName, new MessageLogger());
		msgReader.run();
	}
}
