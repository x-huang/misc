package com.ea.eadp.data.amq;

import javax.jms.Message;

public interface MessageHandler
{
	public static enum Status { DONE, WARN, ERROR, OVER };
	public Status handle(Message msg);
}
