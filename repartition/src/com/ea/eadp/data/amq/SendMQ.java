package com.ea.eadp.data.amq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

import com.ea.eadp.data.jms.client.AMQClientSpec;
import com.ea.eadp.data.jms.client.AMQSession;

public class SendMQ
{
	public static void main(String[] args) 
			throws IOException, JMSException 
	{
		if (args.length != 2) {
			System.err.println("[usage] java Main <server-uri> <dest-name>");
			return;
		}
		
		final String serverUri = args[0];
		final String destName = args[1];
		
		final AMQClientSpec spec = new AMQClientSpec(AMQClientSpec.DestinationType.QUEUE, destName);
		final AMQSession session = new AMQSession(serverUri);;
		final MessageProducer producer = session.createProducer(spec);
			
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int count = 0;
		while (true) {
			final String line = br.readLine();
			if (line == null)
				break;
			if (line.startsWith("#"))
				continue;
			TextMessage message = session.createTextMessage(line);
			producer.send(message);
			count++;
		}
		
		producer.close();
		session.close();
		System.err.println("[done] " + count + " messages sent");
	}
}
