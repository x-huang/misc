package xw.data.hivelite;

import org.apache.hive.service.cli.HiveSQLException;
import org.apache.hive.service.cli.OperationState;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by xhuang on 7/28/16.
 */
public class QueryStatusPolling
{
    final static private Logger logger = LoggerFactory.getLogger(QueryStatusPolling.class);

    static class QueryMonitor {
        static final AtomicInteger count = new AtomicInteger(0);
        final long id;
        OperationState state;
        final HiveLiteQuery query;
        final StatusChangeCallback callback;
        final String desc;

        QueryMonitor(HiveLiteQuery query, StatusChangeCallback callback) throws HiveSQLException {
            this.id = count.incrementAndGet();
            this.state = OperationState.UNKNOWN;
            this.query = query;
            this.callback = callback;
            String stmtBrief = query.stmt.substring(0, 20) + "...";
            this.desc = "{query #" + id + "| " + stmtBrief + "}";
        }

        @Override
        public String toString() {
            return desc;
        }
    }

    private final Set<QueryMonitor> monitors = new ConcurrentHashSet<>();
    private long interval = 5_000L;
    private Timer timer = null;

    public QueryStatusPolling setInterval(long ms) {
        this.interval = ms;
        return this;
    }

    public int size() {
        return monitors.size();
    }

    public QueryStatusPolling monitorQuery(HiveLiteQuery query, StatusChangeCallback callback) throws HiveSQLException {
        final QueryMonitor monitor = new QueryMonitor(query, callback);
        monitors.add(monitor);
        logger.debug("start monitoring query: {}", monitor);
        return this;
    }

    public void scanStatusChange() {
        for (QueryMonitor monitor: monitors) {
            OperationState newState;
            try {
                newState = monitor.query.getState();
            } catch (HiveSQLException e) {
                logger.error("cannot get query status: {}", e.getMessage(), e);
                continue;
            }

            if (monitor.state == newState) {
                continue;
            }

            logger.debug("{} status change: {} -> {}", monitor, monitor.state, newState);
            try {
                monitor.callback.onStatusChange(monitor.query, monitor.state, newState);
            } catch (Exception e) {
                logger.error("error occurred in query #{} status change: {} -> {}",
                        monitor.id, monitor.state, newState, e);
            }
            if (newState.isTerminal()) {
                monitors.remove(monitor);
                logger.debug("removed {} of state {} from monitoring poll", monitor, newState);
            } else {
                monitor.state = newState;
            }
        }
    }

    public synchronized void start() {
        if (timer == null) {
            timer = new Timer(true);
            timer.schedule(new ScanStatusTask(), interval, interval);
        }
    }

    public synchronized void stop() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private class ScanStatusTask extends TimerTask {
        @Override
        public void run() {
            scanStatusChange();
        }
    }
}
