package xw.data.hivelite;

import org.apache.hive.service.cli.OperationState;

/**
 * Created by xhuang on 7/28/16.
 */
public interface StatusChangeCallback
{
    void onStatusChange(HiveLiteQuery query, OperationState oldState, OperationState newState) throws Exception;
}
