package xw.data.hivelite;


import org.apache.hive.service.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.schema.Schema;
import xw.data.schema.TypeUtils;

import java.util.Iterator;
import java.util.concurrent.Future;

/**
 * Created by xhuang on 7/27/16.
 */
public class HiveLiteQuery implements AutoCloseable
{
    final static private Logger logger = LoggerFactory.getLogger(HiveLiteQuery.class);

    private final HiveLiteSession hivelite;
    public final String stmt;
    private OperationHandle op;
    private transient RowSet rowSet;

    HiveLiteQuery(HiveLiteSession hivelite, String stmt, boolean async) throws HiveSQLException {
        this.hivelite = hivelite;
        this.stmt = stmt;
        if (async) {
            this.op = hivelite.client.executeStatementAsync(hivelite.session, stmt, hivelite.confOverlay);
            this.rowSet = null;
        } else {
            this.op = hivelite.client.executeStatement(hivelite.session, stmt, hivelite.confOverlay);
            this.rowSet = hivelite.client.fetchResults(op);
        }
        hivelite.opCount.getAndIncrement();
    }

    @Override
    public void close() throws HiveSQLException {
        if (op == null) {
            logger.warn("query already closed");
            return;
        }
        hivelite.client.closeOperation(op);
        logger.debug("hive query operation closed");
        hivelite.opCount.decrementAndGet();
        op = null;
        rowSet = null;
    }

    public String getStateName() throws HiveSQLException {
        return hivelite.client.getOperationStatus(op).getState().name();
    }

    protected OperationState getState() throws HiveSQLException {
        return hivelite.client.getOperationStatus(op).getState();
    }

    public Future<?> getResultFuture() throws HiveSQLException {
        return hivelite.client.getSessionManager().getOperationManager().getOperation(op).getBackgroundHandle();
    }

    public boolean finished() throws HiveSQLException {
        return hivelite.client.getOperationStatus(op).getState() == OperationState.FINISHED;
    }

    private void checkStatusFinished() throws HiveSQLException {
        if (op == null) {
            throw new IllegalStateException("query already closed");
        }
        final OperationState state = hivelite.client.getOperationStatus(op).getState();
        if (state != OperationState.FINISHED) {
            throw new IllegalStateException("query not yet finished, current state is: " + state.name());
        }
        if (rowSet == null) {
            rowSet = hivelite.client.fetchResults(op);
        }
    }

    public int numRows() throws HiveSQLException {
        checkStatusFinished();
        return rowSet.numRows();
    }

    public int numColumns() throws HiveSQLException {
        checkStatusFinished();
        return rowSet.numColumns();
    }

    public Schema getSchema() throws HiveSQLException {
        checkStatusFinished();
        final Schema.Builder builder = Schema.newBuilder();
        final TableSchema metadata = hivelite.client.getResultSetMetadata(op);
        for (ColumnDescriptor col: metadata.getColumnDescriptors()) {
            final String s = col.getName();
            builder.addColumn(s.substring(s.lastIndexOf('.') + 1),
                    TypeUtils.parse(col.getTypeName().toLowerCase()));
        }
        return builder.build();
    }

    public Iterator<Object[]> getRowIterator() throws HiveSQLException {
        checkStatusFinished();
        return rowSet.iterator();
    }
}
