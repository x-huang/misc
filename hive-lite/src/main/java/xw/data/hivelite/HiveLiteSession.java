package xw.data.hivelite;

import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hive.service.cli.CLIService;
import org.apache.hive.service.cli.OperationHandle;
import org.apache.hive.service.cli.SessionHandle;
import org.apache.hive.service.cli.HiveSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by xhuang on 7/25/16.
 */
public class HiveLiteSession implements AutoCloseable
{
    final static private Logger logger = LoggerFactory.getLogger(HiveLiteSession.class);

    protected final HiveConf hiveConf;
    protected final CLIService client;
    protected final SessionHandle session;
    protected final Map<String, String> confOverlay = new HashMap<>();
    protected final AtomicInteger opCount = new AtomicInteger(0);

    public HiveLiteSession(String username, String password) throws HiveSQLException {
        hiveConf = new HiveConf();
        client = new CLIService(null);
        client.init(hiveConf);

        session = client.openSession(username, password, new HashMap<String, String>());
        logger.debug("new session id: {}", session.getSessionId());
    }

    public void init() throws HiveSQLException {
        setConf(HiveConf.ConfVars.HIVE_SUPPORT_CONCURRENCY.varname, "false");
    }

    public synchronized void execCmd(String cmd) throws HiveSQLException {
        logger.debug("executing cmd: {}", cmd);
        final OperationHandle op = client.executeStatement(session, cmd, confOverlay);
        client.closeOperation(op);
    }

    public void setConf(String name, String value) throws HiveSQLException {
        final String cmd = "SET " + name + " = " + value;
        execCmd(cmd);
    }

    public HiveConf getHiveConf() {
        return hiveConf;
    }

    public Map<String, String> getConfOverlay() {
        return confOverlay;
    }

    public synchronized HiveLiteQuery execQuery(String stmt) throws HiveSQLException {
        logger.debug("executing query: {}", stmt);
        return new HiveLiteQuery(this, stmt, false);
    }

    public synchronized HiveLiteQuery execQueryAsync(String stmt) throws HiveSQLException {
        logger.debug("async-executing query: {}", stmt);
        return new HiveLiteQuery(this, stmt, true);
    }

    @Override
    public void close() throws HiveSQLException {
        final int ops = opCount.get();
        if (ops != 0) {
            logger.warn("closing session while {} operation not closed yet", ops);
        }
        client.closeSession(session);
        logger.debug("hive session closed");
    }
}
