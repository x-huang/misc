package xw.data.stream.text;

import java.util.Collections;
import java.util.Map;

/**
 * Created by xhuang on 5/26/16.
 */
public class RecordTextFormatter
{
    private String fieldDelimiter = "\t";
    private String nullString = "\\N";
    private transient final StringBuilder sb = new StringBuilder();

    public RecordTextFormatter setFieldDelimiter(String delimiter)  {
        this.fieldDelimiter = delimiter;
        return this;
    }

    public RecordTextFormatter setNullRepr(String repr) {
        this.nullString = repr;
        return this;
    }

    public String format(Object[] record) {
        sb.setLength(0);
        String delimiter = "";
        for (Object datum : record) {
            sb.append(delimiter).append(datum == null ? nullString : datum.toString());
            delimiter = fieldDelimiter;
        }
        return sb.toString();
    }

    public static RecordTextFormatter create(String format, Map<String, String> params) {
        final RecordTextFormatter formatter = new RecordTextFormatter();
        if (params.containsKey("nullrepr")) {
            formatter.setNullRepr(params.get("nullrepr"));
        }

        switch (format) {
            case "tsv":
            case "text":
            case "txt":
                return formatter;
            case "csv":
                return formatter.setFieldDelimiter(",");
            case "psv":
                return formatter.setFieldDelimiter("|");
            default:
                throw new IllegalArgumentException("bad text format: " + format);
        }
    }


    public static RecordTextFormatter create(String format) {
        return create(format, Collections.EMPTY_MAP);
    }
}
