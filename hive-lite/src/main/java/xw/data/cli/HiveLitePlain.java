package xw.data.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import org.apache.hive.service.cli.*;
import org.apache.hadoop.hive.conf.HiveConf;
import java.util.*;

/**
 * Created by xhuang on 7/25/16.
 */
public class HiveLitePlain
{
    final static private Logger logger = LoggerFactory.getLogger(HiveLitePlain.class);

    static class Option {

        final String stmt;
        final List<String> cmds;
        final String username;
        final String password;
        final String delimiter;

        static String findOptValue(String[] args, String opt) {
            for (int i=0; i<args.length; i++) {
                if (args[i].equals(opt)) {
                    if (i < args.length-1) {
                        return args[i+1];
                    } else {
                        throw new IllegalArgumentException("option followed by no value: " + opt);
                    }
                }
            }
            return null;
        }

        static List<String> findOptMultiValues(String[] args, String opt) {
            final List<String> values = new ArrayList<>();
            for (int i=0; i<args.length; i++) {
                if (i < args.length-1) {
                    values.add(args[i+1]);
                    i += 1;
                } else {
                    throw new IllegalArgumentException("option followed by no value: " + opt);
                }
            }
            return values;
        }

        public Option(String[] args) throws FileNotFoundException {
            this.cmds = findOptMultiValues(args, "-cmd");
            String s = findOptValue(args, "-e");
            if (s == null) {
                String filename = findOptValue(args, "-f");
                final Scanner scanner;
                if (filename == null) {
                    scanner = new Scanner(System.in);
                } else {
                    scanner = new Scanner(new File(filename));
                }
                stmt = scanner.useDelimiter("\\A").next();
                scanner.close();
            } else {
                stmt = s;
            }

            username = "hive";
            password = "hive";
            delimiter = "\t";
        }
    }

    private static void printRowSet(RowSet rowSet, PrintStream ps, String delimiter) {
        String delim;
        for (Object[] row: rowSet) {
            delim = "";
            for (Object datum: row) {
                ps.append(delim).append(datum==null? "\\N": datum.toString());
                delim = delimiter;
            }
            ps.println();
        }
    }

    public static void main(String[] args) throws IOException, HiveSQLException {
        final Option opt = new Option(args);
        logger.debug("about to execute query: {}", opt.stmt);

        final HiveConf hiveConf = new HiveConf();
        final CLIService client = new CLIService(null);
        client.init(hiveConf);

        final SessionHandle session = client.openSession(opt.username, opt.password, new HashMap<String, String>());
        logger.debug("new session id: {}", session.getSessionId());

        final Map<String, String> confOverlay = new HashMap<>();
        {
            // init
            final String setCmd = "SET " + HiveConf.ConfVars.HIVE_SUPPORT_CONCURRENCY.varname + " = false";
            logger.debug("executing built-in cmd: {}", setCmd);
            final OperationHandle op = client.executeStatement(session, setCmd, confOverlay);
            client.closeOperation(op);
        }

        {
            for (String cmd: opt.cmds) {
                logger.debug("executing custom cmd: {}", cmd);
                final OperationHandle op = client.executeStatement(session, cmd, confOverlay);
                client.closeOperation(op);
            }
        }

        final OperationHandle op = client.executeStatement(session, opt.stmt, confOverlay);
        {
            // print schema
            logger.info("result schema:");
            System.err.println("result schema:");
            for (ColumnDescriptor col: client.getResultSetMetadata(op).getColumnDescriptors()) {
                logger.debug("\t{}\t{}", col.getName(), col.getTypeName());
                System.err.println("\t{}\t{}" +  col.getName() + col.getTypeName());
            }
            // print data
            final RowSet rowSet = client.fetchResults(op);
            logger.debug("fetched {} rows", rowSet.numRows());
            printRowSet(rowSet, System.out, opt.delimiter);
            client.closeOperation(op);
        }

        client.closeSession(session);
        logger.info("[all done]");
    }
}

/*
import org.apache.hive.service.cli._
import org.apache.hadoop.hive.conf.HiveConf
import java.util._
import scala.collection.JavaConversions._

val hiveConf = new HiveConf()
val client = new CLIService(null)
client.init(hiveConf)

val sessionHandle = client.openSession("hive", "hive", new HashMap[String, String]())
println("session id: " + sessionHandle.getSessionId())

val confOverlay = new HashMap[String, String]()

{
    val setCmd = "SET " + HiveConf.ConfVars.HIVE_SUPPORT_CONCURRENCY.varname +  " = false"
    println(setCmd)
    val op = client.executeStatement(sessionHandle, setCmd, confOverlay)
    client.closeOperation(op)
}

val ql = "SELECT * FROM test.exam_v1 LIMIT 3"
val op = client.executeStatement(sessionHandle, ql, confOverlay)

def printRowSet(op: OperationHandle): Unit = {
    val rowSet = client.fetchResults(op)
    val iter = rowSet.iterator()
    while (iter.hasNext) {
        println(iter.next.mkString(","))
    }
}

def printSchema(op: OperationHandle): Unit = {
    client.getResultSetMetadata(op).getColumnDescriptors.foreach { col =>
        println(col.getName + "\t" + col.getTypeName)
    }
}
* */