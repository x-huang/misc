package xw.data.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.hivelite.HiveLiteQuery;
import xw.data.hivelite.HiveLiteSession;
import xw.data.stream.text.RecordTextFormatter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.sql.SQLException;

/**
 * Created by xhuang on 7/25/16.
 */
public class HiveLiteAsync
{
    final static private Logger logger = LoggerFactory.getLogger(HiveLiteAsync.class);

    static class Option {

        final String stmt;
        final List<String> cmds;
        final String username;
        final String password;

        static String findOptValue(String[] args, String opt) {
            for (int i=0; i<args.length; i++) {
                if (args[i].equals(opt)) {
                    if (i < args.length-1) {
                        return args[i+1];
                    } else {
                        throw new IllegalArgumentException("option followed by no value: " + opt);
                    }
                }
            }
            return null;
        }

        static List<String> findOptMultiValues(String[] args, String opt) {
            final List<String> values = new ArrayList<>();
            for (int i=0; i<args.length; i++) {
                if (i < args.length-1) {
                    values.add(args[i+1]);
                    i += 1;
                } else {
                    throw new IllegalArgumentException("option followed by no value: " + opt);
                }
            }
            return values;
        }

        public Option(String[] args) throws FileNotFoundException {
            this.cmds = findOptMultiValues(args, "-cmd");
            String s = findOptValue(args, "-e");
            if (s == null) {
                String filename = findOptValue(args, "-f");
                final Scanner scanner;
                if (filename == null) {
                    scanner = new Scanner(System.in);
                } else {
                    scanner = new Scanner(new File(filename));
                }
                stmt = scanner.useDelimiter("\\A").next();
                scanner.close();
            } else {
                stmt = s;
            }

            username = "hive";
            password = "hive";
        }
    }

    public static void main(String[] args) throws IOException, SQLException, InterruptedException {
        final Option opt = new Option(args);
        logger.debug("about to execute query: {}", opt.stmt);

        final HiveLiteSession hivelite = new HiveLiteSession(opt.username, opt.password);

        for (String cmd: opt.cmds) {
            logger.debug("executing custom cmd: {}", cmd);
            hivelite.execCmd(cmd);
        }

        final HiveLiteQuery query = hivelite.execQueryAsync(opt.stmt);
        while (true) {
            Thread.sleep(1);

            final String status = query.getStateName();
            logger.debug("query status: {}", status);

            if (status.equals("FINISHED")) {
                logger.debug("query schema: {}", query.getSchema().toString());
                logger.debug("result set rows: {}, columns: {}", query.numRows(), query.numColumns());

                final RecordTextFormatter formatter = RecordTextFormatter.create("tsv");
                final Iterator<Object[]> rowIter = query.getRowIterator();
                while (rowIter.hasNext()) {
                    System.out.println(formatter.format(rowIter.next()));
                }
                break;
            }
        }

        query.close();
        hivelite.close();
        logger.info("[all done]");
    }
}

