RCFile and ORC File toolset
===========================

A series of tools to stat/dump/merge Apache Hive RCFile and ORC File (Hive 0.12.0 version) and convert from each other. Currently supports:

- read/dump single RCFile or multiple RCFiles that reside in same directory
- read/dump single ORC file or multiple ORC files that reside in same directory
- write to single ORC file

Writing to RCFiles are not supported yet. 

Library Dependencies

- hadoop-1.2.1
- hive-0.12.0
- eadp-transport-1.0-hive-12 (a cluster data resource streams manipulation library)
- jackson core and mapper 


Internal Design
---------------
This project heavily relies on the eadp-transport project, which is a data resource stream manipulation library. The main idea of this toolset is to open RCFile and ORCFile as standard Java input/output streams and have the transport library to handle data reformat, filtering, copying, and dumping. 

The code base is built upon Hive's object inspector layer, bypassing Hive QL for performance. 

Installation
------------
```sh
git clone https://github.com/x-huang/misc
cd orcfile-tools/
gradle build
cp build/libs/eadp-orc-0.1.jar lib/  (optional)
```

Configuration
-------------

RCFile doesn't keep metadata itself. It relies on Hive to understand what's the data schema and serialization/deserialzation means. ORC file, on the hand, is schema-aware by itself. Therefore, these information are needed to run this toolset:

- RCFile schema, RCFile item separator, key-value separator when reading RCFiles
- ORC file schema when writing ORC files;

Therefore before using this toolset, please setup these environment variables:

    export RCFILE_INPUT_SCHEMA='...'
    export RCFILE_ITEM_SEPARATOR='?'
    export RCFILE_KEYVALUE_SEPARATOR='?'
    export ORCFILE_OUTPUT_SCHEMA=$RCFILE_INPUT_SCHEMA

RCFILE_ITEM_SEPARATOR and RCFILE_KEYVALUE_SEPARATOR are used to parse map value. They 
are defined when user creates the Hive table. One can find it by:

```
hive> show create table a_hive_tbl;
OK
...
ROW FORMAT DELIMITED 
  COLLECTION ITEMS TERMINATED BY '&' 
  MAP KEYS TERMINATED BY '=' 
hive>
```

RCFILE_INPUT_SCHEMA is written in Hive typename form. Simply use the CLI tool PrintSchema to show it:
```
$ java com.ea.eadp.data.orc.cli.PrintSchema hive:///mydb/mytbl
struct<gamertag:string,ip:bigint,mac:string,persona:bigint,region:string,session:string,ts:string,step:int,m:string,g:string,s:string,param:map<string,string>,sku:string>
```

Copy/paste the single output line to replace "..." in RCFILE_INPUT_SCHEMA's value.

Example env.sh file to ```source```

```
LIBDIR=`pwd`/lib

PATHS=(
    $HADOOP_HOME/*
    $HADOOP_HOME/lib/*
    $HIVE_HOME/lib/*
    $HIVE_HOME/conf
    $HADOOP_HOME/conf
    $LIBDIR/eadp-transport-1.0-hive-12.jar
    $LIBDIR/eadp-orc-0.1.jar
)

SAVE_IFS=$IFS
IFS=":"
CLASSPATH="${PATHS[*]}"
IFS=$SAVE_IFS

export CLASSPATH

export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native/Linux-amd64-64
export TRANSPORT_SERVICE_THREADS=5
export TRANSPORT_SERVICE_KEEP_ALIVE=600

export RCFILE_INPUT_SCHEMA='struct<gamertag:string,ip:bigint,mac:string,persona:bigint,region:string,session:string,ts:string,step:int,m:string,g:string,s:string,param:map<string,string>,sku:string>'
export RCFILE_ITEM_SEPARATOR='&'
export RCFILE_KEYVALUE_SEPARATOR='='

export ORCFILE_OUTPUT_SCHEMA=$RCFILE_INPUT_SCHEMA

```

