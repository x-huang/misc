package org.apache.hadoop.hive.ql.io.orc;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

import org.apache.hadoop.hive.common.type.HiveDecimal;
import org.apache.hadoop.hive.common.type.HiveVarchar;
import org.apache.hadoop.hive.serde2.io.DateWritable;
import org.apache.hadoop.hive.serde2.io.DoubleWritable;
import org.apache.hadoop.hive.serde2.io.HiveDecimalWritable;
import org.apache.hadoop.hive.serde2.io.HiveVarcharWritable;
import org.apache.hadoop.hive.serde2.io.ShortWritable;
import org.apache.hadoop.hive.serde2.io.TimestampWritable;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.ByteWritable;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class Writables
{	
	public static Writable getWritableObject(Object o) 
	{
		if (o == null) {
			return NullWritable.get();
		}
		else if (o instanceof byte[]) {
			return new BytesWritable((byte[]) o);
		}
		else if (o instanceof Boolean) {
			return new BooleanWritable((Boolean) o);
		}
		else if (o instanceof Byte) {
			return new ByteWritable((Byte) o);
		}
		else if (o instanceof Date) {
			return new DateWritable((Date) o);
		}
		else if (o instanceof Double) {
			return new DoubleWritable((Double) o);
		}
		else if (o instanceof Float) {
			return new FloatWritable((Float) o);
		}
		else if (o instanceof HiveDecimal) {
			return new HiveDecimalWritable((HiveDecimal) o);
		}
		else if (o instanceof HiveVarchar) {
			return new HiveVarcharWritable((HiveVarchar) o);
		}
		else if (o instanceof Integer) {
			return new IntWritable((Integer) o);
		}
		else if (o instanceof Long) {
			return new LongWritable((Long) o);
		}
		else if (o instanceof Short) {
			return new ShortWritable((Short) o);
		}
		else if (o instanceof String) {
			return new Text((String) o);
		}
		else if (o instanceof Timestamp) {
			return new TimestampWritable((Timestamp) o);
		}
/*		else if (o instanceof List<?>) {
			List<?> list = (List<?>) o;
			ArrayWritable aw = new ArrayWritable(o.getClass());
		}
*/	
		else if (o instanceof Map<?, ?>) { // including MapWritable
			Map<?, ?> map = (Map<? ,?>) o;
			MapWritable mw = new MapWritable();
			for (Map.Entry<?, ?> entry: map.entrySet()) {
				Object key = entry.getKey();
				Object value = entry.getValue();
				if (value == null) {
					System.err.println("[WARN] map key '" + key.toString() 
							+ "' holds a null value; assign to an empty string");
					value = "";
				} 	 
				mw.put(getWritableObject(key), getWritableObject(value));
			}
			return mw;
		}
		else  {
			String msg = "unexpected java object: " + o.getClass().getName();
			throw new IllegalArgumentException(msg);
		}
	}
	
	public static Object getValueObject(Writable w) 
	{
		if (w == null) {
			return null;
		}
		else if (w instanceof NullWritable) {
			return null;
		}
		else if (w instanceof BytesWritable) {
			// by LazyBinary
			return ((BytesWritable) w).getBytes();
		}
		else if (w instanceof BooleanWritable) {
			// by LazyBoolean
			return ((BooleanWritable) w).get();
		}
		else if (w instanceof ByteWritable) {
			// by LazyByte
			return ((ByteWritable) w).get();
		}
		else if (w instanceof DateWritable) {
			// by LazyDate
			return ((DateWritable) w).get();
		}
		else if (w instanceof DoubleWritable) {
			// by LazyDouble
			return ((DoubleWritable) w).get();
		}
		else if (w instanceof FloatWritable) {
			// by LazyFloat
			return ((FloatWritable) w).get();
		}
		else if (w instanceof HiveDecimalWritable) {
			// by LazyHiveDecimal
			return ((HiveDecimalWritable) w).getHiveDecimal();
		}
		else if (w instanceof HiveVarcharWritable) {
			// by LazyHiveVarchar
			return ((HiveVarcharWritable) w).getHiveVarchar();
		}
		else if (w instanceof IntWritable) {
			// by LazyInteger
			return ((IntWritable) w).get();
		}
		else if (w instanceof LongWritable) {
			// by LazyLong
			return ((LongWritable) w).get();
		}
		else if (w instanceof ShortWritable) {
			// by LazyShort
			return ((ShortWritable) w).get();
		}
		else if (w instanceof Text) {
			// by LazyString
			return ((Text) w).toString();
		}
		else if (w instanceof TimestampWritable) {
			// by LazyTimestamp
			return ((TimestampWritable) w).getTimestamp();
		}
		else {
			String msg = "unexpected writable object: " + w.getClass().getName();
			throw new IllegalArgumentException(msg);
		}
	}
}
