// $ANTLR 3.5.1 Schema.g 2014-05-19 19:09:10

package com.ea.eadp.data.schema.parser;

import java.util.LinkedList;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorUtils;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class SchemaParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ID", "','", "':'", "'<'", "'>'", 
		"'bigint'", "'binary'", "'boolean'", "'char'", "'date'", "'datetime'", 
		"'decimal'", "'double'", "'float'", "'int'", "'map'", "'smallint'", "'string'", 
		"'struct'", "'timestamp'", "'tinyint'", "'varchar'", "'void'"
	};
	public static final int EOF=-1;
	public static final int T__5=5;
	public static final int T__6=6;
	public static final int T__7=7;
	public static final int T__8=8;
	public static final int T__9=9;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int T__12=12;
	public static final int T__13=13;
	public static final int T__14=14;
	public static final int T__15=15;
	public static final int T__16=16;
	public static final int T__17=17;
	public static final int T__18=18;
	public static final int T__19=19;
	public static final int T__20=20;
	public static final int T__21=21;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int T__24=24;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int ID=4;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public SchemaParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public SchemaParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return SchemaParser.tokenNames; }
	@Override public String getGrammarFileName() { return "Schema.g"; }


	public static class schema_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "schema"
	// Schema.g:29:1: schema returns [ ObjectInspector oi ] : ty= type EOF !;
	public final SchemaParser.schema_return schema() throws RecognitionException {
		SchemaParser.schema_return retval = new SchemaParser.schema_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EOF1=null;
		ParserRuleReturnScope ty =null;

		Object EOF1_tree=null;

		try {
			// Schema.g:30:2: (ty= type EOF !)
			// Schema.g:30:4: ty= type EOF !
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_type_in_schema73);
			ty=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, ty.getTree());

			EOF1=(Token)match(input,EOF,FOLLOW_EOF_in_schema75); if (state.failed) return retval;
			if ( state.backtracking==0 ) { retval.oi = (ty!=null?((SchemaParser.type_return)ty).oi:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "schema"


	public static class type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "type"
	// Schema.g:33:1: type returns [ ObjectInspector oi ] : (pt= primitive_type |mt= map_type |st= struct_type );
	public final SchemaParser.type_return type() throws RecognitionException {
		SchemaParser.type_return retval = new SchemaParser.type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope pt =null;
		ParserRuleReturnScope mt =null;
		ParserRuleReturnScope st =null;


		try {
			// Schema.g:34:3: (pt= primitive_type |mt= map_type |st= struct_type )
			int alt1=3;
			switch ( input.LA(1) ) {
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 20:
			case 21:
			case 23:
			case 24:
			case 25:
			case 26:
				{
				alt1=1;
				}
				break;
			case 19:
				{
				alt1=2;
				}
				break;
			case 22:
				{
				alt1=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}
			switch (alt1) {
				case 1 :
					// Schema.g:34:5: pt= primitive_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_primitive_type_in_type99);
					pt=primitive_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, pt.getTree());

					if ( state.backtracking==0 ) { retval.oi = (pt!=null?((SchemaParser.primitive_type_return)pt).oi:null); }
					}
					break;
				case 2 :
					// Schema.g:35:5: mt= map_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_map_type_in_type112);
					mt=map_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, mt.getTree());

					if ( state.backtracking==0 ) { retval.oi = (mt!=null?((SchemaParser.map_type_return)mt).oi:null); }
					}
					break;
				case 3 :
					// Schema.g:36:5: st= struct_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_struct_type_in_type125);
					st=struct_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, st.getTree());

					if ( state.backtracking==0 ) { retval.oi = (st!=null?((SchemaParser.struct_type_return)st).oi:null);  }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "type"


	public static class primitive_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "primitive_type"
	// Schema.g:39:1: primitive_type returns [ ObjectInspector oi ] : ( 'void' | 'boolean' | 'tinyint' | 'smallint' | 'int' | 'bigint' | 'float' | 'double' | 'string' | 'varchar' | 'char' | 'date' | 'datetime' | 'timestamp' | 'decimal' | 'binary' );
	public final SchemaParser.primitive_type_return primitive_type() throws RecognitionException {
		SchemaParser.primitive_type_return retval = new SchemaParser.primitive_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal2=null;
		Token string_literal3=null;
		Token string_literal4=null;
		Token string_literal5=null;
		Token string_literal6=null;
		Token string_literal7=null;
		Token string_literal8=null;
		Token string_literal9=null;
		Token string_literal10=null;
		Token string_literal11=null;
		Token string_literal12=null;
		Token string_literal13=null;
		Token string_literal14=null;
		Token string_literal15=null;
		Token string_literal16=null;
		Token string_literal17=null;

		Object string_literal2_tree=null;
		Object string_literal3_tree=null;
		Object string_literal4_tree=null;
		Object string_literal5_tree=null;
		Object string_literal6_tree=null;
		Object string_literal7_tree=null;
		Object string_literal8_tree=null;
		Object string_literal9_tree=null;
		Object string_literal10_tree=null;
		Object string_literal11_tree=null;
		Object string_literal12_tree=null;
		Object string_literal13_tree=null;
		Object string_literal14_tree=null;
		Object string_literal15_tree=null;
		Object string_literal16_tree=null;
		Object string_literal17_tree=null;

		try {
			// Schema.g:40:2: ( 'void' | 'boolean' | 'tinyint' | 'smallint' | 'int' | 'bigint' | 'float' | 'double' | 'string' | 'varchar' | 'char' | 'date' | 'datetime' | 'timestamp' | 'decimal' | 'binary' )
			int alt2=16;
			switch ( input.LA(1) ) {
			case 26:
				{
				alt2=1;
				}
				break;
			case 11:
				{
				alt2=2;
				}
				break;
			case 24:
				{
				alt2=3;
				}
				break;
			case 20:
				{
				alt2=4;
				}
				break;
			case 18:
				{
				alt2=5;
				}
				break;
			case 9:
				{
				alt2=6;
				}
				break;
			case 17:
				{
				alt2=7;
				}
				break;
			case 16:
				{
				alt2=8;
				}
				break;
			case 21:
				{
				alt2=9;
				}
				break;
			case 25:
				{
				alt2=10;
				}
				break;
			case 12:
				{
				alt2=11;
				}
				break;
			case 13:
				{
				alt2=12;
				}
				break;
			case 14:
				{
				alt2=13;
				}
				break;
			case 23:
				{
				alt2=14;
				}
				break;
			case 15:
				{
				alt2=15;
				}
				break;
			case 10:
				{
				alt2=16;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// Schema.g:40:4: 'void'
					{
					root_0 = (Object)adaptor.nil();


					string_literal2=(Token)match(input,26,FOLLOW_26_in_primitive_type144); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal2_tree = (Object)adaptor.create(string_literal2);
					adaptor.addChild(root_0, string_literal2_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("void")); }
					}
					break;
				case 2 :
					// Schema.g:41:4: 'boolean'
					{
					root_0 = (Object)adaptor.nil();


					string_literal3=(Token)match(input,11,FOLLOW_11_in_primitive_type151); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal3_tree = (Object)adaptor.create(string_literal3);
					adaptor.addChild(root_0, string_literal3_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("boolean")); }
					}
					break;
				case 3 :
					// Schema.g:42:4: 'tinyint'
					{
					root_0 = (Object)adaptor.nil();


					string_literal4=(Token)match(input,24,FOLLOW_24_in_primitive_type159); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal4_tree = (Object)adaptor.create(string_literal4);
					adaptor.addChild(root_0, string_literal4_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("tinyint")); }
					}
					break;
				case 4 :
					// Schema.g:43:4: 'smallint'
					{
					root_0 = (Object)adaptor.nil();


					string_literal5=(Token)match(input,20,FOLLOW_20_in_primitive_type167); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal5_tree = (Object)adaptor.create(string_literal5);
					adaptor.addChild(root_0, string_literal5_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("smallint")); }
					}
					break;
				case 5 :
					// Schema.g:44:4: 'int'
					{
					root_0 = (Object)adaptor.nil();


					string_literal6=(Token)match(input,18,FOLLOW_18_in_primitive_type174); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal6_tree = (Object)adaptor.create(string_literal6);
					adaptor.addChild(root_0, string_literal6_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("int")); }
					}
					break;
				case 6 :
					// Schema.g:45:4: 'bigint'
					{
					root_0 = (Object)adaptor.nil();


					string_literal7=(Token)match(input,9,FOLLOW_9_in_primitive_type183); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal7_tree = (Object)adaptor.create(string_literal7);
					adaptor.addChild(root_0, string_literal7_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("bigint"));}
					}
					break;
				case 7 :
					// Schema.g:46:4: 'float'
					{
					root_0 = (Object)adaptor.nil();


					string_literal8=(Token)match(input,17,FOLLOW_17_in_primitive_type191); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal8_tree = (Object)adaptor.create(string_literal8);
					adaptor.addChild(root_0, string_literal8_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("float")); }
					}
					break;
				case 8 :
					// Schema.g:47:4: 'double'
					{
					root_0 = (Object)adaptor.nil();


					string_literal9=(Token)match(input,16,FOLLOW_16_in_primitive_type198); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal9_tree = (Object)adaptor.create(string_literal9);
					adaptor.addChild(root_0, string_literal9_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("double")); }
					}
					break;
				case 9 :
					// Schema.g:48:4: 'string'
					{
					root_0 = (Object)adaptor.nil();


					string_literal10=(Token)match(input,21,FOLLOW_21_in_primitive_type204); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal10_tree = (Object)adaptor.create(string_literal10);
					adaptor.addChild(root_0, string_literal10_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("string")); }
					}
					break;
				case 10 :
					// Schema.g:49:4: 'varchar'
					{
					root_0 = (Object)adaptor.nil();


					string_literal11=(Token)match(input,25,FOLLOW_25_in_primitive_type211); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal11_tree = (Object)adaptor.create(string_literal11);
					adaptor.addChild(root_0, string_literal11_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("varchar")); }
					}
					break;
				case 11 :
					// Schema.g:50:4: 'char'
					{
					root_0 = (Object)adaptor.nil();


					string_literal12=(Token)match(input,12,FOLLOW_12_in_primitive_type218); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal12_tree = (Object)adaptor.create(string_literal12);
					adaptor.addChild(root_0, string_literal12_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("char")); }
					}
					break;
				case 12 :
					// Schema.g:51:4: 'date'
					{
					root_0 = (Object)adaptor.nil();


					string_literal13=(Token)match(input,13,FOLLOW_13_in_primitive_type225); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal13_tree = (Object)adaptor.create(string_literal13);
					adaptor.addChild(root_0, string_literal13_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("date")); }
					}
					break;
				case 13 :
					// Schema.g:52:4: 'datetime'
					{
					root_0 = (Object)adaptor.nil();


					string_literal14=(Token)match(input,14,FOLLOW_14_in_primitive_type231); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal14_tree = (Object)adaptor.create(string_literal14);
					adaptor.addChild(root_0, string_literal14_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("datetime"));}
					}
					break;
				case 14 :
					// Schema.g:53:4: 'timestamp'
					{
					root_0 = (Object)adaptor.nil();


					string_literal15=(Token)match(input,23,FOLLOW_23_in_primitive_type238); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal15_tree = (Object)adaptor.create(string_literal15);
					adaptor.addChild(root_0, string_literal15_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("timestamp"));}
					}
					break;
				case 15 :
					// Schema.g:54:4: 'decimal'
					{
					root_0 = (Object)adaptor.nil();


					string_literal16=(Token)match(input,15,FOLLOW_15_in_primitive_type245); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal16_tree = (Object)adaptor.create(string_literal16);
					adaptor.addChild(root_0, string_literal16_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("decimal"));}
					}
					break;
				case 16 :
					// Schema.g:55:4: 'binary'
					{
					root_0 = (Object)adaptor.nil();


					string_literal17=(Token)match(input,10,FOLLOW_10_in_primitive_type251); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal17_tree = (Object)adaptor.create(string_literal17);
					adaptor.addChild(root_0, string_literal17_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("binary")); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "primitive_type"


	public static class map_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "map_type"
	// Schema.g:58:1: map_type returns [ ObjectInspector oi ] : 'map' '<' kt= primitive_type ',' vt= primitive_type '>' ;
	public final SchemaParser.map_type_return map_type() throws RecognitionException {
		SchemaParser.map_type_return retval = new SchemaParser.map_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal18=null;
		Token char_literal19=null;
		Token char_literal20=null;
		Token char_literal21=null;
		ParserRuleReturnScope kt =null;
		ParserRuleReturnScope vt =null;

		Object string_literal18_tree=null;
		Object char_literal19_tree=null;
		Object char_literal20_tree=null;
		Object char_literal21_tree=null;

		try {
			// Schema.g:59:2: ( 'map' '<' kt= primitive_type ',' vt= primitive_type '>' )
			// Schema.g:59:4: 'map' '<' kt= primitive_type ',' vt= primitive_type '>'
			{
			root_0 = (Object)adaptor.nil();


			string_literal18=(Token)match(input,19,FOLLOW_19_in_map_type270); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal18_tree = (Object)adaptor.create(string_literal18);
			adaptor.addChild(root_0, string_literal18_tree);
			}

			char_literal19=(Token)match(input,7,FOLLOW_7_in_map_type272); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal19_tree = (Object)adaptor.create(char_literal19);
			adaptor.addChild(root_0, char_literal19_tree);
			}

			pushFollow(FOLLOW_primitive_type_in_map_type278);
			kt=primitive_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, kt.getTree());

			char_literal20=(Token)match(input,5,FOLLOW_5_in_map_type280); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal20_tree = (Object)adaptor.create(char_literal20);
			adaptor.addChild(root_0, char_literal20_tree);
			}

			pushFollow(FOLLOW_primitive_type_in_map_type286);
			vt=primitive_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, vt.getTree());

			char_literal21=(Token)match(input,8,FOLLOW_8_in_map_type288); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal21_tree = (Object)adaptor.create(char_literal21);
			adaptor.addChild(root_0, char_literal21_tree);
			}

			if ( state.backtracking==0 ) { retval.oi = ObjectInspectorFactory.getStandardMapObjectInspector((kt!=null?((SchemaParser.primitive_type_return)kt).oi:null), (vt!=null?((SchemaParser.primitive_type_return)vt).oi:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "map_type"


	public static class struct_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "struct_type"
	// Schema.g:62:1: struct_type returns [ ObjectInspector oi ] : 'struct' '<' fields= field_list '>' ;
	public final SchemaParser.struct_type_return struct_type() throws RecognitionException {
		SchemaParser.struct_type_return retval = new SchemaParser.struct_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal22=null;
		Token char_literal23=null;
		Token char_literal24=null;
		ParserRuleReturnScope fields =null;

		Object string_literal22_tree=null;
		Object char_literal23_tree=null;
		Object char_literal24_tree=null;

		try {
			// Schema.g:63:2: ( 'struct' '<' fields= field_list '>' )
			// Schema.g:63:4: 'struct' '<' fields= field_list '>'
			{
			root_0 = (Object)adaptor.nil();


			string_literal22=(Token)match(input,22,FOLLOW_22_in_struct_type305); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal22_tree = (Object)adaptor.create(string_literal22);
			adaptor.addChild(root_0, string_literal22_tree);
			}

			char_literal23=(Token)match(input,7,FOLLOW_7_in_struct_type307); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal23_tree = (Object)adaptor.create(char_literal23);
			adaptor.addChild(root_0, char_literal23_tree);
			}

			pushFollow(FOLLOW_field_list_in_struct_type313);
			fields=field_list();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, fields.getTree());

			char_literal24=(Token)match(input,8,FOLLOW_8_in_struct_type315); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal24_tree = (Object)adaptor.create(char_literal24);
			adaptor.addChild(root_0, char_literal24_tree);
			}

			if ( state.backtracking==0 ) { retval.oi = ObjectInspectorFactory.getStandardStructObjectInspector((fields!=null?((SchemaParser.field_list_return)fields).names:null), (fields!=null?((SchemaParser.field_list_return)fields).ois:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "struct_type"


	public static class field_list_return extends ParserRuleReturnScope {
		public List<String> names;
		public List<ObjectInspector> ois;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "field_list"
	// Schema.g:66:1: field_list returns [ List<String> names , List<ObjectInspector> ois ] : (fld1= field_def ',' flds= field_list |fld2= field_def );
	public final SchemaParser.field_list_return field_list() throws RecognitionException {
		SchemaParser.field_list_return retval = new SchemaParser.field_list_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal25=null;
		ParserRuleReturnScope fld1 =null;
		ParserRuleReturnScope flds =null;
		ParserRuleReturnScope fld2 =null;

		Object char_literal25_tree=null;

		try {
			// Schema.g:67:2: (fld1= field_def ',' flds= field_list |fld2= field_def )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==ID) ) {
				int LA3_1 = input.LA(2);
				if ( (synpred18_Schema()) ) {
					alt3=1;
				}
				else if ( (true) ) {
					alt3=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// Schema.g:67:4: fld1= field_def ',' flds= field_list
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_field_def_in_field_list337);
					fld1=field_def();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, fld1.getTree());

					char_literal25=(Token)match(input,5,FOLLOW_5_in_field_list339); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal25_tree = (Object)adaptor.create(char_literal25);
					adaptor.addChild(root_0, char_literal25_tree);
					}

					pushFollow(FOLLOW_field_list_in_field_list345);
					flds=field_list();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, flds.getTree());

					if ( state.backtracking==0 ) { retval.names = (flds!=null?((SchemaParser.field_list_return)flds).names:null); retval.ois = (flds!=null?((SchemaParser.field_list_return)flds).ois:null); retval.names.add(0, (fld1!=null?((SchemaParser.field_def_return)fld1).name:null)); retval.ois.add(0, (fld1!=null?((SchemaParser.field_def_return)fld1).oi:null)); }
					}
					break;
				case 2 :
					// Schema.g:68:4: fld2= field_def
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_field_def_in_field_list357);
					fld2=field_def();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, fld2.getTree());

					if ( state.backtracking==0 ) { retval.names = new LinkedList<String>(); retval.names.add((fld2!=null?((SchemaParser.field_def_return)fld2).name:null)); retval.ois = new LinkedList<ObjectInspector>(); retval.ois.add((fld2!=null?((SchemaParser.field_def_return)fld2).oi:null)); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "field_list"


	public static class field_def_return extends ParserRuleReturnScope {
		public String name;
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "field_def"
	// Schema.g:71:1: field_def returns [ String name, ObjectInspector oi ] : ID ':' ty= type ;
	public final SchemaParser.field_def_return field_def() throws RecognitionException {
		SchemaParser.field_def_return retval = new SchemaParser.field_def_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID26=null;
		Token char_literal27=null;
		ParserRuleReturnScope ty =null;

		Object ID26_tree=null;
		Object char_literal27_tree=null;

		try {
			// Schema.g:72:2: ( ID ':' ty= type )
			// Schema.g:72:4: ID ':' ty= type
			{
			root_0 = (Object)adaptor.nil();


			ID26=(Token)match(input,ID,FOLLOW_ID_in_field_def376); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			ID26_tree = (Object)adaptor.create(ID26);
			adaptor.addChild(root_0, ID26_tree);
			}

			char_literal27=(Token)match(input,6,FOLLOW_6_in_field_def378); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal27_tree = (Object)adaptor.create(char_literal27);
			adaptor.addChild(root_0, char_literal27_tree);
			}

			pushFollow(FOLLOW_type_in_field_def384);
			ty=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, ty.getTree());

			if ( state.backtracking==0 ) { retval.name = (ID26!=null?ID26.getText():null); retval.oi = (ty!=null?((SchemaParser.type_return)ty).oi:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "field_def"

	// $ANTLR start synpred18_Schema
	public final void synpred18_Schema_fragment() throws RecognitionException {
		ParserRuleReturnScope fld1 =null;
		ParserRuleReturnScope flds =null;


		// Schema.g:67:4: (fld1= field_def ',' flds= field_list )
		// Schema.g:67:4: fld1= field_def ',' flds= field_list
		{
		pushFollow(FOLLOW_field_def_in_synpred18_Schema337);
		fld1=field_def();
		state._fsp--;
		if (state.failed) return;

		match(input,5,FOLLOW_5_in_synpred18_Schema339); if (state.failed) return;

		pushFollow(FOLLOW_field_list_in_synpred18_Schema345);
		flds=field_list();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred18_Schema

	// Delegated rules

	public final boolean synpred18_Schema() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred18_Schema_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}



	public static final BitSet FOLLOW_type_in_schema73 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_schema75 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitive_type_in_type99 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_map_type_in_type112 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_struct_type_in_type125 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_26_in_primitive_type144 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_11_in_primitive_type151 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_24_in_primitive_type159 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_20_in_primitive_type167 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_18_in_primitive_type174 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_9_in_primitive_type183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_17_in_primitive_type191 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_16_in_primitive_type198 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_21_in_primitive_type204 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_25_in_primitive_type211 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_12_in_primitive_type218 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_13_in_primitive_type225 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_14_in_primitive_type231 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_23_in_primitive_type238 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_15_in_primitive_type245 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_10_in_primitive_type251 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_19_in_map_type270 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_7_in_map_type272 = new BitSet(new long[]{0x0000000007B7FE00L});
	public static final BitSet FOLLOW_primitive_type_in_map_type278 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_5_in_map_type280 = new BitSet(new long[]{0x0000000007B7FE00L});
	public static final BitSet FOLLOW_primitive_type_in_map_type286 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_8_in_map_type288 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_22_in_struct_type305 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_7_in_struct_type307 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_field_list_in_struct_type313 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_8_in_struct_type315 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_field_def_in_field_list337 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_5_in_field_list339 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_field_list_in_field_list345 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_field_def_in_field_list357 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_field_def376 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_6_in_field_def378 = new BitSet(new long[]{0x0000000007FFFE00L});
	public static final BitSet FOLLOW_type_in_field_def384 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_field_def_in_synpred18_Schema337 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_5_in_synpred18_Schema339 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_field_list_in_synpred18_Schema345 = new BitSet(new long[]{0x0000000000000002L});
}
