grammar Schema;

options {  
  	language = Java; 
  	output = AST;
  	backtrack=true;
	//k = 2;
} 

@header {
package com.ea.eadp.data.schema.parser;

import java.util.LinkedList;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorUtils;
}


@lexer::header {
package com.ea.eadp.data.schema.parser;
}

// parser rules

schema 	returns [ ObjectInspector oi ]
	: ty = type EOF! { $oi = $ty.oi; }
	;	

type returns [ ObjectInspector oi ]
 	: pt = primitive_type { $oi = $pt.oi; } 
 	| mt = map_type  { $oi = $mt.oi; }
 	| st = struct_type  { $oi = $st.oi;  }
	;
	
primitive_type returns [ ObjectInspector oi ]
	: 'void' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("void")); }
	| 'boolean' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("boolean")); } 
	| 'tinyint' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("tinyint")); } 
	| 'smallint' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("smallint")); }
	| 'int'   { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("int")); }
	| 'bigint' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("bigint"));} 
	| 'float' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("float")); }
	| 'double'{ $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("double")); }
	| 'string' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("string")); }
	| 'varchar' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("varchar")); }
	| 'char' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("char")); }
	| 'date'{ $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("date")); }
	| 'datetime' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("datetime"));}
	| 'timestamp' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("timestamp"));}
	| 'decimal'{ $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("decimal"));}
	| 'binary' { $oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector(PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName("binary")); }
	;
	
map_type returns [ ObjectInspector oi ] 
	: 'map' '<' kt = primitive_type ',' vt = primitive_type '>' { $oi = ObjectInspectorFactory.getStandardMapObjectInspector($kt.oi, $vt.oi); }
	;

struct_type returns [ ObjectInspector oi ]
	: 'struct' '<' fields = field_list '>' { $oi = ObjectInspectorFactory.getStandardStructObjectInspector($fields.names, $fields.ois); }
	;

field_list returns [ List<String> names , List<ObjectInspector> ois ] 
	: fld1 = field_def ',' flds = field_list { $names = $flds.names; $ois = $flds.ois; $names.add(0, $fld1.name); $ois.add(0, $fld1.oi); } 
	| fld2 = field_def { $names = new LinkedList<String>(); $names.add($fld2.name); $ois = new LinkedList<ObjectInspector>(); $ois.add($fld2.oi); } 
	;

field_def returns [ String name, ObjectInspector oi ] 
	: ID ':' ty = type { $name = $ID.text; $oi = $ty.oi; } 	
	;

ID  	: ( 'a'..'z' | 'A'..'Z' | '_' ) ( 'a'..'z' | 'A'..'Z' | '0'..'9' | '_' )*
	;