package com.ea.eadp.data.hive.objectinspector;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hive.ql.io.orc.Writables;
import org.apache.hadoop.hive.serde2.lazy.LazyArray;
import org.apache.hadoop.hive.serde2.lazy.LazyMap;
import org.apache.hadoop.hive.serde2.lazy.LazyObjectBase;
import org.apache.hadoop.hive.serde2.lazy.LazyPrimitive;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryArray;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryMap;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryObject;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryPrimitive;
import org.apache.hadoop.io.Writable;

public class LazyObjectUtils
{
	public static Object lazyToPOJO(LazyObjectBase lazy) 
	{
		if (lazy == null) {
			return null;
		}
		else if (lazy instanceof LazyPrimitive<?, ?>) {
			Writable writable = ((LazyPrimitive<?, ?>) lazy).getWritableObject();
			return Writables.getValueObject(writable);
		}
		else if (lazy instanceof LazyMap) {
			final Map<Object, Object> mapLazy = (Map<Object, Object>) ((LazyMap) lazy).getMap();
			final Map<Object, Object> data = new LinkedHashMap<>();
			for (Map.Entry<Object, Object> entry: mapLazy.entrySet()) {
				LazyObjectBase keyLazy = (LazyObjectBase) entry.getKey();
				LazyObjectBase valueLazy = (LazyObjectBase) entry.getValue();
				data.put(lazyToPOJO(keyLazy), lazyToPOJO(valueLazy));
			}
			return data;
		}
		else if (lazy instanceof LazyArray) {
			final List<Object> data = new ArrayList<>();
			for (Object ele: ((LazyArray) lazy).getList()) {
				data.add(lazyToPOJO((LazyObjectBase) ele));
			}
			return data;
		}
		else {
			String msg = "unexpected lazy object: " + lazy.getClass().getName();
			throw new IllegalArgumentException(msg);
		}
	}
	
	public static Object lazyBinaryToPOJO(LazyBinaryObject<?> lazy)
	{
		if (lazy == null) {
			return null;
		}
		else if (lazy instanceof LazyBinaryPrimitive<?, ?>) {
			Writable writable = ((LazyBinaryPrimitive<?, ?>) lazy).getWritableObject();
			return Writables.getValueObject(writable);
		}
		else if (lazy instanceof LazyBinaryMap) {
			final Map<Object, Object> data = new LinkedHashMap<>();
			for (Map.Entry<Object, Object> entry: ((LazyBinaryMap) lazy).getMap().entrySet()) {
				Writable keyWritable = (Writable) entry.getKey();
				Writable valueWritable = (Writable) entry.getValue();
				data.put(Writables.getValueObject(keyWritable), Writables.getValueObject(valueWritable));
			}
			return data;
		}
		else if (lazy instanceof LazyBinaryArray) {
			final List<Object> data = new ArrayList<>();
			for (Object ele: ((LazyBinaryArray) lazy).getList()) {
				data.add(Writables.getValueObject((Writable) ele));
			}
			return data;
		}
		else {
			String msg = "unexpected lazy binary object: " + lazy.getClass().getName();
			throw new IllegalArgumentException(msg);
		}
	}
	
}
