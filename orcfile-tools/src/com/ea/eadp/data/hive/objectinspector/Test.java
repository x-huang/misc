package com.ea.eadp.data.hive.objectinspector;

import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;

public class Test
{
	public static void main(String[] args) throws Exception
	{
		if (args.length != 1) {
			System.err.println("[USAGE] java ... <schema-string>");
			return;
		}
		
		ObjectInspector oi = ObjectInspectorUtils.getNestedObjectInspector(args[0]);
		System.out.println("object inspector class name => " + oi.getClass().getName());
		System.out.println("object inspector  type name => " + oi.getTypeName());
	}

}
