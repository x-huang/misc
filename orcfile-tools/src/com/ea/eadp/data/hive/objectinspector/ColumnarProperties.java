package com.ea.eadp.data.hive.objectinspector;

import java.util.Properties;

import org.apache.hadoop.hive.serde.serdeConstants;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe.SerDeParameters;

import com.ea.eadp.data.orc.common.Confs;

public class ColumnarProperties
{
	private int _hiveVersion = 12; 
	
	final private Properties  _tbl = new Properties();
	private SerDeParameters _serDeParams = null;
	private boolean _valid = false;
	
	private ColumnarProperties() {}
	
	public int getHiveVersion()
	{
		return _hiveVersion; 
	}
	
	public ColumnarProperties setHiveVersionForRCFileSerDe(int ver)
	{
		_hiveVersion = ver;
		return this;
	}
	
	public boolean usingBinarySerDe()
	{
		// because: [HIVE-4475] - Switch RCFile default to LazyBinaryColumnarSerDe
		return _hiveVersion >= 12; 
	}
	
	public ColumnarProperties setItemSeparator(String sep) 
	{
		if (sep != null) {
			_tbl.put(serdeConstants.COLLECTION_DELIM, sep);
			_valid = false;
		}
		return this;
	}
	
	public ColumnarProperties setKeyValueSeparator(String sep)
	{
		if (sep != null) {
			_tbl.put(serdeConstants.MAPKEY_DELIM, sep);
			_valid = false;
		}
		return this;
	}
	
	@Override
	public String toString() 
	{
		String serde = usingBinarySerDe()? "LazyBinaryColumnarSerDe": "LazySimpleSerDe";
		String descr = "using-serde:" + serde;
		
		if (! usingBinarySerDe()) {
			Object itemSeparator = _tbl.getProperty(serdeConstants.COLLECTION_DELIM, "<default>");
			Object keyValueSeparator = _tbl.getProperty(serdeConstants.MAPKEY_DELIM, "<default>");
			descr += ",item-separator:'" + itemSeparator + "',key-value-separator:'" + keyValueSeparator;
		}
		return descr;
	}
	
	public Properties getProperties()
	{
		return _tbl;
	}
	
	public SerDeParameters getSerDeParameters() throws SerDeException 
	{
		if (_serDeParams == null || !_valid) {
			_serDeParams = LazySimpleSerDe.initSerdeParams(
						Confs.getHadoopConf(), _tbl, ColumnarSerDe.class.getName());
			_valid = true;
		}
		return _serDeParams;
	}
	
	public static ColumnarProperties getDefault() {
		return new ColumnarProperties();
	}
}
