package com.ea.eadp.data.stream.orc;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.Reader;
import org.apache.hadoop.hive.ql.io.orc.RecordReader;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;

import com.ea.eadp.data.hive.objectinspector.ObjectInspectorUtils;
import com.ea.eadp.data.orc.common.Confs;
import com.ea.eadp.data.stream.ObjectInput;

public class OrcFileObjectInput implements ObjectInput
{
	final private URI _uri;
	final private Reader _reader;
	final private StructObjectInspector _oi;
	final private List<? extends StructField> _fields;
	final transient private RecordReader _rows;
	transient private Object _row;
	private long _count;
	private long _limit;
	
	public OrcFileObjectInput(URI file) throws IOException 
	{
		_uri = file;
		Path path = new Path(_uri);
		Configuration conf = Confs.getHadoopConf();
		FileSystem fs = FileSystem.get(_uri, conf);
		_reader = OrcFile.createReader(fs, path);
		_oi = (StructObjectInspector) _reader.getObjectInspector();
		_fields = _oi.getAllStructFieldRefs();
		_rows = _reader.rows(null);
		
		_row = null;
		_count = 0;
		_limit = -1;
		
//	System.err.println("[DEBUG] OI detail: " + ObjectInspectorUtils.describeDetailedObjectInspector(_oi));
		System.err.println("[INFO] reading ORC file: " + file.toString() 
							+ " (rows:" + _reader.getNumberOfRows()+ ")");
// 		System.err.println("[INFO] ORC input object inspector: " + ObjectInspectorUtils.describeObjectInspector(_oi));
	}
	
	public URI getLocationUri() 
	{
		return _uri;
	}
	
	public String getSchema()
	{
		return _oi.getTypeName();
	}
	
	public OrcFileObjectInput setLimit(long limit)
	{
		_limit = limit;
		return this;
	}

	@Override
	public Object read() throws IOException
	{
		if (_limit > 0 && _count >= _limit) {
			return null;
		}
		
		if (! _rows.hasNext()) {
			return null;
		}
		
		_row = _rows.next(_row);
		_count++;
		Object[] data = new Object[_fields.size()];
		for (int i=0; i<_fields.size(); i++) {
			Object o = _oi.getStructFieldData(_row, _fields.get(i));
			ObjectInspector fieldInspector = _fields.get(i).getFieldObjectInspector();
			data[i] = ObjectInspectorUtils.inspectObject(o, fieldInspector);
		}
		return data;
		
	}
	
	@Override
	public void close() throws IOException
	{
		_rows.close();
		_row = null;
	}

	@Override
	public long getCount()
	{
		return _count;
	}
}
