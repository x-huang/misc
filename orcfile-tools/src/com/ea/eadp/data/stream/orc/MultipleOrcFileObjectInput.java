package com.ea.eadp.data.stream.orc;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import com.ea.eadp.data.stream.ObjectInput;

public class MultipleOrcFileObjectInput implements ObjectInput
{
	final private List<URI> _uris;
	private int _index;
	private long _count;
	private OrcFileObjectInput _curr;
	
	public MultipleOrcFileObjectInput(List<URI> files) throws IOException 
	{
		_uris = files;
		_index = 0;
		_curr = new OrcFileObjectInput(_uris.get(0));
		_count = 0;
	}
	
	public List<URI> getLocationUris() 
	{
		return _uris;
	}

	public MultipleOrcFileObjectInput setLimit(long limit)
	{
		throw new UnsupportedOperationException("cannot set limit to a multiple ORC file input");
	}
	
	@Override
	public Object read() throws IOException
	{
		Object data = _curr.read();
		if (data != null) {
			return data;
		}
		
		if (_index < _uris.size()-1) {
			_curr.close();
			_count += _curr.getCount();
			_index++;
			_curr = new OrcFileObjectInput(_uris.get(_index));
			return read();
		}
		else {
			return null;
		}
	}

	
	@Override
	public void close() throws IOException
	{
		_curr.close();
	}

	@Override
	public long getCount()
	{
		return _count + _curr.getCount();
	}

}
