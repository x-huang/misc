package com.ea.eadp.data.stream.orc;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.CompressionKind;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.OrcUtils;
import org.apache.hadoop.hive.ql.io.orc.Writer;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;


import com.ea.eadp.data.hive.objectinspector.ObjectInspectorUtils;
import com.ea.eadp.data.orc.common.Confs;
import com.ea.eadp.data.stream.ObjectOutput;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public class OrcFileObjectOutput implements ObjectOutput
{	
	final private URI _uri;
	final private String _schema;
	private Writer _writer = null;
	private ObjectInspector _oi = null;
	private long _stripeSize = 102_400L;	// NOTE: stripe size must be a multiple of io.bytes.per.checksum, IOException thrown otherwise
	private int _bufferSize = 16_000;	// NOTE: shown in TBLPROPERTIES/orc.compress.size
	private boolean _blockPadding;
	private boolean _overwrite = false;
	private CompressionKind _compress = CompressionKind.SNAPPY;  // NOTE: shown in TBLPROPERTIES/orc.compress; http://blog.erdemagaoglu.com/post/4605524309/lzo-vs-snappy-vs-lzf-vs-zlib-a-comparison-of
	private long _count;
	
	public OrcFileObjectOutput(URI uri, String schema) 
	{
		_uri = uri;
		_schema = schema;
		_blockPadding = ! uri.toString().startsWith("s3n://");
		
		_count = 0;
	}
	
	public OrcFileObjectOutput setStripeSize(Long size) 
	{
		if (size != null && size > 0) {
			_stripeSize = size;
		}
		return this;
	}
	
	public OrcFileObjectOutput setBufferSize(Integer size)
	{
		if (size != null && size > 0) {
			_bufferSize = size;
		}
		return this;
	}
	
	public OrcFileObjectOutput setBlockPadding(Boolean padding)
	{
		if (padding != null) {
			_blockPadding = padding;
		}
		return this;
	}
	
	public OrcFileObjectOutput setOverwrite(Boolean overwrite)
	{
		if (overwrite != null) {
			_overwrite = overwrite;
		}
		return this;
	}
	
	public OrcFileObjectOutput setCompress(String compress)
	{
		_compress = CompressionKind.valueOf(compress);
		return this;
	}
	
	public void initialize() throws IOException 
	{
		Path path = new Path(_uri);
		Configuration conf = Confs.getHadoopConf();
		
		//_writerInspector = StructObjectInspectorBuilder.fromSchema(_schema);
		_oi = ObjectInspectorUtils.getOrcObjectInspector(_schema);
 System.err.println("[DEBUG] OI detail: " + ObjectInspectorUtils.describeDetailedObjectInspector(_oi));
		assert _oi.getTypeName().equals(_schema);
		
		OrcFile.WriterOptions opts = OrcFile.writerOptions(conf)
				.inspector(_oi)
				.stripeSize(_stripeSize)
				.bufferSize(_bufferSize)
				.blockPadding(_blockPadding)
				.compress(_compress);
		
		if (_overwrite) {
			boolean successful = HadoopCommon.delete(_uri);
			if (successful) {
				System.err.println("[INFO] deleted existing file: " + _uri.toString());
			}
		}
		
		_writer = OrcFile.createWriter(path, opts);
		
		System.err.println("[INFO] writting ORC file: " + _uri 
							+ " (compress:" + _compress.toString() 
							+ ",compress-size:" + _bufferSize 
							+ ",stripe-size:" + _stripeSize 
							+ ",block-padding:" + _blockPadding 
							+ ",typename:" + ObjectInspectorUtils.describeObjectInspector(_oi)
							+ ")");
		// System.err.println("[INFO] ORC output object inspector: " + ObjectInspectorUtils.describeObjectInspector(_oi));
	}
	
	public String getSchema() 
	{
		return _schema;
	}
	
	public URI getLocationUri() 
	{
		return _uri;
	}
	
	@Override
	public void close() throws IOException
	{
		_writer.close();
	}

	@Override
	public long getCount()
	{
		return _count;
	}

	@Override
	public void write(Object data) throws IOException
	{
		_writer.addRow(OrcUtils.getOrcStruct((Object[]) data));
		_count++;
	}
}
