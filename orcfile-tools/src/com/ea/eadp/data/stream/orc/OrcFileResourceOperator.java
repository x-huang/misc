package com.ea.eadp.data.stream.orc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.Reader;

import com.ea.eadp.data.common.uri.GeneralUriQuery;
import com.ea.eadp.data.orc.common.Commons;
import com.ea.eadp.data.orc.repr.OrcFilePropertyRepr;
import com.ea.eadp.data.stream.DataInputStream;
import com.ea.eadp.data.stream.DataOutputStream;
import com.ea.eadp.data.stream.IResourceOperator;
import com.ea.eadp.data.stream.ObjectInput;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public class OrcFileResourceOperator implements IResourceOperator
{	
	@Override
	public InputStream createInputStream(URI uri)
			throws URISyntaxException, IOException
	{
		final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
		final List<URI> orcfiles = new ArrayList<>();
		String location = query.getParameterAsString("uri");
		URI locUri = new URI(location);
		if (HadoopCommon.getFileStatusRepr(locUri).isDir) {
			if (! location.endsWith("/"))
				location += "/";
			for (String file: HadoopCommon.listFiles(locUri)) {
				orcfiles.add(new URI(location + file));
			}
		}
		else {
			orcfiles.add(locUri);
		}
		
		String format = query.getParameterAsString("format");
		if (format == null || format.isEmpty()) {
			format = "json";
		}
		
		Long limitLong = query.getParameterAsLong("limit");
		final long limit = (limitLong == null? -1: limitLong);
		
		query.removeParameters("format", "uri", "limit");
		if (query.getAllParameters().size() > 0) {
			System.err.println("[WARN] unused query options: " + query.toString());
		}
		
		final ObjectInput objectInput;
		if (orcfiles.size() == 0) {
			throw new IllegalArgumentException("no rc files");
		}
		else if	(orcfiles.size() == 1) {
			objectInput = new OrcFileObjectInput(orcfiles.get(0));
			((OrcFileObjectInput) objectInput).setLimit(limit);
		}
		else {
			objectInput = new MultipleOrcFileObjectInput(orcfiles);
			((MultipleOrcFileObjectInput) objectInput).setLimit(limit);
		}
		
		return new DataInputStream(objectInput, format);
	}
	

	@Override
	public OutputStream createOutputStream(URI uri) 
			throws URISyntaxException, IOException
	{
		final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
		
		String location = query.getParameterAsString("uri");
		if (location.endsWith("/")) {
			throw new IllegalArgumentException("destination cannot be a directory: " + location);
		}
		
		final URI locUri = new URI(location);
		String format = query.getParameterAsString("format");
		if (format == null || format.isEmpty()) {
			format = "json";
		}
		if (! format.equals("json")) {
			throw new IllegalArgumentException("cannot accept non-json format: " + format);
		}
		
		String schema = query.getParameterAsString("schema");
		if (schema == null) {
			schema = Commons.readSystemSetting("orcfile.output.schema", null);
			if (schema == null) {
				throw new IllegalArgumentException("ORC file output: missing schema");
			}
		}
		if (! schema.startsWith("struct<")) {
			schema = "struct<" + schema + ">";
			System.err.println("[WARN] schema not in struct<...> format... fixed");
		}
		
		Long stripeSize = query.getParameterAsLong("stripesize");
		if (stripeSize == null) {
			String s = Commons.readSystemSetting("orcfile.output.stripe.size", "25600L");
			stripeSize = Long.valueOf(s);
		}
		
		Integer bufferSize = query.getParameterAsInteger("buffersize");
		if (bufferSize == null) {
			String s = Commons.readSystemSetting("orcfile.output.compress.size", "64000");
			bufferSize = Integer.valueOf(s);
		}
		
		final Boolean overwrite;
		if ( query.getParameterAsString("overwrite") != null) {
			overwrite = query.getParameterAsYes("overwrite");
		}
		else {
			String s = Commons.readSystemSetting("orcfile.output.overwrite", "false");
			overwrite = Boolean.valueOf(s);
		}
		
		Boolean blockPadding = query.getParameterAsYes("blockpadding");
		
		String compress = query.getParameterAsString("compress");
		if (compress == null) {
			compress = Commons.readSystemSetting("orcfile.output.compress", "SNAPPY");
		}
		
		query.removeParameters("format", "uri", "schema", "stripesize", "buffersize", 
								"blockpadding", "compress", "overwrite");
		
		if (query.getAllParameters().size() > 0) {
			System.err.println("[WARN] unused query options: " + query.toString());
		}
		
		final OrcFileObjectOutput objectOutput = new OrcFileObjectOutput(locUri, schema);
		objectOutput.setStripeSize(stripeSize)
					.setBufferSize(bufferSize)
					.setBlockPadding(blockPadding)
					.setCompress(compress)
					.setOverwrite(overwrite)
					.initialize();
		
		return new DataOutputStream(objectOutput, format);
	}

	@Override
	public Object describeResource(URI uri) throws URISyntaxException,
			IOException
	{
		final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
		String location = query.getParameterAsString("uri");
		System.err.println("[INFO] reading ORC file: " + location); 
		URI locUri = new URI(location);
		final Configuration conf = new Configuration();
		if (HadoopCommon.getFileStatusRepr(locUri).isDir) {
			if (! location.endsWith("/")) {
				location += "/";
 			}
			List<OrcFilePropertyRepr> orcfiles = new ArrayList<>();
			for (String file: HadoopCommon.listFiles(locUri)) {
				URI fileUri = new URI(location + file);
				Path path = new Path(fileUri);
				FileSystem fs = FileSystem.get(fileUri, conf);
				Reader reader = OrcFile.createReader(fs, path);
				orcfiles.add(new OrcFilePropertyRepr(reader));
			}
			return orcfiles;
		}
		else {
			Path path = new Path(locUri);
			FileSystem fs = FileSystem.get(locUri, conf);
			Reader reader = OrcFile.createReader(fs, path);
			return new OrcFilePropertyRepr(reader);
		}
	}
}
