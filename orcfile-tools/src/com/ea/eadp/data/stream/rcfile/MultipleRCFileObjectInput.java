package com.ea.eadp.data.stream.rcfile;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;

import com.ea.eadp.data.hive.objectinspector.ObjectInspectorUtils;
import com.ea.eadp.data.hive.objectinspector.ColumnarProperties;
import com.ea.eadp.data.stream.ObjectInput;

public class MultipleRCFileObjectInput implements ObjectInput
{
	final private List<URI> _rcfiles;
	private int _index;
	private long _count;
	final private String _schema;
	final private ObjectInspector _oi;
	final private ColumnarProperties _cp;
	private RCFileObjectInput _curr;

	public MultipleRCFileObjectInput(List<URI> rcfiles, String schema, ColumnarProperties cp) throws IOException
	{
		_rcfiles = rcfiles;
		_index = 0;
		_schema = schema;
		_oi = ObjectInspectorUtils.getColumnarStructObjectInspector(schema, cp);
		_cp = cp;
		_curr = new RCFileObjectInput(_rcfiles.get(_index), _oi, cp);
		_count = 0;
	}

	public String getSchema() 
	{
		return _schema;
	}
	
	public MultipleRCFileObjectInput setLimit(long limit)
	{
		throw new UnsupportedOperationException("cannot set limit to a multiple RCFile input");
	}
	
	@Override
	public Object read() throws IOException
	{
		Object data = _curr.read();
		if (data != null) {
			return data;
		}

		if (_index < _rcfiles.size() - 1) {
			_curr.close();
			_count += _curr.getCount();
			_index++;
			_curr = new RCFileObjectInput(_rcfiles.get(_index), _oi, _cp); 
			return read();
		}
		else {
			return null;
		}
	}

	@Override
	public long getCount()
	{
		return _count + _curr.getCount();
	}

	@Override
	public void close() throws IOException
	{
		_curr.close();
	}

}
