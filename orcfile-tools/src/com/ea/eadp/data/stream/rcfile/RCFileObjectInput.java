package com.ea.eadp.data.stream.rcfile;

import java.io.IOException;
import java.net.URI;
/*import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;*/
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFileRecordReader;
import org.apache.hadoop.hive.ql.io.orc.Writables;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStruct;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStructBase;
import org.apache.hadoop.hive.serde2.columnar.LazyBinaryColumnarStruct;
import org.apache.hadoop.hive.serde2.lazy.LazyObjectBase;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryObject;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileSplit;

import com.ea.eadp.data.hive.objectinspector.LazyObjectUtils;
import com.ea.eadp.data.hive.objectinspector.ObjectInspectorUtils;
import com.ea.eadp.data.hive.objectinspector.ColumnarProperties;
import com.ea.eadp.data.orc.common.Confs;
import com.ea.eadp.data.stream.ObjectInput;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;


/*
 * Xiaowan: 
 * 
 * see Hive 0.12.0 RELEASE_NOTES.txt !!!
 * 
 * 	[HIVE-4475] - Switch RCFile default to LazyBinaryColumnarSerDe
 */

public class RCFileObjectInput implements ObjectInput
{
	private final RCFileRecordReader<LongWritable, BytesRefArrayWritable> _reader;
	private long _count;
	private long _limit;
	private final ColumnarProperties _cp;
	private final boolean _useBinarySerDe;
	private final StructObjectInspector _oi;
	private final ColumnarStructBase _lazyStruct;
	
	transient private LongWritable _key = new LongWritable();
	transient private BytesRefArrayWritable _value = new BytesRefArrayWritable();
	
/*	private static CharsetDecoder _decoder;
	static {
		_decoder = Charset.forName("UTF-8").newDecoder()
				.onMalformedInput(CodingErrorAction.REPLACE)
				.onUnmappableCharacter(CodingErrorAction.REPLACE);
	}*/
	
	protected RCFileObjectInput(URI rcfile, ObjectInspector oi, ColumnarProperties cp) 
			throws IOException
	{
		_oi = (StructObjectInspector) oi;
		Configuration conf = Confs.getHadoopConf();
		final ArrayList<Integer> notSkipIds = new ArrayList<>();
		
		_useBinarySerDe = cp.usingBinarySerDe();
		if (_useBinarySerDe) {
			_lazyStruct = new LazyBinaryColumnarStruct(_oi, notSkipIds);
		}
		else {
			// ObjectInspectorUtils.getDefaultSerDeParameters().getNullSequence();
			Text nullSequence;
			try {
				nullSequence = cp.getSerDeParameters().getNullSequence();
			}
			catch (SerDeException e) {
				throw new IOException("cannot initialize columnar serde parameters", e);
			}
			_lazyStruct = new ColumnarStruct(_oi, notSkipIds, nullSequence);
		}
		
		Path path = new Path(rcfile);
		long length = HadoopCommon.getFileStatusRepr(rcfile).length;
		FileSplit split = new FileSplit(path, 0, length, (String[])null);
		_reader = new RCFileRecordReader<>(conf, split);
		_count = 0;
		_limit = -1;
		
		_cp = cp;
		
		System.err.println("[INFO] reading RCfile: " + rcfile.toString() 
							+ " (" + _cp.toString() + ")");
//		System.err.println("[INFO] RCFile input object inspector: " + ObjectInspectorUtils.describeObjectInspector(_oi));
//		System.err.println("[INFO] RCFile input row format: " + _rowFormat.toString());
	}
	
	
	public RCFileObjectInput(URI rcfile, String schema, ColumnarProperties rowFormat) throws IOException 
	{
		this(rcfile, ObjectInspectorUtils.getColumnarStructObjectInspector(schema, rowFormat), rowFormat);
	}
	
	public RCFileObjectInput setLimit(long limit) 
	{
		_limit = limit; 
		return this;
	}

	public String getSchema() 
	{
		return _oi.getTypeName();
	}
	
	@Override
	public Object read() throws IOException
	{
		if (_limit > 0 && _count >= _limit) {
			return null;
		}
		
		if (_reader.next(_key, _value)) {
			_lazyStruct.init(_value);
			int n = _value.size();
			Object[] row = new Object[n];
			for (int i=0; i<n; i++)  {
				Object lazyOrWritable = _lazyStruct.getField(i);
				if (lazyOrWritable == null) {
					row[i] = null;
				}
				else if (lazyOrWritable instanceof Writable) {
					row[i] = Writables.getValueObject((Writable) lazyOrWritable);
				}
				else if (lazyOrWritable instanceof LazyBinaryObject<?>) {
					row[i] = LazyObjectUtils.lazyBinaryToPOJO((LazyBinaryObject<?>) lazyOrWritable);
				}
				else {
					row[i] = LazyObjectUtils.lazyToPOJO((LazyObjectBase) lazyOrWritable);
				}
			}
			_count++;
			return row;
		}
		else {
			return null;
		}
	}

	@Override
	public long getCount()
	{
		return _count;
	}

	@Override
	public void close() throws IOException
	{
		_reader.close();
	}
}
