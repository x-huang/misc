package com.ea.eadp.data.stream.rcfile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hive.ql.io.RCFile;
import org.apache.hadoop.io.Text;

import com.ea.eadp.data.common.uri.GeneralUriQuery;
import com.ea.eadp.data.hive.objectinspector.ColumnarProperties;
import com.ea.eadp.data.orc.common.Commons;
import com.ea.eadp.data.stream.DataInputStream;
import com.ea.eadp.data.stream.IResourceOperator;
import com.ea.eadp.data.stream.hadoop.HadoopCommon;

public class RCFileResourceOperator implements IResourceOperator 
{
	@Override
	public InputStream createInputStream(URI uri)
			throws URISyntaxException, IOException
	{
		final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
		final List<URI> rcfiles = new ArrayList<>();
		String location = query.getParameterAsString("uri");
		URI locUri = new URI(location);
		if (HadoopCommon.getFileStatusRepr(locUri).isDir) {
			if (! location.endsWith("/"))
				location += "/";
			for (String file: HadoopCommon.listFiles(locUri)) {
				rcfiles.add(new URI(location + file));
			}
		}
		else {
			rcfiles.add(locUri);
		}
		
		String format = query.getParameterAsString("format");
		if (format == null || format.isEmpty()) {
			format = "json";
		}
		
		String schema = query.getParameterAsString("schema");
		if (schema == null) {
			schema = Commons.readSystemSetting("rcfile.input.schema", null);
			if (schema == null) {
				throw new IllegalArgumentException("RCFile input: missing schema");
			}
		}
		if (! schema.startsWith("struct<")) {
			schema = "struct<" + schema + ">";
			System.err.println("[WARN] schema not in struct<...> format... fixed");
		}
		
		Long limitLong = query.getParameterAsLong("limit");
		final long limit = (limitLong == null? -1: limitLong);
		
		final ColumnarProperties cp = ColumnarProperties.getDefault();
		{
			String itemSep = Commons.readSystemSetting("rcfile.item.separator", "&");
			String kvSep = Commons.readSystemSetting("rcfile.keyvalue.separator", "=");
			String hiveVersion = Commons.readSystemSetting("rcfile.hive.version", "12");
			cp.setItemSeparator(itemSep).setKeyValueSeparator(kvSep)
			  .setHiveVersionForRCFileSerDe(Integer.parseInt(hiveVersion)); //.getSerDeParameters();
		}
		
		query.removeParameters("format", "schema", "uri", "limit");
		if (query.getAllParameters().size() > 0) {
			System.err.println("[WARN] unused query options: " + query.toString());
		}
		
		
		
		if (rcfiles.size() == 0) {
			throw new IllegalArgumentException("no rc files");
		}
		else if	(rcfiles.size() == 1) {
			RCFileObjectInput input = new RCFileObjectInput(rcfiles.get(0), schema, cp);
			if (limit >= 0) {
				input.setLimit(limit);
			}
			return new DataInputStream(input, format);
		}
		else {
			MultipleRCFileObjectInput input = new MultipleRCFileObjectInput(rcfiles, schema, cp);
			if (limit >= 0) {
				input.setLimit(limit);
			}
			return new DataInputStream(input, format);
		}
	}

	@Override
	public OutputStream createOutputStream(URI uri)
	{
		throw new UnsupportedOperationException("writing to rcfile not yet supported");
	}

	private static RCFile.Reader getRCFileReader(URI locUri) throws IOException
	{
		Path path = new Path(locUri);
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(locUri, conf);
		return new RCFile.Reader(fs, path, conf);
	}
	
	@Override
	public Object describeResource(URI uri) throws URISyntaxException,
			IOException
	{
		final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
		final String location = query.getParameterAsString("uri");
		if (location.endsWith("/")) {
			throw new IllegalArgumentException("destination cannot be a directory: " + location);
		}
		
		RCFile.Reader reader = getRCFileReader(new URI(location));
		final Map<String, Object> metadata = new HashMap<>();
		for (Map.Entry<Text, Text> entry: reader.getMetadata().getMetadata().entrySet()) {
			String key = entry.getKey().toString();
			String val = entry.getValue().toString();
			metadata.put(key, val);
		}
	
		final Map<String, Object> repr = new HashMap<>();
		repr.put("file", location);
		repr.put("metadata", metadata);
		
		return repr;
	}
}
