package com.ea.eadp.data.stream.connector;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.ea.eadp.data.stream.DataInputStream;
import com.ea.eadp.data.stream.DataOutputStream;
import com.ea.eadp.data.stream.ObjectInput;
import com.ea.eadp.data.stream.ObjectOutput;
import com.ea.eadp.data.transport.connector.Connector;

public class ObjectDirectConnector extends Connector
{

	public static boolean objectDirectCapable(InputStream in, OutputStream out)
	{
		return (in instanceof DataInputStream) && (out instanceof DataOutputStream);
	}
	
	@Override
	public void connect(InputStream in, OutputStream out) throws IOException
	{
		 if (! objectDirectCapable(in, out)) {
			 String inStreamClz = in.getClass().getSimpleName();
			 String outStreamClz = out.getClass().getSimpleName();
			 throw new IOException("stream pair not capable for object copy: " 
					 				+ inStreamClz + " & " + outStreamClz);
		 }
		 
		 System.err.println("[INFO] starting object direct connector");
		 
		 final ObjectInput oin = ((DataInputStream)in).getObjectInput();
		 final ObjectOutput oout = ((DataOutputStream)out).getObjectOutput();
		 
		 _read = 0;
		_written = 0;
		_startTime = System.currentTimeMillis();
		_endTime = 0;
		
		Object o;
		while ((o = oin.read()) != null) { 
			oout.write(o);
			_read++;
			_written++;
		}
		
		_endTime = System.currentTimeMillis();
	}

	@Override
	public String getShortName() 
	{
		return "objectdirect";
	}
}
