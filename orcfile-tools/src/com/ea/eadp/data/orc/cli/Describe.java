package com.ea.eadp.data.orc.cli;

import java.io.IOException;
import java.net.URISyntaxException;

import com.ea.eadp.data.orc.common.Commons;

public class Describe
{
	public static void main(String[] args) throws IOException, URISyntaxException
	{
		Commons.registerOperators();
		
		com.ea.eadp.data.transport.cli.Describe.main(args);
	}

}
