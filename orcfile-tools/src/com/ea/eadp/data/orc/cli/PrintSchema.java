package com.ea.eadp.data.orc.cli;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.ea.eadp.data.common.lang.StringUtils;
import com.ea.eadp.data.orc.common.Commons;
import com.ea.eadp.data.stream.DataStreamMaker;

public class PrintSchema
{
	public static void main(String[] args) throws IOException, URISyntaxException
	{
		if (args.length != 1) {
			System.err.println("[FAIL] expecting one URI");
			System.err.println("[USAGE] PrintSchema <uri>");
			return;
		}
		
		Commons.registerOperators();
		
		final String uri = args[0];
		final Object metadata = DataStreamMaker.describeResource(uri);
		if (metadata instanceof com.ea.eadp.data.stream.hive.TableRepr) {
			com.ea.eadp.data.stream.hive.TableRepr tbl = (com.ea.eadp.data.stream.hive.TableRepr)metadata;
			List<String> cols = new ArrayList<>();
			for (com.ea.eadp.data.stream.hive.ColumnRepr col: tbl.columns) {
				cols.add(col.name + ":" + col.type);
			}
			/* Xiaowan: since "SCHEMA" is used for rcfile/orc files, so exclude partition columns.
			for (com.ea.eadp.data.stream.hive.ColumnRepr col: tbl.partitionColumns) {
				cols.add(col.name + ":" + col.type);
			}
			*/
			System.out.println("struct<" + StringUtils.join(cols, ",") + ">");
		}
		else if (metadata instanceof com.ea.eadp.data.orc.repr.OrcFilePropertyRepr) {
			com.ea.eadp.data.orc.repr.OrcFilePropertyRepr orc = (com.ea.eadp.data.orc.repr.OrcFilePropertyRepr)metadata;
			String tn = orc.typeName;
			System.out.println(tn);
		}
		else {
			System.err.print("unsupported schema metadata: " + metadata.getClass().getName());
		}
	}

}
