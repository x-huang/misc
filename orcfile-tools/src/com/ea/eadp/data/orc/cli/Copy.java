package com.ea.eadp.data.orc.cli;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.ea.eadp.data.orc.common.Commons;
import com.ea.eadp.data.transport.TransportService;

public class Copy
{
	public static void main(String[] args) throws Exception 
	{
		if (args.length != 0 && args.length != 2) {
			System.err.println("[FAIL] expceting two URIs or a task list from STDIN");
			System.err.println("[USAGE] Copy <src_uri> <dest_uri>");
			return;
		}
		
		Commons.registerOperators();
		
		final TransportService service = new TransportService();
		service.setConsoleLogger(); // .setRetrier(3);

		final String CONNECTOR = "objectdirect";
		
		if (args.length == 2) {
			service.submitTask(args[0], args[1], CONNECTOR, null);
		}
		else {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String line = br.readLine();
				if (line == null)
					break;
				String[] splitted = line.split("[\\s+]");
				if (splitted.length == 0)	// empty line
					continue;
				
				String src = splitted[0];
				if (src.isEmpty() || src.startsWith("#")) 	// consider it comment
					continue;
				
				if (splitted.length == 1) {
					throw new IllegalArgumentException("bad URIs: " + line);
				}
	
				String dest = splitted[1];
				String connector = (splitted.length == 2)? CONNECTOR: splitted[2];
				if (! connector.equals(CONNECTOR)) {
					throw new IllegalArgumentException("bad connector: " + connector);
				}
				
				service.submitTask(src, dest, connector, null);
			}
		}
		
		service.shutdown();
		service.awaitTermination();
		System.err.println("[INFO] all jobs finished.");
	}
}
