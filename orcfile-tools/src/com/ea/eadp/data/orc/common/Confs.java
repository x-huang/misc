package com.ea.eadp.data.orc.common;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.conf.HiveConf;

public class Confs
{
	private static Configuration defaultHadoopConf = null;
	private static HiveConf defaultHiveConf = null;
	
	public static Configuration getHadoopConf()
	{
		if (defaultHadoopConf == null) {
			defaultHadoopConf = new Configuration();
		}
		return defaultHadoopConf;
	}
	
	public static HiveConf getHiveConf()
	{
		if (defaultHiveConf == null) {
			defaultHiveConf = new HiveConf();
		}
		return defaultHiveConf;
	}
}
