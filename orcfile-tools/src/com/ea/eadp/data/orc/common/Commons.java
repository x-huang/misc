package com.ea.eadp.data.orc.common;

import com.ea.eadp.data.stream.Protocols;
import com.ea.eadp.data.stream.connector.ObjectDirectConnector;
import com.ea.eadp.data.stream.orc.OrcFileResourceOperator;
import com.ea.eadp.data.stream.rcfile.RCFileResourceOperator;
import com.ea.eadp.data.transport.connector.Connectors;

public class Commons
{
	public static String toSystemEnvName(String property)
	{
		return property.replace('.', '_').toUpperCase();
	}
	
	public static String readSystemSetting(String property, String env, String def)
	{
		String value = System.getProperty(property);
		if (value != null) {
			return value;
		}
		value = System.getenv(env);
		if (value != null) {
			return value;
		}
		return def;
	}
	
	public static String readSystemSetting(String property, String def) 
	{
		return readSystemSetting(property, toSystemEnvName(property), def);
	}
	
	public static void registerOperators()
	{
		Protocols.registerOperatorClass("orcfile", OrcFileResourceOperator.class);
		Protocols.registerOperatorClass("orc", OrcFileResourceOperator.class);
		Protocols.registerOperatorClass("rcfile", RCFileResourceOperator.class);
		
		Connectors.registerConnectorClass("objectdirect", ObjectDirectConnector.class);
	}
}
