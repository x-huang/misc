package com.ea.eadp.data.orc.repr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.hive.ql.io.orc.ColumnStatistics;
import org.apache.hadoop.hive.ql.io.orc.Reader;
import org.apache.hadoop.hive.ql.io.orc.StripeInformation;

@XmlRootElement
public class OrcFilePropertyRepr implements Serializable
{
	private static final long serialVersionUID = 4493248686425092003L;
	
	public final long rows;
	public final long contentLength;
	public int rowIndexStride;
	public final String compression;
	public final int compressionSize;
	public final String inspectorCategory;
	public final String typeName;
	public final List<String> metadataKeys;
	public final List<OrcFileColumnStatisticRepr> columnStatistics;
	public final List<OrcFileStripeRepr> stripes;
	public final Map<String, String> metadata;
	
	public OrcFilePropertyRepr(Reader reader) {
		rows = reader.getNumberOfRows();
		contentLength = reader.getContentLength();
		rowIndexStride = reader.getRowIndexStride();
		compression = reader.getCompression().toString();
		compressionSize = reader.getCompressionSize();
		inspectorCategory = reader.getObjectInspector().getCategory().toString();
		typeName = reader.getObjectInspector().getTypeName();
		metadataKeys = new ArrayList<>();
		for (String key: reader.getMetadataKeys()) {
			metadataKeys.add(key);
		}
		columnStatistics = new ArrayList<>();
		ColumnStatistics[] stats = reader.getStatistics();
		for (int i=0; i<stats.length; i++) {
			columnStatistics.add(new OrcFileColumnStatisticRepr(stats[i]));
		}
		
		stripes = new ArrayList<>();
		for (StripeInformation stripe: reader.getStripes()) {
			stripes.add(new OrcFileStripeRepr(stripe));
		}
		
		metadata = new HashMap<>();
		for (String key: reader.getMetadataKeys()) {
			String val = reader.getMetadataValue(key).toString();
			metadata.put(key, val);
		}
	}
}
