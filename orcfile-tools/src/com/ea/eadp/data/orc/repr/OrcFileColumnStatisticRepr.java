package com.ea.eadp.data.orc.repr;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.hive.ql.io.orc.ColumnStatistics;

@XmlRootElement
public class OrcFileColumnStatisticRepr implements Serializable
{
	private static final long serialVersionUID = 7888179701511301860L;
	
	public final String descr;
	
	public OrcFileColumnStatisticRepr(ColumnStatistics stat) 
	{
		descr = stat.toString();
	}
}
