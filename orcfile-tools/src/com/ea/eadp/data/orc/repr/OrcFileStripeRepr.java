package com.ea.eadp.data.orc.repr;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.hive.ql.io.orc.StripeInformation;

@XmlRootElement
public class OrcFileStripeRepr implements Serializable
{
	private static final long serialVersionUID = -6843769108985314172L;
	
	public final long offset;
	public final long data;
	public final long rows;
	public final long index;

	public OrcFileStripeRepr(StripeInformation stripe) {
		offset = stripe.getOffset();
		data = stripe.getDataLength();
		rows = stripe.getNumberOfRows();
		index = stripe.getIndexLength();
	}
}
