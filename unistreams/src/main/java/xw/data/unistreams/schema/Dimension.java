package xw.data.unistreams.schema;

import java.util.List;

/**
 * Created by xhuang on 10/14/2014.
 */
public class Dimension
{
    protected final int[] indices;

    private Dimension(int length) {
        this.indices = new int[length];
    }

    public Dimension(int[] indices) {
        this.indices = new int[indices.length];
        System.arraycopy(indices, 0, this.indices, 0, indices.length);
    }

    public Dimension(List<Integer> list) {
        this.indices = new int[list.size()];
        for (int i=0; i<list.size(); i++) {
            this.indices[i] = list.get(i);
        }
    }

    public static Dimension fromColumnLists(List<String> cols, List<String> select) {
        final int[] indices = new int[select.size()];
        for (int i=0; i<select.size(); i++) {
            int idx = cols.indexOf(select.get(i));
            if (idx < 0) {
                throw new IllegalArgumentException("column '" + select.get(i) + "' doesn't exist in list");
            }
            indices[i] = idx;
        }
        return new Dimension(indices);
    }

    public static Dimension of(int... indices) {
        return new Dimension(indices);
    }

    public int[] getIndices() {
        return indices;
    }

    public int length() {
        return indices.length;
    }

    public Dimension getSubDimension(int n) {
        if (n > indices.length) {
            throw new IllegalArgumentException("sub dimension size must be smaller or equals to current");
        }
        final Dimension dimen = new Dimension(n);
        System.arraycopy(indices, 0, dimen.indices, 0, n);

        return dimen;
    }

    public Dimension append(Dimension that) {
        final Dimension dimen = new Dimension(this.length() + that.length());
        System.arraycopy(this.indices, 0, dimen.indices, 0, this.indices.length);
        System.arraycopy(that.indices, 0, dimen.indices, this.indices.length, that.indices.length);
        return dimen;
    }

    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Dimension)) {
            return false;
        }
        final Dimension that = (Dimension) o;
        if (this.indices.length != that.indices.length) {
            return false;
        }
        for (int i=0; i<indices.length; i++) {
            if (indices[i] != that.indices[i]) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int index : indices) {
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append(index);
        }
        return sb.toString();
    }

}
