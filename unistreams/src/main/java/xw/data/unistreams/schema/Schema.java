package xw.data.unistreams.schema;

import org.apache.hadoop.hive.serde2.typeinfo.StructTypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 6/22/16.
 */
final public class Schema
{
    private final List<String> columns;
    private final List<TypeInfo> types;
    private final Map<String, Integer> columnIndices;

    private Schema() {
        this.columns = new ArrayList<>();
        this.types = new ArrayList<>();
        this.columnIndices = new HashMap<>();
    }

    private Schema addColumn(String col, TypeInfo type) {
        if (columnIndices.containsKey(col)) {
            throw new IllegalArgumentException("redefined column name: " + col);
        }
        columns.add(col);
        types.add(type);
        columnIndices.put(col, columns.size() - 1);
        return this;
    }

    public static class Builder {
        private Schema schema = new Schema();

        public Builder addColumn(String col, TypeInfo type) {
            schema.addColumn(col, type);
            return this;
        }

        public Schema build() {
            return schema;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Schema fromString(String s) {
        if (! s.startsWith("struct<")) {
            s = "struct<" + s + ">";
        }
        return fromStructType( (StructTypeInfo) TypeUtils.parse(s));
    }

    public static Schema fromStructType(StructTypeInfo sti) {
        final Schema schema = new Schema();
        for (String field: sti.getAllStructFieldNames()) {
            schema.addColumn(field, sti.getStructFieldTypeInfo(field));
        }
        return schema;
    }

    public int size() {
        return columns.size();
    }

    public int indexOf(String col) {
        return columnIndices.get(col);
    }

    public TypeInfo typeOf(String col) {
        return types.get(indexOf(col));
    }

    public String columnAt(int idx) {
        return columns.get(idx);
    }

    public TypeInfo typeAt(int idx) {
        return types.get(idx);
    }

    public boolean hasColumn(String col) {
        return columnIndices.containsKey(col);
    }

    public Schema projectionOf(List<String> cols) {
        final Schema projection = new Schema();
        for (String col: cols) {
            projection.addColumn(col, typeOf(col));
        }
        return projection;
    }

    public Schema projectionOf(Dimension dimension) {
        final Schema projection = new Schema();
        for (int idx: dimension.getIndices()) {
            projection.addColumn(columnAt(idx), typeAt(idx));
        }
        return projection;
    }

    public Dimension dimensionOf(List<String> cols) {
        final List<Integer> list = new ArrayList<>();
        for (String col: cols) {
            if (! columnIndices.containsKey(col)) {
                throw new IllegalArgumentException("no such column: " + col);
            }
            list.add(columnIndices.get(col));
        }
        return new Dimension(list);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (int i=0; i<columns.size(); i++) {
            final String col = columns.get(i);
            final TypeInfo type = types.get(i);
            sb.append(delimiter).append(col).append(':').append(type.toString());
            delimiter = ",";
        }
        return sb.toString();
    }

    public StructTypeInfo toStructTypeInfo() {
        return (StructTypeInfo) TypeInfoFactory.getStructTypeInfo(columns, types);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Schema)) {
            return false;
        }

        final Schema that = (Schema) o;
        return size() == that.size() && this.toString().equals(that.toString());
    }

    public boolean semanticallyEquals(Object o) {
        if (!(o instanceof Schema)) {
            return false;
        }

        final Schema that = (Schema) o;
        if (size() != that.size()) {
            return false;
        }

        for (int i=0; i<size(); i++) {
            TypeInfo t1 = this.typeAt(i);
            TypeInfo t2 = that.typeAt(i);
            if (! TypeUtils.semanticallyEqual(t1, t2)) {
                return false;
            }
        }
        return true;
    }

    public static Schema join(Schema schema1, Schema schema2) {
        if (schema1 == null || schema2 == null) {
            throw new NullPointerException("joining null schema");
        }

        final Schema joined = new Schema();
        for (String col: schema1.columns) {
            joined.addColumn(col, schema1.typeOf(col));
        }
        for (String col: schema2.columns) {
            joined.addColumn(col, schema2.typeOf(col));
        }
        return joined;
    }
}
