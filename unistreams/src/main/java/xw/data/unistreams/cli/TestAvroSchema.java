package xw.data.unistreams.cli;

import org.apache.hadoop.hive.serde2.typeinfo.StructTypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
import xw.data.unistreams.resources.avro.AvroSchemaUtils;

/**
 * Created by xhuang on 7/8/16.
 */
public class TestAvroSchema
{
    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("expect one type string");
        }
        final String s;
        if (! args[0].startsWith("struct<")) {
            s = "struct<" + args[0] + ">";
        } else {
            s = args[0];
        }

        final StructTypeInfo sti = (StructTypeInfo) TypeInfoUtils.getTypeInfoFromTypeString(s);
        System.out.println("parsed: " + sti);
        System.out.println("avro schema: " + AvroSchemaUtils.fromStructTypeInfo(sti));
    }
}
