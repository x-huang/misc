package xw.data.unistreams.cli;

import xw.data.unistreams.resources.ResourceDescriptor;

/**
 * Created by xhuang on 6/1/16.
 */
public class TestRD
{
    public static void main(String[] args) {
        final ResourceDescriptor rd = ResourceDescriptor.parse(args[0]);
        System.out.println("env: " + rd.env);
        System.out.println("prefix: " + rd.prefix);
        System.out.println("uris: " + rd.uris);
        System.out.println("params: " + rd.params);
        System.out.println(rd.toString());
    }
}
/*
$ java -Dfilename=my-data -Dformat=tsv xw.data.unistreams.cli.TestRD '${sys:format}:(file://${env:HOME}/${sys:filename}-${version}.${sys:format})?version=1.0'
tsv:(file:///home/hadoop/my-data-1.0.tsv)?version=1.0
* */
