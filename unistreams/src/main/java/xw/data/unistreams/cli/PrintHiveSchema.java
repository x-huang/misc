package xw.data.unistreams.cli;

import xw.data.unistreams.common.args.OptParser;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.hive.HiveExt;
import xw.data.unistreams.resources.hive.repr.TableRepr;

/**
 * Created by xhuang on 5/27/16.
 */
public class PrintHiveSchema
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PrintHiveSchema.class);

    public static void main(String[] args) throws Exception {

        final OptParser opt = OptParser.create().set("-names-only", false)
                .set("-with-part-cols", false).setArgsSize(1)
                .parse(args, System.err);

        final boolean namesOnly = opt.getOption("-names-only").equals(true);
        final boolean withPartCols = opt.getOption("-with-part-cols").equals(true);
        final ResourceDescriptor rd = ResourceDescriptor.parse(opt.getArgument(0));
        rd.assertUriSizeEq(1);
        logger.debug("parsed resource descriptor: {}", rd);

        final TableRepr tbl = HiveExt.ofEnv(rd.env).getTableRepr(rd.getSingleUri());
        System.out.println(namesOnly? tbl.getColsString(withPartCols) : tbl.getTypeString(withPartCols));
    }
}
