package xw.data.unistreams.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.connector.ConnectionStats;
import xw.data.unistreams.connector.Connectors;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.resources.ResourceDescriptor;

/**
 * Created by xhuang on 5/24/16.
 */
public class Copy
{
    private static final Logger logger = LoggerFactory.getLogger(Copy.class);

    public static void main(String[] args) throws Exception {

        if (args.length != 1 && args.length != 2) {
            throw new IllegalArgumentException("expecting one or two resource descriptors");
        }

        final ResourceDescriptor src = ResourceDescriptor.parse(args[0]);
        logger.debug("parsed src resource descriptor: {}", src);

        final ResourceDescriptor dest;
        if (args.length == 2 && ! args[1].equals("-")) {
            dest = ResourceDescriptor.parse(args[1]);
        } else {
            dest = ResourceDescriptor.parse("text:(stdout)?fieldsep=\\t");
        }
        logger.debug("parsed dest resource descriptor: {}", dest);

        final DataSource in = src.source();
        final DataSink out = dest.sink();
        final ConnectionStats stats= Connectors.newBuilder().create().connect(in, out);
        in.close();
        out.close();
        logger.debug("read/written {} records", stats.writes);
    }
}
