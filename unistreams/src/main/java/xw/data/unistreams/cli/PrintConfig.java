package xw.data.unistreams.cli;

import org.apache.hadoop.conf.Configuration;
import xw.data.unistreams.env.Environment;

import java.util.Map;

public class PrintConfig {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PrintConfig.class);

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("[usage] java PrintConfig [env] [hadoop/hive]");
            throw new IllegalArgumentException("CLI argument error");
        }

        final Environment env = Environment.of(args[0]);
        logger.debug("initialized env alias: {}", Environment.alias());
        logger.debug("loaded env: {}", Environment.loaded());

        final String service = args[1];
        final Configuration config;
        switch (service) {
            case "hadoop":
                config = env.getHadoopConf();
                break;
            case "hive":
                config = env.getHiveConf();
                break;
            default:
                logger.error("unknown service: {}", service);
                throw new IllegalArgumentException("unknown service: " + service);
        }

        for (Map.Entry<String, String> entry: config) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }

        logger.info("{} configuration entries", config.size());
    }
}
