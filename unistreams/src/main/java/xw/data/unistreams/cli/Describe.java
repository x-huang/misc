package xw.data.unistreams.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;

/**
 * Created by xhuang on 5/26/16.
 */
public class Describe
{
    private static final Logger logger = LoggerFactory.getLogger(Describe.class);

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            throw new IllegalArgumentException("expecting exactly one resource descriptor");
        }

        final ResourceDescriptor rd = ResourceDescriptor.parse(args[0]);
        logger.debug("parsed resource descriptor: {}", rd);

        final ResourceMetadata metadata = rd.op().metadata();
        logger.info("metadata POJO: {}", metadata.getPOJO());
        System.out.println(metadata.getSchema());

    }
}
