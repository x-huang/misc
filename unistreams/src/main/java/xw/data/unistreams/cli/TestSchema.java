package xw.data.unistreams.cli;

import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 6/22/16.
 */
public class TestSchema
{
    public static void main(String[] args) throws Exception {
        if (args.length != 1 && args.length != 2) {
            throw new IllegalArgumentException("expect one or two schema string");
        }

        if (args.length == 1) {
            final Schema schema = Schema.fromString(args[0]);
            System.out.println("parsed: " + schema.toString());
        } else {
            final Schema schema1 = Schema.fromString(args[0]);
            final Schema schema2 = Schema.fromString(args[1]);
            System.out.println("parsed #1: " + schema1.toString());
            System.out.println("parsed #2: " + schema2.toString());
            System.out.println("strictly equal: " + schema1.equals(schema2));
            System.out.println("semantically equal: " + schema1.semanticallyEquals(schema2));
        }
    }
}


/*
$ java xw.data.unistreams.cli.TestSchema id:string,number:int
parsed: id:string,number:int

$ java xw.data.unistreams.cli.TestSchema id:string,number:int name:string,value:int
parsed #1: id:string,number:int
parsed #2: name:string,value:int
strictly equal: false
semantically equal: true

 */