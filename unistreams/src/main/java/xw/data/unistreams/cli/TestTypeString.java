package xw.data.unistreams.cli;

import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import xw.data.unistreams.schema.TypeUtils;

/**
 * Created by xhuang on 5/27/16.
 */
public class TestTypeString
{
    public static void main(String[] args) throws Exception {
        if (args.length != 1 && args.length != 2) {
            throw new IllegalArgumentException("expect one or two type string");
        }

        if (args.length == 1) {
            final TypeInfo type = TypeUtils.parse(args[0]);
            System.out.println("parsed: " + type.toString());
        } else  {
            final TypeInfo ty1 = TypeUtils.parse(args[0]);
            final TypeInfo ty2 = TypeUtils.parse(args[1]);
            System.out.println("parsed #1: " + ty1.toString());
            System.out.println("parsed #2: " + ty2.toString());
            System.out.println("strictly equal: " + TypeUtils.strictlyEqual(ty1, ty2));
            System.out.println("semantically equal: " + TypeUtils.semanticallyEqual(ty1, ty2));
        }
    }
}
