package xw.data.unistreams.connector;

import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;

import java.io.IOException;

/**
 * Created by xhuang on 6/3/16.
 */
public interface Connector
{
    ConnectionStats connect(DataSource in, DataSink out) throws IOException;
}
