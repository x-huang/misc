package xw.data.unistreams.connector;

/**
 * Created by xhuang on 9/21/16.
 */
public class ConnectionStats
{
    public final long reads;
    public final long writes;

    public ConnectionStats(long reads, long writes) {
        this.reads = reads;
        this.writes = writes;
    }
}
