package xw.data.unistreams.connector;

import xw.data.unistreams.stream.*;

import java.io.IOException;

/**
 * Created by xhuang on 6/3/16.
 */
public class Connectors
{
    private static void prepare(final DataSource in,
                                final DataSink out) throws IOException {
        in.initialize();
        if (in instanceof DataStream && out instanceof DataStream) {
            ((DataStream) out).context().copy(((DataStream) in).context());
        }
        out.initialize();
    }

    public static ConnectionStats doConnect(final DataSource in,
                                 final DataSink out) throws IOException {
        prepare(in, out);
        long count = 0;
        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            out.write(record);
            count++;
        }
        return new ConnectionStats(count, count);
    }

    public static ConnectionStats doConnect(final DataSource in,
                               final DataSink out,
                               final RecordPredicate filter) throws IOException {
        prepare(in, out);
        long reads = 0;
        long writes = 0;
        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            reads++;
            if (filter.eval(record)) {
                out.write(record);
                writes++;
            }
        }
        return new ConnectionStats(reads, writes);
    }

    public static ConnectionStats doConnect(final DataSource in, final DataSink out,
                                            final RecordProcessor editor) throws IOException {
        prepare(in, out);
        long count = 0;
        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            out.write(editor.process(record));
            count++;
        }
        return new ConnectionStats(count, count);
    }

    public static ConnectionStats doConnect(final DataSource in, final DataSink out,
                                            final RecordPredicate filter,
                                            final RecordProcessor editor) throws IOException {
        prepare(in, out);
        long reads = 0;
        long writes = 0;
        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            reads++;
            if (filter.eval(record)) {
                out.write(editor.process(record));
                writes++;
            }
        }
        return new ConnectionStats(reads, writes);
    }

    public static final Connector simple = new Connector() {
        @Override
        public ConnectionStats connect(DataSource in, DataSink out) throws IOException {
            return doConnect(in, out);
        }
    };



    public static final Connector dryrun = new Connector() {
        @Override
        public ConnectionStats connect(DataSource in, DataSink out) throws IOException {
            prepare(in, out);
            return new ConnectionStats(0L, 0L);
        }
    };

    public static class Builder {
        private RecordPredicate filter = null;
        private RecordProcessor editor = null;

        public Builder setFilter(RecordPredicate filter) {
            this.filter = filter;
            return this;
        }

        public Builder setEditer(RecordProcessor editor) {
            this.editor = editor;
            return this;
        }

        public Connector create() {
            if (filter == null && editor == null) {
                return simple;
            } else if (filter == null) {
                return new Connector() {
                    @Override
                    public ConnectionStats connect(DataSource in, DataSink out) throws IOException {
                        return doConnect(in, out, editor);
                    }
                };
            } else if (editor == null) {
                return new Connector() {
                    @Override
                    public ConnectionStats connect(DataSource in, DataSink out) throws IOException {
                        return doConnect(in, out, filter);
                    }
                };
            } else {
                return new Connector() {
                    @Override
                    public ConnectionStats connect(DataSource in, DataSink out) throws IOException {
                        return doConnect(in, out, filter, editor);
                    }
                };
            }
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
