package xw.data.unistreams.stream;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by xhuang on 3/18/16.
 */
public class DataStreams
{
    public static DataSource wrapRecordListAsInput(final List<Object[]> records) {
        return new DataSource() {
            private int index = 0;

            @Override
            public void initialize() throws IOException {

            }

            @Override
            public Object[] read() throws IOException {
                return index<records.size()? records.get(index++): null;
            }

            @Override
            public void close() throws IOException {
            }
        };
    }

    public static DataSink wrapRecordListAsOutput(final List<Object[]> records) {
        return new DataSink() {

            @Override
            public void initialize() throws IOException {

            }

            @Override
            public void write(Object[] record) throws IOException {
                records.add(record);
            }

            @Override
            public void close() throws IOException {
            }
        };
    }

    private static final DataSource DUMMY_EMTPY_DATA_SOURCE = new DataSource() {
        @Override
        public void initialize() throws IOException {
        }

        @Override
        public Object[] read() throws IOException {
            return null;
        }

        @Override
        public void close() throws IOException {
        }

        @Override
        public String toString() {
            return "dummy:empty";
        }
    };

    public static DataSource getDummyEmpty() {
        return DUMMY_EMTPY_DATA_SOURCE;
    }

    public static DataSource getRepeatedRows(final Object[] row, final int count) {
        return new DataSource() {
            private int i;

            @Override
            public void initialize() throws IOException {
                i = 0;
            }

            @Override
            public Object[] read() throws IOException {
                if (i == count) {
                    return null;
                } else {
                    i += 1;
                    return row;
                }
            }

            @Override
            public void close() throws IOException {
            }

            @Override
            public String toString() {
                return "repeat(row," + count + ")";
            }
        };
    }

    public static DataSource wrapRecordList(final List<Object[]> records) {
        return new DataSource() {

            private int index = 0;

            @Override
            public void initialize() throws IOException {
                index = 0;
            }

            @Override
            public Object[] read() throws IOException {
                return index<records.size()? records.get(index++): null;
            }

            @Override
            public void close() throws IOException {
            }

            @Override
            public String toString() {
                return "wrap(list)";
            }
        };
    }

    public static DataSource transform(final DataSource in, final RecordProcessor processor) {
        return new DataSource() {
            @Override
            public void initialize() throws IOException {
                in.initialize();
            }

            @Override
            public Object[] read() throws IOException {
                final Object[] row = in.read();
                return row == null? null: processor.process(row);
            }

            @Override
            public void close() throws IOException {
                in.close();
            }

            @Override
            public String toString() {
                return "transform(" + in.toString() + ")";
            }
        };
    }

    public static DataSource filter(final DataSource in, final RecordPredicate filter) {
        return new DataSource() {
            @Override
            public void initialize() throws IOException {
                in.initialize();
            }

            @Override
            public Object[] read() throws IOException {
                while (true) {
                    final Object[] row = in.read();
                    if (row == null) {
                        return null;
                    } else if (filter.eval(row)) {
                        return row;
                    }
                }
            }

            @Override
            public void close() throws IOException {
                in.close();
            }

            @Override
            public String toString() {
                return "filter(" + in.toString() + ")";
            }
        };
    }

    public static DataSource concatenate(final List<DataSource> streams) {
        return new DataSource() {
            private int no;
            private DataSource in;

            @Override
            public void initialize() throws IOException {
                no = 0;
                in = streams.get(0);
                in.initialize();
            }

            @Override
            public Object[] read() throws IOException {
                while (in != null) {
                    final Object[] row = in.read();
                    if (row != null) {
                        return row;
                    } else {
                        no += 1;
                        in.close();
                        in = no < streams.size()? streams.get(no): null;
                        if (in != null)
                            in.initialize();
                    }
                }
                return null;
            }

            @Override
            public void close() throws IOException {
            }

            @Override
            public String toString() {
                String s ="concatenate(";
                String delim = "";
                for (DataSource in: streams) {
                    s += delim + in.toString();
                    delim = ",";
                }
                s += ")";
                return s;
            }
        };
    }

    public static DataSource slice(final DataSource in, final long limit, final long offset) {
        return new DataSource() {
            long count = 0;
            @Override
            public void initialize() throws IOException {
                in.initialize();
                for (int i=0; i<offset; i++) {
                    // discard *offset* rows;
                    final Object[] row = in.read();
                    if (row == null) {
                        count = limit;
                        break;
                    }
                }
            }

            @Override
            public Object[] read() throws IOException {
                if (count < limit) {
                    count += 1;
                    return in.read();
                } else {
                    return null;
                }
            }

            @Override
            public void close() throws IOException {
                in.close();
            }

            @Override
            public String toString() {
                return "slice(" + in.toString() + ")";
            }
        };
    }

    public static DataSource sort(final DataSource in, final int[] indices, final boolean[] ascendings){

        if (indices.length != ascendings.length) {
            throw new IllegalArgumentException("mismatch length of indices and ascending flags: "
                    + indices.length + " vs. " + ascendings.length);
        }

        return new DataSource() {
            final int ordersLen = indices.length;
            final List<Object[]> rows = new ArrayList<>();
            int index = 0;

            @Override
            public void initialize() throws IOException {
                in.initialize();
                while (true) {
                    final Object[] row = in.read();
                    if (row == null)
                        break;
                    rows.add(row);
                }
                final Comparator<Object[]> comparator = new Comparator<Object[]>() {
                    @Override
                    public int compare(Object[] r1, Object[] r2) {
                        for (int i=0; i<ordersLen; i++) {
                            final int index = indices[i];
                            final Object o1 = r1[index];
                            final Object o2 = r2[index];
                            final int cmp;
                            if (o1 == null) {
                                cmp = o2 == null? 0: -1;
                            } else {
                                //noinspection unchecked
                                cmp = o2 == null? 1: ((Comparable) o1).compareTo(o2);
                            }
                            if (cmp != 0) {
                                return ascendings[i]? cmp: -cmp;
                            }
                        }
                        return 0;
                    }
                };
                Collections.sort(rows, comparator);
            }

            @Override
            public Object[] read() throws IOException {
                return index<rows.size()? rows.get(index++): null;
            }

            @Override
            public void close() throws IOException {
                in.close();
            }

            @Override
            public String toString() {
                return "sort(" + in.toString() + ")";
            }
        };
    }

    private static boolean valuesEqual(Object v1, Object v2) {
        return v1 == null && v2 == null || !(v1 == null || v2 == null) && v1.equals(v2);
    }

    public static boolean equal(final DataSource in1, final DataSource in2) throws IOException {
        while (true) {
            final Object[] row1 = in1.read();
            final Object[] row2 = in2.read();
            if (row1 == null && row2 == null) {
                return true;
            } else if (row1 == null || row2 == null) {
                return false;
            } else if (row1.length  != row2.length) {
                return false;
            } else {
                for (int i=0; i<row1.length; i++) {
                    if (! valuesEqual(row1[i], row2[i])) {
                        return false;
                    }
                }
            }
        }
    }

    public static void dump(final DataSource in, PrintStream ps) throws IOException {
        while (true) {
            final Object[] row = in.read();
            if (row == null) {
                break;
            }
            String delim = "";
            for (Object o: row) {
                ps.append(delim).append(o == null? "null": o.toString());
                delim = "\t";
            }
            ps.println();
        }
    }

    public static void dumpJavaTypes(final DataSource in, PrintStream ps) throws IOException {
        while (true) {
            final Object[] row = in.read();
            if (row == null) {
                break;
            }
            String delim = "";
            for (Object o: row) {
                final Class clz = o == null? Void.class: o.getClass();
                ps.append(delim).append(clz.getName());
                delim = "\t";
            }
            ps.println();
        }
    }
}
