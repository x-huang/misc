package xw.data.unistreams.stream;

/**
 * Created by xhuang on 8/22/16.
 */
public interface RecordProcessor
{
    Object[] process(Object[] record);
}
