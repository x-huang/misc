package xw.data.unistreams.stream;

import xw.data.unistreams.resources.ResourceDescriptor;

import java.io.IOException;
import java.util.Iterator;

public class MultipleDataSource extends DataStreamBase implements DataSource
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MultipleDataSource.class);

    interface Opener {
        Iterator<DataSource> openMultipleSource(ResourceDescriptor rd);
    }

    private final static Opener defaultOpener = new Opener() {

        @Override
        public Iterator<DataSource> openMultipleSource(final ResourceDescriptor rd) {
            return new Iterator<DataSource>() {
                private int index = 0;

                @Override
                public boolean hasNext() {
                    return index < rd.uris.size();
                }

                @Override
                public DataSource next() {
                    return rd.singleUriRd(index++).source();
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("cannot remove data source in iterator");
                }
            };
        }
    };

    private final Iterator<DataSource> iter;
    private DataSource curr = null;

    public MultipleDataSource(ResourceDescriptor rd, Opener opener) {
        super(rd);
        this.iter = opener.openMultipleSource(rd);
    }

    public MultipleDataSource(ResourceDescriptor rd) {
        super(rd);
        this.iter = defaultOpener.openMultipleSource(rd);
    }
    @Override
    public void initialize() throws IOException {
        logger.debug("initializing multiple data source: {}", rd);
        if (iter.hasNext()) {
            curr = iter.next();
            curr.initialize();
            logger.debug("current data source: {}", curr);
        }
    }

    @Override
    public Object[] read() throws IOException {
        final Object[] data = curr.read();
		if (data != null) {
			return data;
		}

		if (iter.hasNext()) {
            curr.close();
            curr = iter.next();
            curr.initialize();
            logger.debug("current data source: {}", curr);
			return read();
		} else {
            curr = null;
			return null;
		}

    }

    @Override
    public void close() throws IOException {
        if (curr != null) {
            curr.close();
            curr = null;
        }
    }
}
