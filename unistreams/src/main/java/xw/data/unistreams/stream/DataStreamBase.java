package xw.data.unistreams.stream;

import xw.data.unistreams.env.Environment;
import xw.data.unistreams.resources.ResourceDescriptor;

/**
 * Created by xhuang on 6/3/16.
 */
public abstract class DataStreamBase implements DataStream
{
    public final ResourceDescriptor rd;
    protected final StreamContext context = new StreamContext();

    protected DataStreamBase(ResourceDescriptor rd) {
        this.rd = rd;
    }

    @Override
    final public StreamContext context() {
        return context;
    }

    @Override
    final public Environment env() {
        return Environment.of(rd.env);
    }

    @Override
    public String toString() {
        return "{" + rd + "}";
    }

}
