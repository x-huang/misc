package xw.data.unistreams.stream;

import java.io.IOException;

public interface Initializable {
    void initialize() throws IOException;
}
