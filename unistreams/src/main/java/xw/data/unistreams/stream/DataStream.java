package xw.data.unistreams.stream;

import xw.data.unistreams.env.Environment;

/**
 * Created by xhuang on 6/2/16.
 */
public interface DataStream extends Initializable, AutoCloseable
{
    Environment env();
    StreamContext context();
}
