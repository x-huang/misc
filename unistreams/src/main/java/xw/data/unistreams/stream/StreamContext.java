package xw.data.unistreams.stream;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by xhuang on 6/3/16.
 */
public class StreamContext extends ConcurrentHashMap<String, Object>
{
    public void copy(StreamContext that) {
        this.putAll(that);
    }

}
