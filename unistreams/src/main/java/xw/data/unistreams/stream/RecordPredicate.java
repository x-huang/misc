package xw.data.unistreams.stream;

/**
 * Created by xhuang on 5/26/16.
 */
public interface RecordPredicate
{
    boolean eval(Object[] record);
}
