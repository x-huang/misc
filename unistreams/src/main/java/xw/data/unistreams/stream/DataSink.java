package xw.data.unistreams.stream;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by xhuang on 5/24/16.
 */
public interface DataSink extends Initializable, Closeable
{
    void write(Object[] record) throws IOException;
}
