package xw.data.unistreams.env;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.ql.metadata.Hive;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import xw.data.unistreams.common.lang.ResourceUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.*;

public class Environment
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Environment.class);

    static final public String HADOOP_CONF_PRIFIX = "HADOOP_CONF.";
    static final public String HIVE_CONF_PRIFIX = "HIVE_CONF.";

    static final private String PATH_PREFIX = "configurations";

    static final private String[] HADOOP_CONF_FILES = {
            "yarn-site.xml",
            "core-site.xml",
            "hdfs-site.xml",
            "mapred-site.xml"
    };
    static final private String[] HIVE_CONF_FILES = {
            "hive-site.xml"
    };
    private static final Map<String, String> alias = new HashMap<>();
    private static final Map<String, Environment> envs = new HashMap<>();

    static {
        HiveConf.setHiveSiteLocation(null); // disable loading from hive-site.xml in case $HIVE_HOME/conf is in classpath

        final String aliasPropsFile = "env-alias.properties";
        final String aliasPropsPath = PATH_PREFIX + "/" + aliasPropsFile;
        final Properties props = ResourceUtils.loadPropertiesSafe(aliasPropsPath);
        if (props != null) {
            for (String key: props.stringPropertyNames()) {
                alias.put(key, props.getProperty(key));
                logger.debug("added built-in env alias: {} = {}", key, props.getProperty(key));
            }
        } else {
            logger.debug("resource {} not found, env alias disabled", aliasPropsPath);
        }

        final Properties sysProps = System.getProperties();
        final String propPrefix = "env.alias.";
        for (String key : sysProps.stringPropertyNames()) {
            if (key.startsWith(propPrefix)) {
                final String aliasName = key.substring(propPrefix.length());
                final String value = sysProps.getProperty(key);
                final boolean overwrite = alias.containsKey(aliasName);
                alias.put(aliasName, value);
                logger.debug("{} user-defined env alias: {} = {}",
                        overwrite? "overwrote": "added", aliasName, value);
            }
        }
    }


    public static synchronized List<String> loaded() {
        return new ArrayList<>(envs.keySet());
    }

    public static Map<String, String> alias() {
        return new TreeMap<>(alias);
    }

    public synchronized static Environment of(String env) {
        if (env == null || env.isEmpty()) {
            env = "default";
        }
        if (! envs.containsKey(env)) {
            envs.put(env, new Environment(env));
        }
        return envs.get(env);
    }

    public final String name;
    private Configuration hadoopConf = null;
    private HiveConf hiveConf = null;

    private Environment(String name) {
        this.name = name;
    }

    public synchronized void unload() {
        hadoopConf = null;
        hiveConf = null;
    }

    private static void loadConfigFile(Configuration conf, String env, String service, String file) {
        final String envResolved;
        if (alias.containsKey(env)) {
            envResolved = alias.get(env);
        } else {
            envResolved = env;
        }
        final String resourcePath = String.format("%s/%s/%s/%s", PATH_PREFIX, envResolved, service, file);
        final URL url = Thread.currentThread().getContextClassLoader().getResource(resourcePath);
        if (url == null) {
            logger.warn("missing configuration file: {}", resourcePath);
        } else {
            logger.debug("loaded configuration file: {}", resourcePath);
            conf.addResource(url);
        }
    }

    public synchronized Configuration getHadoopConf() {
        if (hadoopConf != null) {
            return hadoopConf;
        }

        final Configuration conf = new Configuration(false);
        for (String file: HADOOP_CONF_FILES) {
            loadConfigFile(conf, name, "hadoop", file);
        }
        logger.info("loaded hadoop config of {} entries", conf.size());

        final Properties props = System.getProperties();
        for (String prop: props.stringPropertyNames()) {
            if (!prop.startsWith(HADOOP_CONF_PRIFIX))
                continue;
            final String key = prop.substring(HADOOP_CONF_PRIFIX.length());
            final String value = props.getProperty(prop);
            conf.set(key, value);
            logger.info("set hadoop configuration: {}={}", key, value);
        }

        this.hadoopConf = conf;
        return conf;
    }


    public synchronized HiveConf getHiveConf() {
        if (hiveConf != null) {
            return hiveConf;
        }

        final HiveConf hiveConf = new HiveConf(getHadoopConf(), HiveConf.class);
        for (String file: HIVE_CONF_FILES) {
            loadConfigFile(hiveConf, name, "hive", file);
        }
        logger.info("loaded hive config of {} entries", hiveConf.size());

        hiveConf.setVar(HiveConf.ConfVars.METASTOREURIS, "");	// for hive 0.10.0 or above

        final Properties props = System.getProperties();
        for (String prop: props.stringPropertyNames()) {
            if (! prop.startsWith(HIVE_CONF_PRIFIX))
                continue;
            final String key = prop.substring(HIVE_CONF_PRIFIX.length());
            final String value = props.getProperty(prop);
            hiveConf.set(key, value);
            logger.info("set hive configuration: {}={}", key, value);
        }

        this.hiveConf = hiveConf;
        return hiveConf;
    }

    public Hive getHive() throws HiveException {
        return Hive.get(getHiveConf());
    }

    public FileSystem getFileSystem(URI uri) throws IOException {
        return FileSystem.get(uri, getHadoopConf());
    }
}
