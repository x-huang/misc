package xw.data.unistreams.resources.text;

import xw.data.unistreams.common.lang.StringEscapeUtils;

import java.util.Map;

/**
 * Created by xhuang on 9/17/16.
 */
class TextSerdeParameters
{
    public String fieldSeparator = "\t";
    public String itemSeparator = ",";
    public String kvSeparator = "=";
    public String nullString = "\\N";
    public boolean wrapString = false;
    public boolean wrapArray = false;
    public boolean wrapMap = false;
    public String rowOpening = null;
    public String rowClosing = null;

    public static TextSerdeParameters fromMap(Map<String, String> map) {
        final TextSerdeParameters serdeParams = new TextSerdeParameters();
        if (map.containsKey("fieldsep")) {
            serdeParams.fieldSeparator = StringEscapeUtils.unescapeJava(map.get("fieldsep"));
        }
        if (map.containsKey("itemsep")) {
            serdeParams.itemSeparator = StringEscapeUtils.unescapeJava(map.get("itemsep"));
        }
        if (map.containsKey("kvsep")) {
            serdeParams.kvSeparator = StringEscapeUtils.unescapeJava(map.get("kvsep"));
        }
        if (map.containsKey("wrapstring")) {
            serdeParams.wrapString = map.get("wrapstring").equals("true");
        }
        if (map.containsKey("wraparray")) {
            serdeParams.wrapArray = map.get("wraparray").equals("true");
        }
        if (map.containsKey("wrapmap")) {
            serdeParams.wrapMap = map.get("wrapmap").equals("true");
        }
        if (map.containsKey("nullstring")) {
            serdeParams.nullString = map.get("nullstring");
        }
        if (map.containsKey("rowopening")) {
            serdeParams.rowOpening = map.get("rowopening");
        }
        if (map.containsKey("rowclosing")) {
            serdeParams.rowClosing = map.get("rowclosing");
        }
        return serdeParams;
    }

    @Override
    public String toString() {
        return "TextSerdeParameters{" +
                "fieldSeparator='" + fieldSeparator + '\'' +
                ", itemSeparator='" + itemSeparator + '\'' +
                ", kvSeparator='" + kvSeparator + '\'' +
                ", nullString='" + nullString + '\'' +
                ", wrapString=" + wrapString +
                ", wrapArray=" + wrapArray +
                ", wrapMap=" + wrapMap +
                ", rowOpening='" + rowOpening + '\'' +
                ", rowClosing='" + rowClosing + '\'' +
                '}';
    }
}
