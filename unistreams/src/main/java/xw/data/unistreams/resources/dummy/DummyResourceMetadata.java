package xw.data.unistreams.resources.dummy;

import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 8/28/16.
 */
public class DummyResourceMetadata implements ResourceMetadata
{
    @Override
    public Object getPOJO() {
        return null;
    }

    @Override
    public Schema getSchema() {
        return Schema.newBuilder().build();
    }
}
