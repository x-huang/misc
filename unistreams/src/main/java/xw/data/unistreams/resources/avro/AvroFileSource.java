package xw.data.unistreams.resources.avro;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;

/**
 * Created by xhuang on 7/7/16.
 */
public class AvroFileSource extends DataStreamBase implements DataSource
{
    public AvroFileSource(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public Object[] read() throws IOException {
        return new Object[0];
    }

    @Override
    public void initialize() throws IOException {

    }

    @Override
    public void close() throws IOException {

    }
}
