package xw.data.unistreams.resources;

import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 8/28/16.
 */
public interface ResourceMetadata
{
    Object getPOJO();
    Schema getSchema();
}
