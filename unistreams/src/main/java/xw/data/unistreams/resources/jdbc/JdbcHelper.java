package xw.data.unistreams.resources.jdbc;

import java.util.Map;

/*
 * This interface and its implementations are merely a simple JDBC wrapper.
 * 
 * Instead of choosing existing JDBC wrapper libraries such as MyBatis, 
 * spring-jdbc, ... , I write our own because: 1). our use cases are strictly 
 * CRUB; 2). they may not support teradata; 3) other advantages listed in
 * http://stackoverflow.com/questions/299597/simple-jdbc-wrapper.
 * 
 * Neither to choose ORM libraries because we don't need data relation model
 * at all.
 */

public interface JdbcHelper
{
	enum OnDuplicate {
		THROW_ERROR, IGNORE, REPLAC; 
		
		public static OnDuplicate fromString(String s) {
			s = (s == null)? "" : s;
			s = s.toLowerCase().trim();
			if (s.isEmpty() || s.equals("insert") || s.equals("throwerror")) 
				return THROW_ERROR;
			else if (s.equals("ignore"))
				return IGNORE;
			else if (s.equals("replace"))
				return REPLAC;
			else 
				throw new IllegalArgumentException("unknown on-duplicate method: " + s);
		}
	}
	
	String getDriver();
	String makeUrl(String serverAddr, String dbName,
						  String username, String password,
						  Map<String, String> extraOptions);
	
	String makeSqlSelect(String tblName, String[] selects,
								Map<String, String> criteria, int limit);
	String makeNoopSelectStatement(String tblName);
	String makeDescribeStatement(String tblName);
	String makeDeleteStatement(String tblName);
	String makeDeleteStatement(String tblName, Map<String, String> criteria);
	String makePreparedInsertStatement(String tblName, int nCols, OnDuplicate ins);
	String makePreparedInsertStatement(String tblName, String[] cols, OnDuplicate ins);
	String makeWhereClause(Map<String, String> criteria);
	Integer suggestFetchSize();
}
