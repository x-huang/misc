package xw.data.unistreams.resources.text;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;

/**
 * Created by xhuang on 7/12/16.
 */
public class TextSource  extends DataStreamBase implements DataSource
{
    //private final BufferedReader br;
    //private final RecordTextParser parser;
    private boolean closeResource = true;

    public TextSource(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public Object[] read() throws IOException {
        return new Object[0];
    }

    @Override
    public void initialize() throws IOException {

    }

    @Override
    public void close() throws IOException {

    }
}
