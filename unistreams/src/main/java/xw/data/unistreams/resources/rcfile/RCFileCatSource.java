package xw.data.unistreams.resources.rcfile;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFileRecordReader;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.hive.serde2.columnar.BytesRefWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.FileSplit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;


class RCFileCatSource extends DataStreamBase implements DataSource
{
	private final static Logger logger = LoggerFactory.getLogger(RCFileCatSource.class);

	private RCFileRecordReader<LongWritable, BytesRefArrayWritable> reader = null;
    private final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder()
                .onMalformedInput(CodingErrorAction.REPLACE)
                .onUnmappableCharacter(CodingErrorAction.REPLACE);
	private int ncols = -1;

	transient private LongWritable key = new LongWritable();
	transient private BytesRefArrayWritable value = new BytesRefArrayWritable();

	public RCFileCatSource(ResourceDescriptor rd) {
		super(rd);
		if (rd.params.containsKey("ncols")) {
			this.ncols = (Integer.parseInt(rd.params.get("ncols")));
		}
	}


    public RCFileCatSource setNCols(int n) {
        this.ncols = n;
        return this;
    }

	@Override
	public void initialize() throws IOException {

		if (ncols < 0 && context.containsKey("ncols")) {
            this.ncols = Integer.parseInt((String) context.get("ncols"));
		}

        if (ncols < 0) {
            throw new RuntimeException("'ncols' not set, " +
                "check the temporal order of initialize(ctxt) of src and dest streams");
        }

        final URI uri = rd.getSingleUri();
		final Path path = new Path(uri);
		final FileSystem fs = env().getFileSystem(uri);
		final long length = HadoopCommon.getFileStatusRepr(fs, uri).length;
		final FileSplit split = new FileSplit(path, 0, length, (String[])null);
		reader = new RCFileRecordReader<>(env().getHadoopConf(), split);

		logger.debug("initialized reading from RC File: {}", uri);
	}

	@Override
	public Object[] read() throws IOException {
		if (reader.next(key, value)) {
            // value.resetValid(schema.size());
            final Object[] row = new Object[ncols];
            for (int i=0; i<row.length; i++) {
                final BytesRefWritable v = value.unCheckedGet(i);
                final ByteBuffer bb = ByteBuffer.wrap(v.getData(), v.getStart(), v.getLength());
                final String val = decoder.decode(bb).toString();
				row[i] = val;
				/*
                if (val.equals("\\N")) {
                    row[i] = null;
                } else {
					row[i] = val;
				}*/
            }
            return row;
		} else {
			return null;
		}
	}

	@Override
	public void close() throws IOException {
		reader.close();
	}
}
