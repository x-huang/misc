package xw.data.unistreams.resources.orc;

import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.resources.orc.repr.OrcFilePropertyRepr;
import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 9/16/16.
 */
public class OrcFileMetadata implements ResourceMetadata
{
    public final OrcFilePropertyRepr repr;

    public OrcFileMetadata(OrcFilePropertyRepr repr) {
        this.repr = repr;
    }

    @Override
    public Object getPOJO() {
        return repr;
    }

    @Override
    public Schema getSchema() {
        return Schema.fromString(repr.typeName);
    }
}
