package xw.data.unistreams.resources;

import xw.data.unistreams.common.lang.Registry;
import xw.data.unistreams.resources.dummy.DummyResourceOperator;
import xw.data.unistreams.resources.hive.HiveResourceOperator;
import xw.data.unistreams.resources.jdbc.JdbcResourceOperator;
import xw.data.unistreams.resources.kryo.KryoFileOperator;
import xw.data.unistreams.resources.orc.OrcFileOperator;
import xw.data.unistreams.resources.rcfile.RCFileOperator;
import xw.data.unistreams.resources.text.TextFileOperator;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by xhuang on 5/25/16.
 */
public class Resources
{
    final static private Registry<Class<? extends ResourceOperator>> resOps;

    static {
        resOps = new Registry<>();
        resOps.register(OrcFileOperator.class, "orc");
        resOps.register(RCFileOperator.class, "rc", "rcfile", "rc1", "rc2", "rcfilecat", "rccat");
        resOps.register(TextFileOperator.class, "text", "txt");
        resOps.register(DummyResourceOperator.class, "dummy");
        resOps.register(HiveResourceOperator.class, "hive");
        resOps.register(JdbcResourceOperator.class, "jdbc");
        resOps.register(KryoFileOperator.class, "kryo");
    }

    static public synchronized void registerResourceOperator(Class<? extends ResourceOperator> klass, String... prefix) {
        resOps.register(klass, prefix);
    }

    static public ResourceOperator getOperator(ResourceDescriptor rd) {
        if (rd.prefix == null) {
            throw new NullPointerException("missing format as resource uri prefix: " + rd);
        }

        final Class<? extends ResourceOperator> clz = resOps.get(rd.prefix);
        if (clz == null) {
            throw new IllegalArgumentException("unregistered resource operator: " + rd.prefix);
        }

        try {
            return clz.getDeclaredConstructor(ResourceDescriptor.class).newInstance(rd);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("cannot instantiate resource operator: " + rd);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("no such constructor");
        }
    }

}
