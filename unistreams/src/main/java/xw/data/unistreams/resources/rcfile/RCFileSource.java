package xw.data.unistreams.resources.rcfile;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Map;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFileRecordReader;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe.SerDeParameters;
import xw.data.unistreams.common.hive.objectinspector.Writables;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStruct;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStructBase;
import org.apache.hadoop.hive.serde2.columnar.LazyBinaryColumnarStruct;
import org.apache.hadoop.hive.serde2.lazy.LazyObjectBase;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryObject;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileSplit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.common.hive.objectinspector.LazyObjectUtils;
import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.common.hive.serde.SerDeUtils;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.schema.Schema;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;


/*
 * Xiaowan: 
 * 
 * see Hive 0.12.0 RELEASE_NOTES.txt !!!
 * 
 * 	[HIVE-4475] - Switch RCFile default to LazyBinaryColumnarSerDe
 */

class RCFileSource extends DataStreamBase implements DataSource
{
	private final static Logger logger = LoggerFactory.getLogger(RCFileSource.class);

	// public final URI uri;
    // public final Map<String, String> params;

	private String schema = null;
    private int ncols = -1;
	private int version = -1;

    private RCFileRecordReader<LongWritable, BytesRefArrayWritable> reader = null;
	private ColumnarStructBase lazyStruct = null;
	
	transient final private LongWritable key = new LongWritable();
	transient final private BytesRefArrayWritable value = new BytesRefArrayWritable();
	
	public RCFileSource(ResourceDescriptor rd) {
		super(rd);
	}

	@Override
	public void initialize() throws IOException {

		final URI uri = rd.getSingleUri();
		final Map<String, String> params = rd.params;

		assert rd.prefix != null;

        if (schema == null && params.containsKey("schema")) {
            schema = params.get("schema");
        }
		if (schema == null && context.containsKey("schema")) {
            schema = (String) context.get("schema");
		}
        if (schema == null) {
            throw new RuntimeException("'schema' not set, " +
                "check the temporal order of initialize(ctxt) of src and dest streams");
        }
        if (! schema.startsWith("struct<")) {
			schema = "struct<" + schema + ">";
		}
        this.ncols = Schema.fromString(schema).size();

		if (rd.prefix.endsWith("1")) {
            version = 1;
        } else if (rd.prefix.endsWith("2")) {
            version = 2;
        }

        if (version < 0 && params.containsKey("version")) {
            version = Integer.parseInt(params.get("version"));
        }
        if (version < 0 && context.contains("version")) {
            version = Integer.parseInt((String) context.get("version"));
        }
        if (version != 1 && version != 2) {
            throw new RuntimeException("bad rcfile version: " + version + ", expecting 1 or 2");
        }

		final ArrayList<Integer> notSkipIds = new ArrayList<>();
        for (int i=0; i<ncols; i++) {
            notSkipIds.add(i);
        }

        final boolean useBinarySerDe = version > 1;
        if (useBinarySerDe) {
            final StructObjectInspector oi = ObjectInspectorUtils.getBinaryColumnarStructInspector(schema);
			lazyStruct = new LazyBinaryColumnarStruct(oi, notSkipIds);
            logger.debug("created lazy binary struct");
		} else {
			// ObjectInspectorUtils.getDefaultSerDeParameters().getNullSequence();
            final SerDeParameters serdeParams;
            final Text nullSequence;
            final StructObjectInspector oi;
            try {
                serdeParams = SerDeUtils.getSerDeParameters(env().getHadoopConf(), params);
                oi = ObjectInspectorUtils.getColumnarStructInspector(schema, serdeParams);
				nullSequence = serdeParams.getNullSequence();
			} catch (SerDeException e) {
				throw new IOException("cannot initialize columnar serde parameters", e);
			}
			lazyStruct = new ColumnarStruct(oi, notSkipIds, nullSequence);
            logger.debug("created columnar struct with serde params: {}",
                    SerDeUtils.describeSerDeParameters(serdeParams));
		}

		final Path path = new Path(uri);
		final FileSystem fs = env().getFileSystem(uri);
		final long length = HadoopCommon.getFileStatusRepr(fs, uri).length;
		final FileSplit split = new FileSplit(path, 0, length, (String[])null);
		reader = new RCFileRecordReader<>(env().getHadoopConf(), split);

		logger.debug("initialized reading from RC File: {}", uri);
	}

	@Override
	public Object[] read() throws IOException {
		if (reader.next(key, value)) {
			lazyStruct.init(value);
			final Object[] row = new Object[ncols];
			for (int i=0; i<ncols; i++)  {
				final Object lazyOrWritable = lazyStruct.getField(i);
				if (lazyOrWritable == null) {
					row[i] = null;
				} else if (lazyOrWritable instanceof Writable) {
					row[i] = Writables.getValueObject((Writable) lazyOrWritable);
				} else if (lazyOrWritable instanceof LazyBinaryObject<?>) {
					row[i] = LazyObjectUtils.lazyBinaryToPOJO((LazyBinaryObject<?>) lazyOrWritable);
				} else {
					row[i] = LazyObjectUtils.lazyToPOJO((LazyObjectBase) lazyOrWritable);
				}
			}
			return row;
		} else {
			return null;
		}
	}

	@Override
	public void close() throws IOException {
		reader.close();
	}
}
