package xw.data.unistreams.resources.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.common.uri.DatabaseUri;
import xw.data.unistreams.resources.jdbc.repr.TableRepr;

import java.io.IOException;
import java.net.URI;
import java.sql.*;

public class JdbcCommon
{
    static final Logger logger = LoggerFactory.getLogger(JdbcCommon.class);

	public static Object getTableRepr(URI uri) 
			throws IOException {
		final DatabaseUri dbUri = DatabaseUri.fromUri(uri);
		return getTableRepr(
                dbUri.scheme,
				dbUri.server,
				dbUri.dbName,
				dbUri.tblName,
				dbUri.user,
				dbUri.password
			);
	}
	
	public static TableRepr getTableRepr(String scheme, String server,
									  String dbName, String tblName, 
									  String user, String password)
			throws IOException {

		final JdbcHelper helper = JdbcHelpers.getHelper(scheme);
		
		final String driver = helper.getDriver();
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("jdbc driver not found: " + driver);
		}
		
		final String jdbcUrl = helper.makeUrl(server, dbName, user, password, null);
		logger.debug("using jdbc url: {}", jdbcUrl);

		final Connection conn;
		try {
			conn = DriverManager.getConnection(jdbcUrl);
		} catch (SQLException e) {
			String msg = "jdbc connection error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		final String sql = helper.makeNoopSelectStatement(tblName);
		logger.debug("executing sql: {}", sql);

		final TableRepr schema;
		try {
			final Statement stmt = conn.createStatement();
			final ResultSet rs = stmt.executeQuery(sql);
			ResultSetMetaData md = rs.getMetaData();
			schema = new TableRepr(md);
			conn.close();
		} catch (SQLException e) {
			String msg = "jdbc error: " + e.getMessage();
			throw new IOException(msg, e);		
		}
		return schema;
		
	}
	
	public static int executeUpdate(URI uri, String sql) 
			throws IOException {
		return executeUpdate(DatabaseUri.fromUri(uri), sql);
	}
	
	public static int executeUpdate(final DatabaseUri dbUri, String sql)
			throws IOException {
		// final DatabaseUriParser parser = getParsedDatabaseUri(uri);
		final JdbcHelper helper = JdbcHelpers.getHelper(dbUri.scheme);
		
		final String driver = helper.getDriver();
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("jdbc driver not found: " + driver);
		}
		
		final String jdbcUrl = helper.makeUrl(dbUri.server,
                dbUri.dbName, dbUri.user, dbUri.password, null);
        logger.debug("using jdbc url: {}", jdbcUrl);

		final Connection conn;
		try {
			conn = DriverManager.getConnection(jdbcUrl);
		} catch (SQLException e) {
			String msg = "jdbc connection error: " + e.getMessage();
			throw new IOException(msg, e);
		}

        logger.debug("executing sql: {}", sql);
		int rowCount;
		try {
			final Statement stmt = conn.createStatement();
			rowCount = stmt.executeUpdate(sql);
			conn.close();
		} catch (SQLException e) {
			String msg = "jdbc error: " + e.getMessage();
			throw new IOException(msg, e);		
		}
		return rowCount;
	}

    public static String sqlTypeToTypeString(int type) {
		switch (type) {
			case Types.VARCHAR: return "string";
			case Types.CHAR: return "char";
			case Types.FLOAT: return "float";
			case Types.DOUBLE: return "double";
			case Types.BOOLEAN: return "boolean";
			case Types.BIT:
			case Types.TINYINT: return "tinyint";
			case Types.SMALLINT: return "smallint";
			case Types.INTEGER: return "int";
			case Types.BIGINT: return "bigint";
			case Types.DATE: return "date";
			case Types.TIMESTAMP: return "timestamp";
			case Types.DECIMAL: return "decimal";
			case Types.BINARY: return "binary";
			case Types.JAVA_OBJECT: return "map";
			case Types.ARRAY: return "array";
			case Types.STRUCT: return "struct";
			default: throw new RuntimeException("unsupported jdbc type: " + type);
		}
	}

    public static String getTypeString(ResultSetMetaData md) throws SQLException {
        final StringBuilder sb = new StringBuilder();
        final int count = md.getColumnCount();
		String delimiter = "";
		sb.append("struct<");
		for (int i=1; i<=count; i++) {
            final String name = md.getColumnName(i);
            final int type = md.getColumnType(i);
			sb.append(delimiter).append(name).append(':').append(sqlTypeToTypeString(type));
			delimiter = ",";
		}
		sb.append('>');
        return sb.toString();
    }
}
