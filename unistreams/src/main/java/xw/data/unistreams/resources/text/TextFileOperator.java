package xw.data.unistreams.resources.text;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.resources.ResourceOperator;

import java.io.IOException;

/**
 * Created by xhuang on 5/31/16.
 */
public class TextFileOperator extends ResourceOperator
{
    public TextFileOperator(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public DataSource openSource() {
        throw new UnsupportedOperationException("reading text data not implemented yet");
    }

    @Override
    public DataSink openSink() {
        rd.assertUriSizeEq(1);
        return new TextSink(rd);
    }

    @Override
    public ResourceMetadata metadata() throws IOException {
        return new TextFileMetadata(rd);
    }
}
