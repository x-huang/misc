package xw.data.unistreams.resources.kryo;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.resources.ResourceOperator;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataSource;

import java.io.IOException;

/**
 * Created by xhuang on 9/20/16.
 */
public class KryoFileOperator extends ResourceOperator
{
    public KryoFileOperator(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public DataSource openSource() {
        rd.assertUriSizeEq(1);
        return new KryoFileSource(rd);
    }

    @Override
    public DataSink openSink() {
        rd.assertUriSizeEq(1);
        return new KryoFileSink(rd).setOverwrite(true);
    }

    @Override
    public ResourceMetadata metadata() throws IOException {
        return new KryoFileMetadata(rd);
    }
}
