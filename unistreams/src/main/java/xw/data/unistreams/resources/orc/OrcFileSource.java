package xw.data.unistreams.resources.orc;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.Reader;
import org.apache.hadoop.hive.ql.io.orc.RecordReader;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

class OrcFileSource extends DataStreamBase implements DataSource
{
	private final static Logger logger = LoggerFactory.getLogger(OrcFileSource.class);

	private StructObjectInspector oi = null;
	private List<? extends StructField> fields = null;
	transient private RecordReader rows = null;
	transient private Object row = null;

	public OrcFileSource(ResourceDescriptor rd) {
		super(rd);
	}
	
	public URI getLocationUri() {
		return rd.getSingleUri();
	}

	@Override
	public void initialize() throws IOException {
		final URI uri = rd.getSingleUri();
		final Path path = new Path(uri);
		final FileSystem fs = env().getFileSystem(uri);

		final Reader reader = OrcFile.createReader(fs, path);
		oi = (StructObjectInspector) reader.getObjectInspector();
		fields = oi.getAllStructFieldRefs();
		rows = reader.rows(null);

        context.put("schema", oi.getTypeName());
		logger.debug("initialized reading from ORC file: {} ({} rows)", uri, reader.getNumberOfRows());
	}

	@Override
	public Object[] read() throws IOException {

		if (! rows.hasNext()) {
			return null;
		}
		
		row = rows.next(row);
		final Object[] data = new Object[fields.size()];
		for (int i=0; i< fields.size(); i++) {
			final Object o = oi.getStructFieldData(row, fields.get(i));
			final ObjectInspector fieldInspector = fields.get(i).getFieldObjectInspector();
			data[i] = ObjectInspectorUtils.inspectObject(o, fieldInspector);
		}
		return data;
	}
	
	@Override
	public void close() throws IOException {
		rows.close();
		row = null;
	}

}
