package xw.data.unistreams.resources.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JdbcDataSource extends DataStreamBase implements DataSource
{
    static final Logger logger = LoggerFactory.getLogger(JdbcDataSource.class);

	public final String scheme;
	public final String server;
	public final String dbName;
	public final String tblName;
	public final String user;
	public final String password;
    protected Map<String, String> extraOptions = null;
	protected String[] selects = null;
	protected Map<String, String> querySpec = null;
	protected int limit = -1;
	protected int fetchSize = Integer.MIN_VALUE;
	private Connection conn= null;
	private ResultSet rs = null;
	private int ncols= -1;
	private String sql= null;

	public JdbcDataSource(ResourceDescriptor rd,
						  String scheme, String server,
						  String dbName, String tblName,
						  String user, String password) {
		super(rd);
		this.scheme = scheme;
		this.server = server;
		this.dbName = dbName;
		this.tblName = tblName;
		this.user = user;
		this.password = password;
	}

    public JdbcDataSource setExtraOption(String name, String value) {
        if (extraOptions == null) {
            extraOptions = new HashMap<>();
        }
        extraOptions.put(name, value);
        return this;
    }

	public JdbcDataSource setLimit(Integer limit) {
		this.limit = limit == null? -1: limit;
		return this;
	}
	
	public JdbcDataSource setSelects(String[] selects) {
		this.selects = (selects == null? null : selects.clone());
		return this;
	}
	
	public JdbcDataSource setFilter(Map<String, String> filter) {
		querySpec = new HashMap<>(filter);
		return this;
	}
	
	public JdbcDataSource setSelectFetchSize(int size) {
		fetchSize = size;
		return this;
	}
	
	public JdbcDataSource setCustomSelectStatement(String sql) {
		// validate if the target table same as what in resource uri.
		final Pattern pattern = Pattern.compile("\\bfrom\\s+([^\\s]+)", Pattern.CASE_INSENSITIVE);
		final Matcher matcher = pattern.matcher(sql);
		if (matcher.find()) {
			String fromTable = matcher.group(1);
			String[] tokens = fromTable.split("\\.");
			if (tokens.length == 2) {
				if (! tokens[0].equalsIgnoreCase(dbName)) {
					String msg = "jdbc custom sql: database doesn't match: " 
								+ tokens[0] + " vs. " + dbName;
					throw new IllegalArgumentException(msg);
				}
				else if (! tokens[1].equalsIgnoreCase(tblName)) {
					String msg = "jdbc custom sql: table doesn't match: " 
								+ tokens[1] + " vs. " + tblName;
					throw new IllegalArgumentException(msg);
				}
			}
			else if (tokens.length == 1) {
				if (! tokens[0].equalsIgnoreCase(tblName)) {
					String msg = "jdbc custom sql: table doesn't match: " 
								+ tokens[0] + " vs. " + tblName;
					throw new IllegalArgumentException(msg);
				}
			}
		}
		
		// on SQL syntax error, let JDBC report error in detail 
		this.sql = sql;
		return this;
	}
	
	public void initialize() throws IOException {
		final JdbcHelper helper = JdbcHelpers.getHelper(scheme);

		try {
			Class.forName(helper.getDriver());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("cannot find jdbc driver", e);
		}

		final String jdbcUrl = helper.makeUrl(server, dbName, user, password, extraOptions);
		logger.debug("using jdbc url: {}", jdbcUrl);

        try {
            conn = DriverManager.getConnection(jdbcUrl);
        } catch (SQLException e) {
            throw new IOException("connection error: " + e.getMessage(), e);
        }

        final String sql;
		if (this.sql == null) {
			sql = helper.makeSqlSelect(tblName, selects, querySpec, limit);
		} else {
			if (querySpec.size() > 0) {
                throw new IllegalArgumentException("custom sql statement conflicts with query spec");
			}
			if (selects.length > 0) {
                throw new IllegalArgumentException("custom sql statement conflicts with select parameter");
			}
			sql = this.sql;
		}
        logger.debug("executing sql: {}", sql);

        try {
            final Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            final Integer suggestedFetchSize = helper.suggestFetchSize();
            if (suggestedFetchSize != null) {
                stmt.setFetchSize(suggestedFetchSize);
            }
            rs = stmt.executeQuery(sql);
            final ResultSetMetaData md = rs.getMetaData();
            ncols = md.getColumnCount();

            context.put("schema", JdbcCommon.getTypeString(md));
            logger.debug("set schema: {}", context.get("schema"));
        } catch (SQLException e) {
            throw new IOException("sql error: " + e.getMessage(), e);
        }
        logger.debug("jdbc source initialized");

	}

	@Override
	public Object[] read() throws IOException {
		try {
			if (!rs.next()) {
                return null;
            }

			final Object[] record = new Object[ncols];
			for (int i=0; i< ncols; i++) {
				record[i] = rs.getObject(i+1);
			}
			return record;
		} catch (SQLException e) {
			throw new IOException("sql error reading records", e);
		}
	}

	@Override
	public void close() throws IOException {
		try {
			if (conn != null) {
                conn.close();
            }
		} catch (SQLException e) {
			throw new IOException("sql error disconnecting", e);
		}
	}
}
