package xw.data.unistreams.resources.dummy;

import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.resources.ResourceOperator;

import java.io.IOException;

/**
 * Created by xhuang on 5/31/16.
 */
public class DummyResourceOperator extends ResourceOperator
{
    public DummyResourceOperator(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public DataSource openSource() {
        rd.assertUriSizeEq(1);
        switch (rd.uris.get(0).toString()) {
            case "empty":
            case "/empty":
                return EmptyDataSource.singleton;
            default:
                throw new IllegalArgumentException("unknown dummy resource: " + rd);
        }
    }

    @Override
    public DataSink openSink() {
        rd.assertUriSizeEq(1);
        switch (rd.uris.get(0).toString()) {
            case "null":
            case "/null":
                return NullDataSink.singleton;
            default:
                throw new IllegalArgumentException("unknown dummy resource: " + rd);
        }
    }

    @Override
    public ResourceMetadata metadata() throws IOException {
        return new DummyResourceMetadata();
    }
}
