package xw.data.unistreams.resources.jdbc.repr;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ColumnRepr implements Serializable
{
	private static final long serialVersionUID = -4953888950770492017L;

	public final String catalogName;
	public final String className;
	public final int displaySize;
	public final String label;
	public final String name;
	public final int type;
	public final String typeName;
	public final int precision;
	public final int scale;
	public final String schemaName;
	public final String tableName;
	public final boolean isAutoIncrement;
	public final boolean caseSensitive;
	public final boolean isCurrency;
	public final boolean definitelyWritable;
	public final String nullable;
	public final boolean readOnly;
	public final boolean searchable;
	public final boolean signed;
	public final boolean writable;
	
	static public String translateNullable(int nullable) {
		if (nullable == ResultSetMetaData.columnNoNulls) 
			return "columnNoNulls";
		else if (nullable == ResultSetMetaData.columnNullable)
			return "columnNullable";
		else if (nullable == ResultSetMetaData.columnNullableUnknown)
			return "columnNullableUnknown";
		else 
			throw new IllegalArgumentException("unknown nullable targetValue" + nullable);
	}
	
	public ColumnRepr(ResultSetMetaData md, int column) 
			throws SQLException {

		catalogName = md.getCatalogName(column);
		className = md.getColumnClassName(column);
		displaySize = md.getColumnDisplaySize(column);
		label = md.getColumnLabel(column);
		name = md.getColumnName(column);
		type = md.getColumnType(column);
		typeName = md.getColumnTypeName(column);
		precision = md.getPrecision(column);
		scale = md.getScale(column);
		schemaName = md.getSchemaName(column);
		tableName = md.getTableName(column);
		isAutoIncrement = md.isAutoIncrement(column);
		caseSensitive = md.isCaseSensitive(column);
		isCurrency = md.isCurrency(column);
		definitelyWritable = md.isDefinitelyWritable(column);
		nullable = translateNullable(md.isNullable(column));
		readOnly = md.isReadOnly(column);
		searchable = md.isSearchable(column);
		signed = md.isSigned(column);
		writable = md.isWritable(column);
	}
	
	public static List<ColumnRepr> getColumnRepresentations(ResultSetMetaData md) 
			throws SQLException {
		final int count = md.getColumnCount();
		final ArrayList<ColumnRepr> columns = new ArrayList<>(count);
		for (int i=1; i<=count; i++) {
			columns.add(new ColumnRepr(md, i));
		}
		return columns;
	}

	public String getHiveType() {
		switch (type) {
            case Types.CLOB:
			case Types.VARCHAR: return "string";
			case Types.CHAR: return "char";
			case Types.FLOAT: return "float";
			case Types.DOUBLE: return "double";
			case Types.BOOLEAN: return "boolean";
            case Types.BIT:
			case Types.TINYINT: return "tinyint";
			case Types.SMALLINT: return "smallint";
			case Types.INTEGER: return "int";
			case Types.BIGINT: return "bigint";
			case Types.DATE: return "date";
			case Types.TIMESTAMP: return "timestamp";
			case Types.DECIMAL: return "decimal";
            case Types.BLOB:
			case Types.BINARY: return "binary";
			case Types.JAVA_OBJECT: return "map";
			case Types.ARRAY: return "array";
			case Types.STRUCT: return "struct";
            default: return "unknown_" + type;
			// default: throw new RuntimeException("unsupported jdbc type: "
			//		+ typeName + "(number=" + type + ")" );
		}
	}

	@Override
	public String toString() {
		return "ColumnRepr{" +
				", name='" + name + '\'' +
				", typeName='" + typeName + '\'' +
				", isAutoIncrement=" + isAutoIncrement +
				", nullable='" + nullable + '\'' +
				"}\n";
	}
}
