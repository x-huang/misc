package xw.data.unistreams.resources.jdbc;

import xw.data.unistreams.common.lang.Registry;

import java.io.PrintStream;

public class JdbcHelpers
{
    static private final Registry<JdbcHelper> jdbcHelpers = new Registry<>();
    static {
        jdbcHelpers.register(new MysqlJdbcHelper(), "mysql")
                    .register(new MssqlJdbcHelper(), "mssql")
                    .register(new TeradataJdbcHelper(), "teradata");
    }

	static public JdbcHelper getHelper(String scheme) {
		final JdbcHelper helper = jdbcHelpers.get(scheme);
        if (helper == null) {
            throw new IllegalArgumentException("no such jdbc scheme: " + scheme);
        }
		return helper;
	}

    public static void dump(PrintStream ps) {
        jdbcHelpers.dump(ps);
    }

    public static void registerJdbcHelper(JdbcHelper helper, String name) {
        jdbcHelpers.register(helper, name);
    }
}
