package xw.data.unistreams.resources;


import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;

import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * Created by xhuang on 5/25/16.
 */
public abstract class ResourceOperator
{
    public final ResourceDescriptor rd;

    private ResourceOperator(String env, String format, List<URI> uris, Map<String, String> params) {
        this.rd = ResourceDescriptor.construct(env, format, uris, params);
    }

    public ResourceOperator(ResourceDescriptor rd) {
        this.rd = rd;
    }

    abstract public DataSource openSource();
    abstract public DataSink openSink();
    abstract public ResourceMetadata metadata() throws IOException;
}
