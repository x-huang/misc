package xw.data.unistreams.resources.orc;

import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.resources.ResourceOperator;
import xw.data.unistreams.resources.orc.repr.OrcFilePropertyRepr;
import xw.data.unistreams.stream.MultipleDataSource;

import java.io.IOException;

/**
 * Created by xhuang on 5/25/16.
 */
public class OrcFileOperator extends ResourceOperator
{

    public OrcFileOperator(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public DataSource openSource() {
        rd.assertUriSizeGtEq(1);

        if (rd.uris.size() == 1) {
            return new OrcFileSource(rd);
        } else {
            return new MultipleDataSource(rd);
        }
    }

    @Override
    public DataSink openSink() {
        rd.assertUriSizeEq(1);
        return new OrcFileSink(rd, rd.params.get("schema")).setOverwrite(true);
    }

    @Override
    public OrcFileMetadata metadata() throws IOException {

        if (rd.uris.size() != 1) {
            throw new IllegalArgumentException("expecting exactly one URI");
        }
        return new OrcFileMetadata(OrcFilePropertyRepr.fromURI(rd.env, rd.uris.get(0)));
    }
}
