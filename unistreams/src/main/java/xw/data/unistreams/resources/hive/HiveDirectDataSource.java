package xw.data.unistreams.resources.hive;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class HiveDirectDataSource extends DataStreamBase implements DataSource
{
    private final DataSource ds;
    private final Object[] partSpec;

    public HiveDirectDataSource(ResourceDescriptor rd, List<Object> parts) {
        super(rd);
        this.ds = rd.source();
        if (parts == null) {
            parts = Collections.emptyList();
        }
        this.partSpec = parts.toArray(new Object[parts.size()]);
    }

    @Override
    public void initialize() throws IOException {
        ds.initialize();
    }

    @Override
    public Object[] read() throws IOException {
        final Object[] row = ds.read();
        if (row == null) {
            return null;
        }
        if (partSpec.length == 0) {
            return row;
        } else {
            final Object[] appended = new Object[row.length + partSpec.length];
            System.arraycopy(appended, 0, row, 0, row.length);
            System.arraycopy(appended, row.length, partSpec, 0, partSpec.length);
            return appended;
        }
    }


    @Override
    public void close() throws IOException {
        ds.close();
    }
}
