package xw.data.unistreams.resources.kryo;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hive.com.esotericsoftware.kryo.Kryo;
import org.apache.hive.com.esotericsoftware.kryo.io.Output;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;
import java.net.URI;

/**
 * Created by xhuang on 9/20/16.
 */
public class KryoFileSink extends DataStreamBase implements DataSink
{
    private final static Logger logger = LoggerFactory.getLogger(KryoFileSink.class);

    private final Kryo kryo;
    private Output output = null;
    private boolean overwrite = false;

    public KryoFileSink(ResourceDescriptor rd) {
        super(rd);
        this.kryo = new Kryo();
        kryo.register(Object[].class);
    }

    public KryoFileSink setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
		return this;
	}

    @Override
    public void write(Object[] record) throws IOException {
        kryo.writeObject(output, record);
    }

    @Override
    public void initialize() throws IOException {
        if (output != null) {
            throw new IllegalStateException("already initialized");
        }
        final URI uri = rd.getSingleUri();
        final FileSystem fs = env().getFileSystem(uri);
        if (overwrite && HadoopCommon.delete(fs, uri)) {
			logger.debug("deleted existing file: {}", uri);
		}
        this.output = new Output(HadoopCommon.openOutputStream(fs, uri));
        logger.debug("kryo file '{}' opened for writing", uri);
    }

    @Override
    public void close() throws IOException {
        output.flush();
        output.close();
        output = null;
    }
}
