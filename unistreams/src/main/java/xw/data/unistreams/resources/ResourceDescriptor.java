package xw.data.unistreams.resources;

import xw.data.unistreams.common.lang.ResourceUtils;
import xw.data.unistreams.common.lang.Template;
import xw.data.unistreams.common.uri.UriQuery;
import xw.data.unistreams.common.uri.UriUtils;
import xw.data.unistreams.setting.GlobalSetting;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;

import java.net.URI;
import java.util.*;

/**
 * Created by xhuang on 5/27/16.
 */

/**
 * ResourceDescriptor is a string representation of:
 *
 *  [env::][prefix:]?uri
 *  [env::][prefix:]?(uri[;uri]+)?key=value
 */

public class ResourceDescriptor
{
    private final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ResourceDescriptor.class);
    private final static String URI_DELIMITER = ";";

    private static Map<String, String> alias = new HashMap<>();

    static {
        try {
            final Properties props = ResourceUtils.loadProperties("/resource-alias.properties");
            final Set<String> keys = props.stringPropertyNames();
            for (String key : keys) {
                final String value = props.getProperty(key);
                if (alias.containsKey(value)) {
                    logger.warn("cannot handle transitive alias resolving: {} = {}", key, value);
                } else {
                    alias.put(key, value);
                    logger.debug("added built-in resource alias: {} = {}", key, value);
                }
            }
        } catch (Exception e) {
            logger.warn("failed loading resource alias: {}", e.getMessage(), e);
        }

        final Properties props = System.getProperties();
        final String propPrefix = "resource.alias.";
        for (String key : props.stringPropertyNames()) {
            if (key.startsWith(propPrefix)) {
                final String value = props.getProperty(key);
                if (alias.containsKey(value)) {
                    logger.warn("cannot handle transitive alias resolving: {} = {}", key, value);
                } else {
                    alias.put(key.substring(propPrefix.length()), value);
                    logger.debug("added user-defined resource alias: {} = {}", key, value);
                }
            }
        }
    }

    public static Map<String, String> alias() {
        return new TreeMap<>(alias);
    }

    public final String env;
    public final String prefix;
    public final List<URI> uris;
    public final Map<String, String> params;

    protected ResourceDescriptor(final String s) {

        final String s1;
        final int idx0 = s.indexOf("::");
        if (idx0 < 0) {
            this.env = null;
            s1 = s;
        } else {
            this.env = s.substring(0, idx0);
            s1 = s.substring(idx0 + 2);
        }

        final int idx1 = s1.indexOf(':');
        final String s2;
        if (idx1 >= 0 && idx1 != s1.indexOf("://")) {
            this.prefix = s1.substring(0, idx1);
            s2 = s1.substring(idx1 + 1);
        } else {
            this.prefix = null;
            s2 = s1;
        }
        this.uris = new ArrayList<>();
        if (s2.startsWith("(")) {
            final int idx2 = s2.indexOf(')');
            if (idx2 < 0) {
                throw new IllegalArgumentException("mismatched parenthesis pair in URI: " + s1);
            }
            final String s3 = s2.substring(1, idx2);
            for (String s4: s3.split(URI_DELIMITER)) {
                if (! s4.trim().isEmpty()) {
                    uris.add(URI.create(s4));
                }
            }
            final String s5 =s2.substring(idx2 +1);
            if (s5.isEmpty() || s5.equals("?")) {
                this.params = new HashMap<>();
            } else {
                final String s6 = s5.startsWith("?")? s5.substring(1): s5;
                this.params = UriQuery.parseToMap(s6);
            }
        } else {
            uris.add(URI.create(s2));
            this.params = new HashMap<>();
        }
    }

    private static List<URI> toUris(List<String> list) {
        final List<URI> uris = new ArrayList<>();
        for (String s: list) {
            uris.add(URI.create(s));
        }
        return uris;
    }

    protected ResourceDescriptor(String env, String prefix, List<URI> uris,
                                 Map<String, String> params) {
        this.env = env;
        this.prefix = prefix;
        this.uris = new ArrayList<>(uris);
        this.params = new HashMap<>();
        this.params.putAll(params);
    }

    private static class VarResolver implements Template.Evaluator {

        final Map<String, String> context;
        VarResolver(String s) {
            final int index = s.lastIndexOf('?');
            final String query = index>=0? s.substring(index+1): null;
            context = UriQuery.parseToMap(query);
        }

        @Override
        public String eval(Template.Term term) {
            if (term instanceof Template.Variable) {
                final Template.Variable var = (Template.Variable) term;
                if (var.prefix == null) {
                    if (context.containsKey(var.var)) {
                        return context.get(var.var);
                    } else if (GlobalSetting.containsKey(var.var)) {
                        return (String) GlobalSetting.get(var.var);
                    } else {
                        throw new IllegalArgumentException("variable not defined in either global setting " +
                                "or resource descriptor parameters: " + term);
                    }
                } else if (var.prefix.equals("env")) {
                    final Map<String, String> env = System.getenv();
                    if (! env.containsKey(var.var)) {
                        throw new IllegalArgumentException("variable not defined in environment: " + term);
                    }
                    return env.get(var.var);
                } else if (var.prefix.equals("sys")) {
                    final Properties props = System.getProperties();
                    if (! props.containsKey(var.var)) {
                        throw new IllegalArgumentException("variable not defined in system properties: " + term);
                    }
                    return props.getProperty(var.var);
                } else {
                    throw new IllegalArgumentException("unknown variable prefix: " + var.prefix);
                }
            } else {
                throw new IllegalStateException("unknown template term: " + term);
            }
        }
    }

    public static ResourceDescriptor parse(String s) {
        if (s.startsWith("~")) {
            s = s.substring(1);
        }
        if (alias.containsKey(s)) {
            s = alias.get(s);
        }

        final VarResolver resolver = new VarResolver(s);
        s = Template.substitute(s, resolver);
        return new ResourceDescriptor(s);
    }

    public static ResourceDescriptor of(String s) {
        return parse(s);
    }

    public static ResourceDescriptor construct(String env, String prefix, List<URI> uris,
                                               Map<String, String> params) {
        return new ResourceDescriptor(env, prefix, uris, params);
    }

    public static ResourceDescriptor construct(String env, String prefix, URI uri,
                                               Map<String, String> params) {
        final List<URI> uris = new ArrayList<>();
        uris.add(uri);
        return new ResourceDescriptor(env, prefix, uris, params);
    }

    public static ResourceDescriptor copy(ResourceDescriptor rd) {
        return new ResourceDescriptor(rd.env, rd.prefix, rd.uris, rd.params);
    }

    public void assertUriSizeEq(int n) {
        if (uris.size() != n) {
            throw new IllegalArgumentException("expecting exactly "
                    + n + "uris, got " + uris.size());
        }
    }

    public void assertUriSizeGtEq(int n) {
        if (uris.size() < n) {
            throw new IllegalArgumentException("expecting "
                    + n + " or more uris, got " + uris.size());
        }
    }

    public void assertUriSizeLtEq(int n) {
        if (uris.size() > n) {
            throw new IllegalArgumentException("expecting "
                    + n + " or less uris, got " + uris.size());
        }
    }

    public void assertHasParameter(String... names) {
        for (String name: names) {
            if (! params.containsKey(name)) {
                throw new IllegalArgumentException("missing parameter: " + name);
            }
        }
    }

    public ResourceDescriptor singleUriRd(int index) {
        return ResourceDescriptor.construct(env, prefix, uris.get(index), params);
    }

    public URI getSingleUri() {
        if (uris.size() != 1) {
            throw new IllegalArgumentException("expecting single uri, got " + uris.size());
        }
        return uris.get(0);
    }

    public boolean isPureUri() {
        return prefix == null && params.size() == 0 && uris.size() == 1;
    }

    public boolean hasSingleUri() {
        return uris.size() == 1;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (env != null) {
            sb.append(env).append("::");
        }
        if (prefix != null) {
            sb.append(prefix).append(':');
        }

        if (uris.size() == 1 && params.size() == 0) {
            sb.append(uris.get(0));
        } else {
            String delimiter = "(";
            for (URI uri: uris) {
                sb.append(delimiter).append(uri.toString());
                delimiter = URI_DELIMITER;
            }
            sb.append(")");
            if (params.size() > 0) {
                delimiter = "?";
                for (Map.Entry<String, String> entry: params.entrySet()) {
                    sb.append(delimiter).append(UriUtils.urlEncode(entry.getKey()))
                        .append('=').append(UriUtils.urlEncode(entry.getValue()));
                    delimiter = "&";
                }
            }
        }

        return sb.toString();
    }

    public ResourceOperator op() {
        return Resources.getOperator(this);
    }

    public DataSource source() {
        return op().openSource();
    }

    public DataSink sink() {
        return op().openSink();
    }

    private static class Builder {
        private String env = null;
        private String prefix = null;
        private List<URI> uris = new ArrayList<>();
        private Map<String, String> params = new HashMap<>();

        public Builder setEnv(String env) {
            this.env = env;
            return this;
        }

        public Builder setPrefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        public Builder addUri(URI uri) {
            this.uris.add(uri);
            return this;
        }

        public Builder addUri(String s) {
            this.uris.add(URI.create(s));
            return this;
        }

        public Builder addUris(Collection<URI> uris) {
            this.uris.addAll(uris);
            return this;
        }

        public Builder addParam(String name, String value) {
            params.put(name, value);
            return this;
        }

        public ResourceDescriptor build() {
            return new ResourceDescriptor(env, prefix, uris, params);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}

/*
$ java -Dfilename=my-data -Dformat=tsv xw.data.unistreams.cli.TestRD '${sys:format}:(file://${env:HOME}/${sys:filename}-${version}.${sys:format})?version=1.0'
tsv:(file:///home/hadoop/my-data-1.0.tsv)?version=1.0
* */