package xw.data.unistreams.resources.rcfile;

import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.resources.ResourceOperator;
import xw.data.unistreams.stream.MultipleDataSource;

import java.io.IOException;

/**
 * Created by xhuang on 5/27/16.
 */
public class RCFileOperator extends ResourceOperator
{
    public RCFileOperator(ResourceDescriptor rd) {
        super(rd);
    }

    /*
    recognized prefix:

    rc, rcfile - general rcfile, requires RD parameter version=[1/2]
    rc1, rc2 - rcfile v1 or v2; v2 is the default RC format using binary serde since hive 0.12
    rcfilecat, rccat - cat all data in string format without parsing, read-only, faster. same output as
                    'hive --rcfilecat <file>'
    */
    @Override
    public DataSource openSource() {

        assert rd.prefix != null;

        if (rd.uris.size() == 1) {
            if (rd.prefix.endsWith("cat")) {
                return new RCFileCatSource(rd);
            } else {
                return new RCFileSource(rd);
            }
        } else {
            return new MultipleDataSource(rd);
        }
    }

    @Override
    public DataSink openSink() {
        throw new UnsupportedOperationException("writing to rcfile not supported");
    }

    @Override
    public ResourceMetadata metadata() throws IOException {
        return new RCFileMetadata(rd);
    }
}
