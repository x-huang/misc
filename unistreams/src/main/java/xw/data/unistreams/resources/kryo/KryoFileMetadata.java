package xw.data.unistreams.resources.kryo;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 9/20/16.
 */
public class KryoFileMetadata implements ResourceMetadata
{
    public final ResourceDescriptor rd;

    public KryoFileMetadata(ResourceDescriptor rd) {
        this.rd = rd;
    }

    @Override
    public Object getPOJO() {
        return null;
    }

    @Override
    public Schema getSchema() {
        if (rd.params.containsKey("schema")) {
            String schema = rd.params.get("schema");
            if (! schema.startsWith("struct<")) {
			    schema = "struct<" + schema + ">";
		    }
            return Schema.fromString(schema);
        } else {
            throw new RuntimeException("schema not defined in resource descriptor: " + rd);
        }
    }
}
