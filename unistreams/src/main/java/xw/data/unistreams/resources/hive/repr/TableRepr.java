package xw.data.unistreams.resources.hive.repr;

import org.apache.hadoop.hive.ql.metadata.Table;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@XmlRootElement
public class TableRepr implements Serializable
{
	private static final long serialVersionUID = 7736064656376587835L;
	public final String tableType;
	public final String name;
	public final String database;
	public final String owner;
	public final String dataLocation;
	public final String path;
	public final int createTime;
	public final int lastAccessTime;
	public final String serializationLib;
	public final String protectMode;
	public final int retention; 
	public final int numBuckets;
	public final boolean canDrop;
	public final boolean canWrite;
	public final boolean partitioned;
	public final boolean compressed;
	public final String inputFormat;
	public final String outputFormat;
	
	public final Map<String, String> parameters;
 	public final List<ColumnRepr> columns;
	public final List<ColumnRepr> partitionColumns;
	public final List<ColumnRepr> partitionKeys;

	static protected String translateTableType(Table t) {
		switch (t.getTableType()) {
		case EXTERNAL_TABLE:
			return "external";
		case INDEX_TABLE:
			return "index";
		case MANAGED_TABLE:
			return "managed";
		case VIRTUAL_VIEW:
			return "view";
		default:
			return null;
		}
	}
	
	public TableRepr(Table t) {
		tableType = translateTableType(t);	
		name = t.getTableName();
		database = t.getDbName();
		owner = t.getOwner();
		dataLocation = t.getDataLocation().toString();
		path = t.getPath().toString();
		lastAccessTime = t.getLastAccessTime();
		retention = t.getRetention();
		numBuckets = t.getNumBuckets();
		parameters = t.getParameters();
		canDrop = t.canDrop();
		canWrite = t.canWrite();
		partitioned = t.isPartitioned();
		serializationLib = t.getSerializationLib();
		protectMode = t.getProtectMode().toString();
		
		columns = ColumnRepr.getColumnRepresentations(t.getCols());
		partitionColumns = ColumnRepr.getColumnRepresentations(t.getPartCols());
		partitionKeys = ColumnRepr.getColumnRepresentations(t.getPartitionKeys());
		
		org.apache.hadoop.hive.metastore.api.Table tt = t.getTTable();
		createTime = tt.getCreateTime();
		
		org.apache.hadoop.hive.metastore.api.StorageDescriptor sd = tt.getSd();
		compressed = sd != null && sd.isCompressed();
		inputFormat = (sd == null)? null: sd.getInputFormat();
		outputFormat = (sd == null)? null: sd.getOutputFormat();
	}

    public String getColsString(boolean withPartCols) {
        final StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (ColumnRepr col: columns) {
            sb.append(delimiter).append(col.name);
            delimiter = ",";
        }
        if (withPartCols) {
            for (ColumnRepr col: partitionColumns) {
                sb.append(delimiter).append(col.name);
                delimiter = ",";
            }
        }
        return sb.toString();
    }

    public String getColsString() {
        return getColsString(false);
    }

	public String getTypeString(boolean withPartCols) {
		final StringBuilder sb = new StringBuilder();
		String delimiter = "";
		sb.append("struct<");
		for (ColumnRepr col: columns) {
			sb.append(delimiter).append(col.name).append(':').append(col.type);
			delimiter = ",";
		}
		if (withPartCols) {
			for (ColumnRepr col: partitionColumns) {
				sb.append(delimiter).append(col.name).append(':').append(col.type);
				delimiter = ",";
			}
		}
		sb.append('>');
		return sb.toString();
	}

	public String getTypeString() {
		return getTypeString(false);
	}

	@Override
	public String toString() {
		return "TableRepr{" +
				"tableType='" + tableType + '\'' +
				", name='" + name + '\'' +
				", database='" + database + '\'' +
				", owner='" + owner + '\'' +
				", dataLocation='" + dataLocation + '\'' +
				", path='" + path + '\'' +
				", createTime=" + createTime +
				", lastAccessTime=" + lastAccessTime +
				", serializationLib='" + serializationLib + '\'' +
				", protectMode='" + protectMode + '\'' +
				", retention=" + retention +
				", numBuckets=" + numBuckets +
				", canDrop=" + canDrop +
				", canWrite=" + canWrite +
				", partitioned=" + partitioned +
				", compressed=" + compressed +
				", inputFormat='" + inputFormat + '\'' +
				", outputFormat='" + outputFormat + '\'' +
				", parameters=" + parameters +
				", columns=" + columns +
				", partitionColumns=" + partitionColumns +
				", partitionKeys=" + partitionKeys +
				'}';
	}

	static public String getQualifiedTableName(String dbName, String tblName) {
		return (dbName == null)? tblName : (dbName + "." + tblName); 
	}
}
