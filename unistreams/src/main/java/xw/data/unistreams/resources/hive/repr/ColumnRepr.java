package xw.data.unistreams.resources.hive.repr;

import org.apache.hadoop.hive.metastore.api.FieldSchema;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ColumnRepr implements Serializable
{
	private static final long serialVersionUID = 1323133044144578949L;
	public final String name;
	public final String type;
	public final String comment;
	
	public ColumnRepr(FieldSchema fs) {
		name = fs.getName();
		type = fs.getType();
		comment = fs.getComment();
	}
	
	static public List<ColumnRepr> getColumnRepresentations(List<FieldSchema> fields) {
		ArrayList<ColumnRepr> columns = new ArrayList<>();
		for (FieldSchema fs: fields) {
			columns.add(new ColumnRepr(fs));
		}
		return columns;
	}

	@Override
	public String toString() {
		return "ColumnRepr{" +
				"name='" + name + '\'' +
				", type='" + type + '\'' +
				", comment='" + comment + '\'' +
				'}';
	}
}
