package xw.data.unistreams.resources.text;

import xw.data.unistreams.schema.Schema;

import java.util.Map;

/**
 * Created by xhuang on 7/12/16.
 */
public class RecordTextParser
{
    public static class TextParsingException extends RuntimeException {
        public TextParsingException(String reason, int lineNo, String line) {
            super("error parsing line no. #" + lineNo + " '" + line + "': " + reason);
        }
    }

    private final TextSerdeParameters serdeParams;
    private final Schema schema;
    // private final String[] typenames;

    private RecordTextParser(TextSerdeParameters serdeParams, Schema schema) {
        this.serdeParams = serdeParams;
        this.schema = schema;
    }

    public static RecordTextParser create(String format, Map<String, String> params) {
        final TextSerdeParameters serdeParams = TextSerdeParameters.fromMap(params);
        if (! params.containsKey("schema")) {
            throw new IllegalArgumentException("missing 'schema' in parameters");
        }
        final Schema schema = Schema.fromString(params.get("schema"));
        return new RecordTextParser(serdeParams, schema);
    }

    private int lineNo = 0;

    public Object[] parse(String s) {
        lineNo += 1;
        final String[] splits = s.split(serdeParams.fieldSeparator);
        if (splits.length != schema.size()) {
            throw new TextParsingException("expect " + schema.size()
                    + " items, got " + splits.length, lineNo, s);
        }
        final Object[] record = new Object[schema.size()];
        for (int i=0; i<schema.size(); i++) {
            // TODO
        }
        return record;
    }

}
