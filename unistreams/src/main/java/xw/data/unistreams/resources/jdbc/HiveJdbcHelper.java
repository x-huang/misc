package xw.data.unistreams.resources.jdbc;

import xw.data.unistreams.common.lang.StringUtils;

import java.util.Map;

/**
 * Created by xhuang on 7/12/16.
 */

public class HiveJdbcHelper extends MysqlJdbcHelper
{
    @Override
    public String getDriver() {
        return "org.apache.hive.jdbc.HiveDriver";
    }

    @Override
    public String makeUrl(String serverAddr, String dbName, String username, String password, Map<String, String> extraOptions) {
        return "jdbc:hive2://" + serverAddr + "/" + dbName;
    }

	@Override
	public String makePreparedInsertStatement(String tblName, int nCols, OnDuplicate ins) {
		if (ins != OnDuplicate.THROW_ERROR) {
			throw new UnsupportedOperationException(ins + " is not supported in hivejdbc insertion");
		}

		return "INSERT INTO " + tblName + " VALUES ("
			    + StringUtils.join("?", ", ", nCols) + ")";
	}

	@Override
	public String makePreparedInsertStatement(String tblName, String[] cols, OnDuplicate ins) {
		if (ins != OnDuplicate.THROW_ERROR) {
			throw new UnsupportedOperationException(ins + " is not supported in hivejdbc insertion");
		}

		int nCols = cols.length;
		return "INSERT INTO " + tblName + "(" + StringUtils.join(cols, ", ")
				+ ") VALUES (" + StringUtils.join("?", ", ", nCols) + ")";
	}

    @Override
    public Integer suggestFetchSize() {
        return null;
    }
}
