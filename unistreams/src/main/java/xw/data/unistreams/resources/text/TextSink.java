package xw.data.unistreams.resources.text;


import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataStreamBase;
import org.apache.hadoop.fs.FileSystem;

import java.io.*;
import java.net.URI;

/**
 * Created by xhuang on 6/2/16.
 */
public class TextSink extends DataStreamBase implements DataSink
{
    private final RecordTextFormatter formatter;

    private boolean closeResource = true;
    private PrintStream ps = null;

    public TextSink(ResourceDescriptor rd) {
        super(rd);
        this.formatter = RecordTextFormatter.create(rd.prefix, rd.params);
    }

    @Override
    public void write(Object[] record) throws IOException {
        ps.println(formatter.format(record));
    }

    @Override
    public void close() throws IOException {
        if (ps != null && closeResource) {
            ps.close();
        }
    }

    @Override
    public void initialize() throws IOException {

        final URI uri = rd.getSingleUri();
        switch (uri.getPath()) {
            case "stdout":
            case "/stdout":
                this.ps = System.out;
                this.closeResource = false;
                break;
            case "stderr":
            case "/stderr":
                this.ps = System.err;
                this.closeResource = false;
                break;
            default:
                if (uri.getScheme() == null) {
                    this.ps = new PrintStream(new FileOutputStream(uri.getPath()));
                } else {
                    final FileSystem fs = env().getFileSystem(uri);
                    this.ps = new PrintStream(HadoopCommon.openOutputStream(fs, uri));
                }
        }
    }

}
