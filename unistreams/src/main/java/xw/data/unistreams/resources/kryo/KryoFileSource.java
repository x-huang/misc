package xw.data.unistreams.resources.kryo;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hive.com.esotericsoftware.kryo.Kryo;
import org.apache.hive.com.esotericsoftware.kryo.io.Input;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;
import java.net.URI;

/**
 * Created by xhuang on 9/20/16.
 */
class KryoFileSource extends DataStreamBase implements DataSource
{
    private final static Logger logger = LoggerFactory.getLogger(KryoFileSource.class);

    private final Kryo kryo;
    private Input input = null;

    public KryoFileSource(ResourceDescriptor rd) {
        super(rd);
        this.kryo = new Kryo();
        kryo.register(Object[].class);
    }

    @Override
    public void initialize() throws IOException {
        if (input != null) {
            throw new IllegalStateException("already initialized");
        }
        URI uri = rd.getSingleUri();
        final FileSystem fs = env().getFileSystem(uri);
        this.input = new Input(HadoopCommon.openInputStream(fs, uri));
        logger.debug("kryo file '{}' opened for reading", uri);
    }

    @Override
    public Object[] read() throws IOException {
        return input.eof()? null: kryo.readObject(input, Object[].class);
    }

    @Override
    public void close() throws IOException {
        if (input != null) {
            input.close();
            input = null;
        }
    }
}
