package xw.data.unistreams.resources.jdbc.repr;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

@XmlRootElement
public class TableRepr implements Serializable
{
	private static final long serialVersionUID = -2088919924358382122L;

	public final List<ColumnRepr> columns;

	@Override
	public String toString() {
		return "TableRepr{" +
				"columns=" + columns +
				'}';
	}

	public TableRepr(ResultSetMetaData md) throws SQLException {
		columns = ColumnRepr.getColumnRepresentations(md);
	}

	public String getTypeString() {
		final StringBuilder sb = new StringBuilder();
		String delimiter = "";
		sb.append("struct<");
		for (ColumnRepr col: columns) {
			sb.append(delimiter).append(col.name).append(':').append(col.getHiveType());
			delimiter = ",";
		}
		sb.append('>');
		return sb.toString();
	}

}
