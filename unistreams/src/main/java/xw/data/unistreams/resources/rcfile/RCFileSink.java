package xw.data.unistreams.resources.rcfile;

import org.apache.hadoop.hive.ql.io.RCFileRecordReader;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStructBase;
import org.apache.hadoop.io.LongWritable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;

/**
 * Created by xhuang on 9/22/16.
 */
class RCFileSink extends DataStreamBase implements DataSink
{
 	private final static Logger logger = LoggerFactory.getLogger(RCFileSink.class);

	private String schema = null;
    private int ncols = -1;
	private int version = -1;

    private RCFileRecordReader<LongWritable, BytesRefArrayWritable> reader = null;
	private ColumnarStructBase lazyStruct = null;

	transient final private LongWritable key = new LongWritable();
	transient final private BytesRefArrayWritable value = new BytesRefArrayWritable();

    public RCFileSink(ResourceDescriptor rd) {
        super(rd);
    }


    @Override
    public void write(Object[] record) throws IOException {

    }

    @Override
    public void initialize() throws IOException {

    }

    @Override
    public void close() throws IOException {

    }
}
