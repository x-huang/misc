package xw.data.unistreams.resources.hive.repr;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.metadata.Partition;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@XmlRootElement
public class PartitionRepr implements Serializable
{
	private static final long serialVersionUID = -5698939363905816444L;
	public final boolean canDrop;
	public final boolean canWrite;
	public final List<String> bucketCols;
	public final List<ColumnRepr> columns;
	public final String completeName;
	public final int createTime;
	public final String dataLocation;
	public final String inputFormatClass;
	public final int lastAccessTime;
	public final String location;
	public final String name;
	public final String outputFormatClass;
	public final Map<String, String> parameters;
	public final String partitionPath;
	public final List<String> paths;
	public final String protectMode;
	public final Properties schema;
	public final Map<String, String> spec;
	public final String tableName;
	public final String dbName;
	public final int valuesSize;
	
	private static String getInputFormatClassIgnoreException(Partition p)
	{
		try {
			return p.getInputFormatClass().getName();
		}
		catch (HiveException e) {
			return null;
		}		
	}
	
	private static String getOutputFormatClassIgnoreException(Partition p)
	{
		try {
			return p.getOutputFormatClass().getName();
		}
		catch (HiveException e) {
			return null;
		}
	}
	
	public PartitionRepr(Partition p)
	{
		canDrop = p.canDrop();
		canWrite = p.canWrite();
		bucketCols = p.getBucketCols();
		columns = ColumnRepr.getColumnRepresentations(p.getCols());
		completeName = p.getCompleteName();
		dataLocation = p.getDataLocation().toString();
		inputFormatClass = getInputFormatClassIgnoreException(p);
		lastAccessTime = p.getLastAccessTime();
		location = p.getLocation();
		name = p.getName();
		outputFormatClass = getOutputFormatClassIgnoreException(p);
		parameters = p.getParameters();
		partitionPath = p.getPartitionPath().getName();
		paths = getPathNames(p.getPath());
		protectMode = p.getProtectMode().toString();
		schema = p.getSchema();
		spec = p.getSpec();
		
		org.apache.hadoop.hive.metastore.api.Partition tp = p.getTPartition();
		createTime = tp.getCreateTime();
		tableName = tp.getTableName();
		dbName = tp.getDbName();
		valuesSize = tp.getValuesSize();
	}

	static public List<PartitionRepr> getPartitionRepresentations(List<Partition> partitions)
	{
		ArrayList<PartitionRepr> list = new ArrayList<>();
		for (Partition p: partitions) {
			list.add(new PartitionRepr(p));
		}
		return list;
	}
	
	static public List<String> getPathNames(Path[] paths)
	{
		ArrayList<String> pathNames = new ArrayList<>();
		for (Path p: paths) {
			pathNames.add(p.getName());
		}
		return pathNames;
	}
}
