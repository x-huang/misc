package xw.data.unistreams.resources.jdbc;

import xw.data.unistreams.common.uri.DatabaseUri;
import xw.data.unistreams.common.uri.UriQuery;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.resources.ResourceOperator;
import xw.data.unistreams.resources.jdbc.repr.TableRepr;
import xw.data.unistreams.schema.Schema;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;

import java.io.IOException;
import java.util.*;

public class JdbcResourceOperator extends ResourceOperator
{
    private final transient DatabaseUri dbUri;

	public JdbcResourceOperator(ResourceDescriptor rd) {
		super(rd);
        this.dbUri = DatabaseUri.fromUri(rd.getSingleUri());
	}

	@Override
	public ResourceMetadata metadata() throws IOException {
		final TableRepr table =  JdbcCommon.getTableRepr(
				dbUri.scheme, dbUri.server,
				dbUri.dbName, dbUri.tblName,
				dbUri.user, dbUri.password);

		return new ResourceMetadata() {
            @Override
            public Object getPOJO() {
                return table;
            }

            @Override
			public Schema getSchema() {
				final Schema tblSchema = Schema.fromString(table.getTypeString());
				final String selects = dbUri.getQuery().getParameterAsString("_select");
				if (selects == null) {
					return tblSchema;
				} else {
					final List<String> cols = new ArrayList<>();
					Collections.addAll(cols, selects.split(","));
					return tblSchema.projectionOf(cols);
				}
			}
		};
	}

	@Override
	public DataSource openSource() {
		final UriQuery query = dbUri.getQuery();
		
		final JdbcDataSource jdi = new JdbcDataSource(rd,
                dbUri.scheme, dbUri.server,
                dbUri.dbName, dbUri.tblName,
                dbUri.user, dbUri.password);

        final List<String> extraOpts = query.getPrefixedParameterNames("_extraopt.");
        for (String extraOpt: extraOpts) {
            final String optName = extraOpt.substring("_extraopt.".length());
            jdi.setExtraOption(optName, query.getParameterAsString(extraOpt));
            query.removeParameter(extraOpt);
        }

		jdi.setLimit(query.getParameterAsInteger("_limit"));
		
		final String selects = query.getParameterAsString("_select");
		if (selects != null) {
			jdi.setSelects(selects.split(","));
		}
		
		final String sql = query.getParameterAsString("_sql");
		if (sql != null) {
			jdi.setCustomSelectStatement(sql);
		}
		
		query.removeParameter("_limit")
			 .removeParameter("_select")
			 .removeParameter("_sql");


		jdi.setFilter(query.getAllParameters());
		
		return jdi;
	}


	@Override
	public DataSink openSink() {

		final UriQuery query = dbUri.getQuery();
		
		final JdbcDataSink jdo = new JdbcDataSink(rd,
				dbUri.scheme, dbUri.server,
				dbUri.dbName, dbUri.tblName,
				dbUri.user, dbUri.password
			);

        final List<String> extraOpts = query.getPrefixedParameterNames("_extraopt.");
        for (String extraOpt: extraOpts) {
            final String optName = extraOpt.substring("_extraopt.".length());
            jdo.setExtraOption(optName, query.getParameterAsString(extraOpt));
            query.removeParameter(extraOpt);
        }

		final String selects = query.getParameterAsString("_select");
		if (selects != null) {
            jdo.setSelects(selects.split(","));
        }

		final Integer batchSize = query.getParameterAsInteger("_batchsize");
		final JdbcHelper.OnDuplicate insertMethod =
                JdbcHelper.OnDuplicate.fromString(query.getParameterAsString("_onduplicate"));
		
		query.removeParameter("_select")
		 	 .removeParameter("_onduplicate")
		 	 .removeParameter("_batchsize");
		
		jdo.setInsertMethod(insertMethod);
		
		if (batchSize != null) {
			jdo.setUpdateBatchSize(batchSize);
		}
		
		jdo.setPurgeTable(query.getParameterAsYes("_overwrite"));
		final List<String> overwrites = query.getPrefixedParameterNames("_overwrite.");
		final Map<String, String> spec = new HashMap<>();
		for (String overwrite: overwrites) {
			final String col = overwrite.substring("_overwrite.".length());
			spec.put(col, query.getParameterAsString(overwrite));
            query.removeParameter(overwrite);
		}
		jdo.setPurgeRows(spec);
		
        return jdo;
	}
}
