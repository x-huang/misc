package xw.data.unistreams.resources.hive;

import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.metastore.api.Database;
import org.apache.hadoop.hive.ql.metadata.Hive;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.metadata.Partition;
import org.apache.hadoop.hive.ql.metadata.Table;
import xw.data.unistreams.common.uri.DatabaseUri;
import xw.data.unistreams.env.Environment;
import xw.data.unistreams.resources.hive.repr.DatabaseRepr;
import xw.data.unistreams.resources.hive.repr.PartitionRepr;
import xw.data.unistreams.resources.hive.repr.TableRepr;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.apache.hadoop.hive.metastore.MetaStoreUtils.DEFAULT_DATABASE_NAME;

public class HiveExt
{
	private final Hive hive;

	public HiveExt(HiveConf conf) throws IOException {
		try {
			// https://github.com/spark-jobserver/spark-jobserver/issues/519
            // conf.set("javax.jdo.option.ConnectionURL", "jdbc:derby:memory:myDB;create=true")
			this.hive = Hive.get(conf);
		} catch (HiveException e) {
			throw new IOException("hive initialization error", e);
		}
	}

	private HiveExt(Hive hive) {
		this.hive = hive;
	}

	public static HiveExt wrap(Hive hive) {
		return new HiveExt(hive);
	}

	public static HiveExt ofEnv(String env) throws IOException {
		try {
			return new HiveExt(Environment.of(env).getHive());
		} catch (HiveException e) {
			throw new IOException("hive initialization error", e);
		}
	}


	public DatabaseRepr getDatabaseRepr(URI uri)
			throws IOException {

		final DatabaseUri parser = DatabaseUri.fromUri(uri);
		return getDatabaseRepr(parser.dbName);
	}
	
	public DatabaseRepr getDatabaseRepr(String dbName)
            throws IOException {

		if (dbName == null || dbName.equals("")) {
			dbName = DEFAULT_DATABASE_NAME;
		}

		final Database db;
		try {
			db = hive.getDatabase(dbName);
		} catch (HiveException e) {
			throw new IOException("hive metastore error", e);
		}
		if (db == null) {
			final String msg = "hive database '" + dbName + "' does not exist";
			throw new IOException(msg);
		}
			
		return new DatabaseRepr(db);
	}
	
	public List<Object> getTableReprList(URI uri)
			throws IOException {
		final DatabaseUri parser = DatabaseUri.fromUri(uri);
		return getTableReprList(parser.dbName);
	}
	
	public List<Object> getTableReprList(String dbName)
            throws IOException {

		final ArrayList<Object> tables = new ArrayList<>();
		
		try {
			for (String tblName: hive.getAllTables(dbName)) {
				final Table t = hive.getTable(dbName, tblName, true);
				tables.add(new TableRepr(t));
			}
		} catch (HiveException e) {
			throw new IOException("hive metastore error", e);
		}
		
		return tables;
	}
	
	public TableRepr getTableRepr(URI uri)
            throws IOException {
		final DatabaseUri parser = DatabaseUri.fromUri(uri);
		return getTableRepr(parser.dbName, parser.tblName);
	}
	
	public TableRepr getTableRepr(String dbName, String tblName)
            throws IOException {

		if (dbName == null || dbName.equals(""))
			dbName = DEFAULT_DATABASE_NAME;
		
		final Table tbl;
		try {
			tbl = hive.getTable(dbName, tblName);
		} catch (HiveException e) {
			throw new IOException("hive metastore error", e);
		}
		
		if (tbl == null) {
			final String fullName = dbName + "." + tblName;
			final String msg = "hive table '" + fullName + "' does not exist";
			throw new IOException(msg);
		}
		
		return new TableRepr(tbl);
	}
	
	public List<PartitionRepr> getPartitionReprList(URI uri)
			throws IOException {

		final DatabaseUri parser = DatabaseUri.fromUri(uri);
		final Map<String, String> spec = parser.getQuery().getAllParameters();
		return getPartitionReprList(parser.dbName, parser.tblName, spec);
	}
	
	public List<PartitionRepr> getPartitionReprList(
            String dbName, String tblName, Map<String, String> spec)
			throws IOException {

		if (dbName == null || dbName.equals(""))
			dbName = DEFAULT_DATABASE_NAME;
		
		final List<Partition> ptns;
		try {
			final Table tbl = hive.getTable(dbName, tblName);
			if (tbl == null) {
				final String fullName = dbName + "." + tblName;
				final String msg = "hive table '" + fullName + "' does not exist";
				throw new IOException(msg);
			}
			ptns = hive.getPartitions(tbl, spec);
		} catch (HiveException e) {
			throw new IOException("hive metastore error", e);
		}
		
		final ArrayList<PartitionRepr> list = new ArrayList<>();
		for (Partition p: ptns) {
			list.add(new PartitionRepr(p));
		}
		return list;
	}
	
	public PartitionRepr getPartitionRepr(URI uri)
            throws IOException {
		final DatabaseUri parser = DatabaseUri.fromUri(uri);
		final Map<String, String> spec = parser.getQuery().getAllParameters();
		return getPartitionRepr(parser.dbName, parser.tblName, spec);
	}
	
	public PartitionRepr getPartitionRepr(
            String dbName, String tblName, Map<String, String> spec)
			throws IOException {

		final Partition partition;
		try {
			final Table tbl = (dbName == null || dbName.isEmpty()) ?
						hive.getTable(tblName) : hive.getTable(dbName, tblName);
			if (tbl == null) {
				String fullName = dbName + "." + tblName;
				String msg = "hive table '" + fullName + "' does not exist";
				throw new IOException(msg);
			}
			partition = hive.getPartition(tbl, spec, false);
		} catch (HiveException e) {
			String msg = "hive metastore error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		if (partition == null) {
			String fullName = dbName + "." + tblName;
			String msg = "no such partition in table '" + fullName;
			throw new IOException(msg);
		}
		return new PartitionRepr(partition);
	} 
}
