package xw.data.unistreams.resources.dummy;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;

/**
 * Created by xhuang on 5/31/16.
 */
public class NullDataSink  extends DataStreamBase implements DataSink
{
    public static final NullDataSink singleton =
            new NullDataSink(ResourceDescriptor.parse("dummy:(null)"));

    private NullDataSink(ResourceDescriptor rd) {
        super(rd);
    }

    public void write(Object[] record) {
        // do nothing
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public void initialize() {
    }

}
