package xw.data.unistreams.resources.orc;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.CompressionKind;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.Writer;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataStreamBase;

class OrcFileSink extends DataStreamBase implements DataSink
{
	private final static Logger logger = LoggerFactory.getLogger(OrcFileSink.class);

	private String schema;
	private Writer writer = null;
	private long stripeSize = 102_400L;	// NOTE: stripe size must be a multiple of io.bytes.per.checksum, IOException thrown otherwise
	private int bufferSize = 16_000;	// NOTE: shown in TBLPROPERTIES/orc.compress.size
	private boolean blockPadding;
	private boolean overwrite = false;
	// private CompressionKind compress = CompressionKind.SNAPPY;  // NOTE: shown in TBLPROPERTIES/orc.compress; http://blog.erdemagaoglu.com/post/4605524309/lzo-vs-snappy-vs-lzf-vs-zlib-a-comparison-of
    private CompressionKind compress = CompressionKind.ZLIB;  // https://community.hortonworks.com/questions/4067/snappy-vs-zlib-pros-and-cons-for-each-compression.html

	public OrcFileSink(ResourceDescriptor rd, String schema) {
		super(rd);
		this.schema = schema;
		blockPadding = ! rd.getSingleUri().getScheme().equals("s3n");
	}

    public OrcFileSink(ResourceDescriptor rd) {
        this(rd, null);
    }

	public OrcFileSink setStripeSize(long size) {
		if (size <= 0) {
			throw new IllegalArgumentException("zero or negative stripe size: " + size);
		}
		stripeSize = size;
		return this;
	}
	
	public OrcFileSink setBufferSize(int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("zero or negative buffer size: " + size);
		}
		bufferSize = size;
		return this;
	}
	
	public OrcFileSink setBlockPadding(boolean padding) {
		blockPadding = padding;
		return this;
	}
	
	public OrcFileSink setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
		return this;
	}
	
	public OrcFileSink setCompress(String compress) {
		this.compress = CompressionKind.valueOf(compress);
		return this;
	}

	@Override
	public void initialize() throws IOException {

        if (schema == null && context.containsKey("schema")) {
            schema = (String) context.get("schema");
            logger.debug("set schema from connection context: {}", schema);
        }

        if (schema == null) {
            throw new RuntimeException("orc file schema not set, " +
                    "check the temporal order of initialize(ctxt) of src and dest streams");
        }
        if (! schema.startsWith("struct<")) {
            schema = "struct<" + schema + ">";
        }

		final URI uri = rd.getSingleUri();
		final Path path = new Path(uri);

        final ObjectInspector oi = ObjectInspectorUtils.getStructObjectInspector(schema);
		assert oi.getTypeName().equals(schema);

        final Configuration conf = env().getHadoopConf();
		final OrcFile.WriterOptions opts = OrcFile.writerOptions(conf)
				.inspector(oi)
				.stripeSize(stripeSize)
				.bufferSize(bufferSize)
				.blockPadding(blockPadding)
				.compress(compress);
		
		if (overwrite && HadoopCommon.delete(env().getFileSystem(uri), uri)) {
			logger.debug("deleted existing file: {}", uri);
		}
		
		writer = OrcFile.createWriter(path, opts);
		
		logger.debug("initialized writing to ORC file: {} " +
						"(compress: {}, compress-size: {}, " +
						"stripe-size: {}, block-padding: {}, typename: {})",
				uri, compress, bufferSize, stripeSize, blockPadding,
				ObjectInspectorUtils.describeObjectInspector(oi));
	}
	
	public String getSchema() {
		return schema;
	}
	
	public URI getLocationUri() {
		return rd.getSingleUri();
	}
	
	@Override
	public void close() throws IOException {
		writer.close();
	}

	@Override
	public void write(Object[] record) throws IOException {
        writer.addRow(record);
	}
}
