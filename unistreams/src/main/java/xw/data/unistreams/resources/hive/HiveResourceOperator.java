package xw.data.unistreams.resources.hive;

import xw.data.unistreams.common.uri.DatabaseUri;
import xw.data.unistreams.common.uri.UriQuery;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceOperator;

import java.io.IOException;
import java.util.Map;

/**
 * Created by xhuang on 6/2/16.
 */
public class HiveResourceOperator extends ResourceOperator
{
    public HiveResourceOperator(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public DataSource openSource() {
        final DatabaseUri dbUri = DatabaseUri.fromUri(rd.getSingleUri());
        final HiveDataSource hdi = new HiveDataSource(rd, dbUri.dbName, dbUri.tblName);

        final UriQuery query = dbUri.getQuery();
        for (Map.Entry<String, String> entry: query.getAllParameters().entrySet()) {
            if (! entry.getKey().startsWith("_")) {
                hdi.addQuerySpec(entry.getKey(), entry.getValue());
            }
        }
        if (query.hasParameter("_limit")) {
            hdi.setLimit(Integer.parseInt(query.getParameterAsString("_limit")));
        }
        if (query.getParameterAsYes("_allowpartialspec")) {
            hdi.setAllowPartialSpec(true);
        }

        return hdi;
    }

    @Override
    public DataSink openSink() {
        throw new UnsupportedOperationException("writing to hive not supported");
    }

    @Override
    public ResourceMetadata metadata() throws IOException {
        final DatabaseUri dbUri = DatabaseUri.fromUri(rd.getSingleUri());
        final HiveExt hive = HiveExt.ofEnv(rd.env);
        return new HiveTableMetadata(hive.getTableRepr(dbUri.dbName, dbUri.tblName));
    }
}
