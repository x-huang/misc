package xw.data.unistreams.resources.dummy;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;

/**
 * Created by xhuang on 5/31/16.
 *
 * dummy:empty
 */
public class EmptyDataSource extends DataStreamBase implements DataSource
{
    public static final EmptyDataSource singleton =
            new EmptyDataSource(ResourceDescriptor.parse("dummy:(empty)"));

    private EmptyDataSource(ResourceDescriptor rd) {
        super(rd);
    }

    @Override
    public Object[] read() throws IOException {
        return null;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void close() throws IOException {
    }
}
