package xw.data.unistreams.resources.text;

import xw.data.unistreams.common.lang.StringEscapeUtils;

import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 5/26/16.
 */
public class RecordTextFormatter
{
    private final TextSerdeParameters serdeParams;

    private RecordTextFormatter(TextSerdeParameters serdeParams) {
        this.serdeParams = serdeParams;
    }

    private transient final StringBuilder sb = new StringBuilder();

    public String format(Object[] record) {
        sb.setLength(0);
        if (serdeParams.rowOpening != null) {
            sb.append(serdeParams.rowOpening);
        }
        String delimiter = "";
        for (Object datum : record) {
            sb.append(delimiter);
            serializeObject(datum);
            delimiter = serdeParams.fieldSeparator;
        }
        if (serdeParams.rowClosing != null) {
            sb.append(serdeParams.rowClosing);
        }
        return sb.toString();
    }

    private void serializeObject(Object o) {
        if (o == null) {
            sb.append(serdeParams.nullString);
        } else if (o instanceof List) {
            serializeList((List) o);
        } else if (o instanceof Map) {
            //noinspection unchecked
            serializeMap((Map<Object, Object>) o);
        } else if (o instanceof String) {
            serializeString((String) o);
        } else {
            sb.append(o.toString());
        }
    }

    private void serializeString(String s) {
        if (serdeParams.wrapString) {
            sb.append('"');
        }
        sb.append(StringEscapeUtils.escapeJava(s));
        if (serdeParams.wrapString) {
            sb.append('"');
        }
    }

    private void serializeList(List list) {
        String delim = "";
        if (serdeParams.wrapArray) {
            sb.append('[');
        }
        for (Object item : list) {
            sb.append(delim);
            serializeObject(item);
            delim = serdeParams.itemSeparator;
        }
        if (serdeParams.wrapArray) {
            sb.append(']');
        }
    }

    private void serializeMap(Map<Object, Object> map) {
        String delim = "";
        if (serdeParams.wrapMap) {
            sb.append('{');
        }
        for (Map.Entry entry : map.entrySet()) {
            sb.append(delim);
            serializeObject(entry.getKey());
            sb.append(serdeParams.kvSeparator);
            serializeObject(entry.getValue());
            delim = serdeParams.itemSeparator;
        }
        if (serdeParams.wrapMap) {
            sb.append('}');
        }
    }


    public static RecordTextFormatter create(String format, Map<String, String> params) {
        final TextSerdeParameters serdeParams = TextSerdeParameters.fromMap(params);
        return new RecordTextFormatter(serdeParams);
    }
}
