package xw.data.unistreams.resources.avro;

import org.apache.avro.Schema;
import org.apache.hadoop.hive.serde2.avro.TypeInfoToSchema;
import org.apache.hadoop.hive.serde2.typeinfo.StructTypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 7/8/16.
 */
public class AvroSchemaUtils
{
    public static Schema fromStructTypeInfo(StructTypeInfo sti) {
        final TypeInfoToSchema converter = new TypeInfoToSchema();
        final List<String> colNames = sti.getAllStructFieldNames();
        final List<TypeInfo> colTypes = sti.getAllStructFieldTypeInfos();
        final List<String> colComments = new ArrayList<>();
        return converter.convert(colNames, colTypes, colComments, "xw.data.unistreams.resources.avro", "custom", "record");
    }
}
