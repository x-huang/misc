package xw.data.unistreams.resources.hive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.common.uri.DatabaseUri;
import xw.data.unistreams.common.uri.UriQuery;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.resources.ResourceOperator;
import xw.data.unistreams.resources.hive.repr.TableRepr;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataSink;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by xhuang on 6/7/16.
 */
public class HiveDirectResourceOperator extends ResourceOperator
{
    static final Logger logger = LoggerFactory.getLogger(HiveDirectResourceOperator.class);

    private final transient DatabaseUri dbUri;

    public HiveDirectResourceOperator(ResourceDescriptor rd) {
        super(rd);
        this.dbUri = DatabaseUri.fromUri(rd.getSingleUri());
    }

    @Override
    public DataSource openSource() {
        final TableRepr table;
        try {
            table = HiveExt.ofEnv(rd.env).getTableRepr(dbUri.dbName, dbUri.tblName);
        } catch (IOException e) {
            throw new RuntimeException("cannot get hive table metadata: " + e.getMessage(), e);
        }
        logger.debug("table {}.{}: input-format = {}", table.database, table.name, table.inputFormat);
        switch (table.inputFormat) {
            /*
            case "org.apache.hadoop.hive.ql.io.RCFileInputFormat":
                logger.debug("hive-direct => rcfile");
                return openRCFileDirectInput(table);

            case "org.apache.hadoop.hive.ql.io.orc.OrcInputFormat":
                logger.debug("hive-direct => orc");
                return openOrcDirectInput(table);
            */
            default:
                logger.debug("hive-direct => hive");
                return openHiveDataInput();
        }
    }

    private DataSource openHiveDataInput() {
        final Map<String, String> querySpec = new HashMap<>();
        for (Map.Entry<String, String> entry: dbUri.getQuery().getAllParameters().entrySet()) {
            if (! entry.getKey().startsWith("_")) {
                querySpec.put(entry.getKey(), entry.getValue());
            }
        }
        final HiveDataSource hdi = new HiveDataSource(rd, dbUri.dbName, dbUri.tblName);
        final UriQuery query = dbUri.getQuery();
        if (query.hasParameter("_limit")) {
            hdi.setLimit(Integer.parseInt(query.getParameterAsString("_limit")));
        }
        if (query.getParameterAsYes("_allowpartialspec")) {
            hdi.setAllowPartialSpec(true);
        }
        return hdi;
    }

    private DataSource openRCFileDirectInput(TableRepr table) {
        return null;
    }

    @Override
    public DataSink openSink() {
        throw new UnsupportedOperationException("writing to hive not supported");
    }

    @Override
    public ResourceMetadata metadata() throws IOException {
        return new HiveTableMetadata(HiveExt.ofEnv(rd.env).getTableRepr(dbUri.dbName, dbUri.tblName));
    }


}
