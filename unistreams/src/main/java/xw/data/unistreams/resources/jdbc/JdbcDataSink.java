package xw.data.unistreams.resources.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataStreamBase;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class JdbcDataSink extends DataStreamBase implements DataSink
{
    static final Logger logger = LoggerFactory.getLogger(JdbcDataSink.class);

	protected final String scheme;
	protected final String server;
	protected final String dbName;
	protected final String tblName;
	protected final String user;
	protected final String password;
    protected Map<String, String> extraOptions = null;
	protected String[] selects = null;
	protected JdbcHelper.OnDuplicate insertMethod = JdbcHelper.OnDuplicate.THROW_ERROR;
	protected boolean purgeTable = false;
	protected Map<String, String> purgeRowsSpec = new HashMap<>();
	protected int batchSize = 1000;
	private int batchCount = 0;
	private int[] sqlTypes = null;
	private Connection conn= null;
	private PreparedStatement stmt = null;

	public JdbcDataSink(ResourceDescriptor rd,
						String scheme, String server,
						String dbName, String tblName,
						String user, String password) {
		super(rd);
		this.scheme = scheme;
		this.server = server;
		this.dbName = dbName;
		this.tblName = tblName;
		this.user = user;
		this.password = password;
	}

    public JdbcDataSink setExtraOption(String name, String value) {
        if (extraOptions == null) {
            extraOptions = new HashMap<>();
        }
        extraOptions.put(name, value);
        return this;
    }

	public JdbcDataSink setSelects(String[] selects) {
		this.selects = (selects == null? null : selects.clone());
		return this;
	}
	
	public JdbcDataSink setUpdateBatchSize(int size) {
		batchSize = size;
		return this;
	}
	
	public JdbcDataSink setPurgeTable(boolean purge) {
		purgeTable = purge;
		return this;
	}
	
	public JdbcDataSink setInsertMethod(JdbcHelper.OnDuplicate ins) {
		insertMethod = ins;
		return this;
	}
	
	public JdbcDataSink setPurgeRows(Map<String, String> spec) {
		purgeRowsSpec.clear();
		purgeRowsSpec.putAll(spec);
		return this;
	}
	
	public void initialize() throws IOException {
		final JdbcHelper helper = JdbcHelpers.getHelper(scheme);
		
		String driver = helper.getDriver();
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("cannot find jdbc driver", e);
		}

		final String jdbcUrl = helper.makeUrl(server, dbName, user, password, extraOptions);
		logger.debug("using jdbc url: {}", jdbcUrl);

		try {
			conn = DriverManager.getConnection(jdbcUrl);
		} catch (SQLException e) {
			throw new IOException("connection error: " + e.getMessage(), e);
		}

		final String sqlDelete;
		if (purgeTable) {
			sqlDelete = helper.makeDeleteStatement(tblName);
		}
		else if (purgeRowsSpec.size() > 0) {
			sqlDelete = helper.makeDeleteStatement(tblName, purgeRowsSpec);
		}
		else {
			sqlDelete = null;
		}

		try {
			if (sqlDelete != null) {
				logger.debug("executing sql: {}", sqlDelete);
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sqlDelete);
			}
		} catch (SQLException e) {
            throw new IOException("sql error: " + e.getMessage(), e);
        }
		
		final String sqlNoop = helper.makeNoopSelectStatement(tblName);
		logger.debug("executing sql: {}", sqlNoop);
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sqlNoop);
			final ResultSetMetaData metadata = rs.getMetaData();
			final int nCols = metadata.getColumnCount();
			final String sqlInsert;
			if (selects == null) {
				sqlTypes = new int[nCols];
				for (int i = 0; i < nCols; i++) {
					if (metadata.isAutoIncrement(i + 1)) {
						String colName = metadata.getColumnName(i + 1);
						throw new SQLException("cannot insert into auto increment column: " + colName);
					}
					sqlTypes[i] = metadata.getColumnType(i + 1);
				}
				sqlInsert = helper.makePreparedInsertStatement(tblName, nCols, insertMethod);
			} else {
				final Map<String, Integer> columnNameToIndexMap = new HashMap<>();
				for (int i = 1; i <= nCols; i++) {
					columnNameToIndexMap.put(metadata.getColumnName(i).toLowerCase(), i);
				}
				sqlTypes = new int[selects.length];
				for (int i = 0; i < sqlTypes.length; i++) {
					String colName = selects[i];
					String lowercase = colName.toLowerCase();
					if (lowercase.startsWith("`") && lowercase.endsWith("`")) {
						lowercase = lowercase.substring(1, lowercase.length() - 1);
					}
					if (!columnNameToIndexMap.containsKey(lowercase)) {
						throw new SQLException("column not found in specified insert statement: " + colName);
					}
					int index = columnNameToIndexMap.get(lowercase);
					if (metadata.isAutoIncrement(index)) {
						throw new SQLException("cannot insert into auto increment column: " + colName);
					}
					sqlTypes[i] = metadata.getColumnType(index);
				}
				sqlInsert = helper.makePreparedInsertStatement(tblName, selects, insertMethod);
			}

			logger.debug("prepared sql: {}", sqlInsert);
			this.stmt = conn.prepareStatement(sqlInsert);
			conn.setAutoCommit(false);
		} catch (SQLException e) {
            throw new IOException("sql error: " + e.getMessage(), e);
        }
	}
	
	@Override
	public void write(Object[] record) throws IOException {

        for (int i=0; i<record.length; i++) {
			final Object value = record[i];
			try {
                if (value == null) {
        			stmt.setNull(i+1, sqlTypes[i]);
		        } else {
    		    	stmt.setObject(i+1, value, sqlTypes[i], 4); // scaleOrLength = 4
	        	}
			} catch (SQLException e) {
				throw new IOException("sql error preparing statement", e);
			}
		}
		
		try {
			stmt.addBatch();
		} catch (SQLException e) {
			throw new IOException("sql error adding batch", e);
		}
		
		batchCount++;

		if (batchCount == batchSize) {
			try {
				stmt.executeBatch();
                conn.commit();
			} catch (SQLException e) {
				throw new IOException("sql error executing insert statement", e);
			}
			batchCount = 0;
		}
	}

	@Override
	public void close() throws IOException {
		if (batchCount > 0) {
			try {
				stmt.executeBatch();
                conn.commit();
			} catch (SQLException e) {
				throw new IOException("sql error executing insert statemen", e);
			}
			batchCount = 0;
		}

		try {
			if (conn != null) {
                conn.close();
            }
		} catch (SQLException e) {
			throw new IOException("sql error disconnecting", e);
		}
	}
}
