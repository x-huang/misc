package xw.data.unistreams.resources.text;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 8/28/16.
 */
public class TextFileMetadata implements ResourceMetadata
{
    public final ResourceDescriptor rd;

    public TextFileMetadata(ResourceDescriptor rd) {
        this.rd = rd;
    }

    @Override
    public Object getPOJO() {
        return null;
    }

    @Override
    public Schema getSchema() {
        if (rd.params.containsKey("schema")) {
            return Schema.fromString(rd.params.get("schema"));
        } else {
            throw new RuntimeException("schema not defined in resource descriptor: " + rd);
        }
    }

}
