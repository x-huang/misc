package xw.data.unistreams.resources.rcfile;

import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.schema.Schema;
import xw.data.unistreams.schema.TypeUtils;

/**
 * Created by xhuang on 8/28/16.
 */
public class RCFileMetadata implements ResourceMetadata
{
    public final ResourceDescriptor rd;

    public RCFileMetadata(ResourceDescriptor rd) {
        this.rd = rd;
    }

    @Override
    public Object getPOJO() {
        return null;
    }

    @Override
    public Schema getSchema() {
        assert rd.prefix != null;
        if (rd.prefix.endsWith("cat")) {
            if (rd.params.containsKey("ncols")) {
                final int ncols = Integer.parseInt(rd.params.get("ncols"));
                final Schema.Builder builder = Schema.newBuilder();
                for (int i=0; i<ncols; i++) {
                    builder.addColumn("_col" + i, TypeUtils.stringType);
                }
                return builder.build();
            } else {
                throw new RuntimeException("'ncols' not defined in resource descriptor: " + rd);
            }
        } else {
            if (rd.params.containsKey("schema")) {
                return Schema.fromString(rd.params.get("schema"));
            } else {
                throw new RuntimeException("'schema' not defined in resource descriptor: " + rd);
            }
        }
    }

}
