package xw.data.unistreams.resources.hive;

import xw.data.unistreams.resources.ResourceMetadata;
import xw.data.unistreams.resources.hive.repr.TableRepr;
import xw.data.unistreams.schema.Schema;

/**
 * Created by xhuang on 9/16/16.
 */
public class HiveTableMetadata implements ResourceMetadata
{
    public final TableRepr repr;

    public HiveTableMetadata(TableRepr repr) {
        this.repr = repr;
    }

    @Override
    public Object getPOJO() {
        return repr;
    }

    @Override
    public Schema getSchema() {
        return Schema.fromString(repr.getTypeString(/*withPartitionCols=*/true));
    }
}
