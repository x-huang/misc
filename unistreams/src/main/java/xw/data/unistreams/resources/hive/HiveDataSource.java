package xw.data.unistreams.resources.hive;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.common.type.HiveBaseChar;
import org.apache.hadoop.hive.common.type.HiveDecimal;
import org.apache.hadoop.hive.metastore.api.FieldSchema;
import org.apache.hadoop.hive.ql.exec.FetchOperator;
import org.apache.hadoop.hive.ql.exec.Utilities;
import org.apache.hadoop.hive.ql.exec.mr.ExecDriver;
import org.apache.hadoop.hive.ql.metadata.Hive;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.metadata.Partition;
import org.apache.hadoop.hive.ql.metadata.Table;
import org.apache.hadoop.hive.ql.plan.FetchWork;
import org.apache.hadoop.hive.ql.plan.PartitionDesc;
import org.apache.hadoop.hive.ql.plan.TableDesc;
import org.apache.hadoop.hive.serde2.objectinspector.InspectableObject;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorUtils;
import org.apache.hadoop.mapred.JobConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreamBase;
import xw.data.unistreams.resources.hive.repr.TableRepr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 6/2/16.
 */
public class HiveDataSource extends DataStreamBase implements DataSource
{
	static final Logger logger = LoggerFactory.getLogger(HiveDataSource.class);

    public final String dbName;
	public final String tblName;
	public Map<String, String> querySpec;

    private FetchOperator ftOp = null;
	private int limit = -1;
    private boolean allowPartialSpec = false;
	private int count = 0;

	// Xiaowan: there is a bug in Hive 0.10.0.
	// Cannot rely on InspectableObject.oi which is returned by FetchOperator.getNextRow().
	// The bug is obvious since variable 'rowObjectInspector' in FetchOperator.java is never set.
	// It affects partitioned table only.
	//
	private ObjectInspector oi = null;	// for Hive 0.10.0

    public HiveDataSource(ResourceDescriptor rd, String dbName, String tblName) {
		super(rd);
		this.dbName = dbName;
		this.tblName = tblName;
		this.querySpec = new HashMap<>();
    }

    public HiveDataSource addQuerySpec(String col, String value) {
        this.querySpec.put(col, value);
        return this;
    }

	public HiveDataSource setLimit(int limit) {
		this.limit = limit;
		return this;
	}

    public HiveDataSource setAllowPartialSpec(boolean flag) {
        this.allowPartialSpec = flag;
        return this;
    }

	static protected FetchWork createFetchWork(Hive hive, Table table, int limit,
											   Map<String, String> partSpec)
			throws HiveException {

		final TableDesc tblDesc = Utilities.getTableDesc(table);

		if (!table.isPartitioned()) {
			return new FetchWork(table.getPath(), tblDesc, limit);
		}

		final List<Partition> ptns = hive.getPartitions(table, partSpec);
		final List<Path> listP = new ArrayList<>();
		final List<PartitionDesc> listD = new ArrayList<>();
		for (Partition p : ptns) {
			listP.add(p.getPartitionPath());
			listD.add(Utilities.getPartitionDesc(p));
		}

		// return new FetchWork(listP, listD, limit);	// for Hive 0.9.0
		return new FetchWork(listP, listD, tblDesc, limit);	// for Hive 0.10.0
	}

	private boolean checkPartitionSpec(Table table) {
        if (allowPartialSpec) {
            return true;
        }

		for (FieldSchema partCol: table.getPartCols()) {
			final String colName = partCol.getName();
			if (! querySpec.containsKey(colName)) {
				throw new IllegalArgumentException("missing partition specification: " + colName);
			}
		}
		return true;
	}

	@Override
	public void initialize() throws IOException {

        final Table table;
        try {
            final Hive hive = env().getHive();
            table = hive.getTable(dbName, tblName);
            checkPartitionSpec(table);

            final FetchWork fetch = createFetchWork(hive, table, limit, querySpec);
            final JobConf job = new JobConf(hive.getConf(), ExecDriver.class);

            this.ftOp = new FetchOperator(fetch, job);
            this.oi = ftOp.getOutputObjectInspector();    // for Hive 0.10.0
        } catch (HiveException e) {
            throw new IOException("cannot initialize hive data source: " + e.getMessage(), e);
        }

        context.put("schema", new TableRepr(table).getTypeString(true));
        logger.debug("set schema: ", context.get("schema"));
        logger.debug("hive data source initialized");
	}

    @Override
    public Object[] read() throws IOException {

		while (true) {
			if (limit > 0 && count >= limit) {
                return null;
            }
			InspectableObject io = ftOp.getNextRow();
			if (io == null) {
				// retry to let fetch operator reset record reader
				io = ftOp.getNextRow();
			}
			if (io == null || io.o == null) {
				return null;
			}

			final Object data = ObjectInspectorUtils.copyToStandardJavaObject(io.o, oi);	// for Hive 0.10.0
			// Object data = ObjectInspectorUtils.copyToStandardJavaObject(io.o, io.oi); // for Hive 0.9.0
			if (data != null) {
				count++;
                final List<?> list = (List<?>) data;
                final Object[] record = new Object[list.size()];
				for (int i=0; i<record.length; i++) {
					final Object datum = list.get(i);
					if (datum instanceof HiveDecimal) {
                        // cast HiveDecimal to BigDecimal to avoid client hive dependency.
						record[i] = ((HiveDecimal) datum).bigDecimalValue();
					} else if (datum instanceof HiveBaseChar) {
                        // cast HiveChar and HiveVarchar to String to avoid client hive dependency.
                        record[i] = ((HiveBaseChar) datum).getValue();
                    } else {
						record[i] = datum;
					}
				}
				return record;
			}
		}
    }

    @Override
    public void close() throws IOException {
		try {
			if (ftOp != null)
				ftOp.clearFetchContext();
		} catch (HiveException e) {
			throw new IOException("hive error clearing fetch context", e);
		}
    }
}
