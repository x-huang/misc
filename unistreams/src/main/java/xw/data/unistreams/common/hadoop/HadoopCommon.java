package xw.data.unistreams.common.hadoop;

import org.apache.hadoop.fs.*;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class HadoopCommon
{
	static public Object getFileChecksum(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		return fs.getFileChecksum(path);
	}
	
	static public boolean delete(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		return fs.delete(path, true);	// delete recursively
	}

	static public List<String> listFiles(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		final FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		final FileStatus[] statuses = fs.listStatus(path);
		final List<String> files = new ArrayList<>();
		for (FileStatus s: statuses) {
			if (!s.isDirectory()) {
				files.add(s.getPath().getName());
			}
		}
		return files;
	}
	
	static private void addFilesRecursively(FileSystem fs, String base, String subdir,
											List<String> files) throws IOException {
		final Path path = new Path(base + subdir);
		final FileStatus status = fs.getFileStatus(path);
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + (base + subdir));
		}
		
		final FileStatus[] statuses = fs.listStatus(path);
		for (FileStatus s: statuses) {
			final String segName = s.getPath().getName();
			if (s.isDirectory()) {
				addFilesRecursively(fs, base, subdir + segName + "/", files);
			}
			else {
				files.add(subdir + segName);
			}
		}
	}
	
	static public List<String> listFilesRecursively(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		final FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		final List<String> files = new ArrayList<>();
		addFilesRecursively(fs, uri.getPath(), "", files);
		return files;
	}
	
	static public List<String> listEntries(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		final FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		final FileStatus[] statuses = fs.listStatus(path);
		final List<String> entries = new ArrayList<>();
		for (FileStatus s: statuses) {
			if (s.isDirectory()) {
				entries.add(s.getPath().getName() + "/");
			} else {
				entries.add(s.getPath().getName());
			}
		}
		return entries;
	}

	static public DirectoryRepr getDirectoryRepr(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		return new DirectoryRepr(fs, path);
	}

	static public FileStatusRepr getFileStatusRepr(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		return new FileStatusRepr(fs, path);
	}

	static public FSDataInputStream openInputStream(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		return fs.open(path);
	}
	static public FSDataOutputStream openOutputStream(FileSystem fs, URI uri) throws IOException {
		final Path path = new Path(uri.getPath());
		final Path parentPath = path.getParent();
		if (parentPath != null) {
			fs.mkdirs(path.getParent());
		}
		return fs.create(path, true);
	}
}
