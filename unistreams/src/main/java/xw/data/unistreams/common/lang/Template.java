package xw.data.unistreams.common.lang;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xhuang on 6/7/16.
 */
public class Template
{
    public interface Evaluator {
        String eval(Term term);
    }

    public abstract static class Term {
        public String instantiate(Evaluator evaluator) {
            return evaluator.eval(this);
        }
    }

    public static class Literal extends Term {
        final public String s;
        protected Literal(String s) { this.s = s; }
        @Override
        final public String instantiate(Evaluator evaluator) {
            return s;
        }
        @Override
        public String toString() { return s; }
    }

    public static class Variable extends Term {
        final public String prefix;
        final public String var;
        protected Variable(String p, String v) {
            prefix = p;
            var = v;
        }
        public String fullName() {
            return (prefix == null)? var: (prefix + ":" + var);
        }
        @Override
        public String toString() {
            return "${" + fullName() + '}';
        }
    }

    public static class Function extends Term {
        final public String func;
        final public List<String> args;
        protected Function(String s) {
            int idx1 = s.indexOf('(');
            int idx2 = s.indexOf(')');
            assert idx1 > 0 && idx2 > idx1;
            this.func = s.substring(0, idx1);
            this.args = new ArrayList<>();
            String[] splits = s.substring(idx1+1, idx2).split(",");
            for (String param: splits) {
                String p = param.trim();
                if (! p.isEmpty())
                    args.add(p);
            }
        }
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(func).append('(');
            boolean first = true;
            for (String s: args) {
                if (first)
                    first = false;
                else
                    sb.append(',');
                sb.append(s);
            }
            return sb.append(')').toString();
        }
    }

    static class Lexer {
        // the template variable regex: $var or ${var}
        public static final Pattern regex = Pattern.compile("(\\$\\{\\w+\\})|(\\$\\{\\w+:[^\\{\\}/:]+\\})|(\\$\\{\\w+\\([^\\$\\(\\)\\{\\}]*\\)\\})");
        public static final Pattern varRegex = Pattern.compile("(\\$\\w+)|(\\$\\{\\w+\\})|(\\$\\{\\w+:[^\\{\\}/:]+\\})");
        public static final Pattern funcRegex = Pattern.compile("(\\$\\{\\w+\\([^\\$\\(\\)\\{\\}]*\\)\\})");

        static public List<String> parse(String s) {
            final List<String> tokens = new ArrayList<>();
            final Matcher matcher = regex.matcher(s);

            int start;
            int end = 0;
            while (matcher.find()) {
                start = matcher.start();
                if (start > end) {
                    tokens.add(s.substring(end, start));
                }
                end = matcher.end();
                tokens.add(s.substring(start, end));
            }

            if (s.length() > end) {
                tokens.add(s.substring(end));
            }
            return tokens;
        }

        static public boolean isVariable(String s) {
            return varRegex.matcher(s).matches();
        }

        static public boolean isFunction(String s) {
            return funcRegex.matcher(s).matches();
        }

        static public String[] getVariableDef(String s) {
            if (!s.startsWith("$")) {
                return null;
            }
            String var = s.substring(1);
            if (var.startsWith("{")) {
                var = var.substring(1, var.length() - 1);
            }

            if (var.contains(":")) {
                return var.split(":", 2);
            } else {
                return new String[] { null, var };
            }
        }

        static public String getFunctionExpr(String s) {
            if (!s.startsWith("$")) {
                return null;
            }
            final String s1 = s.substring(1);
            if (s1.startsWith("{")) {
                return s1.substring(1, s1.length() - 1);
            }
            return s1;
        }
    }

    final protected List<Term> terms;

    public Template(String s) {
        final List<String> tokens = Lexer.parse(s);
        terms = new ArrayList<>(tokens.size());
        for (String tok: tokens) {
            if (Lexer.isVariable(tok)) {
                final String[] vardef = Lexer.getVariableDef(tok);
                assert vardef != null;
                terms.add(new Variable(vardef[0], vardef[1]));
            } else if (Lexer.isFunction(tok)) {
                terms.add(new Function(Lexer.getFunctionExpr(tok)));
            } else {
                terms.add(new Literal(tok));
            }
        }
    }

    public String instantiate(Evaluator evaluator) {
        final StringBuilder sb = new StringBuilder();
        for (Term term: terms) {
            sb.append(term.instantiate(evaluator));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (Term term: this.terms) {
            sb.append(term.toString());
        }
        return sb.toString();
    }


    public static String substitute(String s, Evaluator evaluator, int maxIteration) {
        String iter = s;
        int iterCount = 0;
        while (true) {
            if (iterCount > maxIteration) {
                throw new IllegalStateException("substitute iteration exceeds "
                        + maxIteration + " times, template=" + s);
            }

            final Template template = new Template(iter);
            final String instantiated = template.instantiate(evaluator);
            if (iter.equals(instantiated)) {
                return instantiated;
            } else {
                iter = instantiated;
            }
            iterCount++;
        }
    }

    public static String substitute(String s, Evaluator evaluator) {
        return substitute(s, evaluator, 1);
    }

    public static String substitute(String s, final Map<String, String> context, int maxIteration) {
        final Evaluator evaluator = new Evaluator() {
            @Override
            public String eval(Term term) {
                if (term instanceof Variable) {
                    final String fullName = ((Variable) term).fullName();
                    if (! context.containsKey(fullName)) {
                        throw new IllegalArgumentException("variable definition not found in context: " + fullName);
                    }
                    return context.get(fullName);
                } else {
                    return term.toString();
                }
            }
        };

        return substitute(s, evaluator, maxIteration);
    }

    public static String substitute(String s, final Map<String, String> context) {
        return substitute(s, context, 1);
    }
}
