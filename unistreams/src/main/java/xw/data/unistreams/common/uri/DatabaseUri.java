package xw.data.unistreams.common.uri;

import java.net.URI;

/*
 * database URI syntax:
 * 
 *    <scheme>://<user>@<server>/<database>/<table>?password=...&<other_query>
 *    <scheme>://<server>/<database>/<table>?user=...&password=...&<other_query>
 */
public class DatabaseUri
{
	final public URI uri;
	final public String scheme;
	final public String server;
	final public String dbName;
	final public String tblName;
	final public String user;
	final public String password;
	final protected UriQuery query;

	public DatabaseUri(URI uri) {
		this.uri = uri;
		this.scheme = uri.getScheme();
			
		final String host = uri.getHost();
		final int port = uri.getPort();
		this.server = port > 0? (host + ":" + port): host;

		String path = UriUtils.urlDecode(uri.getPath());
		if (path == null || path.isEmpty()) {
			throw new IllegalArgumentException("bad database uri: missing path: " + uri);
		} else {
			if (path.charAt(0) == '/') {
				path = path.substring(1);
			}
			final String[] splits = path.split("[/\\.]");
			if (splits.length > 2) {
				throw new IllegalArgumentException("bad database uri: more than two segments: " + uri);
			}
			this.dbName = UriUtils.urlDecode(splits[0]);
			this.tblName = splits.length < 2? null: UriUtils.urlDecode(splits[1]);
		}
		this.query =  UriQuery.parse(uri);
		
		final String userInfo = uri.getUserInfo();
		if (userInfo == null) {
			this.user = UriUtils.urlDecode(query.getParameterAsString("_user"));
			this.password = UriUtils.urlDecode(query.getParameterAsString("_password"));
		} else {
			final String[] splits = userInfo.split(":", 2);
			if (splits.length == 1) {
				this.user = UriUtils.urlDecode(userInfo);
				this.password = UriUtils.urlDecode(query.getParameterAsString("_password"));
			} else {
				this.user = UriUtils.urlDecode(splits[0]);
				this.password = UriUtils.urlDecode(splits[1]);
			}
		}
		
		if (user != null && password == null) {
			throw new IllegalArgumentException("bad database uri: missing password: " + uri);
		}
		
		query.removeParameter("_user").removeParameter("_password");
	}
	
	public UriQuery getQuery()
	{
		return query;
	}
	
	public static DatabaseUri fromUri(URI uri) {
		return new DatabaseUri(uri);
	}
	
	@Override
	public String toString() {
		return uri.toString();
	}

}
