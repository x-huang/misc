package xw.data.unistreams.common.uri;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 5/27/16.
 */
public class UriUtils
{
    public static String urlDecode(String s) {
        if (s == null) {
            return null;
        }

        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("bad url-encoding: " + s);
        }
    }

    public static String urlEncode(String s) {
        if (s == null) {
            return null;
        }

        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("bad url-encoding: " + s);
        }
    }

    public static List<URI> cast(List<String> list) {
        final List<URI> uris = new ArrayList<>();
        for (String s: list) {
            uris.add(URI.create(s));
        }
        return uris;
    }

}
