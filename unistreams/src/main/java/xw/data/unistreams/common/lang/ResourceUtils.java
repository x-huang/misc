package xw.data.unistreams.common.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by xhuang on 10/7/2014.
 */
public class ResourceUtils
{
    private static final Logger logger = LoggerFactory.getLogger(ResourceUtils.class);

    private static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public static Properties loadProperties(String path) {
        final InputStream in = getClassLoader().getResourceAsStream(path);
        if (in == null) {
            logger.error("cannot load resource as stream: {}", path);
            throw new RuntimeException("cannot load properties as a stream");
        }
        final Properties properties = new Properties();
        try {
            properties.load(in);
            return properties;
        } catch (IOException e) {
            logger.error("cannot load properties as resource: {}", path);
            throw new RuntimeException("cannot load properties as resource", e);
        }
    }

    public static Properties loadPropertiesSafe(String path) {
        final InputStream in = getClassLoader().getResourceAsStream(path);
        if (in == null) {
            logger.error("cannot load resource as stream: {}", path);
            return null;
        }
        final Properties properties = new Properties();
        try {
            properties.load(in);
            return properties;
        } catch (IOException e) {
            logger.error("cannot load properties as resource: {}", path);
            return null;
        }
    }

    public static String loadAsText(String path) {
        final InputStream in = getClassLoader().getResourceAsStream(path);
        if (in == null) {
            logger.error("cannot load resource as stream: {}", path);
            throw new RuntimeException("cannot load text as a stream");
        }
        final Scanner scanner = new Scanner(in, "UTF-8");
        final String text = scanner.useDelimiter("\\A").next();
        scanner.close();
        return text;
    }
}
