package xw.data.unistreams.common.hive.serde;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.serde.serdeConstants;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe.SerDeParameters;
import xw.data.unistreams.setting.GlobalSetting;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by xhuang on 9/23/16.
 */
public class SerDeUtils
{
    private final static Map<String, String> serdeKeyRemap = new HashMap<>();

    static {
        serdeKeyRemap.put("fieldsep", serdeConstants.FIELD_DELIM);
        serdeKeyRemap.put("itemsep", serdeConstants.COLLECTION_DELIM);
        serdeKeyRemap.put("kvsep", serdeConstants.MAPKEY_DELIM);
        serdeKeyRemap.put("nullstr", serdeConstants.SERIALIZATION_NULL_FORMAT);
        serdeKeyRemap.put("escapechar", serdeConstants.ESCAPE_CHAR);
    }

    private static String remapSerDeParamKey(String key) {
        return serdeKeyRemap.containsKey(key)? serdeKeyRemap.get(key): key;
    }

    private static void mergeProperties(Properties dest, Properties src) {
        for (String key: src.stringPropertyNames()) {
            dest.put(remapSerDeParamKey(key), src.getProperty(key));
        }
    }

    private static void mergeProperties(Properties dest, Map<String, String> map) {
        for (String key: map.keySet()) {
            dest.put(remapSerDeParamKey(key), map.get(key));
        }
    }

    public static SerDeParameters getSerDeParameters(Configuration conf,
                                                     Map<String, String> context) throws SerDeException {

        final Properties tblProperties = new Properties();
        mergeProperties(tblProperties, System.getProperties());
        mergeProperties(tblProperties, GlobalSetting.getCopy());
        if (context != null) {
            mergeProperties(tblProperties, context);
        }

        return LazySimpleSerDe.initSerdeParams(conf, tblProperties, ColumnarSerDe.class.getName());
    }

    public static String describeSerDeParameters(SerDeParameters p) {
        final byte[] seps = p.getSeparators();
        return String.format("separators=[0x%02X,0x%02X,0x%02X],null-sequence=%s,escaped=%s,escape-char=%c",
                seps[0], seps[1], seps[2], p.getNullSequence(), p.isEscaped(), p.getEscapeChar());
    }
}
