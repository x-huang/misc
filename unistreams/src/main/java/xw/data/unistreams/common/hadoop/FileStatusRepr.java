package xw.data.unistreams.common.hadoop;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class FileStatusRepr
{
	public final long accessTime;
	public final long blockSize;
	public final String group;
	public final long length;
	public final long modificationTime;
	public final String owner;
	public final String path;
	public final String permission;
	public final short replication;
	public final boolean isDir;
	
	public FileStatusRepr(FileSystem fs, Path p) throws IOException {
		FileStatus status = fs.getFileStatus(p);
		accessTime = status.getAccessTime();
		blockSize = status.getBlockSize();
		group = status.getGroup();
		length = status.getLen();
		modificationTime = status.getModificationTime();
		owner = status.getOwner();
		path = status.getPath().toString();
		permission = status.getPermission().toString();
		replication = status.getReplication();
		isDir = status.isDirectory();
	}
}
