package xw.data.unistreams.common.uri;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UriQuery
{
	final protected Map<String, String> query;

	protected UriQuery() {
		query = new HashMap<>();
	}
	
	public UriQuery(String s) {
		query = parseToMap(s);
	}

	public static UriQuery parse(String queryString) {
		return new UriQuery(queryString);
	}

    public static UriQuery parse(URI uri) {
		return parse(uri.getQuery());
    }

	public static Map<String, String[]> parseToMultiMap(String s) {
		if (s == null) {
			return new HashMap<>(0);
		}

		// In map1 we use strings and ArrayLists to collect the parameter targetValues.
		final HashMap<String, Object> map1 = new HashMap<>();
		int p = 0;
		while (p < s.length()) {
			int p0 = p;
			while (p < s.length() && s.charAt(p) != '=' && s.charAt(p) != '&') {
				p++;
			}
			final String name = UriUtils.urlDecode(s.substring(p0, p));
			if (p < s.length() && s.charAt(p) == '=') {
				p++;
			}
			p0 = p;
			while (p < s.length() && s.charAt(p) != '&') {
				p++;
			}
			final String value = UriUtils.urlDecode(s.substring(p0, p));
			if (p < s.length() && s.charAt(p) == '&') {
				p++;
			}
			final Object x = map1.get(name);
			if (x == null) {
				// The first targetValue of each name is added directly as a string to the map.
				map1.put(name, value);
			} else if (x instanceof String) {
				// For multiple targetValues, we use an ArrayList.
				final ArrayList<String> a = new ArrayList<>();
				a.add((String) x);
				a.add(value);
				map1.put(name, a);
			} else {
				@SuppressWarnings("unchecked")
				final ArrayList<String> a = (ArrayList<String>) x;
				a.add(value);
			}
		}
		// Copy map1 to map2. Map2 uses string arrays to store the parameter targetValues.
		HashMap<String, String[]> map2 = new HashMap<>(map1.size());
		for (Map.Entry<String, Object> e : map1.entrySet()) {
			final String name = e.getKey();
			final Object x = e.getValue();
			final String[] v;
			if (x instanceof String) {
				v = new String[] { (String) x };
			} else {
				@SuppressWarnings("unchecked")
				final ArrayList<String> a = (ArrayList<String>) x;
				v = new String[a.size()];
				a.toArray(v);
			}
			map2.put(name, v);
		}
		return map2;
	}
	
	public static Map<String, String> flatten(Map<String, String[]> multi) {
		final HashMap<String, String> flattened = new HashMap<>();
		for (Map.Entry<String, String[]>entry : multi.entrySet()) {
			final String[] values = entry.getValue();
			if (values.length != 1) {
				final String msg = "query parameter '" + entry.getKey() + "' has more than one values";
				throw new IllegalArgumentException(msg);
			}
			flattened.put(entry.getKey(), values[0]);
		}
		return flattened;
	}

	public static Map<String, String> parseToMap(String s) {
		return flatten(parseToMultiMap(s));
	}

    public int size() {
        return query.size();
    }

    public boolean hasParameter(String param) {
        return query.containsKey(param);
    }

	public String getParameterAsString(String param) {
		return query.get(param);
	}
	
	public Integer getParameterAsInteger(String param) {
		final String value = query.get(param);
		return value == null? null: Integer.parseInt(value);
	}
	
	public Long getParameterAsLong(String param) {
		final String value = query.get(param);
		return value == null? null: Long.parseLong(value);
	}
	
	private static final String[] yesValues = { 
		"1", "yes", "true", "ok" 
	};
	
	public Boolean getParameterAsYes(String param) {
		final String value = query.get(param);
		if (value == null) {
			return false;
		} else {
			for (String yes : yesValues) {
				if (value.equalsIgnoreCase(yes))
					return true;
			}
			return false;
		}
	}

	public Map<String, String> getAllParameters() {
		return query;
	}
	
	public List<String> getPrefixedParameterNames(String prefix) {
		final List<String> prefixed = new ArrayList<>();
		for (String key: query.keySet()) {
			if (key.startsWith(prefix)) {
				prefixed.add(key);
			}
		}
		return prefixed;
	}
	
	public UriQuery addParameter(String param, String value) {
		query.put(param, value);
		return this;
	}
	
	public UriQuery addParameters(Map<String, String> params) {
		query.putAll(params);
		return this;
	}
	
	public UriQuery addParameterSkippingNull(String param, String value) {
		if (value != null)
			query.put(param, value);
		return this;
	}
	
	public UriQuery removeParameter(String param) {
		if (query.containsKey(param))
			query.remove(param);
		return this;
	}
	
	public UriQuery removeParameters(List<String> params) {
		for (String p: params) {
			if (query.containsKey(p))
				query.remove(p);
		}
		return this;
	}
		
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		String delimeter = "";
		for (Map.Entry<String, String> entry: query.entrySet()) {
			sb.append(delimeter).append(entry.getKey()).append("=").append(entry.getValue());
			delimeter = "&";
		}
		return sb.toString();
	}
	
	public String toQueryString() {
		final String qs = this.toString();
		return qs.length() > 0? "?" + qs: qs; 
	}
}
