package xw.data.unistreams.common.lang;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StringUtils
{
	static public String join(String[] array, String delimiter) {
	     final StringBuilder sb = new StringBuilder();
	     String delim = "";
	     for (String s: array) {
			 sb.append(delim).append(s);
			 delim = delimiter;
	     }
	     return sb.toString();
	}
	
	public static String join(List<String> list, String delimiter) {
		final StringBuilder sb = new StringBuilder();
		String delim = "";
		for (String s: list) {
			sb.append(delim).append(s);
			delim = delimiter;
		}
		return sb.toString();
	}
	
	static public String join(String[] array, String delimiter, 
							  String leftDecorator, String rightDecorator) {
	     StringBuilder builder = new StringBuilder();
	     boolean first = true;
	     for (String s: array) {
	    	 if (first) 
	    		 first = false;
	    	 else
	    		 builder.append(delimiter);
	    	 builder.append(leftDecorator).append(s).append(rightDecorator);
	     }
	     return builder.toString();
	}
	
	static public String join(String s, String delimiter, int times) {
	     StringBuilder builder = new StringBuilder();
	     boolean first = true;
	     for (int i=0; i<times; i++) {
	    	 if (first) 
	    		 first = false;
	    	 else
	    		 builder.append(delimiter);
	    	 builder.append(s);
	     }
	     return builder.toString();
	}
	
	static public String[] zip(Map<String, String> map, String connector) 
	{
		String[] zipped = new String[map.size()];
		Iterator<Entry<String, String>> iter = map.entrySet().iterator();
		int idx = 0;
		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			zipped[idx++] = entry.getKey() + connector + entry.getValue();
		}
		return zipped;
	}

	static public String[] zip(List<String> l1, List<String> l2, String connector)
	{
		if (l1.size() != l2.size()) {
			throw new IllegalArgumentException("attempt to zip size unequal lists: " 
												+ l1.size() + " and " + l2.size());
		}
		String[] zipped = new String[l1.size()];
		for (int i=0; i<l1.size(); i++) {
			zipped[i] = l1.get(i) + connector + l2.get(i);
		}
		return zipped;
	}
	
	static public String[] zip(String[] l1, String[] l2, String connector)
	{
		if (l1.length != l2.length) {
			throw new IllegalArgumentException("attempt to zip arrays length mismatched: "
												+ l1.length + " and " + l2.length);
		}
		String[] zipped = new String[l1.length];
		for (int i=0; i<l1.length; i++) {
			zipped[i] = l1[i] + connector + l2[i];
		}
		return zipped;
	}
	
}
