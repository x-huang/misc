package xw.data.unistreams.common.lang;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by yun on 8/24/14.
 */
public class Registry<T>
{
    final private Map<String, T> registry = new HashMap<>();

    public Registry<T> register(T item, String... names) {
        for (String name: names) {
            if (registry.containsKey(name)) {
                throw new IllegalArgumentException("registry entry already exists: " + name);
            }
        }
        if (item == null) {
            throw new IllegalArgumentException("registry cannot accept null item");
        }
        for (String name: names) {
            registry.put(name, item);
        }
        return this;
    }

    public Set<String> getKeys() {
        return registry.keySet();
    }

    public void dump(PrintStream ps) {
        for (Map.Entry<String, T> entry: registry.entrySet()) {
            ps.println(entry.getKey() + "\t" + entry.getValue().toString());
        }
    }

    public T get(String name) {
        if (! registry.containsKey(name)) {
            throw new IllegalArgumentException("no such item in registry: " + name);
        }
        return registry.get(name);
    }
}
