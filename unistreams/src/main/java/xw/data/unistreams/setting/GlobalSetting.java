package xw.data.unistreams.setting;

import java.util.Properties;

/**
 * Created by xhuang on 9/23/16.
 */
public class GlobalSetting
{
    private static final Properties singleton = new Properties();

    public static void set(String key, Object value) {
        singleton.put(key, value);
    }

    public static void unset(String key) {
        singleton.remove(key);
    }

    public static void clear() {
        singleton.clear();
    }

    public static boolean containsKey(String key) {
        return singleton.containsKey(key);
    }

    public static Object get(String key) {
        return singleton.get(key);
    }

    public static Properties getCopy() {
        final Properties copy = new Properties();
        copy.putAll(singleton);
        return copy;
    }
}
