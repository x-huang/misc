# Unistreams

### Intruduction

`Unistreams` is an API abstraction layer that provides format/media agnostic access to various data 
resources that are commonly found in a Hadoop/Hive cluster environment. It uses a plain string format 
to represent data resources and does the following three actions:

- opens an input data record stream for read;
- opens an output data record stream for write;
- returns a descriptive POJO containing associated resource attributes, such as data schema.

The core concept of resource representation is the notation of `Resource Descriptor`, which is analogous 
to an enriched form of *URI* but with extended syntax. In short an `RD` is all information needed 
to access a data resource conbimed and flattened into a *URI* -like string. One could use `RD`s to
represent Hive tables, sql queries, raw ORC or RC files, or screen dump, etc. 

Usage of `Unistreams` is idiomatic. A user needs not to know underlying serialize/deserialize details; 
all of these are supposed to be transparent to users. Here is the shortest Java code example of reading 
from a data stream:

```java
final DataSource ds = ResourceDescriptor.of("some_rd").source();
ds.initialize();	// may throw IOException
Object[] row = null;
while ((row = ds.read()) != null) {
    // consume row
}
ds.close();
```

Note that `Unistreams` works at data record level. For purposes of manipulating binary data, 
such as copying files, one shall seek to file system APIs. 

Also `Unistreams` itself doesn't include a metadata storage thus only can provide data schema 
for data resources that carries metadata by themselves, such as hive table, jdbc database, ORC 
file, ... Some types of resources, such as RCFile, text file, kryo dataset, just don't contain 
metadata. Therefore the users will be responsible to presume the associated schema in that case. 

Currently the following data format/media are supported: 

| Data Media | Read | Write | Describe | RD Example |
|:------------:|------|-------|----------|---------|
|     `hive` tables    |  Y    |  N     |   Y       |  `hive:(db/tbl)`, `hive:db.tbl`
|  `jdbc` (`mysql`, `mssql`, `teradata`) tables  |  Y    |  Y     |   Y   | `jdbc:mysql://user:passwd@host/db.tbl?_select=col1,col2&col3=val`
|   raw `orc` file |  Y     |   Y    |   Y       | `orc:(hdfs:///path/to/file)?schema=<id:bigint,name:string>`
|   raw `rcfile` (bin-serde or not) |  Y     |   N    |   Y       | `rcfile:(s3n:///path/to/file)`
|   `rccat` (all columns remain in string form) |  Y     |   N    |   Y       | `rccat:(s3n://path/to/file)`
| `kryo` (registered with `Object[]`) | Y | Y | Y | `kryo:(file:///path/to/file)`
| `text` file <br/>(tsv, csv, psv, semi-json) | N | Y | Y | `text:(stdout)?fieldsep=\\t` <br/> `~tsv.stdout`, `~csv.stdout`, `~semi.json.stdout`
| `dummy empty` source | Y | N | N | `dummy:empty`
| `dummy null` sink | N | Y | N | `dummy:null`

This list is expanding in further releases.


### Resource Descriptor

A `resource descriptor` is a series text items organized in such a format: 

```
env:: prefix: (uri;...) ? param=value&...
```

It consists of the following four parts:

- `env` - A short word denoting to which cluster environment does this resource belongs. 
If `env` is omitted, `Unistreams` will assume it is in the `default` environment. `Unistreams` 
relies on the `env` name to find cluster configuration. Please refer to the 
**configuration management** section for more detail.
- `prefix` - Denote the format or media of data set. 
- `uris` - A list of URIs separated by semi-colons, indicating the data location. Each URI is an authentic 
Java URI that may have its own scheme, userinfo, hostname, port, path, and query. We allow multiple URIs 
only for *open-for-read* action, in which case it behaves as concatenation of several data streams opened
by single URI form. In single URI case, the embracing parenthesis pair can be omitted, and the trailing 
parameter list, if exists, will be assign to URI query instead of RD parameters. So if URI query and RD params 
need to co-exist, use parenthesis. 
- `params` - a list of `key=value` pairs delimited by `&` sign, just as URI's query part. These parameters are 
passed to resource operator as additional context information, for example, to write into an ORC file, we 
need a `schema=...` parameter. 

#### RD Alias

RD aliasing is a convenient way to help memorize RDs. `Unistreams` provides the following resource alias: 
`~tsv.stdout`, `~csv.stdout`, `~psv.stdout`, `~semi.json.stdout`, `~dummy.empty`, `~dummy.null`. These alias
will be interpreted as real RD upon seen, for example: 

```
~semi.json.stdout = text:(stdout)?fieldsep=,&itemsep=,&kvsep=:&wrapstring=true&wraparray=true&wrapmap=true&rowopening=[&rowclosing=]
```

User can define custom alias by setting the `resource.alias` Java property. For example:

```
java -Dresource.alias.my.alias=some_rd MyProg
```
That way one can use resource alias `~my.alias` in program.


#### RD Variable Substitution

To avoid writing lengthy RD strings it is possible to embed `${[prefix:]var}` in a RD string. Upon parsing, 
`Unistreams` will resolve these variable to produce a materialized concrete RD. 

A RD variable is merely a simple string with optional prefix `sys` or `env`, delimited by a colon. Different 
prefix indicates different source of variable value:

- `${var}` - `Unistreams` is going to look up parameter `var` in RD's parameter list or the `GlobalSetting`. 
`GlobalSetting` is facility singleton object serves as common context of all RDs for variable resolving purpose.
- `${env:var}` - `var` will be looked up in system environment. 
- `${sys:var}` - `var` will be looked up in system properties.


#### About Data Schema

`Unistreams` dumps and can recognize a string representation of schema exactly as Hive's TypeInfo string. It can have recursive compound data types, as long as data media supports it, for example: 

```
struct<id:int,name:string,m1:map<string,date>,a1:array<timestamp>,s1:struct<b:boolean,s:string>>
```

For some actions, such as reading from an RCFile or writing into an ORC file, it requires schema information. 

Recall that write action on some resources will need data schema to proceed, usually it is the user's responsibiliy to assign a schema in that case. However, when in an attempt of *copy* data, one can utilize the schema obtained from the source RD:

```java
final DataSource in = ResourceDescriptor.of("src_rd").source();
final DataSink out = ResourceDescriptor.of("dest_rd").sink();
in.initialize();	// populate *in*'s context with schema=...
out.context().copy(in.context());  // copy *in*'s context to *out*'s context including 'schema'
out.initialize();  // out may require 'schema' 
    
Object[] row = null;
while ((row = in.read()) != null) {
    out.write(row)
}
    
in.close();
out.close();
```

The underlining rationale is that every input/output data stream is associated with a small map-like `context` object which is initialized by `RD` parameters. When `Unistreams` opens an input stream that has schema information, it will fill the `context` with key *'schema'* and value the string format of its schema. 


### Resource-Specific Parameters

<!--In this section we assume all RDs points to the default environment, so that we will omit the `env::` part. -->

Parameters are *key=value* pairs appears in `RD`. A parameter can belong to a URI or belong to entire `RD`, depending on its appearance.

| `RD` Prefix | URI Scheme | URI Query Parameters | `RD` Parameters
|:---------------------:|------------|-----------|-------------------|
|  `hive` |   *\<none\>*  | `_limit=num` <br/> `_allowpartialspec=[true/false]` - default is false <br/> `part_col=val` - partition column spec   | |
| `jdbc`  | `mysql`,<br/>`mssql`,<br/>`teradata` | `_limit=num` <br/> `_select=cols,...` - select specific columns <br/> `_sql=...` - execute arbitrary SQL statement <br/> `_onduplicate=[insert/ignore/replace]` - only for `mysql` <br/> `col=val` - expression as in WHERE clause|  |
| `orc` | `file`,`hdfs`,`s3n` | | `schema=...` - for write operation |
| `rc`, `rcfile`,<br/> `rc1`, `rc2` |`file`,`hdfs`,`s3n` | | `schema=...` - RCFile schema <br/> `version=[1/2]` - use text or binary serde <br/>(prefix `rc1/rc2` implies version too)|
| `rcfilecat`, <br/>`rccat` |`file`,`hdfs`,`s3n` | | `ncols=num` - number of columns |
| `kryo` | `file`,`hdfs`,`s3n` | |  |
| `text`, `txt` | `file`,`hdfs`,`s3n` | | `fieldsep=...` - record field separator <br/> `itemsep=...` - map/array item separator <br/> `kvsep=...` - map key/value separator |


### CLI Tools

- `copy.sh <src-rd> <dest-rd>` - Copy data from one resource stream to another. If `<dest-rd>` is omitted, it is default to displaying tab-delimitered text on standard output, meaning dumping data to console.
- `dump.sh <rd>` - A soft symbolic link to `copy.sh`.
- `describe.sh <rd>` - Print resource schema, if avaible. 
- `print-config.sh <env> [hadoop/hive]` - Print `env`'s *hadoop* or *hive* configuration as *key = value* pairs. It is a good way to test correctness of cluster configuration settings.
- `test-rd.sh <rd>` - Check syntax correctness of `<rd>`.
- `test-schema.sh <schema-string>` - Check syntax correctness of schema string.

Note that an RD often contains characters that have special meaning in shell, thus it is safer to surround it is a pair of single-quote.

#### Example usage

Describe an ORC file: 

```
$ bin/describe.sh -Dmy_loc=hdfs:///hive/warehouse/my_data_location 'prod::orc:(${sys:my_loc}/000472_0)'
...
19:21:24.806 [main] DEBUG Describe - parsed resource descriptor: prod::orc:hdfs:///hive/warehouse/my_data_location/000472_0
...
19:21:27.464 [main] INFO  Describe - metadata POJO: {
       	rows=203177,
       	contentLength=4420795,
       	rowIndexStride=10000,
       	compression='ZLIB',
       	compressionSize=262144,
       	inspectorCategory='STRUCT',
       	typeName='struct<_col0:string,_col1:string,_col2:string,_col3:bigint,_col4:string,_col5:string,_col6:string,_col7:string,_col8:string,_col9:string,_col10:string,_col11:string,_col12:timestamp,_col13:string,_col14:string,_col15:string,_col16:bigint>',
       	metadataKeys=[],
       	columnStatistics=[{className='ColumnStatisticsImpl', statistics={count=203177}}, {className='StringStatisticsImpl', statistics={min=0, max=999962327, count=203177, sum=2130090}}, {className='StringStatisticsImpl', statistics={min=nucleus, max=synergy, count=203177, sum=1422239}}, {className='StringStatisticsImpl', statistics={min= , max=UFC, count=203177, sum=1178937}}, {className='IntegerStatisticsImpl', statistics={min=55374, is-sum-defined=true, max=720005, count=203177, sum=95929248575}}, {className='StringStatisticsImpl', statistics={min=edw_master_ttl_id, max=product_id, count=203177, sum=3178041}}, {className='StringStatisticsImpl', statistics={min= , max= , count=203177, sum=203177}}, {className='StringStatisticsImpl', statistics={min=305867, max=madden-2017-xone-trial, count=203177, sum=1462886}}, {className='StringStatisticsImpl', statistics={min=easku, max=sku, count=203177, sum=1304465}}, {className='StringStatisticsImpl', statistics={min=alpha, max=trial_origin_access, count=203177, sum=824725}}, {className='StringStatisticsImpl', statistics={min=client, max=client_sparta, count=203177, sum=2116392}}, {className='StringStatisticsImpl', statistics={min=android, max=xbox_one, count=203177, sum=984577}}, {className='StringStatisticsImpl', statistics={min=-1001272027588245877, max=fffd50cc-9bde-11e6-e211-09f78e6dbeef, count=203177, sum=6067740}}, {className='TimestampStatisticsImpl', statistics={count=203177}}, {className='StringStatisticsImpl', statistics={min=mp_online, max=online_mp, count=203177, sum=1566888}}, {className='StringStatisticsImpl', statistics={min=BAGK, max=tournament, count=203177, sum=1776302}}, {className='StringStatisticsImpl', statistics={min=0, max=xp7_valley, count=100020, sum=471610}}, {className='IntegerStatisticsImpl', statistics={min=0, is-sum-defined=true, max=140, count=146613, sum=3808867}}],
       	metadata={},
       	stripes[{len=4420792, offset=3, rows=203177, data_len=4414250, index_len=6157, footer_len=385}]
}
_col0:string,_col1:string,_col2:string,_col3:bigint,_col4:string,_col5:string,_col6:string,_col7:string,_col8:string,_col9:string,_col10:string,_col11:string,_col12:timestamp,_col13:string,_col14:string,_col15:string,_col16:bigint
```

Test a schema string:

```
$ bin/test-schema.sh 'id:int,name:string,m1:map<string,date>,a1:array<timestamp>,s1:struct<b:boolean,s:string>'
parsed: id:int,name:string,m1:map<string,date>,a1:array<timestamp>,s1:struct<b:boolean,s:string>
```

Dump multiple ORC files:

```
$ bin/copy.sh -Dmy_loc=hdfs:///hive/warehouse/my_data_location  \
	'prod::orc:(${sys:my_loc}/000472_0;${sys:my_loc}/000473_0;${sys:my_loc}/000474_0)' \
 	| wc -l
...
19:09:19.066 [main] DEBUG Copy - parsed src resource descriptor: prod::orc:(hdfs:///hive/warehouse/my_data_location/000472_0;hdfs:///hive/warehouse/my_data_location/000473_0;my_data_location/000474_0)
19:09:19.069 [main] DEBUG Copy - parsed dest resource descriptor: text:(stdout)?fieldsep=%5Ct
...
19:09:19.092 [main] DEBUG MultipleDataSource - initializing multiple data source: prod::orc:(hdfs:///hive/warehouse/my_data_location/000472_0;hdfs:///hive/warehouse/my_data_location/000473_0;hdfs:///hive/warehouse/my_data_location/000474_0)
...
19:09:22.154 [main] DEBUG OrcFileSource - initialized reading from ORC file: hdfs:///hive/warehouse/my_data_location/000472_0 (203177 rows)
19:09:22.154 [main] DEBUG MultipleDataSource - current data source: {prod::orc:hdfs:///hive/warehouse/my_data_location/000472_0}
19:09:37.187 [main] DEBUG OrcFileSource - initialized reading from ORC file: hdfs:///hive/warehousemy_data_location/000473_0 (207574 rows)
19:09:37.188 [main] DEBUG MultipleDataSource - current data source: {prod::orc:hdfs:///hive/warehouse/my_data_location/000473_0}
19:09:50.945 [main] DEBUG OrcFileSource - initialized reading from ORC file: hdfs:///hive/warehouse/my_data_location/000474_0 (204870 rows)
19:09:50.945 [main] DEBUG MultipleDataSource - current data source: {prod::orc:hdfs:///hive/warehouse/my_data_location/000474_0}
19:10:04.336 [main] DEBUG Copy - read/written 615621 records
615621
```
 
### Configuration Management

Configuring cluster information becomes a big issue in that `Unistreams` is implemented to 
target multiple cluster environments in same JVM. To avoid depending on additional configuration 
service, we ask user to collect configuration files manually and put it in specific locations 
in Java classpath so that `Unistreams` can find configuration for each cluster environment.

First of all we will give a short name for each cluster, for example *'prod'*, *'ci'*, and *'dev'*.

To configure Hadoop, put file *'core-site.xml'*, *'hdfs-site.xml'*, *'yarn-site.xml'* and *'mapred-site.xml'* 
in folder *'configurations/$env/hadoop/'*. To configure Hive, put file *'hive-site.xml'* in folder 
*'configurations/$env/hive/'*. So a complete configuration folder will look list this: 

```
 configurations
 ├── env-alias.properties
 ├── ci
 │   ├── hadoop
 │   │   ├── core-site.xml
 │   │   ├── hdfs-site.xml
 │   │   ├── mapred-site.xml
 │   │   └── yarn-site.xml
 │   └── hive
 │       └── hive-site.xml
 ├── dev
 │   ├── hadoop
 │   │   ├── core-site.xml
 │   │   ├── hdfs-site.xml
 │   │   ├── mapred-site.xml
 │   │   └── yarn-site.xml
 │   └── hive
 │       └── hive-site.xml
 └── prod
     ├── hadoop
     │   ├── core-site.xml
     │   ├── hdfsfs-site.xml
     │   ├── mapred-site.xml
     │   └── yarn-site.xml
     └── hive
         └── hive-site.xml
```


#### Env Alias

In the configuration folder there is a special properties file *'env-alias.properties'* where users 
define environment aliases. The most important entry in this properties file would be `default`. 
Recall that the `env` part of an RD is optional and will be defaulted to `default`, this `default` 
alias tells exactly which cluster to look at.
 
User can overwrite *'configurations/env-alias.properties'* by setting the `env.alias` Java property. 
For example `-Denv.alias.default=dev` overwrites `default=prod` *'env-alias.properties'*. This way
one can quickly switch cluster environment without modifying existing RDs.

#### Deploy Configurations
  
Deploying cluster configurations is meant to be a prerequiste and separate procedure of using `Unistreams`. One can take either actions: 

- Simply create the *'configurations/'* folder and add it to classpath;
- `jar` the *'configurations/'* folder and put it in Java classpath, it is the recommended way for releasing your own product. 

### Dev Notes

Written in Java at 1.7 compilation level. 

Logging through slf4j.

Dependent libraries:

- hadoop 2.7.1
- hive 1.0.1
- kryo 3.0.x (optional) 
- mysql/mssql/teradata jdbc driver (optional)

<!-- this file is compiled by MacDown using github 2 style -->