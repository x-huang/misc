package xw.data.webapp;

import java.util.Arrays;

class Config {

    boolean help = false;
    int port = 4567;
    int maxThreads = 8;
    String publicAssets = "./public_assets/";

    static Config fromArgs(String[] args) {
        final Config config = new Config();
        Arrays.stream(args).forEach(arg -> {
            if (arg.endsWith("help")) {
                config.help = true;
            } else if (arg.startsWith("--port=")) {
                config.port = Integer.parseInt(arg.substring("--port=".length()));
            } else if (arg.startsWith("--max-threads=")) {
                config.maxThreads = Integer.parseInt(arg.substring("--max-threads=".length()));
            } else if (arg.startsWith("--public-assets=")) {
                config.publicAssets = arg.substring("--public-assets=".length());
            } else {
                throw new IllegalArgumentException("unknown argument: " + arg);
            }
        });
        return config;
    }

    public String usage() {
        return "java Main [options...] \n" +
                "  help - print usage\n" +
                "  --port=<num> - use port number, default is 4567\n" +
                "  --max-threads=<num> - servlet thread pool size, default is 8\n" +
                "  --public-assets=<path> - serve public assets in a local directory, " +
                "default is ./public_assets/\n";
    }

    @Override
    public String toString() {
        return "port=" + port + ", maxThreads=" + maxThreads
                + ", publicAssets='" + publicAssets + '\'' ;
    }
}
