package xw.data.webapp;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.common.util.HadoopUtils;
import xw.data.eaql.EAQL;
import xw.data.eaql.env.unistreams.UnistreamsEnv;
import xw.data.eaql.model.stmt.SQLStatement;
import xw.data.eaql.query.QueryBuilder;
import xw.data.eaql.query.SQLQuery;
import xw.data.eaql.query.SQLRead;
import xw.data.eaql.query.SQLWrite;
import xw.data.eaql.schema.Schema;
import xw.data.streams.Streams;
import xw.data.unistreams.common.hadoop.FileStatusRepr;
import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.env.Environment;
import xw.data.unistreams.resources.ResourceDescriptor;
import xw.data.unistreams.stream.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

class Actions {

    private final static Logger logger = LoggerFactory.getLogger(Actions.class);

    private final static Set<String> binaryPrefixes = new HashSet<>();
    static {
        binaryPrefixes.add("");
        binaryPrefixes.add("raw");
        binaryPrefixes.add("binary");
    }

    private static boolean prefixMeansBinaryResource(String p) {
        return p == null || binaryPrefixes.contains(p.toLowerCase());
    }

    static InputStream openAsBinaryStream(ResourceDescriptor rd)
            throws IOException {

        logger.debug("opening resource: {}", rd);

        if (prefixMeansBinaryResource(rd.prefix)) {
            rd.assertUriSizeEq(1);
            final URI uri = rd.getSingleUri();
            final FileSystem fs = Environment.of(rd.env).getFileSystem(uri);
            return HadoopCommon.openInputStream(fs, uri);
        } else {
            final String format = rd.params.getOrDefault("_format", "tsv");
            xw.data.unistreams.stream.DataSource ds = rd.source();
            ds.initialize();
            return Streams.wrapDataSource(ds, format);
        }
    }

    static InputStream openAsBinaryStream(String rd) throws IOException {
        return openAsBinaryStream(ResourceDescriptor.parse(rd));
    }

    static void writeZippedStream(ResourceDescriptor rd, OutputStream out) throws IOException {

        logger.debug("compressing directory: {}", rd);
        final URI uri = rd.getSingleUri();
        final Configuration conf = Environment.of(rd.env).getHadoopConf();
        HadoopUtils.zipFiles(out, conf, uri.toString());
    }


    static void writeZippedStream(String rd, OutputStream out) throws IOException {
        writeZippedStream(ResourceDescriptor.parse(rd), out);
    }

    static Object describe(ResourceDescriptor rd) throws IOException {

        logger.debug("describing resource: {}", rd);

        if (prefixMeansBinaryResource(rd.prefix)) {
            rd.assertUriSizeEq(1);
            final URI uri = rd.getSingleUri();
            final FileSystem fs = Environment.of(rd.env).getFileSystem(uri);
            final FileStatusRepr stat = HadoopCommon.getFileStatusRepr(fs, uri);
            if (stat.isDir) {
                return HadoopCommon.getDirectoryRepr(fs, uri);
            } else {
                return stat;
            }
        } else {
            return rd.op().metadata();
        }
    }

    static Object describe(String rd) throws IOException {
        return describe(ResourceDescriptor.parse(rd));
    }

    static class QueryResult {
        final public Schema schema;
        final public InputStream in;

        QueryResult(Schema schema, InputStream in) {
            this.schema = schema;
            this.in = in;
        }
    }

    static QueryResult query(String s, String format) throws IOException {
        final SQLStatement stmt = EAQL.parseStatement(s);
        logger.debug("execute query: {}", stmt);

        final SQLQuery query = QueryBuilder.build(UnistreamsEnv.get(), stmt);
        if (query instanceof SQLWrite) {
            throw new RuntimeException("INSERT-statement not supported: " + s);
        }
        final SQLRead sqlRead = (SQLRead) query;
        final DataSource ds = sqlRead.getDataSource();
        ds.initialize();
        return new QueryResult(sqlRead.getSchema(), Streams.wrapDataSource(ds, format));

    }

}
