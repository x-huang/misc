package xw.data.webapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;

import static spark.Spark.*;

public class Main {

    private final static Logger logger = LoggerFactory.getLogger(Main.class);
    private final static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) {

        final Config config = Config.fromArgs(args);
        if (config.help) {
            System.out.println(config.usage());
            return;
        }
        logger.info("webapp config: {}", config);

        port(config.port);
        threadPool(config.maxThreads);
        staticFiles.location("/public");
        staticFiles.externalLocation(config.publicAssets);

        options("/*", (req, resp) -> {

            String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                resp.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                resp.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((req, resp) -> {
            logger.debug("incoming request: {} {} - {}", req.requestMethod(), req.url(), req.body());

            resp.header("Access-Control-Allow-Origin", "*");
            resp.header("Access-Control-Request-Method", "GET, POST");
            resp.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");

            // Note: this may or may not be necessary in your particular application
        });

        notFound("<html><body><h1>Path not found, see /help. </h1></body></html>");

        exception(Exception.class, (e, req, resp) -> {
            logger.error("internal error: " + e.getMessage(), e);
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            resp.status(500);
            resp.body(sw.toString()); // stack trace as a string
        });

        get("/help", (req, resp) -> {
            resp.redirect("/help.html");
            return null;
        });

        get("/resource", (req, resp) -> {
            resp.header("Content-Encoding", "gzip");
            return Actions.openAsBinaryStream(req.queryParams("rd"));
        });
        post("/resource", (req, resp) -> {
            resp.header("Content-Encoding", "gzip");
            return Actions.openAsBinaryStream(req.body());
        });

        get("/zip", (req, resp) -> {
            resp.header("Content-Encoding", "zip");
            Actions.writeZippedStream(req.queryParams("rd"), resp.raw().getOutputStream());
            return null;
        });

        post("/zip", (req, resp) -> {
            resp.header("Content-Encoding", "zip");
            Actions.writeZippedStream(req.body(), resp.raw().getOutputStream());
            return null;
        });

        get("/describe", (req, resp) -> {
            resp.header("Content-Type", "application/json");
            return Actions.describe(req.queryParams("rd"));
        }, gson::toJson);

        post("/describe", (req, resp) -> {
            resp.header("Content-Type", "application/json");
            return Actions.describe(req.body());
        }, gson::toJson);

        post("/query", (req, resp) -> {
            final String format = req.headers("X-Format");
            final Actions.QueryResult result = Actions.query(req.body(), format!=null?format:"tsv");
            resp.header("X-Schema", result.schema.toString());
            resp.header("Content-Encoding", "gzip");
            return result.in;
        });

        put("/admin/stop", (req, resp) -> {
            logger.info("stopping server");
            stop();
            return "OK";
        });
    }
}


/*

hadoop@node74-111:/mnt/tmp$ curl -s 'http://localhost:4567/zip?rd=hdfs:///user/hadoop/apps/xplusone_ingestion' | jar xvf /dev/stdin
  created: V1/
 inflated: V1/OozieJobs.properties
  created: V1/conf/
 inflated: V1/conf/log4j2.xml
 inflated: V1/conf/xplusone-ingestion.conf
 inflated: V1/coordinator.properties
 inflated: V1/coordinator.xml
 inflated: V1/env.sh
 inflated: V1/hive-site.xml
 inflated: V1/hive-site.xml.hive10
 inflated: V1/hive-site.xml_bak
  created: V1/lib/
 inflated: V1/lib/ingestion-xplusone-1.0.jar
 inflated: V1/lib/jsch-0.1.44-1.jar
 inflated: V1/lib/jtar-2.2.jar
 inflated: V1/lib/log4j-api-2.0.2.jar
 inflated: V1/lib/log4j-core-2.0.2.jar
 inflated: V1/lib/log4j-slf4j-impl-2.0.2.jar
 inflated: V1/lib/slf4j-api-1.7.4.jar
 inflated: V1/workflow.xml

*/