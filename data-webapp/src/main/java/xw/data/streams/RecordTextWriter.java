package xw.data.streams;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class RecordTextWriter implements RecordWriter {

    private static final Charset charset = Charset.forName("UTF-8");
    private byte fieldDelim = '\t';
    private byte lineDelim = '\n';
    private byte[] nullRepr = "\\N".getBytes(charset);

    public RecordTextWriter setFieldDelimiter(char c) {
        this.fieldDelim = (byte) c;
        return this;
    }

    public RecordTextWriter setLineDelimiter(char c) {
        this.lineDelim = (byte) c;
        return this;
    }

    public RecordTextWriter setNullRepr(String repr) {
        this.nullRepr = repr.getBytes(charset);
        return this;
    }

    @Override
    public void write(Object[] record, OutputStream out) throws IOException {
        for (int i=0, len = record.length; i<len; i++) {
            final Object o = record[i];
            final byte[] bytes = o == null? nullRepr: o.toString().getBytes(charset);
            out.write(bytes);
            out.write(i<len-1? fieldDelim: lineDelim);
        }
    }

}
