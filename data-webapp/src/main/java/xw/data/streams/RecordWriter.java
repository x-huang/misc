package xw.data.streams;

import java.io.IOException;
import java.io.OutputStream;

public interface RecordWriter {

    default void pre(OutputStream out) throws IOException {}
    default void post(OutputStream out) throws IOException {}
    void write(Object[] record, OutputStream out) throws IOException;
}
