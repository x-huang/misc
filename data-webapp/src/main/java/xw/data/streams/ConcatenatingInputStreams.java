package xw.data.streams;

import java.io.IOException;
import java.io.InputStream;

public class ConcatenatingInputStreams extends InputStream {

	final private InputStream[] ins;
	private int index;
	private InputStream curr;
	
	ConcatenatingInputStreams(InputStream[] ins) {
		this.ins = new InputStream[ins.length];
		System.arraycopy(ins, 0, this.ins, 0, ins.length);
		curr = this.ins[0];
		index = 0;
	}
	

	@Override
	public int read() throws IOException {
		int b = curr.read();
		if (b != -1) return b;
		
		if (index < ins.length-1) {
			curr.close();
			index++;
			curr = ins[index];
			return read();
		} else {
			return -1;
		}
	}

	@Override
	public void close() throws IOException {
		IOException first = null;
		for (int i = index; i < ins.length; i++) {
			try {
				ins[i].close();
			} catch(IOException e){
				if (first == null) first = e;
			}
		}
		
		if (first != null) throw first;
	}
}
