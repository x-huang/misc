package xw.data.streams;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class PrettyJsonWriter implements RecordWriter {

    private final Gson gson = new Gson();
    private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private final OutputStreamWriter writer = new OutputStreamWriter(baos);
    private final byte[] NewLineBytes = ",\n  ".getBytes();
    private byte[] delim = null;

    @Override
    public void pre(OutputStream out) throws IOException {
        out.write("[\n".getBytes());
    }

    @Override
    public void post(OutputStream out) throws IOException {
        out.write("\n]".getBytes());
    }

    @Override
    public void write(Object[] record, OutputStream out) throws IOException {
        if (delim != null) out.write(delim);
        gson.toJson(record, writer);
        writer.flush();
        out.write(baos.toByteArray());
        delim = NewLineBytes;
        baos.reset();
    }
}
