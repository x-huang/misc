package xw.data.streams;

import xw.data.unistreams.stream.DataSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class Streams {

    private static InputStream getPreInputStream(RecordWriter writer) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            writer.pre(baos);
        } catch (IOException e) {
            throw new RuntimeException("error writing pre content", e);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    private static InputStream getPostInputStream(RecordWriter writer) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            writer.post(baos);
        } catch (IOException e) {
            throw new RuntimeException("error writing post content", e);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    private static InputStream getDataInputStream(final DataSource ds, RecordWriter writer) {

        return new InputStream() {

            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = baos.toByteArray();
            int index = 0;

            @Override
            public int read() throws IOException {
                try {
                    if (index == -1) {
                        return -1;
                    } else if (index == buffer.length) {
                        final Object[] record = ds.read();
                        if (record == null) {
                            index = -1;
                            return -1;
                        } else {
                            baos.reset();
                            writer.write(record, baos);
                            buffer = baos.toByteArray();
                            index = 0;
                            return read();
                        }
                    } else {
                        return buffer[index++];
                    }
                } catch (IOException e) {
                    throw e;
                }
            }
        };
    }

    public static InputStream wrapDataSource(final DataSource ds, String format) {

        final RecordWriter writer = Writers.get(format);

        return new ConcatenatingInputStreams(new InputStream[] {
                getPreInputStream(writer),
                getDataInputStream(ds, writer),
                getPostInputStream(writer)
        });
    }

}
