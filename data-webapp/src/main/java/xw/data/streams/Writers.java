package xw.data.streams;

public class Writers {

    public static RecordWriter get(String format) {
        switch (format) {
            case "tsv":
            case "csv":
            case "psv":
                final RecordTextWriter writer = new RecordTextWriter();
                if (format.equals("csv")) {
                    writer.setFieldDelimiter(',');
                } else if (format.equals("psv")) {
                    writer.setFieldDelimiter('|');
                }
                return writer;
            case "semijson":
            case "semi-json":
                return new SemiJsonWriter();
            case "json":
                return new CompactJsonWriter();
            case "prettyjson":
            case "pretty-json":
                return new PrettyJsonWriter();
            default:
                throw new IllegalArgumentException("unknown record formatter: " + format);
        }
    }
}
