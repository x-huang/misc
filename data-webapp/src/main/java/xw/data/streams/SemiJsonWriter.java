package xw.data.streams;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class SemiJsonWriter implements RecordWriter {

    private final Gson gson = new Gson();
    private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private final OutputStreamWriter writer = new OutputStreamWriter(baos);

    @Override
    public void write(Object[] record, OutputStream out) throws IOException {
        gson.toJson(record, writer);
        writer.flush();
        out.write(baos.toByteArray());
        out.write('\n');
        baos.reset();
    }
}
