package xw.data.cli;

import org.apache.hadoop.conf.Configuration;
import xw.data.common.util.HadoopUtils;
import xw.data.unistreams.env.Environment;
import xw.data.unistreams.resources.ResourceDescriptor;

public class Unzip {

    public static void main(String[] args) throws Exception {

        final ResourceDescriptor rd = ResourceDescriptor.parse(args[0]);
        final Configuration conf = Environment.of(rd.env).getHadoopConf();
        final String base = rd.getSingleUri().toString();

        try {
            HadoopUtils.unzipFiles(System.in, conf, base);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


/*
hadoop@node74-111:/mnt/xhuang/data-webapp$ curl -s 'http://10.102.74.111:8003/zip?rd=hdfs:///user/hadoop/apps/xplusone_ingestion' | java xw.data.cli.Unzip file:///mnt/tmp/xplusone/
unzip to: file:///mnt/tmp/xplusone/
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/mnt/xhuang/data-webapp/lib/log4j-slf4j-impl-2.0.2.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/usr/lib/hadoop-2.7.0/share/hadoop/common/lib/slf4j-log4j12-1.7.10.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
23:16:05.423 [main] DEBUG HadoopUtils - unzip directory base: file:///mnt/tmp/xplusone
17/01/04 23:16:05 WARN util.NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
23:16:05.818 [main] DEBUG PerformanceAdvisory - Falling back to shell based
23:16:06.150 [main] DEBUG HadoopUtils - creating directory: /mnt/tmp/xplusone/V1
23:16:06.151 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/OozieJobs.properties
23:16:06.180 [main] DEBUG HadoopUtils - creating directory: /mnt/tmp/xplusone/V1/conf
23:16:06.181 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/conf/log4j2.xml
23:16:06.191 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/conf/xplusone-ingestion.conf
23:16:06.201 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/coordinator.properties
23:16:06.212 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/coordinator.xml
23:16:06.222 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/env.sh
23:16:06.233 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/hive-site.xml
23:16:06.246 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/hive-site.xml.hive10
23:16:06.258 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/hive-site.xml_bak

23:16:48.092 [main] DEBUG HadoopUtils - creating directory: /mnt/tmp/xplusone/V1/lib
23:16:48.092 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/ingestion-xplusone-1.0.jar
23:16:48.107 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/jsch-0.1.44-1.jar
23:16:48.195 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/jtar-2.2.jar
23:16:48.209 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/log4j-api-2.0.2.jar
23:16:48.255 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/log4j-core-2.0.2.jar
23:16:48.471 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/log4j-slf4j-impl-2.0.2.jar
23:16:48.485 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/lib/slf4j-api-1.7.4.jar
23:16:48.501 [main] DEBUG HadoopUtils - overwriting file: /mnt/tmp/xplusone/V1/workflow.xml
hadoop@node74-111:/mnt/xhuang/data-webapp$
hadoop@node74-111:/mnt/xhuang/data-webapp$ find /mnt/tmp/xplusone/ | xargs ls -l
-rw-r--r-- 1 hadoop supergroup    363 Dec  2  2014 /mnt/tmp/xplusone/V1/conf/log4j2.xml
-rw-r--r-- 1 hadoop supergroup     12 Jan  4 23:16 /mnt/tmp/xplusone/V1/conf/.log4j2.xml.crc
-rw-r--r-- 1 hadoop supergroup    849 Dec  2  2014 /mnt/tmp/xplusone/V1/conf/xplusone-ingestion.conf
-rw-r--r-- 1 hadoop supergroup     16 Jan  4 23:16 /mnt/tmp/xplusone/V1/conf/.xplusone-ingestion.conf.crc
-rw-r--r-- 1 hadoop supergroup   1932 Dec  2  2014 /mnt/tmp/xplusone/V1/coordinator.properties
-rw-r--r-- 1 hadoop supergroup     24 Jan  4 23:16 /mnt/tmp/xplusone/V1/.coordinator.properties.crc
-rw-r--r-- 1 hadoop supergroup    605 Dec  2  2014 /mnt/tmp/xplusone/V1/coordinator.xml
-rw-r--r-- 1 hadoop supergroup     16 Jan  4 23:16 /mnt/tmp/xplusone/V1/.coordinator.xml.crc
-rw-r--r-- 1 hadoop supergroup     62 Dec  2  2014 /mnt/tmp/xplusone/V1/env.sh
-rw-r--r-- 1 hadoop supergroup     12 Jan  4 23:16 /mnt/tmp/xplusone/V1/.env.sh.crc
-rw-r--r-- 1 hadoop supergroup  84526 Jun 15  2016 /mnt/tmp/xplusone/V1/hive-site.xml
-rw-r--r-- 1 hadoop supergroup  81784 Apr 17  2015 /mnt/tmp/xplusone/V1/hive-site.xml_bak
-rw-r--r-- 1 hadoop supergroup    648 Jan  4 23:16 /mnt/tmp/xplusone/V1/.hive-site.xml_bak.crc
-rw-r--r-- 1 hadoop supergroup    672 Jan  4 23:16 /mnt/tmp/xplusone/V1/.hive-site.xml.crc
-rw-r--r-- 1 hadoop supergroup  51021 Dec  2  2014 /mnt/tmp/xplusone/V1/hive-site.xml.hive10
-rw-r--r-- 1 hadoop supergroup    408 Jan  4 23:16 /mnt/tmp/xplusone/V1/.hive-site.xml.hive10.crc
-rw-r--r-- 1 hadoop supergroup  17016 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/ingestion-xplusone-1.0.jar
-rw-r--r-- 1 hadoop supergroup    144 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.ingestion-xplusone-1.0.jar.crc
-rw-r--r-- 1 hadoop supergroup 213781 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/jsch-0.1.44-1.jar
-rw-r--r-- 1 hadoop supergroup   1680 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.jsch-0.1.44-1.jar.crc
-rw-r--r-- 1 hadoop supergroup  12999 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/jtar-2.2.jar
-rw-r--r-- 1 hadoop supergroup    112 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.jtar-2.2.jar.crc
-rw-r--r-- 1 hadoop supergroup 122755 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/log4j-api-2.0.2.jar
-rw-r--r-- 1 hadoop supergroup    968 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.log4j-api-2.0.2.jar.crc
-rw-r--r-- 1 hadoop supergroup 783469 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/log4j-core-2.0.2.jar
-rw-r--r-- 1 hadoop supergroup   6132 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.log4j-core-2.0.2.jar.crc
-rw-r--r-- 1 hadoop supergroup  22900 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/log4j-slf4j-impl-2.0.2.jar
-rw-r--r-- 1 hadoop supergroup    188 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.log4j-slf4j-impl-2.0.2.jar.crc
-rw-r--r-- 1 hadoop supergroup  26083 Dec  2  2014 /mnt/tmp/xplusone/V1/lib/slf4j-api-1.7.4.jar
-rw-r--r-- 1 hadoop supergroup    212 Jan  4 23:16 /mnt/tmp/xplusone/V1/lib/.slf4j-api-1.7.4.jar.crc
-rw-r--r-- 1 hadoop supergroup    393 Dec  2  2014 /mnt/tmp/xplusone/V1/OozieJobs.properties
-rw-r--r-- 1 hadoop supergroup     12 Jan  4 23:16 /mnt/tmp/xplusone/V1/.OozieJobs.properties.crc
-rw-r--r-- 1 hadoop supergroup   1926 Mar 26  2016 /mnt/tmp/xplusone/V1/workflow.xml
-rw-r--r-- 1 hadoop supergroup     24 Jan  4 23:16 /mnt/tmp/xplusone/V1/.workflow.xml.crc

*/