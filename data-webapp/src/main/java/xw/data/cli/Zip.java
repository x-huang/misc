package xw.data.cli;

import org.apache.hadoop.conf.Configuration;
import xw.data.common.util.HadoopUtils;
import xw.data.unistreams.env.Environment;
import xw.data.unistreams.resources.ResourceDescriptor;

import java.util.Arrays;
import java.util.List;

public class Zip {

    public static void main(String[] args) throws Exception {
        final ResourceDescriptor rd = ResourceDescriptor.parse(args[0]);
        final Configuration conf = Environment.of(rd.env).getHadoopConf();
        final String base = rd.getSingleUri().toString();

        final List<String> paths = Arrays.asList(args).subList(1, args.length);
        if (paths.size() > 0) {
            HadoopUtils.zipFiles(System.out, conf, base, paths);
        } else {
            HadoopUtils.zipFiles(System.out, conf, base);
        }
    }
}

/*
hadoop@node74-111:/mnt/xhuang/data-webapp$ java xw.data.cli.Zip prod::hdfs:///user/hadoop/apps/xplusone_ingestion | java xw.data.cli.Unzip ci::hdfs:///tmp/xplusone
...

hadoop@node78-199:~$ hadoop fs -ls /tmp/xplusone/V1/
Found 10 items
-rw-r--r--   3 hadoop hadoop        393 2014-12-02 06:53 /tmp/xplusone/V1/OozieJobs.properties
drwxr-xr-x   - hadoop hadoop          0 2017-01-04 23:39 /tmp/xplusone/V1/conf
-rw-r--r--   3 hadoop hadoop       1932 2014-12-02 06:53 /tmp/xplusone/V1/coordinator.properties
-rw-r--r--   3 hadoop hadoop        605 2014-12-02 06:53 /tmp/xplusone/V1/coordinator.xml
-rw-r--r--   3 hadoop hadoop         62 2014-12-02 06:53 /tmp/xplusone/V1/env.sh
-rw-r--r--   3 hadoop hadoop      84526 2016-06-15 20:25 /tmp/xplusone/V1/hive-site.xml
-rw-r--r--   3 hadoop hadoop      51021 2014-12-02 06:53 /tmp/xplusone/V1/hive-site.xml.hive10
-rw-r--r--   3 hadoop hadoop      81784 2015-04-17 22:50 /tmp/xplusone/V1/hive-site.xml_bak
drwxr-xr-x   - hadoop hadoop          0 2017-01-04 23:39 /tmp/xplusone/V1/lib
-rw-r--r--   3 hadoop hadoop       1926 2016-03-26 01:32 /tmp/xplusone/V1/workflow.xml

*/

