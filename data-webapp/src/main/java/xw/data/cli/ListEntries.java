package xw.data.cli;

import xw.data.common.util.HadoopUtils;

import java.net.URI;

public class ListEntries {
    public static void main(String[] args) throws Exception {
        HadoopUtils.listEntriesRecursively(URI.create(args[0])).forEach(System.out::println);
    }
}
