package xw.data.common.util;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {

    public static void zipMultipleFiles(OutputStream dest, List<String> paths,
                                        Function<String, InputStream> open)
            throws IOException {

        final ZipOutputStream zos =  new ZipOutputStream(new BufferedOutputStream(dest));
        final byte[] buffer = new byte[32 * 1024];

        for (String p: paths) {
            zos.putNextEntry(new ZipEntry(p));
            if (! p.endsWith("/")) {
                try (final InputStream in = open.apply(p)) {
                    int length;
                    while ((length = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, length);
                    }
                }
            }
            zos.closeEntry();
        }
        zos.close();
    }

}
