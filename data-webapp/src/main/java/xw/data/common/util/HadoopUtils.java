package xw.data.common.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class HadoopUtils {

    private static final Logger logger = LoggerFactory.getLogger(HadoopUtils.class);

    static public Configuration newConf() {
        return new Configuration();
    }

	static private void addEntriesRecursively(FileSystem fs, String base, String subEntry,
											  List<String> entries) throws IOException {

		final Path path = new Path(base + "/" + subEntry);
		final FileStatus status = fs.getFileStatus(path);
		if (status.isDirectory()) {
            if (! subEntry.isEmpty()) {
                entries.add(subEntry + "/");
            }
			for (FileStatus s : fs.listStatus(path)) {
				final String segName = s.getPath().getName();
                final String sub = subEntry.isEmpty()? segName: (subEntry + "/" + segName);
				addEntriesRecursively(fs, base, sub, entries);
			}
		} else {
			entries.add(subEntry);
		}
	}

    public static List<String> listEntriesRecursively(FileSystem fs, URI uri) throws IOException {
        final List<String> entries = new ArrayList<>();
		addEntriesRecursively(fs, uri.getPath(), "", entries);
		return entries;
    }

	public static List<String> listEntriesRecursively(Configuration conf, URI uri) throws IOException {
		return listEntriesRecursively(FileSystem.get(uri, conf), uri);
    }

    public static List<String> listEntriesRecursively(URI uri) throws IOException {
		final Configuration conf = new Configuration();
		return listEntriesRecursively(FileSystem.get(uri, conf), uri);
    }

    private static String removeTrailingSlashes(String s) {
        return s.replaceAll("/*$", "");
    }

    private static String removePrecedingSlashes(String s) {
        return s.replaceAll("^/*", "");
    }

    private static void walkEntriesRecursively(FileSystem fs, String base, String entry,
                                               BiConsumer<String, FileStatus> action)
            throws IOException {
        final Path path = new Path(base + "/" + entry);
		final FileStatus status = fs.getFileStatus(path);
        action.accept(entry, status);

		if (status.isDirectory()) {
			for (FileStatus s : fs.listStatus(path)) {
				final String segName = s.getPath().getName();
                final String sub = entry.isEmpty()? segName: (entry + "/" + segName);
				walkEntriesRecursively(fs, base, sub, action);
			}
		}
    }

    private static String getRelativePath(String prefix, FileStatus status) {
        String s = status.getPath().getName();
        if (! prefix.isEmpty()) {
            s = prefix + "/" + s;
        }
        if (status.isDirectory()) {
            s += "/";
        }
        return s;

    }

    public static void zipFiles(OutputStream dest, Configuration conf,
                                String base, List<String> paths)
            throws IOException {

        final URI baseUri = URI.create(removeTrailingSlashes(base));
        logger.debug("zip directory base: {}", baseUri);
        final FileSystem fs = FileSystem.get(baseUri, conf);

        final ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(dest));
        final byte[] buffer = new byte[32 * 1024];


        for (String p: paths) {
            final String p2 = removeTrailingSlashes(removePrecedingSlashes(p));
            logger.debug("looking into file: {}", p2);

            walkEntriesRecursively(fs, base, p2, (entry, status) -> {
                try {
                    if (entry.isEmpty()) {
                        return;
                    }
                    final ZipEntry ze = new ZipEntry(status.isDirectory()? (entry +"/"): entry);
                    ze.setLastModifiedTime(FileTime.fromMillis(status.getModificationTime()));
                    ze.setLastAccessTime(FileTime.fromMillis(status.getAccessTime()));
                    zos.putNextEntry(ze);
                    logger.debug("adding zip entry: {}", ze.getName());

                    if (! status.isDirectory()) {
                        final Path path = new Path(baseUri.getPath() + "/" + entry);
                        try (final InputStream in = fs.open(path)) {
                            int len;
                            while ((len = in.read(buffer)) > 0) {
                                zos.write(buffer, 0, len);
                            }
                        }
                    }
                    zos.closeEntry();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        zos.close();
        logger.debug("zip output stream closed");
    }

    public static void zipFiles(OutputStream dest, Configuration conf, String base)
            throws IOException {
        zipFiles(dest, conf, base, Collections.singletonList(""));
    }

    public static void unzipFiles(InputStream src, Configuration conf, String base)
            throws IOException {

        final URI baseUri = URI.create(removeTrailingSlashes(base));
        logger.debug("unzip directory base: {}", baseUri);

        final FileSystem fs = FileSystem.get(baseUri, conf);

        final ZipInputStream zis = new ZipInputStream(src);
        final byte[] buffer = new byte[32 * 1024];
        ZipEntry ze;

        while ((ze = zis.getNextEntry()) != null) {
            final Path path = new Path(baseUri.getPath() + "/" + ze.getName());
            if (ze.isDirectory()) {
                logger.debug("creating directory: {}", path);
                fs.mkdirs(path);
            } else {
                logger.debug("overwriting file: {}", path);
                try (OutputStream out = fs.create(path, true)) {
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        out.write(buffer, 0, len);
                    }
                }
            }
            final long atime = ze.getLastAccessTime().toMillis();
            try {
                fs.setTimes(path, -1, atime);
            } catch (IOException ignored) {
                // to avoid the exception: Access time for hdfs is not configured
            }
            final long mtime = ze.getLastModifiedTime().toMillis();
            fs.setTimes(path, mtime, -1);
            zis.closeEntry();
        }

        zis.close();
    }

}
