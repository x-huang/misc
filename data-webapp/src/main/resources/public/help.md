# data-front: a web interface to multi-cluster data resources

### API Endpoints

- GET /help.html

    Get this help document

- GET /resource?rd=`<rd>` <br/>POST /resource - `<rd>`

    Download data(hdfs/s3 file, hive table, jdbc table, etc) referenced by *resource descriptor*.

    Supported format: *tsv, csv, psv, semi-json, json, pretty-json*. Default is *tsv*.

- GET /zip?rd=`<rd>` <br/>POST /zip - `<rd>`
    Download a zipped directory on hdfs/s3 file system referenced by *resource descriptor*.

- GET /describe?rd=`<rd>` <br/>POST /describe - `<rd>`
    Get resource meta information in JSON

- POST /query - `<sql-stmt>`
    Execute an EAQL query

### Examples


```sh
$ curl 'http://localhost:8003/resource?rd=hive:(meta_hd.skus%3F_limit=3)%3F_format=pretty-json'
[
  [1,546,"ps3","ps3/nhl-2011-ps3/main","NHL 2011 PS3",2011,"ps3","2010-09-04","SPORTS","NHL",0,3,0,"130865",0,"Sports","nhl-2011-ps3","nhl-2011-ps3",0,8,0,"2013-01-11","3.09.x",0,"null",null,null],
  [2,541,"ps3","ps3/nhl-2011-ps3-demo/main","NHL 2011 PS3 DEMO",2011,"ps3","2010-08-17","SPORTS","NHL",0,3,0,"130865",0,"Sports","nhl-2011-ps3-demo","nhl-2011-ps3-demo",0,8,0,"null","3.09.x",0,"null",null,null],
  [3,545,"xbl2","xbl2/nhl-2011-xbl2/main","NHL 2011 Xbox 360",2011,"xbox","2010-09-04","SPORTS","NHL",0,3,0,"130866",0,"Sports","nhl-2011-xbl2","nhl-2011-xbl2",0,8,0,"2013-01-11","3.09.x",0,"null",null,null]
]
```
### Also See

- resource descriptor README: 
> [unistreams.html](/unistreams.html)

- EAQL README: 
> [eaql.html](/eaql.html)