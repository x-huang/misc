#!/usr/bin/env bash

# http://stackoverflow.com/questions/11203483/run-a-java-application-as-a-service-on-linux/21283530#21283530

if [ $# -eq 0 ]; then
    echo "[usage] service/sh [start/stop/restart/status]"
    exit 1
fi

pushd `dirname $0` > /dev/null
BIN_DIR=`pwd`
popd > /dev/null

DIST_DIR=$(dirname $BIN_DIR)
PID_FILE=$DIST_DIR/running.pid
echo "[info] located distribution dir: $DIST_DIR"

# optional hadoop classpath inclusion
HADOOP_CLASSPATH=`hadoop classpath 2> /dev/null`
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/hadoop-2.7.0/lib/native/
export CLASSPATH=$DIST_DIR/conf/:$DIST_DIR/lib/*:$HADOOP_CLASSPATH
echo "[info] included hadoop jars in java classpath"

# user configuration
SERVICE_NAME=data-front
JAVA_ARGS=
JAVA_MAIN=xw.data.webapp.Main
APP_ARGS="--port=8003 --public-assets=$DIST_DIR/public"

case $1 in
    start)
        echo "[info] starting service $SERVICE_NAME ..."
        if [ ! -f $PID_FILE ]; then
            # nohup java $JAVA_ARGS $JAVA_MAIN $APP_ARGS 2>> /dev/null >> /dev/null &
            nohup java $JAVA_ARGS $JAVA_MAIN $APP_ARGS &
            echo $! > $PID_FILE
            echo "[info] service $SERVICE_NAME started ..."
        else
            echo "[info] service $SERVICE_NAME is already running ..."
        fi
    ;;
    stop)
        if [ -f $PID_FILE ]; then
            PID=$(cat $PID_FILE);
            echo "[info] stopping service $SERVICE_NAME ..."
            kill $PID;
            echo "[info] service $SERVICE_NAME stopped ..."
            rm $PID_FILE
        else
            echo "[info] service $SERVICE_NAME is not running ..."
        fi
    ;;
    restart)
        if [ -f $PID_FILE ]; then
            PID=$(cat $PID_FILE);
            echo "[info] stopping service $SERVICE_NAME ...";
            kill $PID;
            echo "[info] service $SERVICE_NAME stopped ...";
            rm $PID_FILE
            echo "[info] starting service $SERVICE_NAME ..."
            # nohup java $JAVA_ARGS $JAVA_MAIN $APP_ARGS 2>> /dev/null >> /dev/null &
            nohup java $JAVA_ARGS $JAVA_MAIN $APP_ARGS &
            echo $! > $PID_FILE
            echo "[info] service $SERVICE_NAME started ..."
        else
            echo "[info] service $SERVICE_NAME is not running ..."
        fi
    ;;
    status)
        if [ -f $PID_FILE ]; then
            echo "[info] service $SERVICE_NAME pid file exists"
            PID=$(cat $PID_FILE);
            ps -up $PID
        else
            echo "[info] service $SERVICE_NAME is not running ..."
        fi
    ;;
esac