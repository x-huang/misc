A collection of my side projects.

- *unistreams* - A unified abstraction layer of many data media/formats: hive, jdbc, rc/orc files, kryo, text, ...
- *EAQL* - A SQL-like analytic query engine working on various data media, based on unistreams.
- *streams* - Read/write cluster data resources as binary streams.
- *repartition* - Small tool to re-partition hive table.
- *orcfile* - Utility tools to read/write ORC files (obsolete).
- *hive-lite* - Embedding Hive queries programmably using the CLIService API. 
- *scalatra-hite* - Scalatra wrapper of *hive-lite* allowing executing Hive Queries through web API.
- *data-webapp* - Web application wrapping *eaql* and *hive-lite*. 
