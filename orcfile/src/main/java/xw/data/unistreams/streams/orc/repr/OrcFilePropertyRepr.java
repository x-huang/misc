package xw.data.unistreams.streams.orc.repr;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.ColumnStatistics;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.Reader;
import xw.data.unistreams.common.hadoop.HadoopCommon;

@XmlRootElement
public class OrcFilePropertyRepr implements Serializable
{
	private static final long serialVersionUID = 4493248686425092003L;
	
	public final long rows;
	public final long contentLength;
	public final int rowIndexStride;
	public final String compression;
	public final int compressionSize;
	public final String inspectorCategory;
	public final String typeName;
	public final List<String> metadataKeys;
	public final List<OrcFileColumnStatisticRepr> columnStatistics;
    // skip stripe information cuz its really internal
	// public final List<OrcFileStripeRepr> stripes;
	public final Map<String, String> metadata;
	
	public OrcFilePropertyRepr(Reader reader) {
		rows = reader.getNumberOfRows();
		contentLength = reader.getContentLength();
		rowIndexStride = reader.getRowIndexStride();
		compression = reader.getCompression().toString();
		compressionSize = reader.getCompressionSize();
		inspectorCategory = reader.getObjectInspector().getCategory().toString();
		typeName = reader.getObjectInspector().getTypeName();
		metadataKeys = new ArrayList<>();
		for (String key: reader.getMetadataKeys()) {
			metadataKeys.add(key);
		}
		columnStatistics = new ArrayList<>();
		ColumnStatistics[] stats = reader.getStatistics();
		for (ColumnStatistics stat : stats) {
			columnStatistics.add(new OrcFileColumnStatisticRepr(stat));
		}

        /*
		stripes = new ArrayList<>();
		for (StripeInformation stripe: reader.getStripes()) {
			stripes.add(new OrcFileStripeRepr(stripe));
		}*/
		metadata = new HashMap<>();
		for (String key: reader.getMetadataKeys()) {
			String val = reader.getMetadataValue(key).toString();
			metadata.put(key, val);
		}
	}

	@Override
	public String toString() {
		return "{" +
				"\n\trows=" + rows +
				", \n\tcontentLength=" + contentLength +
				", \n\trowIndexStride=" + rowIndexStride +
				", \n\tcompression='" + compression + '\'' +
				", \n\tcompressionSize=" + compressionSize +
				", \n\tinspectorCategory='" + inspectorCategory + '\'' +
				", \n\ttypeName='" + typeName + '\'' +
				", \n\tmetadataKeys=" + metadataKeys +
				", \n\tcolumnStatistics=" + columnStatistics +
				", \n\tmetadata=" + metadata +
				"\n}";
	}

	public static OrcFilePropertyRepr fromURI(URI uri) throws IOException {
		final Path path = new Path(uri);
		final FileSystem fs = HadoopCommon.getFileSystem(uri);
		final Reader reader = OrcFile.createReader(fs, path);
        return new OrcFilePropertyRepr(reader);
	}
}
