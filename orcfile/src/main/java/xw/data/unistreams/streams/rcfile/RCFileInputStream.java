package xw.data.unistreams.streams.rcfile;

import java.io.IOException;
import java.net.URI;
/*import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;*/
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFileRecordReader;
import org.apache.hadoop.hive.ql.io.orc.Writables;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStruct;
import org.apache.hadoop.hive.serde2.columnar.ColumnarStructBase;
import org.apache.hadoop.hive.serde2.columnar.LazyBinaryColumnarStruct;
import org.apache.hadoop.hive.serde2.lazy.LazyObjectBase;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryObject;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileSplit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.common.hive.objectinspector.LazyObjectUtils;
import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.common.hive.objectinspector.ColumnarProperties;
import xw.data.unistreams.core.RecordInputStream;


/*
 * Xiaowan: 
 * 
 * see Hive 0.12.0 RELEASE_NOTES.txt !!!
 * 
 * 	[HIVE-4475] - Switch RCFile default to LazyBinaryColumnarSerDe
 */

class RCFileInputStream implements RecordInputStream
{
	private final static Logger logger = LoggerFactory.getLogger(RCFileInputStream.class);

	private final RCFileRecordReader<LongWritable, BytesRefArrayWritable> reader;
	private long count;
	private long limit;
	private final StructObjectInspector oi;
	private final ColumnarStructBase lazyStruct;
	
	transient private LongWritable key = new LongWritable();
	transient private BytesRefArrayWritable value = new BytesRefArrayWritable();
	
/*	private static CharsetDecoder _decoder;
	static {
		_decoder = Charset.forName("UTF-8").newDecoder()
				.onMalformedInput(CodingErrorAction.REPLACE)
				.onUnmappableCharacter(CodingErrorAction.REPLACE);
	}*/
	
	protected RCFileInputStream(URI uri, ObjectInspector oi, ColumnarProperties cp)
			throws IOException
	{
		this.oi = (StructObjectInspector) oi;
		final Configuration conf = HadoopCommon.getConf();
		final ArrayList<Integer> notSkipIds = new ArrayList<>();

		final boolean useBinarySerDe = cp.usingBinarySerDe();
		if (useBinarySerDe) {
			lazyStruct = new LazyBinaryColumnarStruct(this.oi, notSkipIds);
		} else {
			// ObjectInspectorUtils.getDefaultSerDeParameters().getNullSequence();
			final Text nullSequence;
			try {
				nullSequence = cp.getSerDeParameters().getNullSequence();
			} catch (SerDeException e) {
				throw new IOException("cannot initialize columnar serde parameters", e);
			}
			lazyStruct = new ColumnarStruct(this.oi, notSkipIds, nullSequence);
		}
		
		final Path path = new Path(uri);
		final long length = HadoopCommon.getFileStatusRepr(uri).length;
		final FileSplit split = new FileSplit(path, 0, length, (String[])null);
		reader = new RCFileRecordReader<>(conf, split);
		count = 0;
		limit = -1;

		// final ColumnarProperties colProps = cp;
		
		logger.debug("initialized reading from RC File: {} ({})", uri, cp);
	}
	
	
	public RCFileInputStream(URI uri, String schema, ColumnarProperties rowFormat) throws IOException {
		this(uri, ObjectInspectorUtils.getColumnarStructObjectInspector(schema, rowFormat), rowFormat);
	}
	
	public RCFileInputStream setLimit(long limit) {
		this.limit = limit;
		return this;
	}

	public String getSchema() {
		return oi.getTypeName();
	}
	
	@Override
	public Object[] read() throws IOException {
		if (limit > 0 && count >= limit) {
			return null;
		}
		
		if (reader.next(key, value)) {
			lazyStruct.init(value);
			final int n = value.size();
			final Object[] row = new Object[n];
			for (int i=0; i<n; i++)  {
				final Object lazyOrWritable = lazyStruct.getField(i);
				if (lazyOrWritable == null) {
					row[i] = null;
				} else if (lazyOrWritable instanceof Writable) {
					row[i] = Writables.getValueObject((Writable) lazyOrWritable);
				} else if (lazyOrWritable instanceof LazyBinaryObject<?>) {
					row[i] = LazyObjectUtils.lazyBinaryToPOJO((LazyBinaryObject<?>) lazyOrWritable);
				} else {
					row[i] = LazyObjectUtils.lazyToPOJO((LazyObjectBase) lazyOrWritable);
				}
			}
			count++;
			return row;
		} else {
			return null;
		}
	}

    @Override
	public long getCount() {
		return count;
	}

	@Override
	public void close() throws IOException {
		reader.close();
	}
}
