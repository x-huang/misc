package xw.data.unistreams.streams.orc;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.Reader;
import org.apache.hadoop.hive.ql.io.orc.RecordReader;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.core.RecordInputStream;

class OrcFileInputStream implements RecordInputStream
{
	private final static Logger logger = LoggerFactory.getLogger(OrcFileInputStream.class);

	public final URI uri;
	final private StructObjectInspector oi;
	final private List<? extends StructField> fields;
	final transient private RecordReader rows;
	transient private Object row;
	private long count;
	private long limit;

	public OrcFileInputStream(URI file) throws IOException {
		this.uri = file;
		final Path path = new Path(uri);
		final FileSystem fs = HadoopCommon.getFileSystem(uri);
		final Reader reader = OrcFile.createReader(fs, path);
		oi = (StructObjectInspector) reader.getObjectInspector();
		fields = oi.getAllStructFieldRefs();
		rows = reader.rows(null);
		
		row = null;
		count = 0;
		limit = -1;

		logger.debug("initialized reading from ORC file: {} ({} rows)", file, reader.getNumberOfRows());
	}
	
	public URI getLocationUri() {
		return uri;
	}
	
	public String getSchema() {
		return oi.getTypeName();
	}
	
	public OrcFileInputStream setLimit(long limit) {
		this.limit = limit;
		return this;
	}

	@Override
	public Object[] read() throws IOException {
		if (limit > 0 && count >= limit) {
			return null;
		}
		
		if (! rows.hasNext()) {
			return null;
		}
		
		row = rows.next(row);
		count++;
		final Object[] data = new Object[fields.size()];
		for (int i=0; i< fields.size(); i++) {
			final Object o = oi.getStructFieldData(row, fields.get(i));
			final ObjectInspector fieldInspector = fields.get(i).getFieldObjectInspector();
			data[i] = ObjectInspectorUtils.inspectObject(o, fieldInspector);
		}
		return data;
	}
	
	@Override
	public void close() throws IOException {
		rows.close();
		row = null;
	}

    @Override
	public long getCount() {
		return count;
	}
}
