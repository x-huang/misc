package xw.data.unistreams.streams.orc;

import xw.data.unistreams.core.RecordInputStream;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


class MultipleOrcFilesObjectInput implements RecordInputStream
{
	final private List<URI> uris;
	private int index;
	private long count;
	transient private OrcFileInputStream curr;
	
	public MultipleOrcFilesObjectInput(List<URI> uris) throws IOException {
		this.uris = new ArrayList<>(uris);
		index = 0;
		curr = new OrcFileInputStream(uris.get(0));
		count = 0;
	}
	
	public List<URI> getLocationUris() {
		return uris;
	}

	public MultipleOrcFilesObjectInput setLimit(long limit) {
		throw new UnsupportedOperationException("cannot set limit to a multiple ORC file input");
	}
	
	@Override
	public Object[] read() throws IOException {
		final Object[] data = curr.read();
		if (data != null) {
			return data;
		}
		
		if (index < uris.size()-1) {
			curr.close();
			count += curr.getCount();
			index++;
			curr = new OrcFileInputStream(uris.get(index));
			return read();
		} else {
			return null;
		}
	}

	
	@Override
	public void close() throws IOException {
		curr.close();
	}

	@Override
	public long getCount() {
		return count + curr.getCount();
	}

}
