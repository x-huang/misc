package xw.data.unistreams.streams.orc;

import xw.data.unistreams.core.RecordInputStream;
import xw.data.unistreams.core.RecordOutputStream;
import xw.data.unistreams.core.ResourceOperator;
import xw.data.unistreams.streams.orc.repr.OrcFilePropertyRepr;

import java.io.IOException;

/**
 * Created by xhuang on 5/25/16.
 */
public class OrcFileOperator extends ResourceOperator
{
    @Override
    public RecordInputStream openInputStream() throws IOException {
        if (uris.size() == 0) {
            throw new IllegalArgumentException("expecting one or more URIs");
        } else if (uris.size() == 1) {
            final OrcFileInputStream orc = new OrcFileInputStream(uris.get(0));
            if (limit > 0) {
                orc.setLimit(limit);
            }
            return orc;
        } else {
            final MultipleOrcFilesObjectInput orc = new MultipleOrcFilesObjectInput(uris);
            if (limit > 0) {
                orc.setLimit(limit);
            }
            return orc;
        }
    }

    @Override
    public RecordOutputStream openOutputStream() throws IOException {
        if (uris.size() != 1) {
            throw new IllegalArgumentException("expecting exactly one URI");
        }
        if (schema == null) {
            throw new NullPointerException("missing schema string");
        }
        final OrcFileOutputStream orc = new OrcFileOutputStream(uris.get(0), schema);
        orc.setOverwrite(true).initialize();
        return orc;
    }

    @Override
    public OrcFilePropertyRepr describe() throws IOException {
        if (uris.size() != 1) {
            throw new IllegalArgumentException("expecting exactly one URI");
        }
        return OrcFilePropertyRepr.fromURI(uris.get(0));
    }
}
