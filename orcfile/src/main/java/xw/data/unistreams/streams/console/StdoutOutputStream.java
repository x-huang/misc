package xw.data.unistreams.streams.console;

import xw.data.unistreams.serde.RecordFormatter;
import xw.data.unistreams.core.RecordOutputStream;
import xw.data.unistreams.serde.RecordTextFormatter;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by xhuang on 5/26/16.
 */
public class StdoutOutputStream implements RecordOutputStream
{
    private PrintStream ps = System.out;
    private RecordFormatter formatter = new RecordTextFormatter();
    private int count = 0;

    protected StdoutOutputStream() {

    }

    public static StdoutOutputStream create() {
        return new StdoutOutputStream();
    }

    public RecordFormatter getRecordFormatter() {
        return formatter;
    }

    public StdoutOutputStream setRecordFormatter(RecordFormatter formatter) {
        this.formatter = formatter;
        return this;
    }

    public StdoutOutputStream setPrintStream(PrintStream ps) {
        this.ps = ps;
        return this;
    }

    @Override
    public void write(Object[] record) throws IOException {
        ps.println(formatter.format(record));
        count += 1;
    }

    @Override
    public void close() throws Exception {

    }

    @Override
    public long getCount() {
        return count;
    }
}
