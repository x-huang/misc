package xw.data.unistreams.streams.rcfile;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;

import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.common.hive.objectinspector.ColumnarProperties;
import xw.data.unistreams.core.RecordInputStream;

class MultipleRCFilesInputStream implements RecordInputStream
{
	final private List<URI> uris;
	private int index;
	private long count;
	final private String schema;
	final private ObjectInspector oi;
	final private ColumnarProperties colProps;
	transient private RCFileInputStream curr;

	public MultipleRCFilesInputStream(List<URI> uris, String schema, ColumnarProperties cp) throws IOException {
		this.uris = uris;
		index = 0;
		this.schema = schema;
		oi = ObjectInspectorUtils.getColumnarStructObjectInspector(schema, cp);
		this.colProps = cp;
		curr = new RCFileInputStream(this.uris.get(index), oi, cp);
		count = 0;
	}

	public String getSchema() {
		return schema;
	}

	public MultipleRCFilesInputStream setLimit(long limit) {
		throw new UnsupportedOperationException("cannot set limit to a multiple RCFile input");
	}
	
	@Override
	public Object[] read() throws IOException {
		final Object[] data = curr.read();
		if (data != null) {
			return data;
		}

		if (index < uris.size() - 1) {
			curr.close();
			count += curr.getCount();
			index++;
			curr = new RCFileInputStream(uris.get(index), oi, colProps);
			return read();
		} else {
			return null;
		}
	}

	@Override
	public long getCount() {
		return count + curr.getCount();
	}

	@Override
	public void close() throws IOException {
		curr.close();
	}

}
