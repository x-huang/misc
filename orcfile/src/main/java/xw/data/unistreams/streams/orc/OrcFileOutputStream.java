package xw.data.unistreams.streams.orc;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.CompressionKind;
import org.apache.hadoop.hive.ql.io.orc.OrcFile;
import org.apache.hadoop.hive.ql.io.orc.OrcUtils;
import org.apache.hadoop.hive.ql.io.orc.Writer;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;
import xw.data.unistreams.core.RecordOutputStream;

class OrcFileOutputStream implements RecordOutputStream
{
	private final static Logger logger = LoggerFactory.getLogger(OrcFileOutputStream.class);

	final private URI uri;
	final private String schema;
	private Writer writer = null;
	private long stripeSize = 102_400L;	// NOTE: stripe size must be a multiple of io.bytes.per.checksum, IOException thrown otherwise
	private int bufferSize = 16_000;	// NOTE: shown in TBLPROPERTIES/orc.compress.size
	private boolean blockPadding;
	private boolean overwrite = false;
	private CompressionKind compress = CompressionKind.SNAPPY;  // NOTE: shown in TBLPROPERTIES/orc.compress; http://blog.erdemagaoglu.com/post/4605524309/lzo-vs-snappy-vs-lzf-vs-zlib-a-comparison-of
	private long count;
	
	public OrcFileOutputStream(URI uri, String schema) {
		this.uri = uri;
		this.schema = schema;
		blockPadding = ! uri.toString().startsWith("s3n://");
		
		count = 0;
	}
	
	public OrcFileOutputStream setStripeSize(long size) {
		if (size <= 0) {
			throw new IllegalArgumentException("zero or negative stripe size: " + size);
		}
		stripeSize = size;
		return this;
	}
	
	public OrcFileOutputStream setBufferSize(int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("zero or negative buffer size: " + size);
		}
		bufferSize = size;
		return this;
	}
	
	public OrcFileOutputStream setBlockPadding(boolean padding) {
		blockPadding = padding;
		return this;
	}
	
	public OrcFileOutputStream setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
		return this;
	}
	
	public OrcFileOutputStream setCompress(String compress) {
		this.compress = CompressionKind.valueOf(compress);
		return this;
	}
	
	public void initialize() throws IOException {
		final Path path = new Path(uri);

		//_writerInspector = StructObjectInspectorBuilder.fromSchema(schema);
		final ObjectInspector oi = ObjectInspectorUtils.getOrcObjectInspector(schema);
		assert oi.getTypeName().equals(schema);

        final Configuration conf = HadoopCommon.getConf();
		final OrcFile.WriterOptions opts = OrcFile.writerOptions(conf)
				.inspector(oi)
				.stripeSize(stripeSize)
				.bufferSize(bufferSize)
				.blockPadding(blockPadding)
				.compress(compress);
		
		if (overwrite && HadoopCommon.delete(uri)) {
			logger.debug("deleted existing file: {}", uri);
		}
		
		writer = OrcFile.createWriter(path, opts);
		
		logger.debug("initialized writing to ORC file: {} " +
						"(compress: {}, compress-size: {}, " +
						"stripe-size: {}, block-padding: {}, typename: {})",
				uri, compress, bufferSize, stripeSize, blockPadding,
				ObjectInspectorUtils.describeObjectInspector(oi));
	}
	
	public String getSchema() {
		return schema;
	}
	
	public URI getLocationUri() {
		return uri;
	}
	
	@Override
	public void close() throws IOException {
		writer.close();
	}

    @Override
	public long getCount() {
		return count;
	}

	@Override
	public void write(Object[] record) throws IOException {
		writer.addRow(OrcUtils.getOrcStruct(record));
		count++;
	}
}
