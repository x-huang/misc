package xw.data.unistreams.streams.orc.repr;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.hive.ql.io.orc.*;

@XmlRootElement
public class OrcFileColumnStatisticRepr implements Serializable
{
    @Override
    public String toString() {
        return "{className='" + className + '\'' +
                ", statistics=" + statistics +
                '}';
    }

    private static final long serialVersionUID = 7888179701511301860L;
	
	// public final String descr;
	public final String className;
	public final Map<String, Object> statistics = new HashMap<>();

	public OrcFileColumnStatisticRepr(ColumnStatistics stat) {

		className = stat.getClass().getSimpleName();

		statistics.put("count", stat.getNumberOfValues());
		if (stat instanceof BinaryColumnStatistics) {
			statistics.put("sum", ((BinaryColumnStatistics) stat).getSum());
		} else if (stat instanceof BooleanColumnStatistics) {
			statistics.put("true-count", ((BooleanColumnStatistics) stat).getTrueCount());
			statistics.put("false-count", ((BooleanColumnStatistics) stat).getFalseCount());
		} else if (stat instanceof DateColumnStatistics) {
			statistics.put("min", ((DateColumnStatistics) stat).getMinimum());
			statistics.put("max", ((DateColumnStatistics) stat).getMaximum());
		} else if (stat instanceof DecimalColumnStatistics) {
			statistics.put("min", ((DecimalColumnStatistics) stat).getMinimum());
			statistics.put("max", ((DecimalColumnStatistics) stat).getMaximum());
			statistics.put("sum", ((DecimalColumnStatistics) stat).getSum());
		} else if (stat instanceof DoubleColumnStatistics) {
			statistics.put("min", ((DoubleColumnStatistics) stat).getMinimum());
			statistics.put("max", ((DoubleColumnStatistics) stat).getMaximum());
			statistics.put("sum", ((DoubleColumnStatistics) stat).getSum());
		} else if (stat instanceof IntegerColumnStatistics) {
			statistics.put("min", ((IntegerColumnStatistics) stat).getMinimum());
			statistics.put("max", ((IntegerColumnStatistics) stat).getMaximum());
			statistics.put("sum", ((IntegerColumnStatistics) stat).getSum());
			statistics.put("is-sum-defined", ((IntegerColumnStatistics) stat).isSumDefined());
		} else if (stat instanceof StringColumnStatistics) {
			statistics.put("min", ((StringColumnStatistics) stat).getMinimum());
			statistics.put("max", ((StringColumnStatistics) stat).getMaximum());
			statistics.put("sum", ((StringColumnStatistics) stat).getSum());
		}
	}
}
