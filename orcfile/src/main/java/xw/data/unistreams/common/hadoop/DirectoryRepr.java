package xw.data.unistreams.common.hadoop;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class DirectoryRepr
{
	public final String path;
	public final String[] entries;
	
	public DirectoryRepr(FileSystem fs, Path p)
			throws IOException
	{
		path = p.toUri().toString();
		
		final FileStatus[] statusList = fs.listStatus(p);
		entries = new String[statusList.length];
		for (int i=0; i<statusList.length; i++) {
			final FileStatus status = statusList[i];
			entries[i] = status.getPath().getName();
			if (status.isDirectory()) {
				entries[i] = entries[i] + "/";
			}
		}
	}
	
	@Override
	public String toString() 
	{
		return path;
	}
}
