package xw.data.unistreams.common.hive.objectinspector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.ql.io.orc.OrcSerde;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.lazy.LazyFactory;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe.SerDeParameters;
import org.apache.hadoop.hive.serde2.lazybinary.LazyBinaryFactory;
import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.MapObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructField;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorUtils;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;

import xw.data.unistreams.common.hadoop.HadoopCommon;
import xw.data.unistreams.common.lang.StringUtils;
import xw.data.unistreams.schema.parser.SchemaLexer;
import xw.data.unistreams.schema.parser.SchemaParser;

public class ObjectInspectorUtils
{
	static private Map<String, ObjectInspector> cachedInspectors; 
	static private Map<String, ObjectInspector> cachedColumnarInspectors;
	static private Map<String, ObjectInspector> cachedOrcInspectors;
    static private Map<String, PrimitiveObjectInspector> cachedPrimitiveInspectors;

	static private SerDeParameters defaultSerDeParameters;

	static {
		cachedInspectors = new HashMap<>();
		cachedColumnarInspectors = new HashMap<>();
		cachedOrcInspectors = new HashMap<>();
        cachedPrimitiveInspectors = new HashMap<>();
		defaultSerDeParameters = null;
	}
	
	public static SerDeParameters getDefaultSerDeParameters() {
		if (defaultSerDeParameters == null) {
			try {
				final Configuration conf = HadoopCommon.getConf();
				final Properties prop = new Properties();
				defaultSerDeParameters = LazySimpleSerDe.initSerdeParams(conf, prop, "");
			} catch (SerDeException e) {
				String msg = "cannot initialize serde parameters: " + e.getMessage();
				throw new RuntimeException(msg, e);
			}
		}
		
		return defaultSerDeParameters;
	}

    public static PrimitiveObjectInspector getPrimitiveInspector(String typename) {
        if (cachedPrimitiveInspectors.containsKey(typename)) {
            return cachedPrimitiveInspectors.get(typename);
        } else {
            final PrimitiveObjectInspector oi = PrimitiveObjectInspectorFactory.getPrimitiveJavaObjectInspector
                    (PrimitiveObjectInspectorUtils.getTypeEntryFromTypeName(typename).primitiveCategory);
            cachedPrimitiveInspectors.put(typename, oi);
            return oi;
        }
    }
	
	public static ObjectInspector getNestedObjectInspector(String schema) {
		if (cachedInspectors.containsKey(schema)) {
			return cachedInspectors.get(schema);
		}
		
		final ANTLRStringStream in = new ANTLRStringStream(schema);
		final SchemaLexer lexer = new SchemaLexer(in);
		final CommonTokenStream tokens = new CommonTokenStream(lexer);
		final SchemaParser parser = new SchemaParser(tokens);
		final ObjectInspector oi;
		try {
			oi = parser.schema().oi;
		}
		catch (RecognitionException e) {
			String msg = "schema syntax error: " + e.getMessage();
			throw new IllegalArgumentException(msg, e);
		}
		
		if (oi == null) {
			String msg = "error parsing schema: '" + schema + "'";
			throw new IllegalArgumentException(msg);
		}
		
		cachedInspectors.put(schema, oi);
		return oi;
	}
	
	public static ObjectInspector getOrcObjectInspector(String schema) {
		if (cachedOrcInspectors.containsKey(schema)) {
			return cachedOrcInspectors.get(schema);
		}
		
		final StructObjectInspector soi = 
				(StructObjectInspector) getNestedObjectInspector(schema);

		
		final List<String> columnNames = new ArrayList<>();
		final List<String> columnTypes = new ArrayList<>();
		
		for (StructField field: soi.getAllStructFieldRefs()) {
			columnNames.add(field.getFieldName());
			ObjectInspector oi = field.getFieldObjectInspector();
			columnTypes.add(oi.getTypeName());
		}
		
		final OrcSerde orcSerde = new OrcSerde();
		final Properties properties = new Properties();
		properties.put("columns", StringUtils.join(columnNames, ","));
		properties.put("columns.types", StringUtils.join(columnTypes, ","));
		orcSerde.initialize(HadoopCommon.getConf(), properties);

        try {
			return orcSerde.getObjectInspector();
		} catch (SerDeException e) {
			throw new RuntimeException("unexpected error in orcSerde.getObjectInspector: " + e.getMessage(), e);
		}
	}
		
	public static ObjectInspector getColumnarStructObjectInspector(String schema, ColumnarProperties cp) {
		if (cachedColumnarInspectors.containsKey(schema)) {
			return cachedColumnarInspectors.get(schema);
		}
		
		final StructObjectInspector soi = 
				(StructObjectInspector) getNestedObjectInspector(schema);
		
		final List<String> columnNames = new ArrayList<>();
		final List<TypeInfo> columnTypes = new ArrayList<>();
		// final List<ObjectInspector> inspectors = new ArrayList<>();
		
		for (StructField field: soi.getAllStructFieldRefs()) {
			columnNames.add(field.getFieldName());
			final ObjectInspector oi = field.getFieldObjectInspector();
			final TypeInfo typeInfo = TypeInfoUtils.getTypeInfoFromObjectInspector(oi);
			columnTypes.add(typeInfo);
			// inspectors.add(oi);
		}
		
		final ObjectInspector oi;
		try {
			if (cp.usingBinarySerDe()) {
				oi = LazyBinaryFactory.createColumnarStructInspector(columnNames, columnTypes);
			}
			else {
				final SerDeParameters serdeParams = cp.getSerDeParameters();
				oi = LazyFactory.createColumnarStructInspector(columnNames, columnTypes, 
									serdeParams.getSeparators(), serdeParams.getNullSequence(), 
									serdeParams.isEscaped(), serdeParams.getEscapeChar());
			}
		} catch (SerDeException e) {
			String msg = "cannot create columnar inspector: " + e.getMessage();
			throw new RuntimeException(msg, e);
		} 
		
		if (oi == null) {
			String msg = "created a null columnar inspector by schema: '" + schema + "'";
			throw new IllegalArgumentException(msg);
		}
		
		cachedColumnarInspectors.put(schema, oi);
		return oi;
	}

	public static String describeObjectInspector(ObjectInspector oi) {
		final String category = oi.getCategory().toString();
		final String className = oi.getClass().getSimpleName();
		final String typeName = oi.getTypeName();
		return category + "/" + className + "/" + typeName;
	}
	
	public static String describeDetailedObjectInspector(ObjectInspector oi) {
		if (oi instanceof StructObjectInspector) {
			final StringBuilder sb = new StringBuilder();
			final StructObjectInspector soi = (StructObjectInspector) oi;
			sb.append(soi.getClass().getSimpleName()).append("{");
			String delimiter = "";
			for (StructField field: soi.getAllStructFieldRefs()) {
				final String name = field.getFieldName();
				final ObjectInspector foi = field.getFieldObjectInspector();
				sb.append(delimiter).append(name).append(": ").append(describeDetailedObjectInspector(foi));
				delimiter = ", ";
			}
			sb.append("}");
			return sb.toString();
		}
		else if (oi instanceof MapObjectInspector) {
			final MapObjectInspector moi = (MapObjectInspector) oi;
			return moi.getClass().getSimpleName() + "<"
					+ moi.getMapKeyObjectInspector().getClass().getSimpleName() + ", "
					+ moi.getMapValueObjectInspector().getClass().getSimpleName() + ">";
		}
		else if (oi instanceof ListObjectInspector) {
			final ListObjectInspector loi = (ListObjectInspector) oi;
			return loi.getClass().getSimpleName() + "<" 
					+ loi.getListElementObjectInspector().getClass().getSimpleName() + ">";
		}
		else if (oi instanceof PrimitiveObjectInspector) {
			return oi.getClass().getSimpleName();
		}
		else {
			throw new IllegalArgumentException("unknown object inspector: "
					+ oi.getClass().getSimpleName());
		}
	}
	
	public static Object inspectObject(final Object o, final ObjectInspector oi) {
		if (oi == null) {
			String msg = "null object inspector";
			throw new IllegalArgumentException(msg);
		}
		else if (oi instanceof PrimitiveObjectInspector) {
			// to turn Writable object to Java primitive type
			final PrimitiveObjectInspector poi = (PrimitiveObjectInspector) oi;
			return poi.getPrimitiveJavaObject(o);
		}
		else if (oi instanceof MapObjectInspector) {
			final MapObjectInspector moi = (MapObjectInspector) oi;
			final ObjectInspector koi = moi.getMapKeyObjectInspector();
			final ObjectInspector voi = moi.getMapValueObjectInspector();
			
			final Map<Object, Object> data = new LinkedHashMap<Object, Object>();
			for (Map.Entry<?, ?> entry: moi.getMap(o).entrySet()) {
				final Object key = entry.getKey();
				final Object value = entry.getValue();
				data.put(inspectObject(key, koi), inspectObject(value, voi));
			}
			return data;
		}
		else if (oi instanceof ListObjectInspector) {
			final ListObjectInspector loi = (ListObjectInspector) oi;
			final ObjectInspector eoi = loi.getListElementObjectInspector();
			
			final List<Object> data = new ArrayList<>();
			for (Object element: loi.getList(o)) {
				data.add(inspectObject(element, eoi));
			}
			return data;
		}
		else {
			final String msg = "unknown object inspector type: " + oi.getClass().getName();
			throw new IllegalArgumentException(msg);
		}
	}


}	
