package xw.data.unistreams.common.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 4/2/16.
 */
public class OptParser
{
    static class Opt {
        public final String prefix;
        public final Object deflt;
        public final boolean isFlag;
        public final boolean mandatory;
        public Object value;

        public Opt(String prefix, Object deflt, boolean mandatory) {
            this.prefix = prefix;
            this.deflt = deflt;
            this.isFlag = prefix.endsWith("=");
            this.mandatory = mandatory;
            this.value = isFlag? false: null;
        }

        public Object getValue() {
            if (value != null) {
                return value;
            } else if (mandatory) {
                throw new IllegalArgumentException("missing argument: " + prefix);
            } else {
                return deflt;
            }
        }
    }

    private final Map<String, Opt> options = new LinkedHashMap<>();
    private final List<String> arguments = new ArrayList<>();

    public static OptParser create() {
        return new OptParser();
    }

    public OptParser set(String prefix, Object defaultValue, boolean mandatory) {
        if (! prefix.startsWith("-")) {
            throw new IllegalArgumentException("option name must starts with '-'");
        } else if (options.containsKey(prefix)) {
            throw new IllegalArgumentException("redefined option: " + prefix);
        } else {
            options.put(prefix, new Opt(prefix, defaultValue, mandatory));
        }
        return this;
    }

    public OptParser set(String prefix, Object defaultValue) {
        return set(prefix, defaultValue, false);
    }

    private boolean setMatchingOption(String arg) {
        for (Map.Entry<String, Opt> entry: options.entrySet()) {
            final String prefix = entry.getKey();
            final Opt opt = entry.getValue();
            if (opt.isFlag && arg.equals(prefix)) {
                opt.value = true;
                return true;
            } else if (!opt.isFlag && arg.startsWith(prefix)) {
                opt.value = arg.substring(prefix.length());
                return true;
            }
        }
        return false;
    }

    public void parse(String[] args) {
        for (String arg: args) {
            if (! arg.startsWith("-")) {
                arguments.add(arg);
            } else {
                if (! setMatchingOption(arg)) {
                    throw new IllegalArgumentException("unknown argument: " + arg);
                }
            }
        }
    }

    public int getArgumentSize() {
        return arguments.size();
    }

    public String getArgument(int i) {
        return arguments.get(i);
    }

    public Object getOption(String prefix) {
        return options.get(prefix);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (Map.Entry<String, Opt> entry: options.entrySet()) {
            final Opt opt = entry.getValue();
            if (opt.isFlag && opt.value.equals(true)) {
                sb.append(delimiter).append(opt.prefix);
            } else if (! opt.isFlag && opt.value != null) {
                sb.append(delimiter).append(opt.prefix).append(opt.value);
            }
            delimiter = " ";
        }
        return sb.toString();
    }

}
