package xw.data.unistreams.common.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HadoopCommon
{
    private static final Logger log = LoggerFactory.getLogger(HadoopCommon.class);

	static private Configuration _conf = null;
	static final public String HADOOP_CONF_PRIFIX = "HADOOP_CONF.";
	
	static public Configuration getConf() {
		if (_conf == null) {
			_conf = new Configuration();
			final Properties props = System.getProperties();
			for (String prop: props.stringPropertyNames()) {
				if (!prop.startsWith(HADOOP_CONF_PRIFIX)) 
					continue;
				final String key = prop.substring(HADOOP_CONF_PRIFIX.length());
				final String value = props.getProperty(prop);
				_conf.set(key, value);
                log.info("set hadoop configuration: {}={}", key, value);
			}
		}
		return _conf;
	}
	
	static public Object getFileChecksum(URI uri) throws IOException {
		final FileSystem fs = FileSystem.get(uri, getConf());
		final Path path = new Path(uri.getPath());
		return fs.getFileChecksum(path);
	}
	
	static public boolean delete(URI uri) throws IOException {
		final FileSystem fs = FileSystem.get(uri, getConf());
		final Path path = new Path(uri.getPath());
		return fs.delete(path, true);	// delete recursively
	}
	
	static public FileSystem getFileSystem(URI uri) throws IOException {
		return FileSystem.get(uri, getConf());
	}
	
	static public List<String> listFiles(URI uri) throws IOException {
		final FileSystem fs = FileSystem.get(uri, getConf());
		final Path path = new Path(uri.getPath());
		final FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		final FileStatus[] statuses = fs.listStatus(path);
		final List<String> files = new ArrayList<>();
		for (FileStatus s: statuses) {
			if (!s.isDirectory()) {
				files.add(s.getPath().getName());
			}
		}
		return files;
	}
	
	static private void addFilesRecursively(FileSystem fs, String base, String subdir,
											List<String> files) throws IOException {
		final Path path = new Path(base + subdir);
		final FileStatus status = fs.getFileStatus(path);
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + (base + subdir));
		}
		
		final FileStatus[] statuses = fs.listStatus(path);
		for (FileStatus s: statuses) {
			final String segName = s.getPath().getName();
			if (s.isDirectory()) {
				addFilesRecursively(fs, base, subdir + segName + "/", files);
			}
			else {
				files.add(subdir + segName);
			}
		}
	}
	
	static public List<String> listFilesRecursively(URI uri) throws IOException {
		final FileSystem fs = FileSystem.get(uri, getConf());
		final Path path = new Path(uri.getPath());
		final FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		final List<String> files = new ArrayList<>();
		addFilesRecursively(fs, uri.getPath(), "", files);
		return files;
	}
	
	static public List<String> listEntries(URI uri) throws IOException {
		final FileSystem fs = FileSystem.get(uri, getConf());
		final Path path = new Path(uri.getPath());
		final FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		final FileStatus[] statuses = fs.listStatus(path);
		final List<String> entries = new ArrayList<>();
		for (FileStatus s: statuses) {
			if (s.isDirectory()) {
				entries.add(s.getPath().getName() + "/");
			}
			else {
				entries.add(s.getPath().getName());
			}
		}
		return entries;
	}

	static public DirectoryRepr getDirectoryRepr(URI uri) throws IOException {
		final FileSystem fs = getFileSystem(uri);
		final Path path = new Path(uri.getPath());
		return new DirectoryRepr(fs, path);
	}

	static public FileStatusRepr getFileStatusRepr(URI uri) throws IOException {
		final FileSystem fs = getFileSystem(uri);
		final Path path = new Path(uri.getPath());
		return new FileStatusRepr(fs, path);
	}
}
