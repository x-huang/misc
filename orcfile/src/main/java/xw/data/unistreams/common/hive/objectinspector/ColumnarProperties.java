package xw.data.unistreams.common.hive.objectinspector;

import java.util.Properties;

import org.apache.hadoop.hive.serde.serdeConstants;
import org.apache.hadoop.hive.serde2.SerDeException;
import org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe;
import org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe.SerDeParameters;

import xw.data.unistreams.common.hadoop.HadoopCommon;

public class ColumnarProperties
{
	private int hiveVersion = 12;
	
	final private Properties tbl = new Properties();
	private SerDeParameters serDeParams = null;
	private boolean valid = false;
	
	private ColumnarProperties() {}
	
	public int getHiveVersion() {
		return hiveVersion;
	}
	
	public ColumnarProperties setHiveVersionForRCFileSerDe(int ver) {
		hiveVersion = ver;
		return this;
	}
	
	public boolean usingBinarySerDe() {
		// because: [HIVE-4475] - Switch RCFile default to LazyBinaryColumnarSerDe
		return hiveVersion >= 12;
	}
	
	public ColumnarProperties setItemSeparator(String sep) {
		if (sep != null) {
			tbl.put(serdeConstants.COLLECTION_DELIM, sep);
			valid = false;
		}
		return this;
	}
	
	public ColumnarProperties setKeyValueSeparator(String sep) {
		if (sep != null) {
			tbl.put(serdeConstants.MAPKEY_DELIM, sep);
			valid = false;
		}
		return this;
	}
	
	@Override
	public String toString() {
		String serde = usingBinarySerDe()? "LazyBinaryColumnarSerDe": "LazySimpleSerDe";
		String descr = "using-serde:" + serde;
		
		if (! usingBinarySerDe()) {
			Object itemSeparator = tbl.getProperty(serdeConstants.COLLECTION_DELIM, "<default>");
			Object keyValueSeparator = tbl.getProperty(serdeConstants.MAPKEY_DELIM, "<default>");
			descr += ",item-separator:'" + itemSeparator + "',key-value-separator:'" + keyValueSeparator;
		}
		return descr;
	}
	
	public Properties getProperties() {
		return tbl;
	}
	
	public SerDeParameters getSerDeParameters() throws SerDeException {
		if (serDeParams == null || !valid) {
			serDeParams = LazySimpleSerDe.initSerdeParams(HadoopCommon.getConf(),
					tbl, ColumnarSerDe.class.getName());
			valid = true;
		}
		return serDeParams;
	}
	
	public static ColumnarProperties getDefault() {
		return new ColumnarProperties();
	}
}
