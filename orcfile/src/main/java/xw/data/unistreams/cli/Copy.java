package xw.data.unistreams.cli;

import xw.data.unistreams.common.lang.StringUtils;
import xw.data.unistreams.core.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 5/26/16.
 */
public class Copy
{
    static class Option {
        String format = "orc";
        long limit = -1;
        String nullRepr = "\\N";
        List<URI> uris = new ArrayList<>();
        URI dest = null;
        String schema = null;

        Option(String[] args) {
            for (String arg: args) {
                if (arg.startsWith("-format=")) {
                    format = arg.substring("-format=".length());
                    if (!format.equals("orc") /* && !opt.format.equals("rcfile") */) {
                        throw new IllegalArgumentException("unknown format: " + format);
                    }
                } else if (arg.startsWith("-limit=")) {
                    limit = Long.parseLong(arg.substring("-limit=".length()));
                } else if (arg.startsWith("-schema=")) {
                    schema =  arg.substring("-schema=".length());
                }  else if (arg.startsWith("-null-repr=")) {
                    nullRepr = arg.substring("-null-repr=".length());
                } else if (!arg.startsWith("-")) {
                    uris.add(URI.create(arg));
                }
            }
            if (schema == null) {
                throw new IllegalArgumentException("missing schema");
            }
            if (uris.size() < 2) {
                throw new IllegalArgumentException("expecting at least two uri");
            }
            dest = uris.get(uris.size() -1);
            uris.remove(uris.size() -1);
        }
    }

    public static void main(String[] args) throws Exception {
        final Option opt = new Option(args);

        final RecordInputStream in = Resources.newFileOperator(opt.format)
                .addUris(opt.uris).setLimit(opt.limit).openInputStream();

        final RecordOutputStream out = Resources.newFileOperator(opt.format)
                .addUri(opt.dest).setSchema(opt.schema).openOutputStream();

        DataStreams.connect(in, out);
        System.err.println("# written " + out.getCount() + " records to " + opt.dest);
    }
}
