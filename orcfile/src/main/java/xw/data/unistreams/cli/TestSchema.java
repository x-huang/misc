package xw.data.unistreams.cli;

import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils;

public class TestSchema
{
	public static void main(String[] args) throws Exception {

		if (args.length != 1) {
			System.err.println("[usage] java ... <schema-string>");
			return;
		}
		
		ObjectInspector oi = ObjectInspectorUtils.getNestedObjectInspector(args[0]);
		System.out.println("object inspector class name => " + oi.getClass().getName());
		System.out.println("object inspector  type name => " + oi.getTypeName());
	}

}

/*

$ java xw.data.common.objectinspector.TestSchema 'struct<s:string,num:int,pairs:map<string,string>>'
object inspector class name => org.apache.hadoop.hive.serde2.objectinspector.StandardStructObjectInspector
object inspector  type name => struct<s:string,num:int,pairs:map<string,string>>

 */