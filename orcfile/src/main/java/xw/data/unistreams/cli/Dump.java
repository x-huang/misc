package xw.data.unistreams.cli;

import xw.data.unistreams.serde.RecordTextFormatter;
import xw.data.unistreams.streams.console.StdoutOutputStream;
import xw.data.unistreams.core.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 5/24/16.
 */
public class Dump
{
    static class Option {
        String format = "orc";
        long limit = -1;
        String nullRepr = "\\N";
        List<URI> uris = new ArrayList<>();

        public static Option parseArgs(String[] args) {
            final Option opt = new Option();
            for (String arg: args) {
                if (arg.startsWith("-format=")) {
                    opt.format = arg.substring("-format=".length());
                    if (!opt.format.equals("orc") /* && !opt.format.equals("rcfile") */) {
                        throw new IllegalArgumentException("unknown format: " + opt.format);
                    }
                } else if (arg.startsWith("-limit=")) {
                    opt.limit = Long.parseLong(arg.substring("-limit=".length()));
                } else if (arg.startsWith("-null-repr=")) {
                    opt.nullRepr = arg.substring("-null-repr=".length());
                } else if (!arg.startsWith("-")) {
                    opt.uris.add(URI.create(arg));
                }
            }
            if (opt.uris.size() == 0) {
                throw new IllegalArgumentException("expecting at least one uri");
            }
            return opt;
        }
    }

    public static void main(String[] args) throws Exception {

        final Option opt = Option.parseArgs(args);
        if (! opt.format.equals("orc")) {
            throw new UnsupportedClassVersionError("reading from other format not supported yet");
        }

        final RecordInputStream in = Resources.newFileOperator(opt.format)
                .addUris(opt.uris).setLimit(opt.limit).openInputStream();

        final StdoutOutputStream stdout = StdoutOutputStream.create();
        ((RecordTextFormatter) stdout.getRecordFormatter()).setNullRepr(opt.nullRepr);

        DataStreams.connect(in, stdout);
        System.err.println("# read " + in.getCount() + " records");

    }
}
