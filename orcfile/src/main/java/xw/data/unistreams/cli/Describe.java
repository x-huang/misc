package xw.data.unistreams.cli;

import xw.data.unistreams.streams.orc.repr.OrcFilePropertyRepr;

import java.net.URI;

/**
 * Created by xhuang on 5/26/16.
 */
public class Describe
{
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            throw new IllegalArgumentException("expecting exactly one URI");
        }

        final URI uri = URI.create(args[0]);
        final Object repr = OrcFilePropertyRepr.fromURI(uri);
        System.out.println(repr);
    }
}
