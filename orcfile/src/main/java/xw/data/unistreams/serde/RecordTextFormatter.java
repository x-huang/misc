package xw.data.unistreams.serde;

/**
 * Created by xhuang on 5/26/16.
 */
public class RecordTextFormatter implements RecordFormatter
{
    private String fieldDelimiter = "\t";
    private String nullString = "\\N";
    private transient final StringBuilder sb = new StringBuilder();

    public RecordTextFormatter setFieldDelimiter(String delimiter)  {
        this.fieldDelimiter = delimiter;
        return this;
    }

    public RecordTextFormatter setNullRepr(String repr) {
        this.nullString = repr;
        return this;
    }

    @Override
    public String format(Object[] record) {
        sb.setLength(0);
        String delimiter = "";
        for (Object datum : record) {
            sb.append(delimiter).append(datum == null ? nullString : datum.toString());
            delimiter = fieldDelimiter;
        }
        return sb.toString();
    }
}
