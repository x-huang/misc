package xw.data.unistreams.serde;

/**
 * Created by xhuang on 5/26/16.
 */
public interface RecordFormatter
{
    String format(Object[] record);
}
