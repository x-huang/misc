// $ANTLR 3.5.2 src/main/java/xw/data/unistreams/schema/parser/Schema.g 2016-05-26 15:33:02

package xw.data.unistreams.schema.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class SchemaLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__5=5;
	public static final int T__6=6;
	public static final int T__7=7;
	public static final int T__8=8;
	public static final int T__9=9;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int T__12=12;
	public static final int T__13=13;
	public static final int T__14=14;
	public static final int T__15=15;
	public static final int T__16=16;
	public static final int T__17=17;
	public static final int T__18=18;
	public static final int T__19=19;
	public static final int T__20=20;
	public static final int T__21=21;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int T__24=24;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int ID=4;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public SchemaLexer() {} 
	public SchemaLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public SchemaLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "src/main/java/xw/data/unistreams/schema/parser/Schema.g"; }

	// $ANTLR start "T__5"
	public final void mT__5() throws RecognitionException {
		try {
			int _type = T__5;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:11:6: ( ',' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:11:8: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__5"

	// $ANTLR start "T__6"
	public final void mT__6() throws RecognitionException {
		try {
			int _type = T__6;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:12:6: ( ':' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:12:8: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__6"

	// $ANTLR start "T__7"
	public final void mT__7() throws RecognitionException {
		try {
			int _type = T__7;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:13:6: ( '<' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:13:8: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__7"

	// $ANTLR start "T__8"
	public final void mT__8() throws RecognitionException {
		try {
			int _type = T__8;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:14:6: ( '>' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:14:8: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__8"

	// $ANTLR start "T__9"
	public final void mT__9() throws RecognitionException {
		try {
			int _type = T__9;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:15:6: ( 'array' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:15:8: 'array'
			{
			match("array"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__9"

	// $ANTLR start "T__10"
	public final void mT__10() throws RecognitionException {
		try {
			int _type = T__10;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:16:7: ( 'bigint' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:16:9: 'bigint'
			{
			match("bigint"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__10"

	// $ANTLR start "T__11"
	public final void mT__11() throws RecognitionException {
		try {
			int _type = T__11;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:17:7: ( 'binary' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:17:9: 'binary'
			{
			match("binary"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__11"

	// $ANTLR start "T__12"
	public final void mT__12() throws RecognitionException {
		try {
			int _type = T__12;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:18:7: ( 'boolean' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:18:9: 'boolean'
			{
			match("boolean"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__12"

	// $ANTLR start "T__13"
	public final void mT__13() throws RecognitionException {
		try {
			int _type = T__13;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:19:7: ( 'char' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:19:9: 'char'
			{
			match("char"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__13"

	// $ANTLR start "T__14"
	public final void mT__14() throws RecognitionException {
		try {
			int _type = T__14;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:20:7: ( 'date' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:20:9: 'date'
			{
			match("date"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__14"

	// $ANTLR start "T__15"
	public final void mT__15() throws RecognitionException {
		try {
			int _type = T__15;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:21:7: ( 'datetime' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:21:9: 'datetime'
			{
			match("datetime"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__15"

	// $ANTLR start "T__16"
	public final void mT__16() throws RecognitionException {
		try {
			int _type = T__16;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:22:7: ( 'decimal' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:22:9: 'decimal'
			{
			match("decimal"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__16"

	// $ANTLR start "T__17"
	public final void mT__17() throws RecognitionException {
		try {
			int _type = T__17;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:23:7: ( 'double' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:23:9: 'double'
			{
			match("double"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__17"

	// $ANTLR start "T__18"
	public final void mT__18() throws RecognitionException {
		try {
			int _type = T__18;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:24:7: ( 'float' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:24:9: 'float'
			{
			match("float"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__18"

	// $ANTLR start "T__19"
	public final void mT__19() throws RecognitionException {
		try {
			int _type = T__19;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:25:7: ( 'int' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:25:9: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__19"

	// $ANTLR start "T__20"
	public final void mT__20() throws RecognitionException {
		try {
			int _type = T__20;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:26:7: ( 'map' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:26:9: 'map'
			{
			match("map"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__20"

	// $ANTLR start "T__21"
	public final void mT__21() throws RecognitionException {
		try {
			int _type = T__21;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:27:7: ( 'smallint' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:27:9: 'smallint'
			{
			match("smallint"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__21"

	// $ANTLR start "T__22"
	public final void mT__22() throws RecognitionException {
		try {
			int _type = T__22;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:28:7: ( 'string' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:28:9: 'string'
			{
			match("string"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__22"

	// $ANTLR start "T__23"
	public final void mT__23() throws RecognitionException {
		try {
			int _type = T__23;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:29:7: ( 'struct' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:29:9: 'struct'
			{
			match("struct"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__23"

	// $ANTLR start "T__24"
	public final void mT__24() throws RecognitionException {
		try {
			int _type = T__24;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:30:7: ( 'timestamp' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:30:9: 'timestamp'
			{
			match("timestamp"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__24"

	// $ANTLR start "T__25"
	public final void mT__25() throws RecognitionException {
		try {
			int _type = T__25;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:31:7: ( 'tinyint' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:31:9: 'tinyint'
			{
			match("tinyint"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__25"

	// $ANTLR start "T__26"
	public final void mT__26() throws RecognitionException {
		try {
			int _type = T__26;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:32:7: ( 'varchar' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:32:9: 'varchar'
			{
			match("varchar"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__26"

	// $ANTLR start "T__27"
	public final void mT__27() throws RecognitionException {
		try {
			int _type = T__27;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:33:7: ( 'void' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:33:9: 'void'
			{
			match("void"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__27"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:93:6: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:93:8: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:93:38: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	@Override
	public void mTokens() throws RecognitionException {
		// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:8: ( T__5 | T__6 | T__7 | T__8 | T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | ID )
		int alt2=24;
		alt2 = dfa2.predict(input);
		switch (alt2) {
			case 1 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:10: T__5
				{
				mT__5(); 

				}
				break;
			case 2 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:15: T__6
				{
				mT__6(); 

				}
				break;
			case 3 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:20: T__7
				{
				mT__7(); 

				}
				break;
			case 4 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:25: T__8
				{
				mT__8(); 

				}
				break;
			case 5 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:30: T__9
				{
				mT__9(); 

				}
				break;
			case 6 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:35: T__10
				{
				mT__10(); 

				}
				break;
			case 7 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:41: T__11
				{
				mT__11(); 

				}
				break;
			case 8 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:47: T__12
				{
				mT__12(); 

				}
				break;
			case 9 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:53: T__13
				{
				mT__13(); 

				}
				break;
			case 10 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:59: T__14
				{
				mT__14(); 

				}
				break;
			case 11 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:65: T__15
				{
				mT__15(); 

				}
				break;
			case 12 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:71: T__16
				{
				mT__16(); 

				}
				break;
			case 13 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:77: T__17
				{
				mT__17(); 

				}
				break;
			case 14 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:83: T__18
				{
				mT__18(); 

				}
				break;
			case 15 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:89: T__19
				{
				mT__19(); 

				}
				break;
			case 16 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:95: T__20
				{
				mT__20(); 

				}
				break;
			case 17 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:101: T__21
				{
				mT__21(); 

				}
				break;
			case 18 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:107: T__22
				{
				mT__22(); 

				}
				break;
			case 19 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:113: T__23
				{
				mT__23(); 

				}
				break;
			case 20 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:119: T__24
				{
				mT__24(); 

				}
				break;
			case 21 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:125: T__25
				{
				mT__25(); 

				}
				break;
			case 22 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:131: T__26
				{
				mT__26(); 

				}
				break;
			case 23 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:137: T__27
				{
				mT__27(); 

				}
				break;
			case 24 :
				// src/main/java/xw/data/unistreams/schema/parser/Schema.g:1:143: ID
				{
				mID(); 

				}
				break;

		}
	}


	protected DFA2 dfa2 = new DFA2(this);
	static final String DFA2_eotS =
		"\5\uffff\12\17\1\uffff\30\17\1\71\1\72\12\17\1\106\1\110\3\17\2\uffff"+
		"\6\17\1\122\1\123\3\17\1\uffff\1\17\1\uffff\2\17\1\132\6\17\2\uffff\1"+
		"\141\1\142\3\17\1\146\1\uffff\1\17\1\150\1\151\3\17\2\uffff\1\155\1\17"+
		"\1\157\1\uffff\1\17\2\uffff\1\17\1\162\1\163\1\uffff\1\164\1\uffff\1\165"+
		"\1\17\4\uffff\1\167\1\uffff";
	static final String DFA2_eofS =
		"\170\uffff";
	static final String DFA2_minS =
		"\1\54\4\uffff\1\162\1\151\1\150\1\141\1\154\1\156\1\141\1\155\1\151\1"+
		"\141\1\uffff\1\162\1\147\1\157\1\141\1\164\1\143\1\165\1\157\1\164\1\160"+
		"\1\141\1\162\1\155\1\162\1\151\1\141\1\151\1\141\1\154\1\162\1\145\1\151"+
		"\1\142\1\141\2\60\1\154\1\151\1\145\1\171\1\143\1\144\1\171\1\156\1\162"+
		"\1\145\2\60\1\155\1\154\1\164\2\uffff\1\154\1\156\1\143\1\163\1\151\1"+
		"\150\2\60\1\164\1\171\1\141\1\uffff\1\151\1\uffff\1\141\1\145\1\60\1\151"+
		"\1\147\2\164\1\156\1\141\2\uffff\2\60\1\156\1\155\1\154\1\60\1\uffff\1"+
		"\156\2\60\1\141\1\164\1\162\2\uffff\1\60\1\145\1\60\1\uffff\1\164\2\uffff"+
		"\1\155\2\60\1\uffff\1\60\1\uffff\1\60\1\160\4\uffff\1\60\1\uffff";
	static final String DFA2_maxS =
		"\1\172\4\uffff\1\162\1\157\1\150\1\157\1\154\1\156\1\141\1\164\1\151\1"+
		"\157\1\uffff\1\162\1\156\1\157\1\141\1\164\1\143\1\165\1\157\1\164\1\160"+
		"\1\141\1\162\1\156\1\162\1\151\1\141\1\151\1\141\1\154\1\162\1\145\1\151"+
		"\1\142\1\141\2\172\1\154\1\165\1\145\1\171\1\143\1\144\1\171\1\156\1\162"+
		"\1\145\2\172\1\155\1\154\1\164\2\uffff\1\154\1\156\1\143\1\163\1\151\1"+
		"\150\2\172\1\164\1\171\1\141\1\uffff\1\151\1\uffff\1\141\1\145\1\172\1"+
		"\151\1\147\2\164\1\156\1\141\2\uffff\2\172\1\156\1\155\1\154\1\172\1\uffff"+
		"\1\156\2\172\1\141\1\164\1\162\2\uffff\1\172\1\145\1\172\1\uffff\1\164"+
		"\2\uffff\1\155\2\172\1\uffff\1\172\1\uffff\1\172\1\160\4\uffff\1\172\1"+
		"\uffff";
	static final String DFA2_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\12\uffff\1\30\51\uffff\1\17\1\20\13\uffff\1\11"+
		"\1\uffff\1\12\11\uffff\1\27\1\5\6\uffff\1\16\6\uffff\1\6\1\7\3\uffff\1"+
		"\15\1\uffff\1\22\1\23\3\uffff\1\10\1\uffff\1\14\2\uffff\1\25\1\26\1\13"+
		"\1\21\1\uffff\1\24";
	static final String DFA2_specialS =
		"\170\uffff}>";
	static final String[] DFA2_transitionS = {
			"\1\1\15\uffff\1\2\1\uffff\1\3\1\uffff\1\4\2\uffff\32\17\4\uffff\1\17"+
			"\1\uffff\1\5\1\6\1\7\1\10\1\17\1\11\2\17\1\12\3\17\1\13\5\17\1\14\1\15"+
			"\1\17\1\16\4\17",
			"",
			"",
			"",
			"",
			"\1\20",
			"\1\21\5\uffff\1\22",
			"\1\23",
			"\1\24\3\uffff\1\25\11\uffff\1\26",
			"\1\27",
			"\1\30",
			"\1\31",
			"\1\32\6\uffff\1\33",
			"\1\34",
			"\1\35\15\uffff\1\36",
			"",
			"\1\37",
			"\1\40\6\uffff\1\41",
			"\1\42",
			"\1\43",
			"\1\44",
			"\1\45",
			"\1\46",
			"\1\47",
			"\1\50",
			"\1\51",
			"\1\52",
			"\1\53",
			"\1\54\1\55",
			"\1\56",
			"\1\57",
			"\1\60",
			"\1\61",
			"\1\62",
			"\1\63",
			"\1\64",
			"\1\65",
			"\1\66",
			"\1\67",
			"\1\70",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\73",
			"\1\74\13\uffff\1\75",
			"\1\76",
			"\1\77",
			"\1\100",
			"\1\101",
			"\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\23\17\1\107\6\17",
			"\1\111",
			"\1\112",
			"\1\113",
			"",
			"",
			"\1\114",
			"\1\115",
			"\1\116",
			"\1\117",
			"\1\120",
			"\1\121",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\124",
			"\1\125",
			"\1\126",
			"",
			"\1\127",
			"",
			"\1\130",
			"\1\131",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\133",
			"\1\134",
			"\1\135",
			"\1\136",
			"\1\137",
			"\1\140",
			"",
			"",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\143",
			"\1\144",
			"\1\145",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"",
			"\1\147",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\152",
			"\1\153",
			"\1\154",
			"",
			"",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\156",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"",
			"\1\160",
			"",
			"",
			"\1\161",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			"\1\166",
			"",
			"",
			"",
			"",
			"\12\17\7\uffff\32\17\4\uffff\1\17\1\uffff\32\17",
			""
	};

	static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
	static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
	static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
	static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
	static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
	static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
	static final short[][] DFA2_transition;

	static {
		int numStates = DFA2_transitionS.length;
		DFA2_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
		}
	}

	protected class DFA2 extends DFA {

		public DFA2(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 2;
			this.eot = DFA2_eot;
			this.eof = DFA2_eof;
			this.min = DFA2_min;
			this.max = DFA2_max;
			this.accept = DFA2_accept;
			this.special = DFA2_special;
			this.transition = DFA2_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__5 | T__6 | T__7 | T__8 | T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | ID );";
		}
	}

}
