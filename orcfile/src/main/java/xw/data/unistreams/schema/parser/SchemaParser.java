// $ANTLR 3.5.2 src/main/java/xw/data/unistreams/schema/parser/Schema.g 2016-05-26 15:33:01

package xw.data.unistreams.schema.parser;

import java.util.LinkedList;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import static org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.getStandardMapObjectInspector;
import static org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.getStandardStructObjectInspector;
import static org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.getStandardListObjectInspector;
import static xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils.getPrimitiveInspector;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class SchemaParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ID", "','", "':'", "'<'", "'>'", 
		"'array'", "'bigint'", "'binary'", "'boolean'", "'char'", "'date'", "'datetime'", 
		"'decimal'", "'double'", "'float'", "'int'", "'map'", "'smallint'", "'string'", 
		"'struct'", "'timestamp'", "'tinyint'", "'varchar'", "'void'"
	};
	public static final int EOF=-1;
	public static final int T__5=5;
	public static final int T__6=6;
	public static final int T__7=7;
	public static final int T__8=8;
	public static final int T__9=9;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int T__12=12;
	public static final int T__13=13;
	public static final int T__14=14;
	public static final int T__15=15;
	public static final int T__16=16;
	public static final int T__17=17;
	public static final int T__18=18;
	public static final int T__19=19;
	public static final int T__20=20;
	public static final int T__21=21;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int T__24=24;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int ID=4;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public SchemaParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public SchemaParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return SchemaParser.tokenNames; }
	@Override public String getGrammarFileName() { return "src/main/java/xw/data/unistreams/schema/parser/Schema.g"; }


	public static class schema_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "schema"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:38:1: schema returns [ ObjectInspector oi ] : ty= type EOF !;
	public final SchemaParser.schema_return schema() throws RecognitionException {
		SchemaParser.schema_return retval = new SchemaParser.schema_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EOF1=null;
		ParserRuleReturnScope ty =null;

		Object EOF1_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:39:2: (ty= type EOF !)
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:39:4: ty= type EOF !
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_type_in_schema86);
			ty=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, ty.getTree());

			EOF1=(Token)match(input,EOF,FOLLOW_EOF_in_schema88); if (state.failed) return retval;
			if ( state.backtracking==0 ) { retval.oi = (ty!=null?((SchemaParser.type_return)ty).oi:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "schema"


	public static class type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "type"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:42:1: type returns [ ObjectInspector oi ] : (pt= primitive_type |at= array_type |mt= map_type |st= struct_type );
	public final SchemaParser.type_return type() throws RecognitionException {
		SchemaParser.type_return retval = new SchemaParser.type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope pt =null;
		ParserRuleReturnScope at =null;
		ParserRuleReturnScope mt =null;
		ParserRuleReturnScope st =null;


		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:43:3: (pt= primitive_type |at= array_type |mt= map_type |st= struct_type )
			int alt1=4;
			switch ( input.LA(1) ) {
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 21:
			case 22:
			case 24:
			case 25:
			case 26:
			case 27:
				{
				alt1=1;
				}
				break;
			case 9:
				{
				alt1=2;
				}
				break;
			case 20:
				{
				alt1=3;
				}
				break;
			case 23:
				{
				alt1=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}
			switch (alt1) {
				case 1 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:43:5: pt= primitive_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_primitive_type_in_type112);
					pt=primitive_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, pt.getTree());

					if ( state.backtracking==0 ) { retval.oi = (pt!=null?((SchemaParser.primitive_type_return)pt).oi:null); }
					}
					break;
				case 2 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:44:5: at= array_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_array_type_in_type125);
					at=array_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, at.getTree());

					if ( state.backtracking==0 ) { retval.oi = (at!=null?((SchemaParser.array_type_return)at).oi:null); }
					}
					break;
				case 3 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:45:5: mt= map_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_map_type_in_type137);
					mt=map_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, mt.getTree());

					if ( state.backtracking==0 ) { retval.oi = (mt!=null?((SchemaParser.map_type_return)mt).oi:null); }
					}
					break;
				case 4 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:46:5: st= struct_type
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_struct_type_in_type150);
					st=struct_type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, st.getTree());

					if ( state.backtracking==0 ) { retval.oi = (st!=null?((SchemaParser.struct_type_return)st).oi:null);  }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "type"


	public static class primitive_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "primitive_type"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:49:1: primitive_type returns [ ObjectInspector oi ] : ( 'void' | 'boolean' | 'tinyint' | 'smallint' | 'int' | 'bigint' | 'float' | 'double' | 'string' | 'varchar' | 'char' | 'date' | 'datetime' | 'timestamp' | 'decimal' | 'binary' );
	public final SchemaParser.primitive_type_return primitive_type() throws RecognitionException {
		SchemaParser.primitive_type_return retval = new SchemaParser.primitive_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal2=null;
		Token string_literal3=null;
		Token string_literal4=null;
		Token string_literal5=null;
		Token string_literal6=null;
		Token string_literal7=null;
		Token string_literal8=null;
		Token string_literal9=null;
		Token string_literal10=null;
		Token string_literal11=null;
		Token string_literal12=null;
		Token string_literal13=null;
		Token string_literal14=null;
		Token string_literal15=null;
		Token string_literal16=null;
		Token string_literal17=null;

		Object string_literal2_tree=null;
		Object string_literal3_tree=null;
		Object string_literal4_tree=null;
		Object string_literal5_tree=null;
		Object string_literal6_tree=null;
		Object string_literal7_tree=null;
		Object string_literal8_tree=null;
		Object string_literal9_tree=null;
		Object string_literal10_tree=null;
		Object string_literal11_tree=null;
		Object string_literal12_tree=null;
		Object string_literal13_tree=null;
		Object string_literal14_tree=null;
		Object string_literal15_tree=null;
		Object string_literal16_tree=null;
		Object string_literal17_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:50:2: ( 'void' | 'boolean' | 'tinyint' | 'smallint' | 'int' | 'bigint' | 'float' | 'double' | 'string' | 'varchar' | 'char' | 'date' | 'datetime' | 'timestamp' | 'decimal' | 'binary' )
			int alt2=16;
			switch ( input.LA(1) ) {
			case 27:
				{
				alt2=1;
				}
				break;
			case 12:
				{
				alt2=2;
				}
				break;
			case 25:
				{
				alt2=3;
				}
				break;
			case 21:
				{
				alt2=4;
				}
				break;
			case 19:
				{
				alt2=5;
				}
				break;
			case 10:
				{
				alt2=6;
				}
				break;
			case 18:
				{
				alt2=7;
				}
				break;
			case 17:
				{
				alt2=8;
				}
				break;
			case 22:
				{
				alt2=9;
				}
				break;
			case 26:
				{
				alt2=10;
				}
				break;
			case 13:
				{
				alt2=11;
				}
				break;
			case 14:
				{
				alt2=12;
				}
				break;
			case 15:
				{
				alt2=13;
				}
				break;
			case 24:
				{
				alt2=14;
				}
				break;
			case 16:
				{
				alt2=15;
				}
				break;
			case 11:
				{
				alt2=16;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:50:4: 'void'
					{
					root_0 = (Object)adaptor.nil();


					string_literal2=(Token)match(input,27,FOLLOW_27_in_primitive_type169); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal2_tree = (Object)adaptor.create(string_literal2);
					adaptor.addChild(root_0, string_literal2_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("void"); }
					}
					break;
				case 2 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:51:4: 'boolean'
					{
					root_0 = (Object)adaptor.nil();


					string_literal3=(Token)match(input,12,FOLLOW_12_in_primitive_type176); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal3_tree = (Object)adaptor.create(string_literal3);
					adaptor.addChild(root_0, string_literal3_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("boolean"); }
					}
					break;
				case 3 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:52:4: 'tinyint'
					{
					root_0 = (Object)adaptor.nil();


					string_literal4=(Token)match(input,25,FOLLOW_25_in_primitive_type183); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal4_tree = (Object)adaptor.create(string_literal4);
					adaptor.addChild(root_0, string_literal4_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("tinyint"); }
					}
					break;
				case 4 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:53:4: 'smallint'
					{
					root_0 = (Object)adaptor.nil();


					string_literal5=(Token)match(input,21,FOLLOW_21_in_primitive_type190); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal5_tree = (Object)adaptor.create(string_literal5);
					adaptor.addChild(root_0, string_literal5_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("smallint"); }
					}
					break;
				case 5 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:54:4: 'int'
					{
					root_0 = (Object)adaptor.nil();


					string_literal6=(Token)match(input,19,FOLLOW_19_in_primitive_type197); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal6_tree = (Object)adaptor.create(string_literal6);
					adaptor.addChild(root_0, string_literal6_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("int"); }
					}
					break;
				case 6 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:55:4: 'bigint'
					{
					root_0 = (Object)adaptor.nil();


					string_literal7=(Token)match(input,10,FOLLOW_10_in_primitive_type206); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal7_tree = (Object)adaptor.create(string_literal7);
					adaptor.addChild(root_0, string_literal7_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("bigint");}
					}
					break;
				case 7 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:56:4: 'float'
					{
					root_0 = (Object)adaptor.nil();


					string_literal8=(Token)match(input,18,FOLLOW_18_in_primitive_type213); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal8_tree = (Object)adaptor.create(string_literal8);
					adaptor.addChild(root_0, string_literal8_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("float"); }
					}
					break;
				case 8 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:57:4: 'double'
					{
					root_0 = (Object)adaptor.nil();


					string_literal9=(Token)match(input,17,FOLLOW_17_in_primitive_type220); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal9_tree = (Object)adaptor.create(string_literal9);
					adaptor.addChild(root_0, string_literal9_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("double"); }
					}
					break;
				case 9 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:58:4: 'string'
					{
					root_0 = (Object)adaptor.nil();


					string_literal10=(Token)match(input,22,FOLLOW_22_in_primitive_type226); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal10_tree = (Object)adaptor.create(string_literal10);
					adaptor.addChild(root_0, string_literal10_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("string"); }
					}
					break;
				case 10 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:59:4: 'varchar'
					{
					root_0 = (Object)adaptor.nil();


					string_literal11=(Token)match(input,26,FOLLOW_26_in_primitive_type233); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal11_tree = (Object)adaptor.create(string_literal11);
					adaptor.addChild(root_0, string_literal11_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("varchar"); }
					}
					break;
				case 11 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:60:4: 'char'
					{
					root_0 = (Object)adaptor.nil();


					string_literal12=(Token)match(input,13,FOLLOW_13_in_primitive_type240); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal12_tree = (Object)adaptor.create(string_literal12);
					adaptor.addChild(root_0, string_literal12_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("char"); }
					}
					break;
				case 12 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:61:4: 'date'
					{
					root_0 = (Object)adaptor.nil();


					string_literal13=(Token)match(input,14,FOLLOW_14_in_primitive_type247); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal13_tree = (Object)adaptor.create(string_literal13);
					adaptor.addChild(root_0, string_literal13_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("date"); }
					}
					break;
				case 13 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:62:4: 'datetime'
					{
					root_0 = (Object)adaptor.nil();


					string_literal14=(Token)match(input,15,FOLLOW_15_in_primitive_type253); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal14_tree = (Object)adaptor.create(string_literal14);
					adaptor.addChild(root_0, string_literal14_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("datetime");}
					}
					break;
				case 14 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:63:4: 'timestamp'
					{
					root_0 = (Object)adaptor.nil();


					string_literal15=(Token)match(input,24,FOLLOW_24_in_primitive_type260); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal15_tree = (Object)adaptor.create(string_literal15);
					adaptor.addChild(root_0, string_literal15_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("timestamp");}
					}
					break;
				case 15 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:64:4: 'decimal'
					{
					root_0 = (Object)adaptor.nil();


					string_literal16=(Token)match(input,16,FOLLOW_16_in_primitive_type267); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal16_tree = (Object)adaptor.create(string_literal16);
					adaptor.addChild(root_0, string_literal16_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("decimal");}
					}
					break;
				case 16 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:65:4: 'binary'
					{
					root_0 = (Object)adaptor.nil();


					string_literal17=(Token)match(input,11,FOLLOW_11_in_primitive_type273); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal17_tree = (Object)adaptor.create(string_literal17);
					adaptor.addChild(root_0, string_literal17_tree);
					}

					if ( state.backtracking==0 ) { retval.oi = getPrimitiveInspector("binary"); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "primitive_type"


	public static class array_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "array_type"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:68:1: array_type returns [ ObjectInspector oi ] : 'array' '<' ty= primitive_type '>' ;
	public final SchemaParser.array_type_return array_type() throws RecognitionException {
		SchemaParser.array_type_return retval = new SchemaParser.array_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal18=null;
		Token char_literal19=null;
		Token char_literal20=null;
		ParserRuleReturnScope ty =null;

		Object string_literal18_tree=null;
		Object char_literal19_tree=null;
		Object char_literal20_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:69:2: ( 'array' '<' ty= primitive_type '>' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:69:4: 'array' '<' ty= primitive_type '>'
			{
			root_0 = (Object)adaptor.nil();


			string_literal18=(Token)match(input,9,FOLLOW_9_in_array_type290); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal18_tree = (Object)adaptor.create(string_literal18);
			adaptor.addChild(root_0, string_literal18_tree);
			}

			char_literal19=(Token)match(input,7,FOLLOW_7_in_array_type292); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal19_tree = (Object)adaptor.create(char_literal19);
			adaptor.addChild(root_0, char_literal19_tree);
			}

			pushFollow(FOLLOW_primitive_type_in_array_type298);
			ty=primitive_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, ty.getTree());

			char_literal20=(Token)match(input,8,FOLLOW_8_in_array_type300); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal20_tree = (Object)adaptor.create(char_literal20);
			adaptor.addChild(root_0, char_literal20_tree);
			}

			if ( state.backtracking==0 ) { retval.oi = getStandardListObjectInspector((ty!=null?((SchemaParser.primitive_type_return)ty).oi:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "array_type"


	public static class map_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "map_type"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:72:1: map_type returns [ ObjectInspector oi ] : 'map' '<' kt= primitive_type ',' vt= primitive_type '>' ;
	public final SchemaParser.map_type_return map_type() throws RecognitionException {
		SchemaParser.map_type_return retval = new SchemaParser.map_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal21=null;
		Token char_literal22=null;
		Token char_literal23=null;
		Token char_literal24=null;
		ParserRuleReturnScope kt =null;
		ParserRuleReturnScope vt =null;

		Object string_literal21_tree=null;
		Object char_literal22_tree=null;
		Object char_literal23_tree=null;
		Object char_literal24_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:73:2: ( 'map' '<' kt= primitive_type ',' vt= primitive_type '>' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:73:4: 'map' '<' kt= primitive_type ',' vt= primitive_type '>'
			{
			root_0 = (Object)adaptor.nil();


			string_literal21=(Token)match(input,20,FOLLOW_20_in_map_type318); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal21_tree = (Object)adaptor.create(string_literal21);
			adaptor.addChild(root_0, string_literal21_tree);
			}

			char_literal22=(Token)match(input,7,FOLLOW_7_in_map_type320); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal22_tree = (Object)adaptor.create(char_literal22);
			adaptor.addChild(root_0, char_literal22_tree);
			}

			pushFollow(FOLLOW_primitive_type_in_map_type326);
			kt=primitive_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, kt.getTree());

			char_literal23=(Token)match(input,5,FOLLOW_5_in_map_type328); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal23_tree = (Object)adaptor.create(char_literal23);
			adaptor.addChild(root_0, char_literal23_tree);
			}

			pushFollow(FOLLOW_primitive_type_in_map_type334);
			vt=primitive_type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, vt.getTree());

			char_literal24=(Token)match(input,8,FOLLOW_8_in_map_type336); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal24_tree = (Object)adaptor.create(char_literal24);
			adaptor.addChild(root_0, char_literal24_tree);
			}

			if ( state.backtracking==0 ) { retval.oi = getStandardMapObjectInspector((kt!=null?((SchemaParser.primitive_type_return)kt).oi:null), (vt!=null?((SchemaParser.primitive_type_return)vt).oi:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "map_type"


	public static class struct_type_return extends ParserRuleReturnScope {
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "struct_type"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:76:1: struct_type returns [ ObjectInspector oi ] : 'struct' '<' fields= field_list '>' ;
	public final SchemaParser.struct_type_return struct_type() throws RecognitionException {
		SchemaParser.struct_type_return retval = new SchemaParser.struct_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal25=null;
		Token char_literal26=null;
		Token char_literal27=null;
		ParserRuleReturnScope fields =null;

		Object string_literal25_tree=null;
		Object char_literal26_tree=null;
		Object char_literal27_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:77:2: ( 'struct' '<' fields= field_list '>' )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:77:4: 'struct' '<' fields= field_list '>'
			{
			root_0 = (Object)adaptor.nil();


			string_literal25=(Token)match(input,23,FOLLOW_23_in_struct_type353); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal25_tree = (Object)adaptor.create(string_literal25);
			adaptor.addChild(root_0, string_literal25_tree);
			}

			char_literal26=(Token)match(input,7,FOLLOW_7_in_struct_type355); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal26_tree = (Object)adaptor.create(char_literal26);
			adaptor.addChild(root_0, char_literal26_tree);
			}

			pushFollow(FOLLOW_field_list_in_struct_type361);
			fields=field_list();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, fields.getTree());

			char_literal27=(Token)match(input,8,FOLLOW_8_in_struct_type363); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal27_tree = (Object)adaptor.create(char_literal27);
			adaptor.addChild(root_0, char_literal27_tree);
			}

			if ( state.backtracking==0 ) { retval.oi = getStandardStructObjectInspector((fields!=null?((SchemaParser.field_list_return)fields).names:null), (fields!=null?((SchemaParser.field_list_return)fields).ois:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "struct_type"


	public static class field_list_return extends ParserRuleReturnScope {
		public List<String> names;
		public List<ObjectInspector> ois;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "field_list"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:80:1: field_list returns [ List<String> names , List<ObjectInspector> ois ] : fld= field_def ( ',' fld= field_def )* ;
	public final SchemaParser.field_list_return field_list() throws RecognitionException {
		SchemaParser.field_list_return retval = new SchemaParser.field_list_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal28=null;
		ParserRuleReturnScope fld =null;

		Object char_literal28_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:84:2: (fld= field_def ( ',' fld= field_def )* )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:84:4: fld= field_def ( ',' fld= field_def )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_field_def_in_field_list388);
			fld=field_def();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, fld.getTree());

			if ( state.backtracking==0 ) { retval.names = new LinkedList<String>(); retval.names.add((fld!=null?((SchemaParser.field_def_return)fld).name:null));
				                    retval.ois = new LinkedList<ObjectInspector>(); retval.ois.add((fld!=null?((SchemaParser.field_def_return)fld).oi:null)); }
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:86:6: ( ',' fld= field_def )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==5) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// src/main/java/xw/data/unistreams/schema/parser/Schema.g:86:8: ',' fld= field_def
					{
					char_literal28=(Token)match(input,5,FOLLOW_5_in_field_list399); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal28_tree = (Object)adaptor.create(char_literal28);
					adaptor.addChild(root_0, char_literal28_tree);
					}

					pushFollow(FOLLOW_field_def_in_field_list405);
					fld=field_def();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, fld.getTree());

					if ( state.backtracking==0 ) { retval.names.add((fld!=null?((SchemaParser.field_def_return)fld).name:null)); retval.ois.add((fld!=null?((SchemaParser.field_def_return)fld).oi:null)); }
					}
					break;

				default :
					break loop3;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "field_list"


	public static class field_def_return extends ParserRuleReturnScope {
		public String name;
		public ObjectInspector oi;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "field_def"
	// src/main/java/xw/data/unistreams/schema/parser/Schema.g:89:1: field_def returns [ String name, ObjectInspector oi ] : ID ':' ty= type ;
	public final SchemaParser.field_def_return field_def() throws RecognitionException {
		SchemaParser.field_def_return retval = new SchemaParser.field_def_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID29=null;
		Token char_literal30=null;
		ParserRuleReturnScope ty =null;

		Object ID29_tree=null;
		Object char_literal30_tree=null;

		try {
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:90:2: ( ID ':' ty= type )
			// src/main/java/xw/data/unistreams/schema/parser/Schema.g:90:4: ID ':' ty= type
			{
			root_0 = (Object)adaptor.nil();


			ID29=(Token)match(input,ID,FOLLOW_ID_in_field_def427); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			ID29_tree = (Object)adaptor.create(ID29);
			adaptor.addChild(root_0, ID29_tree);
			}

			char_literal30=(Token)match(input,6,FOLLOW_6_in_field_def429); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal30_tree = (Object)adaptor.create(char_literal30);
			adaptor.addChild(root_0, char_literal30_tree);
			}

			pushFollow(FOLLOW_type_in_field_def435);
			ty=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, ty.getTree());

			if ( state.backtracking==0 ) { retval.name = (ID29!=null?ID29.getText():null); retval.oi = (ty!=null?((SchemaParser.type_return)ty).oi:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "field_def"

	// Delegated rules



	public static final BitSet FOLLOW_type_in_schema86 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_schema88 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitive_type_in_type112 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_array_type_in_type125 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_map_type_in_type137 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_struct_type_in_type150 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_27_in_primitive_type169 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_12_in_primitive_type176 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_25_in_primitive_type183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_21_in_primitive_type190 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_19_in_primitive_type197 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_10_in_primitive_type206 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_18_in_primitive_type213 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_17_in_primitive_type220 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_22_in_primitive_type226 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_26_in_primitive_type233 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_13_in_primitive_type240 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_14_in_primitive_type247 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_15_in_primitive_type253 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_24_in_primitive_type260 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_16_in_primitive_type267 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_11_in_primitive_type273 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_9_in_array_type290 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_7_in_array_type292 = new BitSet(new long[]{0x000000000F6FFC00L});
	public static final BitSet FOLLOW_primitive_type_in_array_type298 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_8_in_array_type300 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_20_in_map_type318 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_7_in_map_type320 = new BitSet(new long[]{0x000000000F6FFC00L});
	public static final BitSet FOLLOW_primitive_type_in_map_type326 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_5_in_map_type328 = new BitSet(new long[]{0x000000000F6FFC00L});
	public static final BitSet FOLLOW_primitive_type_in_map_type334 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_8_in_map_type336 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_23_in_struct_type353 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_7_in_struct_type355 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_field_list_in_struct_type361 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_8_in_struct_type363 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_field_def_in_field_list388 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_5_in_field_list399 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_field_def_in_field_list405 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_ID_in_field_def427 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_6_in_field_def429 = new BitSet(new long[]{0x000000000FFFFE00L});
	public static final BitSet FOLLOW_type_in_field_def435 = new BitSet(new long[]{0x0000000000000002L});
}
