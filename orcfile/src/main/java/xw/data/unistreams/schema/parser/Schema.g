/*

antlr 3.x grammar to parse hive ql schema.

Preprocess with:

  java -cp tool/antlr-3.5.2-complete.jar org.antlr.Tool -lib . src/main/java/xw/data/unistreams/schema/parser/Schema.g

 */

grammar Schema;

options {  
  	language = Java; 
  	output = AST;
  	backtrack=true;
	k = 2;
} 

@header {
package xw.data.unistreams.schema.parser;

import java.util.LinkedList;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import static org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.getStandardMapObjectInspector;
import static org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.getStandardStructObjectInspector;
import static org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory.getStandardListObjectInspector;
import static xw.data.unistreams.common.hive.objectinspector.ObjectInspectorUtils.getPrimitiveInspector;
}


@lexer::header {
package xw.data.unistreams.schema.parser;
}

// parser rules

schema 	returns [ ObjectInspector oi ]
	: ty = type EOF! { $oi = $ty.oi; }
	;	

type returns [ ObjectInspector oi ]
 	: pt = primitive_type { $oi = $pt.oi; } 
 	| at = array_type { $oi = $at.oi; }
 	| mt = map_type  { $oi = $mt.oi; }
 	| st = struct_type  { $oi = $st.oi;  }
	;
	
primitive_type returns [ ObjectInspector oi ]
	: 'void' { $oi = getPrimitiveInspector("void"); }
	| 'boolean' { $oi = getPrimitiveInspector("boolean"); }
	| 'tinyint' { $oi = getPrimitiveInspector("tinyint"); }
	| 'smallint' { $oi = getPrimitiveInspector("smallint"); }
	| 'int'   { $oi = getPrimitiveInspector("int"); }
	| 'bigint' { $oi = getPrimitiveInspector("bigint");}
	| 'float' { $oi = getPrimitiveInspector("float"); }
	| 'double'{ $oi = getPrimitiveInspector("double"); }
	| 'string' { $oi = getPrimitiveInspector("string"); }
	| 'varchar' { $oi = getPrimitiveInspector("varchar"); }
	| 'char' { $oi = getPrimitiveInspector("char"); }
	| 'date'{ $oi = getPrimitiveInspector("date"); }
	| 'datetime' { $oi = getPrimitiveInspector("datetime");}
	| 'timestamp' { $oi = getPrimitiveInspector("timestamp");}
	| 'decimal'{ $oi = getPrimitiveInspector("decimal");}
	| 'binary' { $oi = getPrimitiveInspector("binary"); }
	;

array_type returns [ ObjectInspector oi ]
	: 'array' '<' ty = primitive_type '>' { $oi = getStandardListObjectInspector($ty.oi); }
	;

map_type returns [ ObjectInspector oi ] 
	: 'map' '<' kt = primitive_type ',' vt = primitive_type '>' { $oi = getStandardMapObjectInspector($kt.oi, $vt.oi); }
	;

struct_type returns [ ObjectInspector oi ]
	: 'struct' '<' fields = field_list '>' { $oi = getStandardStructObjectInspector($fields.names, $fields.ois); }
	;

field_list returns [ List<String> names , List<ObjectInspector> ois ] 
//	: fld1 = field_def ',' flds = field_list { $names = $flds.names; $ois = $flds.ois; $names.add(0, $fld1.name); $ois.add(0, $fld1.oi); }
//	| fld2 = field_def { $names = new LinkedList<String>(); $names.add($fld2.name); $ois = new LinkedList<ObjectInspector>(); $ois.add($fld2.oi); }
//	| fld2 = field_def { $names = new LinkedList<String>(); $names.add($fld2.name); $ois = new LinkedList<ObjectInspector>(); $ois.add($fld2.oi); }
	: fld = field_def { $names = new LinkedList<String>(); $names.add($fld.name);
	                    $ois = new LinkedList<ObjectInspector>(); $ois.add($fld.oi); }
	    ( ',' fld = field_def { $names.add($fld.name); $ois.add($fld.oi); } ) *
	;

field_def returns [ String name, ObjectInspector oi ] 
	: ID ':' ty = type { $name = $ID.text; $oi = $ty.oi; } 	
	;

ID  	: ( 'a'..'z' | 'A'..'Z' | '_' ) ( 'a'..'z' | 'A'..'Z' | '0'..'9' | '_' )*
	;