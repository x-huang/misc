package xw.data.unistreams.core;

import java.io.IOException;

/**
 * Created by xhuang on 3/18/16.
 */
public interface DataStream extends AutoCloseable
{
    Object[] read() throws IOException;
}
