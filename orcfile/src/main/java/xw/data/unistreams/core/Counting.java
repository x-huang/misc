package xw.data.unistreams.core;

/**
 * Created by xhuang on 5/24/16.
 */
public interface Counting
{
    long getCount();
}
