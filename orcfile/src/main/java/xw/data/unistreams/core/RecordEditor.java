package xw.data.unistreams.core;

/**
 * Created by xhuang on 5/26/16.
 */
public interface RecordEditor
{
    void edit(Object[] record);
}
