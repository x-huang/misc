package xw.data.unistreams.core;

import xw.data.unistreams.streams.orc.OrcFileOperator;

/**
 * Created by xhuang on 5/25/16.
 */
public class Resources
{
    static public ResourceOperator newFileOperator(FileFormat format) {
        switch (format) {
            case orc:
                return new OrcFileOperator();
            case rcfile:
            default:
                throw new UnsupportedOperationException("unsupported file format: " + format);
        }
    }

    static public ResourceOperator newFileOperator(String format) {
        return newFileOperator(FileFormat.valueOf(format));
    }
}
