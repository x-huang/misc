package xw.data.unistreams.core;

import java.io.IOException;
import java.util.List;

/**
 * Created by xhuang on 3/18/16.
 */
public class DataStreams
{
    private static final DataStream dummyEmtpyDataStream = new DataStream() {
        @Override
        public Object[] read() throws IOException {
            return null;
        }

        @Override
        public void close() throws IOException {
        }
    };

    public static DataStream getDummyEmpty() {
        return dummyEmtpyDataStream;
    }

    public static DataStream wrapRecordList(final List<Object[]> records) {
        return new DataStream() {
            private int index = 0;

            @Override
            public Object[] read() throws IOException {
                return index<records.size()? records.get(index++): null;
            }

            @Override
            public void close() throws IOException {
            }
        };
    }

    public static DataStream wrapRecordInputStream(final RecordInputStream ris) {
        return new DataStream() {
            @Override
            public Object[] read() throws IOException {
                return ris.read();
            }

            @Override
            public void close() throws Exception {
                ris.close();
            }
        };
    }

    public static void feedRecordOutputStream(final RecordOutputStream ros,
                                              final DataStream ds) throws IOException {
        while (true) {
            final Object[] record = ds.read();
            if (record == null) {
                break;
            }
            ros.write(record);
        }
    }

    public static void connect(final RecordInputStream in,
                               final RecordOutputStream out) throws IOException {

        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            out.write(record);
        }
    }

    public static void connect(final RecordInputStream in,
                               final RecordOutputStream out,
                               final RecordFilter filter) throws IOException {

        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            if (filter.eval(record)) {
                out.write(record);
            }
        }
    }

    public static void connect(final RecordInputStream in,
                               final RecordOutputStream out,
                               final RecordEditor editor) throws IOException {

        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            editor.edit(record);
            out.write(record);
        }
    }

    public static void connect(final RecordInputStream in,
                               final RecordOutputStream out,
                               final RecordFilter filter,
                               final RecordEditor editor) throws IOException {

        while (true) {
            final Object[] record = in.read();
            if (record == null) {
                break;
            }
            if (filter.eval(record)) {
                editor.edit(record);
                out.write(record);
            }
        }
    }
}
