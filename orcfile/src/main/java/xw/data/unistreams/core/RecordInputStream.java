package xw.data.unistreams.core;

import java.io.IOException;

/**
 * Created by xhuang on 5/24/16.
 */
public interface RecordInputStream extends AutoCloseable, Counting
{
    Object[] read() throws IOException;
}
