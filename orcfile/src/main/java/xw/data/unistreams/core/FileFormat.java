package xw.data.unistreams.core;

/**
 * Created by xhuang on 5/25/16.
 */
public enum FileFormat
{
    orc, rcfile;
}
