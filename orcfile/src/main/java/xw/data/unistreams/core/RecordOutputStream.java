package xw.data.unistreams.core;

import java.io.IOException;

/**
 * Created by xhuang on 5/24/16.
 */
public interface RecordOutputStream extends AutoCloseable, Counting
{
    void write(Object[] record) throws IOException;
}
