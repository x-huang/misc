package xw.data.unistreams.core;


import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by xhuang on 5/25/16.
 */
public abstract class ResourceOperator
{
    protected FileFormat format = null;
    protected List<URI> uris = new ArrayList<>();
    protected String schema = null;
    protected long limit = -1;

    public ResourceOperator addUri(String s) {
        this.uris.add(URI.create(s));
        return this;
    }

    public ResourceOperator addUri(URI uri) {
        this.uris.add(uri);
        return this;
    }

    public ResourceOperator addUris(Collection<URI> uris) {
        this.uris.addAll(uris);
        return this;
    }

    public ResourceOperator setFormat(FileFormat format) {
        this.format = format;
        return this;
    }

    public ResourceOperator setFormat(String format) {
        this.format = FileFormat.valueOf(format);
        return this;
    }

    public ResourceOperator setSchema(String schema) {
        this.schema = schema;
        return this;
    }

    public ResourceOperator setLimit(long limit) {
        this.limit = limit;
        return this;
    }

    abstract public RecordInputStream openInputStream() throws IOException;
    abstract public RecordOutputStream openOutputStream() throws IOException;
    abstract public Object describe() throws IOException;
}
