package org.apache.hadoop.hive.ql.io.orc;

import org.apache.hadoop.io.Writable;

/*
 * Xiaowan: have to make such a package name because class 'OrcStruct' 
 * is package scoped.
 */

public class OrcUtils
{
	public static OrcStruct getOrcStruct(Object[] data) {
		final OrcStruct struct = new OrcStruct(data.length);
		for (int i=0; i<data.length; i++) {
			final Writable w = Writables.getWritableObject(data[i]);
			struct.setFieldValue(i, w);
		}
		return struct;
	}
}
