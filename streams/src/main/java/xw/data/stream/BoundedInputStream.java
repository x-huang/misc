package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;

// from: http://ostermiller.org/utils/src/SizeLimitInputStream.java.html
// FIXME: I shall use org.apache.commons.io.input.BoundedInputStream instead.

public class BoundedInputStream extends InputStream 
{
    protected InputStream _in;
    protected long _maxBytesToRead = 0;
    protected long _bytesRead = 0;
    protected long _bytesReadSinceMark = 0;
    protected long _markReadLimitBytes = -1;
 
    public long getBytesRead()
    {
        return _bytesRead;
    }
 
    public long getBytesLeft()
    {
        return _maxBytesToRead - _bytesRead;
    }
 
    public boolean allBytesRead()
    {
        return getBytesLeft() == 0;
    }
 
    public long getMaxBytesToRead()
    {
        return _maxBytesToRead;
    }
 
    public BoundedInputStream(InputStream in, long maxBytesToRead)
    {
        _in = in;
        _maxBytesToRead = maxBytesToRead;
        _bytesRead = 0;
        _bytesReadSinceMark = 0;
        _markReadLimitBytes = -1;
    }
 
    @Override 
    public int read() throws IOException 
    {
        if (_bytesRead >= _maxBytesToRead) {
            return -1;
        }
        int b = _in.read();
        if(b != -1) {
            _bytesRead++;
            _bytesReadSinceMark++;
        }
        return b;
    }
 
    @Override 
    public int read(byte[] b) throws IOException 
    {
        return this.read(b, 0, b.length);
    }
 
    @Override 
    public int read(byte[] b, int off, int len) throws IOException 
    {
        if (_bytesRead >= _maxBytesToRead) {
            return -1;
        }
        long bytesLeft = getBytesLeft();
        if (len > bytesLeft) {
            len = (int)bytesLeft;
        }
        int bytesJustRead = _in.read(b, off, len);
        _bytesRead += bytesJustRead;
        _bytesReadSinceMark += bytesJustRead;
        return bytesJustRead;
    }
 
    @Override 
    public long skip(long n) throws IOException 
    {
        if (_bytesRead >= _maxBytesToRead) {
            return -1;
        }
        long bytesLeft = getBytesLeft();
        if (n > bytesLeft){
            n = bytesLeft;
        }
        return _in.skip(n);
    }
 
    @Override 
    public int available() throws IOException 
    {
        int available = _in.available();
        long bytesLeft = getBytesLeft();
        if (available > bytesLeft) {
            available = (int)bytesLeft;
        }
        return available;
    }
 
    @Override 
    public void close() throws IOException 
    {
        _in.close();
    }
 
    @Override 
    public void mark(int readlimit) 
    {
        if (_in.markSupported()) {
            _markReadLimitBytes = readlimit;
            _bytesReadSinceMark = 0;
            _in.mark(readlimit);
        }
    }
 
    @Override 
    public void reset() throws IOException 
    {
        if (_in.markSupported() && 
        		_bytesReadSinceMark <= _markReadLimitBytes) {
            _bytesRead -= _bytesReadSinceMark;
            _in.reset();
            _bytesReadSinceMark = 0;
        }
    }
 
    @Override 
    public boolean markSupported()
    {
        return _in.markSupported();
    }
}
