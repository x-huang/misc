package xw.data.stream.scp;

public class PipedProcessException extends Exception
{
	private static final long serialVersionUID = 6647539122862631501L;
	protected String _cmd;
	protected int _exitValue;
	protected String _errMsg;
	
	public PipedProcessException(String cmd, int exitValue, String errMsg)
	{
		_cmd = cmd;
		_exitValue = exitValue;
		_errMsg = errMsg;
	}
	
	public String getCommndLine()
	{
		return _cmd;
	}
	
	public int getExitCode()
	{
		return _exitValue;
	}
	
	public String getErrorMessage()
	{
		return _errMsg;
	}
	
	@Override
	public String getMessage()
	{
		return "process '" + _cmd + "' returned anormal exit code: " 
				+ _exitValue + ", with error message: " + _errMsg;
	}
}
