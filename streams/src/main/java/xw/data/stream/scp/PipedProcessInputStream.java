package xw.data.stream.scp;

import java.io.IOException;
import java.io.InputStream;

public class PipedProcessInputStream extends InputStream implements AutoCloseable
{
	final protected PipedProcess _proc;
	final private InputStream _in;
	
	protected PipedProcessInputStream(PipedProcess proc) {
		_proc = proc;
		_in = proc.getStdoutAsInputStream();
	}

	@Override
	public int read() throws IOException
	{
		return _in.read();
	}

	@Override
	public void close() throws IOException
	{
		_in.close();
		try {
			_proc.checkReturnValue();
		}
		catch (PipedProcessException e) {
			throw new IOException("child process error", e);
		}
	}
}
