package xw.data.stream.scp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StdErrCollector extends Thread
{
    static final Logger log = LoggerFactory.getLogger(StdErrCollector.class);

	final private ByteArrayOutputStream _sink;
	final InputStream _stderr;
	
	public StdErrCollector(Process proc)
	{
		_sink = new ByteArrayOutputStream(4096);
		_stderr = proc.getErrorStream();
	}
	
	public String getErrorMessage()
	{
		if (isAlive()) {
			throw new IllegalThreadStateException("stderr collector is yet running");
		}
		return _sink.toString().trim();
	}
	
	@Override
	public void run()
	{
		byte[] buffer = new byte[1024];
		int len;
		try {
			while ((len = _stderr.read(buffer)) != -1) {
				_sink.write(buffer, 0, len);
			}
		}
		catch (IOException e) {
			log.error("IO error while reading stderr", e);
			throw new RuntimeException(e);
		}
	}

}
