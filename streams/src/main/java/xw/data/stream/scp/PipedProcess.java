package xw.data.stream.scp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.stream.DataSink;
import xw.data.stream.DataSource;

public class PipedProcess implements DataSink, DataSource
{
    static final Logger log = LoggerFactory.getLogger(PipedProcess.class);

	protected final List<String> _cmd;
	private Process _proc;
	private StdErrCollector _errCollector;
	
	public PipedProcess(List<String> cmd) throws IOException
	{
		_cmd = cmd;

		ProcessBuilder pb = new ProcessBuilder(_cmd);
		_proc = pb.start();
		_errCollector = new StdErrCollector(_proc);
		_errCollector.start();

		assert processIsRunning();
	}
	
	public String getCommandLine()
	{
		StringBuilder sb = new StringBuilder();
		for (String s: _cmd) {
			if (sb.length() != 0)
				sb.append(" ");
			if (s.contains(" "))
				s = "\"" + s + "\"";
			sb.append(s);
		}
		return sb.toString();
	}
	
	private boolean processIsRunning() 
	{
		try {
			_proc.exitValue();
			return false;
		}
		catch (IllegalThreadStateException e) {
			return true;
		}
	}
	
	public int getProcessReturnValue()
	{
		try {
			return _proc.exitValue();
		}
		catch (IllegalThreadStateException e) {
			return -1;
		}
	}
	
	public void checkReturnValue() throws PipedProcessException
	{
		int retValue;
		while (true) {
			try {
				retValue = _proc.waitFor();
				break;
			}
			catch (InterruptedException e) {
                log.warn("interrupted waiting for child process", e);
			}
		}
		
		if (retValue != 0) {
			String cmdLine = getCommandLine();
			try {
				_errCollector.join();
			}
			catch (InterruptedException e) {
                log.warn("interrupted joining with stderr collector", e);
			}
			String errMsg = _errCollector.getErrorMessage();
			throw new PipedProcessException(cmdLine, retValue, errMsg);
		}
	}
	
	public String getProcessErrorMessage() throws InterruptedException
	{
        log.debug("joining stderr collector thread: {}", _errCollector.getId());
		_errCollector.join();
		return _errCollector.getErrorMessage();
	}
	
	public List<String> getCommand() 
	{
		return _cmd;
	}
	
	public InputStream getStdoutAsInputStream()
	{
		return _proc.getInputStream();
	}
	
	public OutputStream getStdinAsOutputStream()
	{
		return _proc.getOutputStream();
	}
	
	public InputStream getStderrAsInputStream()
	{
		return _proc.getErrorStream();
	}

	@Override
	public InputStream getInputStream() throws IOException
	{
		return new PipedProcessInputStream(this);
		//return getStdoutAsInputStream();
	}

	@Override
	public OutputStream getOutputStream() throws IOException
	{
		return new PipedProcessOutputStream(this);
		//return getStdinAsOutputStream();
	}
}

