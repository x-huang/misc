package xw.data.stream.scp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScpCommand implements CommandLineGenerator
{
	protected boolean _sink;
	protected String _user;
	protected String _host;
	protected String _path;
	protected String _keyFile;
	protected Map<String, String> _options;
	protected boolean _quiet;
	protected boolean _compress;
	protected Integer _port;
	
	public ScpCommand(boolean sink, String user, String host, String path)
	{
		_sink = sink;
		_host = host;
		_user = user;
		_path = path;
		_keyFile = null;
		_port = null;
		_options = null;
		_quiet = false;
		_compress = false;
	}
	
	protected static String getDirName(String path)
	{
		int lastSlash = path.lastIndexOf("/");
		if (lastSlash < 0)
			return null;
		else
			return path.substring(0, lastSlash);
	}
	
	public ScpCommand setKeyFile(String keyFile)
	{
		_keyFile = keyFile;
		return this;
	}
	
	public ScpCommand setCompressMode(boolean compress)
	{
		_compress = compress;
		return this;
	}
	
	public ScpCommand setOption(String opt, String value)
	{
		if (_options == null)
			_options = new HashMap<String, String>();
		_options.put(opt, value);
		return this;
	}
	
	public ScpCommand setQuietness(boolean quiet)
	{
		_quiet = quiet;
		return this;
	}
	
	public ScpCommand setPort(int port)
	{
		_port = port;
		return this;
	}

	@Override
	public List<String> createCommand()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		list.add("ssh");
		
		if (_keyFile != null) {
			list.add("-i");
			list.add(_keyFile);
		}
		
		if (_compress)
			list.add("-C");
		
		if (_port != null) {
			list.add("-p");
			list.add("" + _port);
		}
		
		if (_quiet) 
			list.add("-q");
		
		if (_options != null) {
			ArrayList<String> opts = new ArrayList<String>();
			for (Map.Entry<String, String> entry: _options.entrySet()) {
				opts.add(entry.getKey() + "=" + entry.getValue());
			}
			assert opts.size() > 0;
			String opt_o =opts.get(0);
			if (opts.size() > 1) {
				for (int i=1; i<opts.size(); i++) {
					opt_o = "," + opts.get(i);
				}
			}
			list.add("-o");
			list.add(opt_o);
		}
		
		if (_user != null)
			list.add(_user + "@" + _host);
		else 
			list.add(_host);
		
		String dirname = getDirName(_path);
		
		if (_sink) {
			if (dirname == null)
				list.add("cat > " + _path);
			else 
				list.add("mkdir -p '" + dirname + "'; cat > '" + _path + "'");
		}
		else {
			list.add("cat '" + _path + "'");
		}
				
		return list;
	}
}
