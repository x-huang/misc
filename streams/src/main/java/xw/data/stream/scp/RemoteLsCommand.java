package xw.data.stream.scp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RemoteLsCommand implements CommandLineGenerator
{
	private final String _user;
	private final String _host;
	private final String _path;
	private boolean _showHidden;
	private String _keyFile;
	private Integer _port;
	private boolean _quiet;
	protected Map<String, String> _options;

	public RemoteLsCommand(String user, String host, String path)
	{
		_user = user;
		_host = host;
		_path = path;
		_showHidden = false;
		_keyFile = null;
		_port = null;
		_quiet = false;
		_options = null;
	}
	
	public RemoteLsCommand setShowHidden(boolean showHidden)
	{
		_showHidden = showHidden;
		return this;
	}
	
	public RemoteLsCommand setKeyFile(String keyFile)
	{
		_keyFile = keyFile;
		return this;
	}
	
	public RemoteLsCommand setPort(int port)
	{
		_port = port;
		return this;
	}

	public RemoteLsCommand setQuietness(boolean quiet)
	{
		_quiet = quiet;
		return this;
	}
	
	public RemoteLsCommand setOption(String opt, String value)
	{
		if (_options == null)
			_options = new HashMap<String, String>();
		_options.put(opt, value);
		return this;
	}
	
	@Override
	public List<String> createCommand()
	{	
		ArrayList<String> list = new ArrayList<String>();
		
		list.add("ssh");
		
		if (_keyFile != null) {
			list.add("-i");
			list.add(_keyFile);
		}
				
		if (_port != null) {
			list.add("-p");
			list.add("" + _port);
		}
		
		if (_quiet) 
			list.add("-q");
		
		if (_options != null) {
			ArrayList<String> opts = new ArrayList<String>();
			for (Map.Entry<String, String> entry: _options.entrySet()) {
				opts.add(entry.getKey() + "=" + entry.getValue());
			}
			assert opts.size() > 0;
			String opt_o =opts.get(0);
			if (opts.size() > 1) {
				for (int i=1; i<opts.size(); i++) {
					opt_o = "," + opts.get(i);
				}
			}
			list.add("-o");
			list.add(opt_o);
		}
		
		if (_user != null)
			list.add(_user + "@" + _host);
		else 
			list.add(_host);
		
		list.add("ls " + (_showHidden? "-laL ":"-lAL ") + "'" + _path + "' | sed /^total/d");
		return list;
	}

}
