package xw.data.stream.scp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import xw.data.common.uri.GeneralUriQuery;
import xw.data.stream.IResourceOperator;

public class RemoteResourceOperator implements IResourceOperator
{
	private static ScpCommand createScpCommand(URI uri, boolean asInputStream)
	{
		String user = uri.getUserInfo();
		String host = uri.getHost();
		String path = uri.getPath().substring(1);
		ScpCommand scp = new ScpCommand(!asInputStream, user, host, path);
	
		int port = uri.getPort();
		if (port > 0)
			scp.setPort(port);
		
		GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
		if (query.getParameterAsYes("quiet"))
			scp.setQuietness(true);
		if (query.getParameterAsYes("compress"))
			scp.setCompressMode(true);
		return scp;
	}
	
	/*
	public static InputStream getInputStream(URI uri) throws IOException
	{
		ScpCommand scp = createScpCommand(uri, true);
		PipedProcess proc = new PipedProcess(scp.createCommand());
		proc.start();
		return proc.getInputStream();
	}
	
	public static OutputStream getOutputStream(URI uri) throws IOException
	{
		ScpCommand scp = createScpCommand(uri, false);
		PipedProcess proc = new PipedProcess(scp.createCommand());
		proc.start();
		return proc.getOutputStream();
	}*/
	
	// FIXME: make it private
	protected static List<String> readLines(InputStream in)
			throws IOException
	{
		ArrayList<String> lines = new ArrayList<>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));

		String line = null;
		while ((line = reader.readLine()) != null) {
			lines.add(line);
		}
		return lines;
	}
	
	@Override
	public Object describeResource(URI uri) 
			throws URISyntaxException, IOException
	{
		/*
		String user = uri.getUserInfo();
		String host = uri.getHost();
		String path = uri.getPath().substring(1);
		final RemoteLsCommand ls = new RemoteLsCommand(user, host, path);
		
		int port = uri.getPort();
		if (port > 0)
			ls.setPort(port);
		
		GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
		if (query.getParameterAsYes("quiet"))
			ls.setQuietness(true);
		
		PipedProcess proc = new PipedProcess(ls.createCommand()) ;
		proc.start();
		List<String> lines = readLines(proc.getInputStream());
		*/
		throw new UnsupportedOperationException("describing a remote resource not supported yet");
	}

	@Override
	public InputStream createInputStream(URI uri) 
			throws URISyntaxException, IOException
	{
		ScpCommand scp = createScpCommand(uri, true);
		PipedProcess proc = new PipedProcess(scp.createCommand());
		// proc.start();
		return proc.getInputStream();
	}

	@Override
	public OutputStream createOutputStream(URI uri) 
			throws URISyntaxException, IOException
	{
		ScpCommand scp = createScpCommand(uri, false);
		PipedProcess proc = new PipedProcess(scp.createCommand());
		// proc.start();
		return proc.getOutputStream();
	}
}
