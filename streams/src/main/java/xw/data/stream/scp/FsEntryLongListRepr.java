package xw.data.stream.scp;

public class FsEntryLongListRepr
{
	/*
	permissions
	number of linked hard-links
	owner of the file
	group
	size
	modification date
	creation date and time
	file/directory name
	*/
	protected final String _ll;
	protected final String perm;
	protected final int hardLinks;
	protected final String owner;
	protected final String group;
	protected final long size;
	protected final String date;
	protected final String time;
	protected final String name;
	
	public FsEntryLongListRepr(String ll)
	{
		_ll = ll;
		String[] items = ll.split("\\s+");
		assert items.length == 8;
		perm = items[0];
		hardLinks = Integer.parseInt(items[1]);
		owner = items[2];
		group = items[3];
		size = Long.parseLong(items[4]);
		date = items[5];
		time = items[6];
		name = items[7];
	}

	public boolean isDirectory()
	{
		return perm.charAt(0) == 'd';
	}
}
