package xw.data.stream.scp;

import java.io.IOException;
import java.io.OutputStream;

public class PipedProcessOutputStream extends OutputStream implements AutoCloseable
{
	final protected PipedProcess _proc;
	final private OutputStream _out;
	
	protected PipedProcessOutputStream(PipedProcess proc) {
		_proc = proc;
		_out = proc.getStdinAsOutputStream();
	}
	
	@Override
	public void write(int b) throws IOException
	{
		_out.write(b);
	}
	
	@Override
	public void close() throws IOException
	{
		_out.close();
		try {
			_proc.checkReturnValue();
		}
		catch (PipedProcessException e) {
			throw new IOException("child process error", e);
		}
	}
}
