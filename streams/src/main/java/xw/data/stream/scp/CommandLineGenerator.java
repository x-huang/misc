package xw.data.stream.scp;

import java.util.List;

public interface CommandLineGenerator
{
	public List<String> createCommand();
}
