package xw.data.stream;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public interface IResourceDescriber
{
	public Object describeResource(URI uri) 
			throws URISyntaxException, IOException;
}
