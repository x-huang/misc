package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;

public interface DataSource
{
	public InputStream getInputStream() throws IOException; 
}
