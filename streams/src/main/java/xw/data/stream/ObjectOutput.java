package xw.data.stream;

import java.io.Closeable;
import java.io.IOException;

public interface ObjectOutput extends Closeable
{
	public void write(Object o) throws IOException;
	public long getCount();
}
