package xw.data.stream.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.stream.ObjectInput;

public class JdbcDataInput implements ObjectInput
{
    static final Logger log = LoggerFactory.getLogger(JdbcDataInput.class);

	protected final String _scheme;
	protected final String _server;
	protected final String _dbName;
	protected final String _tblName;
	protected final String _user;
	protected final String _password;
	protected String[] _selects;
	protected Map<String, String> _querySpec;
	protected int _limit;
	protected int _fetchSize; 
	private Connection _conn;
	private ResultSet _rs;
	private int _nCols;
	private int _count;
	private String _sql;

	public JdbcDataInput(String scheme, String server, 
						  String dbName, String tblName, 
						  String user, String password)
	{
		_scheme = scheme;
		_server = server;
		_dbName = dbName;
		_tblName = tblName;
		_user = user;
		_password = password;
		_selects = null;
		_querySpec = null;
		_limit = -1;
		_conn = null;
		_rs = null;
		_nCols = -1;
		_count = 0;
		_fetchSize = Integer.MIN_VALUE;
		_sql = null;  
	}
	
	public JdbcDataInput setLimit(Integer limit)
	{
		_limit = limit == null? -1: limit;
		return this;
	}
	
	public JdbcDataInput setSelects(String[] selects)
	{
		_selects = (selects == null? null : selects.clone());
		return this;
	}
	
	public JdbcDataInput setFilter(Map<String, String> filter)
	{
		_querySpec = new HashMap<String, String>(filter);
		return this;
	}
	
	public JdbcDataInput setSelectFetchSize(int size)
	{
		_fetchSize = size;
		return this;
	}
	
	public JdbcDataInput setCustomSelectStatement(String sql)
	{
		// validate if the target table same as what in resource uri.
		Pattern pattern = Pattern.compile("\\bfrom\\s+([^\\s]+)", Pattern.CASE_INSENSITIVE);
		final Matcher matcher = pattern.matcher(sql);
		if (matcher.find()) {
			String fromTable = matcher.group(1);
			String[] tokens = fromTable.split("\\.");
			if (tokens.length == 2) {
				if (! tokens[0].equalsIgnoreCase(_dbName)) {
					String msg = "jdbc custom sql: database doesn't match: " 
								+ tokens[0] + " vs. " + _dbName;
					throw new IllegalArgumentException(msg);
				}
				else if (! tokens[1].equalsIgnoreCase(_tblName)) {
					String msg = "jdbc custom sql: table doesn't match: " 
								+ tokens[1] + " vs. " + _tblName;
					throw new IllegalArgumentException(msg);
				}
			}
			else if (tokens.length == 1) {
				if (! tokens[0].equalsIgnoreCase(_tblName)) {
					String msg = "jdbc custom sql: table doesn't match: " 
								+ tokens[0] + " vs. " + _tblName;
					throw new IllegalArgumentException(msg);
				}
			}
		}
		
		// on SQL syntax error, let JDBC report error in detail 
		_sql = sql;
		return this;
	}
	
	public void initialize() throws ClassNotFoundException, SQLException
	{
		final JdbcHelper helper = JdbcHelpers.getHelper(_scheme);
		
		String driver = helper.getDriver();
		Class.forName(driver);
		
		String jdbcUrl = helper.makeUrl(_server, _dbName, _user, _password, null);
		log.debug("using jdbc url: {}", jdbcUrl);
		
		_conn = DriverManager.getConnection(jdbcUrl);
		
		final String sql;
		if (_sql == null) {
			sql = helper.makeSqlSelect(_tblName, _selects, _querySpec, _limit);
		}
		else {
			if (_querySpec.size() > 0) {
                log.warn("custom sql statement suppresses user query spec: {}", _sql);
			}
			if (_selects.length > 0) {
                log.warn("custom sql statement suppresses user selects: {}", _sql);
			}
			sql = _sql;
		}
        log.debug("executing sql: {}", sql);

		Statement stmt = _conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		Integer suggestedFetchSize = helper.suggestFetchSize();
		if (suggestedFetchSize != null)
			stmt.setFetchSize(suggestedFetchSize);
		_rs = stmt.executeQuery(sql);
		_nCols = _rs.getMetaData().getColumnCount();

        log.debug("initialized");
	}

	@Override
	public Object read() throws IOException
	{
		try {
			if (!_rs.next()) {
                return null;
            }

			Object[] curRow = new Object[_nCols];
			for (int i=0; i<_nCols; i++) {
				curRow[i] = _rs.getObject(i+1);
			}
			_count++;
            if ((_count % 1_000_000) == 0) {
                log.trace("progress: read rows: {}", _count);
            }
			return curRow;
		}
		catch (SQLException e) {
			throw new IOException("sql error reading records", e);
		}
	}

	@Override
	public long getCount()
	{
		return _count;
	}

	@Override
	public void close() throws IOException
	{
		try {
			if (_conn != null) 
				_conn.close();
		}
		catch (SQLException e) {
			throw new IOException("sql error disconnecting", e);
		}
	}
}
