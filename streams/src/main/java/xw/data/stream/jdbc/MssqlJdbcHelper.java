package xw.data.stream.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import xw.data.common.lang.StringUtils;

public class MssqlJdbcHelper implements JdbcHelper
{
	@Override
	public String getDriver()
	{
		return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	}

	@Override
	public String makeUrl(String serverAddr, String dbName, String username,
			String password, Map<String, String> extraOptions)
	{
		// http://msdn.microsoft.com/en-US/library/ms378428(v=sql.90).aspx
		// example: jdbc:sqlserver://[serverName[\instanceName][:portNumber]][;property=value[;property=value]]
		String url = "jdbc:sqlserver://" + serverAddr + ";databaseName=" + dbName;
		
		HashMap<String, String> options = new HashMap<>();
		if (username != null) {
			options.put("user", username);
		}
		if (password != null) {
			options.put("password", password);
		}
		if (extraOptions != null) {
			options.putAll(extraOptions);
		}
		if (options.size() == 0)
			return url;
		
		String parameters = StringUtils.join(StringUtils.zip(options, "="), ";");
		url += ";" + parameters;
		return url;	
	}
	
	@Override
	public String makeWhereClause(Map<String, String> criteria)
	{
		if (criteria == null || criteria.size() == 0) {
			return null;
		}
		
		ArrayList<String> expressions = new ArrayList<>(criteria.size());
		for (Map.Entry<String, String> entry: criteria.entrySet()) {
			String col = entry.getKey();
			String value = entry.getValue();
			String[] splitted = value.split(",");
			if (splitted.length == 1) {
				expressions.add("CAST(" + col + " AS CHAR) = '" + value + "'");
			}
			else {
				String escapedValues = StringUtils.join(splitted, ", ", "'", "'");
				expressions.add("CAST(" + col + " AS CHAR) IN (" + escapedValues + ")");
			}
		}
		return StringUtils.join(expressions, " AND ");
		
	}
	
	@Override
	public String makeSqlSelect(String tblName, String[] selects,
			Map<String, String> criteria, int limit)
	{
		String select = (selects == null) ? "*" : StringUtils.join(selects, ", ");
		
		final String whereClause = makeWhereClause(criteria);
		
		String sql;
		if (limit > 0) {
			sql = "SELECT TOP " + limit + " "+ select + " FROM " + tblName;
		}
		else {
			sql = "SELECT "+ select + " FROM " + tblName;
		}
		if (whereClause != null) {
			sql += " WHERE " + whereClause;
		}
		
		return sql;
	}

	@Override
	public String makeNoopSelectStatement(String tblName)
	{
		return "SELECT TOP 0 * FROM " + tblName;
	}

	@Override
	public String makeDescribeStatement(String tblName)
	{
		return "EXEC SP_COLUMNS " + tblName;
	}

	@Override
	public String makePreparedInsertStatement(String tblName, int nCols, OnDuplicate ins)
	{
		if (ins != OnDuplicate.THROW_ERROR) {
			throw new UnsupportedOperationException(ins + " is not supported in mssql insertion");
		}
		
		return "INSERT INTO " + tblName + " VALUES (" 
			    + StringUtils.join("?", ", ", nCols) + ")";
	}
	
	@Override
	public String makePreparedInsertStatement(String tblName, String[] cols, OnDuplicate ins)
	{
		if (ins != OnDuplicate.THROW_ERROR) {
			throw new UnsupportedOperationException(ins + " is not supported in mssql insertion");
		}
		
		int nCols = cols.length;
		return "INSERT INTO " + tblName + "(" + StringUtils.join(cols, ", ")  
				+ ") VALUES (" + StringUtils.join("?", ", ", nCols) + ")";
	}

	@Override
	public String makeDeleteStatement(String tblName)
	{
		return "DELETE FROM " + tblName;
	}

	@Override
	public String makeDeleteStatement(String tblName, Map<String, String> criteria)
	{
		final String whereClause = makeWhereClause(criteria);
		return "DELETE FROM " + tblName + " WHERE " + whereClause;
	}

	@Override
	public Integer suggestFetchSize()
	{
		// http://technet.microsoft.com/en-us/library/ms378124.aspx
		return null;
	}
}
