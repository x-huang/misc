package xw.data.stream.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import xw.data.common.uri.DatabaseUriParser;
import xw.data.common.uri.GeneralUriQuery;
import xw.data.stream.DataInputStream;
import xw.data.stream.DataOutputStream;
import xw.data.stream.IResourceOperator;
import xw.data.stream.jdbc.JdbcHelper.OnDuplicate;

public class JdbcResourceOperator implements IResourceOperator
{
	@Override
	public Object describeResource(URI uri) 
			throws URISyntaxException, IOException
	{
		return JdbcCommon.getTableRepr(uri);
	}

	@Override
	public InputStream createInputStream(URI uri) 
			throws URISyntaxException, IOException
	{
		final DatabaseUriParser parser = new DatabaseUriParser(uri);
		final GeneralUriQuery query = parser.getQuery();
		
		final JdbcDataInput jdi = new JdbcDataInput(
				parser.getScheme(),
				parser.getServerAddress(),
				parser.getDatabase(),
				parser.getTable(),
				parser.getUser(),
				parser.getPassword()
			);
		
		jdi.setLimit(query.getParameterAsInteger("limit"));
		
		String selects = query.getParameterAsString("select");
		if (selects != null) {
			jdi.setSelects(selects.split(","));
		}
		
		String sql = query.getParameterAsString("sql");
		if (sql != null) {
			jdi.setCustomSelectStatement(sql);
		}
		
		String format = query.getParameterAsString("format");
		
		query.removeParameter("limit")
			 .removeParameter("format")
			 .removeParameter("select")
			 .removeParameter("sql");
		
		jdi.setFilter(query.getAllParameters());
		
		try {
			jdi.initialize();
		}
		catch (ClassNotFoundException | SQLException e) {
			jdi.close();
			throw new IOException("error initializing jdbc database input stream", e);
		}
	
		return new DataInputStream(jdi, format);
	}

	@Override
	public OutputStream createOutputStream(URI uri) throws URISyntaxException,
			IOException
	{
		final DatabaseUriParser parser = new DatabaseUriParser(uri);
		final GeneralUriQuery query = parser.getQuery();
		
		final JdbcDataOutput jdo = new JdbcDataOutput(
				parser.getScheme(),
				parser.getServerAddress(),
				parser.getDatabase(),
				parser.getTable(),
				parser.getUser(),
				parser.getPassword()
			);

		final String selects = query.getParameterAsString("select");
		if (selects != null)
			jdo.setSelects(selects.split(","));
		
		final String format = query.getParameterAsString("format");	
		final Integer batchSize = query.getParameterAsInteger("batchsize");
		final OnDuplicate insertMethod = OnDuplicate.fromString(query.getParameterAsString("onduplicate"));
		
		query.removeParameter("format")
		 	 .removeParameter("select")
		 	 .removeParameter("onduplicate")
		 	 .removeParameter("batchsize");
		
		jdo.setInsertMethod(insertMethod);
		
		if (batchSize != null) {
			jdo.setUpdateBatchSize(batchSize);
		}
		
		jdo.setPurgeTable(query.getParameterAsYes("overwrite"));
		final List<String> overwrites = query.getPrefixedParameterNames("overwrite.");
		final Map<String, String> spec = new HashMap<>();
		for (String overwrite: overwrites) {
			String col = overwrite.substring("overwrite.".length());
			spec.put(col, query.getParameterAsString(overwrite));
		}
		jdo.setPurgeRows(spec);
		
		try {
			jdo.initialize();
		}
		catch (ClassNotFoundException | SQLException e) {
			jdo.close();
			throw new IOException("error initializing jdbc database output stream", e);
		}
		
		return new DataOutputStream(jdo, format);
	}
}
