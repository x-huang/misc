package xw.data.stream.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import xw.data.common.lang.StringUtils;

public class MysqlJdbcHelper implements JdbcHelper
{

	@Override
	public String getDriver()
	{
		return "com.mysql.jdbc.Driver";
	}

	@Override
	public String makeUrl(String serverAddr, String dbName, String username,
						  String password, Map<String, String> extraOptions)
	{
		// http://dev.mysql.com/doc/refman/5.0/en/connector-j-reference-configuration-properties.html
		// example: jdbc:mysql://[hostname][:port]/dbname[?param1=value1][&param2=value2]..
		String url = "jdbc:mysql://" + serverAddr + "/" + dbName;

		HashMap<String, String> options = new HashMap<>();
		if (username != null) {
			options.put("user", username);
		}
		if (password != null) {
			options.put("password", password);
		}
		if (extraOptions != null) {
			options.putAll(extraOptions);
		}
		if (options.size() == 0)
			return url;
		
		String parameters = StringUtils.join(StringUtils.zip(options, "="), "&");
		url += "?" + parameters;
		return url;
	}
	
	@Override
	public String makeWhereClause(Map<String, String> criteria)
	{
		if (criteria == null || criteria.size() == 0) {
			return null;
		}
		
		ArrayList<String> expressions = new ArrayList<>(criteria.size());
		for (Map.Entry<String, String> entry: criteria.entrySet()) {
			String col = entry.getKey();
			String value = entry.getValue();
			String[] splitted = value.split(",");
			if (splitted.length == 1) {
				expressions.add("CONVERT(" + col + ", CHAR) = \"" + value + "\"");
			}
			else {
				String escapedValues = StringUtils.join(splitted, ", ", "\"", "\"");
				expressions.add("CONVERT(" + col + ", CHAR) IN (" + escapedValues + ")");
			}
		}
		return StringUtils.join(expressions, " AND ");
	}
	
	@Override
	public String makeSqlSelect(String tblName, String[] selects,
									  Map<String, String> criteria, int limit)
	{
		final String select = (selects == null) ? "*" : StringUtils.join(selects, ", ");
		final String whereClause = makeWhereClause(criteria);
		
		String sql = "SELECT " + select + " FROM " + tblName;
		if (whereClause != null) {
			sql += " WHERE " + whereClause;
		}
		if (limit > 0) {
			sql += " LIMIT 0, " + limit;
		}
		return sql;
	}
	
	@Override
	public String makeNoopSelectStatement(String tblName)
	{
		return "SELECT * FROM " + tblName + " LIMIT 0, 0";
	}

	@Override
	public String makeDescribeStatement(String tblName)
	{
		return "DESCRIBE " + tblName;
	}
	
	private static String mysql_insert(OnDuplicate ins) 
	{
		if (ins == OnDuplicate.THROW_ERROR) 
			return "INSERT";
		else if (ins == OnDuplicate.IGNORE)
			return "INSERT IGNORE";
		else
			return "REPLACE";
	}

	@Override
	public String makePreparedInsertStatement(String tblName, int nCols, OnDuplicate ins)
	{
		return mysql_insert(ins) + " INTO " + tblName + " VALUES (" 
			    + StringUtils.join("?", ", ", nCols) + ")";
	}

	@Override
	public String makePreparedInsertStatement(String tblName, String[] cols, OnDuplicate ins)
	{
		int nCols = cols.length;
		return mysql_insert(ins) + " INTO " + tblName + "(" + StringUtils.join(cols, ", ")  
				+ ") VALUES (" + StringUtils.join("?", ", ", nCols) + ")";
	}
	
	@Override
	public String makeDeleteStatement(String tblName)
	{
		return "DELETE FROM " + tblName;
	}

	@Override
	public String makeDeleteStatement(String tblName, Map<String, String> criteria)
	{
		final String whereClause = makeWhereClause(criteria);
		return "DELETE FROM " + tblName + " WHERE " + whereClause;
	}

	@Override
	public Integer suggestFetchSize()
	{
		// http://benjchristensen.com/2008/05/27/mysql-jdbc-memory-usage-on-large-resultset/
		return Integer.MIN_VALUE;
	}
}
