package xw.data.stream.jdbc;

import xw.data.common.lang.Registry;

import java.io.PrintStream;

public class JdbcHelpers
{
    static private final Registry<JdbcHelper> _jdbcHelpers = new Registry<>();
    static {
        _jdbcHelpers.register(new MysqlJdbcHelper(), "mysql")
                    .register(new MssqlJdbcHelper(), "mssql")
                    .register(new TeradataJdbcHelper(), "teradata");
    }

	static public JdbcHelper getHelper(String scheme)
	{
		JdbcHelper helper = _jdbcHelpers.get(scheme);

        if (helper == null) {
            throw new IllegalArgumentException("no such jdbc scheme: " + scheme);
        }
		return helper;
	}

    public static void dump(PrintStream ps)
    {
        _jdbcHelpers.dump(ps);
    }

    public static void registerJdbcHelper(JdbcHelper helper, String name)
    {
        _jdbcHelpers.register(helper, name);
    }
}
