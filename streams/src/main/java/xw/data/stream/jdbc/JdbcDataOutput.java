package xw.data.stream.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.stream.ObjectOutput;
import xw.data.stream.jdbc.JdbcHelper.OnDuplicate;
import xw.data.common.sedes.IteratorUtils;

public class JdbcDataOutput implements ObjectOutput
{
    static final Logger log = LoggerFactory.getLogger(JdbcDataOutput.class);

	protected final String _scheme;
	protected final String _server;
	protected final String _dbName;
	protected final String _tblName;
	protected final String _user;
	protected final String _password;
	protected String[] _selects;
	protected OnDuplicate _insertMethod;
	protected boolean _purgeTable;
	protected Map<String, String> _purgeRowsSpec;
	protected int _batchSize;
	private int _batchCount;
	private int _count;
	private int[] _sqlTypes;
	private Connection _conn;
	private PreparedStatement _stmt;

	public JdbcDataOutput(String scheme, String server, 
						   String dbName, String tblName,
						   String user, String password)
	{
		_scheme = scheme;
		_server = server;
		_dbName = dbName;
		_tblName = tblName;
		_user = user;
		_password = password;
		_selects = null;
		_insertMethod = OnDuplicate.THROW_ERROR;
		_purgeTable = false;
		_purgeRowsSpec = new HashMap<>();
		_batchSize = 1000;
		_batchCount = 0;
		_sqlTypes = null;
		_count = 0;
		_conn = null;
		_stmt = null;
	}
	
	public JdbcDataOutput setSelects(String[] selects)
	{
		_selects = (selects == null? null : selects.clone());
		return this;
	}
	
	public JdbcDataOutput setUpdateBatchSize(int size)
	{
		_batchSize = size;
		return this;
	}
	
	public JdbcDataOutput setPurgeTable(boolean purge)
	{
		_purgeTable = purge;
		return this;
	}
	
	public JdbcDataOutput setInsertMethod(OnDuplicate ins)
	{
		_insertMethod = ins;
		return this;
	}
	
	public JdbcDataOutput setPurgeRows(Map<String, String> spec)
	{
		_purgeRowsSpec.clear();
		_purgeRowsSpec.putAll(spec);
		return this;
	}
	
	public void initialize() throws ClassNotFoundException, SQLException
	{
		final JdbcHelper helper = JdbcHelpers.getHelper(_scheme);
		
		String driver = helper.getDriver();
		Class.forName(driver);
		
		final String jdbcUrl = helper.makeUrl(_server, _dbName, _user, _password, null);
		log.debug("using jdbc url: {}", jdbcUrl);

		_conn = DriverManager.getConnection(jdbcUrl);
		
		final String sqlDelete;
		if (_purgeTable) {
			sqlDelete = helper.makeDeleteStatement(_tblName);
		}
		else if (_purgeRowsSpec.size() > 0) {
			sqlDelete = helper.makeDeleteStatement(_tblName, _purgeRowsSpec);
		}
		else {
			sqlDelete = null;
		}

		if (sqlDelete != null) {
			log.debug("executing sql: {}", sqlDelete);
			Statement stmt = _conn.createStatement();
			stmt.executeUpdate(sqlDelete);
		}
		
		final String sqlNoop = helper.makeNoopSelectStatement(_tblName);
		log.debug("executing sql: {}", sqlNoop);
        Statement stmt = _conn.createStatement();
		ResultSet rs = stmt.executeQuery(sqlNoop);
		final ResultSetMetaData metadata = rs.getMetaData();
		final int nCols = metadata.getColumnCount();
		final String sqlInsert; 
		if (_selects == null) {
			_sqlTypes = new int[nCols];
			for (int i=0; i<nCols; i++) {
				if (metadata.isAutoIncrement(i+1)) {
					String colName = metadata.getColumnName(i+1);
					throw new SQLException("cannot insert into auto increment column: " + colName);
				}
				_sqlTypes[i] = metadata.getColumnType(i+1);
			}
			sqlInsert = helper.makePreparedInsertStatement(_tblName, nCols, _insertMethod);
		}
		else {
			final Map<String, Integer> columnNameToIndexMap = new HashMap<>();
			for (int i=1; i<=nCols; i++) {
				columnNameToIndexMap.put(metadata.getColumnName(i).toLowerCase(), i);
			}
			_sqlTypes = new int[_selects.length];
			for (int i=0; i<_sqlTypes.length; i++) {
				String colName = _selects[i];
				String lowercase = colName.toLowerCase();
				if (lowercase.startsWith("`") && lowercase.endsWith("`")) {
					lowercase = lowercase.substring(1, lowercase.length()-1);
				}
				if (!columnNameToIndexMap.containsKey(lowercase)) { 
					throw new SQLException("column not found in specified insert statement: " + colName);
				}
				int index = columnNameToIndexMap.get(lowercase);
				if (metadata.isAutoIncrement(index)) {
					throw new SQLException("cannot insert into auto increment column: " + colName);
				}
				_sqlTypes[i] = metadata.getColumnType(index);
			}
			sqlInsert = helper.makePreparedInsertStatement(_tblName, _selects, _insertMethod);
		}
		
		log.debug("prepared sql: {}", sqlInsert);
		_stmt = _conn.prepareStatement(sqlInsert);
		_conn.setAutoCommit(false);
	}
	
	private static void statementSetValue(PreparedStatement stmt, 
										  int index, int sqlType, Object value) 
			throws SQLException
	{
		if (value == null) {
			stmt.setNull(index, sqlType);
		}
		else {
			stmt.setObject(index, value, sqlType, 4);
		}
	}
	
	@Override
	public void write(Object o) throws IOException
	{
		Iterator<Object> iter = IteratorUtils.getUnifiedIterator(o);

		int index = 1;
		while (iter.hasNext()) {
			Object datum = iter.next();

			try {
				statementSetValue(_stmt, index, _sqlTypes[index-1], datum);
			}
			catch (SQLException e) {
				throw new IOException("sql error preparing statement", e);
			}
			index++;
		}
		
		try {
			_stmt.addBatch();
		}
		catch (SQLException e) {
			throw new IOException("sql error adding batch", e);
		}
		
		_batchCount++;
        final int writeCount = _count + _batchCount;
        if ((writeCount % 1_000_000) == 0) {
            log.trace("progress: wrote rows: {}, submitted: {}", writeCount, _count);
        }
		if (_batchCount == _batchSize) {
			try {
				_stmt.executeBatch();				
			}
			catch (SQLException e) {
				throw new IOException("sql error executing insert statement", e);
			}
			finally {
				try {
					_conn.commit();
				}
				catch (SQLException e) {
					throw new IOException("sql error executing insert statement", e);
				}
			}
			_count += _batchCount;
			_batchCount = 0;
		}
	}

	@Override
	public long getCount()
	{
		return _count;
	}

	@Override
	public void close() throws IOException
	{
		if (_batchCount > 0) {
			try {
				_stmt.executeBatch();
			}
			catch (SQLException e) {
				throw new IOException("sql error executing insert statemen", e);
			}
			finally {
				try {
					_conn.commit();
				}
				catch (SQLException e) {
					throw new IOException("sql error executing insert statement", e);
				}
			}
			_count += _batchCount;
			_batchCount = 0;
		}
		try {
			if (_conn != null) 
				_conn.close();
		}
		catch (SQLException e) {
			throw new IOException("sql error disconnecting", e);
		}
	}
}
