package xw.data.stream.jdbc;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.common.uri.DatabaseUriParser;

public class JdbcCommon
{
    static final Logger log = LoggerFactory.getLogger(JdbcCommon.class);

	public static Object getTableRepr(URI uri) 
			throws IOException, URISyntaxException
	{
		final DatabaseUriParser parser = DatabaseUriParser.get(uri);
		return getTableRepr(
				parser.getScheme(),
				parser.getServerAddress(),
				parser.getDatabase(),
				parser.getTable(),
				parser.getUser(),
				parser.getPassword()
			);
	}
	
	public static Object getTableRepr(String scheme, String server, 
									  String dbName, String tblName, 
									  String user, String password)
			throws IOException, URISyntaxException
	{	
		final JdbcHelper helper = JdbcHelpers.getHelper(scheme);
		
		String driver = helper.getDriver();
		try {
			Class.forName(driver);
		}
		catch (ClassNotFoundException e) {
			String msg = "jdbc driver not found: " + driver;
			throw new URISyntaxException(driver, msg);
		}
		
		final String jdbcUrl = helper.makeUrl(server, dbName, user, password, null);
		log.debug("using jdbc url: {}", jdbcUrl);

		final Connection conn;
		try {
			conn = DriverManager.getConnection(jdbcUrl);
		}
		catch (SQLException e) {
			String msg = "jdbc connection error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		final String sql = helper.makeNoopSelectStatement(tblName);
		log.debug("executing sql: {}", sql);

		final TableRepr schema;
		try {
			final Statement stmt = conn.createStatement();
			final ResultSet rs = stmt.executeQuery(sql);
			ResultSetMetaData md = rs.getMetaData();
			schema = new TableRepr(md);
			conn.close();
		}
		catch (SQLException e) {
			String msg = "jdbc error: " + e.getMessage();
			throw new IOException(msg, e);		
		}
		return schema;
		
	}
	
	public static int executeUpdate(URI uri, String sql) 
			throws IOException, URISyntaxException
	{
		return executeUpdate(DatabaseUriParser.get(uri), sql);
	}
	
	public static int executeUpdate(final DatabaseUriParser parser, String sql) 
			throws IOException, URISyntaxException 
	{
		// final DatabaseUriParser parser = getParsedDatabaseUri(uri);
		final JdbcHelper helper = JdbcHelpers.getHelper(parser.getScheme());
		
		String driver = helper.getDriver();
		try {
			Class.forName(driver);
		}
		catch (ClassNotFoundException e) {
			String msg = "jdbc driver not found: " + driver;
			throw new URISyntaxException(driver, msg);
		}
		
		final String jdbcUrl = helper.makeUrl(parser.getServerAddress(),
				parser.getDatabase(), parser.getUser(), parser.getPassword(), null);
        log.debug("using jdbc url: {}", jdbcUrl);

		final Connection conn;
		try {
			conn = DriverManager.getConnection(jdbcUrl);
		}
		catch (SQLException e) {
			String msg = "jdbc connection error: " + e.getMessage();
			throw new IOException(msg, e);
		}

        log.debug("executing sql: {}", sql);
		int rowCount;
		try {
			final Statement stmt = conn.createStatement();
			rowCount = stmt.executeUpdate(sql);
			conn.close();
		}
		catch (SQLException e) {
			String msg = "jdbc error: " + e.getMessage();
			throw new IOException(msg, e);		
		}
		return rowCount;
	}
}
