package xw.data.stream.jdbc;

import java.io.Serializable;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TableRepr implements Serializable
{
	private static final long serialVersionUID = -2088919924358382122L;

	public final List<ColumnRepr> columns;
		
	public TableRepr(ResultSetMetaData md) throws SQLException
	{
		columns = ColumnRepr.getColumnRepresentations(md);
	}

}
