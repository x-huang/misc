package xw.data.stream.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import xw.data.common.lang.StringUtils;

public class TeradataJdbcHelper implements JdbcHelper
{
	@Override
	public String getDriver()
	{
		return "com.teradata.jdbc.TeraDriver";
	}

	@Override
	public String makeUrl(String serverAddr, String dbName, String username,
			String password, Map<String, String> extraOptions)
	{
		// refer to: http://developer.teradata.com/doc/connectivity/jdbc/reference/current/jdbcug_chapter_2.html
		// example: jdbc:teradata://DatabaseServerName/ParameterName=Value,ParameterName=Value
		
		final String host;
		final String port;
		{
			String[] splitted = serverAddr.split(":", 2);
			host = splitted[0];
			port = splitted.length == 2? splitted[1]: null;
		}
		
		//String url = "jdbc:teradata://" + serverAddr;
		String url = "jdbc:teradata://" + host;

		HashMap<String, String> options = new HashMap<>();
		options.put("database", dbName);
	
		if (port != null) {
			options.put("dbs_port", port);
		}
		if (username != null) {
			options.put("user", username);
		}
		if (password != null) {
			options.put("password", password);
		}
		if (extraOptions != null) {
			options.putAll(extraOptions);
		}
		if (options.size() == 0)
			return url;
		
		String parameters = StringUtils.join(StringUtils.zip(options, "="), ",");
		url += "/" + parameters;
		return url;
		
	}

	@Override
	public String makeWhereClause(Map<String, String> criteria)
	{
		if (criteria == null || criteria.size() == 0) {
			return null;
		}
		
		ArrayList<String> expressions = new ArrayList<>(criteria.size());
		for (Map.Entry<String, String> entry: criteria.entrySet()) {
			String col = entry.getKey();
			String value = entry.getValue();
			String[] splitted = value.split(",");
			
			/* Note: 
			 * 	'CAST (col AS VARCHAR(num)' is equivalent to 'CONVERT(col, STRING)' 
			 * in mysql or mssql except one has to specify miximum size of the string. 
			 * Fortunately CAST'ed string can not automatically chomped.  
			 */
			if (splitted.length == 1) {
				expressions.add("CAST(" + col + " AS VARCHAR(20)) = '" + value + "'");
			}
			else {
				String escapedValues = StringUtils.join(splitted, ", ", "'", "'");
				expressions.add("CAST(" + col + " AS VARCHAR(20)) IN (" + escapedValues + ")");
			}
		}
		return StringUtils.join(expressions, " AND ");
	}
	
	@Override
	public String makeSqlSelect(String tblName, String[] selects,
			Map<String, String> criteria, int limit)
	{
		String select = (selects == null) ? "*" : StringUtils.join(selects, ", ");
		
		final String whereClause = makeWhereClause(criteria);
		
		String sql;
		if (limit > 0) {
			sql = "SELECT TOP " + limit + " "+ select + " FROM " + tblName;
		}
		else {
			sql = "SELECT "+ select + " FROM " + tblName;
		}
		if (whereClause != null) {
			sql += " WHERE " + whereClause;
		}
		
		return sql;
	}
	
	@Override
	public String makeNoopSelectStatement(String tblName)
	{
		return "SELECT * FROM " + tblName + " WHERE 0 = 1";
	}

	@Override
	public String makeDescribeStatement(String tblName)
	{
		return "HELP TABLE " + tblName;
	}

	@Override
	public String makePreparedInsertStatement(String tblName, int nCols, OnDuplicate ins)
	{
		if (ins != OnDuplicate.THROW_ERROR) {
			throw new UnsupportedOperationException(ins + " is not supported in teradata insertion");
		}
		return "INSERT INTO " + tblName + " VALUES (" 
			    + StringUtils.join("?", ", ", nCols) + ")";
	}
	
	@Override
	public String makePreparedInsertStatement(String tblName, String[] cols, OnDuplicate ins)
	{
		if (ins != OnDuplicate.THROW_ERROR) {
			throw new UnsupportedOperationException(ins + " is not supported in teradata insertion");
		}
		int nCols = cols.length;
		return "INSERT INTO " + tblName + "(" + StringUtils.join(cols, ", ")  
				+ ") VALUES (" + StringUtils.join("?", ", ", nCols) + ")";
	}
	
	@Override
	public String makeDeleteStatement(String tblName)
	{
		return "DELETE FROM " + tblName;
	}

	@Override
	public String makeDeleteStatement(String tblName, Map<String, String> criteria)
	{
		final String whereClause = makeWhereClause(criteria);
		return "DELETE FROM " + tblName + " WHERE " + whereClause;
	}

	@Override
	public Integer suggestFetchSize()
	{
		// http://developer.teradata.com/doc/connectivity/jdbc/reference/current/faq.html#q7
		return null;
	}
}
