package xw.data.stream.jdbc;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ColumnRepr implements Serializable
{
	private static final long serialVersionUID = -4953888950770492017L;

	public final String catalogName;
	public final String className;
	public final int displaySize;
	public final String label;
	public final String name;
	public final int type;
	public final String typeName;
	public final int precision;
	public final int scale;
	public final String schemaName;
	public final String tableName;
	public final boolean isAutoIncrement;
	public final boolean caseSensitive;
	public final boolean isCurrency;
	public final boolean definitelyWritable;
	public final String nullable;
	public final boolean readOnly;
	public final boolean searchable;
	public final boolean signed;
	public final boolean writable;
	
	static public String translateNullable(int nullable)
	{
		if (nullable == ResultSetMetaData.columnNoNulls) 
			return "columnNoNulls";
		else if (nullable == ResultSetMetaData.columnNullable)
			return "columnNullable";
		else if (nullable == ResultSetMetaData.columnNullableUnknown)
			return "columnNullableUnknown";
		else 
			throw new IllegalArgumentException("unknown nullable value" + nullable);
	}
	
	public ColumnRepr(ResultSetMetaData md, int column) 
			throws SQLException
	{
		catalogName = md.getCatalogName(column);
		className = md.getColumnClassName(column);
		displaySize = md.getColumnDisplaySize(column);
		label = md.getColumnLabel(column);
		name = md.getColumnName(column);
		type = md.getColumnType(column);
		typeName = md.getColumnTypeName(column);
		precision = md.getPrecision(column);
		scale = md.getScale(column);
		schemaName = md.getSchemaName(column);
		tableName = md.getTableName(column);
		isAutoIncrement = md.isAutoIncrement(column);
		caseSensitive = md.isCaseSensitive(column);
		isCurrency = md.isCurrency(column);
		definitelyWritable = md.isDefinitelyWritable(column);
		nullable = translateNullable(md.isNullable(column));
		readOnly = md.isReadOnly(column);
		searchable = md.isSearchable(column);
		signed = md.isSigned(column);
		writable = md.isWritable(column);
	}
	
	public static List<ColumnRepr> getColumnRepresentations(ResultSetMetaData md) 
			throws SQLException 
	{
		final int count = md.getColumnCount();
		ArrayList<ColumnRepr> columns = new ArrayList<ColumnRepr>(count);
		for (int i=1; i<=count; i++) {
			columns.add(new ColumnRepr(md, i));
		}
		return columns;
	}
}
