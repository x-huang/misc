package xw.data.stream;

import java.io.IOException;
import java.io.OutputStream;

public interface DataSink
{
	public OutputStream getOutputStream() throws IOException;
}
