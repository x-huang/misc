package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xw.data.common.lang.LoggerUtils;
import xw.data.common.uri.UriUtils;
import xw.data.stream.transform.InputStreamTransformer;
import xw.data.stream.transform.OutputStreamTransformer;
import xw.data.stream.transform.StreamTransformers;

public class DataStreams
{

    static final Logger log = LoggerFactory.getLogger(DataStreams.class);

	static {
		LoggerUtils.disableCertainLoggers();
	}

	private static IResourceOperator getResourceOperator(String scheme)
	{
		Class<? extends IResourceOperator> clz 
				= Protocols.getOperatorClass(scheme.toLowerCase());
		if (clz == null) {
			throw new IllegalArgumentException("unknown uri scheme: " + scheme);
		}
		
		try {
			return clz.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			String errorMsg = "cannot instantiate class: " + clz.getSimpleName();
			throw new RuntimeException(errorMsg, e);
		}
	}

    private static InputStream applyInputStreamTransformers(final InputStream in, String[] names)
            throws IOException
    {
        InputStream iterIn = in;
        for (String name: names) {
            InputStreamTransformer t = StreamTransformers.getInputStreamTransformer(name);
            iterIn = t.transform(iterIn);
            log.debug("applied input stream transformer: {}", name);
        }
        return iterIn;
    }

    private static OutputStream applyOutputStreamTransformers(final OutputStream out, String[] names)
            throws IOException
    {
        OutputStream iterOut = out;
        for (String name: names) {
            OutputStreamTransformer t = StreamTransformers.getOutputStreamTransformer(name);
            iterOut = t.transform(iterOut);
            log.debug("applied output stream transformer: {}", name);
        }
        return iterOut;
    }

	public static InputStream getInputStream(final String rawUri, String... transformers)
			throws IOException, URISyntaxException
	{
		URI uri = new URI(UriUtils.encode(rawUri));

		String scheme = uri.getScheme();
		if (scheme == null || scheme.isEmpty()) {
			throw new IllegalArgumentException("empty uri scheme: " + rawUri);
		}

        log.debug("raw uri: {}, encoded: {}", rawUri, uri.toString());

		final InputStream in = getResourceOperator(scheme).createInputStream(uri);
        log.debug("opened input stream of class: " + in.getClass().getName());

        return applyInputStreamTransformers(in, transformers);
	}
	
	public static OutputStream getOutputStream(final String rawUri, String... transformers)
			throws IOException, URISyntaxException
	{
		URI uri = new URI(UriUtils.encode(rawUri));
		
		String scheme = uri.getScheme();
		if (scheme == null || scheme.isEmpty()) {
			throw new IllegalArgumentException("empty uri scheme: " + rawUri);
		}

        log.debug("raw uri: {}, encoded: {}", rawUri, uri.toString());
		
		final OutputStream out = getResourceOperator(scheme).createOutputStream(uri);
        log.debug("opened output stream of class: {}", out.getClass().getName());
		return applyOutputStreamTransformers(out, transformers);
	}
	
	public static Object describeResource(final String rawUri)
			throws IOException, URISyntaxException
	{
		URI uri = new URI(UriUtils.encode(rawUri));
        log.debug("raw uri: {}, encoded: {}", rawUri, uri.toString());

        String scheme = uri.getScheme();
		if (scheme == null || scheme.isEmpty()) {
			throw new IllegalArgumentException("empty uri scheme: " + rawUri);
		}

        Object descr = getResourceOperator(scheme).describeResource(uri);
        log.debug("got resource description of class: {}", descr.getClass().getName());
        return descr;
	}
}
