package xw.data.stream;

import java.io.IOException;
import java.io.OutputStream;

public class BroadcastingOutputStreams extends OutputStream
{
	final private OutputStream[] _outs;
	
	public BroadcastingOutputStreams(OutputStream[] outs)
	{
		_outs = new OutputStream[outs.length];
		for (int i=0; i<outs.length; i++) {
			_outs[i] = outs[i];
		}
	}

	@Override
	public void write(int b) throws IOException
	{
		for (int i=0; i<_outs.length; i++) {
			_outs[i].write(b);
		}
		// What if one of the output stream is broken? 
		// It is just hard to make broadcast-write action transactional. 
	}

	@Override
	public void close() throws IOException
	{
		IOException first = null;
		
		try {
			for (int i=0; i<_outs.length; i++) {
				_outs[i].close();
			}
		}
		catch (IOException e) {
			if (first == null)
				first = e;
		}
		
		if (first != null)
			throw first;
	}
}
