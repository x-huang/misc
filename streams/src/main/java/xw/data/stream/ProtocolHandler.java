package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class ProtocolHandler extends URLStreamHandler
{
	public static final class ResourceConnection extends URLConnection 
	{
		protected ResourceConnection(URL url)
		{
			super(url);
			super.allowUserInteraction = false;
			super.useCaches = false;
			super.connected = true;
		}

		@Override
		public void connect() throws IOException
		{
			super.connected = true;
		}
		
		@Override
		public InputStream getInputStream() throws IOException
		{
			if (!super.connected) {
				throw new IOException("not connected yet");
			}
			
			String uri = getURL().toString();
			try {
				return DataStreams.getInputStream(uri);
			}
			catch (URISyntaxException e) {
				throw new IOException("invalid url: " + uri, e);
			}
		}
		
		@Override
		public OutputStream getOutputStream() throws IOException
		{
			if (!super.connected) {
				throw new IOException("not connected yet");
			}
			
			String uri = getURL().toString();
			try {
				return DataStreams.getOutputStream(uri);
			}
			catch (URISyntaxException e) {
				throw new IOException("invalid url: " + uri, e);
			}
		}
		
		@Override
		public Object getContent() throws IOException
		{
			throw new IOException("reading resource content as a whole disallowed");
		}

		/*
		 * Xiaowan: this is a tweaked implementation to return a description of resource.
		 *   if getContent() is taking anything as a parameter, then we consider it is 
		 *   a request to 'stat' a  resource.
		 */
		@Override
		@SuppressWarnings("rawtypes") 
		public Object getContent(Class[] classes) throws IOException
		{
			assert classes.length == 1;
			assert classes[0].equals(Object.class);
			
			String uri = getURL().toString();
			try {
				return DataStreams.describeResource(uri);
			}
			catch (URISyntaxException e) {
				throw new IOException("invalid url: " + uri, e);
			}
		}
	}
	
	@Override
	protected URLConnection openConnection(URL url) throws IOException
	{
		return new ResourceConnection(url);
	}
	
	public static URLConnection openUriAsConnection(URL url) throws IOException
	{
		return new ResourceConnection(url);
	}
}
