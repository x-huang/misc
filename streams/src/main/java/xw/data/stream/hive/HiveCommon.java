package xw.data.stream.hive;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.metastore.api.Database;
import org.apache.hadoop.hive.ql.metadata.Hive;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.metadata.Partition;
import org.apache.hadoop.hive.ql.metadata.Table;

import static org.apache.hadoop.hive.metastore.MetaStoreUtils.DEFAULT_DATABASE_NAME;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.common.uri.DatabaseUriParser;

public class HiveCommon
{
    static final Logger log = LoggerFactory.getLogger(HiveCommon.class);

	static private HiveConf _conf = null;
	static final public String HIVE_CONF_PRIFIX = "HIVE_CONF.";
	
	protected static Hive getHive() throws HiveException 
	{ 
		if (_conf == null) {
			// _conf = new HiveConf();
			_conf = new HiveConf(Hive.class); 	// Xiaowan: pending change. This change eliminates 
				// the error that Hive API relies on hive-metastore jar, which is problematic. 
				// It is needed when building assembly:single.
				// Reported by Biao.
			
			// _conf.setBoolVar(HiveConf.ConfVars.METASTORE_MODE, true);  // for hive 0.9.0
			_conf.setVar(HiveConf.ConfVars.METASTOREURIS, "");	// for hive 0.10.0 or above
			
			final Properties props = System.getProperties();
			for (String prop: props.stringPropertyNames()) {
				if (!prop.startsWith(HIVE_CONF_PRIFIX)) 
					continue;
				String key = prop.substring(HIVE_CONF_PRIFIX.length());
				String value = props.getProperty(prop);
				_conf.set(key, value);
                log.info("set hive configuration: {}={}", key, value);
			}
		}	
		
		return Hive.get(_conf);
	
	//	Hive hive = Hive.get(new HiveConf());
	//	return hive;
	}
	
	public static Object getDatabaseRepr(URI uri)
			throws IOException, URISyntaxException
	{
		DatabaseUriParser parser = DatabaseUriParser.get(uri);
		return getDatabaseRepr(parser.getDatabase());
	}
	
	public static Object getDatabaseRepr(String dbName) 
			throws IOException, URISyntaxException
	{
		if (dbName == null || dbName.equals(""))
			dbName = DEFAULT_DATABASE_NAME;
		
		final Database db;
		try {
			Hive hive = getHive();
			db = hive.getDatabase(dbName);
		}
		catch (HiveException e) {
			String msg = "hive metastore error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		if (db == null) {
			String msg = "hive database '" + dbName + "' does not exist";
			throw new URISyntaxException(dbName, msg);
		}
			
		return new DatabaseRepr(db);
	}
	
	public static List<Object> getTableReprList(URI uri)
			throws IOException, URISyntaxException
	{
		DatabaseUriParser parser = DatabaseUriParser.get(uri);
		return getTableReprList(parser.getDatabase());
	}
	
	public static List<Object> getTableReprList(String dbName)
			throws IOException, URISyntaxException
	{
		ArrayList<Object> tables = new ArrayList<Object>();
		
		try {
			Hive hive = getHive();
			if (dbName != null && !dbName.equals(""))
				hive.setCurrentDatabase(dbName);
			for (String tblName: hive.getAllTables()) {
				Table t = hive.getTable(dbName, tblName, true);
				TableRepr repr = new TableRepr(t);
				tables.add(repr);
			}
		}
		catch (HiveException e) {
			String msg = "hive metastore error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		return tables;
	}
	
	public static Object getTableRepr(URI uri)
			throws IOException, URISyntaxException
	{
		DatabaseUriParser parser = DatabaseUriParser.get(uri);
		return getTableRepr(parser.getDatabase(), parser.getTable());
	}
	
	public static Object getTableRepr(String dbName, String tblName)
			throws IOException, URISyntaxException
	{
		if (dbName == null || dbName.equals(""))
			dbName = DEFAULT_DATABASE_NAME;
		
		final Table tbl;
		try {
			Hive hive = getHive();
			tbl = hive.getTable(dbName, tblName);
		}
		catch (HiveException e) {
			String msg = "hive metastore error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		if (tbl == null) {
			String fullName = dbName + "." + tblName;
			String msg = "hive tabe '" + fullName + "' does not exist";
			throw new URISyntaxException(fullName, msg);
		}
		
		return new TableRepr(tbl);
	}
	
	public static List<Object> getPartitionReprList(URI uri)
			throws IOException, URISyntaxException
	{
		DatabaseUriParser parser = DatabaseUriParser.get(uri);
		Map<String, String> spec = parser.getQuery().getAllParameters();
		return getPartitionReprList(parser.getDatabase(), parser.getTable(), spec);
	}
	
	public static List<Object> getPartitionReprList(String dbName, String tblName, 
													Map<String, String> spec)
			throws IOException, URISyntaxException
	{
		if (dbName == null || dbName.equals(""))
			dbName = DEFAULT_DATABASE_NAME;
		
		final List<Partition> ptns;
		try {
			Hive hive = getHive();
			Table tbl = hive.getTable(dbName, tblName);
			if (tbl == null) {
				String fullName = dbName + "." + tblName;
				String msg = "hive tabe '" + fullName + "' does not exist";	
				throw new URISyntaxException(fullName, msg);
			}
			ptns = hive.getPartitions(tbl, spec);
		}
		catch (HiveException e) {
			String msg = "hive metastore error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		ArrayList<Object> list = new ArrayList<Object>();
		for (Partition p: ptns) {
			list.add(new PartitionRepr(p));
		}
		return list;
	}
	
	public static Object getPartitionRepr(URI uri)
			throws IOException, URISyntaxException
	{
		DatabaseUriParser parser = DatabaseUriParser.get(uri);
		Map<String, String> spec = parser.getQuery().getAllParameters();
		return getPartitionRepr(parser.getDatabase(), parser.getTable(), spec);
	}
	
	public static Object getPartitionRepr(String dbName, String tblName, 
										  Map<String, String> spec)
			throws IOException, URISyntaxException
	{
		final Partition partition;
		try {
			Hive hive = getHive();
			Table tbl = (dbName == null || dbName.isEmpty()) ? 
						hive.getTable(tblName) : hive.getTable(dbName, tblName);
			if (tbl == null) {
				String fullName = dbName + "." + tblName;
				String msg = "hive table '" + fullName + "' does not exist";
				throw new URISyntaxException(fullName, msg);
			}
			partition = hive.getPartition(tbl, spec, false);
		}
		catch (HiveException e) {
			String msg = "hive metastore error: " + e.getMessage();
			throw new IOException(msg, e);
		}
		
		if (partition == null) {
			String fullName = dbName + "." + tblName;
			String msg = "no such partition in table '" + fullName;
			throw new URISyntaxException(fullName, msg);
		}
		return new PartitionRepr(partition);
	} 
}
