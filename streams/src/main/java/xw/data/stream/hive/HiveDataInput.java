package xw.data.stream.hive;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hive.metastore.api.FieldSchema;
import org.apache.hadoop.hive.ql.exec.ExecDriver;
import org.apache.hadoop.hive.ql.exec.FetchOperator;
import org.apache.hadoop.hive.ql.exec.Utilities;
import org.apache.hadoop.hive.ql.metadata.Hive;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.metadata.Partition;
import org.apache.hadoop.hive.ql.metadata.Table;
import org.apache.hadoop.hive.ql.plan.FetchWork;
import org.apache.hadoop.hive.ql.plan.PartitionDesc;
import org.apache.hadoop.hive.ql.plan.TableDesc;
import org.apache.hadoop.hive.serde2.objectinspector.InspectableObject;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorUtils;
import org.apache.hadoop.mapred.JobConf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.common.lang.StringUtils;
import xw.data.stream.ObjectInput;

public class HiveDataInput implements ObjectInput
{
    static final Logger log = LoggerFactory.getLogger(HiveDataInput.class);

	static class Selector {
		/* usage: 
		 	java xw.data.transport.cli.Dump \
		 	'hive:///meta_hd/skus?select=<seqno_1>,sid,msid,<\N>,<12>,<"abc">,<timestamp>,<date>,<datetime>&limit=3'
		    [0,1,546,null,12,"abc",1386282265034,"2013-12-05","2013-12-05 22:24:25"]
			[1,2,541,null,12,"abc",1386282265034,"2013-12-05","2013-12-05 22:24:25"]
			[2,3,545,null,12,"abc",1386282265034,"2013-12-05","2013-12-05 22:24:25"]
		*/
		abstract class Term {
			abstract Object readValue(List<Object> row);
			abstract Object readValue(Object[] row);
		}
		
		class SeqNumber extends Term {
			private long _seqno;
			public SeqNumber(int seqno) { _seqno = seqno; }
			@Override Object readValue(List<Object> row) { return _seqno++; }
			@Override Object readValue(Object[] row) { return _seqno++; }
		}
		
		class Constant extends Term {
			private final Object _val;
			public Constant(String repr) {
				if (repr.equals("null") || repr.equals("\\N")) { 
					_val = null; 
				}
				else if (repr.equals("timestamp")) {
					_val = System.currentTimeMillis();
				}
				else if (repr.equals("date")) {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					_val = df.format(new Date());
				}
				else if (repr.equals("datetime")) {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					_val = df.format(new Date());
				}
				else if (repr.charAt(0) == '"' && repr.charAt(repr.length()-1) == '"') {
					_val = repr.substring(1, repr.length()-1);
				}
				else {
					try {
						_val = Long.parseLong(repr);
					}
					catch (NumberFormatException e) {
						throw new IllegalArgumentException("<" + repr + "> is not a valid constant");
					}
				}
			}
			@Override Object readValue(List<Object> row) { return _val; }
			@Override Object readValue(Object[] row) { return _val; }
		}
		
		class ColumnValue extends Term {
			private final int _index;
			public ColumnValue(int idx) { _index = idx; }
			@Override Object readValue(List<Object> row) { return row.get(_index); }
			@Override Object readValue(Object[] row) { return row[_index]; }
		}
		
		// final protected int[] _selects;
		final private Term[] _terms; 
		
		private static int findColumnIndex(List<FieldSchema> cols, String colName)
		{
			int index = 0;
			for (FieldSchema fs: cols) {
				String name = fs.getName();
				if (colName.equalsIgnoreCase(name)) {
					return index;
				}
				index++;
			}
			return -1;
		}
		
		private static String extractConstant(String repr)
		{
			if (repr.charAt(0) != '<')
				return null;
			if (repr.charAt(repr.length()-1) != '>')
				return null;
			return repr.substring(1, repr.length()-1);
		}
		
		public Selector(Table table, String[] selects) 
		{
			if (selects == null || selects.length == 0) {
				_terms = null;
			}
			else {
				// cannot use table.getAllCols() because it put partCols in front of cols
				ArrayList<FieldSchema> allCols = new ArrayList<>();
				allCols.addAll(table.getCols());
				allCols.addAll(table.getPartCols());
				
				_terms = new Term[selects.length];
				for (int i=0; i<selects.length; i++) {
					final String term = selects[i];
					String  cnst = extractConstant(term);
					if (cnst != null) {
						if (cnst.equalsIgnoreCase("seqno_0"))
							_terms[i] = new SeqNumber(0);
						else if (cnst.equalsIgnoreCase("seqno_1"))
							_terms[i] = new SeqNumber(1);
						else
							_terms[i] = new Constant(cnst);
					}
					else {
						int index = findColumnIndex(allCols, term);
						if (index < 0) {
							throw new IllegalArgumentException(term + " is neither a constant nor a table column");
						}
						else {
							_terms[i] = new ColumnValue(index);
						}
					}
				}
			}
		}
		
		public Object select(Object data) 
		{
			if (_terms == null) 
				return data;
			
			if (data.getClass().isArray()) {
				Object[] array = (Object[]) data;
				Object[] selected = new Object[_terms.length];
				for (int i=0; i<selected.length; i++) {
					selected[i] = _terms[i].readValue(array);
				}
				return selected;
			}
			else if (ArrayList.class.isAssignableFrom(data.getClass())) {
				@SuppressWarnings("unchecked")
				ArrayList<Object> al = (ArrayList<Object>) data;
				Object[] selected = new Object[_terms.length];
				for (int i=0; i<selected.length; i++) {
					selected[i] = _terms[i].readValue(al);
				}
				return selected;
			}
			else {
				throw new IllegalArgumentException("unknown object type: "  
					+ data.getClass().getCanonicalName());
			}
		}
	}
	
	static class Filter 
	{
		final protected Object[][] _filterSetting;
		
		public Filter(Table table, Map<String, String> spec)
		{
			List<FieldSchema> cols = table.getCols(); 
			Object[][] filter = new Object[cols.size()][];
			
			int index = 0;
			int nCount = 0;
			for (FieldSchema fs: cols) {
				String col = fs.getName();
				String equalsTo = spec.get(col);
				if (equalsTo != null) {
					/* Note: it turns out fs.getType() always returns 'string' disregard its
					   real data type is string, bigint, or double. It is probably related to
					   data storage format. Might be a lurking bug. -- Xiaowan
					   *** 
					   Now I see it as a headache for which I have to compare datum as Strings
					   
					String type = fs.getType();
					System.out.println("INFO: column type: " + type);*/
					filter[index] = equalsTo.split(",");	// FIXME
					nCount++;
				}
				else {
					filter[index] = null;
				}
				index++;
			}
			
			_filterSetting = (nCount == 0)? null : filter;
		}
		
		public boolean isEmpty() 
		{
			return _filterSetting == null;
		}
		
		public Object filter(Object data) 
		{
			if (_filterSetting == null)
				return data;
			
			if (data.getClass().isArray()) {
				Object[] array = (Object[]) data;
				for (int i=0; i<_filterSetting.length; i++) {
					Object[] equalsTo = _filterSetting[i];
					// if (equalsTo != null && !array[i].equals(equalsTo))
					if (equalsTo != null && !StringUtils.findInArray(array[i].toString(), equalsTo))
						return null;
				}
				return data;
			}
			else if (ArrayList.class.isAssignableFrom(data.getClass())) {
				@SuppressWarnings("unchecked")
				ArrayList<Object> al = (ArrayList<Object>) data;
				for (int i=0; i<_filterSetting.length; i++) {
					Object[] equalsTo = _filterSetting[i];
					// if (equalsTo != null && !al.get(i).equals(equalsTo))
					if (equalsTo != null && !StringUtils.findInArray(al.get(i).toString(), equalsTo))
						return null;
				}
				return data;
			}
			else {
				throw new IllegalArgumentException("unknown object type: "  
						+ data.getClass().getCanonicalName());
			}
		}
		
	}
	
	protected final String _dbName;
	protected final String _tblName;
	protected String[] _selects;
	protected Map<String, String> _querySpec;
	protected int _limit;
	protected boolean _allowPartialSpec;
	protected boolean _disableFiltering;
	
	// these variables are internally used by stream feeder
	private FetchOperator _ftOp = null;
	private Selector _selector = null;
	private Filter _filter = null;

	// Xiaowan: there is a bug in Hive 0.10.0. 
	// Cannot rely on InspectableObject.oi which is returned by FetchOperator.getNextRow().
	// The bug is obvious since variable 'rowObjectInspector' in FetchOperator.java is never set.
	// It affects partitioned table only.
	// 
	private ObjectInspector _oi = null;	// for Hive 0.10.0	

    private long _readCount = 0;
	private long _count = 0;
	
	public HiveDataInput(String dbName, String tblName)
	{
		_dbName = dbName;
		_tblName = tblName;
		_limit = -1;
		_selects = null;
		_querySpec = null;
		_allowPartialSpec = false;
		_disableFiltering = false;
	}
	
	public HiveDataInput setLimit(Integer limit)
	{
		_limit = limit == null? -1: limit;
		return this;
	}
	
	public HiveDataInput setSelects(String[] selects)
	{
		_selects = (selects == null? null : selects.clone());
		return this;
	}
	
	public HiveDataInput setFilter(Map<String, String> filter)
	{
		_querySpec = new HashMap<String, String>(filter);
		return this;
	}
	
	public HiveDataInput setAllowPartialSpec(boolean allow)
	{
		_allowPartialSpec = allow;
		return this;
	}
	
	public HiveDataInput setDisableFiltering(boolean disable)
	{
		_disableFiltering = disable;
		return this;
	}
	
	static protected
	FetchWork createFetchWork(Hive hive, Table table, int limit, Map<String, String> partSpec) 
			throws HiveException
	{
		final TableDesc tblDesc = Utilities.getTableDesc(table);
		
		if (!table.isPartitioned()) {
			String tblPath = table.getPath().toString();
			return new FetchWork(tblPath, tblDesc, limit);
		}

		List<Partition> ptns = hive.getPartitions(table, partSpec);

		List<String> listP = new ArrayList<String>();
		List<PartitionDesc> listD = new ArrayList<PartitionDesc>();
		for (Partition p : ptns) {
			listP.add(p.getPartitionPath().toString());
			listD.add(Utilities.getPartitionDesc(p));
		}
		
		// return new FetchWork(listP, listD, limit);	// for Hive 0.9.0
		return new FetchWork(listP, listD, tblDesc, limit);	// for Hive 0.10.0
	}
	
	private boolean checkPartitionSpec(Table table) 
	{
		if (_allowPartialSpec)
			return true;
		
		for (FieldSchema partCol: table.getPartCols()) {
			String colName = partCol.getName();
			if (!_querySpec.containsKey(colName)) {
				String msg = "missing partition specification: " + colName;
				throw new IllegalArgumentException(msg);
			}
		}
		return true;
	}
	
	public void initialize() throws HiveException
	{
		Hive hive = HiveCommon.getHive();
		final Table table = hive.getTable(_dbName, _tblName);
		checkPartitionSpec(table);

		final FetchWork fetch = createFetchWork(hive, table, _limit, _querySpec);
		JobConf job = new JobConf(hive.getConf(), ExecDriver.class);

		_ftOp = new FetchOperator(fetch, job);
		_oi = _ftOp.getOutputObjectInspector();	// for Hive 0.10.0	
		
		_selector = new Selector(table, _selects);
		_filter = new Filter(table, _querySpec);
		if (_filter.isEmpty()) {
			_filter = null;
		}
		else if (_disableFiltering) {
			throw new IllegalArgumentException("hive data filtering disabled");
		}

        log.debug("initialized");
	}
	
	
	@Override
	public Object read() throws IOException
	{
		while (true) {
			if (_limit > 0 && _count >= _limit) {
                return null;
            }
			
			InspectableObject io = _ftOp.getNextRow();
			if (io == null) {
				// retry to let fetch operator reset record reader
				io = _ftOp.getNextRow(); 
			}
			
			if (io == null || io.o == null) {
				return null;
			}

            _readCount++;
			Object data = ObjectInspectorUtils.copyToStandardJavaObject(io.o, _oi);	// for Hive 0.10.0	
			// Object data = ObjectInspectorUtils.copyToStandardJavaObject(io.o, io.oi); // for Hive 0.9.0
			if ((_readCount % 1_000_000) == 0) {
                log.trace("progress: overall rows scanned: {}, accepted: {}", _readCount, _count);
            }

            if (_filter != null) {
                data = _filter.filter(data);
            }

			if (data != null) {
				data = _selector.select(data);
				_count++;
				return data;
			}
		}
	}

	@Override
	public long getCount()
	{
		return _count;
	}

	@Override
	public void close() throws IOException
	{
		try {
			if (_ftOp != null)
				_ftOp.clearFetchContext();
		}
		catch (HiveException e) {
			throw new IOException("hive error clearing fetch context", e);
		}
	}

}
