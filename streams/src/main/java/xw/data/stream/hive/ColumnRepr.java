package xw.data.stream.hive;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.hive.metastore.api.FieldSchema;

@XmlRootElement
public class ColumnRepr implements Serializable
{
	private static final long serialVersionUID = 1323133044144578949L;
	public final String name;
	public final String type;
	public final String comment;
	
	public ColumnRepr(FieldSchema fs)
	{
		name = fs.getName();
		type = fs.getType();
		comment = fs.getComment();
	}
	
	static public List<ColumnRepr> getColumnRepresentations(List<FieldSchema> fields)
	{
		ArrayList<ColumnRepr> columns = new ArrayList<ColumnRepr>();
		for (FieldSchema fs: fields) {
			columns.add(new ColumnRepr(fs));
		}
		return columns;
	}
}
