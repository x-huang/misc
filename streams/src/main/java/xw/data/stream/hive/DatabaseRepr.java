package xw.data.stream.hive;

import java.io.Serializable;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.hadoop.hive.metastore.api.Database;
import org.apache.hadoop.hive.metastore.api.PrincipalPrivilegeSet;

@XmlRootElement
public class DatabaseRepr implements Serializable
{
	private static final long serialVersionUID = 5714403762585681612L;
	public final String name;
	public final String comment;
	public final String location;
	public final Map<String, String> properties;
	public final String privilege;
	
	// Note: list of tables is not a property of a database
	
	public DatabaseRepr(Database db) 
	{
		comment = db.getDescription();
		location = db.getLocationUri();
		name = db.getName();
		properties = db.getParameters();
		PrincipalPrivilegeSet pps = db.getPrivileges();
		privilege = (pps == null)? null: pps.toString();	
	}
}
