package xw.data.stream.hive;

import static org.apache.hadoop.hive.metastore.MetaStoreUtils.DEFAULT_DATABASE_NAME;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.hive.ql.metadata.HiveException;

import xw.data.common.uri.DatabaseUriParser;
import xw.data.common.uri.GeneralUriQuery;
import xw.data.stream.DataInputStream;
import xw.data.stream.IResourceOperator;

public class HiveResourceOperator implements IResourceOperator
{
	@Override
	public Object describeResource(URI uri) throws URISyntaxException,
			IOException
	{
		final DatabaseUriParser parser = new DatabaseUriParser(uri);
		final GeneralUriQuery query = parser.getQuery();
		
		String describe = query.getParameterAsString("describe");
		if (describe == null)
			describe = "table";
		query.removeParameter("describe");
		
		String dbName = parser.getDatabase();
		if (dbName == null || dbName.isEmpty())
			dbName = DEFAULT_DATABASE_NAME;
		String tblName = parser.getTable();

        switch (describe) {
            case "database":
                return HiveCommon.getDatabaseRepr(dbName);
            case "tables":
                return HiveCommon.getTableReprList(dbName);
            case "table":
                return HiveCommon.getTableRepr(dbName, tblName);
            case "partitions":
                return HiveCommon.getPartitionReprList(dbName, tblName, query.getAllParameters());
            case "partition":
                return HiveCommon.getPartitionRepr(dbName, tblName, query.getAllParameters());
            default:
                throw new UnsupportedOperationException("unable to describe this feature: " + describe);
        }
	}

	@Override
	public InputStream createInputStream(URI uri) throws URISyntaxException,
			IOException
	{
		final DatabaseUriParser parser = new DatabaseUriParser(uri);
		final GeneralUriQuery query = parser.getQuery();
		String dbName = parser.getDatabase();
		if (dbName == null || dbName.isEmpty())
			dbName = DEFAULT_DATABASE_NAME;
		String tblName = parser.getTable();
		
		final HiveDataInput hdi = new HiveDataInput(dbName, tblName);
		
		hdi.setLimit(query.getParameterAsInteger("limit"));
		
		String selects = query.getParameterAsString("select");
		if (selects != null)
			hdi.setSelects(selects.split(","));
		
		Boolean allowPartialSpec = query.getParameterAsYes("allowpartialspec");
		if (allowPartialSpec != null)
			hdi.setAllowPartialSpec(allowPartialSpec);

		Boolean disableFiltering = query.getParameterAsYes("disablefiltering");
		if (disableFiltering != null)
			hdi.setDisableFiltering(disableFiltering);
		
		String format = query.getParameterAsString("format");

		query.removeParameter("limit")
		 	 .removeParameter("format")
		 	 .removeParameter("select")
		 	 .removeParameter("allowpartialspec")
		 	 .removeParameter("ignorefiltering");
		
		hdi.setFilter(query.getAllParameters());
		
		try {
			hdi.initialize();
		}
		catch (HiveException e) {
			hdi.close();
			throw new IOException("error occured when initializing hive data stream", e);
		}
		return new DataInputStream(hdi, format);
	}

	@Override
	public OutputStream createOutputStream(URI uri) throws URISyntaxException,
			IOException
	{
		throw new UnsupportedOperationException("writing to hive not supported yet");
	}
}
