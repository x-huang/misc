package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import xw.data.common.sedes.BufferBasedSerializer;
import xw.data.common.sedes.Serializers;

public class DataInputStream extends InputStream
{
	private static int _byteBufferSize = 40 * 1024;
	private final BufferBasedSerializer _serializer;
	private final ObjectInput _input;
	
	private final ByteBuffer _bb;
	private int _index;
	
	static {
		String sBufferSize = System.getProperty("data.input.stream.buffer.size");
		if (sBufferSize != null) {
			int bufferSize = Integer.parseInt(sBufferSize);
			_byteBufferSize = Math.max(4096, bufferSize);
		}
	}
	
	public DataInputStream(ObjectInput input, String format)
	{
		_serializer = Serializers.createSerializer(format);
		_input = input;
		_bb = ByteBuffer.allocateDirect(_byteBufferSize);
		_index = 0;
	}
	
	public ObjectInput getObjectInput()
	{
		return _input;
	}
	
	@Override
	public int read() throws IOException
	{
		if (_index >= _bb.position()) {
			Object data = _input.read();
			if (data == null)
				return -1;
			_serializer.serialize(data, _bb);
			_index = 0;
		}
		
		byte b = _bb.get(_index++);
		return (int)b;
	}

	@Override
	public void close() throws IOException 
	{
		try {
			_input.close();
		}
		catch (Exception e) {
			throw new IOException("auto close error", e);
		}
		super.close();
	}
}
