package xw.data.stream;

import java.io.Closeable;
import java.io.IOException;

public interface ObjectInput extends Closeable
{
	public Object read() throws IOException;
	public long getCount();
}
