package xw.data.stream;

import java.io.PrintStream;
import java.util.Collection;

import xw.data.common.lang.Registry;
import xw.data.stream.dummy.DummyResourceOperator;
import xw.data.stream.hadoop.HadoopResourceOperator;
import xw.data.stream.hive.HiveResourceOperator;
import xw.data.stream.jdbc.JdbcResourceOperator;
import xw.data.stream.scp.RemoteResourceOperator;
import xw.data.stream.std.Std;
import xw.data.stream.std.StdErr;
import xw.data.stream.std.StdIn;
import xw.data.stream.std.StdOut;

public class Protocols
{
    final static private Registry<Class<? extends IResourceOperator>> _protocols;
    static {
        _protocols = new Registry<>();
        _protocols.register(HadoopResourceOperator.class, "hdfs", "s3n", "file")
                .register(HiveResourceOperator.class, "hive")
                .register(JdbcResourceOperator.class, "mysql", "mssql", "teradata")
                .register(RemoteResourceOperator.class, "scp", "ssh")
                .register(DummyResourceOperator.class, "dummy")
                .register(Std.class, "std")
                .register(StdIn.class, "stdin")
                .register(StdOut.class, "stdout")
                .register(StdErr.class, "stderr");
    }

    static public void initHandlers()
    {
        System.setProperty("java.protocol.handler.pkgs", "com.ea.eadp.data.protocols");
    }

    // resource operator SPI
    public static void registerOperatorClass(Class<? extends IResourceOperator> cls, String protocol)
    {
        _protocols.register(cls, protocol);
    }

    public static Collection<String> getAllProtocols()
    {
        return _protocols.getKeys();
    }

    public static void dump(PrintStream ps)
    {
        _protocols.dump(ps);
    }

    public static Class<? extends IResourceOperator> getOperatorClass(String protocol)
    {
        return _protocols.get(protocol);
    }
}
