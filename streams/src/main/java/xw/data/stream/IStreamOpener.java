package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

public interface IStreamOpener
{
	public InputStream createInputStream(URI uri) 
			throws URISyntaxException, IOException;
	public OutputStream createOutputStream(URI uri) 
			throws URISyntaxException, IOException;
}
