package xw.data.stream.transform;

import java.io.IOException;
import java.io.InputStream;

public interface InputStreamTransformer
{
	public InputStream transform(InputStream in) throws IOException;
}
