package xw.data.stream.transform;

import java.io.IOException;
import java.io.OutputStream;

public interface OutputStreamTransformer
{
	public OutputStream transform(OutputStream out) throws IOException;
}
