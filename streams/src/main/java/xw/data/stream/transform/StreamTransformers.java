package xw.data.stream.transform;

import xw.data.common.lang.Registry;

public class StreamTransformers
{
	private final static Registry<Class<? extends InputStreamTransformer>> _inputTransformers;
	private final static Registry<Class<? extends OutputStreamTransformer>> _outputTransformers;

	static {
		_inputTransformers = new Registry<>();
		_inputTransformers.register(GzipInputStreamTransformer.class, "gzip");
		_inputTransformers.register(GunzipInputStreamTransformer.class, "gunzip");
		
		_outputTransformers = new Registry<>();
		_outputTransformers.register(GzipOutputStreamTransformer.class, "gzip");
		_outputTransformers.register(GunzipOutputStreamTransformer.class, "gunzip");
	}
	
	public static void registerInputTransformerClass(String name, Class<? extends InputStreamTransformer> t) 
	{
		_inputTransformers.register(t, name);
	}

    public static void registerOutputTransformerClass(String name, Class<? extends OutputStreamTransformer> t)
    {
        _outputTransformers.register(t, name);
    }

    public static InputStreamTransformer getInputStreamTransformer(String name)
    {
        final Class<? extends InputStreamTransformer> clz;
        if (name == null || (clz = _inputTransformers.get(name)) == null) {
            throw new IllegalArgumentException("no such input stream transformer: " + name);
        }

        final InputStreamTransformer trans;
        try {
            trans = clz.newInstance();
        }
        catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("cannot instantiate transformer class: " + name, e);
        }
        return trans;
    }

    public static OutputStreamTransformer getOutputStreamTransformer(String name)
    {
        final Class<? extends OutputStreamTransformer> clz;
        if (name == null || (clz = _outputTransformers.get(name)) == null) {
            throw new IllegalArgumentException("no such output stream transformer: " + name);
        }

        final OutputStreamTransformer trans;
        try {
            trans = clz.newInstance();
        }
        catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("cannot instantiate transformer class: " + name, e);
        }
        return trans;
    }
}
