package xw.data.stream.transform;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class GzipStreamCommon
{
	public static final int DEFAULT_BUFFER_SIZE = 2048;
	public static final int GZ_BUFFER_SIZE = 1048576; // 1M
	protected static Executor executor = Executors.newCachedThreadPool();
}
