package xw.data.stream.transform;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

public class GzipOutputStreamTransformer implements OutputStreamTransformer
{
	@Override
	public OutputStream transform(OutputStream out) throws IOException
	{
		return new GZIPOutputStream(out, GzipStreamCommon.GZ_BUFFER_SIZE);
	}

}
