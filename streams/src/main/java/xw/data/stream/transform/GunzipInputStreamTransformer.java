package xw.data.stream.transform;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class GunzipInputStreamTransformer implements InputStreamTransformer
{
	@Override
	public InputStream transform(InputStream in) throws IOException
	{
		return new GZIPInputStream(in, GzipStreamCommon.GZ_BUFFER_SIZE);
	}

}
