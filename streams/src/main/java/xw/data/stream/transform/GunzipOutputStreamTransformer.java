package xw.data.stream.transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.CountDownLatch;
import java.util.zip.GZIPInputStream;

public class GunzipOutputStreamTransformer implements OutputStreamTransformer
{
    static final Logger log = LoggerFactory.getLogger(GunzipOutputStreamTransformer.class);

    protected static class GunzipOutputStream extends OutputStream implements AutoCloseable
	{
		final private OutputStream _guzOut;
		final private Closeable _out;
		final private CountDownLatch _latch;
		volatile private IOException _exc;  // to capture exception from the pumping thread.
		
		public GunzipOutputStream(OutputStream guzOut, Closeable out)
		{
			_guzOut = guzOut;
			_out = out;
			_latch = new CountDownLatch(1);
			_exc = null;
		}
	
		@Override
		public void close() throws IOException
		{
			mayThrowIOException();
			_guzOut.close();
			
			try {
				_latch.await();
			}
			catch (InterruptedException e) {
			}
			finally {
				_out.close();
			}
		}

		@Override
		public void write(int b) throws IOException
		{
			mayThrowIOException();
			_guzOut.write(b);
		}
		
		@Override
		public String toString()
		{
			return _out.toString()  + "|" +  _guzOut.toString();
		}
		
		protected void error(IOException e) 
		{
			_exc = e;
		}
		
		protected void done() 
		{
			_latch.countDown();
		}
		
		private void mayThrowIOException() throws IOException
		{
			IOException exc = _exc;
			_exc = null;
			if (exc != null)
				throw exc;
		}
	}
	
	@Override
	public OutputStream transform(final OutputStream out) throws IOException
	{
		final PipedOutputStream guzOut = new PipedOutputStream();
		final GunzipOutputStream unzipped = new GunzipOutputStream(guzOut, out);
		final PipedInputStream pipe = new PipedInputStream(guzOut);
		
		// data flow: <user> -> unzipped(O) -> pipe(I) -> GZIPInputStream(I) -> pumping_thread -> out(O)
		// (guzOut, out) are wrapped as GunzipOutputStream,
		final Thread pump = new Thread(new Runnable() {
			@Override 
			public void run()
			{
                log.debug("starting pumping thread");
				try {
					InputStream gzIn = new GZIPInputStream(pipe, GzipStreamCommon.GZ_BUFFER_SIZE);
					try {
						byte[] buf = new byte[GzipStreamCommon.DEFAULT_BUFFER_SIZE];
						int read;
						while ((read = gzIn.read(buf)) >= 0) {
							out.write(buf, 0, read);
						}
					}
					finally {
						gzIn.close();
					}
				}
				catch (IOException e) {
					unzipped.error(e);
				}
				finally {
					unzipped.done();
					try {
						pipe.close();
					}
					catch (IOException e) {
                        log.error("error closing pipe", e);
					}
				}
			}
		});
		pump.start();
		
		return unzipped;
	}
}
