package xw.data.stream.transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.CountDownLatch;
import java.util.zip.GZIPOutputStream;

public class GzipInputStreamTransformer implements InputStreamTransformer
{
    static final Logger log = LoggerFactory.getLogger(GzipInputStreamTransformer.class);

	protected static class GzipInputStream extends InputStream implements AutoCloseable 
	{
		final private InputStream _gzIn;
		final private Closeable _in;
		final private CountDownLatch _latch;
		volatile private IOException _exc;  // to capture exception from the pumping thread.
		
		public GzipInputStream(InputStream gzIn, Closeable in)
		{
			_gzIn = gzIn;
			_in = in;
			_latch = new CountDownLatch(1);
			_exc = null;
		}
	
		@Override
		public void close() throws IOException
		{
			mayThrowIOException();
			_gzIn.close();
			
			try {
				_latch.await();
			}
			catch (InterruptedException e) {
			}
			finally {
				_in.close();
			}
		}

		@Override
		public int read() throws IOException
		{
			mayThrowIOException();
			return _gzIn.read();
		}
		
		@Override
		public String toString()
		{
			return _in.toString()  + "|" +  _gzIn.toString();
		}
		
		protected void error(IOException e) 
		{
			_exc = e;
		}
		
		protected void done() 
		{
			_latch.countDown();
		}
		
		private void mayThrowIOException() throws IOException
		{
			IOException exc = _exc;
			_exc = null;
			if (exc != null)
				throw exc;
		}
	}
	
	@Override
	public InputStream transform(final InputStream in) throws IOException
	{
		final PipedInputStream gzIn = new PipedInputStream();
		final GzipInputStream zipped = new GzipInputStream(gzIn, in);
		final PipedOutputStream pipe = new PipedOutputStream(gzIn);
		
		// data flow: in(I) -> pumping_thread ->  GZIPOutputStream(O) -> pipe(O) -> gzIn(I) -> <user>
		// (gzIn, in) are wrapped as GzipInputStream,
		// note it is different than GZIPInputStream which essentially means unzip
		final Thread pump = new Thread(new Runnable() {
			@Override
			public void run()
			{
                log.debug("starting pumping thread");
				try {
					OutputStream gzOut = new GZIPOutputStream(pipe, GzipStreamCommon.GZ_BUFFER_SIZE);
					try {
						byte[] buf = new byte[GzipStreamCommon.DEFAULT_BUFFER_SIZE];
						int read;
						while ((read = in.read(buf)) >= 0) {
							gzOut.write(buf, 0, read);
						}
					}
					finally {
						gzOut.close();
					}
				}
				catch (IOException e) {
					zipped.error(e);
				}
				finally {
					zipped.done();
					try {
						pipe.close();
					}
					catch (IOException e) {
                        log.error("error closing pipe", e);
					}
				}
			}
		});
		pump.start();
		
		return zipped;
	}
}
