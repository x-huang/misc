package xw.data.stream.dummy;

import java.io.IOException;
import java.io.InputStream;

public class EmptyFileInputStream extends InputStream
{
	@Override
	public int read() throws IOException
	{
		return -1;	// EOF all the time
	}

}
