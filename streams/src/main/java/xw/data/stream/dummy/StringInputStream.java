package xw.data.stream.dummy;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class StringInputStream  extends InputStream
{
	protected final ByteArrayInputStream _in;
	protected final int _repeat;
	protected int _count = 0;
	
	public StringInputStream(String text, int repeat)
	{
		_in = new ByteArrayInputStream(text.getBytes());
		_repeat = repeat;
	}
	
	@Override
	public int read() throws IOException
	{
		if (_count >= _repeat) {
			return -1;
		}
		
		int ch = _in.read();
		if (ch != -1) {
			return ch;
		}
		
		_count++;
		_in.reset();
		return read();
	}

}
