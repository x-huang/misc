package xw.data.stream.dummy;

import java.io.IOException;
import java.io.OutputStream;

public class NullFileOutputStream extends OutputStream
{
	@Override
	public void write(int arg0) throws IOException
	{
		// intentionally do nothing
	}

}
