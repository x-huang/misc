package xw.data.stream.dummy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import xw.data.common.uri.GeneralUriQuery;
import xw.data.stream.IResourceOperator;

public class DummyResourceOperator implements IResourceOperator
{
	@Override
	public InputStream createInputStream(URI uri) 
			throws URISyntaxException, IOException
	{
		String path = uri.getPath().substring(1).toLowerCase();
		if (path.equals("empty") || path.equals("null")) {
			return new EmptyFileInputStream();
		}
		else if (path.equals("text")) {
			final GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
			String text = query.getParameterAsString("content");
			if (text == null) {
				throw new URISyntaxException(uri.toString(), "missing content");
			}
			Integer repeat = query.getParameterAsInteger("repeat");
			if (repeat == null) {
				repeat = 1;
			}
			return new StringInputStream(text, repeat);
		}
		else {
			throw new UnsupportedOperationException("unknown dummy stream: " + uri.toString());
		}
	}

	@Override
	public OutputStream createOutputStream(URI uri) 
			throws URISyntaxException, IOException
	{
		String path = uri.getPath().substring(1).toLowerCase();
		if (path.equals("empty") || path.equals("null")) {
			return new NullFileOutputStream();
		}
		else { 
			throw new UnsupportedOperationException("unknown dummy stream: " + uri.toString());
		}
	}
	
	@Override
	public Object describeResource(URI uri) 
			throws URISyntaxException, IOException
	{
		throw new UnsupportedOperationException("nothing to describe for a dummy resource");
	}

}
