package xw.data.stream.hadoop;

import java.io.IOException;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class DirectoryRepr
{
	public final String path;
	public final String[] entries;
	
	public DirectoryRepr(FileSystem fs, Path p)
			throws IOException
	{
		path = p.toUri().toString();
		
		FileStatus[] statusList = fs.listStatus(p);
		entries = new String[statusList.length];
		for (int i=0; i<statusList.length; i++) {
			FileStatus status = statusList[i];
			entries[i] = status.getPath().getName();
			if (status.isDir())
				entries[i] = entries[i] + "/";			
		}
	}
	
	@Override
	public String toString() 
	{
		return path;
	}
}
