package xw.data.stream.hadoop;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import xw.data.stream.BoundedInputStream;
import xw.data.stream.DataSource;

public class HadoopFileDataSource implements DataSource
{
	protected Long _offset; 
	protected Long _size;
	protected final URI _uri;
	
	public HadoopFileDataSource(URI uri) 
			throws URISyntaxException, IOException
	{
		String path = uri.getPath();
		if (path.endsWith("/")) {
			throw new URISyntaxException(uri.toString(), 
						"path not supposed to be a directory");
		}
		_offset = null;
		_size = null;
		_uri = uri;
	}

	public HadoopFileDataSource setOffset(Long offset)
	{
		_offset = offset;
		return this;
	}
	
	public HadoopFileDataSource setSize(Long size)
	{
		_size = size;
		return this;
	}
	
	@Override
	public InputStream getInputStream() throws IOException
	{
		FileSystem fs = HadoopCommon.getFileSystem(_uri);
		FSDataInputStream in = fs.open(new Path(_uri.getPath()));
		if (_offset != null && _offset != 0)
			in.seek(_offset);
		if (_size != null) 
			return new BoundedInputStream(in, _size);
		else
			return in;
	}

}

