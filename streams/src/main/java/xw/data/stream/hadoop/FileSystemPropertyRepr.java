package xw.data.stream.hadoop;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;

public class FileSystemPropertyRepr
{
	// disabled because doesn't work for s3n file system
	// public final String canonicalServiceName; 
	// public final long defaultBlockSize;
	// public final short defaultReplication;
	public final String workingDirectory;
	public final String uri;
	// public final long used;
	
	public FileSystemPropertyRepr(FileSystem fs) throws IOException
	{
		// canonicalServiceName = fs.getCanonicalServiceName();	
		// defaultBlockSize = fs.getDefaultBlockSize();
		// defaultReplication = fs.getDefaultReplication();
		workingDirectory = fs.getWorkingDirectory().toString();
		uri = fs.getUri().toString();
		// used = fs.getUsed();	// not sure if I shall add this. It involves network IO and could be slow.
	}
}
