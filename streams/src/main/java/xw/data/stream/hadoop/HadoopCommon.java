package xw.data.stream.hadoop;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HadoopCommon
{
    static final Logger log = LoggerFactory.getLogger(HadoopCommon.class);

	static private Configuration _conf = null;
	static final public String HADOOP_CONF_PRIFIX = "HADOOP_CONF.";
	
	static protected Configuration getHadoopConfiguration()
	{
		if (_conf == null) {
			_conf = new Configuration();
			final Properties props = System.getProperties();
			for (String prop: props.stringPropertyNames()) {
				if (!prop.startsWith(HADOOP_CONF_PRIFIX)) 
					continue;
				String key = prop.substring(HADOOP_CONF_PRIFIX.length());
				String value = props.getProperty(prop);
				_conf.set(key, value);
                log.info("set hadoop configuration: {}={}", key, value);
			}
		}
		return _conf;
	}
	
	static public Object getFileChecksum(URI uri) throws IOException
	{
		FileSystem fs = FileSystem.get(uri, getHadoopConfiguration());
		Path path = new Path(uri.getPath());
		return fs.getFileChecksum(path);
	}
	
	static public boolean delete(URI uri) throws IOException
	{
		FileSystem fs = FileSystem.get(uri, getHadoopConfiguration());
		Path path = new Path(uri.getPath());
		return fs.delete(path, true);	// delete recursively
	}
	
	static protected FileSystem getFileSystem(URI uri) throws IOException
	{
		return FileSystem.get(uri, getHadoopConfiguration());
	}
	
	static public List<String> listFiles(URI uri) throws IOException
	{
		FileSystem fs = FileSystem.get(uri, getHadoopConfiguration());
		Path path = new Path(uri.getPath());
		FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDir()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		FileStatus[] statuses = fs.listStatus(path);
		List<String> files = new ArrayList<>();
		for (FileStatus s: statuses) {
			if (!s.isDir()) {
				files.add(s.getPath().getName());
			}
		}
		return files;
	}
	
	static private void addFilesRecursively(FileSystem fs, String base, String subdir, List<String> files) throws IOException
	{
		Path path = new Path(base + subdir);
		FileStatus status = fs.getFileStatus(path);
		if (!status.isDir()) {
			throw new IllegalArgumentException("path is not a directory: " + (base + subdir));
		}
		
		FileStatus[] statuses = fs.listStatus(path);
		for (FileStatus s: statuses) {
			String segName = s.getPath().getName();
			if (s.isDir()) {
				addFilesRecursively(fs, base, subdir + segName + "/", files);
			}
			else {
				files.add(subdir + segName);
			}
		}
	}
	
	static public List<String> listFilesRecursively(URI uri) throws IOException
	{
		FileSystem fs = FileSystem.get(uri, getHadoopConfiguration());
		Path path = new Path(uri.getPath());
		FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDir()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		List<String> files = new ArrayList<>();
		addFilesRecursively(fs, uri.getPath(), "", files);
		return files;
	}
	
	static public List<String> listEntries(URI uri) throws IOException
	{
		FileSystem fs = FileSystem.get(uri, getHadoopConfiguration());
		Path path = new Path(uri.getPath());
		FileStatus status = fs.getFileStatus(path);
		
		if (!status.isDir()) {
			throw new IllegalArgumentException("path is not a directory: " + uri.toString());
		}
		
		FileStatus[] statuses = fs.listStatus(path);
		List<String> entries = new ArrayList<>();
		for (FileStatus s: statuses) {
			if (s.isDir()){
				entries.add(s.getPath().getName() + "/");
			}
			else {
				entries.add(s.getPath().getName());
			}
		}
		return entries;
	}
	
	static public DirectoryRepr getDirectoryRepr(URI uri) 
			throws IOException
	{
		FileSystem fs = getFileSystem(uri);
		Path path = new Path(uri.getPath());
		return new DirectoryRepr(fs, path);
	}

	static public FileStatusRepr getFileStatusRepr(URI uri) 
			throws IOException
	{
		FileSystem fs = getFileSystem(uri);
		Path path = new Path(uri.getPath());
		return new FileStatusRepr(fs, path);
	}
	
	static public FileSystemPropertyRepr getFileSystemPropertyRepr(URI uri) 
			throws IOException
	{
		FileSystem fs = getFileSystem(uri);
		return new FileSystemPropertyRepr(fs);
	}
}
