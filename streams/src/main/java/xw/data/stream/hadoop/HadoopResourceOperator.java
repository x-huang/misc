package xw.data.stream.hadoop;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import xw.data.common.uri.GeneralUriQuery;
import xw.data.common.uri.UriUtils;
import xw.data.stream.IResourceOperator;

public class HadoopResourceOperator implements IResourceOperator
{
	@Override
	public Object describeResource(URI uri) throws URISyntaxException,
			IOException
	{
		if (uri.getPath().endsWith("/")) {
			return HadoopCommon.getDirectoryRepr(uri);
		}
		else {
			return HadoopCommon.getFileStatusRepr(uri);
		}
	}

	@Override
	public InputStream createInputStream(URI uri) throws URISyntaxException,
			IOException
	{
		GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
		Long offset = query.getParameterAsLong("offset");
		Long size = query.getParameterAsLong("size");
		URI rootUri = UriUtils.removeQueryPart(uri);
		HadoopFileDataSource source = new HadoopFileDataSource(rootUri);
		return source.setOffset(offset).setSize(size).getInputStream();
	}

	@Override
	public OutputStream createOutputStream(URI uri) throws URISyntaxException,
			IOException
	{
		GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
		Boolean overwrite = query.getParameterAsYes("overwrite");
		URI rootUri = UriUtils.removeQueryPart(uri);
		HadoopFileDataSink sink = new HadoopFileDataSink(rootUri);
		return sink.setOverwrite(overwrite).setMkDirIfNeeded(true).getOutputStream();
	}
	
	/*
	public static InputStream getInputStream(URI uri)
			throws URISyntaxException, IOException
	{
		String path = uri.getPath();
		GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
		Long offset = query.getParameterAsLong("offset");
		Long size = query.getParameterAsLong("size");
		HadoopFileDataSource source = new HadoopFileDataSource(uri.getScheme(), path);
		return source.setOffset(offset).setSize(size).getInputStream();
	}
	
	public static OutputStream getOutputStream(URI uri)
			throws URISyntaxException, IOException
	{
		String path = uri.getPath();
		GeneralUriQuery query =  new GeneralUriQuery(uri.getQuery());
		Boolean overwrite = query.getParameterAsYes("overwrite");
		HadoopFileDataSink sink = new HadoopFileDataSink(uri.getScheme(), path);
		return sink.setOverwrite(overwrite).getOutputStream();
	}
	*/
}
