package xw.data.stream.hadoop;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import xw.data.stream.DataSink;

public class HadoopFileDataSink implements DataSink
{
	protected final URI _uri;
	private boolean _overwrite;
	private boolean _mkdir;
	
	public HadoopFileDataSink(URI uri) 
			throws URISyntaxException, IOException
	{
		String path = uri.getPath();
		if (path.endsWith("/")) {
			throw new URISyntaxException(uri.toString(), 
						"path not supposed to be a directory");
		}
		_uri = uri;
		_overwrite = false;
		_mkdir = false;	// to be safer
	}

	public HadoopFileDataSink setOverwrite(Boolean overwrite)
	{
		if (overwrite != null)
			_overwrite = overwrite;
		return this;
	}
	
	public HadoopFileDataSink setMkDirIfNeeded(Boolean mkdir)
	{
		if (mkdir != null)
			_mkdir = mkdir;
		return this;
	}
	
	@Override
	public OutputStream getOutputStream() throws IOException
	{
		FileSystem fs = HadoopCommon.getFileSystem(_uri);
		Path path = new Path(_uri.getPath());
		if (_mkdir) {
			Path parentPath = path.getParent();
			if (parentPath != null) {
				fs.mkdirs(path.getParent());
			}
		}
		return fs.create(path, _overwrite);
	}
}
