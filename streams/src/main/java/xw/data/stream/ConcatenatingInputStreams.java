package xw.data.stream;

import java.io.IOException;
import java.io.InputStream;

public class ConcatenatingInputStreams extends InputStream 
{
	final private InputStream[] _ins;
	private int _index;
	private InputStream _curr;
	
	public ConcatenatingInputStreams(InputStream[] ins)
	{
		_ins = new InputStream[ins.length];
		for (int i=0; i<ins.length; i++) {
			_ins[i] = ins[i];
		}
		_curr = _ins[0];
		_index = 0;
	}
	
	public int size()
	{
		return _ins.length;
	}

	public int progress()
	{
		return _index;
	}
	
	@Override
	public int read() throws IOException
	{
		int b = _curr.read();
		if (b != -1) 
			return b;
		
		if (_index < _ins.length-1) {
			_curr.close();
			_index++;
			_curr = _ins[_index];
			return read();
		}
		else {
			return -1;
		}
	}

	@Override
	public void close() throws IOException
	{
		IOException first = null;
		
		try {
			for (int i=_index; i<_ins.length; i++) {
				_ins[i].close();
			}
		}
		catch (IOException e) {
			if (first == null)
				first = e;
		}
		
		if (first != null)
			throw first;
	}
}
