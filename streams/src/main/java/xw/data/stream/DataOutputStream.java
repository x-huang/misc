package xw.data.stream;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import xw.data.common.sedes.Deserializers;
import xw.data.common.sedes.BufferBasedDeserializer;

public class DataOutputStream extends OutputStream
{
	private static int _byteBufferSize = 40 * 1024;
	
	private final BufferBasedDeserializer _deserializer;
	private final ObjectOutput _output;
	private final ByteBuffer _bb;
	
	static {
		String sBufferSize = System.getProperty("data.output.stream.buffer.size");
		if (sBufferSize != null) {
			int bufferSize = Integer.parseInt(sBufferSize);
			_byteBufferSize = Math.max(4096, bufferSize);
		}
	}
	
	public DataOutputStream(ObjectOutput output, String format)
	{
		_deserializer = Deserializers.createDeserializer(format);
		_output = output;
		_bb = ByteBuffer.allocateDirect(_byteBufferSize);
	}
	
	public ObjectOutput getObjectOutput()
	{
		return _output;
	}
	
	@Override
	public void write(int arg) throws IOException
	{
		byte b = (byte)arg;
		
		if (isDataDelimitor(b)) {
			Object o = _deserializer.deserialize(_bb);
			_output.write(o);
		}
		else {
			_bb.put(b);
		}
	}
	
	protected boolean isDataDelimitor(byte b) 
	{
		return b == (byte)'\n';
	}

	@Override
	public void close() throws IOException 
	{
		try {
			_output.close();
		}
		catch (Exception e) {
			throw new IOException("auto close error", e);
		}
		super.close();
	}
}
