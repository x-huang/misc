package xw.data.stream;

public interface IResourceOperator 
		extends IStreamOpener, IResourceDescriber
{
}
