package xw.data.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
 * Note:
 * 
 * 	For ticket: http://eadpjira.ea.com/browse/AQUARIUS-2022.
 *  Must be used jointly with SplitByLineGzipConnector
 * 
 * 	It is not a general purpose utility so not implemented as 
 * 	a functionality of DataStreams. Instead I tweaked
 * 	TransportWorker to support this data output stream.
 */

@Deprecated
public class SplittedOutputStream extends OutputStream
{
    static final Logger log = LoggerFactory.getLogger(SplittedOutputStream.class);
	final static protected Pattern _formatPattern = Pattern.compile("%[0-9]+d");
	private OutputStream _out;
	private int _num;
	private String _uri;
	private SplittedOutputUriMaker _uriMaker;
	
	static public String extractSequenceFormat(String uriPattern)
	{
		Matcher matcher = _formatPattern.matcher(uriPattern);
		return matcher.find()? matcher.group(0): null;
	}
	
	static public boolean isSplittedOutputUri(String uri)
	{
		return extractSequenceFormat(uri) != null;
	}
	
	public SplittedOutputStream(SplittedOutputUriMaker uriMaker)
			throws IOException, URISyntaxException
	{
		_num = 0;
		_uriMaker = uriMaker;
		_out = null;
		_uri = null;
		rotate();
	}
	
	public SplittedOutputStream(final String uriPattern)
			throws IOException, URISyntaxException
	{
		_num = 0;
		_uriMaker = new SplittedOutputUriMaker() {
			String _seqFormat = null;
			@Override
			public String makeUri(int no)
			{
				if (_seqFormat == null) {
					_seqFormat = extractSequenceFormat(uriPattern);
					assert _seqFormat != null;
				}
				String strNo = String.format(_seqFormat, no);
				return uriPattern.replace(_seqFormat, strNo);
			}
		};
		_out = null;
		_uri = null;
		rotate();
	}
	
	public SplittedOutputUriMaker getUriMaker()
	{
		return _uriMaker;
	}
	
	@Override
	public void write(int b) throws IOException
	{
		_out.write(b);
	}

	public void rotate() throws IOException
	{
		if (_out != null)
			_out.close();
		_uri = _uriMaker.makeUri(_num++);
		try {
			_out = DataStreams.getOutputStream(_uri);
		}
		catch (URISyntaxException e) {
			throw new IOException("wrong uri syntax: " + _uri, e);
		}
		log.debug("rotated to: {}", _uri);
	}
	
	public String getCurrentUri()
	{
		return _uri;
	}
	
	@Override
	public void close() throws IOException
	{		
		if (_out != null)
			_out.close();
	}
}
