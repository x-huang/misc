package xw.data.stream.std;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import xw.data.stream.IResourceOperator;

/**
 * Created by yun on 8/24/14.
 */
public class StdStreamOperator implements IResourceOperator
{
    @Override
    public InputStream createInputStream(URI uri) throws IOException
    {
        throw new IOException("cannot open as input stream");
    }

    @Override
    public OutputStream createOutputStream(URI uri) throws IOException
    {
        throw new IOException("cannot open as output stream");
    }

    @Override
    public Object describeResource(URI uri) throws IOException
    {
        throw new UnsupportedOperationException("nothing to describe a std stream");
    }
}
