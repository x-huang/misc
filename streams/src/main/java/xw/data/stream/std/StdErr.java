package xw.data.stream.std;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

public class StdErr extends StdStreamOperator
{
	@Override
	public OutputStream createOutputStream(URI uri) throws IOException
	{
		return System.err;
	}
}
