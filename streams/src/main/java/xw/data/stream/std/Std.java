package xw.data.stream.std;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

public class Std extends StdStreamOperator
{
	@Override
	public InputStream createInputStream(URI uri) throws IOException
	{
        assert uri.getScheme().equals("std");
        assert uri.getAuthority() == null;
        if (uri.getPath().equals("in")) {
            return System.in;
        }
        else {
            throw new IllegalArgumentException("expecting uri: std:///in");
        }
	}
	
	@Override
	public OutputStream createOutputStream(URI uri) throws IOException
	{
        assert uri.getScheme().equals("std");
        assert uri.getAuthority() == null;

        switch (uri.getPath()) {
            case "out": return System.out;
            case "err": return System.err;
            default: throw new IllegalArgumentException("expecting uri: std:///out or std:///err");
        }
	}

}
