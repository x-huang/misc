package xw.data.stream.std;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class StdIn extends StdStreamOperator
{
	@Override
	public InputStream createInputStream(URI uri) throws IOException
	{
		return System.in;
	}
}
