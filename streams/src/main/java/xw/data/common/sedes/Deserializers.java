package xw.data.common.sedes;

import static xw.data.common.lang.StringUtils.findInSet;

public class Deserializers
{
	static public BufferBasedDeserializer createDeserializer(String format) 
			throws IllegalArgumentException
	{
		format = (format == null)? "": format.toLowerCase();
		if (findInSet(format, "", "json", "jsonarray")) {
			return new JsonArrayDeserializer();
		}
		else { 
			throw new IllegalArgumentException("illegal format: " + format);
		}
 	}

}
