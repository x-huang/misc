package xw.data.common.sedes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

public class JsonArraySerializer implements BufferBasedSerializer
{
	final private ObjectMapper _jsonMapper;  // shall be static: http://stackoverflow.com/questions/3907929/should-i-make-jacksons-objectmapper-as-static-final
	
	public JsonArraySerializer()
	{
		_jsonMapper = new ObjectMapper();
		_jsonMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		_jsonMapper.setDateFormat(df);
	}
	
	@Override
	public void serialize(Object o, ByteBuffer bb) throws IOException
	{
		bb.clear();
		bb.put(_jsonMapper.writeValueAsBytes(o));
		bb.put((byte)'\n');
	}

}
 

