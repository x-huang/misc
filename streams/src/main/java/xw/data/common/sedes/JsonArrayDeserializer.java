package xw.data.common.sedes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class JsonArrayDeserializer implements BufferBasedDeserializer
{
	final private ObjectMapper _jsonMapper;
	private byte[] _bytes;
	
	public JsonArrayDeserializer()
	{
		_jsonMapper = new ObjectMapper();
		_bytes = new byte[4096];
	}
	
	@Override
	public Object deserialize(ByteBuffer bb) throws IOException
	{
		/* Error: org.codehaus.jackson.JsonParseException: Illegal character 
		 * ((CTRL-CHAR, code 0)): only regular white space (\r, \n, \t) is 
		 * allowed between tokens
		 */
		
		// FIXME: I need to convert byte array to string first
		
		int len = bb.position();
		if (_bytes.length < len) {
			_bytes = new byte[len];
		}
		
		bb.flip();
		bb.get(_bytes, 0, len);

		List<?> o = _jsonMapper.readValue(_bytes, 0, len, List.class);
		bb.clear();
		return o;
	}
}

