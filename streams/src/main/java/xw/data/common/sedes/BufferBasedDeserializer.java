package xw.data.common.sedes;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface BufferBasedDeserializer
{
	// deserialize code in a byte buffer to an object
	Object deserialize(ByteBuffer bb) throws IOException;
}
