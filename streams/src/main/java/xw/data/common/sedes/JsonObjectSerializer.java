package xw.data.common.sedes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.codehaus.jackson.map.ObjectMapper;

public class JsonObjectSerializer implements BufferBasedSerializer
{
	final ObjectMapper _jsonMapper;
	final String[] _schema;
	final LinkedHashMap<String, Object> _map;
	
	public JsonObjectSerializer(String[] cols)
	{
		_jsonMapper = new ObjectMapper();
		_schema = cols.clone();
		_map = new LinkedHashMap<>();
	}
	
	@Override
	public void serialize(Object o, ByteBuffer bb) throws IOException
	{
		if (IteratorUtils.getDataLength(o) != _schema.length)
			throw new IOException("data size doesn't match schema");
		
		_map.clear();
		int index = 0;
		Iterator<Object> iter = IteratorUtils.getUnifiedIterator(o);

		while (iter.hasNext()) {
			_map.put(_schema[index++], iter.next());
		}
			
		bb.clear();
		bb.put(_jsonMapper.writeValueAsBytes(_map));
		bb.put((byte)'\n');
	}

}
