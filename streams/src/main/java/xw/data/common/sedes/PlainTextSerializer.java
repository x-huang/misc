package xw.data.common.sedes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;

public class PlainTextSerializer implements BufferBasedSerializer
{
	private byte _delim;
	private String _nullRepr;
	private Charset _charset;
	
	private final static String _defaultNullRepr;
	
	static {
		String nullRepr = System.getProperty("transport.sedes.null.repr");
		if (nullRepr == null) {
			nullRepr = System.getenv("TRANSPORT_SEDES_NULL_REPR");
		}
		_defaultNullRepr = (nullRepr == null)? "" : nullRepr;
	}
	
	public PlainTextSerializer() 
	{
		_delim = '\t';
		// Xiaowan: use an empty string because text format is not expressive enough!
		_nullRepr = _defaultNullRepr;
		_charset = Charset.forName("UTF-8"); 
	}
	
	public PlainTextSerializer setDelimiter(char delim)
	{
		_delim = (byte)delim;
		return this;
	}
	

	public PlainTextSerializer setNullValueRepr(String repr)
	{
		_nullRepr = repr;
		return this;
	}
	
	public PlainTextSerializer setCharset(String charset)
	{
		_charset = Charset.forName(charset);
		return this;
	}
	
	@Override
	public void serialize(Object o, ByteBuffer bb) throws IOException
	{
		Iterator<Object> iter = IteratorUtils.getUnifiedIterator(o);

		boolean first = true;
		bb.clear();
		
		while (iter.hasNext()) {
			Object elem = iter.next();
			if (first) 
				first = false;
			else
				bb.put(_delim);
			if (elem != null) {
				bb.put(elem.toString().getBytes(_charset));
			}
			else {
				bb.put(_nullRepr.getBytes(_charset));
			}
		}
		bb.put((byte)'\n');
	}
}
