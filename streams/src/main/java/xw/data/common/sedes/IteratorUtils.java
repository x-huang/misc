package xw.data.common.sedes;

import java.util.Iterator;
import java.util.List;

public class IteratorUtils
{
	@SuppressWarnings("unchecked")
	static public Iterator<Object> getUnifiedIterator(Object data)
	{
		if (data instanceof Object[]) {
			return new ObjectArrayIterator((Object[])data);
		}
		else if (data instanceof Iterable<?>) {
			return ((Iterable<Object>)data).iterator();
		}
		else {
			Object[] array = new Object[1];
			array[0] = data;
			return new ObjectArrayIterator(array);
		}
	}
	
	static public int getDataLength(Object data)
	{
		if (data instanceof Object[]) {
			return ((Object[])data).length;
		}
		else if (data instanceof List<?>) {
			return ((List<?>)data).size();
		}
		else {
			return 1;
		}
	}
}
