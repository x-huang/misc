package xw.data.common.sedes;

import static xw.data.common.lang.StringUtils.findInSet;

public class Serializers
{
	static public BufferBasedSerializer createSerializer(String format) 
			throws IllegalArgumentException
	{
		format = (format == null)? "": format.toLowerCase();
		if (findInSet(format, "", "json", "jsonarray")) {
			return new JsonArraySerializer();
		}
		else if (findInSet(format, "text", "txt", "tsv", "csv", "psv")) {
			PlainTextSerializer serializer = new PlainTextSerializer();
			if (format.equals("csv")) {
				serializer.setDelimiter(',');
			}
			else if (format.equals("psv")) {
				serializer.setDelimiter('|');
			}
			// FIXME: to be compatible to legacy code 
			// serializer.setNullValueRepr("<null>");
			return serializer;
		}
		else if (format.startsWith("jsonobject:")) {
			String[] cols = format.substring("jsonobject:".length()).split(",");
			return new JsonObjectSerializer(cols);
		}
		else {
            throw new IllegalArgumentException("illegal format: " + format);
        }
	}
}
