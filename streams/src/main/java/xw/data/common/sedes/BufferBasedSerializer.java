package xw.data.common.sedes;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface BufferBasedSerializer
{
	// directly serialize an object to a byte buffer
	public void serialize(Object o, ByteBuffer bb) throws IOException;
}
