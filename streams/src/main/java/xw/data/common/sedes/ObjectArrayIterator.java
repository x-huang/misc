package xw.data.common.sedes;

import java.util.Iterator;

public class ObjectArrayIterator implements Iterator<Object>
{
	final private Object[] _array;
	private int _index;
	
	public ObjectArrayIterator(Object[] array)
	{
		_array = array;
		_index = 0;
	}
	
	@Override
	public boolean hasNext()
	{
		return _index < _array.length;
	}

	@Override
	public Object next()
	{
		Object o = _array[_index];
		_index++;
		return o;
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException("removing from an object array not allowed");
	}

}
