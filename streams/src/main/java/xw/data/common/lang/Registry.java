package xw.data.common.lang;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by yun on 8/24/14.
 */
public class Registry<T>
{
    final private Map<String, T> _registry = new HashMap<>();

    public Registry<T> register(T item, String... names)
    {
        for (String name: names) {
            if (_registry.containsKey(name)) {
                throw new IllegalArgumentException("registry entry already exists: " + name);
            }
        }
        if (item == null) {
            throw new IllegalArgumentException("registry cannot accept null item");
        }
        for (String name: names) {
            _registry.put(name, item);
        }
        return this;
    }

    public Set<String> getKeys()
    {
        return _registry.keySet();
    }

    public void dump(PrintStream ps)
    {
        for (Map.Entry<String, T> entry: _registry.entrySet()) {
            ps.println(entry.getKey() + "\t" + entry.getValue().toString());
        }
    }

    public T get(String name)
    {
        if (! _registry.containsKey(name)) {
            throw new IllegalArgumentException("no such unknown item in registry: " + name);
        }
        return _registry.get(name);
    }
}
