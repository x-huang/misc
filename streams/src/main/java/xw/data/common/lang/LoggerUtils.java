package xw.data.common.lang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/* Xiaowan: having this utility class to help turn off logs by Hadoop/Hive logger */

public class LoggerUtils
{
	public static void printLoggers()
	{
		@SuppressWarnings("unchecked")
		List<Logger> loggers = Collections.<Logger> list(LogManager.getCurrentLoggers());
		loggers.add(LogManager.getRootLogger());
		for (Logger logger : loggers) {
			Category parent = logger.getParent();
			System.out.println("[INFO] logger: " + logger.getName()
								+ ", level: " + logger.getLevel() 
								+ ", parent: " + (parent==null? null: parent.getName()));
		}
	}
	
	public static List<String> listLoggers()
	{
		List<String> loggers = new ArrayList<>();
		loggers.add(LogManager.getRootLogger().getName());
		@SuppressWarnings("unchecked")
		Enumeration<Logger> e = LogManager.getCurrentLoggers();
		while (e.hasMoreElements()) {
			loggers.add(e.nextElement().getName());
		}
		return loggers;
	}

	public static boolean hasLogger(String name)
	{
		return LogManager.getLogger(name) != null;
	}
	
	public static void setLoggerLevel(String name, String level)
	{
		Logger logger = LogManager.getLogger(name);
		if (logger == null) {
			throw new IllegalArgumentException("no such logger: " + name);
		}
		logger.setLevel(Level.toLevel(level));
	}
	
	public static void setLoggerLevel(String[] names, String level)
	{
		final Level lev = Level.toLevel(level);
		for (String name: names) {
			Logger logger = LogManager.getLogger(name);
			if (logger == null) {
				throw new IllegalArgumentException("no such logger: " + name);
			}
			logger.setLevel(lev);
		}
	}
	
	public static String getLoggerLevel(String name)
	{
		Logger logger = LogManager.getLogger(name);
		if (logger == null) {
			throw new IllegalArgumentException("no such logger: " + name);
		}
		return logger.getLevel().toString();
	}
	
	public static void setRootLoggerLevel(String level)
	{
		Logger root = Logger.getRootLogger();
		root.setLevel(Level.toLevel(level));
	}
	
	public static String getRootLoggerLevel()
	{		
		Logger root = Logger.getRootLogger();
		return root.getLevel().toString();
	}
	
	public static final String[] TRIVIAL_LOGGERS  = {
		"DataNucleus.Datastore",
		"DataNucleus.Persistence",
		"DataNucleus.MetaData",
		"org.apache.hadoop.hive.metastore.ObjectStore",
		"org.apache.hadoop.hive.metastore.HiveMetaStore",
		"org.apache.hadoop.util.NativeCodeLoader",
		"org.apache.hadoop.io.compress.zlib.ZlibFactory",
		"org.apache.hadoop.mapred.FileInputFormat",
		"com.hadoop.compression.lzo.GPLNativeCodeLoader",
		"com.hadoop.compression.lzo.LzoCodec",
		"org.apache.hadoop.hive.ql.io.CodecPool",
		"hive.log"
	};
	
	public static final String[] BOGUS_LOGGERS = {
		"DataNucleus.Plugin"
	};
	
	public static void disableCertainLoggers() {

		setLoggerLevel(BOGUS_LOGGERS, "OFF");
		setLoggerLevel(TRIVIAL_LOGGERS, "ERROR");
		
		String loggers;
		loggers = System.getProperty("transport.bogus.loggers");
		if (loggers == null) {
			loggers = System.getenv("TRANSPORT_BOGUS_LOGGERS");
		}
		if (loggers != null && !loggers.isEmpty()) {
			setLoggerLevel(loggers.split(","), "OFF");
		}
		
		loggers = System.getProperty("transport.trivial.loggers");
		if (loggers == null) {
			loggers = System.getenv("TRANSPORT_TRIVIAL_LOGGERS");
		}
		if (loggers != null && !loggers.isEmpty()) {
			setLoggerLevel(loggers.split(","), "ERROR");
		}
	}
}
