package xw.data.common.lang;

import java.io.PrintStream;

public class ExceptionUtils
{
	public static Throwable getRootCause(Throwable t)
	{
		for (; t.getCause() != null; t = t.getCause());
		return t;
	}
	
	public static void printCustom(PrintStream p, Throwable t, final int depth) 
	{
		while (t != null) {
			p.println("Exception: " + t.getMessage());
			if (depth > 0) {
				StackTraceElement[] stackFrames = t.getStackTrace();
				int len = Math.min(depth, stackFrames.length);
				for (int i=0; i<len; i++) {
					StackTraceElement sf = stackFrames[i];
					p.println("  at " + sf.getClassName() + "(" + sf.getFileName() 
								+ ")" + sf.getLineNumber());
				}
			}
			t = t.getCause();
		}
	}
	
	public static void printCompact(PrintStream p, Throwable t) 
	{
		printCustom(p, t, 1);
	}
}
