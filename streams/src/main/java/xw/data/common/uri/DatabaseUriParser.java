package xw.data.common.uri;

import java.net.URI;
import java.net.URISyntaxException;

/*
 * EADP defined database URI syntax:
 * 
 *    <scheme>://<user>@<server>/<database>/<table>?password=...&<other_query>
 *    <scheme>://<server>/<database>/<table>?user=...&password=...&<other_query>
 */
public class DatabaseUriParser
{
	final protected URI _uri;
	final protected String _scheme;
	final protected String _server;
	final protected String _dbName, _tblName;
	final protected String _user, _password;
	final protected GeneralUriQuery _query;	
	// final protected boolean _overwriteTable;
	// final protected Map<String, String> _overwriteRows;
	
	public DatabaseUriParser(URI uri) throws URISyntaxException
	{
		_uri = uri;
		_scheme = uri.getScheme();
			
		String host = uri.getHost();
		int port = uri.getPort();
		if (port > 0) 
			_server = host + ":" + port;
		else 
			_server = host;
		
		String path = UriUtils.decode(uri.getPath());
		if (path == null || path.isEmpty()) {
			_dbName = null;
			_tblName = null;
		}
		else {
			assert path.charAt(0) == '/';
			String[] splitted = path.substring(1).split("/");
			if (splitted.length > 2 || splitted.length < 1) {
				throw new URISyntaxException(_scheme + "://" + path, "bad database uri");
			}
			
			if (splitted.length == 1) {
				_dbName = null;
				_tblName = UriUtils.decode(splitted[0]);
			}
			else {
				_dbName = UriUtils.decode(splitted[0]);
				_tblName = UriUtils.decode(splitted[1]);
			}
		}
		_query =  new GeneralUriQuery(uri.getQuery());	
		
		String userInfo = uri.getUserInfo();
		if (userInfo == null) {
			_user = UriUtils.decode(_query.getParameterAsString("user"));
			_password = UriUtils.decode(_query.getParameterAsString("password"));
		}
		else {
			String[] splitted = userInfo.split(":", 2);
			if (splitted.length == 1) {
				_user = UriUtils.decode(userInfo);
				_password = UriUtils.decode(_query.getParameterAsString("password"));
			}
			else {
				_user = UriUtils.decode(splitted[0]);
				_password = UriUtils.decode(splitted[1]);
			}
		}
		
		if (_user != null && _password == null) {
			throw new URISyntaxException(uri.toString(), "missing password");
		}
		
		_query.removeParameter("user")
			  .removeParameter("password");
	}
	
	public URI getURI()
	{
		return _uri;
	}
	
	public String getScheme()
	{
		return _scheme;
	}
	
	public String getServerAddress()
	{
		return _server;
	}
	
	public String getDatabase()
	{
		return _dbName;
	}
	
	public String getTable()
	{
		return _tblName;
	}
	
	public String getUser()
	{
		return _user;
	}
	
	public String getPassword()
	{
		return _password;
	}
	
	public GeneralUriQuery getQuery()
	{
		return _query;
	}
	
	public static DatabaseUriParser get(URI uri) 
			throws URISyntaxException
	{
		return new DatabaseUriParser(uri);
	}
	
	@Override
	public String toString()
	{
		return _uri.toString();
	}
}
