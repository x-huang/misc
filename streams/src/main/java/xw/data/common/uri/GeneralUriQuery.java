package xw.data.common.uri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneralUriQuery
{
	final protected Map<String, String> _query;
	
	public GeneralUriQuery()
	{
		_query = new HashMap<String, String>();
	}
	
	public GeneralUriQuery(String s)
	{
		Map<String, String[]> multiQuery = parseQueryString(s);
		_query = flatten(multiQuery);
	}
	
	public GeneralUriQuery(Map<String, String> query)
	{
		_query = new HashMap<String, String>(query);
	}
	
	/* http://www.source-code.biz/snippets/java/10.htm
	public static String urlDecode(String s)
	{
		try {
			return URLDecoder.decode(s, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("Error in urlDecode.", e);
		}
	}
	*/
	
	public static Map<String, String[]> parseQueryString(String s)
	{
		if (s == null)
			return new HashMap<String, String[]>(0);
			
		// In map1 we use strings and ArrayLists to collect the parameter
		// values.
		HashMap<String, Object> map1 = new HashMap<String, Object>();
		int p = 0;
		while (p < s.length()) {
			int p0 = p;
			while (p < s.length() && s.charAt(p) != '=' && s.charAt(p) != '&')
				p++;
			String name = UriUtils.decode(s.substring(p0, p));
			if (p < s.length() && s.charAt(p) == '=')
				p++;
			p0 = p;
			while (p < s.length() && s.charAt(p) != '&')
				p++;
			String value = UriUtils.decode(s.substring(p0, p));
			if (p < s.length() && s.charAt(p) == '&')
				p++;
			Object x = map1.get(name);
			if (x == null) {
				// The first value of each name is added directly as a string to
				// the map.
				map1.put(name, value);
			}
			else if (x instanceof String) {
				// For multiple values, we use an ArrayList.
				ArrayList<String> a = new ArrayList<String>();
				a.add((String) x);
				a.add(value);
				map1.put(name, a);
			}
			else {
				@SuppressWarnings("unchecked")
				ArrayList<String> a = (ArrayList<String>) x;
				a.add(value);
			}
		}
		// Copy map1 to map2. Map2 uses string arrays to store the parameter
		// values.
		HashMap<String, String[]> map2 = new HashMap<String, String[]>(
				map1.size());
		for (Map.Entry<String, Object> e : map1.entrySet()) {
			String name = e.getKey();
			Object x = e.getValue();
			String[] v;
			if (x instanceof String) {
				v = new String[] { (String) x };
			}
			else {
				@SuppressWarnings("unchecked")
				ArrayList<String> a = (ArrayList<String>) x;
				v = new String[a.size()];
				v = a.toArray(v);
			}
			map2.put(name, v);
		}
		return map2;
	}
	
	static public Map<String, String> flatten(Map<String, String[]> multi)
	{
		HashMap<String, String> flattened = new HashMap<String, String>();
		for (Map.Entry<String, String[]>entry : multi.entrySet()) {
			String[] values = entry.getValue();
			if (values.length != 1) {
				String msg = "query parameter '" + entry.getKey() + "' has more than one values";
				throw new IllegalArgumentException(msg);
			}
			flattened.put(entry.getKey(), values[0]);
		}
		return flattened;
	}
	
	public String getParameterAsString(String param)
	{
		return _query.get(param);
	}
	
	public Integer getParameterAsInteger(String param)
	{
		String value = _query.get(param);
		return value == null? null: Integer.parseInt(value);
	}
	
	public Long getParameterAsLong(String param)
	{
		String value = _query.get(param);
		return value == null? null: Long.parseLong(value);
	}
	
	private static final String[] yesValues = { 
		"1", "yes", "true", "ok" 
	};
	
	public Boolean getParameterAsYes(String param)
	{
		String value = _query.get(param);
		if (value == null)
			return false;
		for (String yes: yesValues) {		
			if (value.equalsIgnoreCase(yes))
				return true;
		}
		return false;
	}

	public Map<String, String> getAllParameters()
	{
		return _query;
	}
	
	public List<String> getPrefixedParameterNames(String prefix)
	{
		final List<String> prefixed = new ArrayList<>();
		for (String key: _query.keySet()) {
			if (key.startsWith(prefix)) {
				prefixed.add(key);
			}
		}
		return prefixed;
	}
	
	public GeneralUriQuery addParameter(String param, String value)
	{
		_query.put(param, value);
		return this;
	}
	
	public GeneralUriQuery addParameters(Map<String, String> params)
	{
		_query.putAll(params);
		return this;
	}
	
	public GeneralUriQuery addParameterSkippingNull(String param, String value)
	{
		if (value != null)
			_query.put(param, value);
		return this;
	}
	
	public GeneralUriQuery removeParameter(String param)
	{
		if (_query.containsKey(param))
			_query.remove(param);
		return this;
	}
	
	public GeneralUriQuery removeParameters(List<String> params)
	{
		for (String p: params) {
			if (_query.containsKey(p))
				_query.remove(p);
		}
		return this;
	}
		
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry: _query.entrySet()) {
			if (sb.length() > 0) {
				sb.append('&');
			}
			sb.append(entry.getKey() + "=" + entry.getValue());
		}
		return sb.toString();
	}
	
	public String toQueryString()
	{
		String qs = this.toString();
		return qs.length() > 0? "?" + qs: qs; 
	}
}
