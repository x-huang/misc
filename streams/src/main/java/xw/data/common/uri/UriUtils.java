package xw.data.common.uri;

import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Map;

public class UriUtils
{
	public static Charset UTF8_CharSet = Charset.forName("UTF-8");
	public static final boolean[] RFC3986_LEGAL = new boolean[128];

	static {
		for (int i=0; i<128; i++) {
			RFC3986_LEGAL[i] = false;
		}
		String legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=%";  // Intentionally not to encode '%'
		for (char ch: legalChars.toCharArray()) {
			int i = (int) ch;
			RFC3986_LEGAL[i] = true;
		}
	}
	
    public static String encode(final String content) 
    {
    	if (content == null)
    		return null;
    	
    	final ByteBuffer bb = UTF8_CharSet.encode(content);
    	final StringBuilder sb = new StringBuilder();

    	while (bb.hasRemaining()) {
    		final int i = bb.get();
    		if (i < 128 && RFC3986_LEGAL[i]) {
    			sb.append((char)i);
    		}
    		else {
                sb.append("%");
                char hex1 = Character.toUpperCase(Character.forDigit((i >> 4) & 0xF, 16));
                char hex2 = Character.toUpperCase(Character.forDigit(i & 0xF, 16));
                sb.append(hex1);
                sb.append(hex2);
    		}
    	}
    	return sb.toString();
    }

    public static String decode(final String content)
    {
    	if (content == null)
    		return null;
    	
    	final ByteBuffer bb = ByteBuffer.allocate(content.length());
    	final CharBuffer cb = CharBuffer.wrap(content);
        while (cb.hasRemaining()) {
            final char c = cb.get();
            if (c == '%' && cb.remaining() >= 2) {
                final char uc = cb.get();
                final char lc = cb.get();
                final int u = Character.digit(uc, 16);
                final int l = Character.digit(lc, 16);
                if (u != -1 && l != -1) {
                    bb.put((byte) ((u << 4) + l));
                } else {
                    bb.put((byte) '%');
                    bb.put((byte) uc);
                    bb.put((byte) lc);
                }
            } 
            else {
                bb.put((byte) c);
            }
        }
        bb.flip();
        return UTF8_CharSet.decode(bb).toString();
    }
    
	public static URI removeQueryPart(URI uri)
	{
		String scheme = uri.getScheme();
		String authority = uri.getAuthority();
		if (authority == null)
			authority = "";
		String path = uri.getPath();
		return URI.create(scheme + "://" + authority + path);
	}
	
	public static String getLastSegment(URI uri)
	{
		final String path = uri.getPath();
		if (path == null)
			return null;
		String[] segs = path.split("/");
		for (int i=segs.length-1; i>=0; i--) {
			if (!segs[i].isEmpty())
				return segs[i];
		}
		return null;
	}
	
	public static URI getAndRemoveQueryParameters(URI uri, Map<String, String> params)
	{
		final String qs = uri.getQuery();
		if (qs == null || qs.isEmpty()) {
			return uri;
		}
		
		final GeneralUriQuery query = new GeneralUriQuery(qs);
		for (String param: params.keySet()) {
			String value = query.getParameterAsString(param);
			params.put(param, value);
			query.removeParameter(param);
		}
		
		String scheme = uri.getScheme();
		String authority = uri.getAuthority();
		if (authority == null)
			authority = "";
		String path = uri.getPath();
		String newUri = scheme + "://" + authority + path + query.toQueryString();
		return URI.create(UriUtils.encode(newUri));
	}
	
	public static URI addQueryParameters(URI uri, Map<String, String> params)
	{
		final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
		query.addParameters(params);
		
		String scheme = uri.getScheme();
		String authority = uri.getAuthority();
		if (authority == null)
			authority = "";
		String path = uri.getPath();
		String newUri = scheme + "://" + authority + path + query.toQueryString();
		return URI.create(UriUtils.encode(newUri));
	}
}
