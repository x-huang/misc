package xw.data.common.uri;

import java.net.URI;
import java.net.URISyntaxException;

import static xw.data.common.lang.StringUtils.findInSet;

public class UriValidator
{
	public static boolean validateWithException(final String strUri)
			throws URISyntaxException
	{
		final URI uri = new URI(UriUtils.encode(strUri));
		final String scheme = uri.getScheme();
		
		if (scheme == null) {
			throw new URISyntaxException(strUri, "uri has no scheme");
		}
		else if (findInSet(scheme, "hdfs", "s3n", "file")) {
			String path = uri.getPath();
			if (!path.startsWith("/")) {
				throw new URISyntaxException(strUri, "file system uri must use an absolute path");
			}
		}
		else if (findInSet(scheme, "mysql", "mssql", "teradata")) {
			DatabaseUriParser p = DatabaseUriParser.get(uri);
			if (p.getUser() == null) {
				throw new URISyntaxException(strUri, "jdbc uri missing username");
			}
			else if (p.getPassword() == null) {
				throw new URISyntaxException(strUri, "jdbc uri missing password");
			}
		}
		else if (scheme.equals("hive")) {
			DatabaseUriParser.get(uri);
		}
		else if (findInSet(scheme, "scp", "ssh")) {
			String path = uri.getPath();
			if (!path.startsWith("//")) {
				// Note: special treatment to scp uri. Its path starts with double slashes.
				// First one replaces a colon in 'scp' command line argument syntax.
				// Second one means it is an absolute path.
				throw new URISyntaxException(strUri, "scp uri path must starts with double slash");
			}
		}
		else if (scheme.equals("dummy")) {
			String path = uri.getPath();
			if (! findInSet(path, "empty", "null")) {
				throw new URISyntaxException(strUri, "unsupported dummy resource: " + path);
			}
		}
		else if (scheme.equals("stdin")) {
			System.err.println("[WARN] using stdin may unintendedly block a worker thread forever");
		}
		else if (scheme.equals("stdout")) {
			System.err.println("[WARN] using stdout may unintendedly close System.out");
		}
		else {
			throw new URISyntaxException(strUri, "unknown scheme: " + scheme);
		}
		
		return true;
	}
	
	public static void main(String args[])
	{
		if (args.length != 1) {
			System.err.println("[FAIL] expecting one URI");
			return;
		}
		
		String uri = args[0];
		String invalidChars[] = { "$", "{", "}" }; 
		for (String sc: invalidChars) {
			uri = uri.replace(sc, "_");
		}
		System.err.println("[DEBUG] altered uri: " + uri);

		try {
			validateWithException(uri);
			System.err.println("[DONE] validated ok");
		}
		catch (URISyntaxException e) {
			System.err.println("[ERROR] " + e.getMessage());
		}
	}
}
