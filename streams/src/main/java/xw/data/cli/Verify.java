package xw.data.cli;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import xw.data.stream.DataStreams;

public class Verify
{
	static class VerificationResult 
	{
		final protected long _size;
		final protected String _checkSum;
	
		protected VerificationResult(long size, String checkSum) 
		{
			_size = size;
			_checkSum = checkSum;
		}
		
		@Override
		public boolean equals(Object o) 
		{
			if ( !(o instanceof VerificationResult)) {
				return false;
			}
			VerificationResult vr = (VerificationResult)o;
			return _size == vr._size && _checkSum.equals(vr._checkSum);
		}
		
		@Override   
		public String toString()
		{
			return "length=" + _size + ", md5=" + _checkSum;
		}
	}
	
	private static final int BUFFER_SIZE = 2048;
	
	private static VerificationResult doVerify(InputStream in) 
			throws NoSuchAlgorithmException, IOException
	{
		MessageDigest md = MessageDigest.getInstance("MD5");
		final byte[] buffer = new byte[BUFFER_SIZE];
		long size = 0;
		
		while (true) {
			int nbytes = in.read(buffer);
			if (nbytes > 0) {
				size += nbytes;
				md.update(buffer, 0, nbytes);
			}
			if (nbytes == -1)
				break;
		}
		byte[] md5sum = md.digest();
		BigInteger bigInt = new BigInteger(1, md5sum);
		String checkSum = bigInt.toString(16);
		return new VerificationResult(size, checkSum);
	}
	
	private static VerificationResult doVerify(String uri) 
			throws NoSuchAlgorithmException, IOException, URISyntaxException
	{
		InputStream in = DataStreams.getInputStream(uri);
		return doVerify(in);
	}
	
	public static void main(String[] args) 
			throws NoSuchAlgorithmException, IOException, URISyntaxException
	{
		if (args.length == 0) {
			System.err.println("[FAIL] expecting one or two URIs");
		}
		else if (args.length == 1) {
			VerificationResult vr = doVerify(args[0]);
			System.out.println(args[0] + ": " + vr);
		}
		else {
			VerificationResult vr0 = doVerify(args[0]);
			VerificationResult vr1 = doVerify(args[1]);
			if (vr0.equals(vr1)) {
				System.out.println("identical: " + vr0);
			}
			else {
				System.out.println(args[0] + ": " + vr0);
				System.out.println(args[1] + ": " + vr1);
			}			
		}
	}
}
