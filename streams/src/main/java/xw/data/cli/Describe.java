package xw.data.cli;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xw.data.common.lang.StringUtils;
import xw.data.stream.DataStreams;
import xw.data.stream.hadoop.DirectoryRepr;
import xw.data.stream.hadoop.FileStatusRepr;
import xw.data.stream.hadoop.HadoopCommon;

import org.codehaus.jackson.map.ObjectMapper;

public class Describe
{
	private static SimpleDateFormat _sdf = null;
	
	private static void printFormatted(String desc, Object content) 
	{
		System.out.print(String.format("%14s: ", desc));
		System.out.println(content);
	}
	
	private static void printTuple(Object... args) 
	{
		if (args.length > 0) {
			System.out.print(String.format("%8s", args[0].toString()));
		}
		for (int i=1; i<args.length-1; i++) {
			System.out.print(String.format("%8s", " " + args[i]));
		}
		if (args.length > 1) {
			System.out.println(" " + args[args.length-1]);
		}
		else {
			System.out.println();
		}
	}
	
	private static String millisecToDate(long msec) 
	{
		if (_sdf == null)
			_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return _sdf.format(new Date(msec));
	}
	
	private static void printTuple2(Object... args) 
	{
		if (args.length > 0) {
			System.out.print(String.format("%16s", args[0].toString()));
		}
		for (int i=1; i<args.length; i++) {
			System.out.print(String.format("%8s", " " + args[i]));
		}
		System.out.println();
	}
	
	public static void main(String[] args) 
			throws IOException, URISyntaxException 
	{
		boolean json = false;
		boolean brief = false;
		final String uri;
		if (args.length == 2 && args[0].equals("-json")) {
			json = true;
			uri = args[1];
		}
		else if (args.length == 2 && args[0].equals("-brief")) {
			brief = true;
			uri = args[1];
		}
		else if (args.length == 1) {
			uri = args[0];
		}
		else {
			System.err.println("[FAIL] expecting one URI");
			System.err.println("[USAGE] Describe [-json] [-brief] <uri>");
			return;
		}
		
		Object metadata = DataStreams.describeResource(uri);
		if (json) {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println(mapper.defaultPrettyPrintingWriter().writeValueAsString(metadata));
			return;
		}
		
		if (metadata instanceof FileStatusRepr) {
			FileStatusRepr status = (FileStatusRepr) metadata;
			String len = status.isDir? "<dir>" : "" + status.length;
			String filename = status.path.substring(status.path.lastIndexOf("/") + 1);
			printTuple(status.permission, status.owner, len, millisecToDate(status.modificationTime), filename);
		}
		else if (metadata instanceof DirectoryRepr) {
			DirectoryRepr dir = (DirectoryRepr) metadata;
			for (String entry: dir.entries) {
				final String path = uri.endsWith("/")? (uri + entry):(uri+"/" + entry);
				FileStatusRepr status = HadoopCommon.getFileStatusRepr(new URI(path));
				String len = status.isDir? "<dir>" : "" + status.length; 
				printTuple(status.permission, status.owner, len, millisecToDate(status.modificationTime), entry);
			}
		}
		else if (metadata instanceof xw.data.stream.hive.TableRepr) {
			xw.data.stream.hive.TableRepr tbl = (xw.data.stream.hive.TableRepr)metadata;
			if (brief) {
				List<String> cols = new ArrayList<>();
				for (xw.data.stream.hive.ColumnRepr col: tbl.columns) {
					cols.add(col.name);
				}
				for (xw.data.stream.hive.ColumnRepr col: tbl.partitionColumns) {
					cols.add(col.name);
				}
				System.out.println(StringUtils.join(cols, ","));
			}
			else {
				printFormatted("data location", tbl.dataLocation);
				printTuple("# non-partition columns");
				for (xw.data.stream.hive.ColumnRepr col: tbl.columns) {
					printTuple2(col.name, col.type);
				}
				printTuple("# partition columns");
				for (xw.data.stream.hive.ColumnRepr col: tbl.partitionColumns) {
					printTuple2(col.name, col.type);
				}
			}
		}
		else if (metadata instanceof xw.data.stream.jdbc.TableRepr) {
			xw.data.stream.jdbc.TableRepr tbl = (xw.data.stream.jdbc.TableRepr)metadata;
			if (brief) {
				List<String> cols = new ArrayList<String>();
				for  (xw.data.stream.jdbc.ColumnRepr col: tbl.columns) {
					cols.add(col.name);
				}
				System.out.println(StringUtils.join(cols, ","));
			}
			else {
				printTuple("# schema");
				for (xw.data.stream.jdbc.ColumnRepr col: tbl.columns) {
					String cs = col.caseSensitive? "yes":"no";
					String nullable = col.nullable.equals("columnNullable")? "yes" 
										: (col.nullable.equals("columnNoNulls")? "no" : "n/a");
					String desc = String.format("(ds: %d, cs: %s, n: %s)", 
											      col.displaySize, cs, nullable);
					printTuple2(col.name, col.typeName.toLowerCase(), desc);
				}
			}
		}
		else {
			System.err.println("[ERROR] unrecognized metadata type: " + metadata.getClass().getName());
		}
	}
}

/* 
###  test script ### 

# list a directory
java xw.data.cli.Describe hdfs:///user/hadoop/
 
# stat a file
java xw.data.cli.Describe hdfs:///user/hadoop/startHive.sh
 
# describe a hive table
java xw.data.cli.Describe hive:///default/tbl

# describe a hive partition
java xw.data.cli.Describe "hive:///default/tbl?describe=partition&dt=2013-04-20&hour=20&service=tw-2012-xbl2-demo"

# list partitions
java xw.data.cli.Describe "hive:///default/tbl?describe=partitions&dt=2013-04-20&hour=20"

# describe a mysql table
java xw.data.cli.Describe mysql://xxx@mysql.server.host:3306/db/tbl?password=xxx
 
# describe teradata table
java xw.data.cli.Describe "teradata://teradata.server.host/AAAA_DB/BBBB_TBL?user=xxx&password=xxxxxxx"

*/
