package xw.data.cli;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.RCFileRecordReader;
import org.apache.hadoop.hive.serde2.columnar.BytesRefArrayWritable;
import org.apache.hadoop.hive.serde2.columnar.BytesRefWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.FileSplit;

import xw.data.common.uri.GeneralUriQuery;
import xw.data.stream.DataInputStream;
import xw.data.stream.IResourceOperator;
import xw.data.stream.ObjectInput;
import xw.data.stream.Protocols;
import xw.data.stream.hadoop.HadoopCommon;

public class RCFileCat
{
	public static class RCFileObjectInput implements ObjectInput
	{
		private final RCFileRecordReader<LongWritable, BytesRefArrayWritable> _reader;
		private long _count;
		
		transient private LongWritable _key = new LongWritable();
		transient private BytesRefArrayWritable _value = new BytesRefArrayWritable();
		
		private static CharsetDecoder _decoder;
		static {
			_decoder = Charset.forName("UTF-8").newDecoder()
					.onMalformedInput(CodingErrorAction.REPLACE)
					.onUnmappableCharacter(CodingErrorAction.REPLACE);
		}
		
		public RCFileObjectInput(URI rcfile) throws IOException 
		{
			Path path = new Path(rcfile);
			long length = HadoopCommon.getFileStatusRepr(rcfile).length;
			FileSplit split = new FileSplit(path, 0, length, (String[])null);
			_reader = new RCFileRecordReader<>(new Configuration(), split);
			_count = 0;
		}
		
		@Override
		public Object read() throws IOException
		{
			if (_reader.next(_key, _value)) {
				int n = _value.size();
				Object[] row = new Object[n];
				for (int i=0; i<n; i++) {
					BytesRefWritable v = _value.unCheckedGet(i);
					ByteBuffer bb = ByteBuffer.wrap(v.getData(), v.getStart(), v.getLength());
					row[i] = _decoder.decode(bb).toString();
				}
				_count++;
				return row;
			}
			else {
				return null;
			}
		}

		@Override
		public long getCount()
		{
			return _count;
		}

		@Override
		public void close() throws IOException
		{
			_reader.close();
		}
	}
	
	public static class MultipleRCFileObjectInput  implements ObjectInput
	{
		final private List<URI> _rcfiles;
		private int _index;
		private long _count;
		private ObjectInput _curr;
		
		public MultipleRCFileObjectInput(List<URI> rcfiles) throws IOException 
		{
			_rcfiles = rcfiles;
			_index = 0;
			_curr = new RCFileObjectInput(_rcfiles.get(_index));
			_count = 0;
		}
		
		@Override
		public Object read() throws IOException
		{
			Object data = _curr.read();
			if (data != null)
				return data;
			
			if (_index < _rcfiles.size() -1) {
				_curr.close();
				_count += _curr.getCount();
				_index++;
				_curr = new RCFileObjectInput(_rcfiles.get(_index));
				return read();
			}
			else {
				return null;
			}
		}

		@Override
		public long getCount()
		{
			return _count + _curr.getCount();
		}

		@Override
		public void close() throws IOException
		{
			_curr.close();
		}
	}
	
	public static class RCFileResourceOperator implements IResourceOperator 
	{
		@Override
		public InputStream createInputStream(URI uri)
				throws URISyntaxException, IOException
		{
			final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
			final List<URI> rcfiles = new ArrayList<>();
			String location = query.getParameterAsString("uri");
			URI locUri = new URI(location);
			if (HadoopCommon.getFileStatusRepr(locUri).isDir) {
				if (! location.endsWith("/"))
					location += "/";
				for (String file: HadoopCommon.listFilesRecursively(locUri)) {
					rcfiles.add(new URI(location + file));
				}
			}
			else {
				rcfiles.add(locUri);
			}
			
			String format = query.getParameterAsString("format");
			if (format == null || format.isEmpty()) {
				format = "json";
			}
			
			query.removeParameter("format").removeParameter("uri");
			if (query.getAllParameters().size() > 0) {
				System.err.println("[WARN] unused query options: " + query.toString());
			}
			
			final ObjectInput objectInput;
			if (rcfiles.size() == 0) {
				throw new IllegalArgumentException("no rc files");
			}
			else if	(rcfiles.size() == 1) {
				objectInput = new RCFileObjectInput(rcfiles.get(0));
			}
			else {
				objectInput = new MultipleRCFileObjectInput(rcfiles);
			}
			
			return new DataInputStream(objectInput, format);
		}

		@Override
		public OutputStream createOutputStream(URI uri)
		{
			throw new UnsupportedOperationException("writing to rcfile not supported yet");
		}

		@Override
		public Object describeResource(URI uri) throws URISyntaxException,
				IOException
		{
			final GeneralUriQuery query = new GeneralUriQuery(uri.getQuery());
			final List<String> rcfiles = new ArrayList<>();
			String location = query.getParameterAsString("uri");
			URI locUri = new URI(location);
			if (HadoopCommon.getFileStatusRepr(locUri).isDir) {
				if (! location.endsWith("/"))
					location += "/";
				for (String file: HadoopCommon.listFilesRecursively(locUri)) {
					rcfiles.add(location + file);
				}
			}
			else {
				rcfiles.add(location);
			}
			return rcfiles;
		}
	}
	
	public static void main(String[] args) 
			throws IOException, URISyntaxException 
	{	
		Protocols.registerOperatorClass(RCFileResourceOperator.class, "rcfile");
		Dump.main(args);
	}
}

/*
hadoop@node72-250:/mnt/transport-cli$ time ./transport.sh RCFileCat "rcfile://?uri=/hive/warehouse/hdstest/telemetry-stest-hourly/dt=2013-07-31/hour=23/service=sims4-2013-pc&format=txt" 2> /dev/null | wc
  44283  575679 13429969

real	0m4.040s
user	0m6.416s
sys	0m1.052s

hadoop@node72-250:/mnt/transport-cli$ time ./transport.sh Dump "hive:///hdstest/telemetry_hourly_stest_tbl?dt=2013-07-31&hour=23&service=sims4-2013-pc&format=txt" 2> /dev/null | wc
  44283 1309510 15359441

real	0m8.143s
user	0m12.857s
sys	0m1.304s

hadoop@node72-250:/mnt/transport-cli$ head  -1 rcfile.result 
xxxx	2677605426	d8_d3_85_95_90_70	281477129509234	enUS	$4c7c8ab851f99a28	4a	3	GAMESESS	STRT	rtim=7773&cdur=7773&gmod=LiveMode&txv_=0&pji_=20FD2&rel_=p&ver_=0.0.0.0&pid_=0&gid_=D79A6A7051F99A72&svts=1375312500	sims4-2013-pc

hadoop@node72-250:/mnt/transport-cli$ head -1 hive.result 
xxxx	2677605426	d8_d3_85_95_90_70	281477129509234	enUS	$4c7c8ab851f99a28	4a	3	GAMESESS	STRT	{svts=1375312500, gid_=D79A6A7051F99A72, rtim=7773, ver_=0.0.0.0, rel_=p, gmod=LiveMode, pji_=20FD2, cdur=7773, txv_=0, pid_=0}	sims4-2013-pc	2013-07-31	23	sims4-2013-pc
 */ 