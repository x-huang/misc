package xw.data.cli;

import java.net.URI;

import xw.data.common.uri.DatabaseUriParser;
import xw.data.common.uri.UriUtils;
import xw.data.stream.jdbc.JdbcCommon;

public class ExecuteSQL
{
	public static void main(String args[]) throws Exception
	{
		if (args.length != 2) {
			System.err.println("[FAIL] expceting two command line arguments");
			System.err.println("[USAGE] ExecuteSQL <database_uri> <sql>, or");
			return;
		}
		
		final URI uri = new URI(UriUtils.encode(args[0]));
		String sql = args[1];
		System.err.println("[DEBUG] initial SQL: " + sql);
		
		{
			final DatabaseUriParser parser = new DatabaseUriParser(uri);
			sql = sql.replace("${database}", parser.getDatabase());
			sql = sql.replace("${table}", parser.getTable());
		}
		
		System.err.println("[EXEC] " + sql);
		int rows = JdbcCommon.executeUpdate(uri, sql);
		System.err.println("[DONE] " + rows + " rows of data involved");
	}
	
}
