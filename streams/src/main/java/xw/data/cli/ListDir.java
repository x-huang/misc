package xw.data.cli;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import xw.data.stream.hadoop.HadoopCommon;

// Copy all files in source directory to the target directory
public class ListDir
{
	public static void main(String[] args) 
			throws IOException, InstantiationException, 
				   IllegalAccessException, ClassNotFoundException
	{
		if (args.length != 1) {
			System.err.println("[FAIL] expecting one URI");
			return;
		}

		List<String> files = HadoopCommon.listEntries(URI.create(args[0]));
		if (files == null) {
		    System.err.println("[ERROR] no file/directory is found in the directory");
		}
		else {
            for (String file : files) {
                System.out.println(file);
            }
        }
	}
}
