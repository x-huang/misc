package xw.data.cli;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import xw.data.common.lang.LoggerUtils;
import xw.data.stream.hadoop.FileStatusRepr;
import xw.data.stream.hadoop.HadoopCommon;
import xw.data.stream.hive.HiveCommon;
import xw.data.stream.hive.PartitionRepr;

/*
 * Give rough size of a hive table partition
 */
public class PartSize
{
	private static void printFormatted(String desc, Object content) 
	{
		System.out.print(String.format("%14s: ", desc));
		System.out.println(content);
	}
	
	private static void printTuple(Object... args) 
	{
		if (args.length > 0) {
			System.out.print(String.format("%8s", args[0].toString()));
		}
		for (int i=1; i<args.length; i++) {
			System.out.print(String.format("%8s", args[i]));
		}
		System.out.println();
	}
	
	public static void main(String[] args) throws IOException, URISyntaxException
	{
		LoggerUtils.disableCertainLoggers();
		
		if (args.length != 1) {
			System.err.println("[FAIL] expect a Hive URI with full partition specification");
			return;
		}
		
		final URI uri = new URI(args[0]);
		final PartitionRepr part = (PartitionRepr) HiveCommon.getPartitionRepr(uri);
		
		printFormatted("partition uri", args[0]);
		printFormatted("input format", part.inputFormatClass);
		printFormatted("output format", part.outputFormatClass);
		List<String> files  = HadoopCommon.listFiles(new URI(part.dataLocation));
		long bytes = 0;
		for (String f: files) {
			String path = part.dataLocation + "/" + f;
			FileStatusRepr status = HadoopCommon.getFileStatusRepr(new URI(path));
			printTuple(status.length, f);
			bytes += status.length;
		}
		printFormatted("total size", bytes);
	}

}
