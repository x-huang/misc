package xw.data.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import xw.data.stream.DataStreams;

/* Warning: not meant for binary data, use Copy <src> stdout:/// instead */

public class Dump
{
	public static void main(String[] args) 
			throws IOException, URISyntaxException 
	{
		if (args.length != 1) {
			System.err.println("[FAIL] expecting one URI");
			return;
		}
		
		String uri = args[0];
		InputStream in = DataStreams.getInputStream(uri);
		BufferedReader br = new BufferedReader(new InputStreamReader((in)));
		while (true) {
			String line = br.readLine();
			if (line == null)
				break;
			System.out.println(line);
		}
	}
}
