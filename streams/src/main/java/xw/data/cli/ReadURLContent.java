package xw.data.cli;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/* 
 * To run:
 *	
 *	java -Djava.protocol.handler.pkgs=xw.data.protocols \
 * 		xw.data.transport.cli.ReadURLContent dummy:///empty
 */
public class ReadURLContent
{
	public static String convertStreamToString(InputStream in) 
	{
	    Scanner scanner = new Scanner(in);
	    scanner.useDelimiter("\\A");
	    String s = scanner.hasNext()? scanner.next() : "";
	    scanner.close();
	    return s;
	}
	
	public static void main(String args[]) throws Exception
	{
		if (args.length != 1) {
			System.err.println("usage: ReadURLContent <resource-url>");
			System.err.println("   ex: java -Djava.protocol.handler.pkgs=xw.data.protocols xw.data.transport.cli.ReadURLContent dummy:///empty");
			return;
		}
		
		URL url = new URL(args[0]);
		URLConnection conn = url.openConnection();
		conn.connect();
		
		InputStream in = conn.getInputStream();
		System.out.println(convertStreamToString(in));
	}	
}
