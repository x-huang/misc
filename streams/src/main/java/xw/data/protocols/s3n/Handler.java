package xw.data.protocols.s3n;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import xw.data.stream.ProtocolHandler;

public class Handler extends URLStreamHandler
{
	@Override
	protected URLConnection openConnection(URL url) throws IOException
	{
		assert url.getProtocol().equals("s3n");
		
		return ProtocolHandler.openUriAsConnection(url);
	}
}
