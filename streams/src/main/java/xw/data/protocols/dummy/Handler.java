package xw.data.protocols.dummy;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import xw.data.stream.ProtocolHandler;

public class Handler extends URLStreamHandler
{
	@Override
	protected URLConnection openConnection(URL url) throws IOException
	{
		assert url.getProtocol().equals("dummy");
		
		return ProtocolHandler.openUriAsConnection(url);
	}
}
