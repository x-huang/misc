package xw.data.eaql.env.unistreams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.eaql.query.QueryEnvironment;
import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.resources.ResourceDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xhuang on 8/28/16.
 */
public class UnistreamsEnv implements QueryEnvironment
{
    private static final Logger logger = LoggerFactory.getLogger(UnistreamsEnv.class);
    public static final String SchemaCacheFile = ".schemacache";

    private static final Pattern regexTableName = Pattern.compile("^[_a-zA-Z][_a-zA-Z0-9]*(\\.[_a-zA-Z][_a-zA-Z0-9]*)?$");

    private static String hiveTableToResourceDescriptor(String s) {
        final Matcher matcher = regexTableName.matcher(s);
        if (matcher.matches()) {
            if (matcher.group(1) == null) {
                return "hive:(default." + s + ")";
            } else {
                return "hive:(" + s + ")";
            }
        } else {
            return s;
        }
    }

    private static final UnistreamsEnv singleton = new UnistreamsEnv();

    private final Map<String, Schema> schemaCache = new ConcurrentHashMap<>();

    private UnistreamsEnv() {
    }

    public static UnistreamsEnv get() {
        return singleton;
    }

    public synchronized void loadSchemaCache(String filename) throws IOException {
        final File file = new File(filename);
        if (file.createNewFile()) {
            logger.info("schema cache file not found, created new");
        }

        try (Scanner scanner = new Scanner(new FileInputStream(file), "UTF-8")) {
            while (scanner.hasNextLine()) {
                final String line = scanner.nextLine().trim();
                if (line.isEmpty() || line.startsWith("#")) {
                    continue;
                }
                final String[] splits = line.split("\t", 2);
                if (splits.length != 2) {
                    logger.error("unrecognized schema definition: " + line);
                } else {
                    final String rd = splits[0].trim();
                    final String schema = splits[1].trim();
                    schemaCache.put(rd, Schema.fromString(schema));
                }
            }
        }
    }

    public void loadSchemaCache() throws IOException {
        loadSchemaCache(SchemaCacheFile);
    }

    public synchronized void saveSchemaCache(String filename) throws IOException {
        final String tmpFile = filename + ".tmp";
        Files.deleteIfExists(Paths.get(tmpFile));
        final File file = new File(tmpFile);
        file.createNewFile();
        try (final PrintStream ps = new PrintStream(new FileOutputStream(file))) {
            final List<String> rds = new ArrayList<>(schemaCache.keySet());
            Collections.sort(rds);
            for (String rd: rds) {
                ps.append(rd).append('\t').append(schemaCache.get(rd).toString()).append('\n');
            }
        }
        Files.move(Paths.get(tmpFile), Paths.get(filename),
                StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING);
    }

    public void saveSchemaCache() throws IOException {
        saveSchemaCache(SchemaCacheFile);
    }


    public Map<String, Schema> getSchemaCache() {
        return new HashMap<>(schemaCache);
    }

    @Override
    public Schema getSchema(String s) {
        s = hiveTableToResourceDescriptor(s);
        final ResourceDescriptor rd = ResourceDescriptor.parse(s);

        if (schemaCache.containsKey(s)) {
            logger.debug("found schema of '" + s + "' in cache");
            return schemaCache.get(s);
        }

        logger.debug("getting schema from: {}", s);
        try {
            final Schema schema =  Schema.fromString(rd.op().metadata().getSchema().toString());
            schemaCache.put(s, schema);
            return schema;
        } catch (IOException e) {
            throw new RuntimeException("cannot read schema from '" + s + "': " + e.getMessage(), e);
        }
    }

    @Override
    public DataSource getDataSource(String s) {
        s = hiveTableToResourceDescriptor(s);
        logger.debug("getting data stream from: {}", s);
        final ResourceDescriptor rd = ResourceDescriptor.parse(s);
        return rd.source();
    }

    @Override
    public DataSink getDataSink(String s, Schema schema) {
        final ResourceDescriptor rd = ResourceDescriptor.parse(s);
        if (schema != null) {
            rd.params.put("schema", "struct<" + schema.toString() + ">");
        }
        return rd.sink();
    }
}
