package xw.data.eaql.cli;

import xw.data.eaql.EAQL;

import java.io.IOException;

/**
 * Created by xhuang on 8/15/16.
 */
public class ParseType
{
    public static void main(String[] args) throws IOException {
        System.out.println(EAQL.parseDataType(System.in));
    }
}

/*

$ echo 'struct<col1:map<int,string>,col2:array<timestamp>,col3:union<smallint,tinyint,bool,long>,col4:map<string,array<struct<a:char>>>>' | java xw.data.eaql.cli.ParseType
struct<col1:map<int,string>,col2:array<timestamp>,col3:union<smallint,tinyint,boolean,bigint>,col4:map<string,array<struct<a:char>>>>

$ echo 'array<union<string,int,map<tinyint,struct<col1:timestamp>>>>' | java xw.data.eaql.cli.ParseType
array<union<string,int,map<tinyint,struct<col1:timestamp>>>>


 */