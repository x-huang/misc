package xw.data.eaql.cli;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import xw.data.eaql.parser.EAQLLexer;
import xw.data.eaql.parser.EAQLParser;
import xw.data.antlr4.common.GenericAstPrinter;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.*;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by xhuang on 10/19/15.
 */
public class PrintAST
{
    private enum Target { stmt, expr, type }

    public static void main(String[] args) throws IOException {

        final Target target;
        {

            final OptionParser parser = new OptionParser();
            parser.acceptsAll(Arrays.asList("stmt", "s"), "parse a statement");
            parser.acceptsAll(Arrays.asList("expr", "e"), "parse an expression");
            parser.acceptsAll(Arrays.asList("type", "t"), "parse a type string");
            parser.acceptsAll(Arrays.asList("help", "h"), "print usage").isForHelp();

            final OptionSet opts = parser.parse(args);
            if (opts.has("help")) {
                System.out.println("[intro] print EAQL element abstract syntax tree");
                parser.printHelpOn(System.out);
                return;
            }

            if (opts.has("stmt")) {
                target = Target.stmt;
            } else if (opts.has("expr")) {
                target = Target.expr;
            } else if (opts.has("type")) {
                target = Target.type;
            } else {
                target = Target.stmt;
            }
        }

        final EAQLLexer lexer = new EAQLLexer(new ANTLRInputStream(System.in));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final EAQLParser parser = new EAQLParser(tokens);
        final ParseTreeVisitor<Void> printer = new GenericAstPrinter();

        switch (target) {
            case stmt:
                printer.visit(parser.sql_stmt());
                break;
            case expr:
                printer.visit(parser.expr());
                break;
            case type:
                printer.visit(parser.data_type());
                break;
        }
    }
}
