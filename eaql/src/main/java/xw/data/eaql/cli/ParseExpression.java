package xw.data.eaql.cli;

import xw.data.eaql.EAQL;
import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.Expressions;
import xw.data.eaql.query.util.ExprUtils;

import java.io.IOException;

/**
 * Created by xhuang on 10/20/15.
 */
public class ParseExpression
{
    public static void main(String[] args) throws IOException {
        final Expression e = EAQL.parseExpression(System.in);
        System.out.println(e.toString());
        System.out.println("[cloned] " + Expressions.clone(e));
        System.out.println("[reduced] " + ExprUtils.reduce(e));
    }
}


/*

echo '
SELECT sum(x) as sum, 1*2.0 as number, (len(s) > 10 AND i>100) as flag, z, CAST(i as string)
                FROM a.b
                WHERE x * y + z > 0 AND find(s, "abc") IS NOT NULL AND
                ( s LIKE "%xyz%" OR f NOT BETWEEN 0.1 AND 0.2 OR NOT (y > 1.0) ) ' \
| java xw.data.eaql.cli.ParseQuery \
    a.b=i:int,s:string,f:float,x:int,y:double,z:timestamp \
    a.c=i:int,s:string,f:float,x:int,y:double,z:timestamp
2016-08-16 15:15:07 INFO ParseQuery - output schema: sum:double,number:double,flag:boolean,z:timestamp,i:string
SELECT sum(x) AS sum, 1 * 2.0 AS number, (len(s) > 10 AND i > 100) AS flag, z, CAST (i AS string) FROM a.b WHERE x * y + z > 0 AND find(s, "abc") IS NOT NULL AND (s LIKE "%xyz%" OR f NOT BETWEEN 0.1 AND 0.2 OR NOT (y > 1.0))

*/