package xw.data.eaql.cli;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.eaql.EAQL;
import xw.data.eaql.env.unistreams.UnistreamsEnv;
import xw.data.eaql.model.stmt.SQLStatement;
import xw.data.eaql.query.QueryBuilder;
import xw.data.eaql.query.SQLQuery;
import xw.data.eaql.query.SQLRead;
import xw.data.eaql.query.SQLWrite;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by xhuang on 8/28/16.
 */
public class Query
{
    private static final Logger logger = LoggerFactory.getLogger(Query.class);

    private enum Action {
        DRY_RUN, DUMP_DATA, DUMP_TYPE
    }

    private static Action decideAction(OptionSet opts) {
        if (opts.has("dry-run")) return Action.DRY_RUN;
        if (opts.has("dump-type") && opts.has("dump-data")) {
            throw new IllegalArgumentException("conflicting flags: 'dump-data' and 'dump-type'");
        }
        if (opts.has("dump-data")) return Action.DUMP_DATA;
        if (opts.has("dump-type")) return Action.DUMP_TYPE;
        return Action.DUMP_DATA;
    }

    private static List<SQLStatement> getStatements(OptionSet opts) throws IOException {
        if (opts.has("stmt")) {
            final List<SQLStatement> stmts = new ArrayList<>();
            for (Object v: opts.valuesOf("stmt")) {
                stmts.add(EAQL.parseStatement((String) v));
            }
            return stmts;
        } else {
            return EAQL.parseScript(System.in);
        }
    }

    public static void main(String[] args) throws Exception {

        final Action action;
        final List<SQLStatement> stmts;

        {
            final OptionParser parser = new OptionParser();
            parser.accepts("dump-type", "print result types only");
            parser.accepts("dry-run", "only to test statement syntax correctness");
            parser.accepts("stmt", "query statement, repeatable, if omitted read from stdin").withOptionalArg();
            parser.acceptsAll(Arrays.asList("help", "h"), "print usage").isForHelp();

            final OptionSet opts = parser.parse(args);
            if (opts.has("help")) {
                System.out.println("[intro] executing EAQL queries");
                parser.printHelpOn(System.out);
                return;
            }

            action = decideAction(opts);
            stmts = getStatements(opts);
        }

        for (SQLStatement stmt: stmts) {

            logger.info("executing statement:\n{}", stmt);

            final SQLQuery query = QueryBuilder.build(UnistreamsEnv.get(), stmt);
            if (query instanceof SQLRead) {
                final SQLRead sqlRead = (SQLRead) query;
                logger.info("output schema: {}", sqlRead.getSchema());
                try (final DataSource stream = sqlRead.getDataSource()) {
                    logger.debug("output data stream: {}", stream);
                    if (action == Action.DUMP_DATA) {
                        stream.initialize();
                        DataStreams.dump(stream, System.out);
                    } else if (action == Action.DUMP_TYPE) {
                        stream.initialize();
                        DataStreams.dumpJavaTypes(stream, System.out);
                    } else {
                        logger.info("dry-run: no actions taken");
                    }
                }
            } else if (query instanceof SQLWrite) {
                final SQLWrite sqlWrite = (SQLWrite) query;
                logger.info("data schema: {}", sqlWrite.getSchema());
                if (action == Action.DRY_RUN) {
                    logger.info("dry-run: no actions taken");
                } else {
                    sqlWrite.doWrite();
                }
            }

        }
    }

}
