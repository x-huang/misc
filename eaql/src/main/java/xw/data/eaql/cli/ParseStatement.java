package xw.data.eaql.cli;

import xw.data.eaql.EAQL;

import java.io.IOException;

/**
 * Created by xhuang on 10/19/15.
 */
public class ParseStatement
{
    public static void main(String[] args) throws IOException {
        System.out.println(EAQL.parseStatement(System.in).toString());
    }
}
