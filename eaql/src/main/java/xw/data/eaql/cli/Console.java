package xw.data.eaql.cli;

import jline.console.ConsoleReader;
import jline.console.history.FileHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.eaql.env.unistreams.UnistreamsEnv;
import xw.data.eaql.query.QueryBuilder;
import xw.data.eaql.query.SQLQuery;
import xw.data.eaql.query.SQLRead;
import xw.data.eaql.query.SQLWrite;
import xw.data.eaql.query.eval.udaf.UDAFs;
import xw.data.eaql.query.eval.udf.UDFs;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.util.DateTimeUtils;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreams;
import xw.data.unistreams.setting.GlobalSetting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * Created by xhuang on 7/3/2015.
 */
public class Console
{
    private final static Logger logger = LoggerFactory.getLogger(Console.class);
    private final static String JLineHistoryFile = ".history";
    private final static String SchemaCacheFile = ".schemacache";

    private static class TerminationException extends Exception {}

    private static final ExecutorService executor = Executors.newSingleThreadExecutor();
    private static Future<Boolean> currentFuture = null;

    static abstract class Command implements Callable<Boolean> {
        protected List<String> args = new ArrayList<>();
        public Command addArgs(List<String> args) {
            this.args.addAll(args);
            return this;
        }
        Command addArg(String arg) {
            this.args.add(arg);
            return this;
        }
    }
    private static class Quit extends Command {
        @Override
        public Boolean call() throws Exception {
            throw new TerminationException();
        }
    }

    private static class Help extends Command {
        @Override
        public Boolean call() throws Exception {
            System.out.println("[ EAQL JLine-enabled console ]");
            System.out.println("Supported commands:");
            System.out.println("  quit/exit - exit this console program");
            System.out.println("  help - print this help screen");
            System.out.println("  set <name>=<value> - set global variable which can be used as property value in resource string");
            System.out.println("  unset <name> - unset global variable, or '*' to clear settings");
            System.out.println("  show settings - print global settings");
            System.out.println("  show grammar - print core part of EAQL antlr4 grammar");
            System.out.println("  show UDFs - print supported UDFs");
            System.out.println("  show UDAFs - print supported UDAFs");
            System.out.println("  show cached schema - print cached schema");
            System.out.println("  <eaql-stmts> - SQL-like queries; use 'show grammar' for EAQL antlr4 grammar; see test/*.ql for examples");
            System.out.println("Supported hot keys:");
            System.out.println("  <up>/<down>, ^P/^N - browse historical input lines");
            System.out.println("  ^S/^R - search/reverse-search historical inputs");
            System.out.println("  ^L - clear screen");
            System.out.println("  ^C - quit current running query");
            System.out.println("  ^D - end console session");
            return true;
        }
    }

    private static class Set extends Command {
        @Override
        public Boolean call() throws Exception {
            GlobalSetting.set(args.get(0), args.get(1));
            return true;
        }
    }

    private static class Unset extends Command {
        @Override
        public Boolean call() throws Exception {
            if (args.get(0).equals("*")) {
                GlobalSetting.clear();
            } else {
                GlobalSetting.unset(args.get(0));
            }
            return true;
        }
    }

    private static class ShowSettings extends Command {
        @Override
        public Boolean call() throws Exception {
            final Properties props = GlobalSetting.getCopy();
            final List<String> keys = new ArrayList<>(props.stringPropertyNames());
            Collections.sort(keys);
            for (String key: keys) {
                System.out.println(key + " = " + props.getProperty(key));
            }
            return true;
        }
    }

    private static class ShowGrammar extends Command {
        @Override
        public Boolean call() throws Exception {
            final InputStream in = Console.class.getResourceAsStream("/EAQL.g4");
            if (in == null) {
                System.out.println("g4 file not found in resource root");
            } else {
                final Scanner scanner = new Scanner(in, "UTF-8");
                while (scanner.hasNextLine()) {
                    System.out.println(scanner.nextLine());
                }
                scanner.close();
            }
            return true;
        }
    }

    private static class ShowUDFs extends Command {
        @Override
        public Boolean call() throws Exception {
            final List<String> udfs = UDFs.getAll();
            Collections.sort(udfs);
            for (String udf: udfs) {
                System.out.println("  " + udf);
            }
            return true;
        }
    }

    private static class ShowUDAFs extends Command {
        @Override
        public Boolean call() throws Exception {
            final List<String> udafs = UDAFs.getAll();
            Collections.sort(udafs);
            for (String udaf: udafs) {
                System.out.println("  " + udaf);
            }
            return true;
        }
    }

    private static class ShowCachedSchema extends Command {
        @Override
        public Boolean call() throws Exception {
            final Map<String, Schema> cache = UnistreamsEnv.get().getSchemaCache();
            final List<String> resources = new ArrayList<>(cache.keySet());
            Collections.sort(resources);
            for (String resource: resources) {
                System.out.println(resource + "\t" + cache.get(resource));
            }
            return null;
        }
    }

    private static class ExecuteQuery extends Command {
        @Override
        public Boolean call() throws Exception {
            final SQLQuery query = QueryBuilder.build(UnistreamsEnv.get(), args.get(0));
            if (query instanceof SQLRead) {
                final SQLRead sqlRead = (SQLRead) query;
                logger.info("output schema: {}", sqlRead.getSchema());
                final DataSource stream = sqlRead.getDataSource();
                logger.debug("output data stream: {}", stream);
                stream.initialize();
                DataStreams.dump(stream, System.out);
                stream.close();
            } else if (query instanceof SQLWrite) {
                final SQLWrite sqlWrite = (SQLWrite) query;
                logger.debug("data schema: {}", sqlWrite.getSchema());
                sqlWrite.doWrite();
                logger.debug("data written");
            }
            return true;
        }
    }

    private static class CommandPattern {
        final Pattern pattern;
        final Class<? extends Command> cmdClass;

        CommandPattern(String regex, Class<? extends Command> cmdClass) {
            this.pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            this.cmdClass = cmdClass;
        }

        Command match(String s) {
            final Matcher matcher = pattern.matcher(s);
            if (matcher.matches()) {
                final Command cmd;
                try {
                    final Constructor<? extends Command> ctor = cmdClass.getDeclaredConstructor();
                    ctor.setAccessible(true);
                    cmd = ctor.newInstance();
                } catch (Exception e) {
                    logger.error("cannot instantiate class: {}", cmdClass.getName());
                    throw new RuntimeException("cannot create instance of " + cmdClass.getSimpleName(), e);
                }
                for (int i=1; i<=matcher.groupCount(); i++) {
                    cmd.addArg(matcher.group(i));
                }
                return cmd;
            } else {
                return null;
            }
        }
    }

    private static final CommandPattern[] cmdPatterns = {
            new CommandPattern("\\s*(quit|exit)\\s*;[;\\s]*", Quit.class),
            new CommandPattern("\\s*help\\s*;[;\\s]*", Help.class),
            new CommandPattern("\\s*set\\s+([^\\s=]+)\\s*=\\s*([^\\s]*)\\s*;[;\\s]*", Set.class),
            new CommandPattern("\\s*unset\\s+([^\\s=]+)\\s*;[;\\s]*", Unset.class),
            new CommandPattern("\\s*show\\s+settings\\s*;[;\\s]*", ShowSettings.class),
            new CommandPattern("\\s*show\\s+grammar\\s*;[;\\s]*", ShowGrammar.class),
            new CommandPattern("\\s*show\\s+udfs?\\s*;[;\\s]*", ShowUDFs.class),
            new CommandPattern("\\s*show\\s+udafs?\\s*;[;\\s]*", ShowUDAFs.class),
            new CommandPattern("\\s*show\\s+cached\\s+schema\\s*;[;\\s]*", ShowCachedSchema.class),
            new CommandPattern("(.+)", ExecuteQuery.class)
    };

    private static Command dispatch(String s) {
        for (CommandPattern cp: cmdPatterns) {
            final Command cmd = cp.match(s);
            if (cmd != null) return cmd;
        }
        throw new IllegalStateException("cannot dispatch command: " + s);
    }

    private static void execute(String s) throws TerminationException {
        if (currentFuture != null) {
            throw new IllegalStateException("cannot execute '" + s + "', current future is not clear");
        }
        final Command cmd = dispatch(s);
        final long startTime = System.currentTimeMillis();
        currentFuture = executor.submit(cmd);
        try {
            currentFuture.get();
        } catch (Exception e) {
            if (e.getCause() instanceof TerminationException) {
                throw (TerminationException) e.getCause();
            }
            logger.error("query terminated due to: {}", e.getMessage(), e);
        }
        currentFuture = null;
        final long endTime = System.currentTimeMillis();
        logger.debug("time usage: {}", DateTimeUtils.prettyPrintDuration(endTime - startTime));
    }

    private static File getJLineHistoryFile() throws IOException {
        final File file = new File(JLineHistoryFile);
        if (file.createNewFile()) {
            logger.info("jline history file not found, created new");
        }
        return file;
    }

    private static Map<String, String> loadCachedSchema() throws IOException {
        final File file = new File(SchemaCacheFile);
        if (file.createNewFile()) {
            logger.info("schema cache file not found, created new");
        }

        final Map<String, String> map = new HashMap<>();
        try (Scanner scanner = new Scanner(new FileInputStream(file), "UTF-8")) {
            while (scanner.hasNextLine()) {
                final String line = scanner.nextLine().trim();
                if (line.isEmpty() || line.startsWith("#")) {
                    continue;
                }
                final String[] splits = line.split("\t", 2);
                if (splits.length != 2) {
                    logger.error("unrecognized schema definition: " + line);
                }
                map.put(splits[0].trim(), splits[1].trim());
            }
        }
        return map;
    }

    public static void main(String[] args) throws IOException {

        final ConsoleReader console = new ConsoleReader();
        console.setHistory(new FileHistory(getJLineHistoryFile()));
        console.setHistoryEnabled(true);
        logger.info("entering EAQL jline-enabled console");

        UnistreamsEnv.get().loadSchemaCache();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    ((FileHistory) console.getHistory()).flush();
                } catch (IOException e) {
                    logger.error("cannot write to jline history file: {}", e.getMessage(), e);
                }
                try {
                    UnistreamsEnv.get().saveSchemaCache();
                } catch (IOException e) {
                    logger.error("cannot write to schema cache file: {}", e.getMessage(), e);
                }
            }
        });

        Signal.handle(new Signal("INT"), new SignalHandler() {
            public void handle(Signal sig) {
                if (currentFuture == null) {
                    System.out.println("intercepted 'CTRL-C', use 'CTRL-D' or \"quit;\" to exit");
                } else {
                    logger.warn("captured user 'CTRL-C', cancelling current query...");
                    currentFuture.cancel(true);
                }
            }
        });


        final StringBuilder sb = new StringBuilder();
        int lineNo = 0;
        while (true) {
            lineNo += 1;
            console.setPrompt(lineNo == 1 ? "eaql> " : "    > ");

            final String line;
            try {
                line = console.readLine();
            } catch (IOException e) {
                logger.error("error reading console input", e);
                break;
            }
            if (line == null) {
                System.out.println();
                break;
            }
            final String trimmed = line.trim();
            if (trimmed.isEmpty() && lineNo == 1) {
                lineNo = 0;
                continue;
            }
            sb.append(line).append('\n');

            if (trimmed.endsWith(";")) {
                String cmd = sb.toString();
                sb.setLength(0);
                lineNo = 0;
                try {
                    execute(cmd);
                } catch (TerminationException e) {
                    break;
                } catch (Throwable t) {
                    logger.error("unexpected runtime error: {}", t.getMessage(), t);
                }
            }
        }
        executor.shutdown();
        System.out.println("bye");
    }


}
