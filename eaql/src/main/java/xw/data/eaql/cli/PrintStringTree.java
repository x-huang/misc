package xw.data.eaql.cli;

import xw.data.eaql.parser.EAQLLexer;
import xw.data.eaql.parser.EAQLParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;

/**
 * Created by xhuang on 10/20/15.
 */
public class PrintStringTree
{
    public static void main(String[] args) throws IOException {
        final EAQLLexer lexer = new EAQLLexer(new ANTLRInputStream(System.in));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final EAQLParser parser = new EAQLParser(tokens);
        System.out.println(parser.parse().toStringTree());
    }
}
