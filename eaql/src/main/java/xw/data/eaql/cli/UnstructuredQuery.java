/*
 * Copyright (c) 2017. Xiaowan Huang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package xw.data.eaql.cli;

import com.google.gson.Gson;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import xw.data.eaql.EAQL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Map;

public class UnstructuredQuery {

    private static String readInputStream(InputStream in) throws IOException {
        final StringBuilder sb = new StringBuilder();
        try (final BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            while (true) {
                String line  = br.readLine();
                if (line == null) break;
                sb.append(line).append('\n');
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {

        final String stmt;
        final String json;

        try {
            final OptionParser parser = new OptionParser();
            parser.accepts("stmt", "simple select statement without FROM and GROUP-BY clause").withRequiredArg();
            parser.accepts("json", "a JSON string to be converted to POJO, read from stdin if omitted").withOptionalArg();
            parser.acceptsAll(Arrays.asList("help", "h"), "print usage").isForHelp();

            final OptionSet opts = parser.parse(args);
            if (opts.has("help")) {
                System.out.println("[intro] executing unstructured query on a JSON value");
                parser.printHelpOn(System.out);
                return;
            }

            stmt = (String) opts.valueOf("stmt");
            json = opts.has("json")? (String) opts.valueOf("json"): readInputStream(System.in);
        } catch (Exception e) {
            System.err.println("failed to launch program: " + e.getMessage());
            throw e;
        }

        final xw.data.eaql.query.unstructured.UnstructuredQuery query = EAQL.getUnstructuredQuery(stmt);
        final Object pojo = (new Gson()).fromJson(json, Map.class);
        final Object[] result = query.query(pojo);
        System.out.println(Arrays.toString(result));

    }
}

/*

$ java xw.data.eaql.cli.UnstructuredQuery --help
[intro] executing unstructured query on a JSON value
Option           Description
------           -----------
-h, --help       print usage
--json [String]  a JSON string to be converted to POJO, read from stdin if
                   omitted
--stmt <String>  simple select statement without FROM and GROUP-BY clause

$ cat example.json
{"widget": {
	"debug": "on",
	"window": {
		"title": "Sample Konfabulator Widget",
		"name": "main_window",
		"width": 500,
		"height": 500
	},
	"image": {
		"src": "Images/Sun.png",
		"name": "sun1",
		"hOffset": 250,
		"vOffset": 250,
		"alignment": "center"
	},
	"text": {
		"data": "Click Here",
		"size": 36,
		"style": "bold",
		"name": "text1",
		"hOffset": 250,
		"vOffset": 100,
		"alignment": "center",
		"onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
	}
}}

$ cat example.json | java xw.data.eaql.cli.UnstructuredQuery --stmt '
    SELECT concat(_.widget.image.src, "@", _.widget.window.name,": area=",
                    (_.widget.window.width * _.widget.window.height)),
            _.widget.image.alignment = "center",
            _.widget.window.width > 640 AND _.widget.window.height > 480,
            _.widget.text.onMouseUp IS NOT NULL
'
[result] [Images/Sun.png@main_window: area=250000.0, true, false, true]

$ cat example.json | java xw.data.eaql.cli.UnstructuredQuery  --stmt '
    SELECT concat(_.widget.image.src, "@", _.widget.window.name,": area=",
                    (_.widget.window.width * _.widget.window.height)),
            _.widget.image.alignment = "center",
            _.widget.window.width > 640 AND _.widget.window.height > 480,
            _.widget.text.onMouseUp IS NOT NULL
    WHERE _.widget.debug = "on"
'
[result] [Images/Sun.png@main_window: area=250000.0, true, false, true]

$ cat example.json | java xw.data.eaql.cli.UnstructuredQuery  --stmt '
    SELECT concat(_.widget.image.src, "@", _.widget.window.name,": area=",
                    (_.widget.window.width * _.widget.window.height)),
           _.widget.image.alignment = "center",
           _.widget.window.width > 640 AND _.widget.window.height > 480,
           _.widget.text.onMouseUp IS NOT NULL
    WHERE _.widget.debug = "off"
'
[result] null

*/