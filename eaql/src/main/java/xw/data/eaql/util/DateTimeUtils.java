package xw.data.eaql.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by xhuang on 9/9/16.
 */
public class DateTimeUtils
{
    private static final ThreadLocal<SimpleDateFormat>
            dateFormatHolder = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    private static final ThreadLocal<SimpleDateFormat>
            timestampFormatHolder = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        }
    };

    public static String format(Timestamp ts) {
        return timestampFormatHolder.get().format(new Date(ts.getTime()));
    }

    public static String format(Date date) {
        return dateFormatHolder.get().format(date);
    }

    public static Date date_arithmetic(Date date, int diff) {
        return new Date(date.getTime() + diff * 1000 * 60 * 60 * 24);
    }

    public static long date_diff(Date startDate, Date endDate) {
        final long diff = startDate.getTime() - endDate.getTime();
        return diff/ (1000 * 60 * 60 * 24);
    }

    public static Timestamp to_timestamp(Object v) {
        if (v == null) {
            return null;
        } else if (v instanceof String) {
            return Timestamp.valueOf((String) v);
        } else if (v instanceof Date) {
            return new Timestamp(((Date) v).getTime());
        } else if (v instanceof Timestamp) {
            return (Timestamp) v;
        } else {
            throw new IllegalArgumentException("unknown timestamp representation: " + v);
        }
    }

    public static Date to_date(Object v) {
        if (v == null) {
            return null;
        } else if (v instanceof String) {
            return Date.valueOf((String) v);
        } else if (v instanceof Date) {
            return (Date) v;
        } else if (v instanceof Timestamp) {
            final Timestamp ts = (Timestamp) v;
            return new Date(ts.getTime());
        } else {
            throw new IllegalArgumentException("unknown date representation: " + v);
        }
    }

    public static String printDuration(long t) {
        final int millis  = (int) (t % 1000);
        final int seconds = (int) (t / 1000) % 60;
        final int minutes = (int) ((t / (1000*60)) % 60);
        final int hours   = (int) ((t / (1000*60*60)) % 24);

        final StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append((char)('0' + hours / 10))
                    .append((char)('0' + hours % 10))
                    .append(':');
        }
        if (hours > 0 || minutes > 0) {
            sb.append((char) ('0' + minutes / 10))
                    .append((char) ('0' + minutes % 10))
                    .append(':');
        }
        sb.append((char)('0' + seconds / 10))
                .append((char)('0' + seconds % 10)).append(".")
                .append((char)('0' + millis / 100))
                .append((char)('0' + (millis % 100)/ 10))
                .append((char)('0' + millis % 10) );
        return sb.toString();
    }

    public static String prettyPrintDuration(long t) {
        final int millis  = (int) (t % 1000);
        final int seconds = (int) (t / 1000) % 60;
        final int minutes = (int) ((t / (1000*60)) % 60);
        final int hours   = (int) ((t / (1000*60*60)) % 24);

        final StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append(hours).append(" hr ");
        }
        if (hours > 0 || minutes > 0) {
            sb.append(minutes).append(" min ");
        }
        sb.append(seconds).append(".")
                .append((char)('0' + millis / 100))
                .append((char)('0' + (millis % 100)/ 10))
                .append((char)('0' + millis % 10))
                .append(" sec");
        return sb.toString();
    }
}
