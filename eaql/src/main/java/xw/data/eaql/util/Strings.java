package xw.data.eaql.util;

import java.util.Collection;

/**
 * Created by xhuang on 8/10/16.
 */
public class Strings
{
    public static String join(Collection<?> list, String prefix, String delim, String suffix) {
        final StringBuilder sb = new StringBuilder();
        String delimiter = "";
        if (prefix != null) {
            sb.append(prefix);
        }
        for (Object o: list) {
            sb.append(delimiter).append(o.toString());
            delimiter = delim;
        }
        if (suffix != null) {
            sb.append(suffix);
        }
        return sb.toString();
    }

    public static String join(Collection<?> list, String prefix, String delim) {
        return join(list, prefix, delim, null);
    }

    public static String join(Collection<?> list, String delim) {
        return join(list, null, delim, null);
    }

    public static String escapeValue(Object value) {
        if (value == null) {
            return "null";
        } else if (value instanceof String) {
            return "\"" + value + "\"";
        } else {
            return value.toString();
        }
    }


    public static String stripIdentifier(String s) {
        if (s.startsWith("`") && s.endsWith("`")) {
            return s.substring(1, s.length()-1);
        } else {
            return s;
        }
    }

    public static String stripLiteralString(String s) {
        if ((s.startsWith("\"") && s.endsWith("\"")) || (s.startsWith("'") && s.endsWith("'"))) {
            return s.substring(1, s.length()-1);
        } else {
            throw new IllegalStateException("not a string literal: " + s);
        }
    }

    public static String stripTripleQuoteLiteralString(String s) {
        if ((s.startsWith("\"\"\"") && s.endsWith("\"\"\"")) ||
                (s.startsWith("'''") && s.endsWith("'''"))) {
            return s.substring(3, s.length()-3);
        } else {
            throw new IllegalStateException("not a triple-quoted string literal: " + s);
        }
    }
}
