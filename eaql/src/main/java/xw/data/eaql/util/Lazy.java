package xw.data.eaql.util;

/**
 * Created by xhuang on 8/27/16.
 */
public abstract class Lazy<T>
{
    private transient volatile T instance = null;

    public T get() {
        if (instance == null) {
            synchronized (this) {
                if (instance == null) {
                    instance = createInstance();
                }
            }
        }
        return instance;
    }

    protected abstract T createInstance();
}
