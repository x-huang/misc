package xw.data.eaql.util;

/**
 * Created by xhuang on 8/27/16.
 */
public abstract class LazyEx<T, E extends Exception>
{
    private transient volatile T instance = null;

    public T get() throws E {
        if (instance == null) {
            synchronized (this) {
                if (instance == null) {
                    instance = createInstance();
                }
            }
        }
        return instance;
    }

    protected abstract T createInstance() throws E;
}
