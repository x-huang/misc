package xw.data.eaql;

import xw.data.eaql.model.ModelBuilder;
import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.stmt.SQLStatement;
import xw.data.eaql.model.stmt.SimpleSelectStatement;
import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.StructType;
import xw.data.eaql.parser.EAQLLexer;
import xw.data.eaql.parser.EAQLParser;
import xw.data.eaql.query.QueryBuilder;
import xw.data.eaql.query.SQLQuery;
import xw.data.eaql.query.SQLRead;
import xw.data.eaql.query.SQLWrite;
import xw.data.eaql.query.unstructured.UnstructuredQuery;
import xw.data.eaql.schema.Schema;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import xw.data.unistreams.stream.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by xhuang on 8/17/16.
 */
public class EAQL
{
    static private class Query {
        private final SQLQuery query;

        public Query(SQLQuery query) {
            this.query = query;
        }

        public boolean isReadable() {
            return query instanceof SQLRead;
        }

        public DataSource getDataSource() {
            if (query instanceof SQLRead) {
                return ((SQLRead) query).getDataSource();
            } else {
                throw new IllegalArgumentException("no result set for a non-readable query: " + query);
            }
        }

        public void execute() throws IOException {
            if (query instanceof SQLWrite) {
                ((SQLWrite) query).doWrite();
            } else {
                throw new IllegalArgumentException("cannot execute non-writable query: " + query);
            }
        }
    }

    private static final ModelBuilder builder = new ModelBuilder();

    private static EAQLParser getParser(ANTLRInputStream antlrInputStream) {
        final EAQLLexer lexer = new EAQLLexer(antlrInputStream);
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final EAQLParser parser = new EAQLParser(tokens);
        parser.setErrorHandler(new BailErrorStrategy());
        return parser;
    }

    private static EAQLParser getParser(String s) {
        return getParser(new ANTLRInputStream(s));
    }

    private static EAQLParser getParser(InputStream in) throws IOException {
        return getParser(new ANTLRInputStream(in));
    }

    @SuppressWarnings("unchecked")
    public static List<SQLStatement> parseScript(String s) {
        return (List<SQLStatement>) builder.visit(getParser(s).parse());
    }

    @SuppressWarnings("unchecked")
    public static List<SQLStatement> parseScript(InputStream in) throws IOException {
        return (List<SQLStatement>) builder.visit(getParser(in).parse());
    }

    public static SQLStatement parseStatement(String s) {
        return (SQLStatement) builder.visit(getParser(s).sql_stmt());
    }

    public static SQLStatement parseStatement(InputStream in) throws IOException {
        return (SQLStatement) builder.visit(getParser(in).sql_stmt());
    }

    public static SimpleSelectStatement parseSimpleSelectStatement(String s) {
        return (SimpleSelectStatement) builder.visit(getParser(s).simple_select_stmt());
    }

    public static SimpleSelectStatement parseSimpleSelectStatement(InputStream in) throws IOException {
        return (SimpleSelectStatement) builder.visit(getParser(in).simple_select_stmt());
    }

    public static Expression parseExpression(String s) {
        return (Expression) builder.visit(getParser(s).expr());
    }

    public static Expression parseExpression(InputStream in) throws IOException {
        return (Expression) builder.visit(getParser(in).expr());
    }

    protected static DataType parseDataType(ANTLRInputStream antlrInputStream) {
        final EAQLLexer lexer = new EAQLLexer(antlrInputStream);
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final EAQLParser parser = new EAQLParser(tokens);
        final ModelBuilder builder = new ModelBuilder();
        final Object model = builder.visit(parser.data_type());
        return (DataType) model;
    }

    public static DataType parseDataType(String s) {
        if (s == null) {
            throw new NullPointerException("null type string");
        }
        return (DataType) builder.visit(getParser(s).data_type());
    }

    public static DataType parseDataType(InputStream in) throws IOException {
        return (DataType) builder.visit(getParser(in).data_type());
    }

    public static Schema parseSchema(String s) {
        return Schema.fromString(s);
    }

    public static Schema parseSchema(InputStream in) throws IOException {
        return Schema.fromStructType((StructType) parseDataType(in));
    }

    public static Query getQuery(String s) {
        return new Query(QueryBuilder.build(s));
    }

    public static Query getQuery(InputStream s) throws IOException {
        return new Query(QueryBuilder.build(s));
    }

    public static UnstructuredQuery getUnstructuredQuery(String s) {
        return new UnstructuredQuery(parseSimpleSelectStatement(s));
    }

    public static UnstructuredQuery getUnstructuredQuery(InputStream in) throws IOException {
        return new UnstructuredQuery(parseSimpleSelectStatement(in));
    }
}
