package xw.data.eaql.semantic;

/**
 * Created by xhuang on 8/10/16.
 */
@Deprecated
public class SemanticException extends RuntimeException
{
    public SemanticException(String msg) {
        super(msg);
    }

     public SemanticException(String msg, Exception e) {
        super(msg, e);
    }

}
