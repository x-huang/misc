package xw.data.eaql.semantic;

import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.Function;
import xw.data.eaql.model.stmt.SimpleSelectStatement;
import xw.data.eaql.query.util.ExprUtils;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by xhuang on 8/23/16.
 */
@Deprecated
public class SemanticChecker
{
    private static final Logger logger = LoggerFactory.getLogger(SemanticChecker.class);

    private final SimpleSelectStatement stmt;
    private final Schema schema;
    transient private final Map<String, Function> aggregateFunctions;
    transient private final Set<String> eminentColumnNames;
    transient private final Set<String> selectedColumnNames;
    transient private final Set<String> whereClauseColumnNames;

    /*
    private static void collectAggregateFunctions(Expression e, Map<String, Function> map) {
        if (e instanceof LiteralValue || e instanceof ColumnValue || e instanceof Asterisk) {
            // do nothing
        } else if (e instanceof UnaryExpression) {
            collectAggregateFunctions(((UnaryExpression) e).e, map);
        } else if (e instanceof BinaryExpression) {
            collectAggregateFunctions(((BinaryExpression) e).e1, map);
            collectAggregateFunctions(((BinaryExpression) e).e2, map);
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            if (Functions.isUDAF(f)) {
                map.put(f.toString(), f);
            } else {
                for (Expression p: f.params) {
                    collectAggregateFunctions(p, map);
                }
            }
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }

    public static Map<String, Function> getAllAggregateFunctions(SelectClause select) {
        final Map<String, Function> aggregators = new HashMap<>();
        for (SelectClause.ResultColumn rc: select.resultColumns) {
            collectAggregateFunctions(rc.e, aggregators);
        }
        return aggregators;
    }

    private static void collectEminentColumnNames(Expression e, Set<String> set) {
        if (e instanceof LiteralValue || e instanceof Asterisk) {
            // do nothing
        } else if (e instanceof ColumnValue) {
            set.add(((ColumnValue) e).column);
        } else if (e instanceof UnaryExpression) {
            collectEminentColumnNames(((UnaryExpression) e).e, set);
        } else if (e instanceof BinaryExpression) {
            collectEminentColumnNames(((BinaryExpression) e).e1, set);
            collectEminentColumnNames(((BinaryExpression) e).e2, set);
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            if (! Functions.isUDAF(f)) {
                for (Expression p: f.params) {
                    collectEminentColumnNames(p, set);
                }
            }
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }

    public static Set<String> getAllEminentColumnNames(SelectClause select) {
        final Set<String> columnNames = new HashSet<>();
        for (SelectClause.ResultColumn rc: select.resultColumns) {
            collectEminentColumnNames(rc.e, columnNames);
        }
        return columnNames;
    }

    public static Set<String> getAllEminentColumnNames(Expression e) {
        final Set<String> columnNames = new HashSet<>();
        collectEminentColumnNames(e, columnNames);
        return columnNames;
    }

    public static Set<String> getAllColumnNames(Expression e) {
        final Set<String> columnNames = new HashSet<>();
        collectColumnNames(e, columnNames);
        return columnNames;
    }

    public static Set<String> getAllColumnNames(SelectClause select) {
        final Set<String> columnNames = new HashSet<>();
        for (SelectClause.ResultColumn rc: select.resultColumns) {
            collectColumnNames(rc.e, columnNames);
        }
        return columnNames;
    }

    private static void collectColumnNames(Expression e, Set<String> set) {
        if (e instanceof LiteralValue || e instanceof Asterisk) {
            // do nothing
        } else if (e instanceof ColumnValue) {
            set.add(((ColumnValue) e).column);
        } else if (e instanceof UnaryExpression) {
            collectEminentColumnNames(((UnaryExpression) e).e, set);
        } else if (e instanceof BinaryExpression) {
            collectEminentColumnNames(((BinaryExpression) e).e1, set);
            collectEminentColumnNames(((BinaryExpression) e).e2, set);
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            for (Expression p: f.params) {
                collectEminentColumnNames(p, set);
            }
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    } */

    public SemanticChecker(SimpleSelectStatement stmt, Schema schema) {
        this.stmt = stmt;
        this.schema = schema;
        if (stmt.from == null && schema.size() != 0) {
            throw new RuntimeException("expecting empty schema when selecting from none, got: " + schema);
        }
        this.aggregateFunctions = ExprUtils.getAllAggregateFunctions(stmt.select);
        this.eminentColumnNames = ExprUtils.getAllEminentColumnNames(stmt.select);
        this.selectedColumnNames = ExprUtils.getAllColumnNames(stmt.select);
        this.whereClauseColumnNames = (stmt.where == null)? new HashSet<String>(): ExprUtils.getAllColumnNames(stmt.where.e);

    }

    private SemanticException newColumnNotFoundException(String col) {
        return new SemanticException("Invalid table alias or column reference '"
                        + col + "': (possible column names are: "
                        + Strings.join(schema.columns(), "", ", ") + ")");
    }

    public void checkAll() {
        selectedColumnNamesInSchema();
        whereClauseColumnNamesInSchema();
        groupByKeysInSchema();
        columnNamesInGroupByKeys();
        noStackingAggregateFunction();
        noAggregateFunctionInWhereClause();
    }

    public void columnNamesInGroupByKeys() {
        if (stmt.groupBy == null) {
            return;
        }
        for (String col: eminentColumnNames) {
            if (! stmt.groupBy.columns.contains(col)) {
                throw new SemanticException("Expression not in GROUP BY key '" + col + "'");
            }
        }
    }

    public void selectedColumnNamesInSchema() {
        for (String col: selectedColumnNames) {
            if (! schema.hasColumn(col)) {
                throw newColumnNotFoundException(col);
            }
        }
    }

    public void whereClauseColumnNamesInSchema() {
        for (String col: whereClauseColumnNames) {
            if (! schema.hasColumn(col)) {
                throw newColumnNotFoundException(col);
            }
        }
    }

    public void groupByKeysInSchema() {
        if (stmt.groupBy == null) {
            if (aggregateFunctions.size() == 0 || eminentColumnNames.size() == 0) {
                return;
            } else {
                throw newColumnNotFoundException(eminentColumnNames.iterator().next());
            }

        }
        for (String col: stmt.groupBy.columns) {
            if (! schema.hasColumn(col)) {
                throw newColumnNotFoundException(col);
            }
        }
    }

    public void noStackingAggregateFunction() {
        for (Function f: aggregateFunctions.values()) {
            for (Expression p: f.params) {
                final Map<String, Function> aggregators = new HashMap<>();
                ExprUtils.collectAggregateFunctions(p, aggregators);
                if (aggregators.size() != 0) {
                    throw new SemanticException("Not yet supported place for UDAFs '"
                            + aggregators.keySet().iterator().next() + "': stacking UDAFs");
                }
            }
        }
    }

    public void noAggregateFunctionInWhereClause() {
        if (stmt.where == null) {
            return;
        }
        final Map<String, Function> aggregators = new HashMap<>();
        ExprUtils.collectAggregateFunctions(stmt.where.e, aggregators);
        if (aggregators.size() != 0) {
            throw new SemanticException("Not yet supported place for UDAFs '"
                            + aggregators.keySet().iterator().next() + "': UDAFs in WHERE clause");
        }
    }
}
