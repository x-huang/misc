package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/24/16.
 */
public interface ExprVisitor<T>
{
    T visitAddition(Addition e);
    T visitAnd(And e);
    T visitArrayMapElement(ArrayMapElement e);
    T visitAsterisk(Asterisk e);
    T visitBetween(Between e);
    T visitCaseExprWhen(CaseExprWhen e);
    T visitCaseWhen(CaseWhen e);
    T visitCast(Cast e);
    T visitColumnValue(ColumnValue e);
    T visitColumnValueIndexed(ColumnValueIndexed e);
    T visitDivision(Division e);
    T visitEquals(Equals e);
    T visitFunction(Function f);
    T visitGreaterThan(GreaterThan e);
    T visitGreaterThanEquals(GreaterThanEquals e);
    T visitInSet(InSet e);
    T visitIsNull(IsNull e);
    T visitLessThan(LessThan e);
    T visitLessThanEquals(LessThanEquals e);
    T visitLike(Like e);
    T visitLiteralValue(LiteralValue e);
    T visitMultiplication(Multiplication e);
    T visitNegation(Negation e);
    T visitNot(Not e);
    T visitNotEquals(NotEquals e);
    T visitOr(Or e);
    T visitParenthetical(Parenthetical e);
    T visitRLike(RLike e);
    T visitStructField(StructField e);
    T visitStructFieldIndexed(StructFieldIndexed e);
    T visitSubtraction(Subtraction e);
}
