package xw.data.eaql.model.type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/15/16.
 */
final public class StructType implements CompositeType
{

    public static class StructField {
        public final String name;
        public final DataType type;

        public StructField(String name, DataType type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public String toString() {
            return name + ":" + type;
        }
    }

    public final List<StructField> fields;

    protected StructType() {
        this.fields = new ArrayList<>();
    }

    protected StructType(List<StructField> fields) {
        this.fields = new ArrayList<>(fields);
    }

    protected void addField(StructField field) {
        this.fields.add(field);
    }

    public StructField getField(int i) {
        return fields.get(i);
    }

    @Override
    public String toString() {
        String s = "struct<";
        String delimiter = "";
        for (StructField field: fields) {
            s += delimiter + field.toString();
            delimiter = ",";
        }
        return s + ">";
    }
        @Override
    public Category getCategory() {
        return Category.STRUCT;
    }

    @Override
    public Class getJavaType() {
        return List.class;
    }

    @Override
    public Object getValueZero() {
        final List<Object> zeros = new ArrayList<>();
        for (StructField field: fields) {
            zeros.add(field.type.getValueZero());
        }
        return zeros;
    }

}
