package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public final class NotEquals extends BinaryExpression implements BooleanExpression
{
    public NotEquals(Expression e1, Expression e2) {
        super("!=", e1, e2);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitNotEquals(this);
    }
}
