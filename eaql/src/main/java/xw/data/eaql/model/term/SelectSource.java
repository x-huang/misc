package xw.data.eaql.model.term;

/**
 * Created by xhuang on 8/26/16.
 */
public interface SelectSource
{
    enum Type {
        TABLE, RESOURCE, SUB_QUERY
    }

    String getDataReference();
}
