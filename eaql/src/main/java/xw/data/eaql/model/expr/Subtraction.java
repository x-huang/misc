package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public final class Subtraction extends BinaryExpression implements BooleanExpression
{

    public Subtraction(Expression e1, Expression e2) {
        super("-", e1, e2);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitSubtraction(this);
    }
}
