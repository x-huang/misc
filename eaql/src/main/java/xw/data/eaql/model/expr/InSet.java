package xw.data.eaql.model.expr;

import xw.data.eaql.util.Strings;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by xhuang on 11/3/15.
 */
public class InSet extends UnaryExpression implements BooleanExpression
{
    public final boolean not;
    public final Set<Object> set;

    public InSet(Expression e, boolean not, Collection<Object> set) {
        super(not? "NOT IN": "IN", e);
        this.not = not;
        this.set = new HashSet<>(set);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String delimiter = "";
        sb.append(e.toString()).append(" ").append(op).append(" (");
        for (Object elem: set) {
            sb.append(delimiter).append(Strings.escapeValue(elem));
            delimiter = ", ";
        }
        return sb.append(")").toString();
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitInSet(this);
    }
}
