package xw.data.eaql.model.type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/15/16.
 */
final public class ArrayType implements CompositeType
{
    public final DataType valueType;

    ArrayType(DataType valueType) {
        this.valueType = valueType;
    }

    @Override
    public String toString() {
        return "array<" + valueType.toString() + ">";
    }

    @Override
    public Category getCategory() {
        return Category.ARRAY;
    }

    @Override
    public Class getJavaType() {
        return List.class;
    }

    @Override
    public Object getValueZero() {
        return new ArrayList<>();
    }
}
