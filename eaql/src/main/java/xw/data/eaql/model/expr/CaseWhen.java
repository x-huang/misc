package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 9/7/16.
 */
public class CaseWhen extends AbstractCaseWhen
{
    public final Expression else_;

    public CaseWhen(Expression else_) {
        this.else_ = else_;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitCaseWhen(this);
    }

        @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CASE ");
        for (WhenThenPair pair: whenThenPairs) {
            sb.append(" WHEN ").append(pair.when).append(" THEN ").append(pair.then);
        }
        sb.append(" ELSE ").append(else_).append(" END");
        return sb.toString();
    }
}
