package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class BigIntType implements IntegerNumberType
{
    @Override
    public String toString() {
        return "bigint";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Long.class;
    }

    @Override
    public Object getValueZero() {
        return 0L;
    }
}
