package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/19/2015.
 */
public final class Multiplication extends BinaryExpression implements ArithmeticExpression
{
    public Multiplication(Expression e1, Expression e2) {
        super("*", e1, e2);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitMultiplication(this);
    }
}
