package xw.data.eaql.model.term;

import xw.data.eaql.model.expr.Asterisk;
import xw.data.eaql.model.expr.Expression;

/**
 * Created by xhuang on 9/1/16.
 */
public class ResultColumn
{
    public final Expression e;
    public final String alias;
    // public final String realAlias;

    public ResultColumn(Expression e, String alias) {
        this.e = e;
        this.alias = alias;
        if ((e instanceof Asterisk) && alias != null) {
            throw new RuntimeException("cannot select all columns with alias");
        }
        // this.realAlias = alias==null? Expressions.getDefaultColumnAlias(e): alias;
    }

    @Override
    public String toString() {
        final String s = e.toString();
        return alias == null? s : (s + " AS " + alias);
    }
}
