package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class FloatType implements FloatNumberType
{
    @Override
    public String toString() {
        return "float";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Float.class;
    }

    @Override
    public Object getValueZero() {
        return 0.0f;
    }
}
