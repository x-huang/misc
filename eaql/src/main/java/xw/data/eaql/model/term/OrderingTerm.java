package xw.data.eaql.model.term;

import xw.data.eaql.model.expr.Expression;

/**
 * Created by xhuang on 9/1/16.
 */
public class OrderingTerm
{
    public final Expression e;
    public final boolean asc;

    public OrderingTerm(Expression e) {
        this.e = e;
        this.asc = true;
    }

    public OrderingTerm(Expression e, boolean asc) {
        this.e = e;
        this.asc = asc;
    }

    @Override
    public String toString() {
        return e.toString() + (asc? "": " DESC");
    }
}
