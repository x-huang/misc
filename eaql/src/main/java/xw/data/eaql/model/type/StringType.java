package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/14/16.
 */
final class StringType implements PrimitiveType
{
   @Override
    public String toString() {
        return "string";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return String.class;
    }

    @Override
    public Object getValueZero() {
        return "";
    }
}