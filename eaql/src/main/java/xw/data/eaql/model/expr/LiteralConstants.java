package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/31/16.
 */
public class LiteralConstants
{
    public static final Expression literalNull = new LiteralValue(null);
    public static final Expression literalOne = new LiteralValue(1L);
    public static final Expression literalZero = new LiteralValue(0L);
    public static final Expression literalFloatZero = new LiteralValue(0.0);
    public static final Expression literalTrue = new LiteralValue(true);
    public static final Expression literalFalse = new LiteralValue(false);
    public static final Expression literalEmptyString = new LiteralValue("");
}
