package xw.data.eaql.model.clause;

import xw.data.eaql.util.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/10/16.
 */
public class GroupByClause
{
    public final List<String> columns = new ArrayList<>();

    public void addColumn(String col) {
        columns.add(col);
    }

    @Override
    public String toString() {
        return Strings.join(columns, "GROUP BY ", ", ");
    }
}
