package xw.data.eaql.model.clause;

import xw.data.eaql.model.stmt.SelectStatement;
import xw.data.eaql.model.term.ResourceString;
import xw.data.eaql.model.term.SelectSource;
import xw.data.eaql.model.term.TableName;

/**
 * Created by xhuang on 8/10/16.
 */
public class FromClause
{
    public final SelectSource source;

    public FromClause(SelectSource source) {
        this.source = source;
    }

    public SelectSource.Type getSourceType() {
        if (source instanceof TableName) {
            return SelectSource.Type.TABLE;
        } if (source instanceof ResourceString) {
            return SelectSource.Type.RESOURCE;
        } else if (source instanceof SelectStatement) {
            return SelectSource.Type.SUB_QUERY;
        } else {
            throw new RuntimeException("unknown select source type: " + source.getClass().getName());
        }
    }

    @Override
    public String toString() {
        switch (getSourceType()) {
            case TABLE:
            case RESOURCE:
                return "FROM " + source.toString();
            case SUB_QUERY:
                return "FROM ( " + source.toString() + " )";
            default:
                throw new RuntimeException("unexpected select source type: " + source.getClass().getName());
        }
    }
}
