package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/11/16.
 */
public final class Asterisk implements Expression
{
    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitAsterisk(this);
    }

    @Override
    public String toString() {
        return "*";
    }
}
