package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public final class Parenthetical extends UnaryExpression
{
    public Parenthetical(Expression e) {
        super("(.)", e);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitParenthetical(this);
    }

    @Override
    public String toString() {
        return "(" + e.toString() + ")";
    }
}
