package xw.data.eaql.model.term;

/**
 * Created by xhuang on 8/26/16.
 */
public class ResourceString implements SelectSource
{
    public final String rd;

    public ResourceString(String rd) {
        this.rd = rd;
    }

    @Override
    public String toString() {
        return "\"" + rd  + "\"";
    }

    @Override
    public String getDataReference() {
        return rd;
    }
}
