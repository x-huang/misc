package xw.data.eaql.model.expr;

import xw.data.eaql.util.Strings;

/**
 * Created by xhuang on 9/3/16.
 */
public class ArrayMapElement extends BinaryExpression
{
    public ArrayMapElement(Expression e1, Expression e2) {
        super("[]", e1, e2);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitArrayMapElement(this);
    }

    @Override
    public String toString() {
        return e1.toString() + '[' + Strings.escapeValue(e2) + ']';
    }
}
