package xw.data.eaql.model.clause;

/**
 * Created by xhuang on 9/15/16.
 */
public class OffsetClause
{
    public final long num;

    public OffsetClause(long num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "OFFSET " + num;
    }
}
