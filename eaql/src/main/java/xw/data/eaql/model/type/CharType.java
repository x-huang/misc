package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class CharType implements PrimitiveType
{
    public final int length;

    public CharType(int length) {
        if (length < 1 || length > 255) {
            throw new IllegalArgumentException("bad char length: " + length);
        }
        this.length = length;
    }

    @Override
    public String toString() {
        return "char(" + length + ")";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Character.class;
    }

    @Override
    public Object getValueZero() {
        return '\0';
    }
}
