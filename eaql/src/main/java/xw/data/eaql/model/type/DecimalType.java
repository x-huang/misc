package xw.data.eaql.model.type;

import java.math.BigDecimal;

/**
 * Created by xhuang on 8/15/16.
 */
final class DecimalType implements FloatNumberType
{
    public static final int DEFAULT_PRECISION = 10;
    public static final int DEFAULT_SCALE = 0;

    public final int precision;
    public final int scale;

    public DecimalType() {
        this(DEFAULT_PRECISION, DEFAULT_SCALE);
    }

    public DecimalType(int precision) {
        this(precision, DEFAULT_SCALE);
    }

    public DecimalType(int precision, int scale) {
        if (precision < 0) {
            throw new IllegalArgumentException("bad decimal precision: " + precision);
        }
        if (scale < 0) {
            throw new IllegalArgumentException("bad decimal scale: " + scale);
        }
        this.precision = precision;
        this.scale = scale;
    }

    @Override
    public String toString() {
        if (precision == DEFAULT_PRECISION && scale == DEFAULT_SCALE)
            return "decimal";
        else
            return "decimal(" + precision + "," + scale + ")";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return BigDecimal.class;
    }

    @Override
    public Object getValueZero() {
        return BigDecimal.ZERO;
    }
}
