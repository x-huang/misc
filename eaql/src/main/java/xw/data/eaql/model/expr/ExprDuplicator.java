package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/25/16.
 */
public class ExprDuplicator implements ExprVisitor<Expression>
{
    private final ExprReplacer replacer;

    public ExprDuplicator(ExprReplacer replacer) {
        this.replacer = replacer;
    }

    public ExprDuplicator() {
        this.replacer = new ExprReplacer() {
            @Override
            public Expression replace(Expression e) {
                return null;
            }
        };
    }

    public Expression duplicate(Expression e) {
        return e.accept(this);
    }

    @Override
    public Expression visitAddition(Addition e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Addition(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitAnd(And e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new And(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitArrayMapElement(ArrayMapElement e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new ArrayMapElement(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitAsterisk(Asterisk e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Asterisk();
    }

    @Override
    public Expression visitBetween(Between e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Between(e.e.accept(this), e.not, e.lower, e.upper);
    }

    @Override
    public Expression visitCaseExprWhen(CaseExprWhen e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;

        final CaseExprWhen caseWhen = new CaseExprWhen(e.e.accept(this), e.else_.accept(this));
        for (int i=0; i<e.numWhens(); i++) {
            final Expression when_ = e.when(i).accept(this);
            final Expression then_ = e.then(i).accept(this);
            caseWhen.addWhenThenPair(when_, then_);
        }
        return caseWhen;
    }

    @Override
    public Expression visitCaseWhen(CaseWhen e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;

        final CaseWhen caseWhen = new CaseWhen(e.else_.accept(this));
        for (int i=0; i<e.numWhens(); i++) {
            final Expression when_ = e.when(i).accept(this);
            final Expression then_ = e.then(i).accept(this);
            caseWhen.addWhenThenPair(when_, then_);
        }
        return caseWhen;
    }

    @Override
    public Expression visitCast(Cast e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Cast(e.e.accept(this), e.type);
    }

    @Override
    public Expression visitColumnValue(ColumnValue e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new ColumnValue(e.column);
    }

    @Override
    public Expression visitColumnValueIndexed(ColumnValueIndexed e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new ColumnValueIndexed(e.index);
    }

    @Override
    public Expression visitDivision(Division e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Division(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitEquals(Equals e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Equals(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitStructField(StructField e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new StructField(e.e.accept(this), e.field);
    }

    @Override
    public Expression visitStructFieldIndexed(StructFieldIndexed e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new StructFieldIndexed(e.columnIndex, e.fieldIndex);
    }

    @Override
    public Expression visitFunction(Function f) {
        final Expression r = replacer.replace(f);
        if (r != null) return r;

        final Function f_ = new Function(f.name);
        for (Expression p: f.params) {
            f_.addParam(p.accept(this));
        }
        return f_;
    }

    @Override
    public Expression visitGreaterThan(GreaterThan e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new GreaterThan(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitGreaterThanEquals(GreaterThanEquals e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new GreaterThanEquals(e.e1.accept(this), e.e2.accept(this));
    }


    @Override
    public Expression visitInSet(InSet e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new InSet(e.e.accept(this), e.not, e.set);
    }

    @Override
    public Expression visitIsNull(IsNull e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new IsNull(e.e.accept(this), e.not);
    }

    @Override
    public Expression visitLessThan(LessThan e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new LessThan(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitLessThanEquals(LessThanEquals e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new LessThanEquals(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitLike(Like e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Like(e.e.accept(this), e.not, e.like);
    }

    @Override
    public Expression visitLiteralValue(LiteralValue e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new LiteralValue(e.value);
    }

    @Override
    public Expression visitMultiplication(Multiplication e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Multiplication(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitNegation(Negation e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Negation(e.e.accept(this));
    }

    @Override
    public Expression visitNot(Not e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Not(e.e.accept(this));
    }

    @Override
    public Expression visitNotEquals(NotEquals e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new NotEquals(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitOr(Or e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Or(e.e1.accept(this), e.e2.accept(this));
    }

    @Override
    public Expression visitParenthetical(Parenthetical e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Parenthetical(e.e.accept(this));
    }

    @Override
    public Expression visitRLike(RLike e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new RLike(e.e.accept(this), e.not, e.regex);
    }

    @Override
    public Expression visitSubtraction(Subtraction e) {
        final Expression r = replacer.replace(e);
        if (r != null) return r;
        else return new Subtraction(e.e1.accept(this), e.e2.accept(this));
    }



}
