package xw.data.eaql.model.term;

/**
 * Created by xhuang on 8/13/16.
 */
public class CompoundOperator
{
    public final String op;

    public CompoundOperator(String op) {
        this.op = op;
    }

    @Override
    public String toString() {
        return op;
    }
}
