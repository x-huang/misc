package xw.data.eaql.model.type;

/**
 * Created by xhuang on 9/3/16.
 */
public enum Category
{
    PRIMITIVE, STRUCT, MAP, ARRAY, UNION, VOID
}
