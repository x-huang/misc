package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/19/2015.
 */
public final class ColumnValue implements Expression
{
    public final String column;

    public ColumnValue(String column) {
        this.column = column;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitColumnValue(this);
    }

    @Override
    public String toString() {
        return column;
    }
}
