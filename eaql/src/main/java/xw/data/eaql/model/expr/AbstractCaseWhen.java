package xw.data.eaql.model.expr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 9/7/16.
 */
abstract class AbstractCaseWhen implements Expression
{
    static class WhenThenPair
    {
        final Expression when;
        final Expression then;

        public WhenThenPair(Expression when, Expression then) {
            this.when = when;
            this.then = then;
        }
    }

    final List<WhenThenPair> whenThenPairs = new ArrayList<>();

    public void addWhenThenPair(Expression when, Expression then) {
        whenThenPairs.add(new WhenThenPair(when, then));
    }

    public int numWhens() {
        return whenThenPairs.size();
    }

    public Expression when(int i) {
        return whenThenPairs.get(i).when;
    }

    public Expression then(int i) {
        return whenThenPairs.get(i).then;
    }
}
