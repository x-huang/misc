package xw.data.eaql.model.clause;

import xw.data.eaql.model.term.OrderingTerm;
import xw.data.eaql.util.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/22/16.
 */
public class OrderByClause
{
    public final List<OrderingTerm> orderingTerms = new ArrayList<>();

    public void addOrderingTerm(OrderingTerm term) {
        orderingTerms.add(term);
    }

    @Override
    public String toString() {
        return Strings.join(orderingTerms, "ORDER BY ", ", ");
    }
}
