package xw.data.eaql.model.expr;

import xw.data.eaql.util.Strings;

/**
 * Created by xhuang on 8/19/2015.
 */
public final class LiteralValue implements Expression
{
    public final Object value;

    public LiteralValue(Object value) {
        this.value = value;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitLiteralValue(this);
    }


    @Override
    public String toString() {
        return Strings.escapeValue(value);
    }
}


