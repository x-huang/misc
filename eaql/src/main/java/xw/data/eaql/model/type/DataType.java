package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/14/16.
 */
public interface DataType
{
    Category getCategory();
    Class getJavaType();
    Object getValueZero();
}
