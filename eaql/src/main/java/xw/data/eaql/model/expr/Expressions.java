package xw.data.eaql.model.expr;

import xw.data.eaql.model.type.Types;

import java.util.Set;

/**
 * Created by xhuang on 8/10/16.
 */
public class Expressions
{
    public static boolean isBooleanExpression(Expression e) {
        return e instanceof BooleanExpression;
    }

    public static boolean isArithmeticExpression(Expression e) {
        return e instanceof ArithmeticExpression;
    }

    public static boolean isUnaryExpression(Expression e) {
        return e instanceof UnaryExpression;
    }

    public static boolean isBinaryExpression(Expression e) {
        return e instanceof BinaryExpression;
    }

    public static boolean isFunction(Expression e) {
        return e instanceof Function;
    }

    public static boolean isColumnValue(Expression e) {
        return e instanceof ColumnValue;
    }

    public static boolean isLiteralValue(Expression e) {
        return e instanceof LiteralValue;
    }

    public static boolean isLiteralNull(Expression e) {
        return isLiteralValue(e) && ((LiteralValue) e).value == null;
    }

    public static boolean isLiteralTrue(Expression e) {
        if (! isLiteralValue(e))
            return false;
        final Object value = ((LiteralValue) e).value;
        return value != null && value.equals(true);
    }

    public static boolean isLiteralFalse(Expression e) {
        if (! isLiteralValue(e))
            return false;
        final Object value = ((LiteralValue) e).value;
        return value != null && value.equals(false);
    }

    public static Expression clone(Expression e) {
        final ExprDuplicator duplicator = new ExprDuplicator();
        return duplicator.duplicate(e);
    }

    private static boolean setsEqual(Set<Object> set1, Set<Object> set2) {
        for (Object elem: set1) {
            if (! set2.contains(elem))
                return false;
        }
        for (Object elem: set2) {
            if (! set1.contains(elem))
                return false;
        }
        return true;
    }

    public static boolean equal(Expression e1, Expression e2) {
        if (e1 == e2) {
            return true;
        } else if (! e1.getClass().equals(e2.getClass())) {
            return false;
        } else if (e1 instanceof Asterisk) {
            return true;
        } else if (e1 instanceof LiteralValue) {
            final Object value1 = ((LiteralValue) e1).value;
            final Object value2 = ((LiteralValue) e2).value;
            if (value1 == null) {
                return value2 == null;
            } else {
                return value1.equals(value2);
            }
        } else if (e1 instanceof ColumnValue) {
            return ((ColumnValue) e1).column.equals(((ColumnValue) e2).column);
        } else if (e1 instanceof ColumnValueIndexed) {
            return ((ColumnValueIndexed) e1).index == ((ColumnValueIndexed) e2).index;
        } else if (e1 instanceof StructFieldIndexed) {
            final StructFieldIndexed sf1 = (StructFieldIndexed) e1;
            final StructFieldIndexed sf2 = (StructFieldIndexed) e2;
            return sf1.columnIndex == sf2.columnIndex && sf1.fieldIndex == sf2.fieldIndex;
        }
        else if (e1 instanceof UnaryExpression) {
            final UnaryExpression u1 = (UnaryExpression) e1;
            final UnaryExpression u2 = (UnaryExpression) e2;
            if (! equal(u1.e, u2.e)) {
                return false;
            }
            if (u1 instanceof IsNull) {
                final IsNull is1 = (IsNull) u1;
                final IsNull is2 = (IsNull) u2;
                return is1.not == is2.not;
            } else if (u1 instanceof Like) {
                final Like like1 = (Like) u1;
                final Like like2 = (Like) u2;
                return like1.not == like2.not && like1.like.equals(like2.like);
            } else if (u1 instanceof Between) {
                final Between b1 = (Between) u1;
                final Between b2 = (Between) u2;
                return b1.not == b2.not && b1.lower.equals(b2.lower) && b1.upper.equals(b2.upper);
            } else if (u1 instanceof InSet) {
                final InSet in1 = (InSet) u1;
                final InSet in2 = (InSet) u2;
                return in1.not == in2.not && setsEqual(in1.set, in2.set);
            } else if (u1 instanceof Cast) {
                return Types.strictlyEqual(((Cast) u1).type, ((Cast) u1).type);
            } else if (u1 instanceof StructField) {
                final StructField sf1 = (StructField) u1;
                final StructField sf2 = (StructField) u2;
                return sf1.field.equals(sf2.field);
            } else {
                return true;
            }
        } else if (e1 instanceof BinaryExpression) {
            final BinaryExpression b1 = (BinaryExpression) e1;
            final BinaryExpression b2 = (BinaryExpression) e2;
            return equal(b1.e1, b2.e1) && equal(b1.e2, b2.e2);
        } else if (e1 instanceof Function) {
            final Function f1 = (Function) e1;
            final Function f2 = (Function) e2;
            if (! f1.name.equals(f2.name)) {
                return false;
            }
            if (f1.numParams() != f2.numParams()) {
                return false;
            }
            for (int i=0; i<f1.numParams(); i++) {
                if (! equal(f1.getParam(i), f2.getParam(i))) {
                    return false;
                }
            }
            return true;
        } else if (e1 instanceof CaseExprWhen) {
            final CaseExprWhen cew1 = (CaseExprWhen) e1;
            final CaseExprWhen cew2 = (CaseExprWhen) e2;
            if (cew1.numWhens() != cew2.numWhens() || !equal(cew1.e, cew2.e) || !equal(cew1.else_, cew2.else_)) {
                return false;
            }
            for (int i=0; i<cew1.numWhens(); i++) {
                if (!equal(cew1.when(i), cew2.when(i)) || !equal(cew1.then(i), cew2.then(i))) {
                    return false;
                }
            }
            return true;
        } else if (e1 instanceof CaseWhen) {
            final CaseWhen cw1 = (CaseWhen) e1;
            final CaseWhen cw2 = (CaseWhen) e2;
            if (cw1.numWhens() != cw2.numWhens() || !equal(cw1.else_, cw2.else_)) {
                return false;
            }
            for (int i=0; i<cw1.numWhens(); i++) {
                if (!equal(cw1.when(i), cw2.when(i)) || !equal(cw1.then(i), cw2.then(i))) {
                    return false;
                }
            }
            return true;
        }
        throw new RuntimeException("unexpected expression: " + e1);
    }

    public static boolean isVariableExpression(Expression e) {
        if (e instanceof LiteralValue) {
            return true;
        } else if (e instanceof ColumnValue) {
            return false;
        } else if (e instanceof UnaryExpression) {
            return isVariableExpression(((UnaryExpression) e).e);
        } else if (e instanceof BinaryExpression) {
            return isVariableExpression(((BinaryExpression) e).e1)
                    && isVariableExpression(((BinaryExpression) e).e2);
        } else if (e instanceof Function) {
            return false;
        } else if (e instanceof Asterisk) {
            throw new IllegalStateException("asterisk should have been expanded before checking variability");
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }


}
