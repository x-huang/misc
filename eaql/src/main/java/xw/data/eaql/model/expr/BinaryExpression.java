package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
abstract public class BinaryExpression implements Expression
{
    public final String op;
    public final Expression e1;
    public final Expression e2;

    public BinaryExpression(String op, Expression e1, Expression e2) {
        this.op = op;
        this.e1 = e1;
        this.e2 = e2;
    }

    @Override
    public String toString() {
        return e1.toString() + " " + op + " " + e2.toString();
    }

}
