package xw.data.eaql.model.stmt;

import xw.data.eaql.model.term.ResourceString;
import xw.data.eaql.model.term.SelectSource;
import xw.data.eaql.model.term.TableName;

/**
 * Created by xhuang on 8/10/16.
 */
public class DescribeStatement implements SQLStatement
{
    public final SelectSource source;

    public DescribeStatement(SelectSource source) {
        this.source = source;
    }

    public SelectSource.Type getSourceType() {
        if (source instanceof TableName) {
            return SelectSource.Type.TABLE;
        } if (source instanceof ResourceString) {
            return SelectSource.Type.RESOURCE;
        } else if (source instanceof SelectStatement) {
            return SelectSource.Type.SUB_QUERY;
        } else {
            throw new RuntimeException("unknown select source type: " + source.getClass().getName());
        }

    }

    @Override
    public String toString() {
        if (source instanceof TableName) {
            return "DESCRIBE TABLE " + source;
        } else if (source instanceof ResourceString) {
            return "DESCRIBE " + source;
        } else {
            throw new RuntimeException("unexpected select source: " + source);
        }
    }
}
