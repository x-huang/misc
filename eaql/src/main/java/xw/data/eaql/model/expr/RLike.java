/*
 * Copyright (c) 2017. Xiaowan Huang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package xw.data.eaql.model.expr;

import xw.data.eaql.util.Strings;

import java.util.regex.Pattern;

/**
 * Created by xhuang on 11/3/15.
 */
public class RLike extends UnaryExpression implements BooleanExpression
{
    public final boolean not;
    public final String regex;
    public final Pattern pattern;

    public RLike(Expression e, boolean not, String regex) {
        super(not? "NOT RLIKE": "RLIKE", e);
        this.not = not;
        this.regex = regex;
        if (regex != null) {
            this.pattern = Pattern.compile(regex);
        } else {
            this.pattern = null;
        }
    }

    @Override
    public String toString() {
        // return e.toString() + " LIKE '" + like + "' /*match:" + match + "*/";
        return e.toString() + " " + op + " " + Strings.escapeValue(regex);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitRLike(this);
    }
}
