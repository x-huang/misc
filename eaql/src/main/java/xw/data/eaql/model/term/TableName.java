package xw.data.eaql.model.term;

/**
 * Created by xhuang on 8/26/16.
 */
public class TableName implements SelectSource
{
    public final String db;
    public final String tbl;

    public TableName(String tbl) {
        this.db = null;
        this.tbl = tbl;
    }

    public TableName(String db, String tbl) {
        this.db = db;
        this.tbl = tbl;
    }

    @Override
    public String toString() {
        return db == null? tbl: (db + "." + tbl);
    }

    @Override
    public String getDataReference() {
        return db == null? tbl: (db + "." + tbl);
    }
}
