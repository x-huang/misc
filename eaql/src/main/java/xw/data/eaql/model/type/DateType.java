package xw.data.eaql.model.type;

import java.sql.Date;

/**
 * Created by xhuang on 8/15/16.
 */
final class DateType implements PrimitiveType
{
    @Override
    public String toString() {
        return "date";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Date.class;
    }

    @Override
    public Object getValueZero() {
        return new Date(0);
    }
}
