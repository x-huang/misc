package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/25/16.
 *
 * Note this is not existent SQL expression. It is useful in evaluation
 */
public final class ColumnValueIndexed implements Expression
{
    public final int index;

    public ColumnValueIndexed(int index) {
        this.index = index;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitColumnValueIndexed(this);
    }

    @Override
    public String toString() {
        return "$" + index;
    }
}
