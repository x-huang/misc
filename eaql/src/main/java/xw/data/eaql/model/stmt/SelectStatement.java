package xw.data.eaql.model.stmt;

import xw.data.eaql.model.clause.LimitClause;
import xw.data.eaql.model.clause.OffsetClause;
import xw.data.eaql.model.clause.OrderByClause;
import xw.data.eaql.model.term.SelectSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/10/16.
 */
public class SelectStatement implements SQLStatement, SelectSource
{
    public final List<SelectOrValuesStatement> sovs = new ArrayList<>();
    public final OrderByClause orderBy;
    public final LimitClause limit;
    public final OffsetClause offset;

    public SelectStatement(OrderByClause orderBy, LimitClause limit, OffsetClause offset) {
        this.orderBy = orderBy;
        this.limit = limit;
        this.offset = offset;
    }

    public void addSelectOrValuesStatement(SelectOrValuesStatement stmt) {
        this.sovs.add(stmt);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String compoundOp = "";
        for (SelectOrValuesStatement stmt: sovs) {
            sb.append(compoundOp).append(stmt);
            compoundOp = " UNION ALL ";
        }
        if (orderBy != null) {
            sb.append(' ').append(orderBy);
        }
        if (limit != null) {
            sb.append(' ').append(limit);
        }
        if (offset != null) {
            sb.append(' ').append(offset);
        }
        return sb.toString();
    }

    @Override
    public String getDataReference() {
        throw new RuntimeException("shall not get data reference from select statement: " + this);
    }
}
