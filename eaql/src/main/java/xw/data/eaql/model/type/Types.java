package xw.data.eaql.model.type;

import xw.data.eaql.EAQL;

import java.util.List;

/**
 * Created by xhuang on 8/15/16.
 */
public class Types
{
    public static final VoidType VOID = new VoidType();
    public static final StringType STRING = new StringType();
    // public static final VarcharType VARCHAR = new VarcharType();
    // public static final CharType CHAR = new CharType();
    public static final TinyIntType TINYINT = new TinyIntType();
    public static final SmallIntType SMALLINT = new SmallIntType();
    public static final IntType INT = new IntType();
    public static final BigIntType BIGINT = new BigIntType();
    public static final FloatType FLOAT = new FloatType();
    public static final DoubleType DOUBLE = new DoubleType();
    // public static final DecimalType DECIMAL = new DecimalType();
    public static final TimestampType TIMESTAMP = new TimestampType();
    public static final DateType DATE = new DateType();
    public static final BooleanType BOOLEAN = new BooleanType();
    public static final BinaryType BINARY = new BinaryType();

    public static StructType newStructType(List<StructType.StructField> fields) {
        return new StructType(fields);
    }

    public static ArrayType newArrayType(DataType valueType) {
        return new ArrayType(valueType);
    }

    public static MapType newMapType(PrimitiveType keyType, DataType valueType) {
        return new MapType(keyType, valueType);
    }

    public static UnionType newUnionType(List<DataType> valueTypes) {
        return new UnionType(valueTypes);
    }

    public static CharType newCharType(int length) {
        return new CharType(length);
    }

    public static VarcharType newVarcharType(int length) {
        return new VarcharType(length);
    }

    public static DecimalType newDecimalType() {
        return new DecimalType();
    }

    public static DecimalType newDecimalType(int precision) {
        return new DecimalType(precision);
    }

    public static DecimalType newDecimalType(int precision, int scale) {
        return new DecimalType(precision, scale);
    }

    public static DataType fromString(String s) {
        if (s == null) {
            throw new NullPointerException("null type string");
        }
        return EAQL.parseDataType(s);
    }

    public static boolean isPrimitiveType(DataType type) {
        return type instanceof PrimitiveType;
    }

    public static boolean isNumberType(DataType type) {
        return type instanceof NumberType;
    }

    public static boolean isIntegerNumberType(DataType type) {
        return type instanceof IntegerNumberType;
    }

    public static boolean isFloatNumberType(DataType type) {
        return type instanceof FloatNumberType;
    }

    public static boolean isDecimalType(DataType type) {
        return type instanceof DecimalType;
    }

    public static boolean isStructType(DataType type) {
        return type instanceof StructType;
    }

    public static boolean isArrayType(DataType type) {
        return type instanceof ArrayType;
    }

    public static boolean isMapType(DataType type) {
        return type instanceof MapType;
    }

    public static boolean isUnionType(DataType type) {
        return type instanceof UnionType;
    }

    public static boolean strictlyEqual(DataType t1, DataType t2) {
        if (t1 == t2) {
            return true;
        }

        final Category category = t1.getCategory();
        if (category != t2.getCategory()) {
            return false;
        }
        switch (category) {
            case PRIMITIVE:
                return t1.toString().equals(t2.toString());
            case ARRAY:
                return strictlyEqual(((ArrayType) t1).valueType, ((ArrayType) t2).valueType);
            case MAP:
                final MapType m1 = (MapType) t1;
                final MapType m2 = (MapType) t2;
                return strictlyEqual(m1.keyType, m2.keyType) && strictlyEqual(m1.valueType, m2.valueType);
            case STRUCT:
                final StructType s1 = (StructType) t1;
                final StructType s2 = (StructType) t2;
                if (s1.fields.size() != s2.fields.size()) {
                    return false;
                }
                for (int i = 0; i < s1.fields.size(); i++) {
                    if (! s1.fields.get(i).name.equals(s2.fields.get(i).name)) {
                        return false;
                    }
                    if (! strictlyEqual(s1.fields.get(i).type, s2.fields.get(i).type)) {
                        return false;
                    }
                }
                return true;

            case UNION:
                final UnionType u1 = (UnionType) t1;
                final UnionType u2 = (UnionType) t2;
                if (u1.valueTypes.size() != u2.valueTypes.size()) {
                    return false;
                }
                for (int i=0; i<u1.valueTypes.size(); i++) {
                    if (! strictlyEqual(u1.valueTypes.get(i), u2.valueTypes.get(i))) {
                        return false;
                    }
                }
                return true;

            default:
                throw new IllegalStateException("unknown type category: " + category);
        }
    }

    public static boolean semanticallyEqual(DataType t1, DataType t2) {
        if (t1 == t2) {
            return true;
        }

        final Category category = t1.getCategory();
        if (category != t2.getCategory()) {
            return false;
        }
        switch (category) {
            case PRIMITIVE:
                return t1.toString().equals(t2.toString());
            case ARRAY:
                return semanticallyEqual(((ArrayType) t1).valueType, ((ArrayType) t2).valueType);
            case MAP:
                final MapType m1 = (MapType) t1;
                final MapType m2 = (MapType) t2;
                return semanticallyEqual(m1.keyType, m2.keyType) && semanticallyEqual(m1.valueType, m2.valueType);
            case STRUCT:
                final StructType s1 = (StructType) t1;
                final StructType s2 = (StructType) t2;
                if (s1.fields.size() != s2.fields.size()) {
                    return false;
                }
                for (int i = 0; i < s1.fields.size(); i++) {
                    if (!semanticallyEqual(s1.fields.get(i).type, s2.fields.get(i).type)) {
                        return false;
                    }
                }
                return true;

            case UNION:
                final UnionType u1 = (UnionType) t1;
                final UnionType u2 = (UnionType) t2;
                if (u1.valueTypes.size() != u2.valueTypes.size()) {
                    return false;
                }
                for (int i=0; i<u1.valueTypes.size(); i++) {
                    if (! semanticallyEqual(u1.valueTypes.get(i), u2.valueTypes.get(i))) {
                        return false;
                    }
                }
                return true;

            default:
                throw new IllegalStateException("unknown type category: " + category);
        }
    }


    private Types() {
        // prevent creation
    }
}
