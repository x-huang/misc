package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
abstract public class UnaryExpression implements Expression
{
    public final String op;
    public final Expression e;

    public UnaryExpression(String op, Expression e) {
        this.op = op;
        this.e = e;
    }

    @Override
    public String toString() {
        return op + e.toString();
    }
}
