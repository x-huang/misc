package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 9/5/16.
 */
public interface ExprReplacer
{
    Expression replace(Expression e);
}
