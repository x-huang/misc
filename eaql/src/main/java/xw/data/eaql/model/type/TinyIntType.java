package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class TinyIntType implements IntegerNumberType
{
    @Override
    public String toString() {
        return "tinyint";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Byte.class;
    }

    @Override
    public Object getValueZero() {
        return (byte) 0;
    }
}
