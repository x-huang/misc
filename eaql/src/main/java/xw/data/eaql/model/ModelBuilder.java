package xw.data.eaql.model;

import xw.data.eaql.model.expr.*;
import xw.data.eaql.model.term.*;
import xw.data.eaql.model.clause.*;
import xw.data.eaql.model.stmt.*;
import xw.data.eaql.model.type.*;
import xw.data.eaql.parser.EAQLParser;
import xw.data.eaql.parser.EAQLVisitor;
import xw.data.eaql.util.Strings;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/10/16.
 */
public class ModelBuilder implements EAQLVisitor<Object>
{
    @Override
    public Object visitParse(EAQLParser.ParseContext ctx) {
        final List<SQLStatement> stmts = new ArrayList<>();
        for (EAQLParser.Sql_stmtContext childCtx: ctx.sql_stmt()) {
            stmts.add((SQLStatement) visit(childCtx));
        }
        return stmts;
    }

    @Override
    public Object visitError(EAQLParser.ErrorContext ctx) {
        return null;
    }

    @Override
    public Object visitSql_stmt(EAQLParser.Sql_stmtContext ctx) {
        if (ctx.describe_table_stmt() != null) {
            return visit(ctx.describe_table_stmt());
        } else if (ctx.select_stmt() != null) {
            return visit(ctx.select_stmt());
        } else if (ctx.insert_stmt() != null) {
            return visit(ctx.insert_stmt());
        } else {
            throw new RuntimeException("unknown sql statement: " + ctx.getText());
        }
    }

    @Override
    public Object visitDescribe_table_stmt(EAQLParser.Describe_table_stmtContext ctx) {
        if (ctx.table_name() != null) {
            final TableName tableName = (TableName) visit(ctx.table_name());
            return new DescribeStatement(tableName);
        } else {
            final ResourceString resource = (ResourceString) visit(ctx.resource_descriptor());
            return new DescribeStatement(resource);
        }
    }

    @Override
    public Object visitSelect_stmt(EAQLParser.Select_stmtContext ctx) {
        OrderByClause orderBy = null;
        if (ctx.order_by_clause() != null) {
            orderBy = (OrderByClause) visit(ctx.order_by_clause());
        }
        LimitClause limit = null;
        if (ctx.limit_clause() != null) {
            limit = (LimitClause) visit(ctx.limit_clause());
        }
        OffsetClause offset = null;
        if (ctx.offset_clause() != null) {
            offset = (OffsetClause) visit(ctx.offset_clause());
        }
        final SelectStatement selectStmt = new SelectStatement(orderBy, limit, offset);
        for (EAQLParser.Select_or_valuesContext childCtx: ctx.select_or_values()) {
            selectStmt.addSelectOrValuesStatement((SelectOrValuesStatement) visit(childCtx));
        }
        return selectStmt;
    }

    @Override
    public Object visitSelect_or_values(EAQLParser.Select_or_valuesContext ctx) {
        if (ctx.simple_select_stmt() != null) {
            return visit(ctx.simple_select_stmt());
        } else if (ctx.values_stmt() != null) {
            return visit(ctx.values_stmt());
        } else {
            throw new RuntimeException("unknown select statemnet: " + ctx.getText());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitValues_stmt(EAQLParser.Values_stmtContext ctx) {
        final ValuesStatement stmt = new ValuesStatement();
        for (EAQLParser.Expr_listContext childCtx: ctx.expr_list()) {
            stmt.addExprList((List<Expression>) visit(childCtx));
        }
        return stmt;
    }

    @Override
    public Object visitExpr_list(EAQLParser.Expr_listContext ctx) {
        final List<Expression> exprs = new ArrayList<>();
        for (EAQLParser.ExprContext childCtx: ctx.expr()) {
            exprs.add((Expression) visit(childCtx));
        }
        return exprs;
    }

    @Override
    public Object visitSimple_select_stmt(EAQLParser.Simple_select_stmtContext ctx) {
        final SelectClause select = (SelectClause) visit(ctx.select_clause());
        FromClause from = null;
        if (ctx.from_clause() != null) {
            from = (FromClause) visit(ctx.from_clause());
        }
        WhereClause where = null;
        if (ctx.where_clause() != null) {
            where = (WhereClause) visit(ctx.where_clause());
        }
        GroupByClause groupBy = null;
        if (ctx.group_by_clause() != null) {
            groupBy = (GroupByClause) visit(ctx.group_by_clause());
        }
        return new SimpleSelectStatement(select, from, where, groupBy);
    }

    @Override
    public Object visitSelect_clause(EAQLParser.Select_clauseContext ctx) {
        final SelectClause select = new SelectClause(ctx.K_DISTINCT() != null);
        for (EAQLParser.Result_columnContext childCtx: ctx.result_column()) {
            select.addResultColumn((ResultColumn) visit(childCtx));
        }
        return select;
    }

    @Override
    public Object visitFrom_clause(EAQLParser.From_clauseContext ctx) {
        if (ctx.table_name() != null) {
            final TableName tbl = (TableName) visit(ctx.table_name());
            return new FromClause(tbl);
        } else if (ctx.resource_descriptor() != null) {
            final ResourceString rs = (ResourceString) visit(ctx.resource_descriptor());
            return new FromClause(rs);
        } else {
            final SelectStatement selectStmt = (SelectStatement) visit(ctx.select_stmt());
            return new FromClause(selectStmt);
        }
    }

    @Override
    public Object visitWhere_clause(EAQLParser.Where_clauseContext ctx) {
        final Expression e = (Expression) visit(ctx.expr());
        return new WhereClause(e);
    }

    @Override
    public Object visitGroup_by_clause(EAQLParser.Group_by_clauseContext ctx) {
        final GroupByClause groupBy = new GroupByClause();
        for (EAQLParser.Column_nameContext childCtx: ctx.column_name()) {
            final ColumnValue col = (ColumnValue) visit(childCtx);
            groupBy.addColumn(col.column);
        }
        return groupBy;
    }

    @Override
    public Object visitOrder_by_clause(EAQLParser.Order_by_clauseContext ctx) {
        final OrderByClause orderBy = new OrderByClause();
        for (EAQLParser.Ordering_termContext childCtx: ctx.ordering_term()) {
            orderBy.addOrderingTerm((OrderingTerm) visit(childCtx));
        }
        return orderBy;
    }

    @Override
    public Object visitLimit_clause(EAQLParser.Limit_clauseContext ctx) {
        final long num = Long.parseLong(ctx.NUMERIC_LITERAL().getText());
        return new LimitClause(num);
    }

    @Override
    public Object visitOffset_clause(EAQLParser.Offset_clauseContext ctx) {
        final long num = Long.parseLong(ctx.NUMERIC_LITERAL().getText());
        return new OffsetClause(num);
    }

    @Override
    public Object visitInsert_stmt(EAQLParser.Insert_stmtContext ctx) {
        final ResourceString resource = (ResourceString) visit(ctx.resource_descriptor());
        final boolean overwrite = ctx.K_OVERWRITE() != null;
        final SelectStatement selectStmt = (SelectStatement) visit(ctx.select_stmt());
        return new InsertStatement(resource, overwrite, selectStmt);
    }

    @Override
    public Object visitExpr(EAQLParser.ExprContext ctx) {
        if (ctx.asterisk() != null) {
            return visit(ctx.asterisk());
        } else if (ctx.expr().size() == 0 && ctx.literal_value().size() == 1) {
            return visit(ctx.literal_value(0));
        } else if (ctx.column_name() != null) {
            return visit(ctx.column_name());
        } else if (ctx.function() != null) {
            return visit(ctx.function());
        } else if (ctx.parenthetical() != null) {
            return visit(ctx.parenthetical());
        } else if (ctx.unary_operator() != null) {
            final String op = (String) visit(ctx.unary_operator());
            final Expression e = (Expression) visit(ctx.expr(0));
            switch (op.toUpperCase()) {
                case "+": return e;
                case "-": return new Negation(e);
                default: throw new RuntimeException("unknown unary operator: " + op);
            }
        } else if (ctx.K_NULL() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            return new IsNull(e, ctx.K_NOT() != null);
        } else if (ctx.K_LIKE() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            final LiteralValue literal = (LiteralValue) visit(ctx.literal_value(0));
            final String pattern = (String) literal.value;
            return new Like(e, ctx.K_NOT() != null, pattern);
        } else if (ctx.K_RLIKE() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            final LiteralValue literal = (LiteralValue) visit(ctx.literal_value(0));
            final String regex = (String) literal.value;
            return new RLike(e, ctx.K_NOT() != null, regex);
        } else if (ctx.K_BETWEEN() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            final Object lower = ((LiteralValue) visit(ctx.literal_value(0))).value;
            final Object upper = ((LiteralValue) visit(ctx.literal_value(1))).value;
            return new Between(e, ctx.K_NOT() != null, lower, upper);
        } else if (ctx.K_IN() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            final List<Object> set = new ArrayList<>();
            for (EAQLParser.Literal_valueContext childCtx: ctx.literal_value()) {
                set.add(((LiteralValue) visit(childCtx)).value);
            }
            return new InSet(e, ctx.K_NOT() != null, set);
        } else if (ctx.K_CAST() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            final DataType type = (DataType) visit(ctx.data_type());
            return new Cast(e, type);
        } else if (ctx.K_NOT() != null) {
            final Expression e = (Expression) visit(ctx.expr(0));
            return new Not(e);
        } else if (ctx.K_CASE() != null) {
            if (ctx.getChild(1) instanceof EAQLParser.ExprContext) {
                // case expr when;
                final int n = ctx.expr().size();
                assert n % 2 == 0;
                final Expression e = (Expression) visit(ctx.expr(0));
                final Expression else_ = (Expression) visit(ctx.expr(n-1));
                final CaseExprWhen caseWhen = new CaseExprWhen(e, else_);
                for (int i=1; i<n-1; i+=2) {
                    final Expression when = (Expression) visit(ctx.expr(i));
                    final Expression then = (Expression) visit(ctx.expr(i+1));
                    caseWhen.addWhenThenPair(when, then);
                }
                return caseWhen;
            } else {
                // case when ;
                final int n = ctx.expr().size();
                assert n % 2 == 1;
                final Expression else_ = (Expression) visit(ctx.expr(n-1));
                final CaseWhen caseWhen = new CaseWhen(else_);
                for (int i=0; i<n-1; i+=2) {
                    final Expression when = (Expression) visit(ctx.expr(i));
                    final Expression then = (Expression) visit(ctx.expr(i+1));
                    caseWhen.addWhenThenPair(when, then);
                }
                return caseWhen;
            }
        } else {
            final String op = (String) visit(ctx.getChild(1));
            if (op.equals(".")) {
                final Expression structColumn = (Expression) visit(ctx.expr(0));
                final String field = ctx.IDENTIFIER().getText();
                return new StructField(structColumn, field);
            }
            if (op.equals("[")) {
                final Expression collectionColumn = (Expression) visit(ctx.expr(0));
                final Expression indexKey = (Expression) visit(ctx.expr(1));
                return new ArrayMapElement(collectionColumn, indexKey);
            }
            final Expression l = (Expression) visit(ctx.expr(0));
            final Expression r = (Expression) visit(ctx.expr(1));
            switch (op.toUpperCase()) {
                case "*": return new Multiplication(l, r);
                case "/": return new Division(l, r);
                case "+": return new Addition(l, r);
                case "-": return new Subtraction(l, r);
                case "<": return new LessThan(l, r);
                case "<=": return new LessThanEquals(l, r);
                case ">": return new GreaterThan(l, r);
                case ">=": return new GreaterThanEquals(l, r);
                case "=":
                case "==": return new Equals(l, r);
                case "!=":
                case "<>": return new NotEquals(l, r);
                case "AND":
                case "&&": return new And(l, r);
                case "OR":
                case "||": return new Or(l, r);
                default: throw new RuntimeException("unknown binary operator: " + op);
            }
        }
    }

    @Override
    public Object visitOrdering_term(EAQLParser.Ordering_termContext ctx) {
        final Expression e = (Expression) visit(ctx.expr());
        final boolean asc = ctx.K_DESC() == null;
        return new OrderingTerm(e, asc);
    }

    @Override
    public Object visitResult_column(EAQLParser.Result_columnContext ctx) {
        final Expression e = (Expression) visit(ctx.expr());
        final String alias = ctx.K_AS()!=null? (String) visit(ctx.column_alias()): null;
        return new ResultColumn(e, alias);
    }

    @Override
    public Object visitCompound_operator(EAQLParser.Compound_operatorContext ctx) {
        return ctx.getText();
    }

    @Override
    public Object visitLiteral_value(EAQLParser.Literal_valueContext ctx) {
        final String text = ctx.getText();
        if (ctx.NUMERIC_LITERAL() != null) {
            if (text.contains(".")) {
                final double d = Double.parseDouble(text);
                return new LiteralValue(d);
            } else {
                final long l = Long.parseLong(text);
                if (l == 0L) {
                    return LiteralConstants.literalZero;
                } else if (l == 1L) {
                    return LiteralConstants.literalOne;
                } else {
                    return new LiteralValue(l);
                }
            }
        } else if (ctx.STRING_LITERAL() != null) {
            final String s = Strings.stripLiteralString(text);
            if (s.isEmpty()) {
                return LiteralConstants.literalEmptyString;
            } else {
                return new LiteralValue(s);
            }
        } else if (ctx.K_TRUE() != null) {
            return LiteralConstants.literalTrue;
        } else if (ctx.K_FALSE() != null) {
            return LiteralConstants.literalFalse;
        } else if (ctx.K_NULL() != null) {
            return LiteralConstants.literalNull;
        } else {
            throw new RuntimeException("unknown literal value: " + text);
        }
    }

    @Override
    public Object visitAsterisk(EAQLParser.AsteriskContext ctx) {
        return new Asterisk();
    }

    @Override
    public Object visitFunction(EAQLParser.FunctionContext ctx) {
        final Function function = new Function(ctx.function_name().getText());
        for (EAQLParser.ExprContext childCtx: ctx.expr()) {
            function.addParam((Expression) visit(childCtx));
        }
        return function;
    }

    @Override
    public Object visitUnary_operator(EAQLParser.Unary_operatorContext ctx) {
        return ctx.getText();
    }

    @Override
    public Object visitParenthetical(EAQLParser.ParentheticalContext ctx) {
        final Expression e = (Expression) visit(ctx.expr());
        return new Parenthetical(e);
    }

    @Override
    public Object visitError_message(EAQLParser.Error_messageContext ctx) {
        return null;
    }

    @Override
    public Object visitColumn_alias(EAQLParser.Column_aliasContext ctx) {
        if (ctx.IDENTIFIER() != null) {
            return ctx.IDENTIFIER().getText();
        } else {
            return Strings.stripLiteralString(ctx.STRING_LITERAL().getText());
        }
    }

    @Override
    public Object visitKeyword(EAQLParser.KeywordContext ctx) {
        return ctx.getText();
    }

    @Override
    public Object visitData_type(EAQLParser.Data_typeContext ctx) {
        if (ctx.K_STRUCT() != null) {
            final List<StructType.StructField> fields = new ArrayList<>();
            for (EAQLParser.Struct_fieldContext childCtx: ctx.struct_field()) {
                fields.add((StructType.StructField) visit(childCtx));
            }
            return Types.newStructType(fields);
        } else if (ctx.K_MAP() != null) {
            final PrimitiveType keyType = (PrimitiveType) visit(ctx.primitive_type());
            final DataType valueType = (DataType) visit(ctx.data_type(0));
            return Types.newMapType(keyType, valueType);
        } else if (ctx.K_ARRAY() != null) {
            final DataType type = (DataType) visit(ctx.data_type(0));
            return Types.newArrayType(type);
        } else if (ctx.K_UNIONTYPE() != null) {
            final List<DataType> valueTypes = new ArrayList<>();
            for (EAQLParser.Data_typeContext childCtx: ctx.data_type()) {
                valueTypes.add((DataType) visit(childCtx));
            }
            return Types.newUnionType(valueTypes);
        } else {
            return visit(ctx.primitive_type());
        }
    }

    @Override
    public Object visitStruct_field(EAQLParser.Struct_fieldContext ctx) {
        final String column = ((ColumnValue) visit(ctx.column_name())).column;
        final DataType type;
        if (ctx.data_type() == null) {
            type = Types.STRING;
        } else {
            type = (DataType) visit(ctx.data_type());
        }
        return new StructType.StructField(column, type);
    }

    @Override
    public Object visitPrimitive_type(EAQLParser.Primitive_typeContext ctx) {
        if (ctx.K_STRING() != null) {
            return Types.STRING;
        } else if (ctx.K_VARCHAR() != null) {
            final int len = Integer.parseInt(ctx.NUMERIC_LITERAL(0).getText());
            return Types.newVarcharType(len);
        } else if (ctx.K_CHAR() != null) {
            final int len = Integer.parseInt(ctx.NUMERIC_LITERAL(0).getText());
            return Types.newCharType(len);
        } else if (ctx.K_TINYINT() != null) {
            return Types.TINYINT;
        } else if (ctx.K_SMALLINT() != null) {
            return Types.SMALLINT;
        } else if (ctx.K_INT() != null) {
            return Types.INT;
        } else if (ctx.K_BIGINT() != null) {
            return Types.BIGINT;
        } else if (ctx.K_FLOAT() != null) {
            return Types.FLOAT;
        } else if (ctx.K_DOUBLE() != null) {
            return Types.DOUBLE;
        } else if (ctx.K_DECIMAL() != null) {
            switch (ctx.NUMERIC_LITERAL().size()) {
                case 0:
                    return Types.newDecimalType();
                case 1: {
                    final int precision = Integer.parseInt(ctx.NUMERIC_LITERAL(0).getText());
                    return Types.newDecimalType(precision);
                }
                case 2: {
                    final int precision = Integer.parseInt(ctx.NUMERIC_LITERAL(0).getText());
                    final int scale = Integer.parseInt(ctx.NUMERIC_LITERAL(1).getText());
                    return Types.newDecimalType(precision, scale);
                }
                default:
                    throw new RuntimeException("unexpected decimal type: " + ctx.getText());
            }

        } else if (ctx.K_TIMESTAMP() != null) {
            return Types.TIMESTAMP;
        } else if (ctx.K_DATE() != null) {
            return Types.DATE;
        } else if (ctx.K_BOOLEAN() != null) {
            return Types.BOOLEAN;
        } else if (ctx.K_BINARY() != null) {
            return Types.BINARY;
        } else {
            throw new IllegalStateException("unexpected primitive type: " + ctx.getText());
        }
    }

    @Override
    public Object visitTable_name(EAQLParser.Table_nameContext ctx) {
        if (ctx.IDENTIFIER().size() == 1) {
            return new TableName(null, ctx.IDENTIFIER(0).getText());
        } else {
            return new TableName(ctx.IDENTIFIER(0).getText(), ctx.IDENTIFIER(1).getText());
        }
    }

    @Override
    public Object visitResource_descriptor(EAQLParser.Resource_descriptorContext ctx) {
        return new ResourceString(Strings.stripLiteralString(ctx.getText()));
    }

    @Override
    public Object visitFunction_name(EAQLParser.Function_nameContext ctx) {
        return visit(ctx.any_name());
    }

    @Override
    public Object visitColumn_name(EAQLParser.Column_nameContext ctx) {
        final String col = (String) visit(ctx.any_name());
        return new ColumnValue(col);
    }

    @Override
    public Object visitAny_name(EAQLParser.Any_nameContext ctx) {
        if (ctx.IDENTIFIER() != null) {
            return ctx.IDENTIFIER().getText();
        } else if (ctx.STRING_LITERAL() != null) {
            return Strings.stripLiteralString(ctx.STRING_LITERAL().getText());
        } else if (ctx.keyword() != null) {
            return visit(ctx.keyword());
        } else {
            return visit(ctx.any_name());
        }
    }

    @Override
    public Object visit(ParseTree parseTree) {
        return parseTree.accept(this);
    }

    @Override
    public Object visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public Object visitTerminal(TerminalNode terminalNode) {
        return terminalNode.getText();
    }

    @Override
    public Object visitErrorNode(ErrorNode errorNode) {
        return null;
    }

}
