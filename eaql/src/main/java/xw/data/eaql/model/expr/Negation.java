package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public final class Negation extends UnaryExpression implements ArithmeticExpression
{
    public Negation(Expression e) {
        super("-", e);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitNegation(this);
    }
}
