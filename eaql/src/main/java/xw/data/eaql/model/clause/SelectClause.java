package xw.data.eaql.model.clause;

import xw.data.eaql.model.term.ResultColumn;
import xw.data.eaql.util.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/10/16.
 */
public class SelectClause
{
    public final boolean distinct;
    public final List<ResultColumn> resultColumns = new ArrayList<>();


    public SelectClause(boolean distinct) {
        this.distinct = distinct;
    }

    public void addResultColumn(ResultColumn rc) {
        resultColumns.add(rc);
    }

    @Override
    public String toString() {
        return Strings.join(resultColumns, (distinct? "SELECT DISTINCT ": "SELECT "), ", ");
    }
}
