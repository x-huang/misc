package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 9/7/16.
 */
public class CaseExprWhen extends AbstractCaseWhen
{
    public final Expression e;
    public final Expression else_;

    public CaseExprWhen(Expression e, Expression else_) {
        this.e = e;
        this.else_ = else_;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitCaseExprWhen(this);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CASE ").append(e);
        for (WhenThenPair pair: whenThenPairs) {
            sb.append(" WHEN ").append(pair.when).append(" THEN ").append(pair.then);
        }
        sb.append(" ELSE ").append(else_).append(" END");
        return sb.toString();
    }
}
