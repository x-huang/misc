package xw.data.eaql.model.clause;

/**
 * Created by xhuang on 9/15/16.
 */
public class LimitClause
{
    public final long num;

    public LimitClause(long num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "LIMIT " + num;
    }
}
