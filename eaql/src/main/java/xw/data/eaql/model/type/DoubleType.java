package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class DoubleType implements FloatNumberType
{
    @Override
    public String toString() {
        return "double";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Double.class;
    }

    @Override
    public Object getValueZero() {
        return 0.0;
    }
}
