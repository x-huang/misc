package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public class Not extends UnaryExpression implements BooleanExpression
{
    public Not(Expression e) {
        super("NOT", e);
    }

    @Override
    public String toString() {
        return "NOT " + e.toString();
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitNot(this);
    }
}
