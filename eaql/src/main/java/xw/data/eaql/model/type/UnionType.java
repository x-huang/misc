package xw.data.eaql.model.type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/15/16.
 */
final public class UnionType implements CompositeType
{
    public final List<DataType> valueTypes;

    protected UnionType() {
        this.valueTypes = new ArrayList<>();
    }

    protected UnionType(List<DataType> valueTypes) {
        this.valueTypes = new ArrayList<>(valueTypes);
    }

    public void addType(DataType type) {
        this.valueTypes.add(type);
    }

    @Override
    public String toString() {
        String s = "uniontype<";
        String delimiter = "";

        for (DataType type: valueTypes) {
            s += delimiter + type.toString();
            delimiter = ",";
        }
        return s + ">";
    }

    @Override
    public Category getCategory() {
        return Category.UNION;
    }

    @Override
    public Class getJavaType() {
        return List.class;  // I don't know actually
    }

    @Override
    public Object getValueZero() {
        return new ArrayList<>();
    }
}
