package xw.data.eaql.model.expr;

import xw.data.eaql.util.Strings;

/**
 * Created by xhuang on 11/3/15.
 */
public class Like extends UnaryExpression implements BooleanExpression
{
    public final boolean not;
    public final String like;
    public final String match;
    public final boolean startsWith;
    public final boolean endsWith;

    private static String stringTrim(String s, String t) {
        while (s.startsWith(t)) {
            s = s.substring(t.length());
        }
        while (s.endsWith(t)) {
            s = s.substring(0, s.length() - t.length());
        }
        return s;
    }

    public Like(Expression e, boolean not, String like) {
        super(not? "NOT LIKE": "LIKE", e);
        this.not = not;
        this.like = like;
        if (like != null) {
            this.startsWith = like.startsWith("%");
            this.endsWith = like.endsWith("%");
            this.match = stringTrim(like, "%");
        } else {
            this.startsWith = false;
            this.endsWith = false;
            this.match = null;
        }
    }

    @Override
    public String toString() {
        // return e.toString() + " LIKE '" + like + "' /*match:" + match + "*/";
        return e.toString() + " " + op + " " + Strings.escapeValue(this.like);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitLike(this);
    }
}
