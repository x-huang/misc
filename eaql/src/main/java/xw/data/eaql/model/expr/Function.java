package xw.data.eaql.model.expr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/4/16.
 */
public final class Function implements Expression
{
    public final String name;
    public final List<Expression> params;

    public Function(String name, List<Expression> params) {
        this.name = name;
        this.params = new ArrayList<>(params);
    }

    public Function(String name) {
        this.name = name;
        this.params = new ArrayList<>();
    }

    public void addParam(Expression param) {
        this.params.add(param);
    }

    public int numParams() {
        return params.size();
    }

    public Expression getParam(int idx) {
        return params.get(idx);
    }

    public void assertNumParams(int n) {
        if (params.size() != n) {
            throw new RuntimeException("function '" + name
                    + "' expects " + n + " parameters, got "
                    + params.size() + ": " + this.toString());
        }
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitFunction(this);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(name).append('(');
        String delim = "";
        for (Expression param: params) {
            sb.append(delim).append(param.toString());
            delim = ", ";
        }
        sb.append(')');
        return sb.toString();
    }
}
