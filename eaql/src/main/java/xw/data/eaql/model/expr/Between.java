package xw.data.eaql.model.expr;

import xw.data.eaql.util.Strings;

/**
 * Created by xhuang on 11/4/15.
 */
public class Between extends UnaryExpression implements BooleanExpression
{
    public final boolean not;
    public final Object lower;
    public final Object upper;

    public Between(Expression e, boolean not, Object lower, Object upper) {
        super(not? "NOT BETWEEN": "BETWEEN", e);
        this.not = not;
        this.lower = lower;
        this.upper = upper;
    }

    @Override
    public String toString() {
        return e.toString() + " " + op + " " + Strings.escapeValue(lower)
                + " AND " + Strings.escapeValue(upper);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitBetween(this);
    }
}
