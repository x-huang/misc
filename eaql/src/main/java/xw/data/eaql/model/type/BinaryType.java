package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class BinaryType implements PrimitiveType
{
    @Override
    public String toString() {
        return "binary";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return byte[].class;
    }

    @Override
    public Object getValueZero() {
        return new byte[0];
    }
}
