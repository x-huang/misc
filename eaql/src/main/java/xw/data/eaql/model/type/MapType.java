package xw.data.eaql.model.type;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xhuang on 8/15/16.
 */
public final class MapType implements CompositeType
{
    public final PrimitiveType keyType;
    public final DataType valueType;

    public MapType(PrimitiveType keyType, DataType valueType) {
        this.keyType = keyType;
        this.valueType = valueType;
    }

    @Override
    public String toString() {
        return "map<" + keyType + "," + valueType + ">";
    }

    @Override
    public Category getCategory() {
        return Category.MAP;
    }

    @Override
    public Class getJavaType() {
        return Map.class;
    }

    @Override
    public Object getValueZero() {
        return new HashMap<>();
    }
}
