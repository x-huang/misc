package xw.data.eaql.model.stmt;

import xw.data.eaql.model.term.ResourceString;

/**
 * Created by xhuang on 9/16/16.
 */
public class InsertStatement implements SQLStatement
{
    public final ResourceString destination;
    public final boolean overwrite;
    public final SelectStatement selectStmt;

    public InsertStatement(ResourceString destination, boolean overwrite,
                           SelectStatement selectStmt) {
        this.destination = destination;
        this.overwrite = overwrite;
        this.selectStmt = selectStmt;
    }

    @Override
    public String toString() {
        return "INSERT " + (overwrite?"OVERWRITE":"INTO") + " " + destination
                + selectStmt.toString();
    }

}
