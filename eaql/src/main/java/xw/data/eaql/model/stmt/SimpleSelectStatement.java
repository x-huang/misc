package xw.data.eaql.model.stmt;

import xw.data.eaql.model.clause.FromClause;
import xw.data.eaql.model.clause.GroupByClause;
import xw.data.eaql.model.clause.SelectClause;
import xw.data.eaql.model.clause.WhereClause;

/**
 * Created by xhuang on 8/10/16.
 */
public class SimpleSelectStatement implements SelectOrValuesStatement
{
    public final SelectClause select;
    public final FromClause from;
    public final WhereClause where;
    public final GroupByClause groupBy;

    public SimpleSelectStatement(SelectClause select, FromClause from,
                                 WhereClause where, GroupByClause groupBy) {
        this.select = select;
        this.from = from;
        this.where = where;
        this.groupBy = groupBy;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(select.toString());
        if (from != null) {
            sb.append(' ').append(from);
        }
        if (where != null) {
            sb.append(' ').append(where);
        }
        if (groupBy != null) {
            sb.append(' ').append(groupBy);
        }
        return sb.toString();
    }
}
