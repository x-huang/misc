package xw.data.eaql.model.stmt;

import xw.data.eaql.model.expr.Expression;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 9/1/16.
 */
public class ValuesStatement implements SelectOrValuesStatement
{
    public final List<List<Expression>> values = new ArrayList<>();

    public void addExprList(List<Expression>  exprs) {
        values.add(exprs);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String delim = "";
        sb.append("VALUES ");
        for (List<Expression> exprs: values) {
            sb.append(delim).append("(");
            String delim2 = "";
            for (Expression e: exprs) {
                sb.append(delim2).append(e);
                delim2 = ", ";
            }
            sb.append(")");
            delim = ", ";
        }
        return sb.toString();
    }
}
