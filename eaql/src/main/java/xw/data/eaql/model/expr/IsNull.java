package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public class IsNull extends UnaryExpression implements BooleanExpression
{
    public final boolean not;

    public IsNull(Expression e, boolean not) {
        super(not? "IS NOT NULL": "IS NULL", e);
        this.not = not;
    }

    @Override
    public String toString() {
        return e.toString() + " " + op;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitIsNull(this);
    }
}
