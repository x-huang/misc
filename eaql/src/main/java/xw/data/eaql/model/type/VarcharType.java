package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class VarcharType implements PrimitiveType
{
    public final int length;

    public VarcharType(int length) {
        if (length < 1 || length > 65355) {
            throw new IllegalArgumentException("bad varchar length: " + length);
        }
        this.length = length;
    }

    @Override
    public String toString() {
        return "varchar(" + length + ")";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return String.class;
    }

    @Override
    public Object getValueZero() {
        return "";
    }
}
