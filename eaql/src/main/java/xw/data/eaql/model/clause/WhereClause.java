package xw.data.eaql.model.clause;

import xw.data.eaql.model.expr.Expression;

/**
 * Created by xhuang on 8/10/16.
 */
public class WhereClause
{
    public final Expression e;

    public WhereClause(Expression e) {
        this.e = e;
    }

    @Override
    public String toString() {
        return "WHERE " + e.toString();
    }
}
