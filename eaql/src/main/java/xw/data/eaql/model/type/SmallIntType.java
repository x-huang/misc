package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class SmallIntType implements IntegerNumberType
{
    @Override
    public String toString() {
        return "smallint";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Short.class;
    }

    @Override
    public Object getValueZero() {
        return (short) 0;
    }
}
