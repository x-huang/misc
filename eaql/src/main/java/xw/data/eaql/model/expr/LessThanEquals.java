package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 11/3/15.
 */
public final class LessThanEquals extends BinaryExpression implements BooleanExpression
{
    public LessThanEquals(Expression e1, Expression e2) {
        super("<=", e1, e2);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitLessThanEquals(this);
    }
}
