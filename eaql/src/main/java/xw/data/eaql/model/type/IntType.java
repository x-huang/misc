package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/15/16.
 */
final class IntType implements IntegerNumberType
{
    @Override
    public String toString() {
        return "int";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Integer.class;
    }

    @Override
    public Object getValueZero() {
        return 0;
    }
}
