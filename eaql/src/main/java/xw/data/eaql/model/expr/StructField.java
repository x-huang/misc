package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 9/3/16.
 */
public class StructField extends UnaryExpression
{
    public final String field;

    public StructField(Expression e, String field) {
        super(".", e);
        this.field = field;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitStructField(this);
    }

    @Override
    public String toString() {
        return e.toString() + "." + field;
    }
}
