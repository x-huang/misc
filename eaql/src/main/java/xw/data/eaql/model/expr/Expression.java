package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 8/18/2015.
 */
public interface Expression
{
    // Object eval(Object[] record);
    <T> T accept(ExprVisitor<T> visitor);
    // void setResultType(DataType type);
    // DataType resultType();
}
