package xw.data.eaql.model.expr;

import xw.data.eaql.model.type.DataType;

/**
 * Created by xhuang on 8/14/16.
 */
public final class Cast extends UnaryExpression
{
    public final DataType type;

    public Cast(Expression e, DataType type) {
        super("CAST", e);
        this.type = type;
    }

    @Override
    public String toString() {
        return "CAST (" + e.toString() + " AS " + type.toString() + ")";
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitCast(this);
    }
}
