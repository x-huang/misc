package xw.data.eaql.model.type;

import java.sql.Timestamp;

/**
 * Created by xhuang on 8/15/16.
 */
final class TimestampType implements PrimitiveType
{
    @Override
    public String toString() {
        return "timestamp";
    }

    @Override
    public Category getCategory() {
        return Category.PRIMITIVE;
    }

    @Override
    public Class getJavaType() {
        return Timestamp.class;
    }

    @Override
    public Object getValueZero() {
        return new Timestamp(0);
    }
}
