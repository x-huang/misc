package xw.data.eaql.model.expr;

/**
 * Created by xhuang on 9/3/16.
 */
public class StructFieldIndexed implements Expression
{
    public final int columnIndex;
    public final int fieldIndex;

    public StructFieldIndexed(int columnIndex, int fieldIndex) {
        this.columnIndex = columnIndex;
        this.fieldIndex = fieldIndex;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) {
        return visitor.visitStructFieldIndexed(this);
    }

    @Override
    public String toString() {
        return  "$" + columnIndex + ".$" + fieldIndex;

    }
}
