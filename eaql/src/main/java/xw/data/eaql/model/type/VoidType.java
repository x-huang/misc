package xw.data.eaql.model.type;

/**
 * Created by xhuang on 8/30/16.
 */
public class VoidType implements DataType
{
    @Override
    public String toString() {
        return "void";
    }

    @Override
    public Category getCategory() {
        return Category.VOID;
    }

    @Override
    public Class getJavaType() {
        return Void.TYPE;
    }

    @Override
    public Object getValueZero() {
        return null;
    }
}
