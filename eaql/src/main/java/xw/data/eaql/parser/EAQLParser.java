// Generated from EAQL.g4 by ANTLR 4.5.1

    package xw.data.eaql.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class EAQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, K_STRUCT=5, K_MAP=6, K_ARRAY=7, K_UNIONTYPE=8, 
		K_STRING=9, K_VARCHAR=10, K_CHAR=11, K_TINYINT=12, K_SMALLINT=13, K_INT=14, 
		K_BIGINT=15, K_FLOAT=16, K_DOUBLE=17, K_DECIMAL=18, K_TIMESTAMP=19, K_DATE=20, 
		K_BOOLEAN=21, K_BINARY=22, SCOL=23, DOT=24, OPEN_PAR=25, CLOSE_PAR=26, 
		COMMA=27, ASSIGN=28, STAR=29, PLUS=30, MINUS=31, TILDE=32, PIPE2=33, DIV=34, 
		MOD=35, AMP=36, PIPE=37, LT=38, LT_EQ=39, GT=40, GT_EQ=41, EQ=42, NOT_EQ1=43, 
		NOT_EQ2=44, K_ALL=45, K_AND=46, K_AS=47, K_ASC=48, K_BETWEEN=49, K_BY=50, 
		K_CASE=51, K_CAST=52, K_DESC=53, K_DESCRIBE=54, K_DISTINCT=55, K_ELSE=56, 
		K_END=57, K_FROM=58, K_GROUP=59, K_HAVING=60, K_IF=61, K_IN=62, K_INSERT=63, 
		K_INTO=64, K_IS=65, K_LIKE=66, K_LIMIT=67, K_NOT=68, K_NULL=69, K_OFFSET=70, 
		K_OR=71, K_ORDER=72, K_OVERWRITE=73, K_RLIKE=74, K_SELECT=75, K_TABLE=76, 
		K_THEN=77, K_UNION=78, K_VALUES=79, K_WHEN=80, K_WHERE=81, K_TRUE=82, 
		K_FALSE=83, IDENTIFIER=84, NUMERIC_LITERAL=85, STRING_LITERAL=86, BLOB_LITERAL=87, 
		SINGLE_LINE_COMMENT=88, MULTILINE_COMMENT=89, SPACES=90, UNEXPECTED_CHAR=91;
	public static final int
		RULE_parse = 0, RULE_error = 1, RULE_sql_stmt = 2, RULE_describe_table_stmt = 3, 
		RULE_select_stmt = 4, RULE_select_or_values = 5, RULE_values_stmt = 6, 
		RULE_expr_list = 7, RULE_simple_select_stmt = 8, RULE_select_clause = 9, 
		RULE_from_clause = 10, RULE_where_clause = 11, RULE_group_by_clause = 12, 
		RULE_order_by_clause = 13, RULE_limit_clause = 14, RULE_offset_clause = 15, 
		RULE_insert_stmt = 16, RULE_expr = 17, RULE_ordering_term = 18, RULE_result_column = 19, 
		RULE_compound_operator = 20, RULE_literal_value = 21, RULE_asterisk = 22, 
		RULE_function = 23, RULE_unary_operator = 24, RULE_parenthetical = 25, 
		RULE_error_message = 26, RULE_column_alias = 27, RULE_keyword = 28, RULE_data_type = 29, 
		RULE_struct_field = 30, RULE_primitive_type = 31, RULE_table_name = 32, 
		RULE_resource_descriptor = 33, RULE_function_name = 34, RULE_column_name = 35, 
		RULE_any_name = 36;
	public static final String[] ruleNames = {
		"parse", "error", "sql_stmt", "describe_table_stmt", "select_stmt", "select_or_values", 
		"values_stmt", "expr_list", "simple_select_stmt", "select_clause", "from_clause", 
		"where_clause", "group_by_clause", "order_by_clause", "limit_clause", 
		"offset_clause", "insert_stmt", "expr", "ordering_term", "result_column", 
		"compound_operator", "literal_value", "asterisk", "function", "unary_operator", 
		"parenthetical", "error_message", "column_alias", "keyword", "data_type", 
		"struct_field", "primitive_type", "table_name", "resource_descriptor", 
		"function_name", "column_name", "any_name"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'['", "']'", "'&&'", "':'", null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"';'", "'.'", "'('", "')'", "','", "'='", "'*'", "'+'", "'-'", "'~'", 
		"'||'", "'/'", "'%'", "'&'", "'|'", "'<'", "'<='", "'>'", "'>='", "'=='", 
		"'!='", "'<>'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, "K_STRUCT", "K_MAP", "K_ARRAY", "K_UNIONTYPE", 
		"K_STRING", "K_VARCHAR", "K_CHAR", "K_TINYINT", "K_SMALLINT", "K_INT", 
		"K_BIGINT", "K_FLOAT", "K_DOUBLE", "K_DECIMAL", "K_TIMESTAMP", "K_DATE", 
		"K_BOOLEAN", "K_BINARY", "SCOL", "DOT", "OPEN_PAR", "CLOSE_PAR", "COMMA", 
		"ASSIGN", "STAR", "PLUS", "MINUS", "TILDE", "PIPE2", "DIV", "MOD", "AMP", 
		"PIPE", "LT", "LT_EQ", "GT", "GT_EQ", "EQ", "NOT_EQ1", "NOT_EQ2", "K_ALL", 
		"K_AND", "K_AS", "K_ASC", "K_BETWEEN", "K_BY", "K_CASE", "K_CAST", "K_DESC", 
		"K_DESCRIBE", "K_DISTINCT", "K_ELSE", "K_END", "K_FROM", "K_GROUP", "K_HAVING", 
		"K_IF", "K_IN", "K_INSERT", "K_INTO", "K_IS", "K_LIKE", "K_LIMIT", "K_NOT", 
		"K_NULL", "K_OFFSET", "K_OR", "K_ORDER", "K_OVERWRITE", "K_RLIKE", "K_SELECT", 
		"K_TABLE", "K_THEN", "K_UNION", "K_VALUES", "K_WHEN", "K_WHERE", "K_TRUE", 
		"K_FALSE", "IDENTIFIER", "NUMERIC_LITERAL", "STRING_LITERAL", "BLOB_LITERAL", 
		"SINGLE_LINE_COMMENT", "MULTILINE_COMMENT", "SPACES", "UNEXPECTED_CHAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "EAQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public EAQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ParseContext extends ParserRuleContext {
		public List<Sql_stmtContext> sql_stmt() {
			return getRuleContexts(Sql_stmtContext.class);
		}
		public Sql_stmtContext sql_stmt(int i) {
			return getRuleContext(Sql_stmtContext.class,i);
		}
		public TerminalNode EOF() { return getToken(EAQLParser.EOF, 0); }
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL) {
				{
				{
				setState(74);
				match(SCOL);
				}
				}
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(80);
			sql_stmt();
			setState(89);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(82); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(81);
						match(SCOL);
						}
						}
						setState(84); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==SCOL );
					setState(86);
					sql_stmt();
					}
					} 
				}
				setState(91);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(95);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL) {
				{
				{
				setState(92);
				match(SCOL);
				}
				}
				setState(97);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(98);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ErrorContext extends ParserRuleContext {
		public Token UNEXPECTED_CHAR;
		public TerminalNode UNEXPECTED_CHAR() { return getToken(EAQLParser.UNEXPECTED_CHAR, 0); }
		public ErrorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterError(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitError(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitError(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ErrorContext error() throws RecognitionException {
		ErrorContext _localctx = new ErrorContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_error);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			((ErrorContext)_localctx).UNEXPECTED_CHAR = match(UNEXPECTED_CHAR);
			 
			     throw new RuntimeException("UNEXPECTED_CHAR=" + (((ErrorContext)_localctx).UNEXPECTED_CHAR!=null?((ErrorContext)_localctx).UNEXPECTED_CHAR.getText():null)); 
			   
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmtContext extends ParserRuleContext {
		public Describe_table_stmtContext describe_table_stmt() {
			return getRuleContext(Describe_table_stmtContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Insert_stmtContext insert_stmt() {
			return getRuleContext(Insert_stmtContext.class,0);
		}
		public Sql_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterSql_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitSql_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitSql_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmtContext sql_stmt() throws RecognitionException {
		Sql_stmtContext _localctx = new Sql_stmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_sql_stmt);
		try {
			setState(106);
			switch (_input.LA(1)) {
			case K_DESC:
			case K_DESCRIBE:
				enterOuterAlt(_localctx, 1);
				{
				setState(103);
				describe_table_stmt();
				}
				break;
			case K_SELECT:
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(104);
				select_stmt();
				}
				break;
			case K_INSERT:
				enterOuterAlt(_localctx, 3);
				{
				setState(105);
				insert_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Describe_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_TABLE() { return getToken(EAQLParser.K_TABLE, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode K_DESC() { return getToken(EAQLParser.K_DESC, 0); }
		public TerminalNode K_DESCRIBE() { return getToken(EAQLParser.K_DESCRIBE, 0); }
		public Resource_descriptorContext resource_descriptor() {
			return getRuleContext(Resource_descriptorContext.class,0);
		}
		public Describe_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_describe_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterDescribe_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitDescribe_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitDescribe_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Describe_table_stmtContext describe_table_stmt() throws RecognitionException {
		Describe_table_stmtContext _localctx = new Describe_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_describe_table_stmt);
		int _la;
		try {
			setState(113);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(108);
				_la = _input.LA(1);
				if ( !(_la==K_DESC || _la==K_DESCRIBE) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(109);
				match(K_TABLE);
				setState(110);
				table_name();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(111);
				_la = _input.LA(1);
				if ( !(_la==K_DESC || _la==K_DESCRIBE) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(112);
				resource_descriptor();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_stmtContext extends ParserRuleContext {
		public List<Select_or_valuesContext> select_or_values() {
			return getRuleContexts(Select_or_valuesContext.class);
		}
		public Select_or_valuesContext select_or_values(int i) {
			return getRuleContext(Select_or_valuesContext.class,i);
		}
		public List<Compound_operatorContext> compound_operator() {
			return getRuleContexts(Compound_operatorContext.class);
		}
		public Compound_operatorContext compound_operator(int i) {
			return getRuleContext(Compound_operatorContext.class,i);
		}
		public Order_by_clauseContext order_by_clause() {
			return getRuleContext(Order_by_clauseContext.class,0);
		}
		public Limit_clauseContext limit_clause() {
			return getRuleContext(Limit_clauseContext.class,0);
		}
		public Offset_clauseContext offset_clause() {
			return getRuleContext(Offset_clauseContext.class,0);
		}
		public Select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterSelect_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitSelect_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitSelect_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_stmtContext select_stmt() throws RecognitionException {
		Select_stmtContext _localctx = new Select_stmtContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_select_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			select_or_values();
			setState(121);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_UNION) {
				{
				{
				setState(116);
				compound_operator();
				setState(117);
				select_or_values();
				}
				}
				setState(123);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(125);
			_la = _input.LA(1);
			if (_la==K_ORDER) {
				{
				setState(124);
				order_by_clause();
				}
			}

			setState(128);
			_la = _input.LA(1);
			if (_la==K_LIMIT) {
				{
				setState(127);
				limit_clause();
				}
			}

			setState(131);
			_la = _input.LA(1);
			if (_la==K_OFFSET) {
				{
				setState(130);
				offset_clause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_or_valuesContext extends ParserRuleContext {
		public Simple_select_stmtContext simple_select_stmt() {
			return getRuleContext(Simple_select_stmtContext.class,0);
		}
		public Values_stmtContext values_stmt() {
			return getRuleContext(Values_stmtContext.class,0);
		}
		public Select_or_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_or_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterSelect_or_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitSelect_or_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitSelect_or_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_or_valuesContext select_or_values() throws RecognitionException {
		Select_or_valuesContext _localctx = new Select_or_valuesContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_select_or_values);
		try {
			setState(135);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(133);
				simple_select_stmt();
				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(134);
				values_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Values_stmtContext extends ParserRuleContext {
		public TerminalNode K_VALUES() { return getToken(EAQLParser.K_VALUES, 0); }
		public List<Expr_listContext> expr_list() {
			return getRuleContexts(Expr_listContext.class);
		}
		public Expr_listContext expr_list(int i) {
			return getRuleContext(Expr_listContext.class,i);
		}
		public Values_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_values_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterValues_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitValues_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitValues_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Values_stmtContext values_stmt() throws RecognitionException {
		Values_stmtContext _localctx = new Values_stmtContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_values_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			match(K_VALUES);
			setState(138);
			match(OPEN_PAR);
			setState(139);
			expr_list();
			setState(140);
			match(CLOSE_PAR);
			setState(148);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(141);
				match(COMMA);
				setState(142);
				match(OPEN_PAR);
				setState(143);
				expr_list();
				setState(144);
				match(CLOSE_PAR);
				}
				}
				setState(150);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_listContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Expr_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterExpr_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitExpr_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitExpr_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_listContext expr_list() throws RecognitionException {
		Expr_listContext _localctx = new Expr_listContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_expr_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(151);
			expr(0);
			setState(156);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(152);
				match(COMMA);
				setState(153);
				expr(0);
				}
				}
				setState(158);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Simple_select_stmtContext extends ParserRuleContext {
		public Select_clauseContext select_clause() {
			return getRuleContext(Select_clauseContext.class,0);
		}
		public From_clauseContext from_clause() {
			return getRuleContext(From_clauseContext.class,0);
		}
		public Where_clauseContext where_clause() {
			return getRuleContext(Where_clauseContext.class,0);
		}
		public Group_by_clauseContext group_by_clause() {
			return getRuleContext(Group_by_clauseContext.class,0);
		}
		public Simple_select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simple_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterSimple_select_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitSimple_select_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitSimple_select_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Simple_select_stmtContext simple_select_stmt() throws RecognitionException {
		Simple_select_stmtContext _localctx = new Simple_select_stmtContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_simple_select_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			select_clause();
			setState(161);
			_la = _input.LA(1);
			if (_la==K_FROM) {
				{
				setState(160);
				from_clause();
				}
			}

			setState(164);
			_la = _input.LA(1);
			if (_la==K_WHERE) {
				{
				setState(163);
				where_clause();
				}
			}

			setState(167);
			_la = _input.LA(1);
			if (_la==K_GROUP) {
				{
				setState(166);
				group_by_clause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_clauseContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(EAQLParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public TerminalNode K_DISTINCT() { return getToken(EAQLParser.K_DISTINCT, 0); }
		public Select_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterSelect_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitSelect_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitSelect_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_clauseContext select_clause() throws RecognitionException {
		Select_clauseContext _localctx = new Select_clauseContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_select_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169);
			match(K_SELECT);
			setState(171);
			_la = _input.LA(1);
			if (_la==K_DISTINCT) {
				{
				setState(170);
				match(K_DISTINCT);
				}
			}

			setState(173);
			result_column();
			setState(178);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(174);
				match(COMMA);
				setState(175);
				result_column();
				}
				}
				setState(180);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class From_clauseContext extends ParserRuleContext {
		public TerminalNode K_FROM() { return getToken(EAQLParser.K_FROM, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Resource_descriptorContext resource_descriptor() {
			return getRuleContext(Resource_descriptorContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public From_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_from_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterFrom_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitFrom_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitFrom_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final From_clauseContext from_clause() throws RecognitionException {
		From_clauseContext _localctx = new From_clauseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_from_clause);
		try {
			setState(190);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(181);
				match(K_FROM);
				setState(182);
				table_name();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(183);
				match(K_FROM);
				setState(184);
				resource_descriptor();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(185);
				match(K_FROM);
				setState(186);
				match(OPEN_PAR);
				setState(187);
				select_stmt();
				setState(188);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Where_clauseContext extends ParserRuleContext {
		public TerminalNode K_WHERE() { return getToken(EAQLParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Where_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_where_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterWhere_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitWhere_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitWhere_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Where_clauseContext where_clause() throws RecognitionException {
		Where_clauseContext _localctx = new Where_clauseContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_where_clause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(K_WHERE);
			setState(193);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Group_by_clauseContext extends ParserRuleContext {
		public TerminalNode K_GROUP() { return getToken(EAQLParser.K_GROUP, 0); }
		public TerminalNode K_BY() { return getToken(EAQLParser.K_BY, 0); }
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public Group_by_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_group_by_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterGroup_by_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitGroup_by_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitGroup_by_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Group_by_clauseContext group_by_clause() throws RecognitionException {
		Group_by_clauseContext _localctx = new Group_by_clauseContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_group_by_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(K_GROUP);
			setState(196);
			match(K_BY);
			setState(197);
			column_name();
			setState(202);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(198);
				match(COMMA);
				setState(199);
				column_name();
				}
				}
				setState(204);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Order_by_clauseContext extends ParserRuleContext {
		public TerminalNode K_ORDER() { return getToken(EAQLParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(EAQLParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public Order_by_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_order_by_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterOrder_by_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitOrder_by_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitOrder_by_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Order_by_clauseContext order_by_clause() throws RecognitionException {
		Order_by_clauseContext _localctx = new Order_by_clauseContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_order_by_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205);
			match(K_ORDER);
			setState(206);
			match(K_BY);
			setState(207);
			ordering_term();
			setState(212);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(208);
				match(COMMA);
				setState(209);
				ordering_term();
				}
				}
				setState(214);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Limit_clauseContext extends ParserRuleContext {
		public TerminalNode K_LIMIT() { return getToken(EAQLParser.K_LIMIT, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(EAQLParser.NUMERIC_LITERAL, 0); }
		public Limit_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limit_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterLimit_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitLimit_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitLimit_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Limit_clauseContext limit_clause() throws RecognitionException {
		Limit_clauseContext _localctx = new Limit_clauseContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_limit_clause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(215);
			match(K_LIMIT);
			setState(216);
			match(NUMERIC_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Offset_clauseContext extends ParserRuleContext {
		public TerminalNode K_OFFSET() { return getToken(EAQLParser.K_OFFSET, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(EAQLParser.NUMERIC_LITERAL, 0); }
		public Offset_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_offset_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterOffset_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitOffset_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitOffset_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Offset_clauseContext offset_clause() throws RecognitionException {
		Offset_clauseContext _localctx = new Offset_clauseContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_offset_clause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218);
			match(K_OFFSET);
			setState(219);
			match(NUMERIC_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Insert_stmtContext extends ParserRuleContext {
		public TerminalNode K_INSERT() { return getToken(EAQLParser.K_INSERT, 0); }
		public Resource_descriptorContext resource_descriptor() {
			return getRuleContext(Resource_descriptorContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode K_OVERWRITE() { return getToken(EAQLParser.K_OVERWRITE, 0); }
		public TerminalNode K_INTO() { return getToken(EAQLParser.K_INTO, 0); }
		public Insert_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insert_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterInsert_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitInsert_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitInsert_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Insert_stmtContext insert_stmt() throws RecognitionException {
		Insert_stmtContext _localctx = new Insert_stmtContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_insert_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(221);
			match(K_INSERT);
			setState(222);
			_la = _input.LA(1);
			if ( !(_la==K_INTO || _la==K_OVERWRITE) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(223);
			resource_descriptor();
			setState(224);
			select_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode K_NOT() { return getToken(EAQLParser.K_NOT, 0); }
		public AsteriskContext asterisk() {
			return getRuleContext(AsteriskContext.class,0);
		}
		public List<Literal_valueContext> literal_value() {
			return getRuleContexts(Literal_valueContext.class);
		}
		public Literal_valueContext literal_value(int i) {
			return getRuleContext(Literal_valueContext.class,i);
		}
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public ParentheticalContext parenthetical() {
			return getRuleContext(ParentheticalContext.class,0);
		}
		public TerminalNode K_CAST() { return getToken(EAQLParser.K_CAST, 0); }
		public TerminalNode K_AS() { return getToken(EAQLParser.K_AS, 0); }
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public TerminalNode K_CASE() { return getToken(EAQLParser.K_CASE, 0); }
		public TerminalNode K_END() { return getToken(EAQLParser.K_END, 0); }
		public List<TerminalNode> K_WHEN() { return getTokens(EAQLParser.K_WHEN); }
		public TerminalNode K_WHEN(int i) {
			return getToken(EAQLParser.K_WHEN, i);
		}
		public List<TerminalNode> K_THEN() { return getTokens(EAQLParser.K_THEN); }
		public TerminalNode K_THEN(int i) {
			return getToken(EAQLParser.K_THEN, i);
		}
		public TerminalNode K_ELSE() { return getToken(EAQLParser.K_ELSE, 0); }
		public TerminalNode K_AND() { return getToken(EAQLParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(EAQLParser.K_OR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(EAQLParser.IDENTIFIER, 0); }
		public TerminalNode K_LIKE() { return getToken(EAQLParser.K_LIKE, 0); }
		public TerminalNode K_RLIKE() { return getToken(EAQLParser.K_RLIKE, 0); }
		public TerminalNode K_IS() { return getToken(EAQLParser.K_IS, 0); }
		public TerminalNode K_NULL() { return getToken(EAQLParser.K_NULL, 0); }
		public TerminalNode K_IN() { return getToken(EAQLParser.K_IN, 0); }
		public TerminalNode K_BETWEEN() { return getToken(EAQLParser.K_BETWEEN, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(263);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(227);
				unary_operator();
				setState(228);
				expr(16);
				}
				break;
			case 2:
				{
				setState(230);
				match(K_NOT);
				setState(231);
				expr(6);
				}
				break;
			case 3:
				{
				setState(232);
				asterisk();
				}
				break;
			case 4:
				{
				setState(233);
				literal_value();
				}
				break;
			case 5:
				{
				setState(234);
				column_name();
				}
				break;
			case 6:
				{
				setState(235);
				function();
				}
				break;
			case 7:
				{
				setState(236);
				parenthetical();
				}
				break;
			case 8:
				{
				setState(237);
				match(K_CAST);
				setState(238);
				match(OPEN_PAR);
				setState(239);
				expr(0);
				setState(240);
				match(K_AS);
				setState(241);
				data_type();
				setState(242);
				match(CLOSE_PAR);
				}
				break;
			case 9:
				{
				setState(244);
				match(K_CASE);
				setState(246);
				switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
				case 1:
					{
					setState(245);
					expr(0);
					}
					break;
				}
				setState(253); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(248);
					match(K_WHEN);
					setState(249);
					expr(0);
					setState(250);
					match(K_THEN);
					setState(251);
					expr(0);
					}
					}
					setState(255); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==K_WHEN );
				setState(259);
				_la = _input.LA(1);
				if (_la==K_ELSE) {
					{
					setState(257);
					match(K_ELSE);
					setState(258);
					expr(0);
					}
				}

				setState(261);
				match(K_END);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(336);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(334);
					switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(265);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(266);
						_la = _input.LA(1);
						if ( !(_la==STAR || _la==DIV) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(267);
						expr(16);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(268);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(269);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(270);
						expr(15);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(271);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(272);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(273);
						expr(14);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(274);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(275);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << EQ) | (1L << NOT_EQ1) | (1L << NOT_EQ2))) != 0)) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(276);
						expr(13);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(277);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(278);
						_la = _input.LA(1);
						if ( !(_la==T__2 || _la==K_AND) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(279);
						expr(6);
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(280);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(281);
						_la = _input.LA(1);
						if ( !(_la==PIPE2 || _la==K_OR) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(282);
						expr(5);
						}
						break;
					case 7:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(283);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(284);
						match(T__0);
						setState(285);
						expr(0);
						setState(286);
						match(T__1);
						}
						break;
					case 8:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(288);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(289);
						match(DOT);
						setState(290);
						match(IDENTIFIER);
						}
						break;
					case 9:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(291);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(293);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(292);
							match(K_NOT);
							}
						}

						setState(295);
						match(K_LIKE);
						setState(296);
						literal_value();
						}
						break;
					case 10:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(297);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(299);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(298);
							match(K_NOT);
							}
						}

						setState(301);
						match(K_RLIKE);
						setState(302);
						literal_value();
						}
						break;
					case 11:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(303);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(304);
						match(K_IS);
						setState(306);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(305);
							match(K_NOT);
							}
						}

						setState(308);
						match(K_NULL);
						}
						break;
					case 12:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(309);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(311);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(310);
							match(K_NOT);
							}
						}

						setState(313);
						match(K_IN);
						{
						setState(314);
						match(OPEN_PAR);
						setState(315);
						literal_value();
						setState(320);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(316);
							match(COMMA);
							setState(317);
							literal_value();
							}
							}
							setState(322);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(323);
						match(CLOSE_PAR);
						}
						}
						break;
					case 13:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(325);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(327);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(326);
							match(K_NOT);
							}
						}

						setState(329);
						match(K_BETWEEN);
						setState(330);
						literal_value();
						setState(331);
						match(K_AND);
						setState(332);
						literal_value();
						}
						break;
					}
					} 
				}
				setState(338);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Ordering_termContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(EAQLParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(EAQLParser.K_DESC, 0); }
		public Ordering_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordering_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterOrdering_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitOrdering_term(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitOrdering_term(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ordering_termContext ordering_term() throws RecognitionException {
		Ordering_termContext _localctx = new Ordering_termContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_ordering_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			expr(0);
			setState(341);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(340);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Result_columnContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_aliasContext column_alias() {
			return getRuleContext(Column_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(EAQLParser.K_AS, 0); }
		public Result_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_result_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterResult_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitResult_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitResult_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Result_columnContext result_column() throws RecognitionException {
		Result_columnContext _localctx = new Result_columnContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_result_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(343);
			expr(0);
			setState(348);
			_la = _input.LA(1);
			if (((((_la - 47)) & ~0x3f) == 0 && ((1L << (_la - 47)) & ((1L << (K_AS - 47)) | (1L << (IDENTIFIER - 47)) | (1L << (STRING_LITERAL - 47)))) != 0)) {
				{
				setState(345);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(344);
					match(K_AS);
					}
				}

				setState(347);
				column_alias();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compound_operatorContext extends ParserRuleContext {
		public TerminalNode K_UNION() { return getToken(EAQLParser.K_UNION, 0); }
		public TerminalNode K_ALL() { return getToken(EAQLParser.K_ALL, 0); }
		public Compound_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compound_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterCompound_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitCompound_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitCompound_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Compound_operatorContext compound_operator() throws RecognitionException {
		Compound_operatorContext _localctx = new Compound_operatorContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_compound_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(350);
			match(K_UNION);
			setState(352);
			_la = _input.LA(1);
			if (_la==K_ALL) {
				{
				setState(351);
				match(K_ALL);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_valueContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(EAQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(EAQLParser.STRING_LITERAL, 0); }
		public TerminalNode K_TRUE() { return getToken(EAQLParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(EAQLParser.K_FALSE, 0); }
		public TerminalNode K_NULL() { return getToken(EAQLParser.K_NULL, 0); }
		public Literal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterLiteral_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitLiteral_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitLiteral_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Literal_valueContext literal_value() throws RecognitionException {
		Literal_valueContext _localctx = new Literal_valueContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_literal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(354);
			_la = _input.LA(1);
			if ( !(((((_la - 69)) & ~0x3f) == 0 && ((1L << (_la - 69)) & ((1L << (K_NULL - 69)) | (1L << (K_TRUE - 69)) | (1L << (K_FALSE - 69)) | (1L << (NUMERIC_LITERAL - 69)) | (1L << (STRING_LITERAL - 69)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsteriskContext extends ParserRuleContext {
		public AsteriskContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asterisk; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterAsterisk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitAsterisk(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitAsterisk(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsteriskContext asterisk() throws RecognitionException {
		AsteriskContext _localctx = new AsteriskContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_asterisk);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(356);
			match(STAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_function);
		int _la;
		try {
			setState(374);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(358);
				function_name();
				setState(359);
				match(OPEN_PAR);
				setState(360);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(362);
				function_name();
				setState(363);
				match(OPEN_PAR);
				setState(364);
				expr(0);
				setState(369);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(365);
					match(COMMA);
					setState(366);
					expr(0);
					}
					}
					setState(371);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(372);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitUnary_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitUnary_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_unary_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(376);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParentheticalContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParentheticalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parenthetical; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterParenthetical(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitParenthetical(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitParenthetical(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParentheticalContext parenthetical() throws RecognitionException {
		ParentheticalContext _localctx = new ParentheticalContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_parenthetical);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(378);
			match(OPEN_PAR);
			setState(379);
			expr(0);
			setState(380);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Error_messageContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(EAQLParser.STRING_LITERAL, 0); }
		public Error_messageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error_message; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterError_message(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitError_message(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitError_message(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Error_messageContext error_message() throws RecognitionException {
		Error_messageContext _localctx = new Error_messageContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_error_message);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(382);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_aliasContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(EAQLParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(EAQLParser.STRING_LITERAL, 0); }
		public Column_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterColumn_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitColumn_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitColumn_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_aliasContext column_alias() throws RecognitionException {
		Column_aliasContext _localctx = new Column_aliasContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_column_alias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(384);
			_la = _input.LA(1);
			if ( !(_la==IDENTIFIER || _la==STRING_LITERAL) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public TerminalNode K_AND() { return getToken(EAQLParser.K_AND, 0); }
		public TerminalNode K_AS() { return getToken(EAQLParser.K_AS, 0); }
		public TerminalNode K_ASC() { return getToken(EAQLParser.K_ASC, 0); }
		public TerminalNode K_BETWEEN() { return getToken(EAQLParser.K_BETWEEN, 0); }
		public TerminalNode K_BY() { return getToken(EAQLParser.K_BY, 0); }
		public TerminalNode K_CASE() { return getToken(EAQLParser.K_CASE, 0); }
		public TerminalNode K_CAST() { return getToken(EAQLParser.K_CAST, 0); }
		public TerminalNode K_DESC() { return getToken(EAQLParser.K_DESC, 0); }
		public TerminalNode K_DESCRIBE() { return getToken(EAQLParser.K_DESCRIBE, 0); }
		public TerminalNode K_ELSE() { return getToken(EAQLParser.K_ELSE, 0); }
		public TerminalNode K_END() { return getToken(EAQLParser.K_END, 0); }
		public TerminalNode K_FROM() { return getToken(EAQLParser.K_FROM, 0); }
		public TerminalNode K_GROUP() { return getToken(EAQLParser.K_GROUP, 0); }
		public TerminalNode K_HAVING() { return getToken(EAQLParser.K_HAVING, 0); }
		public TerminalNode K_IF() { return getToken(EAQLParser.K_IF, 0); }
		public TerminalNode K_IN() { return getToken(EAQLParser.K_IN, 0); }
		public TerminalNode K_INSERT() { return getToken(EAQLParser.K_INSERT, 0); }
		public TerminalNode K_INTO() { return getToken(EAQLParser.K_INTO, 0); }
		public TerminalNode K_IS() { return getToken(EAQLParser.K_IS, 0); }
		public TerminalNode K_LIKE() { return getToken(EAQLParser.K_LIKE, 0); }
		public TerminalNode K_LIMIT() { return getToken(EAQLParser.K_LIMIT, 0); }
		public TerminalNode K_NOT() { return getToken(EAQLParser.K_NOT, 0); }
		public TerminalNode K_NULL() { return getToken(EAQLParser.K_NULL, 0); }
		public TerminalNode K_OFFSET() { return getToken(EAQLParser.K_OFFSET, 0); }
		public TerminalNode K_OR() { return getToken(EAQLParser.K_OR, 0); }
		public TerminalNode K_ORDER() { return getToken(EAQLParser.K_ORDER, 0); }
		public TerminalNode K_OVERWRITE() { return getToken(EAQLParser.K_OVERWRITE, 0); }
		public TerminalNode K_RLIKE() { return getToken(EAQLParser.K_RLIKE, 0); }
		public TerminalNode K_SELECT() { return getToken(EAQLParser.K_SELECT, 0); }
		public TerminalNode K_TABLE() { return getToken(EAQLParser.K_TABLE, 0); }
		public TerminalNode K_THEN() { return getToken(EAQLParser.K_THEN, 0); }
		public TerminalNode K_UNION() { return getToken(EAQLParser.K_UNION, 0); }
		public TerminalNode K_WHEN() { return getToken(EAQLParser.K_WHEN, 0); }
		public TerminalNode K_WHERE() { return getToken(EAQLParser.K_WHERE, 0); }
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterKeyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitKeyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(386);
			_la = _input.LA(1);
			if ( !(((((_la - 46)) & ~0x3f) == 0 && ((1L << (_la - 46)) & ((1L << (K_AND - 46)) | (1L << (K_AS - 46)) | (1L << (K_ASC - 46)) | (1L << (K_BETWEEN - 46)) | (1L << (K_BY - 46)) | (1L << (K_CASE - 46)) | (1L << (K_CAST - 46)) | (1L << (K_DESC - 46)) | (1L << (K_DESCRIBE - 46)) | (1L << (K_ELSE - 46)) | (1L << (K_END - 46)) | (1L << (K_FROM - 46)) | (1L << (K_GROUP - 46)) | (1L << (K_HAVING - 46)) | (1L << (K_IF - 46)) | (1L << (K_IN - 46)) | (1L << (K_INSERT - 46)) | (1L << (K_INTO - 46)) | (1L << (K_IS - 46)) | (1L << (K_LIKE - 46)) | (1L << (K_LIMIT - 46)) | (1L << (K_NOT - 46)) | (1L << (K_NULL - 46)) | (1L << (K_OFFSET - 46)) | (1L << (K_OR - 46)) | (1L << (K_ORDER - 46)) | (1L << (K_OVERWRITE - 46)) | (1L << (K_RLIKE - 46)) | (1L << (K_SELECT - 46)) | (1L << (K_TABLE - 46)) | (1L << (K_THEN - 46)) | (1L << (K_UNION - 46)) | (1L << (K_WHEN - 46)) | (1L << (K_WHERE - 46)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Data_typeContext extends ParserRuleContext {
		public Primitive_typeContext primitive_type() {
			return getRuleContext(Primitive_typeContext.class,0);
		}
		public TerminalNode K_STRUCT() { return getToken(EAQLParser.K_STRUCT, 0); }
		public List<Struct_fieldContext> struct_field() {
			return getRuleContexts(Struct_fieldContext.class);
		}
		public Struct_fieldContext struct_field(int i) {
			return getRuleContext(Struct_fieldContext.class,i);
		}
		public TerminalNode K_MAP() { return getToken(EAQLParser.K_MAP, 0); }
		public List<Data_typeContext> data_type() {
			return getRuleContexts(Data_typeContext.class);
		}
		public Data_typeContext data_type(int i) {
			return getRuleContext(Data_typeContext.class,i);
		}
		public TerminalNode K_ARRAY() { return getToken(EAQLParser.K_ARRAY, 0); }
		public TerminalNode K_UNIONTYPE() { return getToken(EAQLParser.K_UNIONTYPE, 0); }
		public Data_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_data_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterData_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitData_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitData_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Data_typeContext data_type() throws RecognitionException {
		Data_typeContext _localctx = new Data_typeContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_data_type);
		int _la;
		try {
			setState(425);
			switch (_input.LA(1)) {
			case K_STRING:
			case K_VARCHAR:
			case K_CHAR:
			case K_TINYINT:
			case K_SMALLINT:
			case K_INT:
			case K_BIGINT:
			case K_FLOAT:
			case K_DOUBLE:
			case K_DECIMAL:
			case K_TIMESTAMP:
			case K_DATE:
			case K_BOOLEAN:
			case K_BINARY:
				enterOuterAlt(_localctx, 1);
				{
				setState(388);
				primitive_type();
				}
				break;
			case K_STRUCT:
				enterOuterAlt(_localctx, 2);
				{
				setState(389);
				match(K_STRUCT);
				setState(390);
				match(LT);
				setState(391);
				struct_field();
				setState(396);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(392);
					match(COMMA);
					setState(393);
					struct_field();
					}
					}
					setState(398);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(399);
				match(GT);
				}
				break;
			case K_MAP:
				enterOuterAlt(_localctx, 3);
				{
				setState(401);
				match(K_MAP);
				setState(402);
				match(LT);
				setState(403);
				primitive_type();
				setState(404);
				match(COMMA);
				setState(405);
				data_type();
				setState(406);
				match(GT);
				}
				break;
			case K_ARRAY:
				enterOuterAlt(_localctx, 4);
				{
				setState(408);
				match(K_ARRAY);
				setState(409);
				match(LT);
				setState(410);
				data_type();
				setState(411);
				match(GT);
				}
				break;
			case K_UNIONTYPE:
				enterOuterAlt(_localctx, 5);
				{
				setState(413);
				match(K_UNIONTYPE);
				setState(414);
				match(LT);
				setState(415);
				data_type();
				setState(420);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(416);
					match(COMMA);
					setState(417);
					data_type();
					}
					}
					setState(422);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(423);
				match(GT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_fieldContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Struct_fieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_field; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterStruct_field(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitStruct_field(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitStruct_field(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Struct_fieldContext struct_field() throws RecognitionException {
		Struct_fieldContext _localctx = new Struct_fieldContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_struct_field);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(427);
			column_name();
			setState(430);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(428);
				match(T__3);
				setState(429);
				data_type();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Primitive_typeContext extends ParserRuleContext {
		public TerminalNode K_STRING() { return getToken(EAQLParser.K_STRING, 0); }
		public TerminalNode K_VARCHAR() { return getToken(EAQLParser.K_VARCHAR, 0); }
		public List<TerminalNode> NUMERIC_LITERAL() { return getTokens(EAQLParser.NUMERIC_LITERAL); }
		public TerminalNode NUMERIC_LITERAL(int i) {
			return getToken(EAQLParser.NUMERIC_LITERAL, i);
		}
		public TerminalNode K_CHAR() { return getToken(EAQLParser.K_CHAR, 0); }
		public TerminalNode K_TINYINT() { return getToken(EAQLParser.K_TINYINT, 0); }
		public TerminalNode K_SMALLINT() { return getToken(EAQLParser.K_SMALLINT, 0); }
		public TerminalNode K_INT() { return getToken(EAQLParser.K_INT, 0); }
		public TerminalNode K_BIGINT() { return getToken(EAQLParser.K_BIGINT, 0); }
		public TerminalNode K_FLOAT() { return getToken(EAQLParser.K_FLOAT, 0); }
		public TerminalNode K_DOUBLE() { return getToken(EAQLParser.K_DOUBLE, 0); }
		public TerminalNode K_DECIMAL() { return getToken(EAQLParser.K_DECIMAL, 0); }
		public TerminalNode K_TIMESTAMP() { return getToken(EAQLParser.K_TIMESTAMP, 0); }
		public TerminalNode K_DATE() { return getToken(EAQLParser.K_DATE, 0); }
		public TerminalNode K_BOOLEAN() { return getToken(EAQLParser.K_BOOLEAN, 0); }
		public TerminalNode K_BINARY() { return getToken(EAQLParser.K_BINARY, 0); }
		public Primitive_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitive_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterPrimitive_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitPrimitive_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitPrimitive_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Primitive_typeContext primitive_type() throws RecognitionException {
		Primitive_typeContext _localctx = new Primitive_typeContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_primitive_type);
		try {
			setState(463);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(432);
				match(K_STRING);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(433);
				match(K_VARCHAR);
				setState(434);
				match(OPEN_PAR);
				setState(435);
				match(NUMERIC_LITERAL);
				setState(436);
				match(CLOSE_PAR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(437);
				match(K_CHAR);
				setState(438);
				match(OPEN_PAR);
				setState(439);
				match(NUMERIC_LITERAL);
				setState(440);
				match(CLOSE_PAR);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(441);
				match(K_TINYINT);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(442);
				match(K_SMALLINT);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(443);
				match(K_INT);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(444);
				match(K_BIGINT);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(445);
				match(K_FLOAT);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(446);
				match(K_DOUBLE);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(447);
				match(K_DECIMAL);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(448);
				match(K_DECIMAL);
				setState(449);
				match(OPEN_PAR);
				setState(450);
				match(NUMERIC_LITERAL);
				setState(451);
				match(CLOSE_PAR);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(452);
				match(K_DECIMAL);
				setState(453);
				match(OPEN_PAR);
				setState(454);
				match(NUMERIC_LITERAL);
				setState(455);
				match(COMMA);
				setState(456);
				match(NUMERIC_LITERAL);
				setState(457);
				match(CLOSE_PAR);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(458);
				match(K_DECIMAL);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(459);
				match(K_TIMESTAMP);
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(460);
				match(K_DATE);
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(461);
				match(K_BOOLEAN);
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(462);
				match(K_BINARY);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_nameContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(EAQLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(EAQLParser.IDENTIFIER, i);
		}
		public Table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterTable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitTable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitTable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_nameContext table_name() throws RecognitionException {
		Table_nameContext _localctx = new Table_nameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_table_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(465);
			match(IDENTIFIER);
			setState(468);
			_la = _input.LA(1);
			if (_la==DOT) {
				{
				setState(466);
				match(DOT);
				setState(467);
				match(IDENTIFIER);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Resource_descriptorContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(EAQLParser.STRING_LITERAL, 0); }
		public Resource_descriptorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resource_descriptor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterResource_descriptor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitResource_descriptor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitResource_descriptor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Resource_descriptorContext resource_descriptor() throws RecognitionException {
		Resource_descriptorContext _localctx = new Resource_descriptorContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_resource_descriptor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(470);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterFunction_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitFunction_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitFunction_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_nameContext function_name() throws RecognitionException {
		Function_nameContext _localctx = new Function_nameContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_function_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(472);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterColumn_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitColumn_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitColumn_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_nameContext column_name() throws RecognitionException {
		Column_nameContext _localctx = new Column_nameContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(474);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Any_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(EAQLParser.IDENTIFIER, 0); }
		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class,0);
		}
		public TerminalNode STRING_LITERAL() { return getToken(EAQLParser.STRING_LITERAL, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Any_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_any_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).enterAny_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EAQLListener ) ((EAQLListener)listener).exitAny_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EAQLVisitor ) return ((EAQLVisitor<? extends T>)visitor).visitAny_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Any_nameContext any_name() throws RecognitionException {
		Any_nameContext _localctx = new Any_nameContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_any_name);
		try {
			setState(483);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(476);
				match(IDENTIFIER);
				}
				break;
			case K_AND:
			case K_AS:
			case K_ASC:
			case K_BETWEEN:
			case K_BY:
			case K_CASE:
			case K_CAST:
			case K_DESC:
			case K_DESCRIBE:
			case K_ELSE:
			case K_END:
			case K_FROM:
			case K_GROUP:
			case K_HAVING:
			case K_IF:
			case K_IN:
			case K_INSERT:
			case K_INTO:
			case K_IS:
			case K_LIKE:
			case K_LIMIT:
			case K_NOT:
			case K_NULL:
			case K_OFFSET:
			case K_OR:
			case K_ORDER:
			case K_OVERWRITE:
			case K_RLIKE:
			case K_SELECT:
			case K_TABLE:
			case K_THEN:
			case K_UNION:
			case K_WHEN:
			case K_WHERE:
				enterOuterAlt(_localctx, 2);
				{
				setState(477);
				keyword();
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(478);
				match(STRING_LITERAL);
				}
				break;
			case OPEN_PAR:
				enterOuterAlt(_localctx, 4);
				{
				setState(479);
				match(OPEN_PAR);
				setState(480);
				any_name();
				setState(481);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 17:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 15);
		case 1:
			return precpred(_ctx, 14);
		case 2:
			return precpred(_ctx, 13);
		case 3:
			return precpred(_ctx, 12);
		case 4:
			return precpred(_ctx, 5);
		case 5:
			return precpred(_ctx, 4);
		case 6:
			return precpred(_ctx, 18);
		case 7:
			return precpred(_ctx, 17);
		case 8:
			return precpred(_ctx, 11);
		case 9:
			return precpred(_ctx, 10);
		case 10:
			return precpred(_ctx, 9);
		case 11:
			return precpred(_ctx, 8);
		case 12:
			return precpred(_ctx, 7);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3]\u01e8\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\3\2\7\2N\n\2\f\2\16\2Q\13\2\3\2\3"+
		"\2\6\2U\n\2\r\2\16\2V\3\2\7\2Z\n\2\f\2\16\2]\13\2\3\2\7\2`\n\2\f\2\16"+
		"\2c\13\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\5\4m\n\4\3\5\3\5\3\5\3\5\3\5"+
		"\5\5t\n\5\3\6\3\6\3\6\3\6\7\6z\n\6\f\6\16\6}\13\6\3\6\5\6\u0080\n\6\3"+
		"\6\5\6\u0083\n\6\3\6\5\6\u0086\n\6\3\7\3\7\5\7\u008a\n\7\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\7\b\u0095\n\b\f\b\16\b\u0098\13\b\3\t\3\t\3\t\7"+
		"\t\u009d\n\t\f\t\16\t\u00a0\13\t\3\n\3\n\5\n\u00a4\n\n\3\n\5\n\u00a7\n"+
		"\n\3\n\5\n\u00aa\n\n\3\13\3\13\5\13\u00ae\n\13\3\13\3\13\3\13\7\13\u00b3"+
		"\n\13\f\13\16\13\u00b6\13\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00c1"+
		"\n\f\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\7\16\u00cb\n\16\f\16\16\16\u00ce"+
		"\13\16\3\17\3\17\3\17\3\17\3\17\7\17\u00d5\n\17\f\17\16\17\u00d8\13\17"+
		"\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\5\23\u00f9\n\23\3\23\3\23\3\23\3\23\3\23\6\23\u0100\n"+
		"\23\r\23\16\23\u0101\3\23\3\23\5\23\u0106\n\23\3\23\3\23\5\23\u010a\n"+
		"\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3"+
		"\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3"+
		"\23\5\23\u0128\n\23\3\23\3\23\3\23\3\23\5\23\u012e\n\23\3\23\3\23\3\23"+
		"\3\23\3\23\5\23\u0135\n\23\3\23\3\23\3\23\5\23\u013a\n\23\3\23\3\23\3"+
		"\23\3\23\3\23\7\23\u0141\n\23\f\23\16\23\u0144\13\23\3\23\3\23\3\23\3"+
		"\23\5\23\u014a\n\23\3\23\3\23\3\23\3\23\3\23\7\23\u0151\n\23\f\23\16\23"+
		"\u0154\13\23\3\24\3\24\5\24\u0158\n\24\3\25\3\25\5\25\u015c\n\25\3\25"+
		"\5\25\u015f\n\25\3\26\3\26\5\26\u0163\n\26\3\27\3\27\3\30\3\30\3\31\3"+
		"\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u0172\n\31\f\31\16\31\u0175"+
		"\13\31\3\31\3\31\5\31\u0179\n\31\3\32\3\32\3\33\3\33\3\33\3\33\3\34\3"+
		"\34\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\7\37\u018d\n\37"+
		"\f\37\16\37\u0190\13\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\7\37\u01a5\n\37\f\37"+
		"\16\37\u01a8\13\37\3\37\3\37\5\37\u01ac\n\37\3 \3 \3 \5 \u01b1\n \3!\3"+
		"!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3"+
		"!\3!\3!\3!\3!\3!\3!\5!\u01d2\n!\3\"\3\"\3\"\5\"\u01d7\n\"\3#\3#\3$\3$"+
		"\3%\3%\3&\3&\3&\3&\3&\3&\3&\5&\u01e6\n&\3&\2\3$\'\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJ\2\16\3\2\678\4\2BBK"+
		"K\4\2\37\37$$\3\2 !\3\2(+\4\2\36\36,.\4\2\5\5\60\60\4\2##II\4\2\62\62"+
		"\67\67\5\2GGTUWX\4\2VVXX\5\2\608:PRS\u0218\2O\3\2\2\2\4f\3\2\2\2\6l\3"+
		"\2\2\2\bs\3\2\2\2\nu\3\2\2\2\f\u0089\3\2\2\2\16\u008b\3\2\2\2\20\u0099"+
		"\3\2\2\2\22\u00a1\3\2\2\2\24\u00ab\3\2\2\2\26\u00c0\3\2\2\2\30\u00c2\3"+
		"\2\2\2\32\u00c5\3\2\2\2\34\u00cf\3\2\2\2\36\u00d9\3\2\2\2 \u00dc\3\2\2"+
		"\2\"\u00df\3\2\2\2$\u0109\3\2\2\2&\u0155\3\2\2\2(\u0159\3\2\2\2*\u0160"+
		"\3\2\2\2,\u0164\3\2\2\2.\u0166\3\2\2\2\60\u0178\3\2\2\2\62\u017a\3\2\2"+
		"\2\64\u017c\3\2\2\2\66\u0180\3\2\2\28\u0182\3\2\2\2:\u0184\3\2\2\2<\u01ab"+
		"\3\2\2\2>\u01ad\3\2\2\2@\u01d1\3\2\2\2B\u01d3\3\2\2\2D\u01d8\3\2\2\2F"+
		"\u01da\3\2\2\2H\u01dc\3\2\2\2J\u01e5\3\2\2\2LN\7\31\2\2ML\3\2\2\2NQ\3"+
		"\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2\2R[\5\6\4\2SU\7\31\2\2TS"+
		"\3\2\2\2UV\3\2\2\2VT\3\2\2\2VW\3\2\2\2WX\3\2\2\2XZ\5\6\4\2YT\3\2\2\2Z"+
		"]\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\a\3\2\2\2][\3\2\2\2^`\7\31\2\2_^\3\2\2"+
		"\2`c\3\2\2\2a_\3\2\2\2ab\3\2\2\2bd\3\2\2\2ca\3\2\2\2de\7\2\2\3e\3\3\2"+
		"\2\2fg\7]\2\2gh\b\3\1\2h\5\3\2\2\2im\5\b\5\2jm\5\n\6\2km\5\"\22\2li\3"+
		"\2\2\2lj\3\2\2\2lk\3\2\2\2m\7\3\2\2\2no\t\2\2\2op\7N\2\2pt\5B\"\2qr\t"+
		"\2\2\2rt\5D#\2sn\3\2\2\2sq\3\2\2\2t\t\3\2\2\2u{\5\f\7\2vw\5*\26\2wx\5"+
		"\f\7\2xz\3\2\2\2yv\3\2\2\2z}\3\2\2\2{y\3\2\2\2{|\3\2\2\2|\177\3\2\2\2"+
		"}{\3\2\2\2~\u0080\5\34\17\2\177~\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082"+
		"\3\2\2\2\u0081\u0083\5\36\20\2\u0082\u0081\3\2\2\2\u0082\u0083\3\2\2\2"+
		"\u0083\u0085\3\2\2\2\u0084\u0086\5 \21\2\u0085\u0084\3\2\2\2\u0085\u0086"+
		"\3\2\2\2\u0086\13\3\2\2\2\u0087\u008a\5\22\n\2\u0088\u008a\5\16\b\2\u0089"+
		"\u0087\3\2\2\2\u0089\u0088\3\2\2\2\u008a\r\3\2\2\2\u008b\u008c\7Q\2\2"+
		"\u008c\u008d\7\33\2\2\u008d\u008e\5\20\t\2\u008e\u0096\7\34\2\2\u008f"+
		"\u0090\7\35\2\2\u0090\u0091\7\33\2\2\u0091\u0092\5\20\t\2\u0092\u0093"+
		"\7\34\2\2\u0093\u0095\3\2\2\2\u0094\u008f\3\2\2\2\u0095\u0098\3\2\2\2"+
		"\u0096\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097\17\3\2\2\2\u0098\u0096"+
		"\3\2\2\2\u0099\u009e\5$\23\2\u009a\u009b\7\35\2\2\u009b\u009d\5$\23\2"+
		"\u009c\u009a\3\2\2\2\u009d\u00a0\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f"+
		"\3\2\2\2\u009f\21\3\2\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a3\5\24\13\2\u00a2"+
		"\u00a4\5\26\f\2\u00a3\u00a2\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a6\3"+
		"\2\2\2\u00a5\u00a7\5\30\r\2\u00a6\u00a5\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7"+
		"\u00a9\3\2\2\2\u00a8\u00aa\5\32\16\2\u00a9\u00a8\3\2\2\2\u00a9\u00aa\3"+
		"\2\2\2\u00aa\23\3\2\2\2\u00ab\u00ad\7M\2\2\u00ac\u00ae\79\2\2\u00ad\u00ac"+
		"\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b4\5(\25\2\u00b0"+
		"\u00b1\7\35\2\2\u00b1\u00b3\5(\25\2\u00b2\u00b0\3\2\2\2\u00b3\u00b6\3"+
		"\2\2\2\u00b4\u00b2\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\25\3\2\2\2\u00b6"+
		"\u00b4\3\2\2\2\u00b7\u00b8\7<\2\2\u00b8\u00c1\5B\"\2\u00b9\u00ba\7<\2"+
		"\2\u00ba\u00c1\5D#\2\u00bb\u00bc\7<\2\2\u00bc\u00bd\7\33\2\2\u00bd\u00be"+
		"\5\n\6\2\u00be\u00bf\7\34\2\2\u00bf\u00c1\3\2\2\2\u00c0\u00b7\3\2\2\2"+
		"\u00c0\u00b9\3\2\2\2\u00c0\u00bb\3\2\2\2\u00c1\27\3\2\2\2\u00c2\u00c3"+
		"\7S\2\2\u00c3\u00c4\5$\23\2\u00c4\31\3\2\2\2\u00c5\u00c6\7=\2\2\u00c6"+
		"\u00c7\7\64\2\2\u00c7\u00cc\5H%\2\u00c8\u00c9\7\35\2\2\u00c9\u00cb\5H"+
		"%\2\u00ca\u00c8\3\2\2\2\u00cb\u00ce\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc"+
		"\u00cd\3\2\2\2\u00cd\33\3\2\2\2\u00ce\u00cc\3\2\2\2\u00cf\u00d0\7J\2\2"+
		"\u00d0\u00d1\7\64\2\2\u00d1\u00d6\5&\24\2\u00d2\u00d3\7\35\2\2\u00d3\u00d5"+
		"\5&\24\2\u00d4\u00d2\3\2\2\2\u00d5\u00d8\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d6"+
		"\u00d7\3\2\2\2\u00d7\35\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d9\u00da\7E\2\2"+
		"\u00da\u00db\7W\2\2\u00db\37\3\2\2\2\u00dc\u00dd\7H\2\2\u00dd\u00de\7"+
		"W\2\2\u00de!\3\2\2\2\u00df\u00e0\7A\2\2\u00e0\u00e1\t\3\2\2\u00e1\u00e2"+
		"\5D#\2\u00e2\u00e3\5\n\6\2\u00e3#\3\2\2\2\u00e4\u00e5\b\23\1\2\u00e5\u00e6"+
		"\5\62\32\2\u00e6\u00e7\5$\23\22\u00e7\u010a\3\2\2\2\u00e8\u00e9\7F\2\2"+
		"\u00e9\u010a\5$\23\b\u00ea\u010a\5.\30\2\u00eb\u010a\5,\27\2\u00ec\u010a"+
		"\5H%\2\u00ed\u010a\5\60\31\2\u00ee\u010a\5\64\33\2\u00ef\u00f0\7\66\2"+
		"\2\u00f0\u00f1\7\33\2\2\u00f1\u00f2\5$\23\2\u00f2\u00f3\7\61\2\2\u00f3"+
		"\u00f4\5<\37\2\u00f4\u00f5\7\34\2\2\u00f5\u010a\3\2\2\2\u00f6\u00f8\7"+
		"\65\2\2\u00f7\u00f9\5$\23\2\u00f8\u00f7\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9"+
		"\u00ff\3\2\2\2\u00fa\u00fb\7R\2\2\u00fb\u00fc\5$\23\2\u00fc\u00fd\7O\2"+
		"\2\u00fd\u00fe\5$\23\2\u00fe\u0100\3\2\2\2\u00ff\u00fa\3\2\2\2\u0100\u0101"+
		"\3\2\2\2\u0101\u00ff\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0105\3\2\2\2\u0103"+
		"\u0104\7:\2\2\u0104\u0106\5$\23\2\u0105\u0103\3\2\2\2\u0105\u0106\3\2"+
		"\2\2\u0106\u0107\3\2\2\2\u0107\u0108\7;\2\2\u0108\u010a\3\2\2\2\u0109"+
		"\u00e4\3\2\2\2\u0109\u00e8\3\2\2\2\u0109\u00ea\3\2\2\2\u0109\u00eb\3\2"+
		"\2\2\u0109\u00ec\3\2\2\2\u0109\u00ed\3\2\2\2\u0109\u00ee\3\2\2\2\u0109"+
		"\u00ef\3\2\2\2\u0109\u00f6\3\2\2\2\u010a\u0152\3\2\2\2\u010b\u010c\f\21"+
		"\2\2\u010c\u010d\t\4\2\2\u010d\u0151\5$\23\22\u010e\u010f\f\20\2\2\u010f"+
		"\u0110\t\5\2\2\u0110\u0151\5$\23\21\u0111\u0112\f\17\2\2\u0112\u0113\t"+
		"\6\2\2\u0113\u0151\5$\23\20\u0114\u0115\f\16\2\2\u0115\u0116\t\7\2\2\u0116"+
		"\u0151\5$\23\17\u0117\u0118\f\7\2\2\u0118\u0119\t\b\2\2\u0119\u0151\5"+
		"$\23\b\u011a\u011b\f\6\2\2\u011b\u011c\t\t\2\2\u011c\u0151\5$\23\7\u011d"+
		"\u011e\f\24\2\2\u011e\u011f\7\3\2\2\u011f\u0120\5$\23\2\u0120\u0121\7"+
		"\4\2\2\u0121\u0151\3\2\2\2\u0122\u0123\f\23\2\2\u0123\u0124\7\32\2\2\u0124"+
		"\u0151\7V\2\2\u0125\u0127\f\r\2\2\u0126\u0128\7F\2\2\u0127\u0126\3\2\2"+
		"\2\u0127\u0128\3\2\2\2\u0128\u0129\3\2\2\2\u0129\u012a\7D\2\2\u012a\u0151"+
		"\5,\27\2\u012b\u012d\f\f\2\2\u012c\u012e\7F\2\2\u012d\u012c\3\2\2\2\u012d"+
		"\u012e\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u0130\7L\2\2\u0130\u0151\5,\27"+
		"\2\u0131\u0132\f\13\2\2\u0132\u0134\7C\2\2\u0133\u0135\7F\2\2\u0134\u0133"+
		"\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0136\3\2\2\2\u0136\u0151\7G\2\2\u0137"+
		"\u0139\f\n\2\2\u0138\u013a\7F\2\2\u0139\u0138\3\2\2\2\u0139\u013a\3\2"+
		"\2\2\u013a\u013b\3\2\2\2\u013b\u013c\7@\2\2\u013c\u013d\7\33\2\2\u013d"+
		"\u0142\5,\27\2\u013e\u013f\7\35\2\2\u013f\u0141\5,\27\2\u0140\u013e\3"+
		"\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2\2\2\u0142\u0143\3\2\2\2\u0143"+
		"\u0145\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u0146\7\34\2\2\u0146\u0151\3"+
		"\2\2\2\u0147\u0149\f\t\2\2\u0148\u014a\7F\2\2\u0149\u0148\3\2\2\2\u0149"+
		"\u014a\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\7\63\2\2\u014c\u014d\5"+
		",\27\2\u014d\u014e\7\60\2\2\u014e\u014f\5,\27\2\u014f\u0151\3\2\2\2\u0150"+
		"\u010b\3\2\2\2\u0150\u010e\3\2\2\2\u0150\u0111\3\2\2\2\u0150\u0114\3\2"+
		"\2\2\u0150\u0117\3\2\2\2\u0150\u011a\3\2\2\2\u0150\u011d\3\2\2\2\u0150"+
		"\u0122\3\2\2\2\u0150\u0125\3\2\2\2\u0150\u012b\3\2\2\2\u0150\u0131\3\2"+
		"\2\2\u0150\u0137\3\2\2\2\u0150\u0147\3\2\2\2\u0151\u0154\3\2\2\2\u0152"+
		"\u0150\3\2\2\2\u0152\u0153\3\2\2\2\u0153%\3\2\2\2\u0154\u0152\3\2\2\2"+
		"\u0155\u0157\5$\23\2\u0156\u0158\t\n\2\2\u0157\u0156\3\2\2\2\u0157\u0158"+
		"\3\2\2\2\u0158\'\3\2\2\2\u0159\u015e\5$\23\2\u015a\u015c\7\61\2\2\u015b"+
		"\u015a\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u015f\58"+
		"\35\2\u015e\u015b\3\2\2\2\u015e\u015f\3\2\2\2\u015f)\3\2\2\2\u0160\u0162"+
		"\7P\2\2\u0161\u0163\7/\2\2\u0162\u0161\3\2\2\2\u0162\u0163\3\2\2\2\u0163"+
		"+\3\2\2\2\u0164\u0165\t\13\2\2\u0165-\3\2\2\2\u0166\u0167\7\37\2\2\u0167"+
		"/\3\2\2\2\u0168\u0169\5F$\2\u0169\u016a\7\33\2\2\u016a\u016b\7\34\2\2"+
		"\u016b\u0179\3\2\2\2\u016c\u016d\5F$\2\u016d\u016e\7\33\2\2\u016e\u0173"+
		"\5$\23\2\u016f\u0170\7\35\2\2\u0170\u0172\5$\23\2\u0171\u016f\3\2\2\2"+
		"\u0172\u0175\3\2\2\2\u0173\u0171\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0176"+
		"\3\2\2\2\u0175\u0173\3\2\2\2\u0176\u0177\7\34\2\2\u0177\u0179\3\2\2\2"+
		"\u0178\u0168\3\2\2\2\u0178\u016c\3\2\2\2\u0179\61\3\2\2\2\u017a\u017b"+
		"\t\5\2\2\u017b\63\3\2\2\2\u017c\u017d\7\33\2\2\u017d\u017e\5$\23\2\u017e"+
		"\u017f\7\34\2\2\u017f\65\3\2\2\2\u0180\u0181\7X\2\2\u0181\67\3\2\2\2\u0182"+
		"\u0183\t\f\2\2\u01839\3\2\2\2\u0184\u0185\t\r\2\2\u0185;\3\2\2\2\u0186"+
		"\u01ac\5@!\2\u0187\u0188\7\7\2\2\u0188\u0189\7(\2\2\u0189\u018e\5> \2"+
		"\u018a\u018b\7\35\2\2\u018b\u018d\5> \2\u018c\u018a\3\2\2\2\u018d\u0190"+
		"\3\2\2\2\u018e\u018c\3\2\2\2\u018e\u018f\3\2\2\2\u018f\u0191\3\2\2\2\u0190"+
		"\u018e\3\2\2\2\u0191\u0192\7*\2\2\u0192\u01ac\3\2\2\2\u0193\u0194\7\b"+
		"\2\2\u0194\u0195\7(\2\2\u0195\u0196\5@!\2\u0196\u0197\7\35\2\2\u0197\u0198"+
		"\5<\37\2\u0198\u0199\7*\2\2\u0199\u01ac\3\2\2\2\u019a\u019b\7\t\2\2\u019b"+
		"\u019c\7(\2\2\u019c\u019d\5<\37\2\u019d\u019e\7*\2\2\u019e\u01ac\3\2\2"+
		"\2\u019f\u01a0\7\n\2\2\u01a0\u01a1\7(\2\2\u01a1\u01a6\5<\37\2\u01a2\u01a3"+
		"\7\35\2\2\u01a3\u01a5\5<\37\2\u01a4\u01a2\3\2\2\2\u01a5\u01a8\3\2\2\2"+
		"\u01a6\u01a4\3\2\2\2\u01a6\u01a7\3\2\2\2\u01a7\u01a9\3\2\2\2\u01a8\u01a6"+
		"\3\2\2\2\u01a9\u01aa\7*\2\2\u01aa\u01ac\3\2\2\2\u01ab\u0186\3\2\2\2\u01ab"+
		"\u0187\3\2\2\2\u01ab\u0193\3\2\2\2\u01ab\u019a\3\2\2\2\u01ab\u019f\3\2"+
		"\2\2\u01ac=\3\2\2\2\u01ad\u01b0\5H%\2\u01ae\u01af\7\6\2\2\u01af\u01b1"+
		"\5<\37\2\u01b0\u01ae\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1?\3\2\2\2\u01b2"+
		"\u01d2\7\13\2\2\u01b3\u01b4\7\f\2\2\u01b4\u01b5\7\33\2\2\u01b5\u01b6\7"+
		"W\2\2\u01b6\u01d2\7\34\2\2\u01b7\u01b8\7\r\2\2\u01b8\u01b9\7\33\2\2\u01b9"+
		"\u01ba\7W\2\2\u01ba\u01d2\7\34\2\2\u01bb\u01d2\7\16\2\2\u01bc\u01d2\7"+
		"\17\2\2\u01bd\u01d2\7\20\2\2\u01be\u01d2\7\21\2\2\u01bf\u01d2\7\22\2\2"+
		"\u01c0\u01d2\7\23\2\2\u01c1\u01d2\7\24\2\2\u01c2\u01c3\7\24\2\2\u01c3"+
		"\u01c4\7\33\2\2\u01c4\u01c5\7W\2\2\u01c5\u01d2\7\34\2\2\u01c6\u01c7\7"+
		"\24\2\2\u01c7\u01c8\7\33\2\2\u01c8\u01c9\7W\2\2\u01c9\u01ca\7\35\2\2\u01ca"+
		"\u01cb\7W\2\2\u01cb\u01d2\7\34\2\2\u01cc\u01d2\7\24\2\2\u01cd\u01d2\7"+
		"\25\2\2\u01ce\u01d2\7\26\2\2\u01cf\u01d2\7\27\2\2\u01d0\u01d2\7\30\2\2"+
		"\u01d1\u01b2\3\2\2\2\u01d1\u01b3\3\2\2\2\u01d1\u01b7\3\2\2\2\u01d1\u01bb"+
		"\3\2\2\2\u01d1\u01bc\3\2\2\2\u01d1\u01bd\3\2\2\2\u01d1\u01be\3\2\2\2\u01d1"+
		"\u01bf\3\2\2\2\u01d1\u01c0\3\2\2\2\u01d1\u01c1\3\2\2\2\u01d1\u01c2\3\2"+
		"\2\2\u01d1\u01c6\3\2\2\2\u01d1\u01cc\3\2\2\2\u01d1\u01cd\3\2\2\2\u01d1"+
		"\u01ce\3\2\2\2\u01d1\u01cf\3\2\2\2\u01d1\u01d0\3\2\2\2\u01d2A\3\2\2\2"+
		"\u01d3\u01d6\7V\2\2\u01d4\u01d5\7\32\2\2\u01d5\u01d7\7V\2\2\u01d6\u01d4"+
		"\3\2\2\2\u01d6\u01d7\3\2\2\2\u01d7C\3\2\2\2\u01d8\u01d9\7X\2\2\u01d9E"+
		"\3\2\2\2\u01da\u01db\5J&\2\u01dbG\3\2\2\2\u01dc\u01dd\5J&\2\u01ddI\3\2"+
		"\2\2\u01de\u01e6\7V\2\2\u01df\u01e6\5:\36\2\u01e0\u01e6\7X\2\2\u01e1\u01e2"+
		"\7\33\2\2\u01e2\u01e3\5J&\2\u01e3\u01e4\7\34\2\2\u01e4\u01e6\3\2\2\2\u01e5"+
		"\u01de\3\2\2\2\u01e5\u01df\3\2\2\2\u01e5\u01e0\3\2\2\2\u01e5\u01e1\3\2"+
		"\2\2\u01e6K\3\2\2\2\60OV[als{\177\u0082\u0085\u0089\u0096\u009e\u00a3"+
		"\u00a6\u00a9\u00ad\u00b4\u00c0\u00cc\u00d6\u00f8\u0101\u0105\u0109\u0127"+
		"\u012d\u0134\u0139\u0142\u0149\u0150\u0152\u0157\u015b\u015e\u0162\u0173"+
		"\u0178\u018e\u01a6\u01ab\u01b0\u01d1\u01d6\u01e5";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}