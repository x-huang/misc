// Generated from EAQL.g4 by ANTLR 4.5.1

    package xw.data.eaql.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link EAQLParser}.
 */
public interface EAQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link EAQLParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(EAQLParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(EAQLParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#error}.
	 * @param ctx the parse tree
	 */
	void enterError(EAQLParser.ErrorContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#error}.
	 * @param ctx the parse tree
	 */
	void exitError(EAQLParser.ErrorContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt(EAQLParser.Sql_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt(EAQLParser.Sql_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#describe_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDescribe_table_stmt(EAQLParser.Describe_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#describe_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDescribe_table_stmt(EAQLParser.Describe_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt(EAQLParser.Select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt(EAQLParser.Select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#select_or_values}.
	 * @param ctx the parse tree
	 */
	void enterSelect_or_values(EAQLParser.Select_or_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#select_or_values}.
	 * @param ctx the parse tree
	 */
	void exitSelect_or_values(EAQLParser.Select_or_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#values_stmt}.
	 * @param ctx the parse tree
	 */
	void enterValues_stmt(EAQLParser.Values_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#values_stmt}.
	 * @param ctx the parse tree
	 */
	void exitValues_stmt(EAQLParser.Values_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#expr_list}.
	 * @param ctx the parse tree
	 */
	void enterExpr_list(EAQLParser.Expr_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#expr_list}.
	 * @param ctx the parse tree
	 */
	void exitExpr_list(EAQLParser.Expr_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#simple_select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSimple_select_stmt(EAQLParser.Simple_select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#simple_select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSimple_select_stmt(EAQLParser.Simple_select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#select_clause}.
	 * @param ctx the parse tree
	 */
	void enterSelect_clause(EAQLParser.Select_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#select_clause}.
	 * @param ctx the parse tree
	 */
	void exitSelect_clause(EAQLParser.Select_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#from_clause}.
	 * @param ctx the parse tree
	 */
	void enterFrom_clause(EAQLParser.From_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#from_clause}.
	 * @param ctx the parse tree
	 */
	void exitFrom_clause(EAQLParser.From_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#where_clause}.
	 * @param ctx the parse tree
	 */
	void enterWhere_clause(EAQLParser.Where_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#where_clause}.
	 * @param ctx the parse tree
	 */
	void exitWhere_clause(EAQLParser.Where_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#group_by_clause}.
	 * @param ctx the parse tree
	 */
	void enterGroup_by_clause(EAQLParser.Group_by_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#group_by_clause}.
	 * @param ctx the parse tree
	 */
	void exitGroup_by_clause(EAQLParser.Group_by_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#order_by_clause}.
	 * @param ctx the parse tree
	 */
	void enterOrder_by_clause(EAQLParser.Order_by_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#order_by_clause}.
	 * @param ctx the parse tree
	 */
	void exitOrder_by_clause(EAQLParser.Order_by_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#limit_clause}.
	 * @param ctx the parse tree
	 */
	void enterLimit_clause(EAQLParser.Limit_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#limit_clause}.
	 * @param ctx the parse tree
	 */
	void exitLimit_clause(EAQLParser.Limit_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#offset_clause}.
	 * @param ctx the parse tree
	 */
	void enterOffset_clause(EAQLParser.Offset_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#offset_clause}.
	 * @param ctx the parse tree
	 */
	void exitOffset_clause(EAQLParser.Offset_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#insert_stmt}.
	 * @param ctx the parse tree
	 */
	void enterInsert_stmt(EAQLParser.Insert_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#insert_stmt}.
	 * @param ctx the parse tree
	 */
	void exitInsert_stmt(EAQLParser.Insert_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(EAQLParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(EAQLParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void enterOrdering_term(EAQLParser.Ordering_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void exitOrdering_term(EAQLParser.Ordering_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#result_column}.
	 * @param ctx the parse tree
	 */
	void enterResult_column(EAQLParser.Result_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#result_column}.
	 * @param ctx the parse tree
	 */
	void exitResult_column(EAQLParser.Result_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#compound_operator}.
	 * @param ctx the parse tree
	 */
	void enterCompound_operator(EAQLParser.Compound_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#compound_operator}.
	 * @param ctx the parse tree
	 */
	void exitCompound_operator(EAQLParser.Compound_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(EAQLParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(EAQLParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#asterisk}.
	 * @param ctx the parse tree
	 */
	void enterAsterisk(EAQLParser.AsteriskContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#asterisk}.
	 * @param ctx the parse tree
	 */
	void exitAsterisk(EAQLParser.AsteriskContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(EAQLParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(EAQLParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(EAQLParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(EAQLParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#parenthetical}.
	 * @param ctx the parse tree
	 */
	void enterParenthetical(EAQLParser.ParentheticalContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#parenthetical}.
	 * @param ctx the parse tree
	 */
	void exitParenthetical(EAQLParser.ParentheticalContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#error_message}.
	 * @param ctx the parse tree
	 */
	void enterError_message(EAQLParser.Error_messageContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#error_message}.
	 * @param ctx the parse tree
	 */
	void exitError_message(EAQLParser.Error_messageContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void enterColumn_alias(EAQLParser.Column_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void exitColumn_alias(EAQLParser.Column_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(EAQLParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(EAQLParser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#data_type}.
	 * @param ctx the parse tree
	 */
	void enterData_type(EAQLParser.Data_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#data_type}.
	 * @param ctx the parse tree
	 */
	void exitData_type(EAQLParser.Data_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#struct_field}.
	 * @param ctx the parse tree
	 */
	void enterStruct_field(EAQLParser.Struct_fieldContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#struct_field}.
	 * @param ctx the parse tree
	 */
	void exitStruct_field(EAQLParser.Struct_fieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#primitive_type}.
	 * @param ctx the parse tree
	 */
	void enterPrimitive_type(EAQLParser.Primitive_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#primitive_type}.
	 * @param ctx the parse tree
	 */
	void exitPrimitive_type(EAQLParser.Primitive_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(EAQLParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(EAQLParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#resource_descriptor}.
	 * @param ctx the parse tree
	 */
	void enterResource_descriptor(EAQLParser.Resource_descriptorContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#resource_descriptor}.
	 * @param ctx the parse tree
	 */
	void exitResource_descriptor(EAQLParser.Resource_descriptorContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#function_name}.
	 * @param ctx the parse tree
	 */
	void enterFunction_name(EAQLParser.Function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#function_name}.
	 * @param ctx the parse tree
	 */
	void exitFunction_name(EAQLParser.Function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(EAQLParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(EAQLParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EAQLParser#any_name}.
	 * @param ctx the parse tree
	 */
	void enterAny_name(EAQLParser.Any_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EAQLParser#any_name}.
	 * @param ctx the parse tree
	 */
	void exitAny_name(EAQLParser.Any_nameContext ctx);
}