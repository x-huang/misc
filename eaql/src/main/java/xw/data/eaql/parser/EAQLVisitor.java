// Generated from EAQL.g4 by ANTLR 4.5.1

    package xw.data.eaql.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link EAQLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface EAQLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link EAQLParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(EAQLParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#error}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(EAQLParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt(EAQLParser.Sql_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#describe_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescribe_table_stmt(EAQLParser.Describe_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt(EAQLParser.Select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#select_or_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_or_values(EAQLParser.Select_or_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#values_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValues_stmt(EAQLParser.Values_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#expr_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_list(EAQLParser.Expr_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#simple_select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimple_select_stmt(EAQLParser.Simple_select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#select_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_clause(EAQLParser.Select_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#from_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrom_clause(EAQLParser.From_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#where_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere_clause(EAQLParser.Where_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#group_by_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroup_by_clause(EAQLParser.Group_by_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#order_by_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrder_by_clause(EAQLParser.Order_by_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#limit_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimit_clause(EAQLParser.Limit_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#offset_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOffset_clause(EAQLParser.Offset_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#insert_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert_stmt(EAQLParser.Insert_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(EAQLParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#ordering_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering_term(EAQLParser.Ordering_termContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#result_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column(EAQLParser.Result_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#compound_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_operator(EAQLParser.Compound_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#literal_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value(EAQLParser.Literal_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#asterisk}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsterisk(EAQLParser.AsteriskContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(EAQLParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(EAQLParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#parenthetical}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthetical(EAQLParser.ParentheticalContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#error_message}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError_message(EAQLParser.Error_messageContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#column_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias(EAQLParser.Column_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(EAQLParser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#data_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_type(EAQLParser.Data_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#struct_field}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_field(EAQLParser.Struct_fieldContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#primitive_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitive_type(EAQLParser.Primitive_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(EAQLParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#resource_descriptor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResource_descriptor(EAQLParser.Resource_descriptorContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_name(EAQLParser.Function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(EAQLParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EAQLParser#any_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_name(EAQLParser.Any_nameContext ctx);
}