package xw.data.eaql.schema;

/**
 * Created by xhuang on 9/2/16.
 */
public class SchemaMismatchException extends Exception
{
    public final Schema schema1, schema2;

    public SchemaMismatchException(String info, Schema schema1, Schema schema2) {
        super(info + ", schemas are '" + schema1 + "' vs. '" + schema2 + "'");
        this.schema1 = schema1;
        this.schema2 = schema2;
    }
}
