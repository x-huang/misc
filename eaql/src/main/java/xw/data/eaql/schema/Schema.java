package xw.data.eaql.schema;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.StructType;
import xw.data.eaql.model.type.Types;

import java.util.*;

/**
 * Created by xhuang on 7/27/2015.
 */
public class Schema
{
    public static final Schema emptySchema = new Schema();

    private final List<String> columns;
    private final Map<String, DataType> columnTypes;
    private final Map<String, Integer> columnIndices;

    private Schema() {
        this.columns = new ArrayList<>();
        this.columnIndices = new HashMap<>();
        this.columnTypes = new HashMap<>();
    }

    public static class Builder {
        private Schema schema = new Schema();
        public Builder addColumn(String column, DataType type) {
            schema.addColumn(column, type);
            return this;
        }
        public Schema build() {
            return schema;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Schema fromStructType(StructType struct) {
        final Schema schema = new Schema();
        for (StructType.StructField field: struct.fields) {
            schema.addColumn(field.name, field.type);
        }
        return schema;
    }

    public static Schema fromString(String s) {
        if (s == null || s.trim().isEmpty()) {
            throw new IllegalArgumentException("empty schema");
        }

        s = s.toLowerCase();
        if (!s.startsWith("struct<")) {
            s = "struct<" + s + ">";
        }

        final StructType struct = (StructType) Types.fromString(s);
        return fromStructType(struct);
    }

    public static Schema join(Schema schema1, Schema schema2) {
        if (schema1 == null || schema2 == null) {
            throw new NullPointerException("joining null schema");
        }
        final Schema schema = new Schema();
        for (String col: schema1.columns) {
            schema.addColumn(col, schema1.typeOf(col));
        }
        for (String col: schema2.columns) {
            schema.addColumn(col, schema2.typeOf(col));
        }
        return schema;
    }

    public int size() {
        return columns.size();
    }

    public List<String> columns() {
        return columns;
    }

    private Schema addColumn(String col, DataType type) {
        if (columnIndices.containsKey(col)) {
            throw new IllegalArgumentException("redefined column name: " + col);
        }
        columns.add(col);
        columnIndices.put(col, columns.size()-1);
        columnTypes.put(col, type);
        return this;
    }

    public String columnAt(int index) {
        return columns.get(index);
    }

    public DataType typeOf(String column) {
        if (! columnIndices.containsKey(column)) {
            throw new IllegalArgumentException("no such column: " + column);
        }
        return columnTypes.get(column);
    }

    public DataType typeAt(int index) {
        if (index < 0 || index >= columns.size()) {
            throw new IllegalArgumentException("no such column index: " + index);
        }
        return columnTypes.get(columns.get(index));
    }

    public int indexOf(String column) {
        if (! columnIndices.containsKey(column)) {
            throw new IllegalArgumentException("no such column: " + column);
        }
        return columnIndices.get(column);
    }

    public boolean hasColumn(String column) {
        return columnIndices.containsKey(column);
    }

    public Schema projectionOf(List<String> columns) {
        final Schema projection = new Schema();
        int index = 0;
        for (String column: columns) {
            projection.columns.add(column);
            projection.columnIndices.put(column, index);
            projection.columnTypes.put(column, this.typeOf(column));
            index++;
        }
        return projection;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (! (obj instanceof Schema)) {
            return false;
        }
        final Schema that = (Schema) obj;
        if (columns.size() != that.columns.size()) {
            return false;
        }
        for (int i=0; i<columns.size(); i++) {
            final String col = columns.get(i);
            if (! col.equals(that.columns.get(i))) {
                return false;
            }
            if (! Types.strictlyEqual(typeOf(col),that.typeOf(col))) {
                return false;
            }
            if (indexOf(col) != that.indexOf(col)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (String name: columns) {
            final DataType type = typeOf(name);
            sb.append(delimiter).append(name).append(':').append(type);
            delimiter = ",";
        }
        return sb.toString();
    }

    public static Schema mergeEliminatingVoid(final Schema s1, final Schema s2)
            throws SchemaMismatchException {

        if (s1.size() != s2.size()) {
            throw new SchemaMismatchException("size mismatch: "
                    + s1.size() + " vs. " + s2.size(), s1, s2);
        }

        final Builder builder = new Builder();
        for (int i=0; i<s1.size(); i++) {
            final String col1 = s1.columns.get(i);
            final String col2 = s2.columns.get(i);
            if (col1.startsWith("_col") && col2.startsWith("_col") && ! col1.equals(col2)) {
                throw new SchemaMismatchException("column name mismatch at index "
                        + (i+1) + ": '" + col1 + "' vs. '" + col2 + "'", s1, s2);
            }
            final String col = col1.startsWith("_col")? col2: col1;
            final DataType type = s1.typeOf(col1);
            final DataType type2 = s2.typeOf(col2);
            if (type == Types.VOID) {
                builder.addColumn(col, type2);
            } else if (type2 == Types.VOID) {
                builder.addColumn(col, type);
            } else {
                if (! Types.strictlyEqual(type, type2)) {
                    throw new SchemaMismatchException("column type mismatch at index "
                            + (i+1) + ": '" + type + "' vs. '" + type2 + "'", s1, s2);
                }
                builder.addColumn(col, type);
            }
        }
        return builder.build();
    }

}
