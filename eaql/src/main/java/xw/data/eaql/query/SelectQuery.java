package xw.data.eaql.query;

import xw.data.eaql.model.stmt.SelectOrValuesStatement;
import xw.data.eaql.model.stmt.SelectStatement;
import xw.data.eaql.model.stmt.SimpleSelectStatement;
import xw.data.eaql.model.stmt.ValuesStatement;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.orderby.OrderBy;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.schema.SchemaMismatchException;
import xw.data.eaql.semantic.SemanticException;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/26/16.
 */
public class SelectQuery extends SQLQueryBase<SelectStatement> implements SQLRead
{
    private final List<SelectOrValuesQuery> sovqs;
    protected final Schema outputSchema;

    SelectQuery(QueryEnvironment env, SelectStatement stmt) {
        super(env, stmt);
        this.sovqs = new ArrayList<>();

        Schema schema = null;
        for (SelectOrValuesStatement sov: stmt.sovs) {
            final SelectOrValuesQuery sovq;
            if (sov instanceof SimpleSelectStatement) {
                sovq = new SimpleSelectQuery(env, (SimpleSelectStatement) sov);
            } else if (sov instanceof ValuesStatement) {
                sovq = new ValuesQuery(env, (ValuesStatement) sov);
            } else {
                throw new RuntimeException("unexpected sql statement: " + sov);
            }

            if (schema == null) {
                schema = sovq.getSchema();
            } else {
                try {
                    schema = Schema.mergeEliminatingVoid(schema, sovq.getSchema());
                } catch (SchemaMismatchException e) {
                    throw new SemanticException("schema mismatch: " + e.getMessage(), e);
                }
            }
            sovqs.add(sovq);
        }
        this.outputSchema = schema;
    }

    public void assertNoVoidType() {
        for (String col: outputSchema.columns()) {
            if (outputSchema.typeOf(col) == Types.VOID) {
                throw new SemanticException("cannot determine type of column '" + col + "'");
            }
        }
    }

    @Override
    public Schema getSchema() {
        return outputSchema;
    }

    @Override
    public DataSource getDataSource() {
        final DataSource unorderedStream;
        if (sovqs.size() == 1) {
            unorderedStream = sovqs.get(0).getDataSource();
        } else {
            List<DataSource> streams = new ArrayList<>();
            for (SelectOrValuesQuery sovq : sovqs) {
                streams.add(sovq.getDataSource());
            }
            unorderedStream = DataStreams.concatenate(streams);
        }

        DataSource out = unorderedStream;
        if (stmt.orderBy != null) {
            final OrderBy orderBy = new OrderBy(stmt.orderBy, outputSchema, out);
            out = orderBy.getOrderedOutputStream();
        }
        if (stmt.limit != null || stmt.offset != null) {
            final long limit = stmt.limit != null? stmt.limit.num: Long.MAX_VALUE;
            final long offset = stmt.offset != null? stmt.offset.num: 0;
            out = DataStreams.slice(out, limit, offset);
        }
        return out;
        /*
        if (stmt.orderBy == null) {
            return unorderedStream;
        } else {
            final OrderBy orderBy = new OrderBy(stmt.orderBy, outputSchema, unorderedStream);
            return orderBy.getOrderedOutputStream();
        }*/
    }

}
