package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 8/30/16.
 */
class UDF_concat implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSizeNoLessThan(paramTypes, 2);
        // UDFs.expectAllParametersType(paramTypes, Types.STRING);
        return Types.STRING;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        String s = "";
        for (Lazy<Object> lazyParam: lazyParams) {
            s += lazyParam.get();
        }
        return s;
    }
}
