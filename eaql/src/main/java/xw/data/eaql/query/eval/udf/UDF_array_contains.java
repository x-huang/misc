package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_array_contains implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 2);
        return Types.BOOLEAN;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final List<?> array = (List) lazyParams.get(0).get();
        final Object elem = lazyParams.get(1).get();
        for (Object e: array) {
            if (e != null && e.equals(elem)) {
                return true;
            }
        }
        return false;
    }
}
