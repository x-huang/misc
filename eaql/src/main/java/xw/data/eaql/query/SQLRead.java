package xw.data.eaql.query;

import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.DataSource;

/**
 * Created by xhuang on 8/17/16.
 */
public interface SQLRead
{
    Schema getSchema();
    DataSource getDataSource();
}
