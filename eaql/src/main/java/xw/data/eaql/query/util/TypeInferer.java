package xw.data.eaql.query.util;

import xw.data.eaql.model.expr.*;
import xw.data.eaql.model.type.*;
import xw.data.eaql.query.eval.udaf.UDAF;
import xw.data.eaql.query.eval.udaf.UDAFFactory;
import xw.data.eaql.query.eval.udaf.UDAFs;
import xw.data.eaql.query.eval.udf.UDF;
import xw.data.eaql.query.eval.udf.UDFException;
import xw.data.eaql.query.eval.udf.UDFs;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.semantic.SemanticException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by xhuang on 8/29/16.
 */
public class TypeInferer implements ExprVisitor<DataType>
{
    public final Schema schema;

    public TypeInferer(Schema schema) {
        this.schema = schema;
    }

    private static DataType upcast(DataType type) {
        if (Types.isIntegerNumberType(type)) {
            return Types.BIGINT;
        } else if (Types.isFloatNumberType(type)) {
            return Types.DOUBLE;
        } else {
            return type;
        }
    }

    private static DataType matchNumberType(DataType t1, DataType t2) {
        if (t1 == Types.DOUBLE || t2 == Types.DOUBLE) {
            return Types.DOUBLE;
        } else {
            return Types.BIGINT;
        }
    }

    private static DataType getConsistentLiteralDataType(Collection<Object> values) {
        DataType type = Types.VOID;
        for (Object value: values) {
            if (type == Types.VOID) {
                type = getLiteralDataType(value);
            } else if (! Types.strictlyEqual(type, getLiteralDataType(value))){
                throw new SemanticException("error inferring result type of " +
                        values + ": expecting all values of same type, got '"
                        + type + "' vs. '" + getLiteralDataType(value));
            }
        }
        return type;
    }

    @SuppressWarnings("unchecked")
    public static DataType getLiteralDataType(Object value) {
        if (value == null) {
            return Types.VOID;
        } else if (value instanceof Long) {
            return Types.BIGINT;
        } else if (value instanceof Double){
            return Types.DOUBLE;
        } else if (value instanceof String) {
            return Types.STRING;
        } else if (value instanceof Boolean) {
            return Types.BOOLEAN;
        } else if (value instanceof ArrayLiteral) {
            final DataType valueType = getConsistentLiteralDataType((ArrayLiteral) value);
            return Types.newArrayType(valueType);
        } else if (value instanceof MapLiteral) {
            final MapLiteral ml = (MapLiteral) value;
            final DataType keyType = getConsistentLiteralDataType(ml.keySet());
            final DataType valueType = getConsistentLiteralDataType(ml.values());
            return Types.newMapType((PrimitiveType) keyType, valueType);
        } else if (value instanceof StructLiteral) {
            final StructLiteral sl = (StructLiteral) value;
            final List<StructType.StructField> fields = new ArrayList<>();
            for (int i=0; i<sl.size(); i++) {
                fields.add(new StructType.StructField("col" + (i + 1), getLiteralDataType(sl.get(i))));
            }
            return Types.newStructType(fields);
        } else {
            throw new RuntimeException("unexpected constant type: "
                    + value.getClass().getName());
        }
    }

    @Override
    public DataType visitAddition(Addition e) {
        return matchNumberType(upcast(e.e1.accept(this)), upcast(e.e2.accept(this)));
    }

    @Override
    public DataType visitAnd(And e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitArrayMapElement(ArrayMapElement e) {
        final DataType type = e.e1.accept(this);
        if (Types.isArrayType(type)) {
            final ArrayType arrayType = (ArrayType) type;
            return arrayType.valueType;
        } else if (Types.isMapType(type)) {
            final MapType mapType = (MapType) type;
            return mapType.valueType;
        } else {
            throw new SemanticException("error inferring result type of '"
                    + e + "': expecting array or map type, got " + type);
        }
    }

    @Override
    public DataType visitAsterisk(Asterisk e) {
        throw new RuntimeException("asterisk shall be expanded before type inferring");
    }

    @Override
    public DataType visitBetween(Between e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitCaseExprWhen(CaseExprWhen e) {
        DataType type = Types.VOID;
        Expression retExpr = null;
        for (int i=0; i<=e.numWhens(); i++) {
            final Expression retExpr2 = i<e.numWhens()? e.then(i): e.else_;
            final DataType type2 = retExpr2.accept(this);
            if (type2 == Types.VOID) continue;
            if (type == Types.VOID) {
                type = type2;
                retExpr = retExpr2;
            } else if (! Types.strictlyEqual(type, type2)) {
                throw new SemanticException("error inferring result type of '" +
                        e + "': expecting all return expression of same type, got '"
                        + retExpr + "' vs. '" + retExpr2
                        + "' with result type: " + type + " vs. " + type2);
            }
        }
        return type;
    }

    @Override
    public DataType visitCaseWhen(CaseWhen e) {

        for (int i=0; i<e.numWhens(); i++) {
            final DataType type = e.when(i).accept(this);
            if (type != Types.VOID && type != Types.BOOLEAN) {
                throw new SemanticException("error inferring result type of '" +
                        e + "': expecting all when-condition expressions boolean type, got '" +
                        type + "' by when-condition '" + e.when(i) + "'");
            }
        }

        DataType type = Types.VOID;
        Expression retExpr = null;
        for (int i=0; i<=e.numWhens(); i++) {
            final Expression retExpr2 = i<e.numWhens()? e.then(i): e.else_;
            final DataType type2 = retExpr2.accept(this);
            if (type2 == Types.VOID) continue;
            if (type == Types.VOID) {
                type = type2;
                retExpr = retExpr2;
            } else if (! Types.strictlyEqual(type, type2)) {
                throw new SemanticException("error inferring result type of '" +
                        e + "': expecting all return expression of same type, got '"
                        + retExpr + "' vs. '" + retExpr2
                        + "' with result type: " + type + " vs. " + type2);
            }
        }
        return type;
    }

    @Override
    public DataType visitCast(Cast e) {
        return e.type;
    }

    @Override
    public DataType visitColumnValue(ColumnValue e) {
        return schema.typeOf(e.column);
    }

    @Override
    public DataType visitDivision(Division e) {
        return Types.DOUBLE;
    }

    @Override
    public DataType visitEquals(Equals e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitStructField(StructField e) {
        final String column = ((ColumnValue) e.e).column;
        final DataType type = schema.typeOf(column);
        if (Types.isStructType(type)) {
            final StructType structType = (StructType) type;
            final int fieldIndex = ExprUtils.getStructFieldIndex(structType, e.field);
            if (fieldIndex >= 0) {
                return structType.fields.get(fieldIndex).type;
            } else {
                throw new SemanticException("error inferring result type of '"
                        + e + "': no such struct field '" + e.field + "'");
            }
        } else {
            throw new SemanticException("error inferring result type of '"
                    + e + "': expecting struct type at column '"
                    + column + "', got " + type);
        }
    }

    @Override
    public DataType visitStructFieldIndexed(StructFieldIndexed e) {
        final DataType type = schema.typeAt(e.columnIndex);
        if (Types.isStructType(type)) {
            final StructType structType = (StructType) type;
            return structType.fields.get(e.fieldIndex).type;
        } else {
            throw new SemanticException("error inferring result type of '"
                    + e + "': expecting struct type at column $"
                    + e.columnIndex + "', got " + type);
        }
    }

    @Override
    public DataType visitFunction(Function f) {
        if (UDAFs.isUDAF(f)) {
            final List<DataType> paramTypes = new ArrayList<>();
            for (Expression param: f.params) {
                paramTypes.add(TypeInferer.inferResultType(schema, param));
            }
            final UDAF udaf = UDAFFactory.createUDAF(f, paramTypes);
            return udaf.returnType();
        } else {
            UDFs.assertDefinedUDF(f.name);
            final UDF udf = UDFs.get(f.name);
            final List<DataType> types = new ArrayList<>();
            for (Expression p: f.params) {
                types.add(p.accept(this));
            }
            try {
                return udf.inferReturnType(types);
            } catch (UDFException e) {
                throw new SemanticException("error inferring return type '" + f + "': " + e, e);
            }
        }
    }

    @Override
    public DataType visitGreaterThan(GreaterThan e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitGreaterThanEquals(GreaterThanEquals e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitInSet(InSet e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitIsNull(IsNull e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitLessThan(LessThan e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitLessThanEquals(LessThanEquals e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitLike(Like e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitLiteralValue(LiteralValue e) {
        return getLiteralDataType(e.value);
    }

    @Override
    public DataType visitMultiplication(Multiplication e) {
        return matchNumberType(upcast(e.e1.accept(this)), upcast(e.e2.accept(this)));
    }

    @Override
    public DataType visitNegation(Negation e) {
        return upcast(e.e.accept(this));
    }

    @Override
    public DataType visitNot(Not e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitNotEquals(NotEquals e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitOr(Or e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitParenthetical(Parenthetical e) {
        return e.e.accept(this);
    }

    @Override
    public DataType visitRLike(RLike e) {
        return Types.BOOLEAN;
    }

    @Override
    public DataType visitSubtraction(Subtraction e) {
        return matchNumberType(upcast(e.e1.accept(this)), upcast(e.e2.accept(this)));
    }

    @Override
    public DataType visitColumnValueIndexed(ColumnValueIndexed e) {
        return schema.typeAt(e.index);
    }

    public static DataType inferResultType(Schema schema, Expression e) {
        final TypeInferer inferer = new TypeInferer(schema);
        return e.accept(inferer);
    }
}
