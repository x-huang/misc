package xw.data.eaql.query;


import xw.data.eaql.schema.Schema;

import java.io.IOException;

/**
 * Created by xhuang on 9/16/16.
 */
public interface SQLWrite
{
    Schema getSchema();
    void doWrite() throws IOException;
}
