package xw.data.eaql.query;

import xw.data.eaql.model.stmt.SQLStatement;

/**
 * Created by xhuang on 9/16/16.
 */
public interface SQLQuery<T extends SQLStatement>
{
    QueryEnvironment getEnvironment();
    T getModel();
}
