package xw.data.eaql.query.orderby;

import xw.data.eaql.model.clause.OrderByClause;
import xw.data.eaql.model.expr.ColumnValue;
import xw.data.eaql.model.term.OrderingTerm;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.semantic.SemanticException;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreams;

/**
 * Created by xhuang on 8/27/16.
 */
public class OrderBy
{
    public final Schema schema;
    protected final DataSource inputStream;
    public final OrderByClause clause;
    private final int[] indices;
    private final boolean[] ascendings;

    public OrderBy(OrderByClause clause, Schema schema, DataSource inputStream) {
        this.schema = schema;
        this.clause = clause;
        this.inputStream = inputStream;

        final int len = clause.orderingTerms.size();
        indices = new int[len];
        ascendings = new boolean[len];
        for (int i=0; i<len; i++) {
            final OrderingTerm term = clause.orderingTerms.get(i);
            if ( ! (term.e instanceof ColumnValue)) {
                throw new SemanticException("expect order-by expression a column name, got " + term.e);
            }
            indices[i] = schema.indexOf(((ColumnValue) term.e).column);
            ascendings[i] = term.asc;
        }
    }

    public DataSource getOrderedOutputStream() {
        return DataStreams.sort(inputStream, indices, ascendings);
    }
}
