package xw.data.eaql.query.eval;

import xw.data.eaql.model.expr.*;

/**
 * Created by xhuang on 8/24/16.
 */
public class SimpleEvaluator extends EvaluatorBase
{
    private static final SimpleEvaluator singleton = new SimpleEvaluator();

    public static SimpleEvaluator get() {
        return singleton;
    }

    private static EvaluationException newException(Expression e) {
        String msg = "cannot evaluate " + e.getClass().getSimpleName() + " expression '"
                + e + "' in simple evaluator, which shall not access raw input";
        return new EvaluationException(msg, e);
    }

    @Override
    public Object visitColumnValue(ColumnValue e) {
        throw newException(e);
    }

    @Override
    public Object visitColumnValueIndexed(ColumnValueIndexed e) {
        throw newException(e);
    }

    @Override
    public Object visitStructField(StructField e) {
        throw newException(e);
    }

    @Override
    public Object visitStructFieldIndexed(StructFieldIndexed e) {
        throw newException(e);
    }

}
