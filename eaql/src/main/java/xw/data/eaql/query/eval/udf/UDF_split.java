package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_split implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 2);
        UDFs.expectParameterType(paramTypes, 0, Types.STRING);
        UDFs.expectParameterType(paramTypes, 1, Types.STRING);
        return Types.newArrayType(Types.STRING);
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final String s = (String) lazyParams.get(0).get();
        if (s == null) {
            return null;
        }

        final String pat = (String) lazyParams.get(1).get();
        if (pat == null) {
            return null;
        }

        final String[] splits = s.split(pat);
        final List<String> list = new ArrayList<>();
        Collections.addAll(list, splits);
        return list;
    }
}
