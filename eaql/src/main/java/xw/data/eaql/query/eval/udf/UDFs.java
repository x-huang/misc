package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 8/30/16.
 *
 * refer to: https://www.qubole.com/resources/cheatsheet/hive-function-cheat-sheet/
 *
 */
public class UDFs
{
    private final static Map<String, UDF> udfs = new HashMap<>();

    static {
        udfs.put("abs", new UDF_abs());
        udfs.put("create_array", new UDF_array());
        udfs.put("array_contains", new UDF_array_contains());
        udfs.put("coalesce", new UDF_coalesce());
        udfs.put("concat", new UDF_concat());
        udfs.put("current_date", new UDF_current_date());
        udfs.put("current_timestamp", new UDF_current_timestamp());
        udfs.put("date_add", new UDF_date_add());
        udfs.put("date_format", new UDF_date_format());
        udfs.put("date_sub", new UDF_date_sub());
        udfs.put("datediff", new UDF_datediff());
        udfs.put("e", new UDF_e());
        udfs.put("floor", new UDF_floor());
        udfs.put("if", new UDF_if());
        udfs.put("length", new UDF_length());
        udfs.put("lower", new UDF_lower());
        udfs.put("lcase", udfs.get("lower"));
        udfs.put("create_map", new UDF_map());
        udfs.put("pi", new UDF_pi());
        udfs.put("printf", new UDF_printf());
        udfs.put("round", new UDF_round());
        udfs.put("size", new UDF_size());
        udfs.put("split", new UDF_split());
        udfs.put("str_to_map", new UDF_str_to_map());
        udfs.put("create_struct", new UDF_struct());
        udfs.put("to_date", new UDF_to_date());
        udfs.put("upper", new UDF_upper());
        udfs.put("ucase", udfs.get("upper"));
    }

    public static List<String> getAll() {
        return new ArrayList<>(udfs.keySet());
    }

    public static UDF get(String name) {
        return udfs.get(name.toLowerCase());
    }

    public static boolean isDefinedUDF(String name) {
        return udfs.containsKey(name.toLowerCase());
    }

    public static void assertDefinedUDF(String name) {
        if (!udfs.containsKey(name.toLowerCase())) {
            throw new RuntimeException("undefined function: '" + name + "'");
        }
    }

    protected static void expectParameterSize(List<?> list, int n) throws UDFException {
        if (list.size() != n) {
            throw new UDFException("expect " + n + " parameter(s), got " + list.size());
        }
    }

    protected static void expectParameterSizeNoLessThan(List<?> list, int n) throws UDFException {
        if (list.size() < n) {
            throw new UDFException("expect " + n + " or more parameters, got " + list.size());
        }
    }

    protected static void expectParameterSizeBetween(List<?> list, int m, int n) throws UDFException {
        if (list.size() < m || list.size() > n) {
            throw new UDFException("expect " + m + " to " + n + " parameters, got " + list.size());
        }
    }

    protected static void expectParameterType(List<DataType> types, int i, DataType... expects) throws UDFException {
        if (types.get(i) == Types.VOID) {
            return;
        }
        boolean matched = false;
        for (DataType expect: expects) {
            if (Types.strictlyEqual(types.get(i), expect)) {
                matched = true;
                break;
            }
        }
        if (! matched) {
            throw new UDFException("expect parameter #" + i + " of " + types + ", got '" + types.get(i));
        }
    }

    protected static void expectParameterTypesEqual(List<DataType> types, int i, int j) throws UDFException {
        if (i == j) {
            return;
        }
        if (types.get(i) != Types.VOID && types.get(j) != Types.VOID &&
                !Types.strictlyEqual(types.get(i), types.get(j))) {
            throw new UDFException("expect same types of parameter #" + i + " and #" + j
                    + ", got '" + types.get(i) + "' and '" + types.get(j) + "'");
        }
    }

    protected static void expectAllParametersType(List<DataType> types, DataType type) throws UDFException {
        for (int i = 0; i < types.size(); i++) {
            if (types.get(i) != Types.VOID && !Types.strictlyEqual(types.get(i), type)) {
                throw new UDFException("expect '" + type + "' type of parameter #" + i + ", got '" + types.get(i));
            }
        }
    }

    protected static DataType getParametersTypeAssertingSame(List<DataType> types) throws UDFException {
        return getParametersTypeAssertingSame(types, 0, 1);
    }

    protected static DataType getParametersTypeAssertingSame(List<DataType> types, int start, int step) throws UDFException {
        DataType type = Types.VOID;
        for (int i = start; i < types.size(); i+= step) {
            if (type == Types.VOID) {
                type = types.get(i);
            } else if (!Types.strictlyEqual(type, types.get(i))) {
                throw new UDFException("expect '" + type + "' type of parameter #" + i + ", got '" + types.get(i));
            }
        }
        return type;
    }
}