/*
 * Copyright (c) 2017. Xiaowan Huang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package xw.data.eaql.query.unstructured;

import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.LiteralConstants;
import xw.data.eaql.model.stmt.SimpleSelectStatement;
import xw.data.eaql.query.eval.PojoEvaluator;
import xw.data.eaql.query.eval.SimplePojoEvaluator;
import xw.data.eaql.query.util.ExprUtils;

/**
 * Query class that performs on filter and select query upon unstructured
 * Map/List/String/primitives based POJO, most likely a fresh POJO parsed
 * by JSON mapper, for which there is not a predefined schema.
 * <p/>
 * UnstructedQuery only deals with single POJO, not a record stream as EAQL
 * is meant for. Hence it only supports simple select statement without FROM
 * or GROUP_BY clause.
 * <p/>
 * Also, column alias in SELECT clause are ignored.
 * <p/>
 * Example usage:
 * <pre>
 final SimpleSelectStatement select = EAQL.parseSimpleSelectStatement(stmt);
 final UnstructuredQuery query = new UnstructuredQuery(select);
 final Object pojo = objectMapper.readValue(json, Map.class)
 final Object[] result = query.query(pojo);
 System.out.println(Arrays.toString(result));
 * </pre>
 *
 */
public class UnstructuredQuery {

    protected final SimpleSelectStatement stmt;
    protected final Expression[] selectedExprs;
    protected final Expression whereCondition;
    protected final PojoEvaluator<Object> evaluator;

    public UnstructuredQuery(SimpleSelectStatement stmt) {
        this.stmt = stmt;
        if (stmt.from != null) {
            throw new IllegalArgumentException("unstructured query doesn't expect a FROM clause: " + stmt);
        }
        if (stmt.groupBy != null) {
            throw new IllegalArgumentException("unstructured query doesn't expect a GROUP-BY clause: " + stmt);
        }

        this.selectedExprs = new Expression[stmt.select.resultColumns.size()];
        for (int i=0; i<selectedExprs.length; i++) {
            selectedExprs[i] = ExprUtils.reduce(stmt.select.resultColumns.get(i).e);
        }

        this.whereCondition = (stmt.where == null)? LiteralConstants.literalTrue: stmt.where.e;
        this.evaluator = new SimplePojoEvaluator();
    }

    public SimpleSelectStatement getModel() {
        return stmt;
    }

    /**
     * Extract selected expressions from a unstructured POJO as an object array
     *
     * @param o the Map/List/String/primitives based POJO
     * @return Arrays of results evaluated by selected expressions, or null if
     *  where-condition is not satisfied.
     */
    public Object[] query(Object o) {

        final Object pred = evaluator.eval(whereCondition, o);

        if (! (pred instanceof Boolean)) {
            throw new RuntimeException("where condition '" + stmt.where +
                    "' doesn't evaluate into a boolean result: " + o);
        }

        if (pred.equals(false)) {
            return null;
        }

        final Object[] result = new Object[selectedExprs.length];
        for (int i=0; i<selectedExprs.length; i++) {
            result[i] = evaluator.eval(selectedExprs[i], o);
        }
        return result;
    }

    @Override
    public String toString() {
        return stmt.toString();
    }
}
