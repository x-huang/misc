package xw.data.eaql.query.eval;

import xw.data.eaql.model.expr.*;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.eval.udf.UDF;
import xw.data.eaql.query.eval.udf.UDFException;
import xw.data.eaql.query.eval.udf.UDFs;
import xw.data.eaql.util.Lazy;

import static xw.data.eaql.query.eval.Values.implicitConvert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 8/24/16.
 */
abstract class EvaluatorBase implements ExprVisitor<Object>
{
    private static EvaluationException newException(UnaryExpression e, Object v) {
        if (v == null) {
             return new EvaluationException(e.op + ": unexpected null operand", e.e);
        } else {
            return new EvaluationException(e.op + ": unexpected data type of " +
                    v + ": " + v.getClass().getName(), e.e);
        }
    }

    private static EvaluationException newException1(BinaryExpression e, Object v) {
        if (v == null) {
             return new EvaluationException(e.op + ": unexpected null left-operand", e.e1);
        } else {
            return new EvaluationException(e.op + ": unexpected data type of left-operand " +
                    v + ": " + v.getClass().getName(), e.e1);
        }
    }

    private static EvaluationException newException2(BinaryExpression e, Object v) {
        if (v == null) {
             return new EvaluationException(e.op + ": unexpected null right-operand", e.e2);
        } else {
            return new EvaluationException(e.op + ": unexpected data type of right-operand " +
                    v + ": " + v.getClass().getName(), e.e2);
        }
    }

    @Override
    public Object visitAddition(Addition e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 + (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 + (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 + (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 + (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitAnd(And e) {
        final Object v1 = e.e1.accept(this);
        if (v1 == null) return null;

        if (v1.equals(false)) {
            return false;
        } else if (! (v1 instanceof Boolean)) {
            throw newException1(e, v1);
        }

        final Object v2 = e.e2.accept(this);
        if (v2 == null) return null;

        if (! (v2 instanceof Boolean)) {
            throw newException2(e, v2);
        }
        return v2;
    }

    @Override
    public Object visitArrayMapElement(ArrayMapElement e) {
        final Object v1 = e.e1.accept(this);
        if (v1 == null) return null;

        final Object v2 = e.e2.accept(this);
        if (v2 == null) return null;

        if (v1 instanceof List) {
            final List list = (List) v1;
            final int index = ((Number) v2).intValue();
            return index >= 0 && index < list.size()? list.get(index): null;
        } else if (v1 instanceof Map) {
            return ((Map) v1).get(v2);
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitAsterisk(Asterisk e) {
        throw new EvaluationException("asterisk expression should be expanded before evaluation", e);
    }

    @Override
    public Object visitBetween(Between e) {
        final Object v = implicitConvert(e.e.accept(this));
        if (v == null) return null;

        if (! (v instanceof Comparable)) {
            throw newException(e, v);
        } else {
            final boolean b = Values.compare(v, e.lower) >= 0 && Values.compare(v, e.upper) <= 0;
            return e.not ^ b;
        }
    }

    @Override
    public Object visitCaseExprWhen(CaseExprWhen e) {
        final Object v = e.e.accept(this);
        if (v == null) return null;

        for (int i=0; i<e.numWhens(); i++) {
            final Object w = e.when(i).accept(this);
            if (w != null && v.equals(w)) {
                return e.then(i).accept(this);
            }
        }
        return e.else_.accept(this);
    }

    @Override
    public Object visitCaseWhen(CaseWhen e) {
        for (int i=0; i<e.numWhens(); i++) {
            final Object w = e.when(i).accept(this);
            if (w != null && w.equals(true)) {
                return e.then(i).accept(this);
            }
        }
        return e.else_.accept(this);
    }

    @Override
    public Object visitCast(Cast e) {
        final Object v = e.e.accept(this);
        if (v == null) return null;

        if (e.type == Types.STRING) {
            return v.toString();
        } else if (e.type == Types.BIGINT) {
            if (v instanceof Number) {
                return ((Number) v).longValue();
            } else if (v instanceof String) {
                try {
                    return Long.parseLong((String) v);
                } catch (NumberFormatException ignored) {
                    return null;
                }
            } else if (v instanceof Boolean) {
                return (Boolean) v ? 1L : 0L;
            } else if (v instanceof Date) {
                return ((Date) v).getTime();
            }
        } else if (e.type == Types.INT) {
            if (v instanceof Number) {
                return ((Number) v).intValue();
            } else if (v instanceof String) {
                try {
                    return Integer.parseInt((String) v);
                } catch (NumberFormatException ignored) {
                    return null;
                }
            } else if (v instanceof Boolean) {
                return (Boolean) v ? 1 : 0;
            } else if (v instanceof Date) {
                return (int) ((Date) v).getTime();
            }
        } else if (e.type == Types.SMALLINT) {
            if (v instanceof Number) {
                return ((Number) v).shortValue();
            } else if (v instanceof String) {
                try {
                    return Short.parseShort((String) v);
                } catch (NumberFormatException ignored) {
                    return null;
                }
            } else if (v instanceof Boolean) {
                return (Boolean) v ? (short)1 : (short)0;
            } else if (v instanceof Date) {
                return (short) ((Date) v).getTime();
            }
        } else if (e.type == Types.TINYINT) {
            if (v instanceof Number) {
                return ((Number) v).byteValue();
            } else if (v instanceof String) {
                try {
                    return Byte.parseByte((String) v);
                } catch (NumberFormatException ignored) {
                    return null;
                }
            } else if (v instanceof Boolean) {
                return (Boolean) v ? (byte)1 : (byte)0;
            } else if (v instanceof Date) {
                return (byte) ((Date) v).getTime();
            }
        } else if (e.type == Types.DOUBLE) {
            if (v instanceof Number) {
                return ((Number) v).doubleValue();
            } else if (v instanceof String) {
                try {
                    return Double.parseDouble((String) v);
                } catch (NumberFormatException ignored) {
                    return null;
                }
            } else if (v instanceof Boolean) {
                return (Boolean) v ? 1.0 : 0.0;
            } else if (v instanceof Date) {
                return (double) ((Date) v).getTime();
            }
        } else if (e.type == Types.FLOAT) {
            if (v instanceof Number) {
                return ((Number) v).floatValue();
            } else if (v instanceof String) {
                try {
                    return Float.parseFloat((String) v);
                } catch (NumberFormatException ignored) {
                    return null;
                }
            } else if (v instanceof Boolean) {
                return (Boolean) v ? 1f : 0f;
            } else if (v instanceof Date) {
                return (float) ((Date) v).getTime();
            }
        } else if (e.type == Types.BOOLEAN) {
            if (v instanceof Boolean) {
                return v;
            } else if (v instanceof Number) {
                return ((Number) v).doubleValue() != 0.0;
            } else if (v instanceof String) {
                return ! ((String) v).isEmpty();
            } else {
                return null;
            }
        }

        throw new EvaluationException("casting '" + v + "' to " + e.type + " is not supported", e);
    }

    @Override
    public Object visitDivision(Division e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v2 instanceof Long && ((long) v2) == 0) {
            return null;
        }
        if (v2 instanceof Double && ((double) v2) == 0.0) {
            return null;
        }

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 / (double) (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 / (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 / (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 / (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitEquals(Equals e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        return v1.equals(v2);
    }

    @Override
    public Object visitFunction(Function f) {
        final UDF udf = UDFs.get(f.name);
        if (udf == null) {
            throw new EvaluationException("error evaluating UDF '" + f.name + "': not defined", f);
        }
        final EvaluatorBase evaluator = this;
        final List<Lazy<Object>> lazyParams = new ArrayList<>();
        for (final Expression p: f.params) {
            lazyParams.add(new Lazy<Object>() {
                @Override
                protected Object createInstance() {
                    return p.accept(evaluator);
                }
            });
        }
        try {
            return udf.eval(lazyParams);
        } catch (UDFException e) {
            throw new EvaluationException("error evaluating UDF '" + f + "': " + e.getMessage(), f, e);
        }
    }

    @Override
    public Object visitGreaterThan(GreaterThan e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;
        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 > (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 > (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 > (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 > (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof String) {
            if (v2 instanceof String) {
                return ((String) v1).compareTo((String) v2) > 0;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitGreaterThanEquals(GreaterThanEquals e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 >= (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 >= (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 >= (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 >= (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof String) {
            if (v2 instanceof String) {
                return ((String) v1).compareTo((String) v2) >= 0;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitInSet(InSet e) {
        final Object v = implicitConvert(e.e.accept(this));
        if (v == null) return null;
        return e.not ^ e.set.contains(v);
    }

    @Override
    public Object visitIsNull(IsNull e) {
        final Object v = e.e.accept(this);
        final boolean b = v == null;
        return e.not ^ b;
    }

    @Override
    public Object visitLessThan(LessThan e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 < (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 < (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 < (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 < (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof String) {
            if (v2 instanceof String) {
                return ((String) v1).compareTo((String) v2) < 0;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitLessThanEquals(LessThanEquals e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 <= (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 <= (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 <= (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 <= (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof String) {
            if (v2 instanceof String) {
                return ((String) v1).compareTo((String) v2) <= 0;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitLike(Like e) {
        if (e.like == null) return null;
        final Object v = e.e.accept(this);
        if (v == null) return null;

        if (v instanceof String) {
            final String s = (String) v;
            final boolean b;
            if (e.startsWith) {
                if (e.endsWith) {
                    b = s.contains(e.match);
                } else {
                    b = s.endsWith(e.match);
                }
            } else {
                if (e.endsWith) {
                    b = s.startsWith(e.match);
                } else {
                    b = s.equals(e.match);
                }
            }
            return e.not ^ b;
        } else {
            throw newException(e, v);
        }
    }

    @Override
    public Object visitLiteralValue(LiteralValue e) {
        return e.value;
    }

    @Override
    public Object visitMultiplication(Multiplication e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 * (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 * (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 * (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 * (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

    @Override
    public Object visitNegation(Negation e) {
        final Object v = implicitConvert(e.e.accept(this));
        if (v == null) return null;

        if (v instanceof Long) {
            return - (long) v;
        } else if (v instanceof Double) {
            return - (double) v;
        } else {
            throw newException(e, v);
        }
    }

    @Override
    public Object visitNot(Not e) {
        final Object v = e.e.accept(this);
        if (v == null) return null;

        if (v instanceof Boolean) {
            return ! (Boolean) v;
        } else {
            throw newException(e, v);
        }
    }


    @Override
    public Object visitNotEquals(NotEquals e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        return ! v1.equals(v2);
    }

    @Override
    public Object visitOr(Or e) {
        final Object v1 = e.e1.accept(this);
        if (v1 == null) {
            return null;
        } else if ( v1.equals(true)) {
            return true;
        } else if (! (v1 instanceof Boolean)) {
            throw newException1(e, v1);
        }

        final Object v2 = e.e2.accept(this);
        if (v2 == null) {
            return null;
        } else if (! (v2 instanceof Boolean)) {
            throw newException2(e, v2);
        }
        return v2;
    }

    @Override
    public Object visitParenthetical(Parenthetical e) {
        return e.e.accept(this);
    }

    @Override
    public Object visitRLike(RLike e) {
        if (e.regex == null) return null;
        final Object v = e.e.accept(this);
        if (v == null) return null;

        if (v instanceof String) {
            final String s = (String) v;
            final boolean b = e.pattern.matcher(s).find();
            return e.not ^ b;
        } else {
            throw newException(e, v);
        }
    }

    @Override
    public Object visitSubtraction(Subtraction e) {
        final Object v1 = implicitConvert(e.e1.accept(this));
        if (v1 == null) return null;

        final Object v2 = implicitConvert(e.e2.accept(this));
        if (v2 == null) return null;

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return (long) v1 - (long) v2;
            } else if (v2 instanceof Double) {
                return (long) v1 - (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else if (v1 instanceof Double) {
            if (v2 instanceof Long) {
                return (double) v1 - (long) v2;
            } else if (v2 instanceof Double) {
                return (double) v1 - (double) v2;
            } else {
                throw newException2(e, v2);
            }
        } else {
            throw newException1(e, v1);
        }
    }

}
