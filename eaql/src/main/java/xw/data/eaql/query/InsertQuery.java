package xw.data.eaql.query;

import xw.data.eaql.model.stmt.InsertStatement;
import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataSource;

import java.io.IOException;

/**
 * Created by xhuang on 9/16/16.
 */
public class InsertQuery extends SQLQueryBase<InsertStatement> implements SQLWrite
{
    private final SelectQuery selectQuery;
    private boolean written = false;

    InsertQuery(QueryEnvironment env, InsertStatement stmt) {
        super(env, stmt);
        this.selectQuery = new SelectQuery(env, stmt.selectStmt);
    }

    @Override
    public Schema getSchema() {
        return selectQuery.getSchema();
    }

    @Override
    public void doWrite() throws IOException {
        if (written) {
            return;
        }

        final String ref = stmt.destination.getDataReference();
        final Schema schema = selectQuery.outputSchema;
        try (final DataSink out = env.getDataSink(ref, schema);
             final DataSource in = selectQuery.getDataSource()) {
            out.initialize();
            in.initialize();
            while (true) {
                final Object[] row = in.read();
                if (row == null) {
                    break;
                }
                out.write(row);
            }
        } finally {
            written = true;
        }
    }
}
