/*
 * Copyright (c) 2017. Xiaowan Huang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package xw.data.eaql.query.eval;

import xw.data.eaql.model.expr.*;
import java.util.Map;

public class SimplePojoEvaluator extends EvaluatorBase implements PojoEvaluator<Object>
{
    private static final String DEFAULT_ROOT_OBJECT_NAME = "_";

    private final String rootObjectName;
    private transient Object root = null;

    public SimplePojoEvaluator() {
        this.rootObjectName = DEFAULT_ROOT_OBJECT_NAME;
    }

    public SimplePojoEvaluator(String rootName) {
        this.rootObjectName = rootName;
    }

    private static EvaluationException newException(Expression e) {
        String msg = "cannot evaluate " + e.getClass().getSimpleName() + " expression '"
                + e + "' in simple POJO evaluator";
        return new EvaluationException(msg, e);
    }

    @Override
    public Object visitColumnValue(ColumnValue e) {
        if (e.column.equals(rootObjectName)) {
            return root;
        } else {
            throw new EvaluationException("unknown root object name: '" + e.column
                    + "', use predefined '" + rootObjectName + "' instead", e);
        }
    }

    @Override
    public Object visitColumnValueIndexed(ColumnValueIndexed e) {
        throw newException(e);
    }

    @Override
    public Object visitStructField(StructField e) {
        final Object v = e.e.accept(this);
        return v == null? null: ((Map) v).get(e.field);
    }

    @Override
    public Object visitStructFieldIndexed(StructFieldIndexed e) {
        throw newException(e);
    }

    @Override
    public Object eval(Expression e, Object pojo) {
        this.root = pojo;
        return e.accept(this);
    }
}


/*
// passed the following function test:

import com.google.gson._
import xw.data.eaql.model.`type`._
import xw.data.eaql.query.eval._
import xw.data.eaql.model.expr._
import xw.data.eaql.EAQL

val pojo = new Gson().fromJson(
  """
    |{"widget": {
    |    "debug": "on",
    |    "window": {
    |        "title": "Sample Konfabulator Widget",
    |        "name": "main_window",
    |        "width": 500,
    |        "height": 500
    |    },
    |    "image": {
    |        "src": "Images/Sun.png",
    |        "name": "sun1",
    |        "hOffset": 250,
    |        "vOffset": 250,
    |        "alignment": "center"
    |    },
    |    "text": {
    |        "data": "Click Here",
    |        "size": 36,
    |        "style": "bold",
    |        "name": "text1",
    |        "hOffset": 250,
    |        "vOffset": 100,
    |        "alignment": "center",
    |        "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
    |    }
    |}}
    |
  """.stripMargin, classOf[java.util.Map[String, Object]])
val evaluator = new SimplePojoEvaluator()
val desc = """
  |concat(_.widget.image.src, "@", _.widget.window.name,
  |   ": area=", (_.widget.window.width * _.widget.window.height))
""".stripMargin
val imageCentered = """
    |_.widget.image.alignment = 'center'
  """.stripMargin
val windowSizeGt640x480 =
  """
    | _.widget.window.width > 640 AND _.widget.window.height > 480
  """.stripMargin
val hasTextMouseUpEvent = """
    |_.widget.text.onMouseUp IS NOT NULL
  """.stripMargin

val exprs = Array(desc, imageCentered, windowSizeGt640x480, hasTextMouseUpEvent)
val result = exprs map { e: String =>
  evaluator.eval(EAQL.parseExpression(e), pojo)
}

println("[expr]\n" + exprs.mkString("\n"))
println("[result]\n" + result.mkString("\n"))

*/