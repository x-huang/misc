package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 8/18/2015.
 */
public class UDAF_sum_double implements UDAF<Double>
{
    private double sum = 0.0;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        sum += ((Number) value).doubleValue();
    }

    @Override
    public Double result() {
        return sum;
    }

    @Override
    public DataType returnType() {
        return Types.DOUBLE;
    }
}
