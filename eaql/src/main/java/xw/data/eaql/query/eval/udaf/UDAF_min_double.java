package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAF_min_double implements UDAF<Double>
{
    private double min = Double.MAX_VALUE;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        double d = ((Number) value).doubleValue();
        if (d < min) {
            min = d;
        }
    }

    @Override
    public Double result() {
        return (min == Double.MAX_VALUE)? null: min;
    }

    @Override
    public DataType returnType() {
        return Types.DOUBLE;
    }
}
