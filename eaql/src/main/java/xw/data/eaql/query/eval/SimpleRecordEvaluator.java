/*
 * Copyright (c) 2017. Xiaowan Huang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package xw.data.eaql.query.eval;

import xw.data.eaql.model.expr.*;
import xw.data.eaql.query.eval.udaf.UDAFs;
import java.util.List;

/**
 * Created by xhuang on 8/24/16.
 */
public class SimpleRecordEvaluator extends EvaluatorBase implements RecordEvaluator
{
    private transient Object[] record = null;

    private static EvaluationException newException(Expression e) {
        String msg = "cannot evaluate " + e.getClass().getSimpleName() + " expression '"
                + e + "' in simple record evaluator, "
                + "probably you forgot to replace it with indexed expression";
        return new EvaluationException(msg, e);
    }

    @Override
    public Object visitColumnValue(ColumnValue e) {
        throw newException(e);
    }

    @Override
    public Object visitStructField(StructField e) {
        throw newException(e);
    }

    @Override
    public Object visitFunction(Function f) {
        if (UDAFs.isUDAF(f)) {
            String msg = "cannot evaluate UDAF '" + f.name + "' in simple record evaluator, " +
                    "probably you used it in wrong step in a query";
            throw new EvaluationException(msg, f);
        }
        return super.visitFunction(f);
    }

    @Override
    public Object visitColumnValueIndexed(ColumnValueIndexed e) {
        return record[e.index];
    }

    @Override
    public Object visitStructFieldIndexed(StructFieldIndexed e) {
        final Object v = record[e.columnIndex];
        return v == null? null: ((List) v).get(e.fieldIndex);
    }

    @Override
    public final Object eval(Expression e, Object[] record) {
        this.record = record;
        return e.accept(this);
    }

}
