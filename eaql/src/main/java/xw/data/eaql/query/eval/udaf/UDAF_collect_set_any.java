package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by xhuang on 8/18/2015.
 */
public class UDAF_collect_set_any implements UDAF<List<Object>>
{
    protected Set<Object> set = new HashSet<>();
    private final DataType elemType;

    public UDAF_collect_set_any(DataType elemType) {
        this.elemType = elemType;
    }

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        set.add(value);
    }

    @Override
    public List<Object> result() {
        return new ArrayList<>(set);
    }

    @Override
    public DataType returnType() {
        return Types.newArrayType(elemType);
    }
}
