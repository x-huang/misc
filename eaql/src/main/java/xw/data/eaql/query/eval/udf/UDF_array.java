package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.util.ArrayLiteral;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_array implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSizeNoLessThan(paramTypes, 1);
        final DataType valueType = UDFs.getParametersTypeAssertingSame(paramTypes);
        return Types.newArrayType(valueType);
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final List<Object> list = new ArrayLiteral<>();
        for (Lazy<Object> lazyParam : lazyParams) {
            list.add(lazyParam.get());
        }
        return list;
    }
}
