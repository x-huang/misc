package xw.data.eaql.query;

import xw.data.eaql.model.stmt.DescribeStatement;
import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/27/16.
 */
public class DescribeQuery extends SQLQueryBase<DescribeStatement> implements SQLRead
{
    DescribeQuery(QueryEnvironment env, DescribeStatement stmt) {
        super(env, stmt);
    }

    @Override
    public Schema getSchema() {
        return env.getSchema(stmt.source.getDataReference());
    }

    @Override
    public DataSource getDataSource() {
        final Schema schema = env.getSchema(stmt.source.getDataReference());
        final List<Object[]> metadata = new ArrayList<>();
        for (String col: schema.columns()) {
            metadata.add(new Object[] {col, schema.typeOf(col).toString()} );
        }
        return DataStreams.wrapRecordList(metadata);
    }

}
