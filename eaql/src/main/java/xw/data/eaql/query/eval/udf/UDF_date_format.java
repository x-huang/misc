package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.DateTimeUtils;
import xw.data.eaql.util.Lazy;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by xhuang on 8/31/16.
 */
class UDF_date_format implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 2);
        UDFs.expectParameterType(paramTypes, 0, Types.STRING, Types.DATE, Types.TIMESTAMP);
        UDFs.expectParameterType(paramTypes, 1, Types.STRING);
        return Types.STRING;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v1 = lazyParams.get(0).get();
        if (v1 == null) {
            return null;
        }
        final Object v2 = lazyParams.get(1).get();
        if (v2 == null) {
            return null;
        }

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat((String) v2);

            final Date date = DateTimeUtils.to_date(v1);
            return sdf.format(date);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
