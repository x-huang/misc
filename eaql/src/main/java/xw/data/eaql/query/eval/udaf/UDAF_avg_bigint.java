package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAF_avg_bigint implements UDAF<Double>
{
    private long sum;
    private long count;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        sum += ((Number) value).longValue();
        count ++;
    }

    @Override
    public Double result() {
        return count == 0? null: ((double) sum / (double) count);
    }

    @Override
    public DataType returnType() {
        return Types.DOUBLE;
    }
}
