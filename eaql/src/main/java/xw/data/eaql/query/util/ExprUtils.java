package xw.data.eaql.query.util;

import xw.data.eaql.model.clause.SelectClause;
import xw.data.eaql.model.expr.*;
import xw.data.eaql.model.term.ResultColumn;
import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.StructType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.eval.SimpleEvaluator;
import xw.data.eaql.query.eval.Values;
import xw.data.eaql.query.eval.udaf.UDAFs;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.semantic.SemanticException;

import java.util.*;

import static xw.data.eaql.model.expr.Expressions.*;

/**
 * Created by xhuang on 8/26/16.
 */
public class ExprUtils
{
    static class ColumnIndexReplacer implements ExprReplacer
    {
        public final Schema schema;

        public ColumnIndexReplacer(Schema schema) {
            this.schema = schema;
        }

        @Override
        public Expression replace(Expression e) {
            /* if (e instanceof ArrayMapElement) {
                final ArrayMapElement indexing = (ArrayMapElement) e;
                if (! Expressions.isColumnValue(indexing.column)) {
                    throw new SemanticException("invalid array/map indexing expression '" + e
                            + "': expecting a column name, got "
                            + indexing.column.getClass().getSimpleName());
                }
                final String column = ((ColumnValue) indexing.column).column;
                final DataType columnType = schema.typeOf(column);
                final int colIndex = schema.indexOf(column);
                final DataType keyType = TypeInferer.inferResultType(schema, indexing.indexKey);
                if (Types.isArrayType(columnType)) {
                    if (! Types.isIntegerNumberType(keyType)) {
                        throw new SemanticException("invalid array indexing expression '" + e
                            + "': expecting an integer type index key, got " + keyType);
                    }
                    return new ArrayElementIndexed(colIndex, indexing.indexKey);
                } else if (Types.isMapType(columnType)) {
                    if (! Types.isPrimitiveType(keyType)) {
                        throw new SemanticException("invalid map indexing expression '" + e
                            + "': expecting a primitive type map key, got " + keyType);
                    }
                    return new MapElementIndexed(colIndex,indexing.indexKey);
                } else {
                    throw new SemanticException("invalid array/map indexing expression '" + e
                            + "': expecting array or map type column, got " + columnType);
                }
            } else */
            if (e instanceof StructField ) {
                final StructField sf = (StructField) e;
                if (! Expressions.isColumnValue(sf.e)) {
                    throw new SemanticException("invalid struct field expression '" + e
                            + "': expecting a column name, got " + sf.e.getClass().getSimpleName());
                }
                final String column = ((ColumnValue) sf.e).column;
                final DataType columnType = schema.typeOf(column);
                if (! Types.isStructType(columnType)) {
                    throw new SemanticException("invalid struct field expression '" + e
                        + "': expecting struct type column, got " + columnType);
                }
                final StructType structType = (StructType) columnType;
                final int colIndex = schema.indexOf(column);
                final int fieldIndex = getStructFieldIndex(structType, sf.field);
                if (fieldIndex < 0) {
                    throw new SemanticException("invalid struct field expression '" + e
                        + "': field name '" + sf.field + "' not found");
                }
                return new StructFieldIndexed(colIndex, fieldIndex);
            } else if (Expressions.isColumnValue(e)) {
                return new ColumnValueIndexed(schema.indexOf(((ColumnValue) e).column));
            } else {
                return null;
            }
        }
    }

    public static int getStructFieldIndex(StructType structType, String fieldName) {
        for (int i=0; i<structType.fields.size(); i++) {
            if (structType.fields.get(i).name.equals(fieldName)) {
                return i;
            }
        }
        return -1;
    }

    public static String toString(Expression[] exprs) {
        final StringBuilder sb =  new StringBuilder();
        String delimiter = "[";
        for (Expression e: exprs) {
            sb.append(delimiter).append(e);
            delimiter = ", ";
        }
        return sb.append("]").toString();
    }

    public static Expression indexColumns(final Schema schema, Expression e) {
        final ExprDuplicator duplicator = new ExprDuplicator(new ColumnIndexReplacer(schema));
        return duplicator.duplicate(e);
    }

    public static Expression[] indexColumns(final Schema schema, Expression[] exprs) {
        final Expression[] indexed = new Expression[exprs.length];
        final ExprDuplicator duplicator = new ExprDuplicator(new ColumnIndexReplacer(schema));
        for (int i=0; i<exprs.length; i++) {
            indexed[i] = duplicator.duplicate(exprs[i]);
        }
        return indexed;
    }

    public static Expression[] indexColumns(final Schema schema, List<Expression> list) {
        final Expression[] indexed = new Expression[list.size()];
        final ExprDuplicator duplicator = new ExprDuplicator(new ColumnIndexReplacer(schema));
        for (int i=0; i<indexed.length; i++) {
            indexed[i] = duplicator.duplicate(list.get(i));
        }
        return indexed;
    }

    public static UnaryExpression replaceUnaryOperand(UnaryExpression e, Expression e_) {
        if (e instanceof Between) {
            return new Between(e_, ((Between) e).not, ((Between) e).lower, ((Between) e).upper);
        } else if (e instanceof Cast) {
            return new Cast(e_, ((Cast) e).type);
        } else if (e instanceof InSet) {
            return new InSet(e_, ((InSet) e).not, ((InSet) e).set);
        } else if (e instanceof IsNull) {
            return new IsNull(e_, ((IsNull) e).not);
        } else if (e instanceof Like) {
            return new Like(e_, ((Like) e).not,((Like) e).like);
        } else if (e instanceof RLike) {
            return new RLike(e_, ((RLike) e).not,((RLike) e).regex);
        } else if (e instanceof Negation) {
            return new Negation(e_);
        } else if (e instanceof Not) {
            return new Not(e_);
        } else if (e instanceof Parenthetical) {
            return new Parenthetical(e_);
        } else if (e instanceof StructField) {
            return new StructField(e_, ((StructField) e).field);
        } else {
            throw new RuntimeException("unexpected unary expression: " + e_);
        }
    }

    public static BinaryExpression replaceBinaryOperands(BinaryExpression e, Expression e1_, Expression e2_) {
        if (e instanceof Addition) {
            return new Addition(e1_, e2_);
        } else if (e instanceof And) {
            return new And(e1_, e2_);
        } else if (e instanceof Division) {
            return new Division(e1_, e2_);
        } else if (e instanceof Equals) {
            return new Equals(e1_, e2_);
        } else if (e instanceof GreaterThan) {
            return new GreaterThan(e1_, e2_);
        } else if (e instanceof GreaterThanEquals) {
            return new GreaterThanEquals(e1_, e2_);
        } else if (e instanceof LessThan) {
            return new LessThan(e1_, e2_);
        } else if (e instanceof LessThanEquals) {
            return new LessThanEquals(e1_, e2_);
        } else if (e instanceof Multiplication) {
            return new Multiplication(e1_, e2_);
        } else if (e instanceof NotEquals) {
            return new NotEquals(e1_, e2_);
        } else if (e instanceof Or) {
            return new Or(e1_, e2_);
        } else if (e instanceof Subtraction) {
            return new Subtraction(e1_, e2_);
        } else if (e instanceof ArrayMapElement) {
            return new ArrayMapElement(e1_, e2_);
        } else {
            throw new RuntimeException("unexpected binary expression: " + e);
        }
    }

    public static Expression reduce(Expression e) {
        if (e instanceof LiteralValue || e instanceof ColumnValue) {
            return e;
        } else if (e instanceof UnaryExpression) {
            if (e instanceof Between) {
                final int cmp = Values.compare(((Between) e).lower, ((Between) e).upper);
                if (cmp > 0) {
                    return ((Between) e).not? LiteralConstants.literalTrue: LiteralConstants.literalFalse;
                }
            }

            final Expression e_ = reduce(((UnaryExpression) e).e);
            if (e instanceof Parenthetical) {
                return e_;
            }
            final UnaryExpression u = replaceUnaryOperand((UnaryExpression) e, e_);
            if (isLiteralValue(e_)) {
                return new LiteralValue(u.accept(SimpleEvaluator.get()));
            } else {
                return u;
            }
        } else if (e instanceof BinaryExpression) {
            final Expression e1_ = reduce(((BinaryExpression) e).e1);
            final Expression e2_ = reduce(((BinaryExpression) e).e2);
            if (e instanceof And) {
                if (isLiteralFalse(e1_) || isLiteralFalse(e2_)) {
                    return LiteralConstants.literalFalse;
                } else if (isLiteralTrue(e1_)) {
                    return e2_;
                } else if (isLiteralTrue(e2_)) {
                    return e1_;
                }
            }
            if (e instanceof Or) {
                if (isLiteralTrue(e1_) || isLiteralTrue(e2_)) {
                    return LiteralConstants.literalTrue;
                } else if (isLiteralFalse(e1_)) {
                    return e2_;
                } else if (isLiteralFalse(e2_)) {
                    return e1_;
                }
            }
            final BinaryExpression b = replaceBinaryOperands((BinaryExpression)e, e1_, e2_);
            if (isLiteralValue(e1_) && isLiteralValue(e2_)) {
                return new LiteralValue(b.accept(SimpleEvaluator.get()));
            } else {
                return b;
            }
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            final Function f_ = new Function(f.name);
            boolean allLiteralParams = true;
            for (Expression p: f.params) {
                final Expression p_ = reduce(p);
                if (! isLiteralValue(p_)) {
                    allLiteralParams = false;
                }
                f_.addParam(p_);
            }
            if (! UDAFs.isUDAF(f_) && allLiteralParams) {
                // pre-evaluate function
                return new LiteralValue(f_.accept(SimpleEvaluator.get()));
            } else {
                return f_;
            }
        } else if (e instanceof CaseExprWhen) {
            final CaseExprWhen cew = (CaseExprWhen) e;
            final CaseExprWhen cew_ = new CaseExprWhen(reduce(cew.e), reduce(cew.else_));
            for (int i=0; i<cew.numWhens(); i++) {
                final Expression when_ = reduce(cew.when(i));
                final Expression then_ = reduce(cew.then(i));
                cew_.addWhenThenPair(when_, then_);
            }
            return cew_;
        } else if (e instanceof CaseWhen) {
            final CaseWhen cw = (CaseWhen) e;
            final CaseWhen cw_ = new CaseWhen(reduce(cw.else_));
            for (int i=0; i<cw.numWhens(); i++) {
                final Expression when_ = reduce(cw.when(i));
                final Expression then_ = reduce(cw.then(i));
                cw_.addWhenThenPair(when_, then_);
            }
            return cw_;
        } else if (e instanceof Asterisk) {
            return e;
            // throw new IllegalStateException("asterisk should have been expanded before reducing");
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }


    public static void collectAggregateFunctions(Expression e, Map<String, Function> map) {
        if (e instanceof LiteralValue || e instanceof ColumnValue
                || e instanceof Asterisk || e instanceof CaseWhen) {
            // do nothing
        } else if (e instanceof CaseExprWhen) {
            collectAggregateFunctions(((CaseExprWhen) e).e, map);
        } else if (e instanceof UnaryExpression) {
            collectAggregateFunctions(((UnaryExpression) e).e, map);
        } else if (e instanceof BinaryExpression) {
            collectAggregateFunctions(((BinaryExpression) e).e1, map);
            collectAggregateFunctions(((BinaryExpression) e).e2, map);
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            if (UDAFs.isUDAF(f)) {
                map.put(f.toString(), f);
            } else {
                for (Expression p: f.params) {
                    collectAggregateFunctions(p, map);
                }
            }
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }

    public static Map<String, Function> getAllAggregateFunctions(SelectClause select) {
        final Map<String, Function> aggregators = new HashMap<>();
        for (ResultColumn rc: select.resultColumns) {
            collectAggregateFunctions(rc.e, aggregators);
        }
        return aggregators;
    }

    private static void collectEminentColumnNames(Expression e, Set<String> set) {
        if (e instanceof LiteralValue || e instanceof Asterisk) {
            // do nothing
        } else if (e instanceof ColumnValue) {
            set.add(((ColumnValue) e).column);
        } else if (e instanceof UnaryExpression) {
            collectEminentColumnNames(((UnaryExpression) e).e, set);
        } else if (e instanceof BinaryExpression) {
            collectEminentColumnNames(((BinaryExpression) e).e1, set);
            collectEminentColumnNames(((BinaryExpression) e).e2, set);
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            if (! UDAFs.isUDAF(f)) {
                for (Expression p: f.params) {
                    collectEminentColumnNames(p, set);
                }
            }
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }

    public static Set<String> getAllEminentColumnNames(SelectClause select) {
        final Set<String> columnNames = new HashSet<>();
        for (ResultColumn rc: select.resultColumns) {
            collectEminentColumnNames(rc.e, columnNames);
        }
        return columnNames;
    }

    public static Set<String> getAllEminentColumnNames(Expression e) {
        final Set<String> columnNames = new HashSet<>();
        collectEminentColumnNames(e, columnNames);
        return columnNames;
    }

    public static Set<String> getAllColumnNames(Expression e) {
        final Set<String> columnNames = new HashSet<>();
        collectColumnNames(e, columnNames);
        return columnNames;
    }

    public static Set<String> getAllColumnNames(SelectClause select) {
        final Set<String> columnNames = new HashSet<>();
        for (ResultColumn rc: select.resultColumns) {
            collectColumnNames(rc.e, columnNames);
        }
        return columnNames;
    }

    private static void collectColumnNames(Expression e, Set<String> set) {
        if (e instanceof LiteralValue || e instanceof Asterisk) {
            // do nothing
        } else if (e instanceof ColumnValue) {
            set.add(((ColumnValue) e).column);
        } else if (e instanceof UnaryExpression) {
            collectColumnNames(((UnaryExpression) e).e, set);
        } else if (e instanceof BinaryExpression) {
            collectColumnNames(((BinaryExpression) e).e1, set);
            collectColumnNames(((BinaryExpression) e).e2, set);
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            for (Expression p: f.params) {
                collectColumnNames(p, set);
            }
        } else {
            throw new RuntimeException("unexpected expression: " + e);
        }
    }

}
