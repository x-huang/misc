package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.PrimitiveType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.util.MapLiteral;
import xw.data.eaql.util.Lazy;

import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_map implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        if (paramTypes.size() % 2 != 0) {
            throw new UDFException("expect even number of parameters, got "
                    + paramTypes.size());
        }
        UDFs.expectParameterSizeNoLessThan(paramTypes, 2);

        final DataType keyType = UDFs.getParametersTypeAssertingSame(paramTypes, 0, 2);
        final DataType valueType = UDFs.getParametersTypeAssertingSame(paramTypes, 1, 2);
        return Types.newMapType((PrimitiveType) keyType, valueType);
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Map<Object, Object> map = new MapLiteral<>();
        for (int i=0; i<lazyParams.size(); i+=2) {
            final Object key = lazyParams.get(i).get();
            final Object value = lazyParams.get(i+1).get();
            map.put(key, value);
        }
        return map;
    }
}
