package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.expr.Function;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by xhuang on 8/30/16.
 */
public class UDAFs
{
    private static Set<String> udafs = new HashSet<>();
    static {
        udafs.add("count");
        udafs.add("sum");
        udafs.add("min");
        udafs.add("max");
        udafs.add("avg");
        udafs.add("collect_set");
        udafs.add("collect_list");
    }

    public static boolean isUDAF(Function f) {
        if (udafs.contains(f.name.toLowerCase())) {
            f.assertNumParams(1);
            return true;
        } else {
            return false;
        }
    }

    public static List<String> getAll() {
        return new ArrayList<>(udafs);
    }
}
