package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 8/29/16.
 */
public interface UDF
{
    DataType inferReturnType(List<DataType> paramTypes) throws UDFException;
    Object eval(List<Lazy<Object>> lazyParams) throws UDFException;
}
