package xw.data.eaql.query.groupby;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.eaql.model.expr.*;
import xw.data.eaql.query.eval.udaf.UDAFFactory;
import xw.data.eaql.query.eval.udaf.UDAFs;
import xw.data.eaql.query.eval.udaf.UDAFVector;
import xw.data.eaql.query.util.TypeInferer;
import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.RecordProcessor;

import java.io.IOException;
import java.util.*;

/**
 * Created by xhuang on 7/20/2015.
 */
public class GroupBy
{
    private static final Logger logger = LoggerFactory.getLogger(GroupBy.class);

    public final Schema inputSchema;
    protected final DataSource inputStream;
    public final Schema outputSchema;

    protected final List<String> groupByKeys;
    protected final List<Function> aggregateFunctions;
    protected final RecordProcessor keyExtractor;
    protected final RecordProcessor valueExtractor;
    protected final UDAFFactory valuesFactory;

    public GroupBy(Schema inputSchema, DataSource inputStream,
                   List<String> groupByKeys, List<Function> aggregateFunctions) {
        this.inputSchema = inputSchema;
        this.inputStream = inputStream;

        this.groupByKeys = groupByKeys;
        this.aggregateFunctions = aggregateFunctions;
        logger.debug("group by keys:{}, aggregators: {}, ", groupByKeys, aggregateFunctions);

        this.keyExtractor = IndexedExtractor.fromColumnList(inputSchema, groupByKeys);
        this.valueExtractor = ExpressionValueExtractor.fromParameterList(inputSchema, aggregateFunctions);

        this.valuesFactory = new UDAFFactory(inputSchema, aggregateFunctions);

        this.outputSchema = buildOutputSchema();
        logger.debug("input schema: <{}>, output schema: <{}>", inputSchema, outputSchema);
    }

    private Schema buildOutputSchema() {
        final Schema.Builder builder = Schema.newBuilder();
        for (String col: groupByKeys) {
            builder.addColumn(col, inputSchema.typeOf(col));
        }
        for (Function f: aggregateFunctions) {
            // use function string as column name
            builder.addColumn(f.toString(), TypeInferer.inferResultType(inputSchema, f));
        }
        return builder.build();
    }

    public DataSource getOutputStream() {

        final int keysLen = groupByKeys.size();
        final int valuesLen = aggregateFunctions.size();

        return new DataSource() {
            final Map<GroupByKeys, UDAFVector> map = new HashMap<>();
            Iterator<GroupByKeys> keyIterator;

            @Override
            public void initialize() throws IOException {
                inputStream.initialize();
                while (true) {
                    final Object[] row = inputStream.read();
                    if (row == null) {
                        break;
                    }
                    final GroupByKeys keys = new GroupByKeys(keyExtractor.process(row));
                    if (! map.containsKey(keys)) {
                        map.put(keys, valuesFactory.newValues());
                    }
                    map.get(keys).aggregate(valueExtractor.process(row));
                }

                keyIterator = map.keySet().iterator();
            }

            @Override
            public Object[] read() throws IOException {
                if (keyIterator.hasNext()) {
                    final GroupByKeys keys = keyIterator.next();
                    final UDAFVector values = map.get(keys);
                    final Object[] result = new Object[keysLen + valuesLen];
                    System.arraycopy(keys.getKeys(), 0, result, 0, keysLen);
                    System.arraycopy(values.result(), 0, result, keysLen, valuesLen);
                    return result;
                } else {
                    return null;
                }
            }

            @Override
            public void close() throws IOException {
                inputStream.close();
            }

            @Override
            public String toString() {
                return "group_by(" + inputStream.toString() + ")";
            }
        };
    }

    public Expression[] indexExpressions(Expression[] exprs) {
        final Expression[] rewritten = new Expression[exprs.length];
        final ExprDuplicator rewriter = new ExprDuplicator(new ExprReplacer() {
            @Override
            public Expression replace(Expression e) {
                if (e instanceof ColumnValue) {
                    final int index = outputSchema.indexOf(((ColumnValue) e).column);
                    return new ColumnValueIndexed(index);
                } else if (e instanceof Function && UDAFs.isUDAF((Function) e)) {
                    final int index = outputSchema.indexOf(e.toString());
                    return new ColumnValueIndexed(index);
                } else {
                    return null;
                }
            }
        });
        for (int i=0; i<exprs.length; i++) {
            rewritten[i] = rewriter.duplicate(exprs[i]);
        }
        return rewritten;
    }
}
