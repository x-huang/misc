package xw.data.eaql.query;

import xw.data.eaql.model.stmt.SelectOrValuesStatement;

/**
 * Created by xhuang on 9/1/16.
 */
interface SelectOrValuesQuery<T extends SelectOrValuesStatement> extends SQLQuery<T>, SQLRead
{
}
