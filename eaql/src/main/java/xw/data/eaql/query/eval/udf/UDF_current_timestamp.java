package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_current_timestamp implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 0);
        return Types.TIMESTAMP;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        return new Timestamp(System.currentTimeMillis());
    }
}
