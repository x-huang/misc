package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.Function;
import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.util.TypeInferer;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.semantic.SemanticException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/19/2015.
 */
public class UDAFFactory
{
    private final Schema schema;
    private final List<Function> aggregators;

    public UDAFFactory(Schema schema, List<Function> aggregators) {
        this.schema = schema;
        this.aggregators = aggregators;
    }

    public static UDAF createUDAF(Function f, List<DataType> paramTypes) {
        f.assertNumParams(1);
        final DataType paramType = paramTypes.get(0);
        switch (f.name.toLowerCase()) {
            case "count": {
                return new UDAF_count();
            }
            case "sum":
                if (Types.isIntegerNumberType(paramType)) {
                    return new UDAF_sum_bigint();
                } else if (Types.isFloatNumberType(paramType)) {
                   return new UDAF_sum_double();
                } else {
                    throw new SemanticException("illegal parameter type of '" + f + "': " + paramType);
                }
            case "min":
                if (Types.isIntegerNumberType(paramType)) {
                    return new UDAF_min_bigint();
                } else if (Types.isFloatNumberType(paramType)) {
                   return new UDAF_min_double();
                } else if (paramType == Types.STRING) {
                    return new UDAF_min_string();
                } else {
                    throw new SemanticException("illegal parameter type of '" + f + "': " + paramType);
                }
            case "max":
                if (Types.isIntegerNumberType(paramType)) {
                    return new UDAF_max_bigint();
                } else if (Types.isFloatNumberType(paramType)) {
                   return new UDAF_max_double();
                } else if (paramType == Types.STRING) {
                    return new UDAF_max_string();
                } else {
                    throw new SemanticException("illegal parameter type of '" + f + "': " + paramType);
                }
            case "avg":
                if (Types.isIntegerNumberType(paramType)) {
                    return new UDAF_avg_bigint();
                } else if (Types.isFloatNumberType(paramType)) {
                   return new UDAF_avg_double();
                } else {
                    throw new SemanticException("illegal parameter type of '" + f + "': " + paramType);
                }
            case "collect_set": {
                return new UDAF_collect_set_any(paramType);
            }
            case "collect_list": {
                return new UDAF_collect_list_any(paramType);
            }
            default:
                throw new SemanticException("unknown aggregate function: '" + f + "'");
        }
    }

    public UDAF create(Function f) {

        final List<DataType> paramTypes = new ArrayList<>();
        for (Expression param: f.params) {
            paramTypes.add(TypeInferer.inferResultType(schema, param));
        }
        return createUDAF(f, paramTypes);
    }

    public UDAFVector newValues() {
        final UDAF[] udafs = new UDAF[aggregators.size()];
        for (int i=0; i< udafs.length; i++) {
            udafs[i] = create(aggregators.get(i));
        }
        return new UDAFVector(udafs);
    }
}
