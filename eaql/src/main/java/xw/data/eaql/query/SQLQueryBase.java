package xw.data.eaql.query;

import xw.data.eaql.model.stmt.SQLStatement;

/**
 * Created by xhuang on 8/27/16.
 */
abstract class SQLQueryBase<T extends SQLStatement> implements SQLQuery<T>
{
    protected final QueryEnvironment env;
    protected final T stmt;

    SQLQueryBase(QueryEnvironment env, T stmt) {
        this.env = env;
        this.stmt = stmt;
    }

    @Override
    public QueryEnvironment getEnvironment() {
        return env;
    }

    @Override
    public T getModel() {
        return stmt;
    }

}
