package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.DateTimeUtils;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 8/31/16.
 */
class UDF_to_date implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 1);
        UDFs.expectParameterType(paramTypes, 0, Types.STRING, Types.TIMESTAMP);
        return Types.DATE;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v = lazyParams.get(0).get();
        try {
            return DateTimeUtils.to_date(v);
        } catch (IllegalArgumentException e) {
            throw new UDFException("expects string or timestamp parameter, got: "
                    + v.getClass().getName());
        }
    }
}
