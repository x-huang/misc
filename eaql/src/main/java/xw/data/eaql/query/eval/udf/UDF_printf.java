package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 8/31/16.
 */
class UDF_printf implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSizeNoLessThan(paramTypes, 1);
        return Types.STRING;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final String format = (String) lazyParams.get(0).get();
        final Object[] args = new Object[lazyParams.size() - 1];
        for (int i=0; i<args.length; i++) {
            args[i] = lazyParams.get(i+1).get();
        }
        return String.format(format, args);
    }
}
