package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 8/18/2015.
 */
public class UDAF_count implements UDAF<Long>
{
    private long count = 0;

    @Override
    public void aggregate(Object value) {
        if (value != null)
            count += 1;
    }

    @Override
    public Long result() {
        return count;
    }

    @Override
    public DataType returnType() {
        return Types.BIGINT;
    }
}
