package xw.data.eaql.query.eval.udaf;


import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAF_max_double implements UDAF<Double>
{
    private double max = Double.MIN_VALUE;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        double d = ((Number) value).doubleValue();
        if (d > max) {
            max = d;
        }
    }

    @Override
    public Double result() {
        return (max == Double.MIN_VALUE)? null: max;
    }

    @Override
    public DataType returnType() {
        return Types.DOUBLE;
    }
}
