package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 8/30/16.
 */
class UDF_length implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 1);
        UDFs.expectParameterType(paramTypes, 0, Types.STRING);
        return Types.INT;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v = lazyParams.get(0).get();
        if (v == null) return null;
        final String s = (String) v;
        return s.length();
    }
}
