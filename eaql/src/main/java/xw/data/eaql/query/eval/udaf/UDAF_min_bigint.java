package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAF_min_bigint implements UDAF<Long>
{
    private long min = Long.MAX_VALUE;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        long l = ((Number) value).longValue();
        if (l < min) {
            min = l;
        }
    }

    @Override
    public Long result() {
        return (min == Long.MAX_VALUE)? null: min;
    }

    @Override
    public DataType returnType() {
        return Types.BIGINT;
    }
}
