package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.List;

/**
 * Created by xhuang on 8/30/16.
 */
class UDF_coalesce implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSizeNoLessThan(paramTypes, 2);
        for (DataType type: paramTypes) {
            if (type != Types.VOID) {
                return type;
            }
        }
        throw new UDFException("all null parameters");
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        for (Lazy<Object> lazy: lazyParams) {
            if (lazy.get() != null) {
                return lazy.get();
            }
        }
        return null;
    }
}
