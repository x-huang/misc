package xw.data.eaql.query.util;

import xw.data.eaql.model.clause.SelectClause;
import xw.data.eaql.model.term.ResultColumn;
import xw.data.eaql.query.eval.udaf.UDAFs;
import xw.data.eaql.model.stmt.SimpleSelectStatement;
import xw.data.eaql.model.type.DataType;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.model.expr.*;

import java.util.*;

/**
 * Created by xhuang on 8/16/16.
 */
public class QueryUtils
{
    public static SelectClause expandSelectClause(SimpleSelectStatement stmt, Schema inputSchema) {
        final SelectClause expanded = new SelectClause(stmt.select.distinct);
        int idx = 0;
        for (final ResultColumn rc: stmt.select.resultColumns) {
            if (rc.e instanceof Asterisk) {
                final List<String> targetColumns = (stmt.groupBy == null)? inputSchema.columns(): stmt.groupBy.columns;
                // expand all columns
                for (String col : targetColumns) {
                    expanded.addResultColumn(new ResultColumn(new ColumnValue(col), col));
                }
            } else {
                // final Expression simplified = ExprUtils.reduce(rc.e);
                final String alias;
                if (rc.alias != null) {
                    alias = rc.alias;
                } else if (rc.e instanceof ColumnValue) {
                    alias = ((ColumnValue) rc.e).column;
                } else {
                    alias = "_col" + idx;
                }
                expanded.addResultColumn(new ResultColumn(rc.e, alias));
            }
            idx += 1;
        }
        return expanded;
    }

    /*
    public static SelectDistinctClause expandSelectDistinctClause(SelectDistinctStatement stmt, Schema inputSchema) {
        final SelectDistinctClause expanded = new SelectDistinctClause();
        int idx = 0;
        for (final ResultColumn rc: stmt.select.resultColumns) {
            if (rc.e instanceof Asterisk) {
                // expand all columns
                for (String col : inputSchema.columns()) {
                    expanded.addResultColumn(new ResultColumn(new ColumnValue(col), col));
                }
            } else {
                // final Expression simplified = ExprUtils.reduce(rc.e);
                final String alias;
                if (rc.alias != null) {
                    alias = rc.alias;
                } else if (rc.e instanceof ColumnValue) {
                    alias = ((ColumnValue) rc.e).column;
                } else {
                    alias = "_col" + idx;
                }
                expanded.addResultColumn(new ResultColumn(rc.e, alias));
            }
            idx += 1;
        }
        return expanded;
    }
    */


    public static List<Expression> expandSelectedExpressions(SimpleSelectStatement stmt, Schema inputSchema) {
        final List<Expression> expanded = new ArrayList<>();
        for (ResultColumn rc: stmt.select.resultColumns) {
            final Expression e = rc.e;
            if (e instanceof Asterisk) {
                final List<String> targetColumns = (stmt.groupBy == null)? inputSchema.columns(): stmt.groupBy.columns;
                // expand all columns
                for (String col : targetColumns) {
                    expanded.add(new ColumnValue(col));
                }
            } else {
                expanded.add(ExprUtils.reduce(rc.e));
            }
        }
        return expanded;
    }

    public static String getDefaultColumnAlias(Expression e) {
        if (e instanceof Asterisk) {
            return null;
        } else if (e instanceof ColumnValue) {
            return ((ColumnValue) e).column;
        } else if (e instanceof Function) {
            final Function f = (Function) e;
            if (UDAFs.isUDAF(f)) {
                f.assertNumParams(1);
                return getDefaultColumnAlias(f.getParam(0));
            } else {
                if (f.params.size() == 1) {
                    return getDefaultColumnAlias(f.params.get(0));
                }
            }
        } else if (e instanceof Cast) {
            return getDefaultColumnAlias(((Cast) e).e);
        }
        return null;
    }

    public static Schema computeSelectClauseOutputSchema(Schema inputSchema, SelectClause select) {
        final Schema.Builder builder = Schema.newBuilder();
        int colIdx = 0;
        for (ResultColumn rc: select.resultColumns) {
            final Expression e = rc.e;
            if (e instanceof Asterisk) {
                // expand all columns
                for (String col: inputSchema.columns()) {
                    final DataType type = inputSchema.typeOf(col);
                    builder.addColumn(col, type);
                }
            } else {
                final String alias;
                if (rc.alias != null) {
                    alias = rc.alias;
                } else {
                    final String defaultAlias = getDefaultColumnAlias(rc.e);
                    alias = defaultAlias != null? defaultAlias: ("_col" + colIdx);
                }
                final DataType type = TypeInferer.inferResultType(inputSchema, e);
                builder.addColumn(alias, type);
            }
            colIdx += 1;
        }
        return builder.build();
    }

    /*
    public static Schema computeOutputSchema(SchemaProvider schemaProvider, SimpleSelectStatement simpleStmt) {
        if (simpleStmt.from == null) {
            return computeSelectClauseOutputSchema(Schema.emptySchema, simpleStmt.select);
        }

        final SelectSource source = simpleStmt.from.source;
        if (source instanceof ResourceString || source instanceof TableName) {
            final Schema inputSchema = schemaProvider.getSchema(source.getDataReference());
            return computeSelectClauseOutputSchema(inputSchema, simpleStmt.select);
        } else if (source instanceof SelectStatement) {
            final SelectStatement subQuery = (SelectStatement) source;
            final Schema inputSchema = computeOutputSchema(schemaProvider, subQuery);
            return computeSelectClauseOutputSchema(inputSchema, simpleStmt.select);
        } else {
            throw new RuntimeException("unexpected select source: " + source);
        }
    }

    public static Schema computeOutputSchema(SchemaProvider schemaProvider, SelectStatement stmt) {
        final Schema outputSchema = computeOutputSchema(schemaProvider, stmt.sovs.get(0));
        for (int i=1; i<stmt.sovs.size(); i++) {
            final Schema outputSchema2 = computeOutputSchema(schemaProvider, stmt.sovs.get(i));
            if (! outputSchema.equals(outputSchema2)) {
                throw new SemanticException("compound select query schema conflict: '"
                        + stmt.sovs.get(0).toString() + "' conflicts with '"
                        + stmt.sovs.get(i).select + "'");
            }
        }
        return outputSchema;
    }
    */
}
