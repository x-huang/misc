package xw.data.eaql.query;

import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.Expressions;
import xw.data.eaql.model.expr.LiteralValue;
import xw.data.eaql.model.stmt.ValuesStatement;
import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.util.ExprUtils;
import xw.data.eaql.query.util.TypeInferer;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.semantic.SemanticException;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.DataStreams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 9/1/16.
 */
public class ValuesQuery extends SQLQueryBase<ValuesStatement> implements SelectOrValuesQuery<ValuesStatement>
{
    private final List<Object[]> rows;
    public final Schema outputSchema;

    private static int checkValuesLength(ValuesStatement stmt) {
        int len = -1;
        for (List<Expression> exprs: stmt.values) {
            if (len < 0) {
                len = exprs.size();
            } else {
                if (len != exprs.size()) {
                    throw new SemanticException("mismatched values list size");
                }
            }
        }
        return len;
    }

    private static List<Object[]> reduceValues(List<List<Expression>> values, int len) {
        final List<Object[]> rows = new ArrayList<>();
        for (List<Expression> exprs: values) {
            final Object[] row = new Object[len];
            for (int i=0; i<len; i++) {
                final Expression reduced = ExprUtils.reduce(exprs.get(i));
                if (! Expressions.isLiteralValue(reduced)) {
                    throw new SemanticException("non-literal value in values statement: " + exprs.get(i));
                }
                row[i] = ((LiteralValue) reduced).value;
            }
            rows.add(row);
        }
        return rows;
    }

    private static Schema inferSchema(List<Object[]> rows) {
        final int len = rows.get(0).length;
        final DataType[] types = new DataType[len];
        for (int i=0; i<len; i++) {
            types[i] = Types.VOID;
        }

        for (Object[] row: rows) {
            for (int i=0; i<len; i++) {
                final DataType type = TypeInferer.getLiteralDataType(row[i]);
                if (types[i] == Types.VOID) {
                    types[i] = type;
                } else if (type != Types.VOID && ! Types.strictlyEqual(types[i], type)) {
                    throw new SemanticException("type of '" + row[i] + "' mismatches previous column: '"
                            + types[i] + "' vs. '" + type + "'");
                }
            }
        }

        final Schema.Builder builder = Schema.newBuilder();
        for (int i=0; i<len; i++) {
            builder.addColumn("_col" + i, types[i]);
        }
        return builder.build();
    }

    ValuesQuery(QueryEnvironment env, ValuesStatement stmt) {
        super(env, stmt);
        final int len = checkValuesLength(stmt);
        this.rows = reduceValues(stmt.values, len);
        this.outputSchema = inferSchema(rows);
    }

    @Override
    public Schema getSchema() {
        return outputSchema;
    }

    @Override
    public DataSource getDataSource() {
        return DataStreams.wrapRecordList(rows);
    }

}
