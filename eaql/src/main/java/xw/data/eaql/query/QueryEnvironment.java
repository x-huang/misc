package xw.data.eaql.query;

import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.DataSink;
import xw.data.unistreams.stream.DataSource;

/**
 * Created by xhuang on 3/18/16.
 */
public interface QueryEnvironment
{
    Schema getSchema(String ref);
    DataSink getDataSink(String ref, Schema schema);
    DataSource getDataSource(String ref);
}
