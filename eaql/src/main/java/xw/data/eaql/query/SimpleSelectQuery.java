package xw.data.eaql.query;

import xw.data.eaql.model.clause.SelectClause;
import xw.data.eaql.model.stmt.SelectStatement;
import xw.data.eaql.model.stmt.SimpleSelectStatement;
import xw.data.eaql.query.eval.RecordEvaluator;
import xw.data.eaql.query.eval.SimpleRecordEvaluator;
import xw.data.eaql.query.groupby.GroupBy;
import xw.data.eaql.model.expr.LiteralConstants;
import xw.data.eaql.query.util.ExprUtils;
import xw.data.eaql.query.util.QueryUtils;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.semantic.SemanticException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.Function;
import xw.data.unistreams.stream.DataStreams;
import xw.data.unistreams.stream.DataSource;
import xw.data.unistreams.stream.RecordPredicate;
import xw.data.unistreams.stream.RecordProcessor;

import java.util.*;


/**
 * Created by xhuang on 8/17/16.
 */
public class SimpleSelectQuery extends SQLQueryBase<SimpleSelectStatement>
        implements SelectOrValuesQuery<SimpleSelectStatement>
{
    private static final Logger logger = LoggerFactory.getLogger(SimpleSelectQuery.class);

    protected final Schema inputSchema;
    protected final DataSource inputStream;

    protected final Schema outputSchema;
    protected final SelectClause expandedSelect;
    public final int resultLen;
    protected final Expression[] selectedExprs;
    protected final Expression whereCondition;

    private final Map<String, Function> aggregateFunctions;

    SimpleSelectQuery(final QueryEnvironment env, SimpleSelectStatement stmt) {
        super(env, stmt);

        logger.debug("building simple select statement: {}", stmt);

        if (stmt.select.distinct && stmt.groupBy != null) {
            throw new SemanticException("group-by not allowed in select distinct statement");
        }

        if (stmt.from == null) {
            this.inputSchema = Schema.emptySchema;
            this.inputStream = DataStreams.getRepeatedRows(new Object[0], 1);
        } else {
            switch (stmt.from.getSourceType()) {
                case TABLE:
                case RESOURCE:
                    final String ref = stmt.from.source.getDataReference();
                    this.inputSchema = env.getSchema(ref);
                    this.inputStream = env.getDataSource(ref);
                    break;
                case SUB_QUERY:
                    final SQLRead subQuery = new SelectQuery(env, (SelectStatement) stmt.from.source);
                    this.inputSchema = subQuery.getSchema();
                    this.inputStream = subQuery.getDataSource();
                    break;
                default:
                    throw new RuntimeException("unexpected select source type: " + stmt.from.source);
            }
        }
        logger.debug("input schema: {}", inputSchema);

        this.expandedSelect = QueryUtils.expandSelectClause(stmt, inputSchema);
        logger.debug("select clause expanded: {}", expandedSelect);

        this.resultLen = expandedSelect.resultColumns.size();
        this.selectedExprs = new Expression[resultLen];
        for (int i=0; i<resultLen; i++) {
            selectedExprs[i] = ExprUtils.reduce(expandedSelect.resultColumns.get(i).e);
        }
        logger.debug("select clause reduced: {}", expandedSelect);

        this.outputSchema = QueryUtils.computeSelectClauseOutputSchema(inputSchema, expandedSelect);
        logger.debug("computed output schema: {}", outputSchema);

        this.whereCondition = (stmt.where == null)? LiteralConstants.literalTrue: stmt.where.e;
        logger.debug("where condition: {}", whereCondition);

        this.aggregateFunctions = ExprUtils.getAllAggregateFunctions(expandedSelect);
        logger.debug("all aggregate functions: {}", aggregateFunctions);

        if (stmt.select.distinct && aggregateFunctions.size() != 0) {
            logger.error("invalid aggregate functions: {}", aggregateFunctions);
            throw new SemanticException("invalid UDAF in select distinct statement");
        }

        // SemanticChecker checker = new SemanticChecker(stmt, inputSchema);
        // checker.checkAll();
   }

    @Override
    public Schema getSchema() {
        return outputSchema;
    }

    private static Object[] evalExprs(RecordEvaluator evaluator,
                                      Expression[] exprs, Object[] record) {
        final Object[] vs = new Object[exprs.length];
        for (int i=0; i<exprs.length; i++) {
            vs[i] = evaluator.eval(exprs[i], record);
        }
        return vs;
    }


    @Override
    public DataSource getDataSource() {

        final RecordEvaluator evaluator = new SimpleRecordEvaluator();
        final Expression whereCondIndexed = ExprUtils.indexColumns(inputSchema, whereCondition);
        logger.debug("indexed where condition: {}", whereCondIndexed);

        final DataSource filteredInputStream = DataStreams.filter(inputStream, new RecordPredicate() {
            @Override
            public boolean eval(Object[] row) {
                final Object result = evaluator.eval(whereCondIndexed, row);
                return result != null && result.equals(true);
            }
        });

        if (stmt.select.distinct) {
            logger.debug("select-distinct mode");
            final Expression[] indexedExprs = ExprUtils.indexColumns(inputSchema, selectedExprs);
            logger.debug("indexed expressions: {}", ExprUtils.toString(indexedExprs));
            final DataSource selectedOutputStream = DataStreams.transform(filteredInputStream, new RecordProcessor() {
                @Override
                public Object[] process(Object[] row) {
                    return evalExprs(evaluator, indexedExprs, row);
                }
            });
            final GroupBy groupBy = new GroupBy(outputSchema, selectedOutputStream, outputSchema.columns(), new ArrayList<Function>());
            return groupBy.getOutputStream();

        } else if (stmt.groupBy == null && aggregateFunctions.size() == 0) {
            logger.debug("no group-by or aggregators, falling through to streaming mode");

            final Expression[] indexedExprs = ExprUtils.indexColumns(inputSchema, selectedExprs);
            logger.debug("indexed expressions: {}", ExprUtils.toString(indexedExprs));

            final RecordProcessor processor = new RecordProcessor() {
                @Override
                public Object[] process(Object[] row) {
                    return evalExprs(evaluator, indexedExprs, row);
                }
            };
            return DataStreams.transform(filteredInputStream, processor);
        }
        else {
            final List<String> groupByKeys = stmt.groupBy == null? new ArrayList<String>(): stmt.groupBy.columns;
            final List<String> aggregatorStubs = new ArrayList<>(aggregateFunctions.keySet());
            final List<Function> aggregators = new ArrayList<>();
            for (String stub: aggregatorStubs) {
                aggregators.add(aggregateFunctions.get(stub));
            }
            final GroupBy groupBy = new GroupBy(inputSchema, filteredInputStream, groupByKeys, aggregators);
            logger.debug("constructed group-by operator with output schema: {}", groupBy.outputSchema);

            final Expression[] indexedExprs = groupBy.indexExpressions(selectedExprs);
            logger.debug("indexed expressions: {}", ExprUtils.toString(indexedExprs));

            final DataSource groupByOutput = groupBy.getOutputStream();
            return DataStreams.transform(groupByOutput, new RecordProcessor() {
                @Override
                public Object[] process(Object[] row) {
                    return evalExprs(evaluator, indexedExprs, row);
                }
            });
        }
    }

}
