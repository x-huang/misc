package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/18/2015.
 */
public class UDAF_collect_list_any implements UDAF<List<Object>>
{
    protected List<Object> list = new ArrayList<>();
    private final DataType elemType;

    public UDAF_collect_list_any(DataType elemType) {
        this.elemType = elemType;
    }

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        list.add(value);
    }

    @Override
    public List<Object> result() {
        return list;
    }

    @Override
    public DataType returnType() {
        return Types.newArrayType(elemType);
    }
}
