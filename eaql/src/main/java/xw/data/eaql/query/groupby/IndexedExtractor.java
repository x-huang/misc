package xw.data.eaql.query.groupby;

import xw.data.eaql.schema.Schema;
import xw.data.unistreams.stream.RecordProcessor;

import java.util.List;

/**
 * Created by xhuang on 8/24/16.
 */
public class IndexedExtractor implements RecordProcessor
{
    private final int[] dimension;

    public IndexedExtractor(int[] dimen) {
        this.dimension = new int[dimen.length];
        System.arraycopy(dimen, 0, this.dimension, 0, dimen.length);
    }

    protected IndexedExtractor(int size) {
        this.dimension = new int[size];
        for (int i=0; i<size; i++) {
            dimension[i] = -1;
        }
    }

    protected void setDimension(int i, int index) {
        dimension[i] = index;
    }

    @Override
    public Object[] process(Object[] row) {
        final Object[] result = new Object[dimension.length];
        for (int i=0; i<dimension.length; i++) {
            result[i] = row[dimension[i]];
        }
        return result;
    }

    public static IndexedExtractor fromColumnList(Schema schema, List<String> columns) {
        final IndexedExtractor extractor = new IndexedExtractor(columns.size());
        for (int i=0; i<columns.size(); i++) {
            extractor.setDimension(i, schema.indexOf(columns.get(i)));
        }
        return extractor;
    }
}
