package xw.data.eaql.query.eval;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by xhuang on 8/31/16.
 */
public class Values
{
    static Object implicitConvert(Object v) {
        if (v == null) {
            return null;
        } else if (v instanceof Long || v instanceof Double) {
            return v;
        } else if (v instanceof Integer || v instanceof Short || v instanceof Byte) {
            return ((Number) v).longValue();
        } else if (v instanceof Float || v instanceof BigDecimal) {
            return ((Number) v).doubleValue();
        } else if (v instanceof Date || v instanceof Timestamp) {
            return v.toString();
        } else {
            return v;
        }
    }

    public static boolean isIntegerNumber(Object v) {
        return v instanceof Long || v instanceof Integer || v instanceof Short || v instanceof Byte;
    }

    public static boolean isFloatNumber(Object v) {
        return v instanceof Double || v instanceof Float || v instanceof BigDecimal;
    }

    public static boolean isBoolean(Object v) {
        return v instanceof Boolean;
    }

    public static boolean equal(Object v1, Object v2) {
        return v1 == null && v2 == null || !(v1 == null || v2 == null) && v1.equals(v2);
    }

    @SuppressWarnings("unchecked")
    public static int compare(Object v1, Object v2) {
        assert v1 instanceof Comparable;
        if (v1.getClass().equals(v2.getClass())) {
            return ((Comparable) v1).compareTo(v2);
        } else if (v1 instanceof Number && v2 instanceof Number) {
            return ((Double)((Number) v1).doubleValue()).compareTo(((Number) v2).doubleValue());
        } else {
            throw new RuntimeException("incomparable values: '" + v1 + "' vs '" + v2 + "'");
        }
    }
}
