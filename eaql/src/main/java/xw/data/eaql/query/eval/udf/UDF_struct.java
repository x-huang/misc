package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.StructType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.query.util.StructLiteral;
import xw.data.eaql.util.Lazy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_struct implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        final List<StructType.StructField> fields = new ArrayList<>();
        for (int i=0; i<paramTypes.size(); i++) {
            fields.add(new StructType.StructField("col" + (i + 1), paramTypes.get(i)));
        }
        return Types.newStructType(fields);
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final List<Object> list = new StructLiteral<>();
        for (Lazy<Object> lazyParam : lazyParams) {
            list.add(lazyParam.get());
        }
        return list;
    }
}
