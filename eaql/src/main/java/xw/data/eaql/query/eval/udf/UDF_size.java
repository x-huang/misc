package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 9/10/16.
 */
class UDF_size implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 1);
        return Types.BIGINT;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v = lazyParams.get(0).get();
        if (v instanceof Collection) {
            return ((Collection) v).size();
        } else if (v instanceof Map) {
            return ((Map) v).size();
        } else {
            throw new UDFException("expects list or map parameter, got: "
                    + v.getClass().getName());
        }
    }
}
