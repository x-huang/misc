package xw.data.eaql.query.groupby;

import xw.data.eaql.model.expr.Asterisk;
import xw.data.eaql.query.eval.udaf.UDAFs;
import xw.data.eaql.model.expr.LiteralConstants;
import xw.data.eaql.query.util.ExprUtils;
import xw.data.eaql.query.eval.RecordEvaluator;
import xw.data.eaql.query.eval.SimpleRecordEvaluator;
import xw.data.eaql.schema.Schema;
import xw.data.eaql.model.expr.Expression;
import xw.data.eaql.model.expr.Function;
import xw.data.unistreams.stream.RecordProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/24/16.
 */
public class ExpressionValueExtractor implements RecordProcessor
{
    private final Expression[] indexedExprs;
    private final RecordEvaluator evaluator;


    protected ExpressionValueExtractor(Expression[] indexedExprs) {
        this.indexedExprs = indexedExprs;
        this.evaluator = new SimpleRecordEvaluator();
    }

    @Override
    public Object[] process(Object[] row) {
        final Object[] result = new Object[indexedExprs.length];
        for (int i=0; i< indexedExprs.length; i++) {
            result[i] = evaluator.eval(indexedExprs[i], row);
        }
        return result;
    }

    public static ExpressionValueExtractor fromColumnValueExpression(Schema schema, Expression[] arr) {
        return new ExpressionValueExtractor(ExprUtils.indexColumns(schema, arr));
    }

    public static ExpressionValueExtractor fromColumnValueExpressionList(Schema schema, List<Expression> list) {
        return new ExpressionValueExtractor(ExprUtils.indexColumns(schema, list));
    }

    public static ExpressionValueExtractor fromParameterList(Schema schema, List<Function> functions) {
        final List<Expression> aggregatorParams = new ArrayList<>();
        for (final Function f : functions) {
            if (! UDAFs.isUDAF(f)) {
                throw new RuntimeException("expecting an aggregate function, got: " + f);
            }
            f.assertNumParams(1);
            final Expression param = f.getParam(0);
            aggregatorParams.add((param instanceof Asterisk)? LiteralConstants.literalOne: param);
        }
        return fromColumnValueExpressionList(schema, aggregatorParams);
    }
}
