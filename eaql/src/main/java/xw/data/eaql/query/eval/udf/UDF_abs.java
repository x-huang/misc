package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.util.Lazy;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by xhuang on 8/31/16.
 */
class UDF_abs implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 1);
        return paramTypes.get(0);
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v = lazyParams.get(0).get();
        if (v == null) {
            return null;
        } else if (v instanceof Long) {
            return Math.abs((long) v);
        } else if (v instanceof Integer) {
            return Math.abs((int) v);
        } else if (v instanceof Short) {
            return (short) Math.abs((short) v);
        } else if (v instanceof Double) {
            return Math.abs((double) v);
        } else if (v instanceof Float) {
            return Math.abs((float) v);
        } else if (v instanceof BigDecimal) {
            return ((BigDecimal) v).abs();
        } else {
            throw new UDFException("expects number parameter, got: "
                    + v.getClass().getName());
        }
    }
}
