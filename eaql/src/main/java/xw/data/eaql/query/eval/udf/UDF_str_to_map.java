package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xhuang on 9/12/16.
 */
class UDF_str_to_map implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSizeBetween(paramTypes, 1, 3);
        return Types.newMapType(Types.STRING, Types.STRING);
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final String s = (String) lazyParams.get(0).get();
        if (s == null) return null;

        String delim1 = ",";
        String delim2 = "=";

        if (lazyParams.size() >= 2) {
            delim1 = (String) lazyParams.get(1).get();
        }
        if (lazyParams.size() >= 3) {
            delim2 = (String) lazyParams.get(2).get();
        }

        final Map<String, String> map = new HashMap<>();
        final String[] splits = s.split(delim1);
        for (String ss: splits) {
            final String[] pair = ss.split(delim2, 2);
            if (pair.length == 2) {
                map.put(pair[0], pair[1]);
            }
        }
        return map;
    }
}
