package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.StructType;
import xw.data.eaql.model.type.Types;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xhuang on 8/23/16.
 */
public class UDAFVector implements UDAF<Object[]>
{
    protected UDAF[] udafs;

    public UDAFVector(UDAF[] udafs) {
        this.udafs = udafs;
    }

    @Override
    public void aggregate(Object value) {
        final Object[] vec = (Object[]) value;
        if (vec.length != udafs.length) {
            throw new RuntimeException("aggregatable vector size mismatch, expecting "
                    + udafs.length + " values, got " + vec.length);
        }
        for (int i=0; i< udafs.length; i++) {
            udafs[i].aggregate(vec[i]);
        }
    }

    @Override
    public Object[] result() {
        final Object[] result = new Object[udafs.length];
        for (int i=0; i< udafs.length; i++) {
            result[i] = udafs[i].result();
        }
        return result;
    }

    @Override
    public DataType returnType() {
        final List<StructType.StructField> fields = new ArrayList<>();
        for (int i=0; i< udafs.length; i++) {
            fields.add(new StructType.StructField("_col" + i, udafs[i].returnType()));
        }
        return Types.newStructType(fields);
    }
}
