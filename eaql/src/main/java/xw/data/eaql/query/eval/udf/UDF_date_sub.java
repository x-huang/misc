package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.DateTimeUtils;
import xw.data.eaql.util.Lazy;

import java.sql.Date;
import java.util.List;

/**
 * Created by xhuang on 8/31/16.
 */
class UDF_date_sub implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 2);
        UDFs.expectParameterType(paramTypes, 0, Types.STRING, Types.DATE);
        UDFs.expectParameterType(paramTypes, 1, Types.BIGINT, Types.INT);
        return Types.DATE;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v1 = lazyParams.get(0).get();
        if (v1 == null) {
            return null;
        }
        final Object v2 = lazyParams.get(1).get();
        if (v2 == null) {
            return null;
        }

        try {
            final Date date = DateTimeUtils.to_date(v1);
            final int diff = ((Number) v2).intValue();
            return DateTimeUtils.date_arithmetic(date, -diff);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
