package xw.data.eaql.query.eval.udaf;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAFException extends Exception
{
    public UDAFException() {
        super("aggregation no real input");
    }

    public UDAFException(String message) {
        super(message);
    }
}
