package xw.data.eaql.query;

import xw.data.eaql.EAQL;
import xw.data.eaql.env.unistreams.UnistreamsEnv;
import xw.data.eaql.model.stmt.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by xhuang on 8/17/16.
 */
public class QueryBuilder
{
    public static SQLQuery build(QueryEnvironment env, SQLStatement stmt) {
        if (stmt instanceof SelectStatement) {
            return new SelectQuery(env, (SelectStatement) stmt);
        } else if (stmt instanceof SimpleSelectStatement) {
            return new SimpleSelectQuery(env, (SimpleSelectStatement) stmt);
        } else if (stmt instanceof DescribeStatement) {
            return new DescribeQuery(env, (DescribeStatement) stmt);
        } else if (stmt instanceof InsertStatement) {
            return new InsertQuery(env, (InsertStatement) stmt);
        } else {
            throw new RuntimeException("unexpected statement: '" + stmt + "'");
        }
    }

    public static SQLQuery build(QueryEnvironment env, String s) {
        return build(env, EAQL.parseStatement(s));
    }

    public static SQLQuery build(QueryEnvironment env, InputStream in) throws IOException {
        return build(env, EAQL.parseStatement(in));
    }

    public static SQLQuery build(SQLStatement stmt) {
        return build(UnistreamsEnv.get(), stmt);
    }

    public static SQLQuery build(String s) {
        return build(UnistreamsEnv.get(), s);
    }

    public static SQLQuery build(InputStream in) throws IOException {
        return build(UnistreamsEnv.get(), in);
    }
}
