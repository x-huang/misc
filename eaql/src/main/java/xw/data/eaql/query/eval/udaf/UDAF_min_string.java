package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 8/18/2015.
 */
public class UDAF_min_string implements UDAF<String>
{
    protected String value = null;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        if (this.value == null || this.value.compareTo((String) value) > 0) {
            this.value = (String) value;
        }
    }

    @Override
    public String result() {
        return value;
    }

    @Override
    public DataType returnType() {
        return Types.STRING;
    }
}
