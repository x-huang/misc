package xw.data.eaql.query.eval.udf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;
import xw.data.eaql.util.Lazy;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by xhuang on 8/30/16.
 */
class UDF_floor implements UDF
{
    @Override
    public DataType inferReturnType(List<DataType> paramTypes) throws UDFException {
        UDFs.expectParameterSize(paramTypes, 1);
        return Types.BIGINT;
    }

    @Override
    public Object eval(List<Lazy<Object>> lazyParams) throws UDFException {
        final Object v = lazyParams.get(0).get();
        if (v == null) return null;
        if (v instanceof Long || v instanceof Integer
                || v instanceof Short || v instanceof Byte) {
            return ((Number) v).longValue();
        } else if (v instanceof Float || v instanceof Double
                || v instanceof BigDecimal) {
            return Math.floor(((Number) v).doubleValue());
        } else {
            throw new UDFException("expects number parameter, got: "
                    + v.getClass().getName());
        }
    }
}
