package xw.data.eaql.query.eval;

import xw.data.eaql.model.expr.Expression;

/**
 * Created by xhuang on 11/3/15.
 */
public class EvaluationException extends RuntimeException
{
    public final Expression expr;

    public EvaluationException(String msg, Expression expr) {
        super(msg);
        this.expr = expr;
    }

    public EvaluationException(String msg, Expression expr, Exception e) {
        super(msg, e);
        this.expr = expr;
    }
}
