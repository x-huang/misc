package xw.data.eaql.query.eval.udaf;


import xw.data.eaql.model.type.DataType;

/**
 * Created by xhuang on 8/18/2015.
 */
public interface UDAF<T>
{
    void aggregate(Object value);
    T result();
    DataType returnType();
}
