package xw.data.eaql.query.eval.udaf;

import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAF_max_bigint implements UDAF<Long>
{
    private long max = Long.MIN_VALUE;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        long l = ((Number) value).longValue();
        if (l > max) {
            max = l;
        }
    }

    @Override
    public Long result() {
        return (max == Long.MIN_VALUE)? null: max;
    }

    @Override
    public DataType returnType() {
        return Types.BIGINT;
    }
}
