package xw.data.eaql.query.groupby;

import java.util.Arrays;

/**
 * Created by xhuang on 10/14/2014.
 */
public class GroupByKeys //implements Comparable<GroupByKeys>
{
    protected final Object[] keys;

    public GroupByKeys(Object[] keys) {
        this.keys = keys;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (! (o instanceof GroupByKeys)) {
            return false;
        }
        final GroupByKeys that = (GroupByKeys) o;
        return Arrays.equals(this.keys, that.keys);
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Object key : keys) {
            hashCode = hashCode * 31 + (key==null? 726: key.hashCode());
        }
        return hashCode;
    }

    public Object[] getKeys() {
        return keys;
    }

    @Override
    public String toString() {
        switch (keys.length) {
            case 0: return "";
            case 1: return keys[0].toString();
            case 2: return keys[0].toString() + '\t' + keys[1];
            case 3: return keys[0].toString() + '\t' + keys[1] + '\t' + keys[2];
            default: {
                final StringBuilder sb = new StringBuilder();
                String delimiter = "";
                for (Object key: keys) {
                    sb.append(delimiter).append(key);
                    delimiter = "\t";
                }
                return sb.toString();
            }
        }
    }

}
