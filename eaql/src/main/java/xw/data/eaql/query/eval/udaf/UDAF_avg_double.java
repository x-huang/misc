package xw.data.eaql.query.eval.udaf;


import xw.data.eaql.model.type.DataType;
import xw.data.eaql.model.type.Types;

/**
 * Created by xhuang on 3/21/16.
 */
public class UDAF_avg_double implements UDAF<Double>
{
    private double sum = 0.0;
    private long count = 0;

    @Override
    public void aggregate(Object value) {
        if (value == null) return;
        sum += ((Number) value).doubleValue();
        count ++;
    }

    @Override
    public Double result() {
        return count == 0? null: sum/count;
    }

    @Override
    public DataType returnType() {
        return Types.DOUBLE;
    }
}
