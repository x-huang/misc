package xw.data.eaql.query.eval.udf;

/**
 * Created by xhuang on 8/30/16.
 */
public class UDFException extends Exception
{
    public UDFException(String msg) {
        super(msg);
    }
}
