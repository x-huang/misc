package xw.data.antlr4.common;

import org.antlr.v4.runtime.tree.*;

import java.io.PrintStream;

/**
 * Created by xhuang on 10/20/15.
 */
public class GenericAstPrinter implements ParseTreeVisitor<Void>
{
    protected int indent = 0;
    protected int indentInc = 2;
    private PrintStream ps = System.out;

    public GenericAstPrinter setInitialIndent(int indent) {
        this.indent = indent;
        return this;
    }

    public GenericAstPrinter setIndentIncrement(int inc) {
        this.indentInc = inc;
        return this;
    }

    public GenericAstPrinter setPrintStream(PrintStream ps) {
        this.ps = ps;
        return this;
    }

    private void printIndent() {
        for (int i=0; i<indent; i++) {
            ps.append(' ');
        }
    }

    protected void printNode(ParseTree tree) {
        ps.append('[').append(tree.getClass().getSimpleName()).append("] ")
                .append(tree.getText()).append("\n");
    }

    @Override
    public Void visit(ParseTree parseTree) {
        printIndent();
        printNode(parseTree);
        indent += indentInc;
        for (int i=0; i<parseTree.getChildCount(); i++) {
            visit(parseTree.getChild(i));
        }
        indent -= indentInc;
        return null;
    }

    @Override
    public Void visitChildren(RuleNode ruleNode) {
        ps.append("[RULE] ").append(ruleNode.getRuleContext().getText()).append("\n");
        return null;
    }

    @Override
    public Void visitTerminal(TerminalNode terminalNode) {
        ps.append("[TERM] ").append(terminalNode.getSymbol().getText()).append("\n");
        return null;
    }

    @Override
    public Void visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
