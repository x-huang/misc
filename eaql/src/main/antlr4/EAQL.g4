
grammar EAQL;

@header {
    package xw.data.eaql.parser;
}

parse
 : ';'* sql_stmt ( ';'+ sql_stmt) * ';'* EOF
 ;

error
 : UNEXPECTED_CHAR 
   { 
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text); 
   }
 ;

sql_stmt
 : describe_table_stmt
 | select_stmt
 | insert_stmt
 ;

describe_table_stmt
 : (K_DESC | K_DESCRIBE) K_TABLE table_name
 | (K_DESC | K_DESCRIBE) resource_descriptor
 ;

select_stmt
 : select_or_values ( compound_operator select_or_values )* order_by_clause? limit_clause? offset_clause?
 ;

select_or_values
 : simple_select_stmt
 | values_stmt
 ;

values_stmt
 : K_VALUES  '(' expr_list ')' ( ',' '(' expr_list ')' )*
 ;

expr_list
 : expr ( ',' expr )*
 ;

simple_select_stmt
 : select_clause from_clause? where_clause? group_by_clause?
 ;

select_clause
 : K_SELECT K_DISTINCT? result_column ( ',' result_column )*
 ;

from_clause
 : K_FROM table_name
 | K_FROM resource_descriptor
 | K_FROM '(' select_stmt ')'
 ;

where_clause
 : K_WHERE expr
 ;

group_by_clause
 : K_GROUP K_BY column_name ( ',' column_name )*
 ;

order_by_clause
 : K_ORDER K_BY ordering_term ( ',' ordering_term )*
 ;

limit_clause
 : K_LIMIT NUMERIC_LITERAL
 ;

offset_clause
 : K_OFFSET NUMERIC_LITERAL
 ;

insert_stmt
 : K_INSERT (K_OVERWRITE | K_INTO) resource_descriptor select_stmt
 ;

/*
    EAQL understands the following binary operators, in order from highest to
    lowest precedence:

    ||
    *    /    %
    +    -
    <<   >>   &    |
    <    <=   >    >=
    =    ==   !=   <>   IS   IS NOT   IN   LIKE   GLOB   MATCH   REGEXP
    AND
    OR
*/
expr
 : asterisk
 | literal_value
 | column_name
 | function
 | expr '[' expr ']'
 | expr '.' IDENTIFIER
 | unary_operator expr
 | expr ( '*' | '/' ) expr
 | expr ( '+' | '-' ) expr
 | expr ( '<' | '<=' | '>' | '>=' ) expr
 | expr ( '=' | '==' | '!=' | '<>') expr
 | expr K_NOT? K_LIKE literal_value
 | expr K_NOT? K_RLIKE literal_value
 | expr K_IS K_NOT? K_NULL
 | expr K_NOT? K_IN ( '(' literal_value ( ',' literal_value )*  ')')
 | expr K_NOT? K_BETWEEN literal_value K_AND literal_value
 | K_NOT expr
 | expr ( K_AND | '&&' ) expr
 | expr ( K_OR | '||' ) expr
 | parenthetical
 | K_CAST '(' expr K_AS data_type ')'
 | K_CASE expr? ( K_WHEN expr K_THEN expr )+ ( K_ELSE expr )? K_END
 ;

ordering_term
 : expr ( K_ASC | K_DESC )?
 ;

result_column
 : expr ( K_AS? column_alias )?
 ;

compound_operator
 : K_UNION K_ALL?
/* | K_UNION
 | K_UNION K_ALL
 | K_INTERSECT
 | K_EXCEPT */
 ;

literal_value
 : NUMERIC_LITERAL
 | STRING_LITERAL
 | K_TRUE
 | K_FALSE
 | K_NULL
 ;

asterisk
 : '*'
 ;


function
 : function_name '(' ')'
 | function_name '(' expr ( ',' expr ) * ')'
 ;

unary_operator
 : '-'
 | '+'
 ;

parenthetical
 : '(' expr ')'
 ;

error_message
 : STRING_LITERAL
 ;

column_alias
 : IDENTIFIER
 | STRING_LITERAL
 ;

keyword
 : K_AND
 | K_AS
 | K_ASC
 | K_BETWEEN
 | K_BY
 | K_CASE
 | K_CAST
//  | K_CREATE
//  | K_DEFAULT
 | K_DESC
 | K_DESCRIBE
 | K_ELSE
 | K_END
// | K_EXISTS
 | K_FROM
 | K_GROUP
 | K_HAVING
 | K_IF
 | K_IN
 | K_INSERT
 | K_INTO
 | K_IS
 | K_LIKE
 | K_LIMIT
 | K_NOT
 | K_NULL
 | K_OFFSET
 | K_OR
 | K_ORDER
 | K_OVERWRITE
 | K_RLIKE
 | K_SELECT
 | K_TABLE
 | K_THEN
 | K_UNION
 | K_WHEN
 | K_WHERE
 ;

// TODO check all names below

data_type
 : primitive_type
 | K_STRUCT '<' struct_field ( ',' struct_field ) * '>'
 | K_MAP '<' primitive_type ',' data_type '>'
 | K_ARRAY '<' data_type '>'
 | K_UNIONTYPE '<' data_type (',' data_type) * '>'
 ;

struct_field
 : column_name ( ':' data_type ) ?
 ;

primitive_type
 : K_STRING
 | K_VARCHAR '(' NUMERIC_LITERAL ')'
 | K_CHAR '(' NUMERIC_LITERAL ')'
 | K_TINYINT
 | K_SMALLINT
 | K_INT
 | K_BIGINT
 | K_FLOAT
 | K_DOUBLE
 | K_DECIMAL
 | K_DECIMAL '(' NUMERIC_LITERAL ')'
 | K_DECIMAL '(' NUMERIC_LITERAL ',' NUMERIC_LITERAL ')'
 | K_DECIMAL
 | K_TIMESTAMP
 | K_DATE
 | K_BOOLEAN
 | K_BINARY
 ;

K_STRUCT : S T R U C T;
K_MAP : M A P;
K_ARRAY : A R R A Y;
K_UNIONTYPE : U N I O N T Y P E;
K_STRING: S T R I N G;
K_VARCHAR: V A R C H A R;
K_CHAR: C H A R;
K_TINYINT: T I N Y I N T | B Y T E;
K_SMALLINT: S M A L L I N T | S H O R T;
K_INT: I N T;
K_BIGINT: B I G I N T | L O N G;
K_FLOAT: F L O A T;
K_DOUBLE: D O U B L E;
K_DECIMAL: D E C I M A L;
K_TIMESTAMP: T I M E S T A M P;
K_DATE: D A T E;
K_BOOLEAN: B O O L E A N | B O O L;
K_BINARY: B I N A R Y;

table_name
 : IDENTIFIER ( '.' IDENTIFIER)?
 ;

resource_descriptor
 : STRING_LITERAL
 ;

function_name
 : any_name
 ;

column_name
 : any_name
 ;

any_name
 : IDENTIFIER 
 | keyword
 | STRING_LITERAL
 | '(' any_name ')'
 ;

SCOL : ';';
DOT : '.';
OPEN_PAR : '(';
CLOSE_PAR : ')';
COMMA : ',';
ASSIGN : '=';
STAR : '*';
PLUS : '+';
MINUS : '-';
TILDE : '~';
PIPE2 : '||';
DIV : '/';
MOD : '%';
// LT2 : '<<';
// GT2 : '>>';
AMP : '&';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';

// http://www.sqlite.org/lang_keywords.html
// K_ADD : A D D;
K_ALL : A L L;
// K_ALTER : A L T E R;
K_AND : A N D;
K_AS : A S;
K_ASC : A S C;
// K_ATTACH : A T T A C H;
K_BETWEEN : B E T W E E N;
K_BY : B Y;
K_CASE : C A S E;
K_CAST : C A S T;
// K_COLUMN : C O L U M N;
// K_CREATE : C R E A T E;
// K_DEFAULT : D E F A U L T;
K_DESC : D E S C;
K_DESCRIBE : D E S C R I B E;
K_DISTINCT : D I S T I N C T;
K_ELSE: E L S E;
K_END: E N D;
// K_EXISTS : E X I S T S;
K_FROM : F R O M;
K_GROUP : G R O U P;
K_HAVING : H A V I N G;
K_IF : I F;
K_IN : I N;
K_INSERT : I N S E R T;
K_INTO : I N T O;
K_IS : I S;
K_LIKE : L I K E;
K_LIMIT : L I M I T;
K_NOT : N O T;
K_NULL : N U L L;
K_OFFSET : O F F S E T;
K_OR : O R;
K_ORDER : O R D E R;
K_OVERWRITE: O V E R W R I T E;
K_RLIKE: R L I K E;
K_SELECT : S E L E C T;
K_TABLE : T A B L E;
K_THEN: T H E N;
K_UNION : U N I O N;
// K_USING : U S I N G;
K_VALUES : V A L U E S;
K_WHEN: W H E N;
K_WHERE : W H E R E;
K_TRUE: T R U E;
K_FALSE: F A L S E;

IDENTIFIER
 : '`' ( ~ '`' )* '`'
 | [a-zA-Z_] [a-zA-Z_0-9]* // TODO check: needs more chars in set
 ;

NUMERIC_LITERAL
 : DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
 | '.' DIGIT+ ( E [-+]? DIGIT+ )?
 ;

//BIND_PARAMETER
// : '?' DIGIT*
// | [:@$] IDENTIFIER
// ;

STRING_LITERAL
 : '\'' ( ~'\'' | '\'\'' )* '\''
 | '"' ( ~ '"' | '""' )* '"'
 ;

BLOB_LITERAL
 : X STRING_LITERAL
 ;

SINGLE_LINE_COMMENT
 : '--' ~[\r\n]* -> channel(HIDDEN)
 ;

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

SPACES
 : [ \u000B\t\r\n] -> channel(HIDDEN)
 ;

UNEXPECTED_CHAR
 : .
 ;

fragment DIGIT : [0-9];

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
