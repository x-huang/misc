
grammar EAQL;

parse
 : ';'* sql_stmt ( ';'+ sql_stmt) * ';'* EOF
 ;

sql_stmt
 : describe_table_stmt
 | select_stmt
 | insert_stmt
 ;

describe_table_stmt
 : (K_DESC | K_DESCRIBE) K_TABLE table_name
 | (K_DESC | K_DESCRIBE) resource_descriptor
 ;

select_stmt
 : select_or_values ( compound_operator select_or_values )* order_by_clause? limit_clause? offset_clause?
 ;

select_or_values
 : simple_select_stmt
 | values_stmt
 ;

values_stmt
 : K_VALUES  '(' expr_list ')' ( ',' '(' expr_list ')' )*
 ;

expr_list
 : expr ( ',' expr )*
 ;

simple_select_stmt
 : select_clause from_clause? where_clause? group_by_clause?
 ;

select_clause
 : K_SELECT K_DISTINCT? result_column ( ',' result_column )*
 ;

from_clause
 : K_FROM table_name
 | K_FROM resource_descriptor
 | K_FROM '(' select_stmt ')'
 ;

where_clause
 : K_WHERE expr
 ;

group_by_clause
 : K_GROUP K_BY column_name ( ',' column_name )*
 ;

order_by_clause
 : K_ORDER K_BY ordering_term ( ',' ordering_term )*
 ;

limit_clause
 : K_LIMIT NUMERIC_LITERAL
 ;

offset_clause
 : K_OFFSET NUMERIC_LITERAL
 ;

insert_stmt
 : K_INSERT (K_OVERWRITE | K_INTO) resource_descriptor select_stmt
 ;

/*
    EAQL understands the following binary operators, in order from highest to
    lowest precedence:

    ||
    *    /    %
    +    -
    <<   >>   &    |
    <    <=   >    >=
    =    ==   !=   <>   IS   IS NOT   IN   LIKE   GLOB   MATCH   REGEXP
    AND
    OR
*/
expr
 : asterisk
 | literal_value
 | column_name
 | function
 | expr '[' expr ']'
 | expr '.' IDENTIFIER
 | unary_operator expr
 | expr ( '*' | '/' ) expr
 | expr ( '+' | '-' ) expr
 | expr ( '<' | '<=' | '>' | '>=' ) expr
 | expr ( '=' | '==' | '!=' | '<>') expr
 | expr K_NOT? K_LIKE literal_value
 | expr K_IS K_NOT? K_NULL
 | expr K_NOT? K_IN ( '(' literal_value ( ',' literal_value )*  ')')
 | expr K_NOT? K_BETWEEN literal_value K_AND literal_value
 | K_NOT expr
 | expr ( K_AND | '&&' ) expr
 | expr ( K_OR | '||' ) expr
 | parenthetical
 | K_CAST '(' expr K_AS data_type ')'
 | K_CASE expr? ( K_WHEN expr K_THEN expr )+ ( K_ELSE expr )? K_END
 ;

ordering_term
 : expr ( K_ASC | K_DESC )?
 ;

result_column
 : expr ( K_AS? column_alias )?
 ;

compound_operator
 : K_UNION K_ALL?
 ;

literal_value
 : NUMERIC_LITERAL
 | STRING_LITERAL
 | K_TRUE
 | K_FALSE
 | K_NULL
 ;

asterisk
 : '*'
 ;


function
 : function_name '(' ')'
 | function_name '(' expr ( ',' expr ) * ')'
 ;

unary_operator
 : '-'
 | '+'
 ;

parenthetical
 : '(' expr ')'
 ;

error_message
 : STRING_LITERAL
 ;

column_alias
 : IDENTIFIER
 | STRING_LITERAL
 ;

data_type
 : primitive_type
 | K_STRUCT '<' struct_field ( ',' struct_field ) * '>'
 | K_MAP '<' primitive_type ',' data_type '>'
 | K_ARRAY '<' data_type '>'
 | K_UNIONTYPE '<' data_type (',' data_type) * '>'
 ;

struct_field
 : column_name ( ':' data_type ) ?
 ;

primitive_type
 : K_STRING
 | K_VARCHAR '(' NUMERIC_LITERAL ')'
 | K_CHAR '(' NUMERIC_LITERAL ')'
 | K_TINYINT
 | K_SMALLINT
 | K_INT
 | K_BIGINT
 | K_FLOAT
 | K_DOUBLE
 | K_DECIMAL
 | K_DECIMAL '(' NUMERIC_LITERAL ')'
 | K_DECIMAL '(' NUMERIC_LITERAL ',' NUMERIC_LITERAL ')'
 | K_DECIMAL
 | K_TIMESTAMP
 | K_DATE
 | K_BOOLEAN
 | K_BINARY
 ;

table_name
 : IDENTIFIER ( '.' IDENTIFIER)?
 ;

resource_descriptor
 : STRING_LITERAL
 ;

function_name
 : any_name
 ;

column_name
 : any_name
 ;

any_name
 : IDENTIFIER 
 | keyword
 | STRING_LITERAL
 | '(' any_name ')'
 ;

