import xw.data.eaql.EAQL
import xw.data.eaql.model.expr.Expression
import xw.data.eaql.model.expr.Expressions
import xw.data.eaql.query.util.ExprUtils
import spock.lang.Specification

class ExpressionSpec extends Specification
{
    def "expression equals to itself and cloned copy"() {
        given:
            Expression e = EAQL.parseExpression(s)
            Expression e__ = EAQL.parseExpression(s)
            print("[clone] " + e)
            Expression e_ = Expressions.clone(e)
            println(" => " + e)
        expect:
            Expressions.equal(e, e__)
            Expressions.equal(e, e_)
        where:
            s << [
                    "*",
                    '0',
                    ".123456",
                    "_col",
                    "_col[1]",
                    '_col["a"]',
                    '_col.a',
                    '_col[x]',
                    '_col[1]["a"].a[x]',
                    "(s * 2 - 1) / 3 + 1 = 12",
                    "(not a and b or c) <> false",
                    "func(1, x+y, -t)",
                    "s like \"%xyz%\"",
                    "s not like \"%xyz%\"",
                    "x > 1 AND x >= 0 AND y < 2 AND y <= 1",
                    "s in (null, 1, 3.14, 'xyz', true, false)",
                    "s not in (null, 1, 3.14, 'xyz', true, false)",
                    "x between 0.0 AND 1.0",
                    "x not between 0.0 AND 1.0",
                    "x IS NULL",
                    "x IS NOT NULL",
                    "cast ( parse_int(num_string, 16) as string)",
                    'case when x>=100 then "x>=100" when x>=60 then "60<=x<100" else "x<60" end',
                    'case x when 1 then "x=1" when 2 then "x=2" else "else" end',
            ]
    }

    def "expression reduces properly"() {
        given:
            Expression e1 = EAQL.parseExpression(t[0])
            print("[reduce] " + e1)
            Expression e1_ = ExprUtils.reduce(e1)
            println(" => " + e1_)
            Expression e2 = EAQL.parseExpression(t[1])
        expect:
            Expressions.equal(e1_, e2)
        where:
            t << [
                    ['_col', '_col'],
                    ['(2*3+4)/5-1', '1.0'],
                    ['0=1 OR x', 'x'],
                    ['s not in (null, 1, 3.14, "xyz", true, false)',
                        's not in ("xyz", 1, false, 1, 3.14, null, "xyz", true, false)'],
                    ['a OR (b AND (NOT c))', 'a OR b AND NOT c'],
                    ['x LIKE "%abc%"', 'x LIKE "%abc%"'],
                    ['x RLIKE "^a?b+c*$"', 'x RLIKE "^a?b+c*$"'],
                    ['NOT ((x LIKE "%abc%"))', 'NOT x LIKE "%abc%"'],
                    ['x OR 0=1', 'x'],
                    ['0=1 AND x', 'false'],
                    ['x AND 0=1', 'false'],
                    ['1=1 OR x', 'true'],
                    ['x OR 1=1', 'true'],
                    ['1=1 AND x', 'x'],
                    ['x AND 1=1', 'x'],
                    ['func(1, x+y, -t)', 'func(1, x+y, -t)'],
                    ['x between 1 and 2', 'x between 1 and 2'],
                    ['x between 1 and 0', 'false'],
                    ['x not between 1 and 0', 'true'],
                    // ['x*y-z - (x*y-z)', '0'],
                    ['if(100>99+2,  "yes", "no")', '"no"'],
                    ['concat("a", "b", "c")', '"abc"'],
                    ['case x when 1+2 then "3" when (1+2)*(2+1) then "9" else "else" end',
                        'case x when 3 then "3" when 9 then "9" else "else" end']
            ]
    }

}