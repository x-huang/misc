import xw.data.eaql.EAQL
import xw.data.eaql.model.stmt.SelectStatement
import xw.data.eaql.semantic.SemanticChecker
import xw.data.eaql.semantic.SemanticException
import xw.data.eaql.schema.Schema
import spock.lang.Specification

/**
 * Created by xhuang on 7/31/2015.
 */
class SemanticCheckerSpec extends Specification {

    final String schemaString = "b:boolean,i:int,l:bigint,f:float,d:double,s:string,ts:timestamp,dt:date"
    final Schema schema = Schema.fromString(schemaString)

    def "semantic check OK"() {
        when:
            SelectStatement stmt = (SelectStatement) EAQL.parseStatement(s);
            SemanticChecker checker = new SemanticChecker(stmt.sovs.get(0), schema)
            checker.checkAll()
        then:
            notThrown(SemanticException)
        where:
            s << [
                    'select 0 from tbl',
                    'select d from tbl',
                    'select sum(d) from tbl',
                    'select func(s, i + l, f * d, func2(ts)) from tbl',
                    'select s from tbl b AND i>0 AND l>=0 OR ( s LIKE "%abc%" AND ts IS NOT NULL)'
            ]
    }

    def "semantic check not OK"() {
        when:
            println("[checking] " + s)
            SelectStatement stmt = (SelectStatement) EAQL.parseStatement(s);
            SemanticChecker checker = new SemanticChecker(stmt.sovs.get(0), schema)
            checker.checkAll()
        then:
            thrown(SemanticException)
        where:
            s << [
                    'select d_ from "hive:///db/tbl"',
                    'select sum(d_) from "hive:///db/tbl"',
                    'select s, sum(d) from tbl',
                    'select s from tbl where sum(d) > 100.0',
                    'select func(s, i + l, f * d, func2(x)) from tbl',
                    'select s from "!@#$%^&*" where b AND i>0 AND l>=0 OR ( s LIKE "%abc%" AND x IS NOT NULL)'
            ]
    }

}