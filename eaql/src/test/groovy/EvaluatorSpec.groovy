import xw.data.eaql.query.QueryBuilder
import xw.data.eaql.query.QueryEnvironment
import xw.data.eaql.schema.Schema
import xw.data.unistreams.stream.DataSink
import xw.data.unistreams.stream.DataSource
import spock.lang.Specification

class EvaluatorSpec extends Specification
{
    static class DummyEnv implements QueryEnvironment {
        @Override
        Schema getSchema(String ref) {
            return null
        }

        @Override
        DataSource getDataSource(String ref) {
            return null
        }

        @Override
        DataSink getDataSink(String ref, Schema schema) {
            return null
        }
    }

    static final DummyEnv dummyEnv = new DummyEnv()

    static Object[] readFirstRow(String query) {
        DataSource ds = QueryBuilder.build(dummyEnv, query).dataSource
        ds.initialize()
        Object[] row = ds.read()
        ds.close()
        return row
    }

    static boolean valuesEqual(Object v1, Object v2) {
        return v1 == null && v2 == null || !(v1 == null || v2 == null) && v1.equals(v2);
    }

    static boolean rowsEqual(Object[] row1, Object[] row2) {
        if (row1 == null && row2 == null) {
            return true;
        } else if (row1 == null || row2 == null) {
            return false;
        } else if (row1.length  != row2.length) {
            return false;
        } else {
            for (int i=0; i<row1.length; i++) {
                if (! valuesEqual(row1[i], row2[i])) {
                    return false;
                }
            }
            return true
        }
    }

    def "query output equals to value"() {
        given:
            println("[query] " + s[0] + " => " + s[1])
            Object[] row1 = readFirstRow(s[0])
            Object[] row2 = readFirstRow(s[1])
        expect:
            row1.length == row2.length
            rowsEqual(row1, row2)
        where:
            s << [
                    // test selecting constants
                    [ 'SELECT "abc", true, 100, 3.14', 'VALUES("abc", true, 100, 3.14)' ],

                    // test selecting columns
                    [ 'SELECT _col1, _col3, _col2, _col0 FROM ( VALUES("abc", 3.14, true, 100)) ', 'VALUES(3.14, 100, true, "abc")' ],

                    // test '+'
                    [ 'SELECT 1 + 2', 'VALUES(3)' ],
                    [ 'SELECT 3.14 + 3.14', 'VALUES(6.28)' ],
                    [ 'SELECT 1 + 2.0', 'VALUES(3.0)' ],

                    // test '&&'
                    [ 'SELECT true AND true', 'VALUES(true)' ],
                    [ 'SELECT true && false', 'VALUES(false)' ],
                    [ 'SELECT false AND true', 'VALUES(false)' ],
                    [ 'SELECT false && false', 'VALUES(false)' ],

                    // test BETWEEN

                    [ 'SELECT 3 BETWEEN 1 AND 3', 'VALUES(true)' ],
                    [ 'SELECT 2 BETWEEN 0 AND 3.14', 'VALUES(true)' ],
                    [ 'SELECT 9.99 BETWEEN 0 AND 3.14', 'VALUES(false)' ],
                    [ 'SELECT "abc" BETWEEN "abc" AND "rst"', 'VALUES(true)' ],
                    [ 'SELECT "xyz" BETWEEN "abc" AND "rst"', 'VALUES(false)' ],

                    [ 'SELECT 3 NOT BETWEEN 1 AND 3', 'VALUES(false)' ],
                    [ 'SELECT 2 NOT BETWEEN 0 AND 3.14', 'VALUES(false)' ],
                    [ 'SELECT 9.99 NOT BETWEEN 0 AND 3.14', 'VALUES(true)' ],
                    [ 'SELECT "abc" NOT BETWEEN "abc" AND "opq"', 'VALUES(false)' ],
                    [ 'SELECT "xyz" NOT BETWEEN "abc" AND "opq"', 'VALUES(true)' ],

                    // test CASE-expr-WHEN
                    [ 'SELECT CASE _col0 WHEN 1 THEN "A" WHEN 2 THEN "B" WHEN 3 THEN "C" ELSE "else" END FROM ( VALUES(3)) ', 'VALUES("C")' ],
                    [ 'SELECT CASE _col0 WHEN 1 THEN "A" WHEN 2 THEN "B" WHEN 3 THEN "C" ELSE "else" END FROM ( VALUES(4)) ', 'VALUES("else")' ],

                    // test CASE-WHEN
                    [ 'SELECT CASE WHEN _col0 > 0 THEN "positive" WHEN _col0=0 THEN "zero"  ELSE "negative" END FROM ( VALUES(0)) ', 'VALUES("zero")' ],
                    [ 'SELECT CASE WHEN _col0 > 0 THEN "positive" WHEN _col0=0 THEN "zero"  ELSE "negative" END FROM ( VALUES(3.14)) ', 'VALUES("positive")' ],
                    [ 'SELECT CASE WHEN _col0 > 0 THEN "positive" WHEN _col0=0 THEN "zero"  ELSE "negative" END FROM ( VALUES(-99)) ', 'VALUES("negative")' ],

                    // test CAST
                    [ 'SELECT CAST(3 AS double)', 'VALUES(3.0)'],
                    [ 'SELECT CAST(-3.99 AS bigint)', 'VALUES(-3)'],
                    [ 'SELECT CAST(3.14 AS string)', 'VALUES("3.14")'],
                    [ 'SELECT CAST("3.14" AS double)', 'VALUES(3.14)'],
                    [ 'SELECT CAST(CAST(CAST(CAST(100 AS smallint) AS bigint) AS float) AS string)', 'VALUES("100.0")'],

                    // test '/'
                    [ 'SELECT 3 / 2', 'VALUES(1.5)' ],
                    [ 'SELECT 30 / 2', 'VALUES(15.0)' ],
                    [ 'SELECT 3 / 0', 'VALUES(null)' ],
                    [ 'SELECT 3 / 0.0', 'VALUES(null)' ],

                    // test '='
                    [ 'SELECT 3.14 = 3.14 ', 'VALUES(true)'],
                    [ 'SELECT "abc" = "abc" ', 'VALUES(true)'],
                    [ 'SELECT 3 = "3.0" ', 'VALUES(false)'],
                    [ 'SELECT 3 = NULL', 'VALUES(null)'],

                    // test '>'
                    [ 'SELECT "abc" > "ab" ', 'VALUES(true)'],
                    [ 'SELECT "abc" > "abc" ', 'VALUES(false)'],
                    [ 'SELECT 3.14 > 3', 'VALUES(true)'],
                    [ 'SELECT 3.14 > 3.14', 'VALUES(false)'],
                    [ 'SELECT 3.14 > null', 'VALUES(null)'],

                    // test '>='
                    [ 'SELECT "abc" >= "ab" ', 'VALUES(true)'],
                    [ 'SELECT "ab" >= "abc" ', 'VALUES(false)'],
                    [ 'SELECT "abc" >= "abc" ', 'VALUES(true)'],
                    [ 'SELECT 3.14 >= 3', 'VALUES(true)'],
                    [ 'SELECT 3.14 >= 3.14', 'VALUES(true)'],
                    [ 'SELECT 3.14 >= null', 'VALUES(null)'],

                    // test IN-set
                    [ 'SELECT "abc" IN ("ab", "bc") ', 'VALUES(false)'],
                    [ 'SELECT "abc" IN ("abc", "xyz", null) ', 'VALUES(true)'],
                    [ 'SELECT "abc" NOT IN ("ab", "bc") ', 'VALUES(true)'],
                    [ 'SELECT "abc" NOT IN ("abc", "xyz", null) ', 'VALUES(false)'],

                    // test IS-NULL
                    [ 'SELECT "abc" IS NULL ', 'VALUES(false)'],
                    [ 'SELECT 1/0 IS NULL ', 'VALUES(true)'],
                    [ 'SELECT "abc" IS NOT NULL ', 'VALUES(true)'],
                    [ 'SELECT 1/0 IS NOT NULL ', 'VALUES(false)'],

                    // test '<'
                    [ 'SELECT "abc" < "ab" ', 'VALUES(false)'],
                    [ 'SELECT "ab" < "abc" ', 'VALUES(true)'],
                    [ 'SELECT "abc" < "abc" ', 'VALUES(false)'],
                    [ 'SELECT 3.14 < 3', 'VALUES(false)'],
                    [ 'SELECT 3.14 < 3.14', 'VALUES(false)'],
                    [ 'SELECT 3.14 < null', 'VALUES(null)'],

                    // test '<='
                    [ 'SELECT "abc" <= "ab" ', 'VALUES(false)'],
                    [ 'SELECT "abc" <= "abc" ', 'VALUES(true)'],
                    [ 'SELECT 3.14 <= 3', 'VALUES(false)'],
                    [ 'SELECT 3.14 <= 3.14', 'VALUES(true)'],
                    [ 'SELECT 3.14 <= null', 'VALUES(null)'],

                    // test LIKE
                    [ 'SELECT "abc" LIKE "abc"', 'VALUES (true)'],
                    [ 'SELECT "abc" LIKE "ab%"', 'VALUES (true)'],
                    [ 'SELECT "abc" LIKE "%bc"', 'VALUES (true)'],
                    [ 'SELECT "abc" LIKE "%b%"', 'VALUES (true)'],
                    [ 'SELECT "abc" LIKE "%x%"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT LIKE "abc"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT LIKE "ab%"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT LIKE "%bc"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT LIKE "%b%"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT LIKE "%x%"', 'VALUES (true)'],

                    // test RLIKE
                    [ 'SELECT "abc" RLIKE "abc"', 'VALUES (true)'],
                    [ 'SELECT "abc" RLIKE "ab"', 'VALUES (true)'],
                    [ 'SELECT "abc" RLIKE "bc"', 'VALUES (true)'],
                    [ 'SELECT "abc" RLIKE "b"', 'VALUES (true)'],
                    [ 'SELECT "abc" RLIKE "x"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT RLIKE "abc"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT RLIKE "ab"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT RLIKE "bc"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT RLIKE "b"', 'VALUES (false)'],
                    [ 'SELECT "abc" NOT RLIKE "x"', 'VALUES (true)'],
                    [ 'SELECT "abc" RLIKE "^a.*c$"', 'VALUES (true)'],
                    [ 'SELECT "abc" RLIKE "^\\w*$"', 'VALUES (true)'],

                    // test '*'
                    [ 'SELECT 3*2', 'VALUES(6)'],
                    [ 'SELECT 3.0*2', 'VALUES(6.0)'],
                    [ 'SELECT 3.0*2.0', 'VALUES(6.0)'],

                    // test negation
                    [ 'SELECT -(3*2)', 'VALUES(-6)'],
                    [ 'SELECT -(3.0*2)', 'VALUES(-6.0)'],
                    [ 'SELECT -(3.0*2.0)', 'VALUES(-6.0)'],

                    // test NOT
                    [ 'SELECT NOT true', 'VALUES(false)'],
                    [ 'SELECT NOT (1=2)', 'VALUES(true)'],
                    [ 'SELECT NOT (1/0)', 'VALUES(null)'],

                    // test '!='
                    [ 'SELECT 3.14 != 3.14 ', 'VALUES(false)'],
                    [ 'SELECT "abc" != "abc" ', 'VALUES(false)'],
                    [ 'SELECT 3 != "3.0" ', 'VALUES(true)'],
                    [ 'SELECT 3 != NULL', 'VALUES(null)'],

                    // test PARENTHETICAL
                    [ 'SELECT (1+2)*(3+4)', 'VALUES(21)'],

                    //  test '-'
                    [ 'SELECT 1 - 2', 'VALUES(-1)' ],
                    [ 'SELECT 1 - 1', 'VALUES(0)' ],
                    [ 'SELECT 3.14 - 3.14', 'VALUES(0.0)' ],
                    [ 'SELECT 1 - 2.0', 'VALUES(-1.0)' ],

                    // test compound data types and element indexing expressions
                    [ 'SELECT _col0[2], _col1["k1"], _col2.col3 FROM (VALUES ( create_array(1,2,3), create_map("k1", 1, "k2", 2, "k3", 3), create_struct(true, 1, 3.14, "abc")))', 'VALUES(3, 1, 3.14)'],
                    [ 'SELECT _col0.col3["key"][2] FROM (VALUES (create_struct("value1", "value2", create_map("key", create_array(1,2,3)))))', 'VALUES(3)'],

                    // test date/timestamp functions
                    [ 'SELECT date_format(to_date("2016-02-29"), "MM-dd") = "02-29"', 'VALUES(true)'],
                    [ 'SELECT datediff("2016-01-01", "2017-01-01")', 'VALUES(-366)'],
                    [ 'SELECT date_add("2016-02-28", 2) = "2016-03-01"', 'VALUES(true)'],
                    [ 'SELECT date_add("2016-03-01", -2) = "2016-02-28"', 'VALUES(true)'],
                    [ 'SELECT date_sub("2016-03-01", 2) = "2016-02-28"', 'VALUES(true)'],
            ]
    }
}
