import xw.data.eaql.EAQL
import spock.lang.Specification

class ModelBuilderSpec extends Specification {

    def "model builder output persists "() {
        given:
        print("[parse] " + stmt)
        String stmt1 = EAQL.parseStatement(stmt).toString()
        println(" => " + stmt1)
        String stmt2 = EAQL.parseStatement(stmt1).toString()
        expect:
        stmt1.equals(stmt2)
        where:
        stmt << [
                """DESCRIBE TABLE a.b;""",
                """SELECT x,y,z FROM "any-data-source-ref" WHERE x=0 AND y>1.2 GROUP BY z""",
                """SELECT *, count(*) AS count, sum(x)
                    FROM a.b
                    WHERE x * y + z > 0 AND find(s, s1) IS NOT NULL AND
                    ( s LIKE "%xyz%" OR t NOT BETWEEN 0.1 AND 0.2 OR NOT (y > 1.0) )
                """,
                """SELECT x, cnt FROM (SELECT x,y,cnt FROM 'a.b.c.d' WHERE x > 0 GROUP BY x, y) GROUP BY x """,
                """sElEcT x1 AS x, cnt1 AS cnt FROM a.b UnIoN SeLeCt x2 AS x, cnt2 AS cnt FROM a.c ORDER BY x desc""",
                """select cast(x as string) as string_x from tbl""",
                """select x, "1", a[1], m["key"], s.fld_name from tbl limit 100 offset 99""",
                'INSERT INTO "any-ref." SELECT * FROM a.b',
                'INSERT OVERWRITE "any-ref." SELECT * FROM a.b',

        ]
    }
}