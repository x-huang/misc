import xw.data.eaql.model.type.Types
import spock.lang.Specification

/**
 * Created by xhuang on 8/16/16.
 */
public class TypeParserSpec extends Specification
{
    def "two type strings strictly equal"() {
        given:
            def t1 = Types.fromString(s[0])
            def t2 = Types.fromString(s[1])
        expect:
            Types.strictlyEqual(t1, t2)
        where:
            s << [
                    ['string', 'String'],
                    ['bigint', 'long'],
                    ['ARRAY<bool>', "array<BOOLean>"],
                    ['struct<col1:map<int,date>,col2:array<uniontype<tinyint,smallint,int,bigint,decimal>>>',
                            'STRUCT<col1:MAP<INT,DATE>,col2:ARRAY<UNIONTYPE<TINYINT,SMALLINT,INT,BIGINT,DECIMAL>>>']
            ]
    }

    def "two type strings semantically equal"() {
        given:
            def t1 = Types.fromString(s[0])
            def t2 = Types.fromString(s[1])
        expect:
            Types.semanticallyEqual(t1, t2)
        where:
            s << [
                    ['string', 'String'],
                    ['bigint', 'long'],
                    ['ARRAY<bool>', "array<BOOLean>"],
                    ['struct<col1:map<int,date>,col2:array<uniontype<tinyint,smallint,int,bigint,decimal>>>',
                            'STRUCT<col1:MAP<INT,DATE>,col2:ARRAY<UNIONTYPE<TINYINT,SMALLINT,INT,BIGINT,DECIMAL>>>'],
                    ['struct<col1:map<int,date>,col2:array<uniontype<tinyint,smallint,int,bigint,decimal>>>',
                            'STRUCT<col3:MAP<INT,DATE>,col4:ARRAY<UNIONTYPE<TINYINT,SMALLINT,INT,BIGINT,DECIMAL>>>']
            ]
    }

}