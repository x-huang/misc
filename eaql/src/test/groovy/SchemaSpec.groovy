import xw.data.eaql.schema.Schema
import spock.lang.Specification

/**
 * Created by xhuang on 7/31/2015.
 */
class SchemaSpec extends Specification {

    def "schema equals after serialization and deserialization"() {
        given:
            def schema1 = Schema.fromString(s);
            def s1 = schema1.toString();
            def schema2 = Schema.fromString(s1)
            def s2 = schema2.toString()

        expect:
            schema1.equals(schema2)
            s1.equals(s2)

        where:
            s << [
                'title,platform,`date`,adnetwork,country,impressions:long,clicks:long,attributed_installs:long,cost:double,matched_installs:long',
                'struct<col1:map<int,date>,col2:array<uniontype<tinyint,smallint,int,bigint,decimal>>,string_1,string_2>'
            ]
    }

}
