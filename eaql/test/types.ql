
-- test array indexing and struct field
SELECT msgids, msgids[1], cmn, cmn.userid, cmn.druid
FROM "hive:(fifa_gt.fifafut_mm_gt?_limit=3&_allowpartialspec=true)";

-- test big decimal
-- note that HiveDecimal shall be casted to BigDecimal
select edw_offer_item_id, item_total_amt, usd_exchange_rate
FROM "hive:(billing_stg.user_item_spend_real_fps_inc?_limit=10&_allowpartialspec=true)";


-- test date and timestamp
SELECT install_date, install_timestamp
FROM "hive:(pin_stest.user_game?_limit=10&_allowpartialspec=true)";

-- test varchar
SELECT publisher_sub_site_name
FROM "hive:(mobile.etl_hasoffers?_limit=10&_allowpartialspec=true)";

