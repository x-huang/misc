
SELECT * FROM "hive:(meta_hd.skus)";

SELECT title, sum(sid), sum(msid*msid) 
FROM "hive:(meta_hd.skus)" 
WHERE platform = "pc" GROUP BY title ORDER BY title;

SELECT year,title,100*(sum(sid)+sum(msid*msid))
FROM meta_hd.skus
WHERE platform = "pc" 
GROUP BY year, title
ORDER BY year desc, title;

SELECT count(if(platform  LIKE "ps%", 1, null))  
FROM "hive:(default.skus)";

SELECT printf("platform=%s,sum=%d", platform, sum(msid)) 
from "hive:(meta_hd.skus)" 
GROUP BY platform;


SELECT printf("platform=%s, sum=%d", p, sum(ms)) as description 
FROM 
(	SELECT lcase(platform) as p, sum(msid) as ms 
	from "hive:(meta_hd.skus)" 
	GROUP BY platform
) 
group by p 
ORDER by description;


values ("fake1", 0) 
union 
select distinct lcase(platform) as platform, 1 as flag from meta_hd.skus 
union 
values ("fake2", 0) 
order by platform;


