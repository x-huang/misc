SELECT CAST ("314" AS LONG); 
 
SELECT if(100>99,  "yes", "no"); 
 
SELECT if(100>99+2,  "yes", "no"); 
 
SELECT concat("a",  "b", "c"); 
 
SELECT coalesce(null,  null, 999);
 
SELECT coalesce(true,  false );

select id, sum(count*count) 
from (
	select "abc" as id, 10000 as count 
	union 
	select "abc" as id, 999 as count 
	union 
	select "xyz" as id, -1 as count
) 
group by id
order by id desc;


select * from (values (1,"abc"), (2, "xyz"), (3, "opq") ) where _col0 > 1;
select * from (values (1,"abc"), (2, null), (null, "opq") ) where _col0 > 1;

-- test case-when
select case when _col0>2 then ">2" else "<=2" end, _col1
from (values (1,"abc"), (2, "xyz"), (3, "opq") );

-- test case-expr-then
select case _col0+1 when 3 then "=2" else "!=2" end, _col1
from (values (1,"abc"), (2, "xyz"), (3, "opq") );


-- test collections
SELECT array_contains(collect_set(id), "abc") as id from (
    select "abc" as id, 10000 as count
    union
    select "abc" as id, 999 as count
    union
    select "xyz" as id, -1 as count
);

SELECT collect_list(id)[2] as id from (
    select "abc" as id, 10000 as count
    union
    select "abc" as id, 999 as count
    union
    select "xyz" as id, -1 as count
);

-- test split
SELECT split("abc.xyz.srt", "\.");

-- test SELECT DISTINCT
SELECT DISTINCT _col0 FROM ( VALUES("abc"), ("xyz"), ("abc"));


