# EAQL

### Introduction

`EAQL`, the *Easy Analytic Query Language*, is a minimal SQL engine and accompanying utility tools working on immutable datasets. Being an analytic query language, it suits best for small to moderately sized, schema-ful data, and features responsiveness, simplicity, and generality, at both console interaction level and Java API level. 

Meanwhile, `EAQL` is:

- Not a full SQL implementation. One obvious missing feature is the *JOIN*-operation.
- Not updatable. The meaning of *immutable* is one cannot update or append data to existing resources; it supports only reading and writing-entirely.
- Not supposed to work on very large datasets. `EAQL` is single-threaded and processes data in streaming manner thus will take quite long dealing with over giga-bytes data. In case of *SELECT*-statements having *GROUP BY*, `EAQL` inevitably needs to aggregate data in memory, and will potentially run out of memory. 
- Not a JDBC engine or client where you can prepare a statement and reuse it multiple times. It only takes materialized query statements in text representation.  

In short, use `EAQL` as an analytic tool, not as an ETL processor. 

The shortest API usage example:

```java
final String stmt = "SELECT * FROM 'prod::hive:(db1.tbl1)'" +
	" WHERE col1=\"some_value\" AND col2 BETWEEN 1 AND 10" + 
	" GROUP BY col3" +
	" ORDER BY col4 DESC";
final DataSource stream = EAQL.getQuery(stmt).openResultSet();

stream.initialize(); // may throw IOException
Object[] row = null;
while ((row = stream.read()) != null) { // may throw IOException
    System.out.println(Arrays.toString(row));
}
stream.close(); // may throw IOException
```

Note that in the SQL statement, instead of having a table name in the *FROM*-clause, `EAQL` extends it to accept a string of `resource descriptor`, as explained in `Unistreams` README. This is the compelling feature of `EAQL` to allow accessing various data resources in different storages/clusters/formats using this single platform. The example shows fetching data from a Hive table in the *prod* cluster. 

The best scenarios of using `EAQL` would be:

- Do some light-weighted, ad-hoc queries that are not worth launching Hive queries.
- In a need of accessing data resources in different cluster environment, hence executing one single hive script is not feasible. 
- Want to embed data accessing functionality in your program but don't want to dive into Hive/JDBC/file-system APIs.
- Use in a web application backend as the DAO layer with query statements hard-coded or dynamically constructed from user request. 
- Put in the final stage in a ETL job for some data summarization/validation/re-formatting work.
- Need a convenient way to temporarily cache data for late usage.
- (new in 0.2.1) Validate or transform unstructured data (schema-less raw data made of primitives/string/List/Map, e.g., POJO obtained by JSON object mapper). Thus it is useful in data ingestion phase. 

### Language Features

`EAQL` aims the Hive Query Language as prototype and tries to comply with its grammar as much as possible. The language features:

- All primitive SQL data types and three compound data types: `array`, `map`, `struct`.
- Full set of expressions, including literal, column value, boolean, arithmetic, `CAST`, `array`/`map`/`struct` dereference, functions, aggregate functions, `BETWEEN`, `LIKE`, `CASE`, `CASE-WHEN`, ...
- `DESCRIBE`-statement to get data schema.
- `SELECT` and `SELECT DISTINCT`-statement with `WHERE`, `GROUP BY`, `ORDER BY`, `LIMIT`, `OFFSET` clauses.
- `VALUES`-statement to present dummy data.
- `SELECT` or `VALUES` are embeddable in `FROM`-clause.
- `UNION` operator to concatenate several `SELECT` or `VALUES` statements.
- `INSERT` statement to write results.


##### Grammer

The parser is written in ANLTR 4. Here is a core part of `EAQL` g4 grammar: 

```
grammar EAQL;

parse
 : ';'* sql_stmt ( ';'+ sql_stmt) * ';'* EOF
 ;

sql_stmt
 : describe_table_stmt
 | select_stmt
 | insert_stmt
 ;

describe_table_stmt
 : (K_DESC | K_DESCRIBE) K_TABLE table_name
 | (K_DESC | K_DESCRIBE) resource_descriptor
 ;

select_stmt
 : select_or_values ( compound_operator select_or_values )* order_by_clause? limit_clause? offset_clause?
 ;

select_or_values
 : simple_select_stmt
 | values_stmt
 ;

values_stmt
 : K_VALUES  '(' expr_list ')' ( ',' '(' expr_list ')' )*
 ;

expr_list
 : expr ( ',' expr )*
 ;

simple_select_stmt
 : select_clause from_clause? where_clause? group_by_clause?
 ;

select_clause
 : K_SELECT K_DISTINCT? result_column ( ',' result_column )*
 ;

from_clause
 : K_FROM table_name
 | K_FROM resource_descriptor
 | K_FROM '(' select_stmt ')'
 ;

where_clause
 : K_WHERE expr
 ;

group_by_clause
 : K_GROUP K_BY column_name ( ',' column_name )*
 ;

order_by_clause
 : K_ORDER K_BY ordering_term ( ',' ordering_term )*
 ;

limit_clause
 : K_LIMIT NUMERIC_LITERAL
 ;

offset_clause
 : K_OFFSET NUMERIC_LITERAL
 ;

insert_stmt
 : K_INSERT (K_OVERWRITE | K_INTO) resource_descriptor select_stmt
 ;

expr
 : asterisk
 | literal_value
 | column_name
 | function
 | expr '[' expr ']'
 | expr '.' IDENTIFIER
 | unary_operator expr
 | expr ( '*' | '/' ) expr
 | expr ( '+' | '-' ) expr
 | expr ( '<' | '<=' | '>' | '>=' ) expr
 | expr ( '=' | '==' | '!=' | '<>') expr
 | expr K_NOT? K_LIKE literal_value
 | expr K_NOT? K_RLIKE literal_value
 | expr K_IS K_NOT? K_NULL
 | expr K_NOT? K_IN ( '(' literal_value ( ',' literal_value )*  ')')
 | expr K_NOT? K_BETWEEN literal_value K_AND literal_value
 | K_NOT expr
 | expr ( K_AND | '&&' ) expr
 | expr ( K_OR | '||' ) expr
 | parenthetical
 | K_CAST '(' expr K_AS data_type ')'
 | K_CASE expr? ( K_WHEN expr K_THEN expr )+ ( K_ELSE expr )? K_END
 ;

ordering_term
 : expr ( K_ASC | K_DESC )?
 ;

result_column
 : expr ( K_AS? column_alias )?
 ;

compound_operator
 : K_UNION K_ALL?
 ;
``` 

### UDFs and UDAFs

*UDF* stands for [*User Defined Functions*](https://cwiki.apache.org/confluence/display/Hive/LanguageManual+UDF#LanguageManualUDF-Built-inFunctions), `EAQL` chooses to implement some of the Hive build-in UDFs: `abs`, `array`, `array_contains`, `coalesce`, `concat`, `current_date`, `current_timestamp`, `date_add`, `date_format`, `date_sub`, `datediff`, `e`, `floor`, `if`, `length`, `lower`, `map`, `pi`, `printf`, `round`, `size`, `split`, `str_to_map`, `struct`, `to_date`, `upper`. The list may expand in further release.

*UDAF* stands for [*User Defined Analytic Functions*](https://cwiki.apache.org/confluence/display/Hive/LanguageManual+UDF#LanguageManualUDF-Built-inAggregateFunctions(UDAF)).
`EAQL` supports these UDAFs: `avg`, `collect_list`, `collect_set`, `count`, `max`, `min`, `sum`.

Table-Generating Functions (*UDTF*) is not supported.

### The EAQL Console

The usage of `EAQL` console is much like Hive console. One can use arrows keys and Ctrl+S, Ctrl+R to edit lines and navigate lines history. Example usage of this console tool:

```
$ bin/console.sh
eaql> help;
[ EAQL JLine-enabled console ]
Supported commands:
  quit/exit - exit this console program
  help - print this help screen
  set <name>=<value> - set global variable which can be used as property value in resource string
  unset <name> - unset global variable, or '*' to clear settings
  show settings - print global settings
  show grammar - print core part of EAQL antlr4 grammar
  show UDFs - print supported UDFs
  show UDAFs - print supported UDAFs
  show cached schema - print cached schema
  <eaql-stmts> - use 'show grammar' for EAQL antlr4 grammar; see test/*.ql for examples
Supported hotkeys:
  <up>/<down>, ^P/^N - browse historical input lines
  ^S/^R - search/reverse-search historical inputs
  ^L - clear screen
  ^C - quit current running query
  ^D - end console session
```

Showcase the *SELECT*-statement and expressions:

```  
eaql> select id, sum(count*count)
    > from (
    >        select "abc" as id, 10000 as count
    >        union
    >        select "abc" as id, 999 as count
    >        union
    >        select "xyz" as id, -1 as count
    > )
    > group by id
    > order by id desc;
xyz    	1
abc    	100998001
eaql> select case when _col0>2 then ">2" else "<=2" end, _col1
    > from (values (1,"abc"), (2, "xyz"), (3, "opq") );
<=2    	abc
<=2    	xyz
>2     	opq
```

Redirect query result into other location: 

```
eaql> insert into "orc:file:///tmp/my_tmp_data.orc" select * from (values (1,"abc"), (2, "xyz"), (3, "opq") );
...
21:30:55.645 [pool-2-thread-1] DEBUG OrcFileSink - initialized writing to ORC file: file:///tmp/my_tmp_data.orc (compress: ZLIB, compress-size: 16000, stripe-size: 102400, block-padding: true, typename: STRUCT/StandardStructObjectInspector/struct<_col0:bigint,_col1:string>)
21:30:55.649 [pool-2-thread-1] INFO  Console - data written
eaql> select * from "orc:file:///tmp/my_tmp_data.orc";
1      	abc
2      	xyz
3      	opq
eaql> quit;
bye
```

Tips: 

- Use *set* command to avoid writing lengthy resource descriptor strings.
- `EAQL` caches data schema to avoid reading metadata for seen resources (it is very helpful for Hive resources since Hive metastore is slow). Fetched schema strings are written into *$PWD/.schemacache* and persists console sessions. But sometimes data schema undergoes *alteration*, in that case please manually delete *.schemacache* file, or edit it accordingly.
- For query results that will be reused often, it is recommended to `INSERT INTO` an *ORC* file and read from it instead afterwards. Choose *ORC* format because it persists data schema.

### Query Examples and Idioms

Simple examples reading dummy data:

```sql
SELECT CAST ("314" AS LONG);

SELECT if(100>99,  "yes", "no");

SELECT if(100>99+2,  "yes", "no");

SELECT concat("a",  "b", "c");

SELECT coalesce(null,  null, 999);

SELECT coalesce(true,  false );

select id, sum(count*count)
from (
       	select "abc" as id, 10000 as count
       	union
       	select "abc" as id, 999 as count
       	union
       	select "xyz" as id, -1 as count
)
group by id
order by id desc;

select * from (values (1,"abc"), (2, "xyz"), (3, "opq") ) where _col0 > 1;

select * from (values (1,"abc"), (2, null), (null, "opq") ) where _col0 > 1;

-- test case-when
select case when _col0>2 then ">2" else "<=2" end, _col1  from (values (1,"abc"), (2, "xyz"), (3, "opq") );

-- test case-expr-then
select case _col0+1 when 3 then "=2" else "!=2" end, _col1  from (values (1,"abc"), (2, "xyz"), (3, "opq") );
```

Example of accessing compound data types:

```sql
-- test array indexing and struct field
SELECT msgids, msgids[1], cmn, cmn.userid, cmn.druid
FROM "hive:(db1.tbl1)?_limit=3&_allowpartialspec=true)";
```

Examples of accessing raw data files:

```sql
set my_location = ... 

describe 'orc:hdfs:///hive/warehouse/${my_location}/000000_0';

select _col1, _col2  from 'orc:hdfs:///hive/warehouse/${my_location}/000000_0';

select distinct _col1, _col2  from 'orc:hdfs:///hive/warehouse/${my_location}/000000_0';

-- row count of distinct tuples
select count(1) FROM (select distinct _col1, _col2  from 'orc:hdfs:///hive/warehouse/${my_location}/000000_0');

-- row count of all selected tuples
select count(1) FROM (select _col1, _col2  from 'orc:hdfs:///hive/warehouse/${my_location}/000000_0');
```

Examples of querying Hive data:

```sql
set my_table = ...

SELECT * FROM "hive:(${my_table})";

SELECT title, sum(sid), sum(msid*msid)
FROM "hive:(${my_table})"
WHERE platform = "pc" GROUP BY title ORDER BY title;

SELECT year,title,100*(sum(sid)+sum(msid*msid))
FROM ${my_table}
WHERE platform = "pc"
GROUP BY year, title
ORDER BY year desc, title;

SELECT count(if(platform  LIKE "ps%", 1, null))
FROM "hive:(${my_table})";

SELECT printf("platform=%s,sum=%d", platform, sum(msid))
from "hive:(${my_table})"
GROUP BY platform;

SELECT printf("platform=%s, sum=%d", p, sum(ms)) as description
FROM
(      	SELECT lcase(platform) as p, sum(msid) as ms
       	from "hive:(${my_table})"
       	GROUP BY platform
)
group by p
ORDER by description;

-- example of SELECT DISTINCT
values ("fake1", 0)
union
select distinct lcase(platform) as platform, 1 as flag from ${my_table}
union
values ("fake2", 0)
order by platform;
```

Example of querying mysql database:

```sql
select * from "jdbc:mysql://user:password@mysql.host/db1/tbl1?_select=name,id" LIMIT 10;
```

### Unstructured Query

Since version 0.2.1, EAQL is extended to support query on unstructured data at API level. Programer can obtain an `UnstructuredQuery` instance by providing a simple select statement, which

- Cannot have a `FROM` clause. Unlike having the `unistreams` library for structured data, EAQL does not include a media access library for unstructured data, thus user need to implement her own, mostly by a JSON parser. 
- Cannot have a `GROUP BY` clause. Aggregation does not make sense on unschemaed data. 

`UnstructuredQuery` statement uses the special name `_` to represent the input POJO, as if you have an incoming record which has only one column named "_" and its column value is the POJO of *any* type. See examples below.

`UnstructuredQuery` performs query one POJO at a time. Method `query` returns null if the `WHERE` condition is not satisfied, or an array of values extracted by computing the `SELECT` expressions. 

Code snippet:

```java
String json = ... // json string
String stmt = ... // a simple select statement

UnstructuredQuery query = EAQL.getUnstructuredQuery(stmt);
Object pojo = (new Gson()).fromJson(json, Map.class);
Object[] result = query.query(pojo);
System.out.println(Arrays.toString(result));
```        

Usage example:

```
$ java xw.data.eaql.cli.UnstructuredQuery --help
[intro] executing unstructured query on a JSON value
Option           Description
------           -----------
-h, --help       print usage
--json [String]  a JSON string to be converted to POJO, read from stdin if
                   omitted
--stmt <String>  simple select statement without FROM and GROUP-BY clause

$ cat example.json
{"widget": {
	"debug": "on",
	"window": {
		"title": "Sample Konfabulator Widget",
		"name": "main_window",
		"width": 500,
		"height": 500
	},
	"image": {
		"src": "Images/Sun.png",
		"name": "sun1",
		"hOffset": 250,
		"vOffset": 250,
		"alignment": "center"
	},
	"text": {
		"data": "Click Here",
		"size": 36,
		"style": "bold",
		"name": "text1",
		"hOffset": 250,
		"vOffset": 100,
		"alignment": "center",
		"onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
	}
}}

$ cat example.json | java xw.data.eaql.cli.UnstructuredQuery --stmt '
    SELECT concat(_.widget.image.src, "@", _.widget.window.name,": area=",
                    (_.widget.window.width * _.widget.window.height)),
            _.widget.image.alignment = "center",
            _.widget.window.width > 640 AND _.widget.window.height > 480,
            _.widget.text.onMouseUp IS NOT NULL
'
[result] [Images/Sun.png@main_window: area=250000.0, true, false, true]

$ cat example.json | java xw.data.eaql.cli.UnstructuredQuery  --stmt '
    SELECT concat(_.widget.image.src, "@", _.widget.window.name,": area=",
                    (_.widget.window.width * _.widget.window.height)),
            _.widget.image.alignment = "center",
            _.widget.window.width > 640 AND _.widget.window.height > 480,
            _.widget.text.onMouseUp IS NOT NULL
    WHERE _.widget.debug = "on"
'
[result] [Images/Sun.png@main_window: area=250000.0, true, false, true]

$ cat example.json | java xw.data.eaql.cli.UnstructuredQuery  --stmt '
    SELECT concat(_.widget.image.src, "@", _.widget.window.name,": area=",
                    (_.widget.window.width * _.widget.window.height)),
           _.widget.image.alignment = "center",
           _.widget.window.width > 640 AND _.widget.window.height > 480,
           _.widget.text.onMouseUp IS NOT NULL
    WHERE _.widget.debug = "off"
'
[result] null
```

### Dev Notes

`EAQL` implementation consists of these three parts:

- a SQL parser empowered by ANTLR 4,
- an analytic query engine,
- a unified data access layer (see my another project: `Unistreams`).

and explicitly depends on these libraries:

- slf4j 1.7.x
- antlr4-runtime 4.5.x
- unistreams 0.2
- jline 2.x (optional, only for console usage)

Typically, from taking a query statement to returning a data stream, `EAQL` goes through the following steps:

1. Parse it into an ANTLR syntax tree. 
2. Traverse the syntax tree and construct a *SQLStatement* object. This object is merely a model holding all information presented in statement literal. *SQLStatement* is analogous to query specification.
3. From the *SQLStatement*, build a *SQLQuery* object that specifies concrete procedures that combine into a query process: filtering with *WHERE*-conditions, group-by, order-by, slicing, projection, concatenation, ... All procedures are abstracted as transforming of a data stream. Note that *SQLQuery* is *lazy*: no real I/O is conducted in this step. *SQLQuery* is analogous to query plan.
4. Return user the *DataSource* of the *SQLQuery* (for *SELECT* or *DESCRIBE*) and let user to *open*, *read/write*, *close* it. Those actions all may throw *IOException*.  

`EAQL` will not return *DataSink* for user to programmatically write data into. *DataSink* is internally used in *INSERT*-query only.

### Disclaimer

`EAQL` is a complicated implementation and yet lacks polishing. There may be still errors in some corner cases, especially related to correctness. So if used in production scenario, always pick some results and verify with their Hive/SQL counterparts before releasing. Report error to the author if inconsistency found.    

<!-- this file is compiled by MacDown using github 2 style -->