
HADOOP_CLASSPATH=`hadoop classpath 2> /dev/null`
CONFDIR=`pwd`/conf
LIBDIR=`pwd`/lib

PATHS=(
	$CONFDIR
	$LIBDIR/*
	$HIVE_HOME/lib/*
	$HIVE_HOME/conf
)

SAVE_IFS=$IFS
IFS=":"
CLASSPATH="${PATHS[*]}:$HADOOP_CLASSPATH"
IFS=$SAVE_IFS

export CLASSPATH

export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native/Linux-amd64-64:/usr/lib/hadoop-2.7.0/lib/native

java xw.data.eaql.cli.Console $*

